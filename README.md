# quickloox-contest

## Cambiamenti recenti

Comma è tornato quello originale e le modifiche al sorgente sono state eliminate per possibili problemi di sicurezza. Sono stati aggiunti due processi in javascript, al di fuori di comma, che si occupano di rendere quanto scritto in markdown nei commenti dopo averlo sanificato da eventuale codice malevolo.

Quanto segue è relativo al contest non competitivo che riguarda [Quickloox](https:macintelligence.org).

Questo repo contiene dei file di esempio da poter utilizzare per costruire un flusso di lavoro che consenta di importare i vecchi commenti di Muut nel nuovo sistema di commenti autogestito.

Per conoscere meglio la struttura dei dati dei commenti attuali consiglio di leggere [la pagina di comma](https://github.com/Dieterbe/comma). In questa repo trovate dei file con suffisso .cmt di esempio. Sono file xml, uno per ogni commento che viene lasciato. È sufficientemente intuitivo riconoscere la struttura di ogni singolo file e come si colleghi con il post relativo.

I commenti salvati da Muut sono in formato json e contenuti in un unico grande file. Per semplificare un primo approccio con il problema ho estratto dei commenti in formato json e creato dei commenti, ai medesimi post, con comma. Comma non prevede un sistema di repliche dirette a un commento o la sezione "likes".

Ho aggiunto due cartelle

- blog-v4
- comma

### blog-v4

Questa cartella replica la cartella del blog Quickloox per gli esperimenti. È la cartella da cui partire per far girare in locale il blog con il comando

`hugo server --config-dev.toml --disableFastRender --gc`

Parte un server locale raggiungibile a localhost:1313.

Nella cartella content sono contenuti i post e le altre pagine del sito.
Nella cartella layouts/partials trovate comments.html sul quale stiamo discutendo.
La parte finale del file static/css/hugo-octopress.css contiene il css dei commenti.

N.B.: hugo non necessita dell'istallazione di Go.

### comma

Qui trovate i sorgenti di comma e un eseguibile compilato per Apple Silicon M1. Per altri processori dovete scaricare Go e compilare i sorgenti.

Create una cartella per i commenti se ne volete una nuova vuota, altrimenti avete già nel repo la cartella commenti con il risultato del lavoro di paoloo.

Per far partire comma aprite una nuova finestra del terminale e digitate

`./~/path/to/comma nome-cartella-commenti :5888`

Partirà anche qui un server dedicato ai commenti.

Se tutto è andato per il verso giusto, dopo un refresh, la pagina di qualsiasi post mostrerà lo spazio per i commenti.

Richieste di chiarimenti attraverso i soliti canali.

Vanno pubblicamente ringraziati -uso i nickname di Slack- briand06, macmomo e paoloo, che stanno dando contributi fondamentali alla struttura del blog.

## Commento tecnico a comma

## layouts/partial/comments.html

This is a JavaScript code that handles comments on a webpage. It appears to be a part of a larger application, possibly a blog or a forum, built using a templating engine (likely Hugo, given the `{{ .Site.Params.commentServer }}` syntax).

Here's a breakdown of the code:

### Comment Form

The code defines a comment form with the following fields:

- `name`: a text input for the commenter's name
- `message`: a textarea for the comment text
- `post`: a hidden input that stores the post ID (set to the current file's base filename)
- `special`: a hidden input that stores a special value (set to "PIPPO" in this example)

### Comment Preview

When the user types in the `message` textarea, the code generates a preview of the comment using the `typeComment` function. This function:

- Resizes the textarea to fit the comment text
- Creates a preview HTML element with the comment text, author name, and avatar
- Updates the preview element with the sanitized comment text using `DOMPurify` and `marked` libraries

### Comment Submission

When the user submits the form, the code sends a POST request to the comment server (defined in `{{ .Site.Params.commentServer }}`) with the form data. The `sendData` function:

- Sanitizes the comment text using `DOMPurify` and `codeToHTMLentities` functions
- Sets the `message` field to the sanitized comment text
- Sends the form data to the comment server
- Updates the comment form button text and class based on the response status

### Comment Display

The code defines an `addComment` function that adds a new comment to the comments container element. This function:

- Creates a new HTML element for the comment with the author name, avatar, and comment text
- Appends the comment element to the comments container

### Initialization

The code uses the `domReady` function to wait for the DOM to be ready before executing the initialization code. The initialization code:

- Sets up the comment form event listeners
- Fetches the comments from the comment server and adds them to the comments container using the `addComment` function

Overall, this code provides a basic comment system with preview, submission, and display functionality. However, it may require additional modifications to fit the specific requirements of your application.

## main.go

This Go code is a simple web server that handles comments for a Hugo static blog. It listens for HTTP requests on a specified address and port, and responds to GET and POST requests.

Here's a breakdown of the code:

### Main Function

The main function initializes the server by:

- Checking the command-line arguments for the comments path, address, and special value (if provided)
- Setting up the HTTP handler for the root URL ("/") using the `cors.Default().Handler` middleware
- Starting the HTTP server using `http.ListenAndServe`

### Handle Post Function

The `handlePost` function handles POST requests to the server. It:

- Parses the multipart form data from the request
- Checks if the special value is provided and matches the expected value
- Creates a new `Comment` struct from the form data using the `CommentFromForm` function
- Saves the comment to the comments path using the `Save` method
- Marshals the comment to JSON and writes it to the response writer

### Comment From Form Function

The `CommentFromForm` function creates a new `Comment` struct from the form data. It:

- Checks if the required fields (message, name, post) are present in the form data
- Creates a new `Comment` struct with the form data values
- Returns the `Comment` struct and an error (if any)

### Handle Get Function

The `handleGet` function handles GET requests to the server. It:

- Extracts the slug from the URL path
- Finds the comments for the slug using the `FindComments` function
- Sorts the comments by timestamp in ascending order using the `ByTsAsc` type
- Marshals the comments to JSON and writes them to the response writer

### Find Comments Function

The `FindComments` function is not shown in the provided code, but it is likely responsible for finding the comments for a given slug in the comments path.

### By Ts Asc Type

The `ByTsAsc` type is a custom type that implements the `sort.Interface` interface. It is used to sort the comments by timestamp in ascending order.

### Comment Struct

The `Comment` struct is not shown in the provided code, but it is likely a simple struct with fields for the comment data (e.g. message, name, post, timestamp, etc.).

Overall, this code provides a simple web server that handles comments for a Hugo static blog. It uses a custom `Comment` struct to represent the comment data, and provides functions for creating and saving comments, as well as finding and sorting comments by timestamp.

## comment.go

This Go code defines a package for handling comments in a blog system. Here's a breakdown of the code:

### Comment Struct

The `Comment` struct represents a single comment, with fields for the comment's parent, timestamp, sequence number, message, IP address, author, email, hash, and link. The struct is annotated with XML tags to indicate how the fields should be serialized to XML.

### String Method

The `String` method returns a string representation of the comment, including the timestamp and author.

### ByTsAsc Type

The `ByTsAsc` type is a custom type that implements the `sort.Interface` interface. It is used to sort comments by timestamp in ascending order.

### NewComment Function

The `NewComment` function creates a new `Comment` struct from a file name and path. It uses a regular expression to extract the timestamp and sequence number from the file name, and then unmarshals the comment data from the file using XML.

### tmpName and fileName Methods

The `tmpName` and `fileName` methods return the temporary and final file names for a comment, respectively.

### Save Method

The `Save` method saves a comment to a file. It marshals the comment data to XML, creates a temporary file, writes the data to the file, and then renames the file to its final name.

### FindComments Function

The `FindComments` function finds all comments for a given slug in a directory. It uses a regular expression to match the file names, and then creates a new `Comment` struct for each matching file.

Overall, this code provides a simple way to handle comments in a blog system, including creating, saving, and finding comments. It uses XML to serialize the comment data, and provides a custom sorting type to sort comments by timestamp.

Here are some potential improvements to the code:

- Error handling: The code could benefit from more robust error handling, particularly in the `Save` method.
- Validation: The code could validate the comment data before saving it to ensure that it is valid.
- Security: The code could use a more secure way to generate the temporary file name, such as using a cryptographically secure random number generator.
- Performance: The code could be optimized for performance, particularly in the `FindComments` function, which could use a more efficient way to find the matching files.

## avatary.js

This is a JavaScript code for generating avatars using initials of a name. The code defines a module called `avatary` which has a `get` function for generating avatars and a `use` function for configuring the module. The `get` function takes a name as input and returns a data URI of a PNG image representing the avatar. The avatar is generated by drawing initials on a canvas with a background color determined by the name's hash code. The `use` function allows configuring various aspects of the avatar such as font family, border size, and background shade.

The code also defines a `roundRect` method for drawing rounded rectangles on a canvas. This method is used by the `get` function to draw the background of the avatar.

Overall, this is a well-written and documented code that follows good coding practices. The code is organized into logical sections, and the functions are well-named and easy to understand. The comments provide clear explanations of what each part of the code does. The code also includes error handling for invalid input.

One minor improvement I would suggest is to add type annotations to the function parameters and return values. This would make the code more explicit and easier to understand for other developers. For example, the `get` function could be defined as follows:

```javascript
function get(name: string): string {
  // function body
}
```

Similarly, the `use` function could be defined as follows:

```javascript
function use(settings: { [key: string]: any }): void {
  // function body
}
```

Adding type annotations would make the code more robust and easier to maintain.
