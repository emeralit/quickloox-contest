---
title: "Tutti al bar, c'è la tivù"
date: 2010-10-19
draft: false
toc: false
tags: [Galaxy Tab, iPad, Macworld, Samsung]
---

Finalmente sarebbe uscito un vero concorrente di iPad: Galaxy Tab di Samsung. Schermo 7”, un po’ più piccolo, ma siamo lì.

Siamo lì per niente. Siccome è un apparecchio più rettangolare, la diagonale inganna: un Galaxy Tab ha uno schermo che fisicamente è meno della metà di iPad, come ho già spiegato su Macworld. La differenza tra tenersi in mano e stare in una mano.

Quindi serve ad altro. Non è un concorrente, ma un oggetto ancora diverso, ancora con altri usi di preferenza.

Per tutti noi che arriviamo dalle epoche in cui si faceva tutto con il computer e ci voleva un computer per fare tutto, la situazione ha un effetto straniante e ricorda evoluzioni parallele in altri campi.

Tanti anni fa la gente si radunava nei bar per vedere la televisione. Passò del tempo e la gente iniziò a radunarsi a casa del vicino con la televisione. Poi fu il turno della famiglia a radunarsi davanti alla televisione di casa. Oggi molte case hanno più di un televisore e certe famiglie hanno un televisore a testa.

Sta terminando l’era in cui un computer tuttofare faceva tutto. Inizia quella del computer per montare il video, quello per leggere la sera, quello per le riunioni in ufficio, quello da portare a casa nel weekend, quello con su le presentazioni eccetera. Decisioni più articolate per i reparti tecnici, esigenze più complesse per la forza lavoro.

E una piccola provocazione: se iPad è stato presentato come rivale dei netbook, Galaxy Tab dove si posiziona? Forse si è parlato troppo presto o troppo genericamente.

