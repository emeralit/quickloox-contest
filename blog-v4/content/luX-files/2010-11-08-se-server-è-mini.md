---
title: "Se server è mini"
date: 2010-11-08T20:04:20+02:00
draft: false
toc: false
tags: [Apple, Mac mini, Xserve]
---

A contorno della notizia dell’uscita di produzione dei server aziendali Xserve di Apple, aggiungo che di previsioni non se ne potevano fare, però Steve Jobs ha fornito un indirizzo strategico preciso quando, salendo sul palco a inizio 2010 per presentare iPad, ha qualificato Apple come una mobile company.

Naturale evoluzione allora della strategia aziendale oppure scarso successo della linea? Ambedue le risposte sarebbero di poco interesse e quindi lancio una provocazione. Se fosse una ennesima anticipazione dell’andamento del mercato e, da qui a qualche mese, si affermasse un segmento di macchine da server locale, come sono i Mac mini?
