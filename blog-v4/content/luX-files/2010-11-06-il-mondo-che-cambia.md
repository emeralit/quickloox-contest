---
title: "Il mondo che cambia (e che no)"
date: 2010-11-06T19:32:01+02:00
draft: false
toc: false
tags: [Apple, Microsoft, Sony]
---

Ieri si leggeva che magari Sony avrebbe potuto comprare Apple. Oggi si legge che Apple potrebbe comprare Sony.

Ieri si diceva che i Mac costassero più dei Pc. Oggi si vede che la tavoletta di Apple costa meno delle altre.

La vera rappresentante del mondo com’era una volta è Microsoft. Negli ultimi risultati trimestrali ha macinato oltre tre miliardi di utile su Windows e circa la stessa cifra nel settore business. Sul nuovo, considerando come tale la divisione Online Services, perdita secca di 560 milioni di dollari.
