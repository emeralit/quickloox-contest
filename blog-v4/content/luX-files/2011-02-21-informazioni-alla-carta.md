---
title: "Informazioni alla carta"
date: 2011-02-21T23:45:43+02:00
draft: false
toc: false
tags: [Pocket-lint]
---

Ottimo infographic di Pocket-lint sulla distribuzione di mercato degli smartphone in Europa, con dati nazione per nazione, Italia compresa.
