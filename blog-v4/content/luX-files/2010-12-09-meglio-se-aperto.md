---
title: "Meglio se aperto"
date: 2010-12-09T11:51:39+02:00
draft: false
toc: false
tags:
  [Adobe, Flash, HTML5, Microsoft, OpenOffice, Silverlight, Silverlight 5, Word]
---

Microsoft sta intelligentemente riposizionando Silverlight 5 come strumento di sviluppo invece come concorrente di Flash di Adobe.

I cui giorni come plugin ubiquo e parte integrante di Internet sono contati, checché affermino in Adobe.

Ad ambedue, la modesta e umile esortazione a sfruttare tutto ciò che c’è di buono in queste tecnologie in termini di sviluppo e authoring e smettere di affermarle sul mercato come piattaforme di streaming o di pubblicazione di contenuti. Si sta arrivando a farlo con standard più aperti, efficienti e sicuri come Html5 e quelli, a tutti, fa più comodo usare.

Anche perché non li controlla nessuno. Dopo l’ennesima azienda che vuole tagliare i costi allo spasimo ma non si rinuncia a Word perché OpenOffice sconcerta gli impiegati (ma che gente viene assunta per svolgere lavori di responsabilità?), le tecnologie software proprietarie che creano dipendenza psicologica e incapacità di vedere oltre il proprio naso mi lasciano sempre più perplesso.
