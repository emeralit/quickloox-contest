---
title: "Dura lex sed Moore"
date: 2010-10-21T17:51:57+02:00
draft: false
toc: false
tags: ["brian krzanich", intel, moore]
---

Cnet ha pubblicato una intervista moderatamente interessante a Brian Krzanich, senior vice president and general manager for manufacturing and supply chain di Intel.

A parte la promessa di arrivare a circuiti da 13 nanometri (e anche più sottili) entro il 2013, c’è una delle poche affermazioni corrette che si trovino in giro rispetto alla legge di Moore, che non riguarda la velocità dei processori – che poi sarebbe al massimo una frequenza – bensì la densità dei transistor, tradotta di volta in volta in maggiori prestazioni, minor consumo oppure ingombro ridotto.

Si legge anche che nel fabbricare i processori di domani la pura prestazione conta fino a un certo punto rispetto all’integrazione sul silicio di controller, grafica e altri sottosistemi.

L’era dei gigahertz venduti un tanto al chilo è davvero finita.