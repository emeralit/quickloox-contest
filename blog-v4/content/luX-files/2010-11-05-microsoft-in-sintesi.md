---
title: "Microsoft in sintesi"
date: 2010-11-05T19:27:35+02:00
draft: false
toc: false
tags: [Microsoft, Ray Ozzie]
---

Ho terminato di leggere il passo d’addio di Ray Ozzie, Chief Software Architect di Microsoft, scritto prima di essere esautorato dalla sua carica.

È pieno di riflessioni importanti e di visione chiara del futuro informatico prossimo venturo. Scritto in un linguaggio criptico, tendenzialmente incomprensibile a chi sia fuori dalla cerchia, appesantito, ellittico.

Non trovo sintesi migliore dei pregi e dei difetti dell’odierna Microsoft.
