---
title: "Grande comunicazione a basso livello"
date: 2010-11-19T19:42:44+02:00
draft: false
toc: false
tags: [Aarp, Dorling Kindersley, Microsoft]
---

Quasi commovente, questo spot Microsoft su Windows 7 + Windows Live.

Ricorda vagamente questo spot di Dorling Kindersley sul futuro dell’editoria, pensato mesi or sono.

Aarp fece qualcosa del genere nel 2007.

Se il reparto creativi è a corto di idee e il budget è a secco, ecco un suggerimento vincente: se Microsoft si permette di lavorare di terza mano, a tutti è concessa la quarta. E il giochino della doppia direzione di lettura mica è così difficile.

Vuoi risparmiare denaro

Perché c’è la crisi

invece che elaborare una grande idea

Plagi la creatività degli altri

Tutta questione di intonazione nella lettura.
