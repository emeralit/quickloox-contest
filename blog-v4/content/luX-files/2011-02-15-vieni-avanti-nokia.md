---
title: "Vieni avanti Nokia"
date: 2011-02-15T07:10:57+02:00
draft: false
toc: false
tags:
  [
    Bing,
    Droid,
    Ericsson,
    Hewlett-Packard,
    Lg,
    Microsoft,
    Motorola,
    Nokia,
    Nortel,
    Palm,
    Sendo,
    Stephen Elop,
    Verizon,
    Windows Mobile,
  ]
---

Internet aiuta anche quelli con la memoria corta e permette di risalire alle passate alleanze strategiche strette da Microsoft nel settore mobile.

Nel settembre 2000 Ericsson formò una joint venture con Microsoft attorno a Windows Mobile. I profitti iniziarono a diminuire e venne adottato Android.

A febbraio 2001 fu la volta di Sendo, che poi venne figurativamente alle mani con Microsoft per questioni di proprietà intellettuale e dichiarò bancarotta nel 2005.

Settembre 2003 è la data dell’accordo con Motorola, che lanciò una serie di apparecchi Windows Mobile fino al Q, Blackberry killer. Profitti ai minimi e fuga verso Android. Oggi Motorola si regge sulla vendita dei Droid, il cui nome dice tutto.

Settembre 2005, è ora per Palm di sfornare apparecchi Windows Mobile ed esprimere pubblicamente dubbi sulle capacità di Apple di entrare nel mobile con successo. Oggi è una divisione di Hewlett-Packard.

Successivamente venne l’ora di Nortel, luglio 2006. Due anni dopo, la bancarotta.

A gennaio 2009 arrivò l’accordo con Verizon, primo operatore mobile americano. I risultati? Bing motore di ricerca di serie su qualche apparecchio. Poi ha puntato tutto su Droid.

Lg strinse l’accordo nel febbraio 2009. Dopo qualche apparecchio Windows Mobile, avere perso anni di presenza sul mercato e tutta la profittabilità, l’azienda è passata ad Android.

Anche Nokia ha stretto un accordo con Microsoft. No, non l’attuale; in agosto 2009 Stephen Elop, president della divisione business di Microsoft, annunciò il porting di Microsoft Office su Symbian. Oggi Elop è amministratore delegato di Nokia e ha appena annunciato il declassamento sostanziale di Symbian.

In bocca al lupo.
