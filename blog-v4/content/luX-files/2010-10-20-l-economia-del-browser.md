---
title: "L'economia del browser"
date: 2010-10-20T16:32:12+02:00
draft: false
toc: false
tags: [Simplenote]
---

È una giornata di lavoro solo marginalmente particolare.

Mi rendo conto di avere aperto sul desktop un editor di testo orientato alla compatibilità con Html e un foglio di calcolo.

Dopo di che, in una finestra piuttosto ampia del browser, sono aperti posta elettronica, chat, una bozza di progetto grafico condivisa con Google Drawing, appunti di lavoro condivisi con Simplenote, il calendario e un sistema di gestione contenuti.

Parte significativa di tutto questo è diretta alla condivisione e alla collaborazione: Simplenote mi assicura matematicamente la presenza degli stessi appunti su iPad nel momento in cui mi recherò dal cliente; Google Drawing permette a un gruppo di lavoro di cui faccio parte di modificare collaborativamente la bozza, il sistema di gestione contenuti è quello del sito di un’azienda con la quale sto collaborando e così via.

Certo non posso sperare di creare una presentazione in Google Docs come potrei realizzarla sul desktop. D’altro canto, se dovessi implementare da zero un ufficio basato su queste logiche di collaborazione e di lavoro alla scrivania, ho la sensazione che i costi ammonterebbero a una frazione di quello che molte aziende spendono attualmente per ottenere i medesimi risultati in modo più macchinoso e magari pure insicuro.

Certo, aggiornare al ventunesimo secolo quel backoffice compatibile con il solo Internet Explorer 6 (chi ha avuto quella lungimirante idea di legare uno standard aperto, Html, a una tecnologia proprietaria?) avrebbe un costo. Oso dire che oggi costa più lasciarlo com’è.
