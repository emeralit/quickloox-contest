---
title: "Regali di Natale"
date: 2010-12-16T12:18:49+02:00
draft: false
toc: false
tags: [Ideos, Vodafone]
---

Nel fare a giornalisti e blogger gli auguri di Natale, Vodafone ha avuto occasione di rimarcare i buoni numeri di vendita che sta ottenendo la propria offerta di Ideos, smartphone Android in vendita promozionale a 99 euro.

A memoria, non c’era mai stato uno smartphone a 99 euro sotto Natale. Oggetti che vanno su Internet, mandano posta, chiedono di collegarsi a server magari anche aziendali, maneggiano applicazioni software, sono computer.

Oggi il traffico generato dagli smartphone è una minima percentuale del totale. Domani, quando 99 euro saranno la normalità e magari l’offerta promozionale sarà a 79 euro, o 59 euro? In mano a cinque o dieci volte le persone che ne hanno in mano uno adesso, che cosa sarà del sito aziendale, magari quello con l’ecommerce? Qualcuno ha elaborato una strategia di presenza mobile o si attende di essere presi di sorpresa da una marea montante?

Inizierei a pensare al Natale 2011. Anche a quello 2012. Tranquilli, checché ne dicano i Maya il mondo non finirà. Certe vecchie sicurezze, invece…
