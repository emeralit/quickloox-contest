---
title: "Provaci ancora Steve"
date: 2010-12-15T12:05:49+02:00
draft: false
toc: false
tags:
  [
    Apple,
    Bert Keely,
    Bill Gates,
    Consumer Electronics Show,
    iPad,
    Las Vegas,
    Microsoft,
    New York Times,
    Tablet PC,
  ]
---

Scrive il New York Times che in occasione del Consumer Electronics Show 2011 Microsoft presenterà una serie di tavolette destinate a fare concorrenza a iPad di Apple.

Era già successo l’anno scorso però e oggi di concorrenza ce n’è assai poca, tutta basata con sistemi operativi non Microsoft.

Il New York Times mostra anche una foto del 2000. La didascalia recita Bert Keely, software architect Microsoft, mostra un prototipo del primo Tablet Pc durante il discorso di Bill Gates al Comdex di Las Vegas del 2000.

Siamo sicuri che Microsoft abbia da dire alcunché di interessante su questo mercato?
