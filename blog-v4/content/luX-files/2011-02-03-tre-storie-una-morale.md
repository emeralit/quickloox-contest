---
title: "Tre storie, una morale"
date: 2011-02-03T12:54:26+02:00
draft: false
toc: false
tags: [Bing, Google, Harry Shum, Microsoft, Scott Prevost]
---

Harry Shum, Corporate Vice Presidente Microsoft per il motore di ricerca Bing, ha bloggato a proposito della polemica aperta da Google per cui il motore Microsoft copierebbe i risultati delle ricerche di Google. Google ha allestito finti risultati che si ottengono effettuando ricerche di parole prive di senso comune e diverse volte il risultato ottenuto cercando su Bing le stesse parole prive di senso è identico ai falsi messi in piedi da Google.

La versione di Shum è che non di copia si tratta: gli utenti che accettano volontariamente di farlo inviano a Bing informazioni anche quando usano altri motori di ricerca. Quindi è apprendimento. Diciamo a essere buoni che sia emulazione, via.

Scott Prevost, figura cardine nello sviluppo di Bing, ha lasciato Microsoft per eBay.

Nell’ultimo trimestre fiscale la divisione online di Microsoft ha perso 543 milioni di dollari. Da cinque anni il bilancio della divisione è fisso sul rosso e negli ultimi quattro trimestri la perdita totale è di 2,5 miliardi di dollari.

Credo di vedere la morale.
