---
title: "Poteri di ricerca"
date: 2011-02-04T12:57:09+02:00
draft: false
toc: false
tags: [Bing, Francesco Armando, Google]
---

L’amico Francesco Armando non è d’accordo con la mia sintesi dello scontro verbale tra Google e Microsoft su Bing e lo spiega sul suo sito.

Deve avere ragione lui perché tecnicamente ne sa molto più di me.

Ovviamente Google sostiene di avere ragione e Microsoft fa la stessa cosa.

In effetti qui nessuno ha copiato, ma il risultato è lo stesso.
