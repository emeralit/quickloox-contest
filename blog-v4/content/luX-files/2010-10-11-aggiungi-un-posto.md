---
title: "Aggiungi un posto al centralino"
date: 2010-10-11T18:11:28+02:00
draft: false
toc: false
tags:
  [
    Android,
    Apple,
    Google,
    Microsoft,
    Nintendo,
    Nokia,
    Sony,
    Steve Ballmer,
    WIndows Mobile,
    Windows Phone 7,
  ]
---

I migliori auguri per la presentazione ufficiale di Windows Phone 7.

Un interrogativo: ha senso per Microsoft ritentare di guadagnare terreno tra gli smartphone?

Le previsioni puntano unanimi a una prossima grande espansione del mercato, riassumibile in uno per volta tutti i cellulari diventeranno smartphone o giù di lì.

L’attitudine di Microsoft, tuttavia, è di essere numero uno ovunque. Raggiunto l’obiettivo con Windows e con Office, però, non è sempre andata così bene.

In ordine di tempo: sui server è dietro Linux. Tra i browser perde terreno un mese dopo l’altro e, se si dà per buona l’ultima rilevazione di settembre, una pagina su due viene vista con un browser diverso da Explorer. Nei videogiochi, al più, gioca alla pari con Sony e Nintendo. Sulla ricerca Internet, Bing ha ancora da togliere terreno a Google. Dieci anni dopo avere definito il tablet Pc, i tablet li vende Apple e la cosa è costata a Ballmer parte del suo bonus annuale.

Tra gli smartphone c’è già una Microsoft ed è Nokia, ultimamente in ribasso ma pur sempre seduta su una quota di mercato imponente. Di sistema operativo concesso in licenza ai costruttori c’è già Android, dotato di inerzia formidabile e gratis per chi lo voglia; l’idea di guadagnare dalle vendite di Android attraverso le cause in tribunale o gli accordi con i produttori porta cassa nel breve, ma appare poco lungimirante. Togliere mercato ad Apple si può fare, se Windows Phone 2010 non avesse le specifiche di iPhone 2007 e se il nuovo sistema non avesse attinto da Zune, il che non depone proprio a suo favore.

Con queste premesse, quanta strada può fare Windows Phone 7?

L’abbondanza di concorrenza fa bene a tutti e ben venga un nuovo concorrente agguerrito. Se Windows Phone 7 non farà il botto, però, sembra avviato a diventare un’altra fonte di perdite per l’azienda di Steve Ballmer. Che ha denaro da spendere in quantità sconosciuta a chiunque altro e se lo può permettere all’infinito, chiaro; però somiglierebbe all’ammissione di non sapere elaborare strategie di successo fuori dalla rendita miliardaria garantita di Windows e Office.
