---
title: "Settantasette milioni di assurdità"
date: 2010-11-12T20:16:16+02:00
draft: false
toc: false
tags: [Fortune, Gartner, Idc, Philip Elmer-DeWitt]
---

Gartner sostiene che nel terzo trimestre 2010 siano stati venduti 417 milioni di cellulari.

Idc sostiene che siano 340,5 milioni.

C’è una differenza di settantasette milioni di unità. Un numero strabiliante, quasi pari agli 81 milioni di smartphone venduti in totale nel trimestre. Assurdo, scrive Philip Elmer-DeWitt su Fortune, e ha ragione.

Tra i commenti spicca quello di una voce anonima, che afferma di avere lavorato per una delle due aziende. Chiama il procedimento di raccolta dei risultati sausage-making e spiega, in soldoni, come le cifre vengano talmente approssimate che il risultato finale discorda dalla realtà in percentuali indecenti e che la categoria Others viene usata come tappeto sotto il quale nascondere le cabale numeriche usate per fare quadrare il tutto.

Il mantra diventò “preserva i ritmi di crescita e al diavolo le cifre autentiche”, scrive.

Per saperlo, qualcuno prende decisioni in azienda sulla base di questi dati? Consiglio di lanciare in aria qualche dado a venti facce, quelli per giocare di ruolo. Risultati più aderenti alla realtà e risparmi da capogiro.
