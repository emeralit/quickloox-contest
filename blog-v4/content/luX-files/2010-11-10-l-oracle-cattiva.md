---
title: "L'Oracle cattiva"
date: 2010-11-10T20:08:45+02:00
draft: false
toc: false
tags: [Java, Microsoft, Office, OpenOffice, OpenSolaris, Oracle, Sun]
---

Si è mangiata Sun e ha messo in crisi (di crescita, nonostante tutto) Java e OpenOffice, il primo che era riuscito a sopportare perfino l’embrace and extend di Microsoft, il secondo che è riuscito a proporsi come alternativa credibile nientepopodimeno che a Office.

Considerato che la comunità open source nutre seri dubbi anche sulla sopravvivenza di OpenSolaris, c’è da chiedersi se un atteggiamento di autosufficienza e chiusura così a tappeto non possa finire per rivelarsi controproducente.
