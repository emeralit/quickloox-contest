---
title: "Qualcosa non va"
date: 2011-05-23T07:37:48+02:00
draft: false
toc: false
tags: []
---

l market cap (quotazione moltiplicato numero di titoli) di Microsoft è sceso al terzo posto assoluto tra le aziende di tecnologia. Dopo il sorpasso da parte di Apple, è passata davanti anche Ibm.

Si noti che questa Ibm non ha niente a che vedere con quella degli anni novanta, che vendeva personal computer, per fare l’esempio più eclatante. International Business Machines ha riveduto il proprio business model è si è trasformata. I numeri dicono che ci è riuscita piuttosto bene e ha saputo ricollocarsi con efficacia in un mercato in grande cambiamento, crescendo e generando profitto.

Adesso ascoltiamo un brano dell’intervento di Marc Benioff, amministratore delegato di Salesforce, a commento degli ultimi (positivi) risultati finanziari:

La nostra ammiraglia, Sales Cloud, ha continuato nel trimestre a stritolare la concorrenza. La disperata strategia Microsoft di prezzi stracciati a fronte di prodotti indifferenziati e altamente proprietari ha avuto sostanzialmente lo stesso impatto sul nostro business che i tablet Windows e Zune hanno avuto contro iPad e iPad. Noi chiamiamo la strategia Microsoft strategia Zune.

È il concetto secondo il quale loro possono proporre una offerta proprietaria e indifferenziata a un prezzo inferiore e in qualche modo impattare su un prodotto altamente differenziato e di alto valore, amato dai clienti. Con questa strategia Zune Microsoft non ha cambiato i nostri tassi di conversione né influenzato i nostri prezzi di vendita.

I clienti desiderano prodotti visionari che gli portino un vantaggio competitivo, non i prodotti me-too stile Zune che si confinano dentro queste piattaforme vecchie, proprietarie, desktop-driven che stanno andando a morire.

Microsoft sta crescendo e generando profitti. Ma arrivano tutti dal business di Windows e dal business di Office. Tolto qualche discreto risultato con i videogiochi, Microsoft non conquista mercati significativi da quasi vent’anni. Nel frattempo, per fare un esempio da poco, una Ibm ha saputo reinventarsi e ottenere risultati eccellenti.

Forse qualcosa non va.
