---
title: "Pensieri di marketing digitale"
date: 2010-12-10T11:54:12+02:00
draft: false
toc: false
tags: [marketing, Seth Godin]
---

Rubati a Seth Godin:

I media digitali si allargano. Non è come con la carta, possono divenire qualcosa di più grande.

Nel cercare di accrescere i profitti, i marketer digitali commettono quasi sempre lo stesso errore. Continuano ad affastellare sempre più cose, comunicazione e offerta, perché, via, è gratis.

Un link in più, un banner in più, un’offerta in più sulla pagina di Groupon [per ora solo americano].

Le scienze economiche ci dicono che la cosa giusta da fare è produrre fino a quando l’ultimo oggetto prodotto non viene venduto a un costo marginale. In altre parole, continuare ad aggiungere fino a che smette di funzionare.

Il comportamento umano ci dice che questo è un effetto più permanente di quanto sembri. una volta sovraccaricato l’utente, lo abbiamo addestrato a non prestare più attenzione. Più affollamento non è gratis. È uno spostamento permanente, una desensibilizzazione a tutta l’informazione, non solo all’ultimo bit.

E tornare indietro è difficile.

Di più non è sempre meglio. Non lo è quasi mai.
