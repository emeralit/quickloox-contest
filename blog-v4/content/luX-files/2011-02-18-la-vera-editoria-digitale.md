---
title: "La vera editoria digitale"
date: 2011-02-18T20:28:47+02:00
draft: false
toc: false
tags: [App Store, Lendle]
---

Non l’ha ancora vista nessuno. Però sta scaldando i muscoli. I primi segnali autentici non sono i boatos di Amazon sulla vendita degli ebook. Invece è nato Lendle, sistema di prestito di libri elettronici Kindle. E Apple ha definito il proprio sistema di abbonamenti a periodici attraverso App Store. Controverso solo per quanti non conoscano il modello di business degli editori tradizionali e sappiano quanto incidono veramente distribuzione e pubblicità. Mentre Google replica.
