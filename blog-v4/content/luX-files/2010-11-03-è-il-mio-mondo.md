---
title: "È il mio mondo che cambia…"
date: 2010-11-03T19:18:55+02:00
draft: false
toc: false
tags: [Access]
---

(…nella forma e nel colore…)

Oggi in riunione sono emersi quattro problemi diversi sollevati da tre intervenuti diversi, tutti collegati al fatto di avere scelto Access come database aziendale.

Che va benissimo, fino a che non emergano esigenze di multipiattaforma.
