---
title: 'Chi sa, parli'
date: 2011-02-07T10:59:27.000Z
draft: false
toc: false
tags:
  - FacebookTal Golesworthy
  - Marfan
  - sindrome di Marfan
---
Rileggendo la storia dell’ingegnere malato di sindrome di Marfan, che ha progettato da sé il congegno in grado di evitargli una pericolosa procedura chirurgica e la cui invenzione ha aiutato in sei anni altre venti persone con le medesime condizioni, ho pensato provocatoriamente alle aziende, ai social network, alle nuove possibilità di informazione offerte da Internet e di azione che consentono tecnologie in rapida diffusione come la stampa 3D.

Senza Cad, senza la possibilità di analizzare digitalmente le proprie risonanze magnetiche, senza stampa 3D, Tal Golesworthy sarebbe stato obbligato per il resto della vita alla terapia anticoagulante.

Ma ora pensiamo ai motori di ricerca, che consentono a un paziente di scoprire cose che anche il dottore più aggiornato magari non ha avuto il tempo di leggere. Pensiamo alle tecnologie di comunicazione a distanza (cinquant’anni fa come si sarebbe esposta una nuova idea a un chirurgo? Da parte di un ingegnere?). Pensiamo ai social network, dove reti di pazienti con una condizione in comune possono talvolta scoprire quello che i medici curanti non hanno avuto le condizioni di appurare.

Fattori tecnologici e umani insieme concorrono a offrire opportunità ieri sconosciute e soluzioni che non sempre arrivano dal lato più ovvio.

Proviamo a vedere ingenuamente un’azienda come un grande organismo, con grandi qualità e qualche problemino. Se immaginiamo le sue persone un po’ nello stile di Woody Allen in Tutto quello che avete sempre voluto sapere sul sesso e non avete mai osato chiedere, una buona e coraggiosa strategia di social network interno ed esterno potrebbe portare a soluzioni nuove per problemi che magari non c’è ancora stato modo di affrontare degnamente.

Che c’è da perdere? Invece che proibire Facebook per timore che i dipendenti passino metà del tempo a cazzeggiare, mettiamo a budget premi consistenti per chi metta a punto soluzioni che valgano, in risparmi o utili aggiuntivi, almeno quanto il premio. C’è solo da guadagnarci, invece.

