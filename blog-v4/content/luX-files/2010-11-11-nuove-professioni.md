---
title: "Nuove professioni"
date: 2010-11-11T20:12:30+02:00
draft: false
toc: false
tags:
  [
    Delicious,
    Facebook,
    FriendFeed,
    LinkedIn,
    Posterous,
    Tumblr,
    Twitter,
    YouTube,
  ]
---

Le aziende hanno da che mondo è mondo un reparto marcom>, marketing & communication.

E da che mondo è mondo c’è sempre stato dentro solo il marketing. La comunicazione non è mai pervenuta, legata all’ufficio stampa o delegata all’agenzia.

Oggi ci sono aziende alle prese con il sito web, l’assistenza via chat, l’account Twitter ufficiale e quelli dei dipendenti e dirigenti, YouTube e similari, LinkedIn se non capita di doversi mettere su Facebook, e Tumblr, e FriendFeed, e Delicious, e Posterous, e gli aggregatori, e i blog interni, e quello esterno…

Non so chi comincerà a scrivere curriculum centrati su questo aspetto, ma mi attendo di vedere entro breve biglietti da visita stampati con il titolo Net Communication Manager.
