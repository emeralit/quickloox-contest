---
title: "L'importanza della biodiversità"
date: 2011-02-17T23:34:00+02:00
draft: false
toc: false
tags: [MeeGo, Microsoft, Nokia, Qt, Symbian, Yahoo]
---

In un’epoca dove sempre più si cita l’ecosistema come ingrediente fondamentale del successo si capisce come l’accordo tra Nokia e Microsoft risulti scioccante.

Indubitabilmente l’accordo è molto vantaggioso per Microsoft, il cui Windows Phone 7 faceva numeri deludenti e da qui a un anno farà numeri molto superiori.

Nokia è la nuova Yahoo!, mero strumento che consente a Microsoft di guadagnare quote di mercato, vuoto di qualunque spinta innovativa. Lo hardware? Non sono più gli anni novanta e almeno cinque aziende da questo punto di vista sono più vitali e veloci nel time-to-market.

L’azienda finlandese si affanna a spiegare che Symbian non muore, che Qt continua, manca solo l’annuncio del pieno supporto a MeeGo – prima vittima di tutta la situazione – e si sono viste tutte. Come potrebbe essere diverso d’altronde, se il consenso degli analisti punta alla comparsa dei primi modelli Nokia Windows Phone 7 per la fine del 2011 se non oltre?

La verità, triste, è che quanto realizzato finora da Nokia in termini di ecosistema era di una pochezza evidente e che il lavoro in corso all’interno dell’azienda continuava a non essere all’altezza. Ovviamente Nokia non muore: resta una delle prime aziende al mondo per numeri e il solo accordo con Microsoft le ha fruttato miliardi che altrimenti avrebbe visto con molta più fatica. È però diventata un altro satellite di Microsoft, senza altra ragione di esistere che il ruolo di cinghia di trasmissione.

Esce vincente da tutto questo solo Microsoft, che conferma di avere escogitato un sistema di espansione assai più conveniente di costose macroacquisizioni. Che vincano anche i destinatari dell’offerta è tutto da vedere.
