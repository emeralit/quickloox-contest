---
title: "Chi si accorda con chi"
date: 2010-10-18T17:55:08+02:00
draft: false
toc: false
tags: [Aol, Bing, Google, Microsoft, Yahoo]
---

Confesso umilmente di avere difficoltà a valutare adeguatamente i significati dell’accordo tra Microsoft e Yahoo.

La realtà non mi aiuta. Se dovessi tentare di spiegarlo oggi, sarebbe qualcosa tipo Yahoo si condanna all’anonimato tecnologico diventando una succursale di Bing.

Microsoft si mangia un morso per volta la quota di Yahoo nel mercato della ricerca e, dopo avere promesso a Yahoo la condivisione dei proventi, lavora pazientemente per arrivare a prenderseli tutti.

Il tutto per fare concorrenza a Google, che neanche se ne accorge.

Se si aggiunge il gossip riguardante il possibile acquisto di Yahoo, venti miliardi di capitalizzazione, da parte di Aol, due miliardi e mezzo di capitalizzazione, sostanzialmente l’acquisto di qualcosa il cui principale generatore di reddito lavora per Microsoft, mi vengono le vertigini.

Sarò grato a chiunque vorrà illuminarmi.