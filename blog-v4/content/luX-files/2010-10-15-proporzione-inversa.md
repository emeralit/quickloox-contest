---
title: "Proporzione Inversa"
date: 2010-10-15T17:58:22+02:00
draft: false
toc: false
tags: [Apple, John Sculley, Pepsi Co.]
---

Guardo la pagina personale di John Sculley, amministratore delegato di Pepsi Co. e di Apple, e risalgo a quella di svariati sottodirigenti che ho incontrato, gente che non lascerà ricordo neanche nel consiglio di quartiere e che ha curriculum divisi in capitoli da quanto sono lunghi.