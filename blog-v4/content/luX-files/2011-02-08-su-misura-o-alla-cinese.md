---
title: "Su misura o alla cinese?"
date: 2011-02-08T13:02:00+02:00
draft: false
toc: false
tags: [IBM]
---

Anche Ibm si prepara a fornire strumenti di produttività online.

La domanda è capziosa e in parte si risponde da sola, visto che il codice strutturale è in gran parte lo stesso, ma esiste la questione dell’interfaccia: vincerà il modello delle app, estendendosi eventualmente al desktop, o invece il browser come chiunque avrebbe scommesso a mani basse solo un paio di annetti or sono?
