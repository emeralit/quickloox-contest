---
title: "Mission Impossible"
date: 2011-03-21T07:30:11+02:00
draft: false
toc: false
tags: []
---

Quiz: quale azienda deriva il 96 percento del proprio fatturato dalla pubblicità, dispone di una piattaforma video attualmente in negoziati con la Nba del basket professionistico americano, ha a libro paga varie celebrità e dispone di studi cinematografici propri e sta predisponendo un servizio di abbonamento a media di ogni tipo?

La domanda se l’è posta il New York Times.

E dà abbastanza da pensare in termini di percezione di un’azienda, di mission dell’azienda stessa e anche di filoni di fatturato.
