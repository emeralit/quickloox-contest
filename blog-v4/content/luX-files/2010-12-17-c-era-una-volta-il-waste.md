---
title: "C'era una volta il waste"
date: 2010-12-17T12:22:02+02:00
draft: false
toc: false
tags: [Del.icio.us, Yahoo]
---

Yahoo! si prepara a licenziamenti pari al quattro percento del personale totale e ha chiuso o consolidato vari servizi tra i quali del.icio.us.

Triste tramonto di un’azienda che pare abbia intrapreso tutte le strade tranne che quella dell’innovazione e del coraggio. Eppure era nata dall’entusiasmo di uno studente; eppure per lungo tempo era il punto di arrivo sognato da tutti quanti accendevano una startup. Ora altro genio e altra R&D finiscono nel bidone del secco, come era già accaduto mesi fa con l’abbandono della tecnologia di ricerca che pure è stata a lungo uno dei vanti dell’azienda..

L’epopea del web sta lasciando il posto a un panorama dove i soldi contano (troppo) più delle idee.
