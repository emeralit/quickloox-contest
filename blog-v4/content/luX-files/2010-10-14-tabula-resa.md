---
title: "Tabula resa"
date: 2010-10-14T18:00:53+02:00
draft: false
toc: false
tags: [Apple, BlackBerry, Greg Sullivan, iOS, iPad, iPhone, Windows 7, Windows Phone 7]
---

Riuscirà Microsoft con Windows Phone 7 a colmare gli anni di ritardo che la separano da Android, BlackBerry e iOS?

La sfida sarà interessante. Lo sfidante è un colosso, la partenza è stata con un piede insospettabilmente giusto, il primo spot pubblicitario coglie nel segno dopo tanta comunicazione autolesionista degli ultimi anni. D’altronde lo scarto è molto ampio; prevedere l’esito oggi è davvero impossibile.

La notizia laterale è tuttavia che Greg Sullivan, senior product manager di Microsoft, ha confermato l’intenzione di insistere su Windows 7 per le tavolette.

Tradotto: mentre, per rimediare al fallimento di Windows Mobile, Microsoft ha scelto con saggezza di ridisegnare da capo il sistema operativo apposta per gli smartphone, per i tablet non lo farà.

Si noti che Apple ha sviluppato apposta iOS, Hewlett-Packard ha acquistato Palm per avere WebOS apposta, Google sta sviluppando apposta Android (e ha Chrome OS), Research in Motion utilizza apposta Qnx.

Tanti apposta suggeriscono che Microsoft, puntando su Windows 7 per i tablet, ha scelto di ignorare dieci anni di fin de non recevoir da parte del mercato, dal paleolitico Tablet Pc al fiasco di Origami.

Si sappia che, sui tablet, Microsoft ha scelto l’irrilevanza.