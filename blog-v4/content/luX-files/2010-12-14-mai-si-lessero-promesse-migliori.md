---
title: "Mai si lessero promesse migliori"
date: 2010-12-14T12:02:19+02:00
draft: false
toc: false
tags:
  [
    Ces,
    Cnet,
    Consumer Electronics Show,
    Hewlett-Packard,
    HP,
    Las Vegas,
    Slate,
    Steve Ballmer,
  ]
---

A gennaio 2010 Steve Ballmer presentò al Consumer Electronics Show di Las Vegas una serie di tablet. A parte lo Slate di Hewlett-Packard, comparso otto mesi dopo in – si vocifera – non più di diecimila esemplari venduti solo a clienti business, degli altri non c’è traccia, o sfuggono ai radar.

Sempre al Consumer Electronics Show 2010 sono stati presentati vari ereader, della cui sorte rende conto Cnet.

Tra morti, feriti, dispersi, prezzi tagliati a metà e meno della metà, si capisce che gli annunci non contano più nulla. Esistono solo i prodotti che escono davvero e sono realmente supportati.
