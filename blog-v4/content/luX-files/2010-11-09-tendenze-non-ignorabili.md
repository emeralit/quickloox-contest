---
title: "Tendenze non ignorabili"
date: 2010-11-09
draft: false
toc: false
tags: [App]
---

Oggi mattinata passata a spiegare a una azienda come creare app.

Domani, mattinata passata a spiegare a una azienda diversa dalla prima come creare una app.

Le tavolette stanno diventando rapidamente non ignorabili e le aziende che non intendono accorgersene si vedranno sfilare sotto gli occhi un treno importante. E, azzardo, pure qualche commessa.
