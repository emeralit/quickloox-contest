---
title: "O Windows o tablet"
date: 2010-10-07T18:24:41+02:00
draft: false
toc: false
tags:
  [
    Apple,
    Courier,
    Dell Streak,
    Galaxy Tab,
    iPad,
    Microsoft,
    Samsung,
    Steve Ballmer,
    Tablet PC,
  ]
---

Sul blog Ping che tengo da anni per Macworld Italia ho linkato una vecchia copertina di Byte, rivista di riferimento per l’informatica personale del secolo scorso, datata 1991, raffigurante quella che indiscutibilmente è una tavoletta ante litteram.

Steve Ballmer, amministratore delegato Microsoft, ha dichiarato che per Natale saranno in vendita tavolette con Windows 7.

È lo stesso Steve Ballmer che ha dimostrato lo scorso gennaio varie tavolette. Non se ne è vista una.

Anche Apple ha presentato una tavoletta a gennaio. Ne ha vendute circa otto milioni al momento e con certezza supererà i dieci milioni di unità da qui a fine anno.

Da aprile, iPad non ha praticamente avuto concorrenza degna di questo nome. Inizia ad arrivare ora, grazie a Samsung (Galaxy Tab) e Dell (Streak). Entrambi usano il sistema operativo Android.

Il PlayBook di Research In Motion (sistema operativo QNX) è programmato per Early 2011, promesse e intanto gli iPad vanno a milioni.

Stante che Microsoft ha varato il concetto di Tablet Pc nel 2001 e stanno per essere passati dieci anni caratterizzati da vendite risibili, che lo scorso novembre ha pubblicizzato il concept di Courier per poi annunciare che non se ne sarebbe fatto niente, che lo stesso Ballmer si è visto decurtare parte del suo bonus dirigenziale anche per i problemi di Microsoft nel settore, è chiaro fin d’ora che il settore tablet vedrà Windows in una posizione marginale, quando non irrilevante. Chi sta pensando a tavolette aziendali, guardi a iPad o ad Android come minimo fino a tutto il 2011.
