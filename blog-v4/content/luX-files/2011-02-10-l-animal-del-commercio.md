---
title: "L'animal del commercio"
date: 2011-02-10T07:01:19+02:00
draft: false
toc: false
tags: [Microsoft, sales guy, Steve Ballmer, Steve Jobs]
---

Fanno impressione le ultime settimane di Microsoft, trascorse a macinare utili come nessun’altra azienda può peremttersi – la sfida a distanza con Apple è stata vinta, niente sorpasso – e a tritare senza pietà dirigenti di altissimo livello.

Secondo It World Steve Ballmer starebbe ora pensando di piazzare gente più tecnica e meno commerciale ai posti di comando per salvarsi il paraurti. Sarebbero in arrivo altri avvicendamenti di rilievo.

Steve Jobs aveva parlato nel 2004 dei periodi turbolenti che aspettano un’azienda nel momento in cui inizia ad affidarsi al sales guy.

Ballmer è il più sales guy, capace di esprimersi davanti a una platea di tecnici come certi organizzatori di schemi di vendita piramidale.

Le conclusioni è davvero troppo presto per tirarle. La situazione tuttavia resta interessante.
