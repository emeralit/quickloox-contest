---
title: "Non c'è limite al plagio"
date: 2011-04-01T07:32:56+02:00
draft: false
toc: false
tags: []
---

E così, dopo Microsoft, anche Sony apre il suo primo negozio monomarca a Los Angeles, con ampi spazi illuminati, aiuto tecnico dietro il bancone, unità dimostrative pienamente accessibili.

Apple, che sarebbe nel business della tecnologia di consumo, insegna alle multinazionali come allestire negozi. Mi chiedo quanto sia avanti sulla tecnologia di consumo, soprattutto pensando ai tablet, un anno dal primo iPad e neanche l’ombra di un concorrente degno con solidi numeri di vendita.
