---
title: "Loghi e cataloghi"
date: 2010-10-13T18:04:02+02:00
draft: false
toc: false
tags: [Andrea Jung, Apple, Avon, Gap, J.Crew, Millard Drexler, Sourcesense, Steve Jobs]
---

La catena di vendita al dettaglio di abbigliamento casual Gap ha cambiato il proprio logo, dopo vent’anni.

La scelta è stata giustificata dalla president Marka Hansen sul seguitissimo Huffington Post, con frasi in aziendalese stretto come abbiamo scelto questo design perché è più attuale e contemporaneo e onora il nostro passato ma contemporaneamente lo porta avanti.

L’accoglienza generale su Internet è stata pessima e sono nati siti come Crap Logo Yourself, dove basta scrivere una parola per vederla trattata alla stregua del nuovo logo (crap sta per schifezza).

C’è che si è spinto a pensare che Gap abbia disegnato intenzionalmente un pessimo logo per catturare attenzione. L’azienda ha smentito per annunciare – non è dato sapere quanto a posteriori – che il nuovo logo fa parte di un progetto di crowdsourcing per arrivare a una scelta guidata a ispirata dalla comunità su Internet.

Il crowdsourcing è oggetto di controversie ma è una scelta concepibile in ambito aziendale: Sourcesense, che sviluppa soluzioni open source in ambito altamente professionale, ha scelto il proprio logo indicendo un concorso pubblico su Internet.

Ma non è questo il punto. La vera lezione è che il pubblico è attento alla comunicazione aziendale, anche quella che si penserebbe poco importante, compresa la comunicazione grafica e di design.

Chi è abituato a pensare al design della propria comunicazione come un mero sistema per fare lavorare in qualche modo i grafici che non si possono ancora licenziare, producendo orrori di ogni tipo da comunicati stampa di una sciatteria indecente a cataloghi prodotti per tagliare i costi (e le vendite, da tanto son brutti) fino a siti inguardabili, forse avrebbe convenienza a ripensarci. Anche perché, se non è positivo che il pubblico reagisca negativamente a un cambiamento, è positivo che reagisca; se restasse indifferente sarebbe cosa ben peggiore.

Per quanti credono che siano questioni limitate a chi vende abbigliamento, Millard Drexler, per anni amministratore delegato di Gap e oggi di altra azienda omologa, J.Crew, siede da anni nel consiglio di amministrazione di Apple. Dove siede pure Andrea Jung, amministratore delegato di Avon. E Steve Jobs, Ceo di Apple, ha fatto parte per anni del consiglio di amministrazione di Gap.

La quale ha ripreso a usare il vecchio logo.