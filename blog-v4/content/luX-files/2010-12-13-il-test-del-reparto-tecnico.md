---
title: "Il Test Del Reparto Tecnico"
date: 2010-12-13T12:00:02+02:00
draft: false
toc: false
tags: [Json, Xml]
---

Trovare qualcuno in azienda che sappia riassumere e spiegare in italiano coerente e chiaro a un qualunque dipendente i contenuti di questo post sulla dicotomia tra Xml e Json.

Sembra il delirio di un programmatore appena eliminato a un torneo di scacchi online e invece si tratta di questioni cruciali per la produzione, comunicazione e documentazione aziendale per il prossimo quinquiennio.
