---
title: "Chi ha incastrato html5"
date: 2010-10-08T18:21:10+02:00
draft: false
toc: false
tags:
  [
    Adobe,
    Apple,
    Flash,
    HTML5,
    Microsoft,
    Silverlight,
    SIP,
    W3C,
    World Wide Web Consortium,
  ]
---

Al World Wide Web Consortium, il W3C, non fa piacere che ci sia tanto fervore nell’adozione di Html5 in un momento in cui le specifiche sono lontane dall’essere complete.

La posta in gioco è rilevante, tra gli standard aperti e le soluzioni proprietarie come Flash e Silverlight, tra il principio di un linguaggio comune che elabori anche media complessi e i plugin ad hoc della prima azienda che cerca di imporre uno standard de facto.

Considerato il peso che giganti come Microsoft e Apple hanno gettato sulla contesa (il sito Apple in particolare è un trionfo di Html5) e considerato che Flash 10.1, a dispetto delle promesse di Adobe, funziona veramente poco su qualunque piattaforma diversa da un personal computer propriamente detto, ci si aspetterebbe una risposta sollecita a una domanda tecnologica possente: è chiaro che privati, aziende, istituzioni vogliono modi efficaci per trasmettere comunicazione evoluta.

E al W3C parlano con nonchalance di metterci quattro o cinque anni – se va bene – per arrivare a uno standard.

Viene in mente il calcio professionistico italiano, milioni di euro che cambiano mani sotto la supervisione in campo di arbitri pagatissimi, ma irrimediabilmente dilettanti.

Ma l’analogia forse più forte e sgradevole è dell’Italia anni ottanta, quando la Sip voleva l’omologazione dei primi modem e pretendeva che gli acquirenti di un modem pagassero la tassa di concessione di utenza telegrafica.

Un mondo in esplosione gestito da burocrati arretrati e incapaci. E infatti si diffusero come i funghi i modem non omologati (l’omologazione serviva solo a gonfiare il prezzo e a dilatare i tempi, per rallentare la diffusione), che la gente comprava non certo per evadere, quanto perché in tutta evidenza la legislazione era stupida e inadeguata.

È la stessa cosa ora. Nei corsi di formazione appaiono già insegnanti di web authoring che spiegano Html5 ai designer di domani. Quelli di oggi hanno già imparato l’arte del fallback, usare le funzioni migliori a disposizione e fornire un piano B ai browser che non sono ancora in grado di usarle. Piuttosto mi chiedo: visto che dentro W3C sono rappresentate più o meno tutte le grandi multinazionali tecnologiche, chi è che sta remando contro per riuscire a posporre il più possibile uno standard Html5 compiuto?
