---
title: "Splende il sole su una nuvola"
date: 2010-11-04T19:23:07+02:00
draft: false
toc: false
tags: [Amazon, S3]
---

Nel tanto parlare e promettere di cloud computing, qualcuno fa attivamente e si tratta di Amazon, che ha appena ridotto i prezzi del proprio servizio S3.

Stoccare un terabyte di dati per un anno costa 168 dollari, con affidabilità – sostiene Amazon – a undici nove (99,999999999%).

C’è che si affida in ambito professionale a pratiche assai più dilettantistiche e scommetto che spende anche molto di più, con meno sicurezze.
