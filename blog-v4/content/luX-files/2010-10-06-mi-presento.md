---
title: "Mi presento, io mi chiamo Lucio Braganolo"
date: 2010-10-06T18:27:59+02:00
draft: false
toc: false
tags:
  [
    Apple,
    BlackBerry,
    Elio e le Storie Tese,
    Enrico Frascari,
    Enrico Negroni,
    Gianugo Rabellino,
    Italo Vignoli,
    Mac OS X,
    Microsoft,
    Oracle,
    Saskatchewan,
    Windows,
  ]
---

ntanto mi sento onorato dal comparire in un luogo autorevole e prestigioso. Saluto Enrico Frascari, Enrico Negroni e l’amico Italo Vignoli, da tempo blogger per Computerworld; la mia prima sfida sarà ben figurare rispetto al loro livello. E ringrazio Marco Tennyson e Paolo Morati per avermi aperto questa opportunità; la mia seconda sfida sarà soddisfare le loro aspettative (spero non si accorgano che il titolo parafrasa Elio e le Storie Tese).

Tuttavia si tratta di priorità secondarie rispetto alla Sfida Zero che attende ogni blogger: dare significato e valore alle proprie pagine in una logica di servizio e rispetto dei propri lettori, oltre che di quelli della testata.

La mia estrazione relativamente all’Information Technology non è aziendale e nella mia carriera gli anni di lavoro dipendente sono stati pochi. Tuttavia la gavetta nel mondo della comunicazione professionale, un’esperienza oramai plurisecolare di editoria a tutti i livelli e su tutti i media e il mio ruolo di consulente hanno contribuito a formare l’immagine di un mondo che posso contrapporre al bombardamento quotidiano di notizie caratteristico dei tempi.

Da una parte le aziende italiane hanno la saggezza di evitare il coinvolgimento avventuristico nella tendenza di moda. Dall’altra sono caratteristicamente miopi nell’ignorare i cambiamenti utili, persino quelli che a fronte di una spesa irrisoria consentono risparmi o miglioramenti decisivi. Se diciotto ministri canadesi si dotano di tavolette per tagliare migliaia di dollari in consumo di carta, a qualcuno in azienda potrebbe suonare un campanello. Non suona e vai di fotocopie.

Trovo dirigenti illuminati che valutano ciò che è meglio. Trovo anche aspiranti pensionati che hanno Windows sulla scrivania, Oracle nei server, BlackBerry come smartphone, Mac OS X nel reparto marketing e sviluppatori impegnati su Red Hat, e biascicano di uniformizzazione come se fosse il 1995. Se c’è una cosa che ha stabilito il XXI secolo, è la vittoria della biodiversità informatica contro i pregiudizi. Semplicemente, è falso che un solo sistema operativo è meglio per tutti.

Pregiudizi, pregiudizi, che veramente sarebbe ora di spazzare via. Esiste un pregiudizio contro Apple, che tuttavia oggi avvicina Microsoft in fatturato e qualcosa vorrà pure dire. Esiste un pregiudizio contro Microsoft, che ha intrapreso politiche discutibili verso il software aperto e che però oggi assume Gianugo Rabellino, già vice president dell’Apache XML Project Management Committee, per lavorare all’interoperabilità tra piattaforme Microsoft e non Microsoft. Conosco Gianugo e la sua competenza in ambito open source. Se va in Microsoft (negli uffici di Redmond), è per fare lavoro serio e non FUD, il Fear-Uncertainty-Doubt per cui Microsoft si è distinta (per modo di dire) nel secolo scorso.

Insomma, ci sono tante cose di cui parlare e discutere. Amo essere contraddetto con argomenti fondati; amo imparare. Amo le novità e amo la concretezza. Amo questo mondo, con tutte le sue contraddizioni, e amo raccontarlo. Se qualcuno avrà piacere di leggerne e chiacchierarne insieme, ne sarò molto lieto e può darsi che faremo strada interessante insieme. Nessun bisogno di amarmi, naturalmente.

Buon viaggio a tutti.
