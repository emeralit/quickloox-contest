---
title: "Il guasto infinito"
date: 2010-10-12T18:07:05+02:00
draft: false
toc: false
tags: [Android, Apple, G2, Htc, iPhone 4, T-Mobile, YouTube]
---

Prima di Internet e magari di YouTube, l’immagine di qualità di un’azienda e dei suoi prodotti dipendeva fortemente dalla percentuale e frequenza di guasti. L’importante era che i guasti si contenessero a una frazione giudicata accettabile della produzione complessiva.

Nell’era digitale avere un supporto tecnico non basta più e il funzionamento dei prodotti diventa anche faccenda di marketing e comunicazioni esterne, perché vendere milioni di unità perfette non basta più; è sufficiente che un prodotto malfunzionante vada in mano a una persona abbastanza intraprendente da pubblicare un video su YouTube che subito la questione diventa planetaria.

Se ne è accorta quest’estate Apple quando è arrivata a organizzare un evento apposta per contenere il gossip nato attorno alla death grip, la presa della morte, su iPhone 4. E tuttora negli Apple Store entrano persone a chiedere se è vero che ci sono problemi.

Non conta più se sia vero, né che ci siano problemi oppure meno; il punto è che l’azienda di oggi deve essere preparata a fronteggiare non solo il normale guasto, ma anche la sua pubblicizzazione esasperata. Che sia un guasto su un milione, accettabilissimo, ha zero rilevanza.

Ultimissimo esempio, lo smartphone G2 fabbricato da Htc e commercializzato negli States da T-Mobile: sta facendo rumore il video di un esemplare difettato nella cerniera.

Ammesso che il filmato sia vero, è un esemplare solo, su chissà quanti. E però su Internet vale per l’intera fornitura. Pane per le riflessioni del marketing & comunicazione.
