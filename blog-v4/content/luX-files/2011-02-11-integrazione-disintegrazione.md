---
title: "Integrazione, disintegrazione"
date: 2011-02-11T07:04:18+02:00
draft: false
toc: false
tags:
  [
    Android,
    Apple,
    Bada,
    Hewlett-Packard,
    HP,
    Microsoft,
    Nokia,
    Samsung,
    Tood Bradley,
    WebOS,
  ]
---

Una delle cose migliori che possa capitare all’industria informatica dopo trent’anni di monopolio: Hewlett-Packard ha annunciato l’intenzione di portare WebOs sul personal computer. Todd Bradley, a capo del gruppo client della multinazionale, ha accennato esplicitamente alla possibilità di immettere sul mercato cento milioni e più di computer basati su WebOS. L’azienda, ha ricordato Bradley, vende due computer al secondo e ha un miliardi di clienti.

Apple ha dimostrato che il controllo di hardware e software, con la conseguente integrazione ottimizzata, permette di creare macchine di qualità superiore a prezzi accessibili per il mercato.

Se Hewlett-Packard mantenesse la promessa con coraggio e lucidità, ne guadagneremmo tutti: svuotata ulteriormente di significato la parola Wintel, ci sarebbero più concorrenza, più sicurezza, più avanzamenti tecnologici.

Di segno completamente opposto il gossip su Nokia, che avrebbe cassato il sistema operativo MeeGo – sostituto di Symbian – senza delineare una strategia alternativa. Riprogettazione di MeeGo per allinearlo al livello degli altri concorrenti, come Microsoft ha scelto di fare con Windows Phone 7 buttando alle ortiche Windows Mobile, o altro?

Se altro significasse adozione di un sistema operativo altrui, come potrebbero essere Android o lo stesso Windows Phone 7, Nokia si autodeclasserebbe di fatto a Oem qualunque come una Htc o una Samsung (che pure sta elaborando il proprio sistema, Bada) e non c’è dubbio che la sua attuale e già traballante posizione di leader si disintegrerebbe entro al massimo un triennio, se non prima.
