---
title: "Linux in provetta"
date: 2011-10-10
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Hanno ulteriormente migliorato la versione di Linux realizzata in JavaScript dentro il browser. Ora si possono perfino salvare file localmente e ritrovarli in una sessione successiva.

Se provare Linux una volta richiedeva una complicata partizione del disco e installazione, poi sono arrivati i Live Cd, poi le macchine virtuali – VirtualBox va ed è pure gratis – ma che bastasse accendere Safari o equivalente non lo si sarebbe mai pensato. Non io, perlomeno.

Il login è quello tipico dei Linux freschi di installazione: root senza password. Il kernel è la versione 3.0.4, credo una delle più recenti disponibili. E la velocità è del tutto accettabile, perfino sul sovraccarico Safari del mio MacBook Pro inizio 2009.

