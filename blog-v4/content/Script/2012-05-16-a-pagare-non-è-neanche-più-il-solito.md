---
title: "A pagare non è neanche più il solito"
date: 2012-05-16
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Al momento di effettuare il versamento dell’Iva dovuta per il primo trimestre 2012, mi sono accorto che Banca Mediolanum ha sospeso il servizio di pagamento moduli F23 e F24 (quelli usati dai professionisti per il versamento).

Scelta interessante e poco condivisibile, sospendere il servizio di pagamento proprio nel giorno della scadenza. Certo, anche colpa mia che arrivo all’ultimo momento.

Contatto il gentilissimo servizio di assistenza via chat e scopro che il servizio è sospeso, a quanto si sa finora, per tutto il mese di maggio.

Al che chiamo il numero verde (dove, prevedibilmente, hanno attivato una linea apposta) e mi spiegano che ci sono problemi con Taxtel. Per chi non lo sapesse, Taxtel è un servizio di Equitalia Nord, incaricato di lucrare sul pagamento telematico delle tasse. Farsi pagare per farti pagare le tasse penso sia un primato mondiale.

Ignoro se il problema sia di Taxtel oppure di Banca Mediolanum, o se investa tutte le banche che hanno scelto di appoggiarsi a Taxtel per il pagamento dei tributi. Se qualcuno ha notizie le ascolto molto volentieri, come giornalista e persona di Rete. Come cliente Mediolanum mi interessa anche poco: mi ritrovo un disservizio costoso e pericoloso, che non è una sospensione di un’ora del saldo, ma di un mese del pagamento tributi. E le mie spese di gestione del conto, capisco, restano invariate anche se i servizi evaporano.

Come contribuente, Taxtel non dovrebbe neanche esistere. Il mio impegno di contribuente si esaurisce pagando fino all’ultimo centesimo le tasse dovute alle scadenze date. Taxtel vuole guadagnarci un extra per sé, al pari di certe squallide figure da vicolo che si vedono nei serial polizieschi. Ok, ma usualmente nei serial qualcuno risponde non voglio problemi. Se vuoi la tua parte, non creare problemi e lascia che la gente paghi.

Da tutto questo deriva una piccola lezione di comportamento per le aziende su Internet. Occorrono la trasparenza, la chiarezza, l’immediatezza, la sincerità e il supporto. Che si vendano conti correnti o ceramiche artigianali.

Banca Mediolanum mi ha avvisato con un Sms, che non spiega niente a parte avvisare della sospensione. Rimanda al sito, dove non si trova niente in prima battuta e bisogna impegnarsi per arrivare a una pagina dove si accenna a Taxtel e vengono suggerite varie soluzioni che il cliente conosce benissimo, non sono offerte da Mediolanum e la cui lettura serve solo ad accrescere il malumore.

Invece attivi una finestra sul sito dove spieghi per filo e per segno tutto quello che succede e perché succede, che cosa stai facendo per risolvere il problema e con una stima sul tempo di soluzione, tutto aggiornato in tempo reale. Tranquilli: costa (molto) meno che mobilitare uno stormo di poveracci sul centralino.

Questa sarebbe la normalità, specie per una banca, cui non devono certo fare difetto le risorse straordinarie. E siamo invece a un livello inaccettabile.

Il livello di servizio eccellente comprenderebbe trovare in tempo tendente a zero una soluzione alternativa efficace e offrirla contestualmente ai clienti, con una formula di compensazione del disagio creato (che sia il regalo di un punto fedeltà o una lettera firmata con il sangue dall’amministratore delegato è irrilevante, conta il rapporto con il cliente). Dire ai clienti abbiamo un problema con Taxtel è come dire non sappiamo risolvere i tuoi problemi quando diventano un nostro problema. Per la banca costruita intorno a te, e qualunque altra, è grave.

Costa? Certo. Se però risolvi il problema in modo efficace, ti ritrovi con clienti fedeli. Se tagli un servizio essenziale per un mese e ti limiti ad avvisare con un Sms, forse i clienti li perdi e mi sa che costa di più. I piccoli roditori di Taxtel non hanno questi problemi, nel loro rosicchiare. Una banca sì.

Difatti, se la cosa non si concluderà in modo più degno di come è cominciata, cambierò banca.

