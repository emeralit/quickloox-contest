---
title: "In fuga dal centro"
date: 2012-05-13
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Sono rimasto colpito dall’articolo di NoodleMac dal titolo Perché Evernote potrebbe essere il futuro delle applicazioni Mac.

Applicazione gratuita con aggiornamenti automatici, contenente un mini word processor, capace di digerire grafica, immagini, video, Pdf, quasi qualunque cosa, comprese cosette tipo riconoscere il testo scritto a mano in note cartacee scandite e date in pasto al programma.

Tutto quello che mangia Evernote viene sincronizzato in rete e rimane accessibile, tramite Evernote, praticamente a qualsiasi computer e qualsiasi sistema. Un domani nulla vieta ai suoi autori di inserire funzioni di foglio di calcolo, database, disegno vettoriale, qualunque cosa.

In fin dei conti, iWork e iLife – Pages, Numbers, Keynote, iPhoto, GarageBand, iMovie – stanno seguendo una traiettoria simile: accesso da ovunque, dati sincronizzati su Internet.

Nel quadro mancano le applicazioni sofisticate, per fare le cose veramente complicate di cui hanno bisogno in pochi, e mi sono reso conto che forse sono avviate a estinzione le applicazioni, per così dire, di medio calibro. Da una parte le cose semplici, che servono a tutti e fanno le cose semplici ed essenziali, appoggiate alla Rete per avere sincronizzazione e ubiquità. Dall’altra gli strumenti superpotenti e superdifficili, per cose supercomplicate.

In fin dei conti sta succedendo lo stesso anche a me. Se devo scrivere, sono nove volte su dieci su BBEdit, che inizia a essere un programma sofisticato. Negli anni probabilmente finirò per rotolare verso opzioni complesse come LaTeX o emacs. Mentre, per quell’altra volta su dieci, effettivamente Pages è perfino troppo complicato.

Spariranno le vie di mezzo e forse ci guadagneremo tutti.

