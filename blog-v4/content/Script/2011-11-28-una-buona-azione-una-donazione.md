---
title: "Una buona azione, una donazione"
date: 2011-11-28
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Mi scrive Giuseppe:

ho trovato il consiglio di installare AppleJack in diversi tuoi articoli.

Non so se ho risolto definitivamente il problema della lentezza del PowerBook, ma il suo sviluppatore merita una buona donazione.

Conservare i tuoi articoli (per esempio Radical Unix e Radical Unix Reloaded, anno 2004) è stata una fortuna e oggi mi sono potuto dedicare alla riga di comando.

Se un giorno vorrai dedicare un post su Script alla manutenzione fai-da-te, AppleJack e il Terminale devono fare parte della cassetta degli attrezzi.

Tutto questo non serve a mostrare quanto sono bravo, semmai a mostrare quanto può portare lontano l’essere bravi come Giuseppe. Leggere e documentarsi, riconoscere il valore del software, fare un passo in più del minimo sindacale e scoprire che è vantaggioso.

Veramente nel 2004 ho scritto (non è vero: radicalmente, questo sì, adattato da un originale americano) Radical Unix? È proprio vero. Adesso che sono cambiate proprio tante cose non può fare male a nessuno se lo ripubblico, qui sotto. Il Terminale invecchia, ma lentamente. Metti che a qualcuno possa interessare, potrei anche ripubblicare Radical Unix Reloaded. Attendo commenti!

Radical Unix
Vivere senz’Aqua trasforma in mostri del Terminale.
di Rob Griffiths
[with:~] Lucio\ Bragagnolo

Mac OS X si vive splendidamente e al cento per cento senza il minimo bisogno di quell’orrore nero chiamato Terminale.

Ora che lo abbiamo scritto, ci segua chi se la sente. Entriamo negli inferi di Unix, dove gli spostati senza una vita vera e senza partner con cui uscire la sera compilano programmi open source, si collegano a computer remoti e biascicano formule arcane come grep, sed e fdisk. Sono gli abitanti dei sotterranei oscuri e misteriosi di Mac OS X, dove passano le fogne, i tubi dell’acqua, i cavi elettrici e le fibre ottiche.

Sono i Terminale-dipendenti, quelli che vedono Aqua solo per entrare in Applicazioni/Utility.

Le basi di Unix
utte le forme d’arte si articolano entro confini definiti. Il Terminale è ristrettissimo: una finestra dove si digitano comandi di testo. Non ci sono menu, icone, finestre di dialogo. Perfino il mouse è pressoché inutile. Si chiama riga di comando, command line.

La prima volta che si apre il Terminale non appare molto nella sua finestra, tranne la data e l’ora dell’ultimo login (il login equivale a iniziare una sessione di Terminale), un messaggio di benvenuto e un prompt, cioè una sequenza di caratteri che indicano la possibilità di digitare un comando. Il prompt, così com’è, si riduce a pochi elementi: per cominciare, il nome del Mac (almeno quello secondo Unix) e la parte che indica in che directory ci troviamo. È come vedere una cartella aperta nel Finder, solo che qui l’indicazione è scritta nel prompt. Niente icone, in Unix.

La primissima volta che si apre il Terminale la directory in cui ci troviamo – detta working directory, directory di lavoro – è la home, indicata per convenzione da una tilde (~, Opzione-5). Come se avessimo la cartella Inizio aperta nel Finder. Man mano che ci si sposta nel sistema l’indicazione cambia. Se all’inizio è qualcosa tipo

nomedelmac:~

può diventare

nomedelmac:/Users

oppure

nomedelmac:/System/Library/PreferencePanes/

o altro. Le barre, slash, indicano il passaggio da una directory all’altra, come aprire più cartelle nidificate. Directory è la parola Unix per dire cartella.

Il prompt di serie termina con il nome dell’utente e un carattere di conferma che sì, si può inserire un comando:

nomedelmac:~ nomedellutente$

Poi non c’è nient’altro, a parte il cursore: un rettangolo nero che si sposta man mano che digitiamo i comandi. Digito un comando, vado a capo, il comando viene eseguito, ritorna il prompt pronto ad accettare un comando nuovo: le sessioni di Terminale consistono in ripetizioni più o meno numerose di questo ciclo.

Siccome in Unix non si vedono cartelle né icone, occorrono comandi che dicano in che cartella ci si trova e che permettano di spostarsi da una cartella all’altra: rispettivamente pwd (print working directory, stampa (sullo schermo, per ora) la directory di lavoro, e cd (change directory, cambia directory). Ah, e che cosa c’è dentro una directory: ls (sta per list, elenco).

Digitando il comando pwd e andando a capo, il Terminale risponderà con una cosa come

/Users/nomedellutente

È il path, cioè il percorso da cui si arriva alla directory: in questo caso, dal disco rigido (che anticiperebbe il primo slash ma viene dato per sottinteso, che contiene la directory Users, che contiene la directory nomedellutente. Maiuscole e minuscole contano, in Unix; secondariamente, tutti i nomi delle directory sono in inglese. Quindi /Applications, anche se nel Finder vediamo la cartella Applicazioni.

Digitando ls apparirà un elenco di file e directory contenute nella directory di lavoro, tipo

Desktop Library Pictures Sites
Documents Movies Public
Faxes Music Shared

Non si direbbe ma sono in ordine alfabetico, solo per colonne. Equivale a vedere una lista di file mostrati icona per icona dentro una cartella del Finder.

Il comando cd sposta in un’altra directory:

cd /Users/nomedellutente/Music

Tanto per imparare alla svelta un altro comando Unix: cal. Mostra il calendario del mese in corso.

Prime considerazioni sui comandi Unix: sono spesso molto sintetici e sovente si scrivono un carattere per mano (c-p, r-m, l-s e così via). Nella preistoria le risorse di elaborazione e il tempo disponibile per usare i pochi computer esistenti erano limitati, così si cercava di risparmiare al massimo tempo e digitazione. Questi comandi sono i dinosauri dell’informatica. Solo che loro, invece che estinguersi, prosperano tuttora.

Tempo di risposta
Il Terminale è un argomento comune di discussione sul web. Online circolano decine di tutorial che spiegano che cosa digitare per ottenere certi risultati senza soffermarsi troppo sul perché si ottengono. Tecnicamente lo si definisce un sistema call-and-response: gli dici qualcosa, lui risponde, gli dici qualcos’altro e avanti così.

Scorciatoie di Terminale
I primi trucchi che stiamo per svelare sono talmente banali che uno si chiederà che ci stia a fare, il Terminale. Durante l’installazione di Mac OS X è prevista di serie l’installazione del cosiddetto BSD Subsystem. Se qualcuno ha volontariamente disattivato l’opzione, deselezionando la casella relativa durante una installazione Ad Hoc, qualcosa potrebbe non funzionare. Soluzione: impostare una nuova installazione Ad Hoc e installare il solo sottosistema BSD.

Trascinare il percorso
Invece che consumare polpastrelli con il comando cd per indicare cartelle e percorsi, si può trascinare sul Terminale dal Finder con il mouse l’icona della cartella o del file cui intendiamo fare riferimento. Magicamente, nel Terminale apparirà il percorso bell’è pronto.

I limiti del mouse
Nel Terminale il mouse serve a ben poco. Il cursore si sposta con i tasti freccia destro e sinistro. Tuttavia il mouse può selezionare testo nella finestra, per copiarlo e incollarlo al prompt.

Si può inoltre usare il mouse per posizionare il cursore, seppure a costo di operare un trucchetto. Apriamo il menu Terminale: Impostazioni Finestra e selezionamo la casella Clic + opzione per selezionare il cursore. Dopo di che, per l’appunto, Opzione-clic permetterà di posizionare il cursore. Utile per correggere un errore in mezzo a un comando estremamente lungo, dove usare i tasti freccia diventa inefficiente.

Tabulazioni che completano
Si sa già che si può selezionare una icona del Finder dentro la finestra in primo piano digitando i primi caratteri del suo nome. Nel Terminale è praticamente la stessa cosa. Quando si scrive un percorso, per esempio, si possono scrivere i primi due o tre caratteri e poi premere Tab; a completare il nome della directoory ci pensa il Terminale. Bastano poche avvertenze:

Maiuscolo e minuscolo fanno la differenza.
Il Terminale aggiunge automaticamente le barre inverse (backslash) che servono per scrivere caratteri speciali come lo spazio e il dollaro. Però i caratteri speciali relativi ai caratteri che digitiamo noi spettano, sì, a noi, e dobbiamo mettere i backslash manualmente (lo spazio, nel Terminale, è backslash-spazio, o “\ “).
Se il Terminale non è in grado di completare il nome emette un beep e aspetta che digitiamo più caratteri o correggiamo eventuali errori.
Se i caratteri che abbiamo digitato corrispondono a più directory, il Terminale fa beep e mostra una lista di alternative. Digitiamo ancora uno o due caratteri in più, per restringere la scelta a un nome solo, e premiamo nuovamente Tab.

Chi non conosce il passato è condannato a ripeterlo
Il Terminale ricorda gli ultimi 150 comandi che sono stati digitati. Freccia in su ripercorre tutta la storia della digitazione all’indietro, comando dopo comando. Freccia in giù risale verso il presente. Molte volte un comando che ci serve è ottenibile ricorrendo a un comando già dato in passato, dove basta modificare qualcosa.

Wildcard, o metacaratteri
Alcuni caratteri rappresentano categorie di caratteri e consentono di risparmiare grandi quantità di tempo. L’esempio classico è l’asterisco, che significa al posto di qualsiasi cosa. Se nella directory di lavoro corrente ci sono più file che finiscono con la sequenza ld (per esempio: articoli-macworld, indirizzi-macworld, newsfromtheworld), basta il comando

ls *ld

per vederli elencati tutti in un colpo solo. Letteralmente il comando è mostrami tutto quello che c’è, basta che il nome finisca in ld. Il trucco funziona anche nell’altro senso:

ls Gi*

trova Gigi, Giada, Giorgio, Gioia, Giovanni, Giuseppina, Giambattista, Gigliola e così via (ma non giornale o giaculatoria, perché maiuscole e minuscole contano). Digitare

ls *em*

individua settembre, Settembre, novembre, Novembre, dicembre e Dicembre, ma non empatia o idem. Deve esserci qualcosa sia prima che dopo i caratteri cercati.

Cambio directory rapido
Un trattino significa voglio tornare alla directory di lavoro precedente. Se eravano in ~/Documents/macworld/articoli e ci spostiamo in /Applications/giochi/Unreal, il comando cd - riporta istantaneamente a ~/Documents/macworld/articoli. Ancora cd - ed eccoci di nuovo in /Applications/giochi/Unreal. Si noti che tra cd e il trattino c’è uno spazio.

Consiglio: Se capita di lavorare in continuazione con due directory diverse, sempre quelle, invece di commutare tra l’una e l’altra conviene aprire due finestre del Terminale e dedicare ciascuna a una directory.

Del terminale e delle shell
Aprire il Terminale significa azionare automaticamente un programma Unix che si chiama tcsh nelle edizioni vecchie di Mac OS X e bash in quelle recenti: comunque, una shell. Una shell è, tecnicamente parlando, un programma che intepreta i comandi digitati, li passa al kernel (la parte pensante del sistema operativo) e mostra la risposta del kernel.
Possiamo pensare alla shell come al Finder di Unix: il programma che fa navigare tra le directory, mostra che cosa c’è in esse, lancia programmi, apre documenti e via dicendo.
Le shell hanno manuali di lunghezza inusitata. Il comando man tcsh (o man bash) mostra decine e decine di schermate di documentazione, esempi, note tecniche. Leggere la manualistica della shell è un ottimo investimento per chi vuole conoscere un po’ di Unix.
Ci sono più tipi (quasi dialetti) di shell, ognuno con piccola variazioni nelle sintassi: per esempio sh o Bourne shell, zsh e csh. Tutte queste sono comprese in Mac OS X. Ognuna ha le sue pagine man, leggendo le quali si può decidere quale si preferisce usare.
Al limite, si possono aprire più finestre Terminale ed eseguire una shell diversa in ciascuna finestra. Le finestre sono completamente indipendenti l’una dall’altra. Una prova. Digitare in ciascuna il comando cal, o uptime.

