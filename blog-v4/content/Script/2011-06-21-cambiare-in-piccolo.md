---
title: "Cambiare in piccolo"
date: 2011-06-21
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Non mi sarei mai sognato di portare all’assemblea di condominio il mio MacBook Pro 17” nel case servisse aprire un foglio di calcolo e improvvisare scenari di spesa.

Poi è uscito Numbers per iPad. Alla penultima assemblea di condominio non mi ha creato grandi problemi portare iPad, per lo stesso scopo. L’ingombro è minimo ed è anche un buon argomento di conversazione.

Ora è uscito Numbers per iPhone. All’ultima assemblea di condominio avevo solo iPhone in tasca. Lo scenario di spesa l’ho improvvisato lì senza grossi problemi. Zero ingombro, ottimo risultato.

Ci sono da mille anni, i fogli di calcolo per smartphone. Ma prima si usavano con lo stilo o addirittura con la tastiera di bordo. Oggi fanno l’effetto della clava e delle selci scheggiate. Avere Numbers su iPhone, pronto a portata di polpastrello, è un altro cambio di vita.

