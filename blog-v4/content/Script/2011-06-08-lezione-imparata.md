---
title: "Lezione imparata"
date: 2011-06-08
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Apple ha già pubblicato la versione italiana delle 250 nuove funzioni di Lion, rendendo felicemente inutile il mio lavoro di traduzione anticipata.

Posso però dire di avere installato la Developer Preview 4 di Lion come disco di lavoro. Per lo stato attuale del software è ai limiti dell’azzardo. Ma è fattibile.

Nei limiti dell’accordo di riservatezza, sono a disposizione per informazioni.

