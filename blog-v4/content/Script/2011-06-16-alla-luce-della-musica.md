---
title: "Alla luce della musica"
date: 2011-06-16
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Avevo già accennato in passato alle operazioni sinestesiche di Alessio Nanni, pianista toscano giovane e intraprendente oltre che bravo e Mac user di quelli certificati.

Sinestesia è quando uno stimolo sensoriale solletica anche sensi che a rigore non sarebbero interessati. Respiri il profumo di un frutto e ne avverti il sapore in bocca, anche se non lo hai assaggiato; ascolti musica e nella testa nascono colori, anche se non si sta guardando alcunché di speciale. Eccetera.

Alessio ha eseguito la Ballata in si minore numero 2 di Franz Liszt (spero sia tutto esatto) su un pianoforte particolare, in una sala apposita, con una illuminazione pensata per l’occasione.

Se il video e il sonoro saranno capaci di risvegliare anche qualche altro senso, sarà stata sinestesia perfetta.

