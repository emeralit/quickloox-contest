---
title: "Salvo comunque"
date: 2012-04-09
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Ho un Pdf da spedire e, prima di spedirlo, alcune modifiche da applicare.

Lo modifico e lo spedisco.

A me però serve il Pdf nel suo stato originale, non in quello modificato.

Su Lion e Mountain Lion, con Anteprima, le modifiche vengono salvate automaticamente.

Lo stato originale è sempre a mia disposizione, grazie a Versioni. Nel momento in cui mi servirà, basterà un comando.

Nel vecchio modo avrei creato una copia supplementare del documento, mediante Salva con nome. Un comando, e siamo pari. Poi avrei però dovuto gestire la copia supplementare, cestinandola, archiviandola, oppure lasciando sul disco una copia inutile di un documento. Un comando in più.

In una circostanza come questa, Versioni è superiore al vecchio Salva con nome. Un comando in meno. E salvo comunque.

