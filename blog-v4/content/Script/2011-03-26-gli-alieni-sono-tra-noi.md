---
title: "Gli alieni sono tra noi"
date: 2011-03-26
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Scrive Stefano:

Media World vicino a casa: decido di acquistare per un nostro consulente un portatile che monta Windows 7 (non voglio annoiarti sul perché non abbia proposto un bel MacBook Pro).

Unico requisito chiesto dal nostro uomo è quello del peso: il più leggero possibile.

Mi avvicino al reparto dove in mostra ho decine di portatili e chiedo al commesso quale sia il pezzo più leggero.

Mi indica un VAIO e poi mi snocciola il peso, aggiungendo: …senza batteria però!

Allibito controbatto che avere il peso di un portatile senza la sua batteria è quanto meno fuorviante. Lui dice che …sono tutti così.

Danno tutti il peso senza batteria. Ma che senso ha? Ma si rendono conto di quel che dicono?

Poi guardo in fondo, a ore sei dal commesso, e mi ritrovo un bel MacBook Air impolverato sullo scaffale: mi rendo conto che siamo su un altro pianeta.

Siamo anche su un pianeta decisamente più abitabile.

