---
title: "Come è fatto dentro"
date: 2012-01-04
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Parlando di giochi di ruolo roguelike, ho scoperto LambdaRogue, libero e gratuito, che aggiunge anche un corredo grafico e sonoro rispettabile al tema classico dell’eroe che si avventura nel sotterraneo a caccia di gloria e di tesori.

Rispetto ad Angband c’è molta più grafica, molto più sonoro e anche molta più trama, tanto che si arriva alle sottotrame (in Angband c’è “solo” da arrivare al centesimo livello, con un condimento genericamente tolkieniano). Anche l’interfaccia è più moderna e semplifica di molto l’altrimenti necessaria memorizzazione di tanti comandi da tastiera.

Ciliegina sulla torta, per i curiosi del fatto tecnico, LambdaRogue non è ancora pacchettizzato come applicazione vera e propria per Mac. C’è da da fare doppio clic su un piccolo file eseguibile che fa partire il Terminale e da lì carica la finestra vera e propria del gioco. Tutti gli ingredienti del pacchetto software sono dunque visibili.

Se fa tempo brutto e non è ancora ripreso il lavoro, è un perfetto antidoto alla televisione.

