---
title: "La superiorità del prodotto"
date: 2012-04-03
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Microsoft è al lavoro per dimostrare alfine la concreta superiorità di Windows Phone sopra le alternative. Eccone un esempio:

Una ragazza ci ha raccontato di essere in cerca di uno smartphone ed essere orientata verso un iPhone. I commessi di Verizon [provider americano] suggerivano con vigore che invece avrebbe dovuto comprare Windows Phone. Davanti alla decisione di acquisto di iPhone e alla richiesta di conoscere le differenze tra iPhone 4 e iPhone 4S, il commesso è diventato scortese, ha alzato gli occhi al cielo e si è limitato a vendere il telefono. Più tardi un altro commesso le ha detto che, volendo davvero autonomia della batteria, avrebbe fatto meglio a riconsegnare iPhone 4S per prendere iPhone 4, e che il suo BlackBerry precedente aveva un’autonomia superiore di iPhone.

Viene da pensare a quanto il consiglio dei commessi sarà stato disinteressato.

