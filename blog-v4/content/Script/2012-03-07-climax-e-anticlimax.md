---
title: "Climax e anticlimax"
date: 2012-03-07
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Apro iPad e vedo apparire una dopo l’altra le foto scattate su iPhone. Niente magia, Streaming foto. Ma a me pare magico ugualmente e per un minuto sono tornato bambino. Impagabile.

In seguito sbaglio a digitare e App Store mi ammonisce di avere inserito una password incorretta.

Incorretta.

Certo, potevano scrivere disgiusta e sarebbe stato peggio.

