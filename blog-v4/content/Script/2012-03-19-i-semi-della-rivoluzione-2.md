---
title: "I semi della rivoluzione 2"
date: 2012-03-19
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Ecco il link per vedere via web la presentazione che ho tenuto a Trento in occasione del convegno Erickson su La Scuola nell’Era Digitale e ho brevemente (ehm) riassunto qualche giorno fa.

Da aggiungere c’è una breve introduzione sugli isomorfismi tra gioco di ruolo e studio scolastico: due ambienti in cui si entra privi di esperienza per imparare nuovi poteri, compiere imprese che poi verranno validate e apprendere da insegnanti e da libri. Non sono andato molto più in là di così dal momento che l’argomento principale in realtà era un altro; non mi stupirei comunque, un giorno, di avere classi virtuali guidate dal docente all’interno di ambienti di simulazione o ricostruzione eccetera eccetera.

Su queste basi ho salutato il pubblico per mano del mio personaggio principale in World of Warcraft e ho brevemente riassunto l’isomorfismo di cui sopra durante la presentazione. In passato mi è capitato di “fare lezione” di comunicazione a un’aula di corsisti portando tutti dentro Lumen et Umbra e facendo interazione attraverso l’interfaccia testuale (dal Terminale di Mac, sostituire i due punti con uno spazio in caso di collegamento telnet). Oggi, con gli strumenti che ci ritroviamo, si potrebbe fare di tutto e di più anche in modo più immersivo.

Mi auguro che Apple metta prima o poi mano al formato di memorizzazione di iWork, perché ho tenuto una presentazione composta soprattutto di immagini e la dimensione del file si gonfiava come un pallone.

