---
title: "La soluzione cinque percento"
date: 2011-10-09
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Sollecitato, parlo di Richard Stallman che alla notizia della scomparsa di Steve Jobs ha commentato

Steve Jobs, pioniere del computer come prigione alla moda, progettato per mutilare gli sprovveduti dalla loro libertà, è morto.

Come il sindaco di Chicago disse del precedente e corrotto sindaco Daley, non sono lieto che sia morto, ma sono lieto che se ne sia andato. Nessuno merita di morire – non Jobs, non il signor Bill, neanche persone colpevoli di mali ancora peggiori dei loro. Ma meritiamo tutti la fine della maligna influenza di Jobs sul computing delle persone.

Sfortunatamente questa influenza continua nonostante la sua assenza. Possiamo solo sperare che i suoi successori, nel tentativo di continuare nella sua scia, siano meno efficaci.

Eliminerei subito le questioni del pessimo gusto, del cinismo gratuito, del sarcasmo fuori luogo. Tutto si commenta da sé.

Ho conosciuto Stallman di persona. È un eremita del nostro tempo, non di quelli veri che si isolano per esempio sulle montagne dell’Abruzzo e dell’Umbria, ma un eremita digitale. La sua visione del computing aperto come totalmente aperto è idealmente sacrosanta e il ruolo di Stallman negli anni novanta è stato cardinale. L’uomo ha le sue peculiarità (non possiede nemmeno una borsa, ma viaggia con una sporta di plastica del supermarket, o almeno così l’ho sempre visto), ma una forza interiore indicibile e un coraggio da leone.

Quando la sua visione diventa ideologica e passa sopra la realtà, tuttavia, è deleteria. Piccolezza morale a parte, è l’avere sbagliato totalmente bersaglio a sgonfiarla e smontarla come neanche meriterebbe del tutto.

Apple, ho sostenuto più volte, è un’azienda del cinque percento. I suoi prodotti sono troppo chiusi? Mettiamo che lo siano. Siamo sprovveduti a usare prodotti chiusi? Facciamo finta per un momento che sia vero.

Il fatto è che sono Apple (arrotondo) cinque computer su cento. Sono Apple – ha tenuto a mostrarlo Tim Cook annunciando iPhone 4S – cinque cellulari su cento. Non è da escludere che un domani, a mercato maturo, saranno Apple cinque tavolette su cento. Sono Apple più di settanta lettori musicali su cento, ma questo a Stallman non interessa e neanche a noi.

Il nucleo di OS X e iOS è aperto. Forse non esattamente come piace a Stallman, ma chiunque se lo può scaricare e pasticciare a piacimento. Con Windows, per non fare nomi, è proibito.

Una parte enorme nei meriti di Jobs sta nell’avere reso disponibili le tecnologie più avanzate e il design più innovativo a milioni di persone che altrimenti forse, chissà, magari avrebbero visto tutto comunque. Ma non si sa quando e non si sa come. Apple raramente inventa cose veramente nuove; le rende utilizzabili in modo semplice e potente a un pubblico ampio. Prima di Macintosh, di mouse e finestre sapevano solo al Palo Alto Research Center di Xerox. Prima di iPhone esistevano smartphone con schermo al tocco, ma solo dopo iPhone sono diventati la scelta principe. La predicazione di Stallman sulla libertà del software è idealmente ineccepibile e totalmente condivisibile. Purtroppo lo sviluppo del sistema operativo Gnu propugnato da Stallman è iniziato nel 1984, quando è uscito Macintosh. E non è ancora finito.

Se fosse per Stallman, staremmo ancora aspettando la tecnologia del 1984. Parlo per noi persone normali, non per i programmatori e i maghi del Terminale, chiaro. Macintosh, con tutti i suoi difetti, ci ha portato un computing più facile. Gnu, con tutti i suoi pregi, è patrimonio di un manipolo di guru e solo loro. iPad ci ha portato un computing ancora più facile di Macintosh. Lo stilita che predica asceticamente in cima alla colonna e non scende mai è un santo, ma siamo noi peccatori a sostenerlo e a issargli da mangiare fin lassù.

Il punto chiave è quel cinque percento. Facciamo finta, per assurdo, che Mac e iOS siano le piattaforme più chiuse e pericolose per la libertà del mondo.

Sono il cinque percento.

Ci rimettiamo noi. Il mondo resta libero. Chi vedesse la luce, avrebbe una scelta alternativa enorme.

Se Mac raggiungesse il novantacinque percento del mercato, sarebbe da guardare con sospetto, comunque. Invece è il cinque. Occorre sospettare di chi sta al novantacinque. Quando non c’è abbondante scelta alternativa, la situazione è grave.

Che poi tutto questo arrivi in un’epoca dove tengo installati sul computer sette browser diversi (non scherzo: Safari, Firefox, Chrome, Camino, iCab, OmniWeb, Lynxlet) suona stonatissimo. Quando la lotta di Stallman è iniziata, di browser ce n’era uno solo, al novantacinque percento, e non era per niente aperto. I tre motori dei miei sette browser sono aperti al cento percento.

Perché il browser? Perché è un simbolo: sul browser posso basare un sistema operativo, lavorare come in ufficio e giocare a volontà.

Qualsiasi critica sulla (presunta) chiusura dei prodotti Apple, fino a che i suddetti sono al cinque percento, è fuori bersaglio.

E portarla ora è particolarmente di cattivo gusto, Mr. Stallman.

