---
title: "Lo scontrino non scontato"
date: 2012-06-14
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Abbiamo tutti presenti la situazione: il ristoratore vede benissimo che siamo turisti e così ci prova, ricaricando l’impossibile sui prezzi. Se poi il turista è uno straniero, possibilmente non avvedutissimo, peggio ancora.

Naturalmente il sistema si basa sull’idea di un afflusso continuo di turisti nuovi, perché quelli che hanno già provato il metodo penseranno bene di andare altrove, con danno generale all’economia. Altrettanto naturalmente non si discute della normale economia di mercato: se la domanda supera l’offerta, è sacrosanto che il prezzo salga. L’alta stagione non è una furberia, ma una realtà legittima. Si discute dell’abuso del metodo, del bicchiere di acqua del rubinetto a dieci euro, della speculazione commerciale che è cattiva quanto quella finanziaria.

Allora facciamo finta che l’associazione locale dei ristoratori, la Pro Loco, il Comune, chi per loro, stabilisca un tetto massimo equo di spesa. Il cliente effettua l’ordine tramite un iPad o un iPhone, collegato in modo opportuno al registratore di cassa. Se il prezzo supera la cifra equa, appare un avviso e il registratore non rilascia lo scontrino. Al che il cliente si rende conto del tentativo di fregarlo.

Sembra impossibile? Eppure hanno cominciato. In Cina.

Ci sono un sacco di distinguo in tutta questa vicenda, a partire dai fatti che un iPad opportunamente programmato potrebbe sostituire totalmente il registratore di cassa e che certa Italia possiede risorse inesauribili per trovare l’inganno non appena fatta la legge.

Però c’è da riflettere molto su ciò che le reti, le connessioni, gli apparecchi mobili rendono possibile. E su un terzo fatto, che abbiamo più probabilità di uscire bene dai tempi attuali se cominciamo a pensare in modo nuovo, creativo invece che più furbo degli altri, capace di spaccare le abitudini incrostate. Non c’è più niente di scontato e non parlo dei prezzi al dettaglio.

