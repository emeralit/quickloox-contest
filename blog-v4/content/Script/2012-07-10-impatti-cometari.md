---
title: "Impatti cometari"
date: 2012-07-10
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Mi scrive, graditissimo, Piergiovanni e lo fa con un iPad. Lui è un professore. A proposito dei discorsi fatti su iPad nelle scuole, la sua testimonianza estemporanea – ma sembra incisa nel marmo – è questa.

Trovo il tempo di scriverti durante questi Esami di Stato ormai agli sgoccioli, per raccontarti una storia, una di quelle storie belle che nel nostro Paleozoico Didattico appaiono come la cometa Hale-Bopp. Liceo Scientifico Statale Antonio Pacinotti, Cagliari, 3 luglio, esami orali della quinta D. Una ragazza come tante, adolescente come tante, si siede, firma, inizia a parlare esponendo la sua tesina multimediale. Ebbè, come tutti. No. Lei tira fuori il suo iPad e mostra un lavoro preparato con iBooks Author. Mi lancia uno sguardo di complicità, vedendo che tenevo la mia insostituibile Cassiopea davanti a me (scribacchiavo su Pages) e inizia a spiegare cosa era iBooks Author. Tesina interessante, materiale ben assemblato, semplice nella sua struttura eppure davvero eccellente, e mi ha divertito vedere i colleghi “addetti ai lavori” cadere dalle nuvole davanti a questo contenuto. Non faccio commenti, l’episodio si commenta da solo. Ma ti avviso, io che nel mondo della scuola ci sto da ventuno anni: la strada è lunga. Quando ai ragazzi parli dell’iPad ti dicono che costa quanto un pc e i colleghi “illustri” storcono il naso.

Sai perché gli USA sono avanti a noi riguardo la scuola digitale? Perché nelle scuole e nelle università americane si studia, non si fa selezione culturale. E Internet non serve a copiare la versione di latino ma a trovare risorse, proprio perché la didattica viene impostata in modo tale che lo studente sia motivato allo studio autonomo, modalità ben diversa dal retaggio gentiliano della nostra scuola. Ma sarebbe troppo lungo ora approfondire, godiamoci questa storia bella prima di andare in ferie!

Aggiungo: ho appena capito perché spiego nelle scuole che Wikipedia a scuola va scritta e non va letta. Un bravo professore me lo ha appena spiegato in poche parole.

Inoltre: al momento di obiettare che non ci sono i soldi per dare un iPad a tutti, dovremo anche trovare “soluzioni” per boicottare quelli che li hanno e si elevano con le forze proprie sopra il minimo comune denominatore. Altrimenti, forse è meglio dare quelli che si possono.

Infine: la tesina presentata ai professori avrebbe avuto lo stesso impatto presentata con un portatile, invece che con una tavoletta capace di passare con naturalezza di mano in mano?

