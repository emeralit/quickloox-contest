---
title: "Il fondo del barile"
date: 2011-05-25
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Direttamente dal 25 ottobre 2001 questo articolo di tale Arne Alsin, autore della rubrica The Turnaround Artist (l’artista delle inversioni di tendenza) per The Street. Qualche perla.

Non comprate azioni Apple. Se ne avete, vendetele. So che l’azienda ha un seguito molto leale, ai limiti del culto, ma la base più ampia di credenti si erode da anni.

Con meno del cinque percento del mercato, l’azienda […] è irrilevante.

Si va sul sicuro dicendo che il modello di business di Apple è irrimediabilmente guasto.

Per sopravvivere, Apple deve convincere gli utilizzatori di Windows a spostarsi sulla piattaforma Mac. Ma siccome Apple non è competitiva né sui prezzi né sulle applicazioni, non c’è ragione di spostarsi. La partita è finita. Dell, Ibm e Hewlett-Packard hanno sul mercato Pc una presa sicura, con il modello di Dell che nel lungo termine è chiaramente quello vincente.

La storia di Apple ora è cibo per gli storici dell’industria.

Il titolo dell’articolo è Apple sta grattando il fondo del barile.

Complimenti per l’analisi.

