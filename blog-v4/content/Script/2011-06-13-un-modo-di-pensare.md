---
title: "Un modo di pensare"
date: 2011-06-13
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Non che debba arrivare io a segnalare i video nei quali Steve Jobs dà il via alla Worldwide Developers Conference 2011 e chiede alla municipalità di Cupertino il permesso di costruire un nuovo campus Apple.

Mi permetto invece di confrontare lo stile di Steve Jobs nel fronteggiare cinquemila sviluppatori paganti (non poco) per venire a sentire come mai dovrebbero ancora sviluppare software per gli apparecchi Apple e nel presentarsi quasi come un imputato davanti a una decina di consiglieri comunali di provincia, evidentemente meno avvezzi alle platee globali.

Jobs sa benissimo in tutti e due i casi di essere filmato e di finire sugli schermi di tutto il mondo. E dà l’impressione che gli importi poco, nell’uno o nell’altro caso. Che sia padrone della scena o che una consigliera gli chieda perché mai la città dovrebbe dare il consenso alla costruzione di un nuovo campus che aumenterà gli alberi riducendo la superficie edificata, nel dare una nuova casa a dodicimila (e magari oltre) dipendenti Apple dal 2015.

Poi vale la pena di guardare i progetti del nuovo campus esposti da Jobs ai consiglieri e al sindaco. Quando si parla dei prodotti Apple e del loro design, poi si vada a vedere a come l’azienda immagina la propria prossima sede. E poi si pensi ad altre aziende, dove forse l’innovazione è di casa. Ma la casa non contiene alcuna innovazione e forse c’è un modo di produrre, anche valido, tuttavia manca un modo di pensare.

