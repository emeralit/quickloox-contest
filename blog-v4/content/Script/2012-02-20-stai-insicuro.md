---
title: "Stai insicuro"
date: 2012-02-20
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Scrivo da iPad in attesa che Mac finisca di macinare. Mi sono lanciato nell’installazione di Mountain Lion Developer Preview, che non è andata a buon fine. Il disco soffriva di errori non riparabili con Utility Disco.

Si tratta del caso più spiacevole che può accadere. Se qualcosa va storto e Lion o Mountain Lion avviano l’installazione ma non riescono a completarla, il computer resta prigioniero dell’installatore, che continua a ripartire a ogni avvio e a riproporre lo stesso problema.

Recuperare è abbastanza semplice, per fortuna. Ho messo in servizio un vecchio disco di riserva dove ho effettuato una installazione di riserva di Lion. Ho fatto ripartire il sistema da lì, ho spianato il disco con problemi e adesso è in corso il recupero da Time Machine di tutto quanto va recuperato.

La cosa che fa veramente innervosire sono le opzioni di sicurezza. Se recuperi un backup da Time Machine e vuoi che la nuova utenza abbia lo stesso nome e la stessa password della vecchia, devi effettuare il recupero da un utente diverso; accedi a un Portachiavi diverso dal tuo originale e ci vuole una password di amministrazione, e il vecchio portachiavi login, se per caso non hai esattamente a portata di mano la giusta password, non si apre, costringendoti a una migrazione del portachiavi. Non la faccio lunga: in assoluto, la cosa più noiosa di tutto il processo di recupero e ricostruzione di una nuova utenza è tenere conto di tutte le questioni sollevate dalle funzioni di sicurezza e protezione di OS X.

Delle quali sono il primo sostenitore. Molto meglio un computer un po’ troppo sicuro che uno troppo facilmente perforabile. E l’altroieri ho installato a casa di altri un Pc Windows 7: alla terza volta che il sistema chiedeva il permesso inutile per fare una cosa assolutamente normale, ho ringraziato le divinità di dover risolvere a casa mia problemi dei Mac invece che problemi dei Pc.

Tuttavia non nego che in questo momento mi piacerebbe sintonizzarmi sul telegiornale e scoprire che è stato compiuto un progresso clamoroso e fantascientifico nella sicurezza dei sistemi operativi, che blinda il computer come un carro armato quando è bersaglio di attacchi e invece ha la resistenza del pandoro quando è l’utente legittimo ad avanzare giuste pretese.

Difficile che accadrà mai, volere una cosa e il suo contrario insieme è sempre complicato. Torno a vedere come andiamo con Assistente Migrazione.

