---
title: "L immortalità dell anima"
date: 2012-07-16
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Chiedo a chiunque, oggi, di estrarre da un cassetto un apparecchio elettronico tascabile (della giacca) del 1997, e con quell’apparecchio fare controllo remoto di un computer. Adesso.

Con Newton lo hanno fatto.

Una dimostrazione di tecnologia in anticipo sui tempi come questa non credo si vedrà mai più. Si tenga presente che, anno 2012, la risoluzione logica degli smartphone è ancora paragonabile a quella fisica di Newton. Un’anima immortale.

