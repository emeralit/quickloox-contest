---
title: "Sottile come un supercomputer"
date: 2011-05-15
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Per evitare di dare troppo per scontate certe cose, Jack Dongarra ha portato il software Linpack su iPad 2.

Dongarra è uno degli autori di Linpack, software utilizzato dal 1979 per valutare la capacità di calcolo dei supercomputer.

iPad 2 fornisce prestazioni di calcolo tra 1,5 e 1,65 gigaflop, miliardi di operazioni in virgola mobile (con i decimali) per secondo.

Significa che nel 1994 un iPad 2 sarebbe entrato nell’elenco dei supercomputer più veloci del mondo, con risultati simili a quelli di un Cray 2, il primo della classifica nel 1985.

Certo, manca il divertimento del costruire un cluster di calcolo a partire da una pila di schede logiche di Apple II… ma in compenso il raffreddamento di iPad 2 è molto meno ingombrante di quello di un Cray.

