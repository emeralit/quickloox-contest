---
title: "Solo vantaggi"
date: 2011-10-14
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Gli autori di un sito che seguo spesso per mille ragioni di lavoro e di aggiornamento hanno ultimato una revisione del sito stesso e hanno illustrato i progressi ottenuti grazie alla revisione:

Le vostre slide appariranno senza problemi su qualsiasi apparecchio, riprodotte fedelmente a partire da uno dei 28 formati di presentazione che supportiamo.

Potete inviare il link di presentazione a un vostro amico o collega qualunque apparecchio stia utilizzando. Potrà vedere la presentazione.

Il caricamento delle slide è più veloce del 30 percento.

Le slide sono riprodotte in formato web e per essere viste non servono plugin né software aggiuntivo.

Il sito è SlideShare, l’equivalente di YouTube per le presentazioni.

Se adesso scrivo che SlideShare prima pubblicava le slide in Flash e ora lo fa in Html5, istantaneamente qualcuno pensa che io ce l’abbia con Adobe, che conduca una qualche crociata, che esista un duello all’OK Corral tra Adobe e Apple e un sacco di altre banalità.

Nulla di tutto questo. Posso vedere le presentazioni di SlideShare su iPhone e su iPad e prima non potevo. Anche i miei amici con Android lo possono fare. E se domani esce qualcosa di nuovo, anche lui potrà mostrare le presentazioni.

Non bastasse, il caricamento è quasi di un terzo più veloce. Perché bisognerebbe metterla come una forma di tifo o di ostilità, quando si tratta semplicemente di avere un servizio migliore?

