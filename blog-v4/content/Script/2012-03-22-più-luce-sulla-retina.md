---
title: "Più Luce Sulla Retina"
date: 2012-03-22
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Monumentale articolo di DisplayMate che mette a confronto gli schermi Retina di iPad 2, nuovo iPad e iPhone 4.

Conclusioni stringatissime: il nuovo iPad darà il meglio di sé dove c’è da leggere testo piccolo e cogliere dettagli grafici. Complessivamente la qualità dello schermo è migliorata rispetto a iPad 2 e, in parte, a iPhone 4, che partiva da livelli più alti grazie alla superficie ridotta dello schermo. Non è tanto importante la risoluzione quanto l’aumento della saturazione del colore e avremo il nirvana grafico quando la retroilluminazione a Led avverrà con un pannello per ciascuna colore primario elettronico, verde rosso e blu.

Per chi mastica poco inglese ci sono anche varie tabelle… illuminanti, con sfondi rossi gialli e verdi a chiarire subito il livello essenziale di valutazione.

Per chi lo mastica di più, si trovano anche considerazioni… brillanti su dove i fabbricanti di tavolette, tutti, hanno da migliorare, per esempio la gestione dei riflessi sullo schermo, la reattività del sensore di luce ambientale, i controlli di luminosità (definiti sostanzialmente inutili, fuori dal risparmio di batteria) e così via.

Interessante il punto sugli schermi Oled di cui si parla spesso. Fino a che i prezzi non si abbassano e l’efficienza non cresce, gli attuali schermi Lcd con tecnologia Ips sono imbattibili.

