---
title: "Guarda, Senza Browser"
date: 2011-05-06
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Una delle obiezioni principali all’ascesa di Html5 per la programmazione di giochi è che alla fine si finisce sempre per stare dentro un browser. A volte una vista di browser; il gioco è conchiuso nella sua app la quale tuttavia, con i mezzi del sistema o autonomamente, riproduce nella schermata quello che accadrebbe dentro un browser.

Anche questa frontiera è caduta e a dimostrarlo sono Biolab Disaster e Drop, due creazioni di PhobosLab. Gratis per iPhone e, certo, anche giocabili dentro un browser, volendo, l’uno e l’altro.

Ciò che li rende interessanti è tuttavia proprio il fare a meno del browser o del suo succedaneo, per usare direttamente la macchina di interpretazione di linguaggio JavaScript presente in iOS.

I vantaggi sono maggiore velocità ed efficienza, oltre per esempio a poter organizzare una gestione del sonoro, attualmente una delle manchevolezze fondamentali di Html5.

Sono cose semplici, dimostrative. Eppure aprono una nuova via all’evoluzione della programmazione per iOS. Via autorizzata da Apple, che non guasta.

