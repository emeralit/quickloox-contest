---
title: "Allearsi è gratis"
date: 2011-11-14
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Si scopre sempre qualcosa. Ho appena scoperto l’esistenza della Free Game Alliance, una specie di patto di solidarietà tra cinque giochi open source gratuiti e di buonissima qualità, per mutua promozione e perché l’unione fa la forza.

Al momento l’alleanza include il mio preferito The Battle for Wesnoth (strategia a turni), MegaGlest (strategia a turni), Planeshift (gioco di ruolo di massa online), Rigs of Rods (simulazione automobilistica) e Xonotic (sparatutto in prima persona).

A prima vista sembrerebbe che MegaGlest, Xonotic e Rigs of Rods non si possano giocare su Mac.

Basta un po’ di intraprendenza. Esiste una beta di MegaGlest per Mac, che si trova aggiornata seguendo uno dei thread sul forum del gioco. Per i più intraprendenti, c’è una guida alla compilazione.

Rigs of Rods compilato per Mac è disponibile, sebbene in una versione non ancora aggiornata. Si trova sulla pagina dei download.

Xonotic pare non esistere per Mac, a leggere sul sito. Invece dentro il file da scaricare si trova anche un eseguibile .app che funziona.

Magari qualcuno scopre qualcosa di divertente per le serate invernali con televisione noiosa (o indipendentemente). Senza Free Game Alliance, tre su cinque mi sarebbero passati inosservati. Ed è tutto software gratuito.

