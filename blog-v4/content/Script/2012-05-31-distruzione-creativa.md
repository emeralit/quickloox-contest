---
title: "Distruzione creativa"
date: 2012-05-31T13:37:54+02:00
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Di solito non c’è da essere contenti per l’abbandono di un progetto open source. L’eccezione che conferma la regola è Moonlight, il tentativo di creare una alternativa libera a Silverlight di Microsoft.

Il quale a sua volta è il tentativo di Microsoft di ostacolare Flash.

Miguel de Icaza, il fondatore del progetto open source Mono da cui discende Moonlight, ha dichiarato che quest’ultimo è stato abbandonato.

La ragione? Silverlight non ha riscosso grande successo su web e non è diventata quella tecnologia indispensabile che pensavo sarebbe diventata.

La battaglia dei plugin si è conclusa con la sconfitta di tutti contendenti: Flash per raggiunti limiti di età, Silverlight perché si giustifica solo se esiste Flash, Moonlight che ha un senso solo partendo da Silverlight.

Oggi il web come si deve è privo di fastidiosi, pesanti, spreconi, insicuri plugin per browser. Ogni passo verso la loro dipartita va accolto con serenità e soddisfazione, Moonlight compreso. Nell’ottica della distruzione creativa, dalla loro scomparsa mai troppo veloce emergono standard migliori e beneficio di tutti.

Difatti a usare Silverlight sono rimasti elefanti acefali come la Rai o siti con una infrastruttura obsoleta.

Se qualcuno pensa che io abbia un fatto personale contro Flash o Silverlight, sbaglia di grosso: ce l’ho contro Moonlight. Doveva permettermi di usare il Character Builder di Dungeons & Dragons senza bisogno di Silverlight (Wizards of the Coast progetta ottimi giochi, ma sull’informatica pensano di trovarsi nel 1992). Invece non ci è mai neanche andato vicino.

Un conto sono gli ideali. Ma il gioco di ruolo non si tocca.

