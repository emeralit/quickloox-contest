---
title: "Un sentiero accidentato"
date: 2012-03-23
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Un segnale di dove ci stia portando il futuro della comunicazione in rete? Nature Valley Trail View, applicazione del principio di Street View che permette di percorrere passo per passo i sentieri di tre meravigliose aree naturali degli Stati Uniti a ottima risoluzione fotografica e con note a commento, fotografie, coordinate geografiche eccetera.

La bussola indica chiaramente la direzione di dove vanno i siti. A vedere la pagina di ingresso, potrebbe un documentario di National Geographic. Naturalmente non c’è traccia di plugin da aggiungere al browser: per offrire splendide esperienze multimediali si lavora con i nuovi standard del web: Html5, Css, JavaScript. La dimensione che può raggiungere la pagina prima di superare la definizione delle foto è notevole.

Contemporaneamente la navigazione mostra le contraddizioni e gli ostacoli ancora da superare. Servono un bel po’ di banda e un computer non troppo vecchio: l’informatica sarà disciplina matura quando anche un computer vecchio potrà fare una cosa nuova, esattamente come la mia auto del secolo scorso può tranquillamente affrontare un nuovo tratto autostradale. E viene richiesta una risoluzione minima di 1.024 x 768, perché un iPad può visitare i parchi, ma un iPhone no. Errore da matita rossa, anche perché uno potrebbe averlo in funzione sul posto.

Certo, uno può pensare a come si vedrà il sito sul nuovo iPad, con lo schermo a risoluzione altissima e il processore superveloce… il sentiero presenta asperità, ma è quello giusto e ci porta verso panorami che mai avremmo immaginato.

