---
title: "A volte ritornano, gli idisk"
date: 2012-10-10
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
nteresse e curiosità per OpenDrive: gli ennesimi cinque gigabyte gratis e il resto a pagamento, che però stavolta si montano nel Finder alla stregua di un qualunque altro disco, come ai bei vecchi tempi di iDisk.

Cose positive: condivisione file, comando facile di disinstallazione, non si prende cinque gigabyte sul disco reale, una app per iPhone.

Cose migliorabili: una icona in più – sempre troppe – nella barra dei menu e la mancanza di una app per iPad.

Da provare.

