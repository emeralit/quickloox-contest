---
title: "Libertà Vo Cercando"
date: 2012-03-18
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
App Store per iOS e App Store su Mac sarebbero una minaccia alla nostra libertà di avere apparecchi di cui siamo i pieni padroni.

Ci ho pensato leggendo la recensione di Byword a opera di Shawn Blanc. Byword è a metà tra un word processor semplice e un editor di testo altrettanto semplice, che però ha la possibilità di scrivere in Markdown, una sintassi semplificata e facile da leggere trasformabile automaticamente in Html. Costa 2,39 euro su App Store per iOS e 7,99 euro su App Store per Mac.

(Nel secolo della rete serve più avere Html che i vecchi .doc).

Scrive Blanc:

Questa recensione è stata scritta ed editata esclusivamente in Byword. [compreso il necessario per pubblicarla su web].

Ho iniziato questo articolo un martedì notte sul mio iPhone attorno alle 23:30, mentre mio figlio era sveglio per la poppata. Mercoledì mattina l’ho continuato aprendo Byword in ufficio sul mio MacBook Air. [Byword è compatibile iCloud e sincronizza i documenti su tutti gli apparecchi]. Dopo pranzo, l’ho completata nel mio coffee shop preferito con iPad attaccato a una tastiera Bluetooth.

Nota ovvia:

Non significa che d’ora in poi scriverò sempre gli articoli in più luoghi su una pletora di apparecchi, ma fa piacere avere un editor di testo su tutti gli apparecchi che mi va di usare e fa piacere che tutti gli articoli su cui lavoro siano sincronizzati e sempre accessibili da una stessa applicazione.

Probabilmente Shawn Blanc, nel rivolgersi a Byword e agli App Store, ha sacrificato qualche sua libertà. In cambio ha ottenuto altre libertà, per esempio quella di poter lavorare a un articolo comunque e dovunque in automatico. Al prezzo complessivo, fosse stato italiano, di 10,38 euro. Più o meno un quinto di quello che sarebbe costato un programma equivalente dieci o venti anni fa.

È libertà questa? Forse non con la elle maiuscola. La trovo comunque una libertà interessante.

Più che altro. Posso avere il riferimento di una soluzione software come questa che funzioni nel modo descritto, a un prezzo paragonabile, di prima che arrivassero gli App Store? Può darsi che io stia riscoprendo l’acqua calda.

Non ho l’ardire di sostenere che gli App Store abbiano favorito l’emergere di soluzioni come queste, a questi prezzi. Però vorrei sentire opinioni contrarie a questa tesi, motivate, con dei fatti verificabili a sostegno.

