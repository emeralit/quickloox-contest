---
title: "Il buon tempo andato"
date: 2011-06-12
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Per i fan di quando si stava meglio pur stando peggio, Olduse.net ha iniziato a riprodurre l’utilizzo di Usenet con trent’anni esatti di ritardo sul presente, ossia come funzionava il tutto nel 1981.

Mentre pubblicavo ho scoperto anche di meglio: Telehack, una simulazione incredibilmente ricca, con tanto di Bbs che veramente esistevano e giochi perfettamente praticabili, dal mitico Adventure in poi (grazie a Francesco!)

Istruttivo, divertente, nostalgico… e meglio il 2011.

