---
title: "In ritardo alla festa"
date: 2011-10-12
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Chiedo scusa: mi ero dimenticato che anche quest’anno Sillysoft organizza Luxtoberfest, un mese di tornei multiplayer online di Lux, apprezzatissimo clone di Risiko, anzi, Risk che l’azienda ha arricchito e diversificato in ogni modo concepibile e forse pure oltre.

Anche con il ritardo in acconto c’è ancora molto che deve succedere e un sacco di divertimento per chi apprezza il genere.

Sul forum di Luxtoberfest si trova ogni particolare e, ovviamente, anche il calendario degli eventi.

Lux costa “ben” 14,95 dollari su Mac (in promozione) e 3,99 euro su iOS, con l’aggiunta degli essenziali però gratuiti Lux Touch e Lux Usa.

Consiglio agli appassionati di strategia e boardgame una attenta occhiata a Castle Vox, definito da Sillysoft un incrocio tra Axis & Allies e Diplomacy. Si gioca multiplayer volendo, si gioca gratis in versione base, possiede la capacità di variare il ritmo di gioco a totale piacimento per avere il massimo del relax o della frenesia a scelta e per il gioco singolo ci sono intelligenze artificiali a disposizione che vengono date per micidiali, ove non ci si accontentasse di quelle semplici.

Soprattutto, si gioca in contemporanea e tutte le mosse vengono elaborate nello stesso momento dal programma; non si dovrà mai attendere il turno di un altro. Aspetto solo un’edizione di Risiko con la contemporaneità delle mosse e sarò (un giocatore di boardgame) felice.

