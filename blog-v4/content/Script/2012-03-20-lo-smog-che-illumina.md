---
title: "Lo smog che illumina"
date: 2012-03-20
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Il leggendario smog londinese degli anni sessanta non esiste più. La città è molto più pulita di un tempo e la sua aria, sempre nell’insieme delle metropoli, è decisamente più respirabile e limpida di un tempo.

Il riferimento mi era tuttavia prezioso per introdurre la mail di Beppe, cui lascio la parola.

Come sai lavoro al [prestigioso quotidiano di Londra], ho appena ricevuto una mail dalle Risorse umane, dice più o meno:

“Ci rendiamo conto che iPad è sempre più centrale nel nostro settore. Molte persone lo trovano estremamente utile sul posto di lavoro e il [quotidiano] ne incoraggia attivamente il suo utilizzo. Per questo motivo ti offriamo 200 sterline di sconto su un qualsiasi modello e lo offriamo a tutti i dipendenti indistintamente”.

Penso che lo comprerò ma il punto è che secondo me siamo alla vigilia di una penetrazione massiccia di Apple in ambito professional e il cavallo di Troia sarà lo strumento apparentemente meno professional di Apple.

Un altro effetto collaterale dell’enorme penetrazione di iPad è che non sviluppiamo più assolutamente niente che non sia portabile su iOS, Flash qui è un lontano ricordo.

Un altro aneddoto interessante: gli uffici di Google sono proprio di fronte a noi, qualche settimana fa è venuto a parlare il vicepresidente di Google europa e ha raccontato un po’ della strategia Google per il futuro.

La cosa più interessante che ha detto è stata: “abbiamo appena smantellato la divisione mobile”… mormorio in sala… “infatti d’ora in avanti tutte le applicazioni Google verranno pensate PRIMA per il mobile e dopo rilasciate alle altre piattaforme”… la sala applaude.

Poi ha dato cifre impressionanti sulle percentuali di ricerche fatte usando un dispositivo mobile rispetto a laptop e desktop e, dato ancora più interessante, ha detto che il 25% delle ricerche mobile sono vocali!

Non vedo l’ora di sentire tutti quelli che erano costretti a usare Word in azienda, quando verranno costretti a usare iPad.

In realtà conto che non avvenga e che iPad si diffonda grazie alla soddisfazione invece che all’oppresione. Però, chi si ricorda quando le aziende erano fortezze impenetrabili poste a difesa di Windows e di Office?

In omaggio per gli acquirenti prossimi venturi del nuovo iPad, una pagina di sfondi scrivania potenzialmente artistici (il nuovo obbligo sociale, chi non si personalizza uno schermo del genere?) e un’altra pagina ancora.

Chitika sta monitorando il tasso di adozione del nuovo iPad sulla propria rete di siti editori di inserzioni pubblicitarie e questo è il risultato. Ricordo che il nuovo iPad è stato presentato venerdì scorso in una rosa ristretta di nazioni. Non c’è da fidarsi troppo di Chitika che in passato ha preso cantonate discrete, ma come idea dell’andazzo generale merita considerazione.

