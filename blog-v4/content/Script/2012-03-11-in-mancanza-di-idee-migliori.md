---
title: "In mancanza di idee migliori"
date: 2012-03-11
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Mi ha dato l’idea di questo post dumbo54: non ho visto l’ultimo Windows Phone ma sarei curioso di avere un tuo commento.

Neanch’io l’ho visto, confesso. Tutti quelli che lo hanno provato ne parlano bene e certamente anche solo il fatto di non imitare pedissequamente iOS e Android merita di per sé un elogio.

Nondimeno – e qui commento – ho la sensazione che rimanga una bella prova d’artista e poco più. Anche se domani facesse il quindici percento invece del poco più che virgola di oggi.

Non solo contano sempre meno i numeri. Sta contando meno anche l’interfaccia, a favore dell’ecosistema complessivo. Vuoi avere un oggetto bello da tenere in mano, con feeling. Vuoi che sia facile da usare e che abbia una montagna di applicazioni a disposizione. Vuoi che non dia pensieri di sicurezza e che ti porti velocemente e con eleganza a raggiungere l’obiettivo.

Più aree il prodotto manca di soddisfare, meno ha speranze. Più le soddisfa tutte, meglio funziona. Se ha una sola caratteristica formidabile, ma pecca sul resto, non funziona più.

Può darsi che sia solo il mio delirio di opinione. Tuttavia mi ha dato da pensare l’analisi dell’impareggiabile Horace Dediu di Asymco, sulla base dei più recenti dati comScore riferiti al mercato americano.

I risultati sommari: iOS e Android continuano ad avanzare. Windows Phone e BlackBerry continuano a indietreggiare.

L’analisi: le possibilità degli ultimi due di ritornare a contare dipendono da quanto riusciranno a strappare utilizzatori esistenti alle piattaforme vincenti, o da quanto riusciranno ad aggiudicarsi nuovi utilizzatori nel mercato che si allarga.

La sua conclusione: il primo obiettivo fa tremare le vene dei polsi, perché il tasso di abbandono di iOS e Android, per tutta una serie di motivi, è molto basso. Il secondo si basa su strategie che le piattaforme dominanti perseguono allo stesso modo, finora con molto più successo.

In pratica: o arriva una idea veramente nuova a scardinare lo status quo attuale, come ha fatto iPhone nel 2007, o è molto probabile che Windows Phone resti marginale, anche ove decuplicasse il proprio attuale seguito.

