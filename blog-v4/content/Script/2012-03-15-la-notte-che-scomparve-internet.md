---
title: "La notte che scomparve internet"
date: 2012-03-15
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
L’altra sera mi collego a WordPress ma non c’è Internet. FastWeb, spiega la spia rossa del modem, è assente.

Ho necessità di collegamento e non posso attendere. Tiro fuori dal cassetto la chiavetta cellulare e la attacco al Mac.

Solo che tempo fa ho reinstallato il sistema e dal backup di Time Machine non riesco a fare saltare fuori la vecchia configurazione, che non ricordo. Probabilmente, con più tempo a disposizione di quello che ho prima di andare a dormire, ci arriverei, ma non ne ho voglia. Così, invece che comandare la connessione dalla barra dei menu di Mac, mi rassegno a fare partire l’inefficiente programma in dotazione alla chiavetta.

Che non parte. Preferenze? Qualche problema nel sistema? Di fatto, appare lo splash screen e se ne sta lì, forse per sempre.

Non c’è problema: in casa è arrivato da ieri un nuovo MacBook Pro. Attacco la chiavetta lì. Purtroppo il programma inefficiente vuole Java e, per usarlo su Lion, devo scaricare da Internet il runtime Java, non più di serie. Collegamento a Internet che al momento, essendo il mio obiettivo, è spiacevole come prerequisito.

FastWeb è sempre spenta. Beh, qualcosa di quello che mi serve potrei farlo con iPad 3G. Accendo e il collegamento appare attivo, ma non passa un bit che sia uno. Con Vodafone su iPad mi è successo un paio di volte. Può accadere.

Provo, riprovo, spengo, riaccendo, smoccolo, ma niente da fare. Mi resterebbe da accendere iPhone e usarlo come hotspot, ma il mio tempo è scaduto e il mio umore lo ha seguito. Vado a letto. La mattina dopo, cambiata la congiunzione astrale, mi alzo e tutto funziona come se non si fosse mai interrotto.

Prima considerazione: per riuscire a stare fuori da Internet, al giorno d’oggi, sono necessarie combinazioni veramente straordinarie.

Seconda considerazione: un malfunzionamento dell’acquedotto sarebbe stato preso con più serenità.

