---
title: "L'Innovazione"
date: 2013-04-15
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Nostalgia delle vecchie polemiche su iPad mini che non è innovativo perché i tablet da sette pollici sono arrivati prima e iPad mini è la stessa cosa.

(Tant’è vero che, essendo iPad mini da 7,9” invece che da 7”, faceva una differenza tale da fare presentare a Samsung un tablet da 8” poche settimane dopo iPad mini, pur avendo l’azienda coreana una nutrita offerta di modelli da 7”).

Guardiamo a dove c’è l’innovazione vera, certificata dal Wall Street Journal:

Una fonte familiare con i prodotti Microsoft ha dichiarato che le tavolette da sette pollici non facevano parte della strategia aziendale lo scorso anno, ma i dirigenti Microsoft hanno capito che avevano bisogno di una risposta alla popolarità crescente di tavolette più piccole come Nexus 7 della scorsa estate e iPad mini lo scorso ottobre.

Lo hanno capito e intanto a oggi della loro tavoletta più piccola non si vede traccia.

L’innovazione.

