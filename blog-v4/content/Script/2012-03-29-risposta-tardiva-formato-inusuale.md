---
title: "Risposta tardiva, formato inusuale"
date: 2012-03-29
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Tempo fa un’amica di Manuel chiedeva consigli su quale word processor usare sul lavoro. Ne uscì un post molto lungo e si spera vagamente utile, con numerose indicazioni.

L’amica, scriveva Manuel, consegna prevalentemente file Pdf.

Se la tematica fosse emersa oggi avrei potuto includere nell’elenco Pagesmith, programma che risponde parzialmente a una domanda di tanti anni fa: perché nessuno scrive un word processor in PostScript, che è il linguaggio poi usato dalle stampanti per rappresentare la pagina?

Beh, Pagesmith usa come formato nativo Pdf.

Significa che nessuno può modificare un documento Pagesmith senza avere una copia del programma. Al tempo stesso, precisano gli autori, Pagesmith non apre documenti Pdf di altri. Li crea e basta.

Oltre alla scelta inusuale del formato sono state adottate altre scelte non convenzionali. Tutti i comandi stanno fuori dall’area del testo, niente finestre di dialogo o galleggianti a togliere concentrazione; categorizzazione automatica dei font per scegliere presto e bene quelli che servono; accento sull’uso degli stili (un documento complesso senza stili è un documento che c’è voluto troppo tempo per redigere) e funzioni tipografiche avanzate sempre a portata di mano.

Appare chiaro che Pagesmith sia fatto per esigenze precise, specifiche e limitate. Chi sia abituato a criticare un programma per quello che non sa fare, lo potrà seppellire. D’altro canto, è matematica la sicurezza che quanto vediamo a schermo corrisponderà in tutto e per tutto a quello che uscirà dalla stampante.

Questo però non è il tempo dei programmi tuttofare. È il tempo delle soluzioni puntuali a bisogni particolari. E Pagesmith, 31,99 euro, troverà estimatori.

Sul sito di Fortunate Bear è anche disponibile la versione prova.

