---
title: "Lezione di design"
date: 2012-04-16
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Le domande sono finte, inventate da me. Le risposte sono di Jonathan Ive, il designer capo di Apple… ad altre domande, aventi nulla a che vedere con le mie.

Il resto dell’intervista a Ive, comprendente molte altre domande e diversi punti da notare, sta su This Is London.

Finta domanda: Come avviare un’iniziativa di successo nel mondo della tecnologia?

Jonathan Ive: Quello che amo [della California] è un ottimismo rimarchevole e l’attitudine verso la messa in pratica e l’esplorazione delle idee, senza la paura di fallire. C’è un senso semplice e pratico in un paio di persone che hanno un’idea e aprono una società per realizzarla. […] Non c’è il senso di cercare di fare soldi, è questione di avere un’idea e realizzarla.

Domanda: Che cosa rispondi alle critiche sul design dei prodotti Apple?

Ive: [Per essere un grande designer] è molto importante procedere con leggerezza, essere inquisitivi e interessati ad avere torto. […] Occorrono focalizzazione assoluta e l’entusiasmo di approfondire un contesto, capire che cosa è importante, anzi, terribilmente importante. Tutto questo riguarda le contraddizioni in cui ci si trova a operare.

Domanda: Come mai Apple non ha prodotto un netbook, seguendo la moda del momento?

Ive: I nostri obiettivi sono molto semplici: progettare e costruire prodotti migliori. Se non riusciamo a fare qualcosa di meglio, non lo facciamo.

Domanda: Perché ogni anno appare un modello di iPhone, a parte la memoria o il colore, e non invece dodici o tredici diversi in dimensioni, funzioni, interfaccia, batteria eccetera, come fanno altri costruttori?

Ive: La maggior parte dei nostri concorrenti mira a fare qualcosa di diverso o desidera apparire nuova; penso che siano obiettivi completamente sbagliati. […] Apparire diversi non è questione di prezzo, di data di uscita o di strani risultati di marketing: questi sono obiettivi aziendali, con scarso riguardo per le persone che usano il prodotto.

Domanda (vera): Quando arrivi con una nuova idea di prodotto come iPod, cerchi di risolvere un problema?

Ive: […] È un approccio molto pragmatico, quello con le sfide minori. È più difficile essere intrigati da una opportunità. […] Non è un problema di cui sei consapevole, nessuno ha formulato un’esigenza. Ma cominci a farti domande: e se facciamo così?, se lo uniamo a questo, potrebbe essere utile? Ciò crea opportunità che potrebbero sostituire intere categorie di oggetti, invece che rispondere tatticamente a un singolo problema.

Domanda: Ma se molte persone scrivono che manca questo o dovrebbe esserci quell’altro, come possono avere torto?

Ive: È sleale chiedere di progettare a persone che non hanno un senso delle opportunità del domani a partire dal contesto di oggi.

Domanda: Quando arriva un concorrente che ha la stessa lista di componenti e costa un po’ meno, come si fa?

Ive: Negli ultimi vent’anni abbiamo imparato che spesso le persone non sanno spiegare perché qualcosa piace, ma come consumatori sono incredibilmente capaci di discernimento e distinguono tra prodotti il cui design è stato grandemente curato e altri prodotti, che contengono solo cinismo e avidità. […] Credo che la connessione emotiva sviluppata verso i nostri prodotti sia dovuta all’avvertire la cura che ci mettiamo e alla quantità di lavoro che è occorsa per crearli.

