---
title: "Citazione citabile"
date: 2012-03-04
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Se citassi una celebre frase di Michael Dell a fronte del fatto che a oggi vale sedici volte quell’azienda in capitalizzazione di mercato (quotazione moltiplicato numero di azioni), verrei rimproverato di ripeterla per l’ennesima volta.

Così ne cito un’altra, pronunciata questa settimana: Non siamo più un’azienda di computer, ma un’azienda di tecnologie informatiche.

Inteso che Dell resta un’azienda con oltre trenta miliardi di dollari di capitalizzazione di mercato, che saremmo ben fortunati ad avere in Italia a generare lavoro e ricchezza, fare i bulli vendendo computer non funziona più.

Apple attualmente vale più del sistema di superstrade americano. Ma probabilmente è più chiaro scrivere che vale più del Belgio. O della Polonia. O dell’Arabia Saudita.

Il Belgio è molto bello, ma un pensiero a scambiarlo con Apple nell’Eurozona io lo farei.

