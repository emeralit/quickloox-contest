---
title: "La patente dell'inettitudine"
date: 2013-04-22
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Esiste iPatente e potrebbe essere una buona cosa, una app per iPhone del Ministero delle infrastrutture e dei trasporti, gratuita, che offre tutta una serie di servizi interessanti.

Se viene inserito il numero della patente, si accede a servizi ancora più validi. A patto di registrarsi prima sul Portale dell’Automobilista.

La app, inspiegabilmente, non lo fa. Per la registrazione si viene buttati su Safari. Sarebbe niente, se non fosse che il sito non è ottimizzato per iPhone.

Bisogna compilare una serie di campi e poi attendere di ricevere una password per posta. I campi sono preimpostati ciascuno con la propria tipologia di contenuto, ovvero nel campo Cognome è scritta la parola Cognome. Bisogna stare attenti o il tempo di compilazione raddoppia, perché ogni volta va cancellato il contenuto preimpostato allo scopo di inserire il proprio.

Nell’epoca di Unicode, si viene informati che le lettere accentate vanno inserite come lettera-più-apostrofo. Stile 1993.

Compilata la pagina e ricevuta la password, tocca compilare un’altra pagina, dove viene richiesto il triplo delle informazioni di prima.

Nel caso di una persona comune, al termine di questa odissea nell’incompetenza arriverà finalmente la possibilità di usare iPatente. Nel mio caso no; il nome Lucio non è pienamente soddisfacente per la generazione del codice fiscale, che richiede anche l’uso del secondo nome di battesimo.

Ma la mia patente, per motivi che datano al secolo scorso e di cui francamente non ho idea alcuna, è intestata a Lucio Bragagnolo, senza secondo nome.

Così, al termine della lunga compilazione, posso scegliere.

Se inserisco nome e cognome, la procedura si blocca perché il codice fiscale non quadra.

Se inserisco primo nome, secondo nome e cognome, in modo che il codice fiscale corrisponda, la procedura si blocca perché il numero della patente non corrisponde al titolare.

Si noti che la mia patente è perfettamente valida. Si noti che da decenni pago tutte le tasse relative e nessuno ha mai sollevato problemi, incassando serenamente.

Si noti che tutti, tutti, tutti i dati richiesti per l’iscrizione al Portale dell’Automobilista e l’uso completo di iPatente sono già in possesso del Ministero.

Di questi tempi la politica è irritante. Ma i politici di tanto in tanto, a spizzichi e bocconi, cambiano, se ne vanno, muoiono.

I burocrati di stato no. Stanno lì granitici. Nessuno li può mandare via, sono immortali e quand’anche non lo fossero e andassero in pensione, vengono sostituiti da gente ancora più inetta.

