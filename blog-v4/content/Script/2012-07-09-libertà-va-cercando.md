---
title: "Libertà Va Cercando"
date: 2012-07-09
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Torno sul tema del software libero e di App Store, perché nella discussione sul blog di Paolo Attivissimo siamo arrivati al commento numero 74, che definisce dato oggettivo la seguente informazione:

VLC è rilasciato interamente con una licenza free software; a causa dell’incompatibilità dell’EULA dell’Apple Store con la GPL, il programma è stato rimosso.

A volere essere sofisti, non c’è niente di oggettivo nell’incompatibilità della licenza software Gpl con Apple Store, un negozio dove si comprano computer e accessori; ma qui si va alla sostanza e possiamo lasciare perdere la confusione mentale altrui.

L’affermazione che il programma sia stato rimosso per incompatibilità della licenza con App Store è non verificabile e del tutto discutibile.

Le cose cono andate come spiega CreateDigitalMotion e riassumo qui sotto. I link necessari ad approfondire sono raggiungibili da questo.

Vlc è software open source è prodotto da un certo numero di sviluppatori volontari. Uno di questi, Rémi Denis-Courmont, non ha gradito che la software house Applidium pubblicasse una versione per iOS di Vlc.

Denis-Courmont, nonostante il disaccordo di molti altri sviluppatori del progetto compreso il responsabile generale, ha ritenuto che Applidium violasse la sua proprietà intellettuale e ha chiesto tanto ad Applidium di soprassedere, quanto ad Apple di togliere il programma dallo Store.

Applidium ritiene che la pubblicazione di Vlc su iOS fosse in accordo con la licenza Gpl versione 2 utilizzata (dire Gpl non ha assolutamente nulla di oggettivo: da versione 2 a versione 3, per dire, le cose cambiano).

Apple ha tolto la app dallo Store. Per incompatibilità? Non lo sappiamo. Non lo sa neanche Denis-Courmont. Intervistato, dichiara che magari Apple ha ritenuto ci fosse incompatibilità. O voleva evitare a priori una disputa, oppure aveva motivi commerciali o altro.

Nel contempo, come si è letto ieri, su App Store si trovano decine di app free software e anche qualcuna importante, il che suggerisce come parlare di oggettività sia fuori luogo. I fatti non lo autorizzano.

Ci si può chiedere perché Denis-Courmont se la sia presa a cuore personalmente. Altri programmatori come lui impegnati su Vlc non hanno condiviso la sua azione e hanno dovuto subirla. Azione in base alla quale chi non ha fatto in tempo a scaricare Vlc su iOS, non lo può avere. Non esattamente lo spirito del free software, checché dica eventualmente la lettera delle licenze (suggerimento: le licenze software si adeguano all’evoluzione dei tempi).

Denis-Courmont dichiara di avere iniziato a collaborare con Nokia nel 2005 per essere assunto nel gennaio 2007 (il mese sta nel suo profilo LinkedIn), dopo avere completato la tesi di laurea nei laboratori dell’azienda. Probabilmente avrà ricevuto un bonus di ingresso in opzioni azionarie, come accade molto spesso per posizioni come la sua in aziende come Nokia.

Al 15 gennaio 2007 le azioni Nokia valevano 20,13 dollari. il 29 ottobre 2008 39,71 dollari. Il possesso, poniamo, di mille azioni significava per Courmont passare da un gruzzoletto di ventimila dollari a uno di quasi quarantamila.

Poi iPhone ha iniziato a sconvolgere il mercato. Nokia è entrata in crisi nera. Mentre scrivo, il titolo vale 1,92 dollari. Il gruzzoletto ipotetico di Courmont è passato da quarantamila dollari a meno di duemila.

Un gesto di reazione contro Apple e contro iPhone è comprensibile e ipotizzabile. Solo un’ipotesi. Nessuno finora ha voluto, saputo, potuto escluderla, nemmeno – per quanto so – Denis-Courmont stesso. Peccato, se fosse una vicenda di vile denaro. Suonerebbe meglio la battaglia di libertà a difesa del software libero, ma lui – sempre a mia conoscenza – non lo ha fatto, limitandosi a inviare note burocratiche di violazione di copyright.

È possibile, non certo ma possibile, che Vlc sia assente da App Store per una venale ripicca personale.

Queste informazioni sono state inviate in forma più sintetica al blog di Paolo, solo che mentre scrivo non hanno passato il filtro della moderazione. Sono passate circa ventidue ore.

Certo, esiste la possibilità che i commenti si siano persi per strada nel mare di Internet, tutti i tre che ho inviato in ore diverse. Oppure che lui non abbia avuto tempo per quei commenti, anche se nel frattempo ne sono apparsi altri.

(Il titolo del post cita la Divina Commedia, ampiamente disponibile sul quel terminale “stupido” e lucchettato di iPad).

