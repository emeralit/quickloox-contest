---
title: "I dannosi e le beffe"
date: 2012-04-22
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---

Sta diventando di moda affermare tra il caffè e l’ammazzacaffè che Apple sì, però, non può continuare a crescere a questo ritmo perché è troppo.

Il che potrebbe essere assolutamente vero. Dopotutto si tratta della più grande azienda di elettronica di consumo al mondo. Crescere continuativamente a doppia cifra significa che il giro di affari, in termini assoluti, deve moltiplicarsi a livelli mostruosi. Esistono barriere materiali e organizzative che impegnano ben più di certa concorrenza.

Ma potrebbe anche non esserlo. Nei suoi mercati, Mac occupa una cifra singola, iPhone – contando l’intero mondo cellulare – pure. iPad è dominante ma quel mercato si è appena aperto; nella televisione, l’hobby Apple Tv potrebbe diventare una cosa seria e allora chissà; imprese come iTunes Store e App Store sono state inventate dal puro nulla e sono diventate colossi e dunque niente vieta in principio che altre invenzioni possano portare ulteriori progressi.  
Ma qualcuno ha addirittura invocato una interpretazione fantasiosa della legge dei grandi numeri. Il fatto che il crescere del numero di osservazioni porti il risultato di scelte casuali a convergere verso la media è stato trasformato nell’idea che un’azienda non può crescere più di tanto più delle altre, o che se cresce “troppo” a un certo punto deve smettere, così, quasi per magia.

Il punto non era contraddire una idiozia, quanto poterlo fare con arguzia. Farsi beffe degli stupidi, gli esseri più nocivi al mondo, è assai più vincente che contraddire le stupidità.

Finalmente qualcuno ce l’ha fatta, su Confounded by Confounding: oltre a spiegare nuovamente la legge dei grandi numeri, l’autore del pezzo scrive:

Vorrei anche notare che anni fa, quando Michael Dell auspicava la chiusura di Apple e la restituzione dei soldi agli azionisti, e il titolo aveva un valore di due cifre basse invece che tre cifre medio-alte come adesso, nessuno diceva “la legge dei grandi numeri finirà per fare ricrescere Apple”!

Apple non riprese a crescere per la legge dei grandi numeri, ma per una strategia vincente tradotta in prodotti eccezionali. Se smetterà di crescere sarà per difetto di strategia e di prodotti, non in virtù di equazioni.

Gli stupidi resteranno tali. Ma riderne darà più soddisfazione che non il semplice aver ragione.
