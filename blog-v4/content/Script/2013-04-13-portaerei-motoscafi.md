---
title: "Portaerei, Motoscafi"
date: 2013-04-13
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Settimana di lavoro intenso ma soprattutto di viaggi e impegni esterni, ovvero ancora più lavoro, perché occorre produrre nei tempi morti, negli intervalli, sul treno.

Settimana in cui ho usato per più tempo iPad che Mac.

La cosa più difficile, paradosso, è stata bloggare. Le app specializzate funzionano sempre meglio solo che, specie quando i blog sono numerosi e tutti diversi, i casi particolari si moltiplicano.

Sempre più spesso tendo a lavorare con Textastic in Markdown oppure Html, per poi inviare il risultato dove serve. Cambiano le modalità di elaborazione – c’è più authoring all’inizio della catena invece che alla fine – e la qualità è quella abituale, alta o bassa che fosse.

Non sono un pioniere. Horace Dediu di Asymco pubblica poco tempo dopo il terzo compleanno di iPad le stime di vendita di Gartner dei computer per il primo trimestre 2013. Se sono centrate, le vendite di computer subiscono una consistente flessione (con l’eccezione, forse, di Mac; una stima di Idc li vede in flessione – minore della media – mentre Gartner addirittura li indica con il segno positivo). Le vendite di computer a tavoletta invece aumenterebbero in modo sostanziale e, tre anni e centinaia di concorrenti dopo, da iPad non si prescinde.

John Gruber commenta che non si comprano più come prima, i camion; le auto sono sempre più diffuse.

Lo aveva detto Steve Jobs. Computer come i camion della prima motorizzazione, tavolette come le auto che in modo crescente hanno preso il posto dei mezzi tuttofare.

Nel mio piccolo cambierò metafora, appena un poco: anche oggi ho lasciato alla fonda la portaerei Mac e sono in giro con l’agile motoscafo iPad.

