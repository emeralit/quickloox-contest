---
title: "Mancanze di stile"
date: 2012-03-28
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Jolie O’Dell non ha gradito l’evento di lancio del nuovo iPad e lo ha motivato su VentureBeat. A suo modo di vedere, ha mostrato la sdrucitura del marchio Apple e ha reso evidente una certa trascuratezza altrimenti assente dagli eventi precedenti, quando c’era Steve Jobs.

Il pessimo gioco di parole che presenta il nuovo iPad come Risoluzionario, la goffaggine del logo rielaborato, il bizzarro nome di prodotto — tutto puntava a una leadershipt che non capisce o non si cura più della coerenza nella propria iconografia.

Roba grossa. Quando c’era Steve, caro lei, tutto filava. Adesso le cose stanno già andando in vacca. Dopotutto la coda della gente che non aspetta altro si è fatta chilometrica. E durante la precedente assenza di Jobs da Apple è stato sbagliato tutto, no? Quindi c’è speranza. Se abbastanza gente si convince che adesso non è più come prima, forse si riesce a provocare del danno e da cosa nasce cosa.

È già. Come ha notato brillantemente Matt Thomas, Steve Jobs non avrebbe avuto la rozzezza di chiamare un prodotto il nuovo qualchecosa.

Mai si sarebbe abbassato a promuovere una caratteristica chiave di prodotto con uno sciocco gioco di parole.

Non parliamo neanche dell’ipotesi di corrompere la sacra purezza del marchio aziendale con cromatismi, effetti luminosi, solarizzazioni, retinature. Magari preparate da qualche cane sciolto come quelli che si trovano su siti depravati come DeviantArt.

Prima che Apple manchi di stile, qualcuno manca di memoria.
