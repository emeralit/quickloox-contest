---
title: "Apro, Quindi Video"
date: 2012-04-21
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Le coincidenze non sono determinate da un Destino invisibile, ma costruiscono ugualmente tematiche che finiscono per diventare rilevanti.

Così nel giro di ventiquattr’ore mi sono ritrovato in mano due racconti di prima mano sull’apertura dello schermo di iMac allo scopo di levare polvere che riesce a depositarsi all’interno, talvolta tra vetro esterno e pannello video interno, talvolta tra pannello Led (retroilluminazione) e pannello Lcd.

Il primo è dell’amico Paolo, che ha scelto la strada radicale: smontaggio integrale, tre ore e mezza, meglio se aiutati da un assistente nei momenti cruciali di montaggio e smontaggio.

Il secondo l’ho ritrovato preparando il secondo numero di Macworld: due minuti e quarantadue di asportazione video del vetro anteriore e relativa pulizia, grazie a una ventosa strategica.

In sé non c’è niente di veramente speciale nel trovare informazioni di fai-da-te. Viceversa, se fino all’altroieri qualcuno mi avesse accennato alla possibilità di aprire lo schermo di iMac, avrei avuto un brivido. Adesso mi rendo conto che è solo questione di apertura mentale. E cauta, riferita allo schermo.

