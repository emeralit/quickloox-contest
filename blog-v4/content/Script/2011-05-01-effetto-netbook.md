---
title: "Effetto netbook"
date: 2011-05-01
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Microsoft ha registrato nel primo trimestre del 2010 risultati finanziari buoni, ma sull’utile netto l’azienda è stata battuta per la prima volta di sempre da Apple, che nei propri risultati finanziari l’ha staccata di circa un miliardo di dollari su cinque.

L’utile netto ha una certa valenza perché il business di Microsoft è in gran parte del tipo cash cow, prodotti cui mungere molto denaro tramite clienti pressoché automatici senza chiedere sforzi clamorosi, tipo Office o Windows.

Microsoft ha associato una leggera diminuzione dei fatturati provenienti da Windows a una flessione del quaranta percento nella vendita globale dei netbook.

Bei tempi quando Apple, in pieno boom dei netbook, spiegava che erano apparecchi intrinsecamente limitati e che ovviamente l’azienda si sarebbe tenuta aperta ogni opzione, ma il percorso dei netbook non pareva interessante. E i siti spiegavano come dovesse affrettarsi a entrare con la propria innovazione in quel mercato. L’idea di innovare arrivando per ultima non era ovviamente delle più felici.

A marzo 2009 Preston Gralla di Computerworld spiegava dottamente come Apple avrebbe avuto bisogno di lanciare un netbook. L’azienda, scrisse Gralla, sembra ignorare l’economia in crollo attorno a essa e continua a credere di poter prosperare vendendo computer prezzati come champagne in un mondo prezzato come birra.

Chi ha puntato sui netbook inizia a perderci è l’effetto è talmente profondo che se ne è accorta nel borsellino persino Microsoft.

