---
title: "Non è un paese per nostalgici"
date: 2012-04-07
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Lo storico informatico David Greelish propone che il nuovo futuro campus Apple a Cupertino contenga una zona adibita a museo. E io sono pochissimo d’accordo, anzi, direi niente.

Apple non è un’azienda come le altre. Non ha paura del cambiamento, bensì della stagnazione. È il ciclo della vita che fa capire a chiunque come si possa continuare a guardare avanti solamente facendo bene i conti con quello che sta indietro. E proseguendo nel cammino.

Di aziende legate al passato ce ne sono già mille altre e l’unica che, per dirla in modo paradossale, è legata al presente ce la dobbiamo tenere com’è, se necessario con i suoi eccessi, perché se si mette a guardare il passato anche lei ci perdiamo tutti.

Si badi bene, adoro il retrocomputing. Vorrei poter visitare la raccolta di memorabilia Apple donata all’Università di Stanford nel 1997 e conservata in un luogo segreto. Sono sommamente orgoglioso della mia qualifica di socio onorario di All About Apple e considero il loro lavoro un capolavoro. Guai a chi tocca il mio MessagePad 2100 Newton o il mio Ql Sinclair.

Detto ciò, del passato devono occuparsi le istituzioni, la cultura, il popolo, il tempo, quest’ultimo impagabile nel cestinare senza patemi ciò che non vale la pena di essere ricordato.

L’azienda che reinventa la tecnologia e ogni volta ci spinge un passo più avanti, a volte recalcitranti, a volte non proprio nel modo migliore, ma comunque avanti, che lavori a questo, non a spolverare l’Apple I prima che entri la comitiva a fotografarlo.

