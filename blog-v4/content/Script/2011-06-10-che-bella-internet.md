---
title: "Che bella internet"
date: 2011-06-10
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Da amante dei lavori di Maurits Cornelis Escher ho avuto piacere di scoprire un font (gratuito per uso personale) intitolato Hommage à Escher.

Da questo, visto che amo anche Tolkien e il genere fantasy, mi ha fatto piacere arrivare al font Rivendell, dello stesso autore.

Per scoprire che li ha creati grazie a FontStruct, un editor di font online che basta avere un browser per utilizzare.

E che esiste un programma chiamato Celtic Knot Thingy per la creazione di nodi celtici.

Quale biblioteca, centro di sapere, consesso di saggi mi avrebbe permesso di viaggiare così tanto in così poco?

Senza contare che Celtic Knot Thingy dovrebbe funzionare anche su Mac… ma gli autori non garantiscono. Ho anche qualcosa da imparare per la sera.

Che bella Internet.

