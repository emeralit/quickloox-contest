---
title: "La lotteria del rigore"
date: 2012-02-14
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Il 16 febbraio terminerà la copertura di AppleCare sul mio MacBook Pro 17”. Per coincidenza proprio pochi giorni fa la batteria ha iniziato a chiedere assistenza. Ho programmato una visita all’Apple Store di Carugate, dove nel giro di una quindicina di minuti mi sono ritrovato con una nuova batteria (non necessariamente una batteria nuova), che uso sporadicamente da quando c’è iPad, ma comunque non guasta.

Alla fine i 349 euro di AppleCare sono simbolicamente rientrati per 179 euro, tanto mi sarebbe costata la batteria in assenza di garanzia. Mi è convenuto? Decisamente sì. Vero che ho sostanzialmente pagato il Mac 170 euro più del prezzo di acquisto dell’hardware, ma si tratta della mia macchina da lavoro e se fosse successo qualcosa, poniamo, alla scheda logica o allo schermo, la riparazione in garanzia con protezione di tre anni sarebbe diventata istantaneamente di convenienza totale. Quando cambierò Mac, stipulerò certamente un altro piano AppleCare.

Intanto, il mio possesso di iPad arriverà a due anni. Fisicamente parlando, si tratta del terzo iPad: ho avuto due sostituzioni a seguito di un tentativo catastrofico di installare una beta di iOS 5. L’iPad che ho in mano, secondo Chipmunk, è stato costruito a luglio 2011 e quindi è molto giovane. Il mio acquisto originale data a maggio 2010 e, se avessi stipulato un accordo AppleCare, questo scadrebbe nel giro di tre mesi.

È estremamente probabile che a maggio il mio iPad sarà ancora vivo e vegeto. Per come sono andate le cose, è probabile che lo sarebbe stato anche il modello originale, fatti salvi i miei tentativi di giocare al piccolo sviluppatore. Quando cambierò iPad, certamente non acquisterò AppleCare.

Ecco perché la domanda sulla convenienza di AppleCare, che ricevo sovente, non ha una risposta assoluta. Dipende da quello che si fa, dall’importanza che ha, su quale apparecchio. Come tutte le forme di assicurazione è una lotteria, meglio, una scommessa contro il destino avverso. Quando riguarda Mac mi sento rigorosamente portato a proteggermi e quando concerne iOS, invece, no.

