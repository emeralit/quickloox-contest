---
title: Alla greca"
date: 2012-05-24
draft: false
toc: false
comments: true
categories: [ ]
tags: ["script"]
---

Il debito pubblico greco era di 270 miliardi di dollari ed è stato rinegoziato a 130 miliardi di dollari. Sempre una cifra spaventosa, ma meno della metà. In fretta. A partire da una situazione di circa centomila detentori di titoli sparsi per il globo. Normalmente si tratta di vicende molto lente e molto complesse, per motivi facilmente comprensibili.

Lo ha fatto per conto del governo greco il gruppo Bondholder Communications. Lo strumento: cento iPad equipaggiati con una app su misura, che condivideva in sicurezza e in tempo reale su tutto il team di lavoro l’andamento delle trattative, il valore corrente del debito e tutti gli altri dati necessari. Il team comprendeva il Ministero delle finanze, la Banca di Grecia, l’ente borsistico ellenico, Deutsche Bank, Hsbc e Lazard.

Racconta Bob Apfel, direttore del gruppo: Guardavo centinaia di milioni di titoli riallocarsi mentre i ragazzi correvano nei corridoi, nel giro di mezzo secondo si prendevano decisioni che non avrebbero potuto essere prese senza la piattaforma dati.

Sempre secondo Apfel, si tratta della transazione finanziaria più grande della storia e non avrebbe potuto essere portata a termine in questo tempo e con questa efficacia senza iPad.

Dopo di che possiamo ancora pensare che si tratti di giocattoli. E mettere il cervello in insalata.
