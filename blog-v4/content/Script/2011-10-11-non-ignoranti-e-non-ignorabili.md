---
title: "Non ignoranti e non ignorabili"
date: 2011-10-11
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Ken Segall, creativo americano che ha avuto la ventura di conoscere e lavorare con Steve Jobs, ha commentato uno dei tanti articoli tesi a sottovalutare l’impatto di Jobs sul mondo della tecnologia.

All’argomentazione che Jobs abbia semplicemente creato apparecchi molto venduti e per il resto non abbia fatto niente di speciale, magari per ridurre la povertà o scoprire farmaci salvavita, Segall risponde:

Le rivoluzioni di Steve hanno fatto questo e altro. Steve è la persona che ha mostrato una via migliore agli occhi dei costruttori di computer. I suoi apparecchi stanno trasformando la medicina e l’apprendimento. Le sue invenzioni – e le numerose copie delle sue invenzioni – hanno aiutato le persone a sollevarsi contro coloro che le hanno lungamente private della propria libertà. Hanno permesso a molti di intraprendere carriere che prima non sarebbero state possibili.

Ho visto argomentare che, se diamo a Steve questo tipo di riconoscimento, allora dovremmo darlo anche a ExxonMobil. Se non fosse per il loro carburante, i mezzi di soccorso non potrebbero raggiungere le zone disastrate.

Non proprio. La differenza è che Steve ha visto la potenza della strada tecnologica quando era ancora agli inizi. Il miraggio dei personal computer era che permettevano a persone ordinarie di fare cose straordinarie. È vero che nessuno, Steve compreso, poteva predire esattamente che cosa avrebbe potuto essere raggiunto o inventato usando i computer. Ma certamente sapeva che questo tipo di tecnologia aveva il potere di cambiare il mondo. Mettere il potere nelle mani degli individui era la sua passione.

Perché tanto bisogno di sminuire la figura di Jobs anche oltre l’obiettività e il giusto equilibrio delle valutazioni? Per un motivo semplice, spiega Segall, descritto verso la fine del famoso spot Think Different:

Potete citarli, essere in disaccordo con loro; potete glorificarli o denigrarli ma l’unica cosa che non potrete mai fare è ignorarli, perché riescono a cambiare le cose, perché fanno progredire l’umanità.

I denigratori sono persone sfortunate. Vorrebbero poter ignorare ma non possono, perché Jobs ha cambiato anche il loro mondo, dunque denigrano per mancanza di alternative.

