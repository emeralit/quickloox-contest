---
title: "Che palloni"
date: 2013-01-18
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Quando la biglietteria automatica del parcheggio del calcetto, pilotata da Windows, informa che il computer potrebbe essere a rischio. Di influenza, se piove.
Del resto, come riferisce Mario, oramai i bambini sanno che ci vuole l’antivirus prima ancora di uscire di casa da soli. E qualcuno dovrà pure avere una responsabilità per questo squallore.

