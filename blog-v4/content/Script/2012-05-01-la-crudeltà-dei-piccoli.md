---
title: "La crudeltà dei piccoli"
date: 2012-05-01
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Da iMore:

Gli Apple Store hanno sostituito con iPad gli iMac tradizionalmente in servizio presso il “tavolo dei bambini”. I tavoli, che prima erano equipaggiati con un quartetto di iMac, ora presentano invece quattro iPad.

Come gli iMac precedenti, configurati con giochi e programmi educativi, gli iPad contengono varie app per bambini. Non sono esattamente sicuro di quando sia accaduto. […] Mio nipote [semplifico: il testo parla di godson, bambino di cui la voce narrante è stato padrino al battesimo] di tre anni, che era con me, ha impiegato solo un paio di istanti ad adattarsi. Ha lanciato una app-libro e si è messo a giocare, esattamente come fa a casa. È stato molto più facile per lui trovare una app e usare iPad di quanto sia mai stato maneggiare iMac e il suo mouse.

Ora, sappiamo che Apple è cattiva. Intenzionata dal primo momento a uccidere Mac per sostituirlo con gli iCosi, perché pensa solo al denaro. Mica per niente, tra gennaio e marzo di quest’anno, ha venduto più Mac di quanti ne abbia mai venduti in qualsiasi altro periodo equivalente, dalla nascita di Mac a oggi.

Non è questo che mi preoccupa. Sarà capitato più o meno a tutti di avere a che fare con un bimbo di tre anni cui viene sottratto un giocattolo, un pezzo di cibo, un genitore (apparentemente). Ci vogliono pazienza, destrezza, creatività o si rischia quanto meno un pianto.

A questo moccioso levano iMac e mettono al suo posto iPad. E lui non fa una piega, neanche della boccuccia. Insensibile, cinico, freddo, privo di qualsiasi emotività.

È già triste vedere Apple che assassina il Mac così, senza pietà, infrangendo un record di vendite dopo l’altro. L’anima insensibile di questi bambini anaffettivi e senza cuore, che non battono ciglio di fronte alla scomparsa dell’adorato computer sostituito da una rapace e consumistica tavoletta, è però il colpo peggiore.

