---
title: "Controspionaggio digitale"
date: 2012-02-15
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Storia impressionante del New York Times sulla sicurezza digitale di chi viaggia in Cina e in Russia.

Lo spionaggio digitale in questi Paesi, dovuto alla dittatura che si insinua ovunque oppure al vuoto di potere che favorisce il proliferare di gang tecnologiche, pare imperversi. Oggetti del desiderio ancora più appetitosi e delicati delle classiche carte di credito sono segreti industriali, segreti di Stato e gli stessi computer dei viaggiatori. Se un occidentale riportasse in patria e in azienda il proprio apparecchio e lo collegasse alla rete aziendale, senza sapere che la sicurezza dell’apparecchio stesso è compromessa, diventerebbe una quinta colonna involontaria e potenzialmente preziosa.

Così, racconta l’articolo, politici e uomini di affari americani che si recano in Asia ricorrono a misure di sicurezza draconiane. Nel 2010 la Camera di commercio degli Stati Uniti sarebbe stata vittima di furto di dati e di informazioni per circa un mese e mezzo, a causa di funzionari dell’ente che si sono recati in Cina e sono tornati con apparecchi compromessi a loro insaputa, che inseriti nella rete interna hanno cominciato a “lavorare”. Anche dopo la bonifica è stato scoperto un termostato che comunicava con un indirizzo Ip cinese. E una stampante ha iniziato di punto in bianco, spontaneamente, a stampare documenti scritti in mandarino.

L’articolo del Times esordisce con la storia di Kenneth G. Lieberthal, sinologo di Brookings Institution, e delle misure di sicurezza da lui adottate.

Lieberthal lascia a casa portatile e telefono propri e passa la frontiera con hardware preso apposta, che viene reinstallato da zero prima di partire e reinstallato da zero al ritorno. Mentre è in Cina tiene spenti Bluetooth e Wi-Fi, tiene sempre d’occhio il telefono (questo sembra normale) e, durante gli incontri, non solo spegne il telefono ma leva anche la batteria, nel timore che in qualche modo gli possano accendere il microfono a distanza. Si collega a Internet solo tramite un canale cifrato e difeso da password (normale anche questo) e porta con sé una chiavetta Usb dalla quale copia e incolla le password sul computer, per evitare che software di keylogging installato clandestinamente dai cinesi sul computer possa possa carpire la password nel momento in cui la digita.

Probabilmente sottovaluto il problema. Però non posso fare a meno di chiedermi perché non vada in Cina con un iPhone e un iPad. A una analisi superficiale, metà dei suoi problemi è risolta in partenza e l’altra si semplifica.

