---
title: "Riassumendo"
date: 2012-06-20
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Il tema dell’inserimento di iPad nella scuola è destinato a permanere per un certo tempo. Senza neanche troppe ripetizioni.

Emeralit mi segnala per esempio il caso di Citia: What Technology Wants by Kevin Kelly, 7,99 euro per iPad.

Citia, come spiega una recensione di ReadWriteWeb, è sostanzialmente un’edizione condensata di un libro più grande, che non ne sostituisce affatto la consultazione e invece schematizza il pensiero e le linee logiche su cui si muove l’autore, in forma grafica.

La recensione tira in ballo perfino HyperCard per chiarire il concetto del funzionamento della app. Può darsi che il concetto sia valido oppure no, però c’è spazio per riflettere e certamente basta guardare la recensione per capire che il lavoro svolto per spiegare e riassumere, non solo testuale ma anche grafico e logico, ha un potenziale.

La riflessione è questa: le antologie scolastiche. Raccolgono e riassumono testi importanti di autori importanti. Non ne sostituiscono la consultazione. Quanto funzionano per schematizzare e rendere le linee di pensiero degli autori? Forse un supporto digitale potrebbe trasformare una antologia scolastica in qualcosa di più?

Peggio ancora: fermandosi ai primi del Novecento, quanto occorre a un insegnante di buona volontà per mettere insieme una antologia ad hoc, semplicemente recuperando i testi originali su Internet (gratis) e cucendo opportunamente il tutto mediante ipertesto?

È una provocazione, ovvio. C’è il discorso delle note a margine, i diritti di autore che affliggono praticamente tutta la produzione novecentesca, ci vuole tempo, ci vuole fatica.

Eppure, prima del digitale e prima di iPad, l’unico modo per farlo sarebbe stato raccogliere pagine sparse in una cartellina, duplicabile solo a prezzi da fotocopiatrice e non parliamo del tempo necessario. Vuol dire che qualcosa, se non si muove, inizia a poterlo fare.

