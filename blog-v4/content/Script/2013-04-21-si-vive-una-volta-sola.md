---
title: "Si vive una volta sola"
date: 2013-04-21
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Spero mi si scuserà se, dopo avere già accennato a un’ottima rassegna di giochi roguelike, gli accodo anche quella appena pubblicata da Cult of Mac.

Per vari motivi: primo, appaiono diversi titoli che non erano stati nominati. Secondo, si parla anche di iOS. Terzo, condivido buona parte dei giudizi espressi. Quarto, permadeath.

L’autore, John Brownlee, torna più volte su un punto che separa piuttosto nettamente questo genere dagli altri: quando il personaggio muore – salvo regolazioni opzionali o trucchi – è per sempre. Si ricomincia daccapo e magari significa avere perso il risultato di molte ore di gioco.

Qualcosa che ti fa giocare in modo molto diverso e che porta l’esperienza del gioco di ruolo più vicina appunto all’immedesimazione: quella chiocciola che appare sullo schermo – i roguelike oggi hanno spesso una modalità grafica, ma altrimenti sono fatti di caratteri alfanumerici – siamo noi, più che una distrazione vuota. Il gioco casual ha pieno diritto di cittadinanza, il gioro di ruolo usa e getta è insipido.

Non so se darei la palma del preferito a Dungeon Crawl Stone Soup come fa Brownlee. Però, al netto di qualche problema logistico, lo si può giocare via web e non è da tutti.

