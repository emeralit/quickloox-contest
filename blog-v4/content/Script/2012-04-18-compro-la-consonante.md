---
title: "Compro la consonante"
date: 2012-04-18
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Per completare la trilogia su Jonathan Ive: ma come si fa a pensare di avere in casa il progettista di iMac, MacBook Pro, Mac mini, AirPort Base Station, iPhone, iPad, Apple Tv… e affidare un progetto di design a Philippe Starck?

Che è un genio, beninteso. Ma in che rapporto sarebbe con Ive? Collaboratore? Rivale? Sostituto? Imitatore? Sputtanatore?

A qualunque persona con una normale irrorazione sanguigna del cervello la cosa è apparsa immediatamente pura – e scaltra – autopubblicità. Ad altri è apparsa una notizia da vendere.

Altri che, scopiazzando pure l’errore nell’indirizzo dell’articolo originale, lo hanno chiamato per un intero articolo Stark. Per risparmiare sulle consonanti, forse. O in omaggio alle Stark Industries possedute dal milionario che abita l’armatura di Iron Man. Chissà chi ne sarà il designer. Jonathan Ive?

