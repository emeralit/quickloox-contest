---
title: "Intelligenza e imitazione"
date: 2011-11-20
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Ci sono molti modi di commentare la notizia della divulgazione del protocollo di Siri, che in teoria permette a qualsiasi apparecchio di rivolgere una domanda all’intelligenza artificiale messa in piedi da Apple per iPhone 4S.

Nella pratica non succederà o sarà qualcosa di molto limitato nei numeri, un po’ come il jailbreak per apparecchi iOS che consente lo scaricamento di programmi non provenienti da App Store. L’unica altra alternativa è che Apple decida, un domani piuttosto lontano (2013, per capirci), di aprire Siri al mondo.

A me è venuto da pensare all’intelligenza e al lavoro necessari per realizzare un servizio come Siri, all’intelligenza e al lavoro assai meno intensivo che serve per aprire la scatola costruita da altri e all’intelligenza assolutamente limitata dei vorrei-ma-non-posso che si applicheranno (poco) per copiare iPhone 4S.

In fin dei conti, il cane che porta il giornale al proprietario ha una descrizione astratta assimilabile a quella di una rete industriale di distribuzione.

