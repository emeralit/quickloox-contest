---
title: "Uno su duecento milioni"
date: 2013-04-19
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Quando parlo dei progressi globali verso l’uso di Html5 al posto degli anacronistici plugin per browser vengo sempre ammonito sull’inadeguatezza dello standard per i giochi.

Non per tutti: RuneScape si trova in piena fase di collaudo della nuova versione con interfaccia grafica riveduta e motore grafico basato proprio su Html5.

Stavolta l’anacronismo non è Flash, ma Java. Il relativo plugin è piagato in continuazione da rilevanti problemi di sicurezza, al punto che i recenti aggiornamenti di Mountain Lion lo disattivano tout court e sta ai singoli, loro responsabilità, riaccenderlo.

RuneScape non si è mai potuto giocare senza Java, dalla sua nascita ai giorni nostri.

Ed è un peccato. La conversione a Html5 spalanca invece orizzonti da sogno, più che su desktop dove è scontato, su computer da tasca e tavolette, che questa versione Html5 non considererà, più per questioni di interfaccia che altro. Ma sai mai le prossime…

Avrei sempre voluto avere più tempo per considerare RuneScape. Meno spettacolare e aggressivo di altri prodotti, ma solido e longevo. Più che per la grafica o gli effetti speciali mi interessa per le migliaia di persone sempre online in qualunque momento. E adesso, se apro la pagina di RuneScape, la mancanza del plugin Java mi tiene comunque fuori (non mi sogno minimamente di attivarlo apposta, checché questo succeda per un numero di persone che presumo consistente).

(In diretta: mi è appena arrivato un aggiornamento di Safari che permette di abilitare il plugin Java su singoli siti. Il ragionamento cambia marginalmente).

Html5 provvederà come motore grafico ai bisogni di migliaia di persone in contemporanea. E se si pensa agli account registrati nel tempo su RuneScape, sono tanti, tanti. Ne ho uno (vecchio) anch’io.

