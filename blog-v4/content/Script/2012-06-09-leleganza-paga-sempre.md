---
title: "L'eleganza Paga Sempre"
date: 2012-06-09
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Zune è un grande investimento per noi, disse Bill Gates nel 2006, ed è una visione che ci porterà avanti per anni.

Apparentemente sei anni, visto che oggi Zune è ufficialmente terminato.

Mi aspetto un funerale di prima classe, come quello di iPhone celebrato nel 2010 dai dipendenti Microsoft.

Nella pagina ci sono un paio di filmati, di cui raccomando caldamente la visione.

