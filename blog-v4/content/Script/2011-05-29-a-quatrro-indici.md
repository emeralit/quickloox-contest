---
title: "A quatrro indici"
date: 2011-05-29
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
L’infaticabile e geniale Horace Dediu di Asymco ha deciso di risolvere il problema di come valutare l’andamento delle aziende che producono smartphone. Infatti, guardando un parametro al posto di un altro, per esempio la quota di venduto sul totale degli smartphone al posto della quota di venduto sul totale dei cellulari, si ottengono quadri diversi della situazione e farsi un’idea del panorama diventa complicato.

Così ha creato un indice composito, che tiene nota di quattro parametri nello stesso tempo.

Il grafico risultante è piuttosto indicativo.

Per altro in materia, trovo affascinante anche la rappresentazione del mercato in forma vettoriale, sempre per opera di Dediu.

