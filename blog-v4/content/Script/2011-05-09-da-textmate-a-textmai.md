---
title: "Da textmate a textmai"
date: 2011-05-09
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Sono notoriamente un intenso consumatore di BBEdit. Cionostante mi scoccia non vedere ancora all’orizzonte TextMate 2.

Questo perché la concorrenza fa bene a tutti e TextMate, quando è nato nel 2006, prometteva di portare novità interessanti per svecchiare positivamente il mondo degli editor di testo.

Il problema è che a maggio 2011 tutto quello che si sa di TextMate 2 è che l’autore ci sta lavorando. Parola sua, del… 14 giugno 2009.

Il tempo che è passato non sarebbe di per sé un problema, se non fosse che la pagina contiene affermazioni come la fine del lavoro è in vista, diciamo che il 90 dei moduli stanno arrivando a compimento e cose così. Cresce la sensazione che una versione 2 di TextMate potrebbe non arrivare mai, o metterci talmente tanto tempo che sarebbe abbastanza lo stesso.

Infatti gli altri non stanno fermi. Produrre qui una rassegna di tutte le alternative disponibili per l’editing di testo, la programmazione e lo sviluppo web è oramai impresa superiore a qualsiasi forza. Ed è una situazione fantastica, l’apoteosi della libertà di scelta.

Tra le ultime novità e le varianti più curiose in tema segnalo comunque la beta di Sublime Text 2, l’ancor meno pronto Vico, il creativo sistema di abbreviazioni di testo Zen Coding e l’enigmatico Chocolat (di cui deve ancora arrivare la beta).

Devo anche dire che, dovessi spostarmi da BBEdit, mi dirigerei con decisione verso l’ambito grafico-visivo, su programmi stile Coda; oppure compierei una scelta fortemente monastica, verso editor ancora più complessi e puri come emacs (dentro il Terminale), magari in una delle sue incarnazioni più Mac-like come Aquamacs.

