---
title: "Guadagnare su apple"
date: 2012-03-21
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Come avevo anticipato a Carolus, che aveva segnalato la cosa…

Mike Daisey è un uomo di spettacolo americano che da tempo si concentra sulle condizioni di lavoro nelle fabbriche cinesi. Recentemente ha terminato a New York le repliche di uno spettacolo intitolato L’agonia e l’estasi di Steve Jobs, che ha debuttato nel gennaio 2011.

L’opera getta appunto una luce cruda sul tema ed è stata presentata come il frutto delle visite in Cina di Daisey.

Senonché, dove gli servivano certi fatti o certe situazioni Daisey ha inventato.

Trattandosi di teatro e non di giornalismo non c’è nulla di male: solo uno sprovveduto va a vedere un film di Oliver Stone pensando che la verità sia veramente quella e che tutti siano stati censurati tranne il regista coraggioso.

Solo che Daisey avrebbe potuto spiegare onestamente le cose un anno fa. Invece ha fatto passare per autentici i suoi racconti fino a quando non è andato a esporli per radio a una trasmissione americana di una certa notorietà in patria. Che li ha verificati e li ha smontati, per poi chiederne conto a Daisey in una nuova puntata della trasmissione, dove quest’ultimo non ha fatto gran figura. Si può leggere un resoconto approssimato della puntata.

Sappiamo che per un anno Apple ha evitato di prendere posizione pubblica nei confronti della situazione. Sì, la Apple cattiva e censoria, che silenzia tutto quanto non risponde ai propri canoni.

Nei pochi giorni di rappresentazioni ancora in programma dopo l’episodio, Daisey ha tagliato dal proprio monologo le parti inventate o falsificate e ha portato a termine la tournée. Per il resto dell’anno, sono rimaste nel copione e sono state spacciate come vere.

Tutto questo non significa che gli operai cinesi vivano come operai italiani o americani. Quel Paese vive sotto dittatura da decenni e ha un ritardo globale spaventoso nei confronti del mondo civile. Difatti le condizioni dei lavoratori europei nel XIX secolo sono vagamente paragonabili, in rapporto al contesto generale, a quelle di oggi in Cina.

Né significa che il tema della produzione di massa della tecnologia e dei suoi risvolti umanitari non meriti attenzione.

Significa che ci si può arricchire sulla pelle degli operai cinesi in diversi modi, compreso il mentire in pubblico a scopo di denaro, successo e fama.

E che si può guadagnare su Apple in diversi modi, compreso sfruttarne la notorietà in modo invero poco etico, per un campione della denuncia sociale. Si possono fare molti soldi e molta strada, puntando il dito contro il nome sulla bocca di tutti e dicendo qualsiasi cosa, basta che sembri vera.

Passerai per imparziale, campione della libera espressione, voce fuori dal coro.

