---
title: "I semi della rivoluzione"
date: 2012-03-14
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
A Trento ho parlato di rivoluzione. Una rivoluzione che devono fare gli insegnanti, perché sono coloro che si confrontano con gli studenti (che rivoluzione è senza gli studenti in mezzo?) e perché, senza insegnanti, gli studenti finiscono in strada a lanciare slogan senza imparare niente, salvo nozioni improvvisate di socializzazione estemporanea e sopravvivenza ai lacrimogeni, non prioritarie rispetto ai bisogni della nazione e delle famiglie.

La tendenza a manifestare per le strade per protestare, invece che dedicare il tempo per lo studio a studiare, ha portato abbastanza generazioni di studenti a trasformarsi in analfabeti di ritorno, la cui ignoranza dell’italiano e del resto si abbina al disinteresse completo per l’apprendimento. Una piaga da combattere e eradicare.

Rivoluzione che deve partire da basso e in cui bisogna arrangiarsi. Chi aspetta i soldi dal Ministero, magari per comprare la carta igienica, non li vedrà mai e appassirà dentro la fortezza Bastiani del proprio istituto ad attendere Tartari che non arriveranno. Se mancano i soldi li si trovino, magari chiedendoli su Internet come fanno gli americani (latori di tanti difetti e che però insegnano sempre a rimboccarsi le maniche). Anche perché, come è successo in India nell’ambito di un esperimento del 2000, quando proprio mancava tutto, hanno murato computer completi di accesso a Internet in una zona-ghetto. E ragazzini analfabeti, mancanti di tutto, si affollavano spontaneamente davanti al computer, imparando. Disordinatamente, ma più di niente.

Per fare la rivoluzione serve fare rete. Gli avvocati fanno rete, i panettieri fanno rete, i notai fanno rete, gli insegnanti invece di fare rete si sentono soli e trascurati, dal Ministero prima di tutto. Devono fare rete tra loro e con i loro studenti, e con di questi ultimi le famiglie. Legge di Metcalfe: la potenza di una rete cresce con il quadrato dei nodi.

Dalla potenza della rete possono nascere cose come Wikipedia (che a scuola si dovrebbe scrivere, non leggere) o come la Khan Academy, contenente oltre tremila video didattici per imparare un sacco di cose, liberamente accessibili a chiunque.

Serve anche il software libero, per la rivoluzione. Il software libero è liberamente modificabile e distribuibile, costa zero, permette di contribuire allo sviluppo tecnologico e di coinvolgere gli studenti in progetti costruttivi, offre le stesse possibilità del software a pagamento. A scuola non si deve imparare Word, ma a scrivere bene quale che sia lo strumento. E magari capire LaTeX invece che il .doc.

E magari capire Html, invece che .doc. Html è leggero, completo, standard, universale, offre una splendida opportunità di esprimere libertà creativa e al tempo stesso apprendere elementi di strutturazione dei contenuti. I compiti in classe, le ricerche, le dispense, dovrebbero essere in Html. Tra cinquant’anni nuovi studenti e nuovi professori potranno prendere e leggere gli stessi materiali prodotti oggi in Html e approfittarne, arricchirli, aggiornarli in modo molto più utile di quanto ci si metta ad aggiornare vecchi documenti Word.

Sempre per fare la rivoluzione occorre fare una cosa assolutamente prioritaria per i nostri ragazzi: mettere a loro disposizione oggetti che consentano di creare tutto quello che riescono a immaginare. Il computer è un oggetto del genere, solo che è ingombrante e inadatto all’utilizzo in classe. Le tavolette, invece, prendono il meglio del computer tralasciandone i lati scomodi.

Software libero e Html permettono di creare dispense, materiali didattici, applicazioni web e siti che i ragazzi possono utilizzare in modo universale, persino con uno smartphone, e approfittarne per imparare in modo più interessante e coinvolgente. Per chi non intendesse dotarsi della libertà di creare il proprio codice e i propri materiali, le tavolette mettono a disposizione una quantità invereconda di programmi gratuiti e a prezzo irrisorio per imparare, scoprire, creare come mai si è potuto fare prima.

Faticoso? Difficile? Certamente. Ma i tempi sono cambiati. Per molto gli insegnanti hanno avuto la possibilità di smettere di imparare e continuare a insegnare. Oggi devono imparare intanto che insegnano. Un insegnante che non impara è un ossimoro.

Questo è il succo delle cose che ho detto. Prossimamente pubblico il link alla presentazione e più avanti, spero poco, l’intenzione è avere l’audio.

