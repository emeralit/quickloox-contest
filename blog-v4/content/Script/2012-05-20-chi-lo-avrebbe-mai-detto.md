---
title: "Chi lo avrebbe mai detto"
date: 2012-05-20T12:08:40+02:00
draft: false
toc: false
comments: true
categories: [ ]
tags: ["script"]
---
Non dovrebbe mancare molto all’infornata 2012 di MacBook Pro.

Settimana scorsa due persone mi hanno spiegato di avere fatto smontare il SuperDrive dal proprio Mac portatile, per inserire al suo posto un secondo disco rigido.

Giusto perché ove uscissero MacBook Pro privi di unità ottica, o che la contemplano solo come opzione, magari esterna, non mancherà lo stracciavesti di professione. O forse dovremmo dire, uno che non ha visto dove porta la strada.
