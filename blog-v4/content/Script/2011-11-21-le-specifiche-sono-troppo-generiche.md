---
title: "Le specifiche sono troppo generiche"
date: 2011-11-21
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Si legge di tanto in tanto che Apple ha aperto l’era post-Pc, lanciando i primi apparecchi che sono computer ma appunto non sono Pc nel senso scontato del termine. Post-Pc è stato anche un termine impiegato da Steve Jobs e di conseguenza ha rapidamente raggiunto un pubblico numeroso.

Vero o non vero che sia, il fatto autentico – che ha un impatto reale sulla nostra vita oltre le sigle – è che Apple ha aperto l’era post-spec, come la chiamano gli americani. Quella in cui le specifiche di prodotto non contano più.

La platea degli utilizzatori si è enormemente allargata, per cominciare, e in molti non capiscono niente delle specifiche. In quanti potrebbero valutare con reale competenza l’impatto della Ram di una scheda video sulle prestazioni complessive? Io non ne ho la minima idea, eppure questo non mi impedisce di acquistare un computer che reputo eccellente. Quel dato non importa.

Secondo, è sempre più difficile confrontare gli apparecchi. Prima il mondo era Windows e quindi la differenza la faceva la scheda logica. Quando si confrontava un Windows con un Mac, si faceva andare qualche programma esistente su ambedue le piattaforme e si sviluppò quasi una scienza del benchmark, che catturasse la potenza di un apparecchio indipendentemente dalla lista delle sue specifiche. Oggi prendi un iPad ed è evidente dal primo istante che, come potenza, è vastamente inferiore a quella di qualsiasi Pc propriamente detto. Eppure, se si fa una gara a chi arriva prima a leggere la posta, vince iPad. La potenza è importante, ma non fa più la differenza nell’uso.

E questo porta alla terza argomentazione, quella che mi piace maggiormente. I nuovi calcolatori sono sempre più progettati per esseri umani, ascoltando i bisogni degli esseri umani. Prima erano architetture astratte studiate per raggiungere la massima velocità. Un po’ come motori d’auto potentissimi, dove sedili, carrozzeria, comfort, quadro comandi e climatizzatore erano problemi trascurabili, da affrontare più tardi e distrattamente.

Quando si incontra una persona, certamente può avere una sua importanza quanto pesa, quanto è alta, di che colore ha gli occhi. Ma se quella persona ci è simpatica o troviamo una sintonia di pensieri, le sue specifiche passano in secondo, secondissimo piano.

Quello che blatera dello schermo grande o piccolo, del processore più o meno pompato, quello che conta le porte Usb, è uno che è rimasto indietro. Oggi conta ciò che la tecnologia può fare per noi e quanto risulta piacevole ed efficace il farlo. Gli smartphone Android si vendono a tonnellate, ma tre quinti del traffico web mobile li fa iOS. Perché con iPhone, iPod touch e iPad si va davvero su Internet comodamente e sugli altri apparecchi invece Internet la si usa poco, non essendo piacevole né efficace. Nonostante abbiano un browser velocissimo e specifiche eccellenti.

Android sta cominciando a diventare come Windows, infatti. Nel tentativo di distinguersi, i costruttori mettono insieme specifiche più grosse l’una dell’altra. Peccato che contino sempre meno rispetto al valore dell’oggetto e non per niente ci sono grandi attese di vendita per Kindle Fire, che in fatto di specifiche sembra una cenerentola rispetto a tutti gli altri.

Lo si comincia a capire, per esempio da questo articolo di Mg Siegler per TechCrunch oppure da Drewbot e anche da questo intervento di Matt Burns sempre su TechCrunch.

Questa non è una tendenza. È un traguardo. Non è questione di avere opinione contraria, ma di essere arrivato o avere ancora strada da percorrere.

