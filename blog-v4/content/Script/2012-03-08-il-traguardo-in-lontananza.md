---
title: "Il traguardo in lontananza"
date: 2012-03-08
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Così è arrivato il momento della terza generazione di iPad con lo schermo Retina.

Le specifiche non contano più nulla a questo punto. Ha ragione chi ha scritto che non si prende un iPad in quanto tale, ma per il suo ecosistema, e che non c’è niente di inaspettato o di incredibile nella nuova tavoletta, né era necessario che ci fosse. E in chat, anche se ho letto più che scrivere, mi sono trovato bene come al solito. Un grosso grazie a tutti e a Gand in primo luogo.

Naturalmente con il senno di poi, sarebbe bastato guardare all’evoluzione di iPhone e presupporre che la differenza tra nuovo prodotto e vecchio prodotto sarebbe stata molto simile. Ancora una volta, è la cosa meno interessante di tutte.

Ieri mattina mi hanno chiesto a bruciapelo che cosa ci si sarebbe dovuti attendere dal nuovo iPad. Non ci avevo dedicato neanche un momento, ma ho pensato allo schermo Retina che era assolutamente certo, ho considerato che su iPad esistono GarageBand e iMovie ma non iPhoto e ho concluso che ci saremmo dovuti attendere iPhoto o Aperture per iPad. iPhoto è puntualmente arrivato. Non perché ci siano di mezzo capacità divinatorie, ma perché è l’evoluzione naturale di questo apparecchio.

È tutto talmente ovvio e quasi scontato che si ha l’impressione di non avere in vista l’obiettivo vero, il punto di arrivo della strategia. È troppo suggestivo per essere vero. Tuttavia, se quanto vediamo fosse una ennesima fase di un programma destinato per il 2015 o il 2020 a portarci a qualcosa che ora neanche immaginiamo…? Bel pensiero, che il traguardo sia ancora lontano.

Non ho in programma l’acquisto del nuovo iPad. Almeno non ora. Ma dopo il 23 marzo voglio proprio vedere come vengono le foto sullo schermo nuovo, un milione di pixel in più di un televisore Full Hd, circa ottocentomila più del mio MacBook Pro 17”, in formato mattonella. Non vedo l’ora.

