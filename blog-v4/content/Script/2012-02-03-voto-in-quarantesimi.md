---
title: "Voto in quarantesimi"
date: 2012-02-03
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
L’amico Paolo ha fatto girare l’indirizzo di una pagina contenente 40 scorciatoie da usare su iPhone.

Tendo a prestare poca attenzione a queste raccolte, ma Paolo difficilmente butta via il suo tempo, così ce ne ho messo un po’ anch’io. Con la convinzione, sotto sotto, di saperle già tutte.

Sono caduto alla quarta.

Non ho contato con attenzione, ma potrei averne conosciute 33 o 34.

Tra quelle mancanti, c’era qualcosa di davvero utile.

Insomma, cinque minuti li merita.

