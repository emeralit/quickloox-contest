---
title: "Sincronicità"
date: 2007-01-16
draft: false
tags: ["ping"]
---

L'All About Apple Club e relativo museo Apple è arrivato sulle pagine di <a href="http://www.allaboutapple.com/present/pictures/parlano_di_noi/vogue_2007b.jpg" target="_blank">Vogue Italia</a> nel nome del retrocomputing e di quei Mac che, diversamente dai cassoni grigi senz'anima, valgono anche quando sono &#8220;inutili&#8221;.

Del tutto casualmente ci sono finito di mezzo anch'io. Il merito, chiaro, è interamente del Club.