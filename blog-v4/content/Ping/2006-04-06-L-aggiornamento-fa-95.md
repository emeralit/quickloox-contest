---
title: "L'aggiornamento fa 95"
date: 2006-04-06
draft: false
tags: ["ping"]
---

Aggiornato con pieno successo a Mac OS X 10.4.6. Doppio riavvio regolamentare, come su tutte le macchine PowerPc.

Ma riavvio previsto e contemplato. Quelli indesiderati, da inizio anno, restano due. Essendo passati novantacinque giorni da inizio anno, la media è di un riavvio non voluto ogni 47 giorni (e mezzo).

Considerato che uso il mac dodici ore e più al giorno e che al momento di riavviare per l'aggiornamento erano aperti ventinove programmi, con decine di finestre… beh, non male, il Mac con Mac OS X.