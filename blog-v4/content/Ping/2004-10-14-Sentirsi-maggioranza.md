---
title: "Sentirsi maggioranza"
date: 2004-10-14
draft: false
tags: ["ping"]
---

Quando i complessi di inferiorità si capovolgono

Noi che usiamo Macintosh siamo una élite. Quindi siamo in pochi (al massimo una trentina di milioni). I meno accorti, invece che godersi il vantaggio competitivo, si rodono il fegato temendo chissà che cosa perché sai mai, metti che Apple domani abbia problemi…

Beh, non succederà, almeno per un po'. In accordo con i più recenti risultati finanziari, Apple ha passato gli ottocentomila Macintosh venduti nell'utimi trimestre fiscale. Più sei percento rispetto all'identico trimestre fiscale di un anno prima.

Ma il bello è che Apple, sempre nel trimestre, ha venduto oltre <em>due milioni</em> di iPod. Più cinquecento e passa percento di aumento.

Altro che élite. Se si parla di iPod, il mercato è di massa che più massa non si può. A essere minoranza per una volta sono gli altri, con tutti quegli aggeggi brutti, inefficienti e di poco valore.

Apple non sparisce, tranquilli.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>