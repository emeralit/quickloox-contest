---
title: "Riavvio psicologico"
date: 2006-02-18
draft: false
tags: ["ping"]
---

Ieri, il secondo riavvio indesiderato del Mac da inizio anno a oggi.

Era una cosa già successa a ridosso dell'Epifania. Per motivi sconosciuti portatile e cellulare non si vedevano, e spegnere-riaccendere il cellulare non era bastato.

In realtà non c'era bisogno di riavviare. Il mese scorso avevo terminato e riattivato il processo <code>blued</code> e tutto aveva ripreso a funzionare correttamente. Tuttavia mi trovavo a parlare di lavoro e, inconsciamente, ho trovato più lineare riavviare che aprire il Terminale.

Lezione: a volte i (miei) problemi con il Mac sono psicologici. Ed era un riavvio non necessario, ma per onestà deve contare lo stesso. Siamo a due in 48 giorni.