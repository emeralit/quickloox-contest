---
title: "Scegli il passato per il tuo avvenire"
date: 2007-12-11
draft: false
tags: ["ping"]
---

Gente convinta che l'informatica cambi e si evolva a ritmo vertiginoso, prego, godetevi questa schermata inviata dal gentil <strong>Carmelo</strong>.

Manca solo il <em>jingle</em> con lo slogan <em>studia un linguaggio di programmazione di cinquant'anni fa e avrai il lavoro assicurato</em>.

Cobol è davvero magia arcana. Per conto mio, appena ho cinque minuti corro a fare pasticci inenarrabili in Lisp. Un altro che, a conoscerlo come si deve, non avrai mai il problema del licenziamento.