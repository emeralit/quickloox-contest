---
title: "Scrivanie tolleranti"
date: 2001-12-13
draft: false
tags: ["ping"]
---

Open Desktop: uno slogan che capisce anche il Terminale

Apple veniva rimproverata di promuovere, con Macintosh, un’architettura proprietaria e chiusa, lato hardware e lato software.
Quei giorni sono proprio passati. Usb è standard. FireWire, inventata da Apple, è standard. Airport è standard. Il prossimo standard Mpeg-4 si baserà sulla struttura di QuickTime, che è Apple.
Per non parlare del software di sistema. Ora Mac OS X ha base Unix: il che vuol dire che, per chi è disposto a occuparsi dei dettagli, su Mac possono essere riutilizzati più o meno numerosi programmi che oggi esistono per Linux, Bsd e altri dialetti Unix. È possibile lanciare programmi Java con un doppio clic (no, non era ovvio). Intanto funzionano i programmi Mac OS X e anche quelli del Macintosh Classic.
Tutta questa abbondanza Apple la chiama Open Desktop. E funziona: perfino scrivendolo (in minuscolo) sul Terminale di Mac OS X e premendo Invio.