---
title: "Parole in vendita"
date: 2009-02-16
draft: false
tags: ["ping"]
---

Cos&#236; Steve Jobs nel 2001 annunciò gli Apple Store:

<cite>Gli Apple Store sono un nuovo straordinario modo di comprare computer. Invece che stare a sentire di megahertz e megabyte, i consumatori possono capire e sperimentare che cosa possono veramente fare con un computer, per esempio creare filmati, masterizzare Cd musicali personali e pubblicare foto digitali su un proprio sito.</cite>

Cos&#236; Kevin Turner annuncia oggi la futura apertura dei Microsoft Store:

<cite>Stiamo lavorando duramente per trasformare l'esperienza di acquisto al dettaglio di Pc e prodotti Microsoft, migliorando l'articolazione e la dimostrazione delle proposte di valore e di innovazione di Microsoft, in modo che siano chiare, semplici e lineari per i consumatori di tutto il mondo.</cite>

Le <a href="http://macenstein.com/default/archives/2748" target="_blank">dichiarazioni originali</a> testimoniano che la traduzione non cambia la sostanza del discorso.

Esercizio: con un sforzo di fantasia, immaginarsi i due negozi. Quale avrà i clienti più contenti?