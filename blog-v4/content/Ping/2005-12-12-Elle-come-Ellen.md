---
title: "Elle come Ellen"
date: 2005-12-12
draft: false
tags: ["ping"]
---

Sempre coincidenze, ma sempre in mezzo

Stamattina caso urgente al telefono. L., carissima amica, deve tenere una presentazione in università. Tutto a posto, tutto preparato, tutto perfetto quando, dando gli ultimi ritocchi alla presentazione, un ultimo salvataggio e puff! il file sparisce. Non c'è Terminale, non c'è utility, non c'è niente. Andato.

Che programma stavi usando, L.?

<cite>PowerPoint.</cite>

Faresti meglio a usare Keynote.

<cite>Lo so, ma c'è qualche problema, la compatibilità, qui chiedono le cose che vanno con il Pc, nella conversione cambiano i font eccetera.</cite>

Dovresti provare OpenOffice. Da quello che ho visto mi sembra che la compatibiltà sia ottima.

<cite>Eh, ma sai, qui in università bisogna usare uno sfondo preciso e non so…</cite>

Certo, dico, bisogna stare attenti allo sfondo, se esiste ancora un file in cui metterlo, naturalmente.

<cite>Beh sì…</cite>

Fisicamente L. non potrebbe essere più diversa. Ma in quel momento avevo davanti agli occhi Ellen Feiss, quella dello <a href="http://www.adweek.com/aw/creative/best_spots_02/021014_02.jsp">spot Apple</a>. E, sicuramente per coincidenza, c'era sempre di mezzo Microsoft.

<cite>I was writing a paper on the Pc and it was, like, beep, beep, beep, beep, beep, and then, like, half of my paper was gone…</cite>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>