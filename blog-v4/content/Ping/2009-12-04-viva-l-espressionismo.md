---
title: "Viva l'espressionismo"
date: 2009-12-04
draft: false
tags: ["ping"]
---

Ne ho già parlato altrove, ma voglio ribadire: <a href="http://daringfireball.net/2009/11/liberal_regex_for_matching_urls" target="_blank">l'espressione regolare escogitata da John Gruber</a> per individuare qualsiasi Url in qualunque testo tornerà utile a molti, in un sacco di occasioni:

<code>(([w-]+://?|www[.])[^s()<>]+(?:([wd]+)|([^[:punct:]s]|/)))</code>

E c'è anche una <a href="http://daringfireball.net/linked/2009/11/28/storm-regex" target="_blank">spiegazione dettagliata</a> per chi vuole capirne di più.