---
title: "La linea di Don"
date: 2002-02-10
draft: false
tags: ["ping"]
---

Il nuovo iMac piace anche agli scettici a oltranza, nonché ai grandi esperti in usabilità

“Ho esaminato il nuovo iMac estesamente e l’ho studiato con attenzione, cercando problemi o lacune. Non ho trovato niente. È design brillante”.
<link>Così parlò Donald Norman</link>http://news.bbc.co.uk/hi/english/sci/tech/newsid_1761000/1761289.stm, ex Apple Fellow, esperto di usabilità tra i maggiori, autore di libri come “La caffettiera del masochista” e “il computer invisibile” (vivamente consigliati) e grande critico di tutto ciò che è mal progettato in funzione della facilità di utilizzo.
Dedicato a tutti i critici della domenica, quelli dal videoregistratore con l’orologio che lampeggia perché non hanno ancora imparato a regolarlo.

<link>Lucio Bragagnolo</link>lux@mac.com