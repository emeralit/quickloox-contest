---
title: "In questo mondo di ladri"
date: 2004-10-05
draft: false
tags: ["ping"]
---

Quando la classe non è acqua, perché fa acqua

Steve Ballmer, Ceo di Microsoft, ha dichiarato in un'occasione pubblica che gli utilizzatori di iPod sono ladri di musica.

Forse il monopolista colpevole di concorrenza sleale e abuso di posizione dominante credeva che la musica fosse sua.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>