---
title: "Più che Second, Double"
date: 2007-08-30
draft: false
tags: ["ping"]
---

Non so come ringraziare direttamente <strong>Nicola Vajente</strong>, che mi ha segnalato l'articolo dell'<a href="http://www.repubblica.it/2007/04/sezioni/scienza_e_tecnologia/second-life-news/bluff/bluff.html" target="_blank">inversione di tendenza di Repubblica</a> su <a href="http://www.secondlife.com" target="_blank">Second Life</a>, ma ha usato il servizio di segnalazione del sito invece che un suo indirizzo di posta.

Repubblica, la parte informatica, è una vergogna e ne diffido esattamente come quando per loro Second Life sembrava il nuovo paradiso virtuale. Adesso ce ne sarebbero due, quella vera, semideserta tranne che per sex shop e discoteche, e quella mediale, di cui parlano giornali e web.

È invece interessante il fatto che, se i media iniziano a parlarne male, per Second Life scatta finalmente l'ora della verità. Se prospera nonostante, è roba seria. Se si sgonfia, era un soufflè.

Io sono entrato un paio di volte ancora facendomi forza, ma continuo a trovarlo un posto dove se vuoi qualcosa di interessante te lo devi creare tu e non è il mio genere, nel senso che per creare qualcosa scelgo più volentieri altri ambiti.

Non ho ancora sentito nessuno che mi abbia confermato di persona di fare soldi veri in Second Life con una sua attività personale. Non che sia un requisito, ma tanta gente era entrata inseguendo (spesso senza confessarlo) quel sogno e sono curioso di sapere se si sia realizzato.