---
title: "Finiscono i saldi"
date: 2005-08-28
draft: false
tags: ["ping"]
---

Approfittiamone prima che sia tardi

Scade oggi l'offertissima di Mariner Software: sconto del 50 percento su tutti i pacchetti famiglia da cinque licenze.

I pacchetti famiglia sono quelli da cinque licenze: costano più che una singola applicazione ma meno di cinque licenze individuali. Per scuole, aziende, organizzazioni sono una buonissima soluzione per essere in regola a prezzi accettabili. Il 50 percento sul Five Pack è un'occasionaccia vera.

L'offerta scade oggi ma abbiamo il vantaggio del fuso orario. Nel peggiore dei casi, c'è tempo fino a mezzanotte per l'ultima corsa allo <a href="http://msw1.net/dbm83/l.htm?5721&55837&5718&test">shopping</a>.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>