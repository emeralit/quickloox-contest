---
title: "Assemblea anticondominiale"
date: 2009-12-06
draft: false
tags: ["ping"]
---

Ho trovato piacevole e leggera il giusto la chiacchierata su vita senza Word di venerd&#236; scorso. Buon pubblico, clima disteso, davvero chiacchierata sul tema e anche sui derivati, dall'esigenza di insegnare ai ragazzi i concetti invece dei programmi fino a qualche considerazione sull'opportunità di usare programmi meno costosi e meno ingombranti, soprattutto in ambito professionale (segreto: se Word è usato per scrivere, si tratta di dilettanti. Di solito Word è usato per scopi che non sono scrivere, ma compiacere qualcuno, supplire a mancanza di cultura informatica eccetera).

Al grido (sussurrato) di <i>Word è buono per scrivere le lettere al condominio e anche l&#236; è meglio Pages</i> siamo scivolati verso una pizza piacevolissima e un dopopizza dentro la sede di @Work.

Come sempre, ma non come abitudine, un grosso grazie a chi c'era e a chi avrebbe voluto esserci. E anche a chi non avrebbe voluto, mai fare cose controvoglia. :-)

Ho tenuto per l'occasione una piccola presentazione, che ho <a href="http://www.youtube.com/watch?v=ns5nPweyuM8" target="_blank">esportato su YouTube</a> per gli eventualmente interessati (settanta secondi e si vede tutto). Sta anche su iWork.com, ma bisogna fornirmi un indirizzo email cui farsi invitare. In una presentazione i messaggi sono sempre forti e netti, senza troppe sfumature: nessuno si ritenga offeso o segnato a dito. Il tono non ha questa intenzione, semmai si propone di raggiungere anche lo spettatore indifferente. C'è abbastanza riuscito.