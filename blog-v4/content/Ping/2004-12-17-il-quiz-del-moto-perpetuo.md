---
title: "Il Quiz del Moto Perpetuo"
date: 2004-12-17
draft: false
tags: ["ping"]
---

Caro lettore, scopri con me come si alimentano le tastiere senza fili

Nell'Apple Center entra un signore, che curiosa un po' in giro e si ferma a contemplare le tastiere Bluetooth. Poi fa <em>belle queste tastiere senza fili… ma come sono alimentate?</em>

Io avrei fornito una delle seguenti risposte:

a) con il suo bel cavo di alimentazione;<br>
b) pannelli solari posizionati sul fondo dell'unità;<br>
c) dinamo realizzate in nanotecnologia collegate a ciascun tasto;<br>
d) uranio arricchito, le importiamo dall'Iran;<br>
e) volere è potere;<br>
f) nel suo quartiere verrà presto installato un gruppo elettrogeno;<br>
g) a rumore, bisogna cantare Bocelli intanto che si digita;<br>
h) è il nuovo modello con dentro gli gnomi;<br>
i) Apple ha appena brevettato il moto perpetuo.

Tu che cosa avresti risposto?

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>