---
title: "La sovversione di Barney"
date: 2005-07-04
draft: false
tags: ["ping"]
---

Perché avere un Mac è andare controcorrente e riuscire a lavorare come si deve

La parola a Barney Panofsky:

<cite>Per motivi di lavoro ho dovuto passare le ultime tre giornate al Palalottomatica dell'Eur a Roma per seguire il congresso nazionale di un partito politico italiano.</cite>

<cite>[…] volevo segnalare con grande entusiasmo che <em>tutti</em> i fotoreporter accreditati all'evento e qualche cronista usavano PowerBook e iBook. :-)</cite>

<cite>Le Ethernet/Wan messe a disposizione per giornalisti e fotografi nella sala stampa spesso non bastavano per tutti. È bastato avviare la condivisione AirPort da uno dei PowerBook collegati alla rete e il gioco in pochi minuti si è fatto :) Tutti i Macuser potevano inviare foto e testi alle redazioni senza sgomitare sui tavolini e bisticciarsi per un cavetto inciampando sui fili intrecciati degli alimentatori… :P</cite>

<cite>Impietosi i commenti verso chi, invano, cercava di collegare laptop aziendali rigorosamente IBM, Acer e Toshiba :-) Guarda caso erano loro quelli che più rompevano le scatole ai tecnici della sala…</cite>

<cite>Per noi un cambio postazione e un paio di clic ;-)</cite>

<cite>Credo che i discorsi &ldquo;prestazioni&rdquo; e &ldquo;design&rdquo; siano fiori all'occhiello per noi clienti di Cupertino, ma la semplicità d'uso e l'affidabilità del Mac continua a rimanere IMHO il miglior valore aggiunto per far impennare la produttività di chi col portatile deve lavorarci tutti i giorni…</cite>

<cite>Intel sì o no? Chissenefrega :P</cite>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>