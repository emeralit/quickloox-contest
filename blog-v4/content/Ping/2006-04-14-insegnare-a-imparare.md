---
title: "Insegnare a imparare"
date: 2006-04-14
draft: false
tags: ["ping"]
---

<strong>Yoghi Bear</strong> mi ha scritto:

<cite>Sono ricapitato per caso oggi su <a href="http://www.pagetutor.com/pagetutor/makapage/index.html" target="_blank">questa pagina</a> dopo 2 anni. Può sembrare una pagina stupida vedendola così ma è il miglior tutorial per imparare a scrivere Html e capire quello che si fa per chi non ne sa proprio niente.</cite>

In questo senso, che possa magari sembrare una pagina stupida è il miglior complimento. Grazie!