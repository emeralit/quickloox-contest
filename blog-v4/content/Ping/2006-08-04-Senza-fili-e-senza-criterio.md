---
title: "Senza fili e senza criterio"
date: 2006-08-04
draft: false
tags: ["ping"]
---

La supposta dimostrazione della <a href="http://news.com.com/1606-2_3-6101573.html?tag=ne.video.6060109" target="_blank">violazione della sicurezza di un MacBook</a> con il wireless attivato non lo è.

La cosa è avvenuta durante il convegno di hacker BlackHat, ma non è avvenuta <em>dentro</em> il convegno.

Non è stata infatti una dimostrazione in diretta, su grande schermo, di fronte a testimoni. Ma tutto ciò che possiamo esaminare è un video, registrato e sottoposto a montaggio preventivo.

Ma la cosa che veramente suona strana è il perché inserire una scheda di rete esterna dentro un Mac abbondantemente dotato di scheda di rete interna.

La risposta può essere una sola: perché con la scheda interna, ammesso che sia vero, non avrebbe funzionato.

Cos&#236; diventa evidente che hanno usato un MacBook unicamente per farsi maggiore pubblicità e, al massimo, hanno violato la sicurezza di chissà quale scheda di rete esterna.

Grosso grazie ad <strong>Alan</strong>.