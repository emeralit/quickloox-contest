---
title: "Quanta invidia John"
date: 2008-04-04
draft: false
tags: ["ping"]
---

Ci sono vari motivi per cui invidio John Gruber. Uno è che sta usando tuttora una Extended Keyboard II di Apple, collegata ai Mac di oggi tramite un <a href="http://www.amazon.com/exec/obidos/asin/B000067V8L/ref=nosim/daringfirebal-20" target="_blank">adattatore Adb-Usb iMate di Griffin</a>.

Come sottolinea Gruber, un adattatore da 39 dollari a molti può sembrare ingiustificato come costo, visto il prezzo di una tastiera attuale.

Se però la tastiera è <a href="http://www.flickr.com/photos/gruber/sets/72157604797968156/" target="_blank">in uso da quattordici anni</a>, il discorso sul prezzo cambia un bel po'.

Vorrei avere sottomano uno di quelli che il-Mac-costa-di-più e chiedergli quanto durano le tastiere dei suoi Pc supereconomici.

P.S.: Gruber scrive della sua tastiera perché, dopo quattordici anni, gli si è rotto un tasto. L'ha sostituita con&#8230; un'altra tastiera identica, comprata anni e anni fa per il momento in cui sarebbe servita. E l'invidia è ancora maggiore.