---
title: "Non si arriva mai a tutto"
date: 2007-11-30
draft: false
tags: ["ping"]
---

Non avrei mai pensato che ci fosse un sistema facilissimo, ancora più facile di quello immediatamente visibile, per legare un'applicazione a uno Space su Leopard.

<a href="http://web.mac.com/brethil/Brethils_Page/Thoughts/Entries/2007/11/23_An_easy_way_to_bind_applications_to_spaces.html" target="_blank">Dany</a> invece c'è arrivato.