---
title: "Notizie dal mondo reale"
date: 2005-06-07
draft: false
tags: ["ping"]
---

La differenza tra chi vive dentro le notizie e chi vive e basta

Ho l'impressione che la Lunga Marcia verso i processori Intel scatenerà un trimestre di puro caos primordiale e che i veri conti si dovranno fare dopo.

Oggi, dopo l'Annuncio, ho ricevuto due messaggi in iChat. Il primo:

<cite>Ho sentito una cosa stranissima su La7. Vogliono mettere i Pentium nei Mac. Hai qualche fonte attendibile?</cite>

Il secondo:

<cite>Io avevo intenzione di aspettare il portatile G5 e adesso mi tocca almeno un anno di attesa.</cite>

Come se i PowerBook G5 fossero previsti per una data qualsiasi.

È ancora assai presto per dire una parola definitiva, ma per molti cambierà, tutto sommato, assai poco.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>