---
title: "Rimessa all'indice"
date: 2007-03-13
draft: false
tags: ["ping"]
---

Ho scoperto una conseguenza dello spegnimento inaspettato del Mac durante l'ultimo aggiornamento di iTunes e QuickTime.

Si è azzerato l'indice di Spotlight. Ora si sta ricostruendo e nessun problema, è che la completezza della cronaca è un valore. :-)