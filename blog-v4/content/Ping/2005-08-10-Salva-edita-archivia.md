---
title: "Salva, edita, archivia"
date: 2005-08-10
draft: false
tags: ["ping"]
---

Collegamento tra due programmi che non mi aspettavo

Questo suggerimento è già passato in una mailing list che frequento, ma mi sembra interessante e voglio riproporlo anche a chi non lo avesse già letto. Prego gli altri di scusarmi.

In pratica: TextEdit legge e scrive in formato webarchive. Ovvero: si può registrare su disco una pagina Html completa di tutte le sue parte grafiche, via Safari, appunto come webarchive. Il file ottenuto si può aprire con TextEdit e modificarlo a piacere.

Sconsiglierei di usare questa funzione per falsificare pagine Web da presentare a un gonzo da imbrogliare (salvo pesci d'aprile). Invece, per annotare o per lavorare a più mani, è una cosa di una facilità quasi vergognosa.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>