---
title: "Memoria da ricordare"
date: 2003-03-14
draft: false
tags: ["ping"]
---

Il PowerBook G4 da 12” supererà la barriera dei 640 megabyte

Chiamiamolo Aluminum 12”, visto che si è sempre parlato di Titanium. È un ottimo portatile, compromesso ideale tra l’iBook e la gamma professionale, compattissimo, che sta dappertutto e ha anche muscoli (in termini di calcolo) da mostrare.

“Peccato che sia anche lui limitato a 640 megabyte di Ram”, ha commentato Piero.

Sorry, Piero, ma non è così. Il piccolo Aluminum è predisposto per accettare i moduli Dimm da un gigabyte e quindi potrà arrivare a 1,1 gigabyte di Ram (128 megabyte sono saldati sulla scheda madre).

Si tratta solo di attendere il momento in cui saranno disponibili, ma per la prima volta un portatile Apple con schermo da 12” può montare più di 640 megabyte di Ram. C’è da farsi un nodo al cavo di almentazione.

<link>Lucio Bragagnolo</link>lux@mac.com
