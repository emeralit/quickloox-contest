---
title: "Una parola al giorno"
date: 2004-10-20
draft: false
tags: ["ping"]
---

Ancora un po' di tempo e Apple la aggiungerà al vocabolario Mac

La parola è <em>Xsan</em>. Per il momento si può giocare a fare i misteriosi ancora per un po', dato che non ha nessuna applicazione concreta sui Mac di oggi, adesso.

Ma è questione, immagino, solo di qualche mese. Intanto i più intraprendenti possono arrivare a sapere già molto, se non tutto, nei modi consueti.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>