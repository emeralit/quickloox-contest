---
title: "Have e have-not"
date: 2005-09-17
draft: false
tags: ["ping"]
---

Può esserci una questione di punti di vista su un oggetto che non ha spessore?

Apple ha sostituito l'iPod più venduto della sua storia con un altro modello che però è clamorosamente più leggero, privo di parti in movimento, con schermo a colori e spessore oltraggioso da quanto è minimo. Eppure c'è gente impegnata a parlare delle cose che iPod nano non fa, invece di quelle che fa.

Gente che non lo ha tenuto in mano, non lo ha usato seriamente e non si è resa conto di che oggetto è. Disquisisce sulla mancanza, per dire, di una porta video. Manca anche la porta per la macchinetta del caffè, che pure si può prendere insieme alla musica.

Non è tutta questione di avere o non avere il nano, comunque.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>