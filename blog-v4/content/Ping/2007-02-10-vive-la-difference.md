---
title: "Vive la difference"
date: 2007-02-10
draft: false
tags: ["ping"]
---

Non mi piace molto segnalare video, perché stare alla finestra è roba da televisore. Mentre guardi un video non fai niente, fanno qualcosa gli altri (quelli che hanno girato <a href="http://www.youtube.com/watch?v=6gmP4nk0EOE" target="_blank">il video</a>) e questo è un modo ideale per restare indietro.

Ogni regola ha però le sue eccezioni e non ho ancora trovato niente che spieghi meglio il significato di Web 2.0 e quale impatto ha avuto Internet sulla trasformazione del mondo. E lo sta avendo. La visione, per una volta, è istruttiva e fa guadagnare qualcosa invece che perdere tempo.

E il Mac? Beh&#8230; vuoi guidare la macchina girando lo sterzo a suon di chiave inglese oppure con un servosterzo ergonomico?