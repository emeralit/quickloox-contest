---
title: "A Natale possiamo essere tutti più buoni"
date: 2006-12-15
draft: false
tags: ["ping"]
---

Il mitico <strong>Carlo</strong> di <a href="http://freesmug.org" target="_blank">FreeSmug</a> mi ricorda che sarebbe buona cosa segnalare <a href="http://macheist.com/" target="_blank">MacHeist</a>… e ha ragione da vendere.

Un bundle di nove ottimi programmi. Prezzo totale, sopra i 307 dollari. Prezzo scontato, 49.

Un quarto della cifra va un’organizzazione umanitaria. Ce ne sono otto diverse possibili, tra le quali PreventCancer.org e Global Hunger Project.

Se si raccolgono più di centomila dollari in totale, nel bundle entra anche TextMate.

Ancora tre giorni di tempo. Ottimi programmi a ottimo prezzo, si fa pure del bene; aspettare e perdere l’occasione è futile. :-)