---
title: "Tanto per capirci"
date: 2010-03-22
draft: false
tags: ["ping"]
---

Il risultato che hai trovato su Wikipedia non è la fine della tua ricerca. È l'inizio.