---
title: "Banana split"
date: 2005-02-13
draft: false
tags: ["ping"]
---

I fatti e le opinioni in arrivo

Quando il primo marzo il valore delle azioni Apple sembrerà improvvisamente dimezzarsi, sarà per effetto dello split che il consiglio di amministrazione della società ha deciso l'altroieri. Ogni azione sarà convertita in due azioni da metà valore ciascuna.

Non cambierà niente se non che il numero delle azioni esistenti raddoppierà.

Eppure posso stare certo che qualcuno arriverà sulle mailing list o nei forum più frequentati a dare la sconvolgente notizia e a chiedersi il perché dell'improvvisa discesa.

Banana people.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>