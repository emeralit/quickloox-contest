---
title: "Niente voodoo su 10.3.9"
date: 2005-04-19
draft: false
tags: ["ping"]
---

Qualunque problema ha un motivo, basta cercarlo

Alcune persone hanno avuto problemi con l'aggiornamento a 10.3.9 di Mac OS X. I problemi con Safari sono legati all'uso di programmuzzi come AcidSearch o altri, che dovrebbero potenziare le funzioni del browser ma non sono scritti benissimo.

Per i problemi con Java si è scatenata una caccia alla pozione magica che ha dell'incredibile. C'è chi installa un Security Update più vecchio di quello attuale, ponendo il sistema in uno stato di rischio malfunzionamento ben peggiore. C'è chi forza un prebinding su tutto il sistema, come minimo perdendo tempo.

Le macchine problematiche hanno, da prima dell'update, una libreria shared danneggiata: /System/Library/Frameworks/JavaVM.framework/Versions/1.4.2/Libraries/classes.jsa.

Le mie capacità non arrivano oltre. Non so se la libreria venga ricreata, se vada ricreata a mano, se veramente serva a qualcosa oppure se è come la vite in più che resta sempre sul tavolo una volta richiuso il computer, ma è certo che basta prendere quella libreria e spostarla in un'altra cartella (non buttarla via!) per riavere Java funzionante come prima.

Ho anche la mia idea su come sistemare definitivamente la questione (applicare 10.3.9 combo dopo avere tolto la libreria) ma non sono in grado di controllare, nel senso che i computer che ho aggiornato finora funzionano alla perfezione.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>