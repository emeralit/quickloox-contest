---
title: "300+ novità: Desktop"
date: 2007-10-24
draft: false
tags: ["ping"]
---

La barra dei menu semitrasparente e il Dock con i riflessi 3D li si son visti. Meno evidente, forse, che la finestra in primo piano ha ombre più accentuate (e un colore diverso della toolbar), a dare un aspetto 3D più pronunciato.

Dentro il Dock si possono avere <em>stack</em>, pile di oggetti che con un clic si aprono ad arco (se pochi) o a griglia (se tanti). C'è uno <em>stack</em> apposta per i file scaricati da Safari, iChat e Mail. Dentro uno <em>stack</em> i file sono organizzabili per nome, data, tipo. Un Ctrl-clic sullo <em>stack</em> e si definisce l'ordine.

Il Dock si può sincronizzare su più Mac. Bisogna avere un abbonamento a .Mac, però.

Finisco con una cosa che è inedita prima della mitica <a href="http://www.apple.com/it/macosx/features/300.html" target="_blank">pagina riassuntiva</a> di tutte le modifiche principali di Leopard: se trascini un documento sopra un'applicazione che sta dentro il Dock, e premi la barra spazio, la puoi usare al volo, come se fosse una cartella a molla. Per fare un esempio: trascini una foto su iPhoto, premi la barra spazio, la trascini nella libreria di iPhoto. Non è che iPhoto di apre e basta, come con un normale <em>drag and drop</em>.