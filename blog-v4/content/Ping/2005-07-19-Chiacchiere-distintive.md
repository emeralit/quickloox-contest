---
title: "Chiacchiere distintive"
date: 2005-07-19
draft: false
tags: ["ping"]
---

Ennesimo annuncio di supporto dei Mac del futuro

Intego, produttrice di vari prodotti di sicurezza informatica per Mac, ha <a href="http://www.intego.com/news/pr62.asp">annunciato</a> il supporto dei Mac con processore Intel dal primo giorno in cui verranno messi in vendita.

La notizia non è più attuale, ma ho già consegnato un pezzo sull'argomento a Macworld quello cartaceo, e questo annnuncio non si era ancora verificato.

Devo aggiungere un'altra voce all'elenco di decine e decine di software house che hanno già creato i file Universal Binary, o hanno già fatto sapere che non c'è proprio problema, o comunque si sono impegnate pubblicamente ad arrivare in tempo.

C'è chi sostiene che la transizione del software Mac ai processori Intel sia una cosa di difficoltà estrema, problematica, insidiosa. Da quello che si vede, per ora, sono (parafrasando De Niro ne Gli Intoccabili) chiacchiere distintive. Nel senso che rivelano la statura tecnica di chi le fa.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>