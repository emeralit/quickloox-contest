---
title: "300+ novità: Stampa"
date: 2007-11-11
draft: false
tags: ["ping"]
---

Anche l'architettura di stampa, in Leopard, è stata rifatta da capo a piedi o quasi.

L'anteprima di stampa è disponibile nello stesso dialogo di stampa, invece che dover premere il pulsante Anteprima come in Tiger e precedenti.

Le stampanti riconosciute dal sistema sono state portate a oltre duemila.

Viene supportato il protocollo di autenticazione Kerberos. Tradotto: è possibile l'autenticazione della stampa.

Il sistema si accorge quando si passa da una rete a un'altra e cambia anche la stampante di default. Cos&#236; a casa ho subito la mia DeskJet 995 Hewlett-Packard, in @Work commuta sulla Color LaserJet 3700.

Quando le stampanti hanno qualità di stampa professionale, Leopard sa sfruttarla e usare fino a 16 bit per canale di colore.

Cups, il sistema di stampa <em>open source</em>, è stato aggiornato alla versione 1.3 e da solo meriterebbe un <em>post</em>.

Per finire, i driver di stampa ora arrivano anche attraverso Aggiornamento Software.

Ma la <a href="http://www.apple.com/it/macosx/features/300.html#printing" target="_blank">pagina delle 300 novità di Leopard</a> continua.