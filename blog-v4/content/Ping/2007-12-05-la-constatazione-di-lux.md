---
title: "La Constatazione di lux"
date: 2007-12-05
draft: false
tags: ["ping"]
---

<cite>Dati il tempo occorrente a diffondere un meme e il tempo necessario a dimostrare che è malato, il loro prodotto è una costante.</cite>

In italiano: più un'idea è da poco, meno tempo ci vuole a farla girare, e più tempo serve per smontarla.

Un <a href="http://it.wikipedia.org/wiki/Meme" target="_blank">meme</a> è un pezzo di informazione capace di autoriprodursi.

Uno degli ultimi memi ho sentito (dall'amico <a href="http://www.maclovers.com" target="_blank">Blue</a>): Apple ha usato l'open source come escamotage, perché essendo partita da software open source per realizzare Mac OS X doveva per forza mantenere open source il frutto del proprio lavoro e quindi il fatto che esista Darwin è dovuto solo a questo.

È una cosa che non sta né in cielo né in terra, ma per poter essere smentita in modo inoppugnabile richiede due giorni di lavoro, a partire dalle licenze di ciascun pezzo di software che concorre a fare Mac OS X fino a mostrare le parti di Darwin che arrivano da Apple e non sono state trovate pronte da usare.

Due giorni di lavoro da mettere su questa cosa non li ho. Ma che servano non ci piove e questo rafforza l'idea che il meme sia sciocco, perché a dirlo, invece, bastano dieci secondi.