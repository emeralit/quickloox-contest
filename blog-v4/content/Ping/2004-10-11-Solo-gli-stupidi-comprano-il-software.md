---
title: "Solo gli stupidi comprano il software"
date: 2004-10-11
draft: false
tags: ["ping"]
---

Con una postilla: a meno che non ne valga proprio la pena

Mi scrive l'amico Mario Rizzi e mi segnala una serie di programmi eccellenti per il lavoro che svolgono, gratuiti o dal prezzo ridicolo, perfettamente funzionali e perfettamente grafici, niente roba Unix, comandi astrusi, Terminali, file di configurazione.

Mi riprometto di tornare spesso sul tema perché, se uno preferisce vivere di Office, è meglio che sia almeno consapevole di che cosa si perde (nel portafogli).

Tanto per cominciare, ecco un programma eccellente per classificare tutti i libri di casa, e non solo: <a href="http://books.aetherial.net/">Books</a>.

È gratuito: provalo e, dopo avere ringraziato Mario, dimmi se vale la pena di spendere soldi in qualcos'altro.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>