---
title: "Fame di widget"
date: 2007-06-03
draft: false
tags: ["ping"]
---

Prima <strong>Dany</strong>, con <a href="http://shockwidgets.com/?widget=miniStat" target="_blank">la collezione di miniStat</a>. Poi <strong>Marco</strong>, con <a href="http://www.duespaghi.it/spagolab/widget/dashboard/" target="_blank">il widget di Duespaghi</a>. Grazie mille più mille duemila a tutti e due.

miniStat è roba tutto sommato tecnica, per quanto interessante. Che Duespaghi abbia un widget, invece, è un (bel) segno dei tempi. Mac dà gusto anche a un sito, di suo gustosissimo. :-)