---
title: "Non più boot"
date: 2005-10-20
draft: false
tags: ["ping"]
---

Ci si poteva accorgere da prima che i tempi stanno per cambiare

Un sacco di gente si è resa conto improvvisamente che i nuovi iPod, essendo privi di FireWire, non permettono di avviare un Mac facendo, appunto, il boot dal meraviglioso oggettino che altrimenti non è mai stato così bello e così versatile.

Ma viene sconsigliato ufficialmente dal 4 marzo 2004, come testimonia questa <a href="http://docs.info.apple.com/article.html?artnum=93739">nota tecnica</a>.

Il che dà anche da pensare rispetto a quando sia iniziata la progettazione degli iPod nano appena presentati.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>