---
title: "I manuali da leggere"
date: 2005-03-04
draft: false
tags: ["ping"]
---

Per la prima volta nella vita guardo un manuale Mac e…

Mentre aspettavo la fine di un lungo trasferimento dati ho dato un'occhiata, per noia, al manuale che accompagna i Mac.

I manuali dei Mac li ho sempre snobbati, lo ammetto. Ma devo riconoscere che, nelle poche pagine da cui sono composti, si trovano varie informazioni interessanti. Per esempio il tipo esatto di banchi di memoria da montare per espandere la Ram, come allungare al massimo la durata della batteria, come pulire macchina e schermo, come si raggiunge l'alloggiamento della memoria supplementare e altre cose.

Di solito i manuali Mac vengono criticati perché raccontano poco o niente, ed è vero. Ma allora perché metà della gente che frequenta forum e mailing list fa domande le cui risposte sono esattamente contenute nel manuale che hanno acquistato assieme alla macchina?

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>