---
title: "Animazione in comunità"
date: 2007-03-29
draft: false
tags: ["ping"]
---

Non ricordo più chi avesse chiesto come fare disegni animati vecchio stile su Mac OS X e, adesso che me lo sono dimenticato, ho trovato <a href="http://www.les-stooges.org/pascal/pencil/index.php?id=Home" target="_blank">Pencil</a>.

La cosa più interessante è <a href="http://www.idevgames.com/" target="_blank">dove l'ho trovato</a>. Da frequentare molto più di quanto suggerisca l'etichetta.