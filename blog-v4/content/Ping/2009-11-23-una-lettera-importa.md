---
title: "Una lettera importa"
date: 2009-11-23
draft: false
tags: ["ping"]
---

Ci sono lettere aperte e lettere importanti.

Per quella di <a href="http://infinite-labs.net" target="_blank">Emanuele</a>, promettente sviluppatore per iPhone, rivolta ad Apple e in particolare a chi manda avanti App Store, volevo trovare una sintesi tra aperta e importante e non ho saputo fare meglio.

La <a href="http://infinite-labs.net/openletter/" target="_blank">lettera</a> riguarda proprio la possibilità di fare meglio con App Store, nell'interesse di tutti, e se avessi il potere di farla leggere a tutti i dirigenti di Apple ne approfitterei.

Ho solo il potere di linkarla e invito chi ne condividesse il contenuto a fare altrettanto, dove e come vorrà.