---
title: "Piccolo anniversario personale"
date: 2005-01-20
draft: false
tags: ["ping"]
---

Non so quanti Ping ho scritto, ma oggi calcolarlo è molto facile

Quando ho cominciato era quasi una sfida: riuscirò a trovare un argomento sensato ogni giorno in ambito Mac?

Oggi posso dire che semmai il problema è l'abbondanza. Se non è uscito regolarmente un Ping ogni giorno, si deve a qualche pausa vacanziera e qualche problema tecnico. Anche dove fosse mancata l'ispirazione, gli spunti sono innumerevoli e questo piccolo grande mondo Macintosh è non solo un po' pazzo, ma anche tanto tanto interessante.

Per farla breve, come sfizio ho classificato ogni Ping con un codice alfabetico di tre lettere. il primo Ping è stato <em>aaa</em>. Questo è <em>azz</em> e il prossimo sarà <em>baa</em>.

Non li conto, ma per quello di oggi è semplice: festeggio il Ping numero 26 al quadrato.

Ora devo trovare una torta per tutte queste candeline.

Chiedo scusa per l'autocelebrazione indebita. Da domani si riparte. :-)

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>