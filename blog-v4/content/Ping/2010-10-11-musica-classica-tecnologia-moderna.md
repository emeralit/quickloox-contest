---
title: "Musica classica, tecnologia moderna"
date: 2010-10-11
draft: false
tags: ["ping"]
---

L'amico Alessio Nanni, pianista di talento e solido utente Mac, ne ha combinata una delle sue: ha scritto un linguaggio di programmazione che trasforma i segnali audio in forme e colori.

E ha pubblicato su Internet <a href="http://www.alessionanni.com/synaesthesia/" target="_blank">Synaesthesia</a>, il Preludio in do diesis minore di Rachmaninov applicato alla sua tecnologia.

L'esperimento è interessante e richiede tassativamente lo schermo pieno e bene illuminato per coglierne tutte le sfumature. Piaccia o non piaccia, malissimo che vada si possono godere quattro minuti e mezzo di ottimo pianoforte classico.

Me lo sono guardato su Mac, in Flash, dato che il sito di Alessio non prevede altre strade. Tiratina d'orecchie mentale e poi ho deciso che avrei visto la stessa cosa su iPad, in un modo e nell'altro.

Il codice Flash sul sito si limita a inscatolare un video di Vimeo, dove sono andato e in dieci secondi di ricerca ho trovato un <a href="http://vimeo.com/15275100" target="_blank">link perfetto per iPad</a>.

Doppiamente istruttive le note a margine presenti su Vimeo. Da una parte le nozioni artistiche di colore e suono, legate a chi ha sperimentato storicamente il loro connubio, per esempio Kandinsky (a me viene anche in mente Jean-Michel Jarre, ma sono incolto e chiedo scusa).

Dall'altra parte leggo <cite>Avviso importante: per evitare di perdere fotogrammi nella porzione visivamente più veloce del filmato, si raccomanda fortemente di usare la riproduzione Html5 al posto di quella Flash.</cite>

Complimenti a tre piani per Alessio: per l'esecuzione, per l'iniziativa, per avere chiarito in due righe mesi di diatriba su dove si debba puntare tra Html5 e Flash.