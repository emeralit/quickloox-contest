---
title: "Interessante-filmloop"
date: 2006-01-13
draft: false
tags: [ping]
---

Da quando Guy Kawasaki ha creato il suo blog se ne è andato un altro pezzo di tempo dedicabile al lavoro, perché il suo blog è davvero imperdibile. Spero si stanchi. :-D

Nel frattempo, consiglio vivamente un software che consiglia lui: FilmLoop. Non perché sia particolarmente fantastico; non ho ancora deciso se è fondamentale come Google Earth o World of Warcraft. Ma è decisamente interessante come tentativo di trovare una via nuova per visionare in modo creativo e costruttivo grandi insiemi di foto e altre informazioni.
