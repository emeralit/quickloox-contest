---
title: "Polemica stonata"
date: 2004-11-04
draft: false
tags: ["ping"]
---

La Fimi sbotta per l'iTunes Music Store ed è una sonora balla

Me ne occupo un po' in ritardo, ma è gustosa. Secondo <a href="http://webnews.html.it/news/2406.htm">Webnews</a>, La Fimi (Federazione Industria Musicale Italiana) avrebbe attaccato Apple subito dopo l'apertura dell'iTunes Music Store per &ldquo;non avere aperto alcuna trattativa con le etichette indipendenti&rdquo;.

A parte l'assurdità della situazione per cui una federazione parla a nome degli indipendenti (che indipendenti sono, allora? Ma in Italia sono cose normali), ci sono collaboratori di Macworld che stanno facendo esattamente consulenza a etichette indipendenti che vogliono entrare nell'iTunes Music Store, ergo la sparata della Fimi è una balla.

A che scopo? Un quarto d'ora di visibilità in più, approfittando di un nome più grosso del tuo. Poca roba, ma evidentemente si accontentano.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>