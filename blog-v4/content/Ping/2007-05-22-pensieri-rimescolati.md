---
title: "Pensieri rimescolati"
date: 2007-05-22
draft: false
tags: ["ping"]
---

Ogni tanto si sente che gli iPod mancano di questo o di quello (la radio, il microfono e cos&#236; via).

Io stesso ho comprato il primo iPod shuffle perché era un iPod ma poteva essere anche un pendrive da un gigabyte.

Poi Apple ha fatto il secondo iPod shuffle. Ancora più piccolo del primo. Ma non era neanche un pendrive, a meno di non portarsi dietro il fastidiosissimo minidock. Solo musica e stop.

Apple sta vendendo il secondo iPod shuffle a carrettate, anche se non è più un pendrive.

Conclusione: niente da eccepire sulla comodità di avere lo shuffle che fa anche da pendrive. Ma la ragione primaria per la quale si vende iPod shuffle è la musica.

Il resto è marginale. Guarda caso, oggi un pendrive da un gigabyte si compra a nove euro. Come dire che il valore di quella funzione aggiuntiva era veramente minimo.