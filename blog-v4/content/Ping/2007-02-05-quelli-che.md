---
title: "Quelli che"
date: 2007-02-05
draft: false
tags: ["ping"]
---

Quelli che Apple dipende troppo da Steve Jobs e lo ripetono ogni quarto d'ora, <strong>oh yeah</strong>.

Quelli che Dell non sa che pesci pigliare e <a href="http://news.com.com/Michael+Dell+back+as+CEO%2C+Rollins+resigns/2100-1014_3-6155185.html?tag=nefd.lede" target="_blank">richiama il fondatore Michael Dell</a> a fare il Ceo, e manco se ne accorgono e continuano a dire che Apple dipende troppo da Steve Jobs, <em>oh yeah</em>.

(grazie a <strong>Paolo</strong>)