---
title: "Il bello della verità è il suo opposto?"
date: 2006-06-11
draft: false
tags: ["ping"]
---

&Egrave; ciò che mi ha chiesto (molti giorni fa, ma è tutta colpa mia) <strong>Vittorio</strong> dopo essersi imbattuto in questo <a href="http://vippoblog.blogspot.com/2006/05/non-preoccupiamoci.html" target="_blank">post</a>. Il titolo dell'articolo originale, ha aggiunto Vittorio, è <em>Windows morde la mela</em>. Come se non fosse, e arriviamo alla domanda, l'esatto contrario.

Ci si chiede se mai nella storia potremo vedere un titolo sulla mela che morde Windows, senza la genuflessione obbligatoria a santa Microsoft.