---
title: "Riavviare e non salvare"
date: 2005-01-05
draft: false
tags: ["ping"]
---

Anche con un driver stupido si può ragionare

Mi sono trovato a installare il driver di una nuova stampante (Epson Stylus Photo R200), pura senza avere urgenza immediata di stampare. Mi sono reso conto che il driver pretendeva di chiudere tutti i programmi in funzione per procedere all'installazione e poi richiedeva il riavvio del Mac.

Il riavvio ci sta, perché è tecnicamente giustificato, ma la chiusura di tutti i programmi, per me che ne tengo aperti a decine, era veramente un colpo basso.

Però non c'era via di scampo e ho a malincuore dato l'ok.

Il driver ha cominciato a chiudere tutto, ma i programmi che avevano in giro lavoro non salvato, invece che chiudersi, si sono fermati chiedendo correttamente se salvare.

Stavo chiudendo anche quelli, quando mi sono accorto che il driver aveva già iniziato l'installazione!

Più tardi il riavvio è stato inevitabile, ma non era automatico. Se i programmi con cui volevo continuare a lavorare avessero avuto tutti un qualsiasi documento non salvato, sarebbero rimasti aperti e intanto il driver si sarebbe installato ugualmente.

Chi ha orecchie per intendere.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>