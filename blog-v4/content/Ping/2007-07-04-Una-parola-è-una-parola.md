---
title: "Una parola è una parola"
date: 2007-07-04
draft: false
tags: ["ping"]
---

<cite>Ha! Hai già chiuso con i venerdì del gioco!</cite>

Per niente. Solo un gran casino nell’ambito di una riorganizzazione piuttosto radicale del lavoro voluta a lungo e finalmente realizzata che mi porterà, tra l’altro, ad avere progressivamente più tempo per questo blog, che mi piace. Merito di chi legge più che di chi scrive… in qualche modo mi sdebiterò.

Il punto tuttavia erano i venerdì del gioco. Siccome sono un po’ in ritardo gioco in casa e approfitto del suggerimento di Carlo: venerdì 6 luglio si gioca a <a href="http://www.runescape.com" target="_blank">Runescape</a>, dalle 13 alle 14, stanza iChat <em>gamefriday</em> per coordinarsi. L’ambiente è quello del gioco di ruolo, quindi non aspettiamoci grandi risultati di gioco, ma un assaggio speriamo succulento sì.