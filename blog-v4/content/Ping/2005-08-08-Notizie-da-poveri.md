---
title: "Notizie per poveri"
date: 2005-08-08
draft: false
tags: ["ping"]
---

Mica di denaro. Di spirito e capacità logiche

Sarà la calura, sarà la mancanza di ferie, sarà la granita troppo fredda dopo il branzino al pepe, ma non capisco.

Il 6 giugno Steve Jobs è salito sul palco del raduno mondiale degli sviluppatori dicendo che Mac passava ai processori Intel. A ruota Phil Schiller (il quale non dice una virgola se non è controfirmata da Jobs) ha spiegato che, se qualcuno vuole installare Windows sui Mac con processore Intel, Apple non lo impedirà. Contemporaneamente, come è ovvio, farà di tutto per impedire che Mac OS X possa girare su computer non Mac, processore o non processore.

Sono passati due mesi. Adesso girano sedicenti <a href="http://www.macnn.com/articles/05/08/05/intel.mac.os.restricted/">notizie</a> come quelle che Windows può essere installato sui Mac (come Apple ha detto sessanta giorni fa) e che Mac OS X supporta un meccanismo per impedire l'installazione su computer non Mac (come Apple ha detto sessanta giorni fa).

Questo, si badi bene, su computer che potrebbero essere completamente diversi da quelli che Apple venderà davvero da qui a un anno. Oltre a non essere una notizia, non ha alcuna utilità concreta.

Non capisco proprio. E sì che il branzino sembrava davvero buono.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>