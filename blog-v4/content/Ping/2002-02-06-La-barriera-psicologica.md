---
title: "La barriera psicologica logora chi ce l’ha"
date: 2002-02-06
draft: false
tags: ["ping"]
---

Il gigahertz di PowerPC G4 e l’effetto autoradio

Secondo <link>Punto Informatico</link>http://www.punto-informatico.com/p.asp?i=38857 i nuovi Power Macintosh con processore (o processori) da un gigahertz fanno superare ad Apple una “barriera psicologica”.
Secondo me le ragioni per comprare un Power Mac sono mille altre, a cominciare dalla facilità d’uso per passare dalle migliori interfacce grafiche del mondo alla versatilità delle macchine biprocessore usate con Mac OS X per arrivare al puro e semplice design, che conta, anche nel mondo dei computer.
Ma chi vive nel mondo dei Pc, grigio squadrato e inefficiente, capisce una sola logica: più gigahertz, più velocità (come se i gigahertz, invece, non fossero una <b>frequenza</b>). Un po’ come esibire l’autoradio con tanti watt di uscita, invece di ascoltare buona musica al volume giusto.

<link>Lucio Bragagnolo</link>lux@mac.com