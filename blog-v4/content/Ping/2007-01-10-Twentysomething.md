---
title: "Twentysomething, o: del dopo-keynote"
date: 2007-01-10
draft: false
tags: ["ping"]
---

Quando sono arrivato in chat c'erano quasi una ventina di persone, numero toccato e superato man mano che arrivava gente nuova. Fino quasi a mezzanotte abbiamo condiviso l'evento. Entrando nella chatroom, mi sono sentito come quando ci si trova a mangiare una pizza e si arriva dieci minuti dopo. Tavolata piena e piacevolissima. Grazie a tutti. :-)

