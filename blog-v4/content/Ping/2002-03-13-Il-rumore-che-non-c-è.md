---
title: "Il rumore che non c’è"
date: 2002-03-13
draft: false
tags: ["ping"]
---

Come risolvere uno sciocco problema ancora presente in Mac OS X

Se il sottosistema addetto al suono di Mac OS X non emette alcun rumore per trenta secondi, va automaticamente in stop e, quando richiamato, ci mette tre o quattro secondi a reagire, alterando a volte la sincronia tra suoni e immagini. Funzione ottima per i portatili, dove occorre risparmiare energia; un po’ meno quando il portatile è alimentato o sui desktop, che sono alimentati sempre. Le macchine affette sono gli iBook dal (Dual Usb) in poi, i PowerBook G4 (Gigabit Ethernet) e le macchine desktop del 2002. Fortunatamente un programmatore di nome Jonathan Feinberg ha scritto
<link>KeepSoundAwake</link>http://mrfeinberg.com/KeepSoundAwake/, programmino che ogni venti secondi fa suonare un non-suono, assolutamente muto, che tiene sveglio l’audio del Mac. Da non usare quando si va a batteria, ma utile in molti altri casi. Nel frattempo,
<link>lamentarsi</link>http://www.apple.com/it/macosx/feedback con Apple, che metta a posto il bug.

<link>Lucio Bragagnolo</link>lux@mac.com