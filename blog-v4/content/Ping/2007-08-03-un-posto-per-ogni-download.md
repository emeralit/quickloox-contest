---
title: "Un posto per ogni download"
date: 2007-08-03
draft: false
tags: ["ping"]
---

Attenzione: inoltrandosi in agosto, aumenta il rischio di trovare tra queste righe temi di piccola programmazione, scripting e altre materie esoteriche che non c'è mai tempo per sviluppare compiutamente durante la <em>regular season</em>.

Per esempio, è molto comprensibile e preciso <a href="http://www.nullsense.net/2007/3/8/applescript-folderactions-on-osx" target="_blank">questo script</a> che smista il contenuto di una cartella download in altre cartelle secondo il tipo di file (video, audio, immagini eccetera).

Se si hanno altri gusti, ecco una <a href="http://homepage.mac.com/dougeverly/folderorg.html" target="_blank">folder action</a> per lo stesso scopo.

Guarda caso, mi risulta che in Leopard esista una cartella Download predefinita.

Grazie a <strong>Dany</strong> per la segnalazione!

P.S.: domani, per chi fosse in zona, ci si vede a <a href="http://www.allaboutapple.com/speciali/hone.htm" target="_blank">H&#244;ne</a>. :-)