---
title: "AppleScript e la moneta cattiva"
date: 2008-02-28
draft: false
tags: ["ping"]
---

Se ho voglia di uscire, allora gioco a Dungeons &#38; Dragons con gli amici. Altrimenti, gioco a World of Warcraft con gli amici.

Prendiamo decisioni tutti i giorni. Lo fa anche AppleScript. Non è difficile; basta ricordarsi che <em>se</em> si dice <em>if</em>, <em>allora</em> si dice <em>then</em> e <em>altrimenti</em> si dice <em>else</em>. Diversamente dagli umani, AppleScript non capisce quando abbiamo finito i <em>se</em> e dobbiamo metterci un <code>end if</code> che non appartiene né all'italiano né all'inglese, ma è tutto.

L'AppleScript che segue mostra il funzionamento di una moneta un po' bislacca. Lo lascio volutamente senza troppe spiegazioni. Per funzionare ha bisogno di estrarre un numero a caso e compiere un test per capire se un numero è pari o dispari. Se qualcosa non è chiaro, sono ammesse domande. Se qualcosa è chiaro, si è compreso molto di AppleScript!

Il tutto va naturalmente inserito nello Script Editor e azionato con il pulsante Esegui.

<code>set moneta to (random number from 0 to 2)</code>
<code>if moneta = 0 then</code>
<code>	display dialog "Chi ha scritto un programma cos&#236; sciocco che non sa lanciare una moneta?"
</code>	
<code>else if (moneta / 2) as integer = moneta / 2 then</code>
<code>	display dialog "Croce!"</code>
<code>else</code>
<code>	display dialog "Testa!"</code>
<code>end if</code>

Da adesso, la periodicità degli interventi su AppleScript cambia. Non più ogni sette giorni, ma il 7, il 14, il 21 e il 28 di ogni mese.