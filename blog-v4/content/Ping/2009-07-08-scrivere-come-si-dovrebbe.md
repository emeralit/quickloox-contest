---
title: "Scrivere come si dovrebbe"
date: 2009-07-08
draft: false
tags: ["ping"]
---

Si chiama 2008, ma è ugualmente la nuova versione di TeX, il linguaggio di impaginazione e composizione di testo più avanzato che esista al mondo.

Pesa più di un gigabyte MacTex, ma si installa automaticamente nel giro di quattro-otto minuti.

<a href="http://www.tug.org/mactex/" target="_blank">MacTeX 2008</a> è la parte software. Il <i>know-how</i> ce lo può mettere, per cominciare, l'ottima <a href="http://www.macworld.it/blogs/ping/?p=2272" target="_self">guida in italiano di Lorenzo Pantieri</a>.

Un altro modo per combattere analfabetismo di ritorno, sciatteria e bruttura.