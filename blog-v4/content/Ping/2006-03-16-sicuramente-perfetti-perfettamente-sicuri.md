---
title: "Sicuramente perfetti, perfettamente sicuri"
date: 2006-03-16
draft: false
tags: ["ping"]
---

Il <a href="http://docs.info.apple.com/article.html?artnum=303453" target="_blank">Security Update 2006-002</a> è andato a buonissimo fine. I riavvii indesiderati, non richiesti da un aggiornamento, restano due da inizio anno. Sono passati quasi 74 giorni (esattamente 73 giorni e 22 ore, più di un quinto dell'anno) e la media viaggia verso un riavvio indesiderato ogni 37 giorni.

Da notare che il Security Update sistema tre problemi minori introdotti proprio con il Security Update 2006-001. Nessuno è sicuramente perfetto, men che meno i programmatori. Nessuno è perfettamente sicuro, ma i Mac adesso dovrebbero esserlo ancora di più.