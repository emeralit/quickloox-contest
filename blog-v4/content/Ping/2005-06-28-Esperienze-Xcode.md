---
title: "Nuove esperienze"
date: 2005-06-28
draft: false
tags: ["ping"]
---

Tra gli sviluppatori c'è chi non sta perdendo tempo

Al convegno mondiale degli sviluppatori Apple Steve Jobs ha reiterato l'invito, un po' pressante, ad adottare Xcode come sistema di sviluppo dei programmi per Mac.

La cosa bella è che alcuni sviluppatori, come <a href="http://xcode-experiences.blogspot.com">Marshall Clow</a>, hanno aperto blog in cui raccontano della loro esperienza da switcher, dell'ambiente di sviluppo però.

Per tutti quelli che fanno programmazione oltre la dose modica quotidiana di AppleScript sono letture utili, che aiuteranno sicuramente nella transizione.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>