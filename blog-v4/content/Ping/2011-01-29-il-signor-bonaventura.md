---
title: "Il signor Bonaventura"
date: 2011-01-29
draft: false
tags: ["ping"]
---

Chi ha letto qualcosa del passato lo conoscerà, il personaggio disegnato da <a href="http://www.sto-signorbonaventura.it/" target="_blank">Sergio Tofano</a> quando ancora i &#8220;fumetti&#8221; avevano le rime baciate sotto ogni illustrazione a raccontare la storia. Il signor Bonaventura attraversava vicende alterne ma alla fine in qualche modo la fortuna gli arrideva e incassava un milione (che poi divenne miliardo per adeguarsi all'inflazione).

Mentre fervono le discussioni su Mac App Store, gli autori di Pixelmator, che hanno scelto dal primo momento di trasferirvisi armi e bagagli rinunciando a ogni altro mezzo di distribuzione, <a href="http://www.pixelmator.com/weblog/2011/01/25/pixelmator-grosses-1-million-on-the-mac-app-store" target="_blank">annunciano</a> che il loro programma fai-a-meno-di-Photoshop-e-spendi-molto-meno ha incassato un milione di dollari in venti giorni di vendita.

Siccome il programma è in vendita in offerta speciale a 29 dollari (<a href="http://itunes.apple.com/it/app/pixelmator/id407963104?mt=12" target="_blank">23,99 euro</a> e questo <i>link</i> funziona solo con Mac OS X 10.6.6) si fanno presto i conti. In venti giorni il programma ha guadagnato quasi trentacinquemila nuovi clienti e, tolta la percentuale che va ad Apple, l'azienda ha incassato settecentomila dollari, cioè trentacinquemila dollari al giorno.

Pixelmator non è Adobe né Microsoft, ma una azienda piccola. Per loro, cito dal <i>blog</i>, annunciare questi numeri è <i>estasiante</i> e <cite>un successo cos&#236; straordinario sa di fantastico</cite>.

Forse sono stati i signori Bonaventura di Mac App Store. Forse rimarranno gli unici a vedersi cambiata la vita. Siccome però guadagnano vendendo prodotti, vuol dire che il loro prodotto è stato comprato. Hanno incassato molto in poco tempo, intanto che molti approfittavano di Mac App Store per portarsi a casa una copia di Pixelmator a prezzo speciale.

L'azienda è contenta, i clienti paganti e disposti a pagare aumentano e certo nessuno è stato costretto a comprare. Mac App Store è cos&#236; sbagliato?

Attendiamo altre vicende e altre situazioni. Nessuna però potrà prescindere dalla situazione preesistente. Dalle affermazioni di Pixelmator la questione sembra essere non tanto che abbiano ottenuto successo, quanto che prima, senza Mac App Store, i numeri fossero ben diversi. E si tenga presente che a Mac App Store accede attualmente solo un sottoinsieme di utilizzatori di Mac.

