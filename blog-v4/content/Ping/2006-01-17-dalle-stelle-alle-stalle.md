---
title: "Dalle stelle alle stalle"
date: 2006-01-17
draft: false
tags: [ping]
---

Difficile che passino dieci giorni senza qualcosa di concreto in arrivo da Apple o addentellati. Tra iPod, portatili costosi, portatili economici, desktop da casa, desktop professionali, software semplice e software superprofessionale, monitor, accessori, novità, servizi Internet, server, update di sistema, aggiornamenti di sicurezza, annunci finanziari, andamento delle azioni, dichiarazioni del vertice, prodotti di terze parti c’è da perdersi. E c’è un sacco da studiare.

C’è una categoria di persone che ha risolto il problema. Parla solo di cose che non ci sono. Prima degli annunci parla di che cosa arriverà. Dopo gli annunci, parla di che cosa non è arrivato (e arriverà, ovvio, come no? la prossima volta, visto che non era questa).

Mi ricorda la differenza tra astronomi e astrologi. I primi guardano le stelle e fanno progredire la scienza. I secondi guardano le stelle e prevedono il tuo futuro. I secondi sono più furbi: non studiano niente e fanno pure i soldi.

È interessante riflettere su questo. Gli astrologi e gli esperti di quello che non c’è fanno i soldi. Alle spalle di chi?
