---
title: "L’anno che è venuto"
date: 2004-01-07
draft: false
tags: ["ping"]
---

Riflessioni a caldo sul keynote di Macworld Expo

E così Steve Jobs ha presentato le prossime novità di Apple per l’inizio del 2004. Mi è venuto da pensare.
Apple fa iBook e nel 2003 li ha portati a G4 (significativo.)
Apple fa PowerBook e nel 2003 li ha portati alla linea alluminio (significativo).
Apple fa iMac e nel 2003 li ha portati allo schermo a 20” (significativo).
Apple fa Power Macintosh e nel 2003 li ha portati a G5 (epocale).
Apple fa Mac OS X e nel 2003 lo ha portato a Panther (significativo).
Apple fa l’iTunes Music Store e nel 2003 lo ha fatto partire (significativo).
Apple fa Final Cut Pro e nel 2003 lo ha portato alla versione 4 (significativo).

Mancavano gli iPod, Final Cut Express, iLife e Xserve. Eccoci serviti. Unica eccezione: la linea dei monitor. Ma per il resto gli ultimi dodici mesi sono stati un rinnovo pressoché completo, totale e come minimo significativo di tutti i prodotti base di Apple.

Non è affatto poco.

<link>Lucio Bragagnolo</link>lux@mac.com