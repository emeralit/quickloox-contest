---
title: "Presentat'… March"
date: 2006-03-28
draft: false
tags: ["ping"]
---

Non sopporto le presentazioni e i software per le presentazioni. Quando ho potuto ho sempre fatto cose molto semplici in Html, se non addirittura sequenze di immagini in libertà.

Avevo però un problema da poco tempo e molta criticità e, disperato (per scrivere dieci righe di Html ci metto dodici ore), ho aperto Keynote 2.

Rimango delle mie opinioni, ma accidenti se è facile.