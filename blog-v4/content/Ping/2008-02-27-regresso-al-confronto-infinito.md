---
title: "Regresso al (confronto) infinito"
date: 2008-02-27
draft: false
tags: ["ping"]
---

Mi chiedeva se c'è un modo per confrontare due file di testo. Ho pensato che lo faccio d'abitudine con <a href="http://www.barebones.com/products/bbedit" target="_blank">BBEdit</a>, che è pagamento.

Però l'idea di pagare per avere qualcosa che vale dà fastidio a molti e allora ho pensato a <a href="http://www.barebones.com/products/textwrangler" target="_blank">TextWrangler</a>, che è gratis.

Certo, resta sempre da scaricare un file da Internet. Perché allora non usare FileMerge, dentro i Developer Tools di Apple, cartella <code>/Developer/Applications/Utilities</code>?

S&#236;, ma vanno installati. Il comando <code>diff</code> dentro il Terminale, invece, è già pronto da usare. Addirittura, il comando

<code>diff -rq cartella1 cartella2</code>

confronta tra loro due cartelle, comprese le sottocartelle.

Usare il Terminale è sempre poco rassicurante, ma se niente va bene forse è il caso di non porre la domanda.