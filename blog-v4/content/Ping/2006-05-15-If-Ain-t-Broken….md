---
title: "If It Ain't Broken…"
date: 2006-05-15
draft: false
tags: ["ping"]
---

Se non è rotto, non lo riparare (<em>Don't Fix It</em>). Se tutta la gente che lancia Utility Disco per verificare un disco che funziona perfettamente facesse invece un backup regolare, risparmierebbe tempo.

Se tutto il tempo speso a &ldquo;ripulire&rdquo; cartelle System, &ldquo;riordinare&rdquo; i font (ma in che ordine?), &ldquo;ripulire&rdquo; le preferenze, andasse in <a href="http://www.oxixares.com/glucas/" target="_blank">elaborazione di numeri primi</a>, la comunità dei matematici sarebbe più contenta.

In alternativa c'è sempre la <a href="http://setiathome.berkeley.edu/" target="_blank">ricerca degli extraterrestri</a>. Obiettivo ben più a portata che non riorganizzare un disco rigido meglio di quanto sa farlo un sistema operativo.