---
title: "Good tasti"
date: 2006-06-14
draft: false
tags: ["ping"]
---

Mi sto abituando a usare <a href="http://mail.google.com" target="_blank">Gmail</a> senza il mouse, come il resto dell'esperienza Mac. Se la gente che fa web imparasse da Google, saremmo più avanti di dieci anni. In quanti sono capaci di mettere, se non altro, le shortcut da tastiera via Html con l'attributo <a href="http://www.cs.tut.fi/~jkorpela/forms/accesskey.html" target="_blank">accesskey</a>?

Pochi o nessuno. Basta navigare per accorgersene. Miliardi di pagine con davanti una meravigliosa introduzione Flash, che non serve a nulla e stufa dopo tre secondi, e nessuno che pensi all'usabilità.

Che è un segno dei tempi. Tutti a guardare nell'ombelico del proprio sito, <em>uh quant'è bello oddio come son bravo</em>, ce ne fosse uno che si preoccupa di chi quelle pagine dovrà vederle, e come.

P.S.: è sempre valida l'offerta di un invito in Gmail per chi mai dovesse averne bisogno. :-)