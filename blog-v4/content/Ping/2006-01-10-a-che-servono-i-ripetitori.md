---
title: "A che servono i ripetitori"
date: 2006-01-10
draft: false
tags: ["ping"]
---

Ha compiuto felicemente la settimana di vita l'<a href="http://www.repubblica.it/2005/f/sezioni/scienza_e_tecnologia/google2/googlepc/googlepc.html">articolo</a> di Alessio Balbi di Repubblica. Un pc targato Google? Forse venerdì l'annuncio.

Dal che si deduce che Balbi, se il pc ci sarebbe stato o meno, non aveva la minima idea. Nell'articolo non prova neanche a sbagliare da solo; si limita a ripetere quello che altri hanno scritto, senza controllarlo e senza neppure esprimere una opinione.

Insomma, un lavoro da fotocopiatrice, o da monaco amanuense dell'alto Medioevo. Ma, quando si parla di informatica, a che serve Repubblica?