---
title: "Chiosa di lux alla Legge di Murphy"
date: 2006-02-23
draft: false
tags: ["ping"]
---

Se cerchi due file, visualizzare uno di essi richiede lo scorrimento della finestra del Finder.