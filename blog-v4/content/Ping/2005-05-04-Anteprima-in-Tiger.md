---
title: "Tiger in Anteprima"
date: 2005-05-04
draft: false
tags: ["ping"]
---

Tanti miglioramenti su ciascun programma. Per esempio…

Mi perdonerai se parlo a ripetizione di Tiger. È che il sistema è appena arrivato e come fa un doppio clic ti accorgi di qualcosa. Nel contempo qualcuno ha avuto il coraggio di scrivere che tutto sommato è solo un aggiornamento minore, che non merita la spesa, mentre a me pare valga il doppio del suo prezzo.

Prendiamo Anteprima. Programma cenerentolo, umile, di servizio. Apple ha aggiunto uno strumento di ritaglio e la possibilità di inserire annotazioni sopra i documenti. Nel programma sono integrati il servizio di cattura schermate e una funzione di slideshow per vedere più immagini una dopo l'altra.

Cosa che a me piace più di tutte: ora, se la barra degli strumenti è impostata a solo testo, è comunque possibile indicare un numero di pagina (prima non si riusciva).

Acrobat Reader, chi lo apre più?

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>