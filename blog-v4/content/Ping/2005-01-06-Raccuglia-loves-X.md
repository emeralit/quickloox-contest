---
title: "Un amore di sistema"
date: 2005-01-06
draft: false
tags: ["ping"]
---

Non è solo il diavolo a nascondersi nei dettagli

Mi scrive Alex Raccuglia:

<em>La cosa più bella del Mac è che ti diventa amico senza che tu te ne accorga: l'icona di iPhoto comprende anche il modello della fotocamera da te utilizzata. Se hai una Canon come me mette la Canon, se hai una Nikon, come te…</em>

<em>Io ADORO questo sistema operativo… :-)</em>

Vorrei solo fare notare che Alex non è un ragazzino in tempesta ormonale ma un apprezzato sviluppatore di software, produttore di un vero lungometraggio - <a href="http://www.progettoskarr.net">Skarr</a> - interamente creato con Macintosh nonché professionista affermato.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>