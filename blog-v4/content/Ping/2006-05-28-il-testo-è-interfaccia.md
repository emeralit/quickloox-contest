---
title: "Il testo è interfaccia"
date: 2006-05-28
draft: false
tags: ["ping"]
---

Era ora. Uno dei designer web più autorevoli mondo, Derek Powazek, afferma a chiare lettere <a href="http://www.alistapart.com/articles/learntowrite" target="_blank">A tutti i designer: imparate a scrivere!</a>

Dedicato a chi pensa che il cuore di un sito siano cinquanta secondi di Flash inutile messi all'inizio di roba sgrammaticata.