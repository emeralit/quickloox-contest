---
title: "Semplicità è felicità"
date: 2009-02-12
draft: false
tags: ["ping"]
---

Questi signori hanno portato il concetto all'esasperazione goliardica. Oltre la provocazione però è difficile dare loro torto, quando spiegano che se nella preparazione di una pagina Html un Css richiede più di 47 minuti di tempo <a href="http://giveupandusetables.com/" target="_blank">ha più senso usare una tabella</a> (e mettono a disposizione il timer che dopo 47 minuti scodella il codice della tabella pronto da usare).

Allontenerei pure la quetione dallo Html. Recentemente ho incoraggiato l'estensore di un progetto da proporre a crearne una versione da una pagina, invece di una pesante presentazione Keynote e più utile, per convincere il destinatario, del progetto vero, che he comprende sette o otto.

L'idea per il mio prossimo libro in lavorazione è stata accettata dall'editore dietro presentazione di una scaletta solo testo e una mail di quattro paragrafi. La scaletta ovviamente era buona e l'idea del libro è stata accettata perché buona, non perché la mail era breve.

Tuttavia, le cose brevi e semplici sono quelle vincenti, pur essendo le più difficili da realizzare. Guarda le Preferenze di Sistema di Mac OS X e i Pannelli di controllo di Windows: i secondi traboccano di funzioni e testo inutile. Come diceva Steve Jobs, il buon design non è quello che accumula funzioni, ma quello che toglie tutto quanto non è essenziale. Il buon design aumenta anche la felicità globale. :-)