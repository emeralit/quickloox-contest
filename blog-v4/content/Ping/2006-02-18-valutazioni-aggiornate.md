---
title: "Valutazioni aggiornate"
date: 2006-02-18
draft: false
tags: ["ping"]
---

Sono l'ultimo ad averlo scoperto, ma la <strong>Calcolatrice</strong> di Tiger, che all'inizio aveva smesso di farlo, ora aggiorna regolarmente i tassi di cambio delle valute.

Prezioso, direbbe Bisio nella pubblicità.