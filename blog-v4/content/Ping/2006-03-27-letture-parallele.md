---
title: "Letture parallele"
date: 2006-03-27
draft: false
tags: ["ping"]
---

&Egrave; interessante leggere uno a fianco dell'altro <a href="http://www.repubblica.it/2006/c/sezioni/scienza_e_tecnologia/rivolmela/settamela/settamela.html" target="_blank">Zucconi su Repubblica</a> e Andrew Thomas su <a href="http://www.theinquirer.net/?article=30551" target="_blank">The Inquirer</a>.

Uno parla di <cite>setta di fanatici</cite>, l'altro di <cite>gruppo religioso fondamentalista</cite>. Uno scrive di <cite>massoni della Mela</cite>, mentre per l'altro l'amministratore delegato della società sarebbe tale Ron L. Jobs, in assonanza con lo scrittore di fantascienza fondatore di Scientology. Uno definisce il Mac <cite>comico</cite> e così via.

Se parlassi di cattiva informazione italiana il direttore mi rampognerebbe, quindi niente equivoci. Zucconi è un genio del giornalismo informatico e Repubblica online è uno splendido sito per sapere tutto sul Mac. The Inquirer scrive invece cose semplicemente insultanti per quanti, come me almeno, usano il Mac perch&eacute; è il miglior computer sulla piazza.