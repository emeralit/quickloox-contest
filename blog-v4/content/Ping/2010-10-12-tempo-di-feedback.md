---
title: "Tempo di feedback"
date: 2010-10-12
draft: false
tags: ["ping"]
---

L'ultimo che ho inviato riguarda Pages. Quando si indica il colore di sfondo di un paragrafo e lo stesso paragrafo viene bordato sui quattro lati, il colore di sfondo si adatta alla distanza verticale dei bordi, ma non a quella orizzontale: cos&#236; lo sfondo colorato si interrompe all'estremo destro e sinistro del testo prima di arrivare a toccare il bordo.

È un peccato, perché in fase di definizione degli stili sarebbe molto utile poter ottenere questi effetti attraverso come proprietà del paragrafo, anziché come aggiunte da inserire in altro modo. Aggiungere un rettangolo colorato sotto il testo con Pages è un attimo, solo che non fa parte dello stile del testo e va rifatto manualmente ogni volta che serve.

Già che c'ero ho lasciato un altro <a href="http://www.apple.com/feedback/pages.html" target="_blank">feedback</a>, questa volta propositivo, chiedendo di poter avere bordi di paragrafo arrotondabili, naturalmente quando i bordi formino angoli.