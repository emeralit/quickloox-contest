---
title: Un sistema di transizione
date: 2002-08-26
draft: false
tags: ["ping"]
---


Non lasciarti ingannare: l’affermazione di Mac OS X non è ancora cominciata

Fa un po’ specie leggere gente che si lamenta di qualche aspetto di Mac OS X “visto che c’è da più di un anno”.
La verità è che cambiare sistema operativo non è come cambiare sapone (anche se c’è chi resta indeciso sul provider di Adsl per settimane, forse senza sapere che comunque la linea arriva da Telecom Italia). I tempi sono lunghi.
Non ho lo spazio per tutti i numeri ma, allo stato attuale delle cose, Mac OS X viene usato all’incirca dal 10% di tutti noi.
Da qui a un anno si arriverà al 50% circa e solo allora Mac OS X sarà diventato veramente il sistema operativo ufficiale di Apple, a prescindere da tutti i proclami fatti per questioni di marketing. Adottarlo adesso significa pagare qualche scotto pionieristico. Ma anche essere in vantaggio sui ritardatari e imparare mentre loro restano all’oscuro.

<link>Lucio Bragagnolo</link>lux@mac.com
