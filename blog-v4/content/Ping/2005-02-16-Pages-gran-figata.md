---
title: "Pagine da antologia"
date: 2005-02-16
draft: false
tags: ["ping"]
---

Quando un programma vale il doppio del suo prezzo anche per metà pacchetto

Ho avuto l'occasione di giocare un po' con Pages, il <em>word processor with an incredible sense of style</em> della suite iWork, e sono e-n-t-u-s-i-a-s-t-a-.

Per motivi professionali mi sono visto passare tra le mani tutti i programmi di impaginazione professionale. Potentissimi e inaccessibili a un essere umano normale.

Come word processor, Pages non è particolarmente potente; come impaginatore, nemmeno.

Ma ha una facilità di utilizzo che solo Apple poteva tirare fuori dal cilindro. In un'ora ho risolto con piena soddisfazione stupidi problemi di layout grafico che su AppleWorks facevano perdere la nottata. Questo è veramente il programma di scrittura e impaginazione semplice che mancava, per tutti quelli che come me hanno bisogno di un decimo delle capacità di InDesign a un decimo, appunto, del prezzo di InDesign.

iWork costa se non sbaglio (è notte fonda, scusami) 79 euro e comprende Pages e Keynote. Se anche non te ne frega niente di Keynote, compra iWork per usare Pages, che da solo, sia pure in versione 1.0, vale il doppio di quella cifra.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>