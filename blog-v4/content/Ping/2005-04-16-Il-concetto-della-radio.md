---
title: "Il concetto della radio"
date: 2005-04-16
draft: false
tags: ["ping"]
---

Piccola e spontanea lezione su come sono pensati gli iPod

Una mia conoscente ha ricevuto in regalo un iPod shuffle e lei, buona ascoltatrice di musica, è invece utente di computer appena occasionale.

Parlandomi dell'esperienza con il regalo ha detto testualmente <em>è la radio migliore che c'è perché trasmette proprio la tua musica</em>.

Così dicendo ha messo un aggraziato macigno sulle polemiche di quelli che giudicano iPod non completo senza la radio o altre funzioni che l'oggetto non è pensato per essere, anche se potrebbe benissimo fare.

iPod shuffle: la radio migliore che c'è. Se serve ad Apple, le cedo lo slogan gratis. D'altronde non è mio.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>