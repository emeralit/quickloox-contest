---
title: "Se Exposé non ci arriva"
date: 2005-04-07
draft: false
tags: ["ping"]
---

Trucchetto per riappropriarsi delle finestre orfane

L'ho scoperto stanotte. Nel mio intento di evitare quanto possibile l'uso del mouse a favore della tastiera, sto usando con maggiore frequenza Exposé.

A volte succede di operare su più finestre ma, al momento di selezionarne una con i tasti cursore (lo so che potrei fare un clic con il mouse, ma è tutta conoscenza), si scopre che in qualche modo rimane fuori dalla successione e non si riesce a selezionare.

Il più delle volte basta selezionare un'altra finestra, diversa da quella che era in primo piano al momento di fare partire Exposé. All'F10 successivo Exposé dispone le finestre in modo diverso e quasi certamente permetterà di selezionare la finestra prima trascurata.

Lo so che pretendere di usare solo la tastiera in un computer nato con il mouse è ambizioso, ma ci sono implicazioni interessanti di interfaccia utente ed ergonomia che voglio approfondire.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>