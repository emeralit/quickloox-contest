---
title: "Nelle viscere di iMovie"
date: 2003-03-18
draft: false
tags: ["ping"]
---

Una volta non era così interessante scavare nelle applicazioni

Uno dei lati interessanti di Mac OS X è che il suo retaggio Unix rende i programmi molto più leggibili che in passato. Occorre sempre fegato, per carità, e fare quello che si vuole fare solo se si sa quello che si vuole fare. Ma molte cose sono davvero più facili.

Un esempio. Ti piacerebbe personalizzare il mondo in cui iMovie 3 manda i progetti a iDvd? Tipo fargli scegliere un nome di progetto differente, o aggiungere filmati a un progetto esistente invece di creare un progetto nuovo, o – per assurdo, ma neanche tanto – tentare di farlo funzionare con iMovie 2?

Beh. /Applications/iMovie.app/Contents/Resources/English.lproj/iMovieToiDVDWorkflow.

Se usi l’italiano, o altra lingua, sostituisci la lingua giusta a “English”. E scopri lo stesso quanto è facile scavare nelle viscere di iMovie.

<link>Lucio Bragagnolo</link>lux@mac.com