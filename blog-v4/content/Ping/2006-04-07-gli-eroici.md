---
title: "Gli Eroici"
date: 2006-04-07
draft: false
tags: ["ping"]
---

Torno su <a href="http://www.allaboutapple.com/" target="_blank">All About Apple</a>, perch&eacute; il club è uno dei più vivi e attivi sulla scena Mac italiana, anzi, direi il più vivo e vitale di tutti, senza offesa per nessuno.

Stasera organizzano per le 21, all'Hotel Garden in viale Faraggiana ad Albissola Marina (SV), una <a href="http://www.allaboutapple.com/ospiti/hernan/concerto.jpg" target="_blank">lezione-concerto</a> del pianista Hernan Laurentis dal titolo <em>I procedimenti dell'unità in Beethoven</em>.

Che c'entra Beethoven con il Mac? In tempi di Amici di Maria de Filippi, con tutto il rispetto, questo è <em>think different</em> ed è anche cambiare il mondo… una sonata alla volta. :-D