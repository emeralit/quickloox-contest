---
title: "Il problema che non c'è"
date: 2006-02-06
draft: false
tags: ["ping"]
---

Mi spieghi una cosa?

Apple ha annunciato a giugno scorso, agli sviluppatori, che lo hardware Intel arrivava a giugno. Poi ha anticipato di sei mesi e ha cominciato a presentare Mac con Intel. Ha anche dichiarato per bocca di Steve Jobs che intendono presentare macchine Intel su tutte le gamme entro fine 2006 invece che fine 2007. Accelerazione totale e inaspettata, di cui gli sviluppatori niente sapevano.

Adesso apro <a href="http://ranchero.com/netnewswire/" target="_blank">NetNewsWire Lite</a>, il mio reader Rss preferito. Sfoglio le notizie  a disposizione. Più o meno si abbraccia la scorsa settimana.

Senza nessuna pretesa di precisione nel conteggio:

annunciato <a href="http://www.markspace.com/missingsync_palmos.php" target="_blank">Missing Sync for Palm Os</a> per Mac Intel;
<ul type="circle">
	<li>Adobe conferma l'impegno a universalizzare la Creative Suite e annuncia l'aggiornamento a Intel per <a href="http://labs.macromedia.com/technologies/lightroom/" target="_blank">Lightroom</a> di prossima uscita;</li>
	<li><a href="http://www.maxon.net" target="_blank">Maxon</a> presenta Cinema 4D universalizzato;</li>
	<li><a href="http://www.ableton.com/index.php?main=news-archive&amp;sub=intel_mac_support" target="_blank">Ableton</a> presenta Live 5.2</a> universalizzato;</li>
	<li><a href="http://www.propellerheads.se/" target="_blank">Propellerhead</a> promette l'universalizzazione dei suoi programmi;</li>
	<li><a href="http://www.m-audio.com" target="_blank">M-Audio</a> presenta la beta dei suoi driver universalizzati;</li>
	<li>Bare Bones annuncia <a href="http://www.barebones.com/products/yojimbo/index.shtml" target="_blank">Yojimbo</a> universalizzato;</li>
	<li>annunciato <a href="http://www.tribeworks.com/" target="_blank">iShell 4.5 </a>per Mac Intel;</li>
	<li>Pangea rilascia <a href="http://www.pangeasoft.net/otto/" target="_blank">Otto Matic</a> universalizzato;</li>
	<li>presentato <a href="http://www.mailsteward.com/" target="_blank">MailSteward 5.0</a> universalizzato;</li>
	<li><a href="http://www.chronosnet.com/About/PR/universal.html" target="_blank">Chronos</a> annuncia l'universalizzazione dell'intero parco software;</li>
	<li>pronto <a href="http://ranchero.com/marsedit/" target="_blank">MarsEdit 1.1</a> universalizzato;</li>
	<li><a href="http://www.aspyr.com/games.php/mac/complete/" target="_blank">Aspyr</a> comunica ottimi risultati sui benchmark delle versioni universali preliminari di loro giochi come Doom 3, The Sims 2, Quake 4 e Knights of the Old Republic;</li>
	<li><a href="http://www.aspyr.com/games.php/mac/complete/" target="_blank">Barefeats</a> pubblica i testi di velocità di Doom 3, Unreal Tournament 2004 e altro su Mac con Intel.</li>
</ul>

Attenzione. Non è una ricerca. Ho semplicemmente letto i titoli dei feed. Non sono andato su <a href="http://www.versiontracker.com/macintel" target="_blank">VersionTracker</a> (dove ci sono oltre 400 programmi comparsi in meno di tre settimane), non ho controllato tutti gli annunci di nuovo software. Solo un'occhiata ai feed.

Qualcuno, a giugno scorso, opinava in libertà che le grosse software house non avrebbero avuto grossi problemi con la transizione, quelle medie e piccole avrebbero potuto averne e che gli sviluppatori singoli avessero già perso il treno.

I fatti dicono che, con sei mesi di anticipo sul previsto e senza preavviso, i programmmi universalizzati spuntano come funghi e non certo dalle sole grosse software house.

Era una presa in giro o solo incompetenza?