---
title: "L'utility delle cose"
date: 2010-03-22
draft: false
tags: ["ping"]
---

<a href="http://www.threemagination.com/capsee/" target="_blank">CapSee</a> è una vera utility.

Mostra con assoluta evidenza a video se è stato premuto il blocco maiuscole e la trovo imperdibile. Non perché sia gratuita.