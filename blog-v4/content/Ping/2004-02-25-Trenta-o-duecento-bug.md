---
title: "Security e update"
date: 2004-02-25
draft: false
tags: ["ping"]
---

Meglio trenta giorni o duecento?

Apple ha da poco reso disponibile un ennesimo Security Update. In pratica sono stati chiusi alcuni buchi di sicurezza essenzialmente secondari, entro un mese dalla loro scoperta. I sistemi vulnerabili, considerate le circostanze necessarie, sono stati pochi e sono rimasti vulnerabili – a bug noto – per poco tempo.

Il 10 febbraio scorso Microsoft ha ammesso l’esistenza di un bug da lei stessa definito critico, che permette a qualunque attaccante di prendere il controllo di un computer Windows che si trovi all’interno della stessa rete. Il bug è stato reso noto il... 18 di agosto 2003. A bug noto, tutto il parco macchine Windows del mondo è stato vulnerabile per quasi duecento giorni a un bug critico che consente, tanto per andarci pieno, di assumere il controllo totale del computer colpito.

Apple, peraltro, lavora malissimo sulla documentazione degli update alla sicurezza. Ma io dormo meglio con Mac OS X.

<link>Lucio Bragagnolo</link>lux@mac.com