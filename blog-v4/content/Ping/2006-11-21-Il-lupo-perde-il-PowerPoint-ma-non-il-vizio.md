---
title: "Il lupo perde il PowerPoint ma non il vizio"
date: 2006-11-21
draft: false
tags: ["ping"]
---

Sono alla <a href="http://www.osba.eu" target="_blank">Open Source Business Academy</a> organizzata da <a href="http://www.sourcesense.com/it/home" target="_blank">Sourcesense</a> e <a href="http://it.sun.com/" target="_blank">Sun Microsystems</a>.

Il primo relatore collega il Mac, apre la sua presentazione con Keynote e fa un figurone.

Il secondo relatore collega il Pc, apre la sua presentazione con PowerPoint. È più brutta, ma potrebbe dipendere solo dall'abilità tecnica (sono convinto invece che dipenda dal complesso dell'interazione utente-hardware-software, ma resta una mia opinione).

Comunque, dopo la terza <em>slide</em> l'avanzamento automatico si blocca. Il relatore si trova a dover richiamare la prossima slide con il menu contestuale di PowerPoint, e non fa una bella figura.

Ovviamente è una pura coincidenza. Da un caso particolare non si ricava una regola generale. A volte succede il contrario. Eccetera. Di fatto, oggi, è cos&#236;.