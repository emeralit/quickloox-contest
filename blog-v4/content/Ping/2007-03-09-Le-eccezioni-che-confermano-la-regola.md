---
title: "Eccezioni che confermano la regola"
date: 2007-03-09
draft: false
tags: ["ping"]
---

Ho trovato un altro programma conosciuto che funziona solo su Mac Intel e non su PowerPc: <a href="http://www.freeverse.com/heroes5" target="_blank">Heroes of Might and Magic 5</a> di Freeverse.

Adesso si tratta di capire se sia una scelta strategica o tecnica, come è stato per esempio per Adobe (tanto svelta a riciclare Premiere solo per Intel quanto ha saputo essere lenta pur di avere la Creative Suite 3 su tutte le piattaforme). Per me è la seconda, anche se non ho pezze di appoggio.

Viene da pensare a quanti hanno speso fiumi di inchiostro per spiegare quanto era difficile e faticosa la transizione a Intel, che nelle parole originali di Steve Jobs avrebbe dovuto essere tuttora in corso di questi giorni, mentre invece già da mesi non esistono più in vendita Mac PowerPc. Talmente difficile che, dei programmi fondamentali, non esiste più niente che non sia Universal.

Adesso è tempo per questi imbecilli di tramontare; stanno sorgendo gli imbecilli che <cite>PowerPc è obsoleto</cite>.