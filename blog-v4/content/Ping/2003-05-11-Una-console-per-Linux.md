---
title: "Una console per Linux"
date: 2003-05-11
draft: false
tags: ["ping"]
---

Quando ti accorgi che non è un gran che, la puoi riciclare

A Webbit ho visto un cosiddetto rack composto da quattro Xbox, la console di videogiochi Microsoft che testimonia come non sia possibile avere il monopolio dappertutto (nei videogame PlayStation e PlayStation 2 sono assolutamente irraggiungibili per vendite, software e base installata).

Le quattro console lavoravano come un solo computer, pilotato dal sistema operativo free Gnu/Debian.

Una bellezza.

<link>Lucio Bragagnolo</link>lux@mac.com