---
title: "L'oggi è il giocare di ieri"
date: 2010-04-23
draft: false
tags: ["ping"]
---

Sempre sull'onda del fiorire di iniziative Html5, ecco <a href="http://www.kesiev.com/akihabara/" target="_blank">Akihabara</a>, libreria che usa un piccolo insieme dello standard per creare videogiochi nello stile <i>otto bit</i> giapponese in voga negli anni ottanta.

Solo che stavolta girano nel <i>browser</i> senza bisogno di <i>plugin</i> esterni.

Importante: i giochini di esempio sono simpatici e l&#236; si fermano. Ma queste sono librerie. Qualsiasi programmatore cui ne serva un pezzo le prende, le usa, magari le migliora. Dagli esempi nascono esempi sempre migliori e via dicendo.

L'obiezione che Html5 sia pochissimo diffuso è fondata. Ricorda i tempi di quando, molti anni fa, raccomandavo che i siti fossero visibili da qualsiasi <i>browser</i>. Mi rispondevano che le alternative a Internet Explorer erano pochissimo diffuse. Oggi sfiorano il quaranta percento.