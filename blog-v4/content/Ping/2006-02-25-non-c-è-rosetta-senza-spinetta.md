---
title: "Non c'è Rosetta senza spinetta"
date: 2006-02-25
draft: false
tags: ["ping"]
---

Più che dei worm sono preoccupato dai siti che girano come gli organetti di una volta e scrivono per riflesso condizionato, con le frasi fatte.

Quando vedi una frase tipo <em>Più compatibilità con Rosetta</em>, vuol dire che la compatibilità era zero ed è salita a cento. Un programma non può essere più compatibile con Rosetta, o meno; o funziona, o non funziona.