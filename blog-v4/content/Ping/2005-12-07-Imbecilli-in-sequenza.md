---
title: "Quando è troppo è troppo"
date: 2005-12-07
draft: false
tags: ["ping"]
---

L'imbecillità in sequenza provoca danni in misura esponenziale

Riporto la mail che ha diramato <strong>Alessio Ferraro</strong>, dell'All About Apple Museum:

<cite>Questa notte la scuola elementare di Quiliano, il comune dove risiede il <a href="http://www.allaboutapple.com">Museo Apple</a>, è stato depredato da ladri senza scrupoli, che hanno lasciato senza computer l'aula informatica delle Scuole Elementari (10 postazioni).</cite>

<cite>La presente mail serve come appello: avete in casa dei vecchi PC (intendiamo PC Compatibili) ancora funzionanti, da donare alla Scuola?</cite>

<cite>Anche se non sono più modernissimi, potrebbero essere più che sufficienti per ricostruire l'aula informatica della Scuola Elementare, e il Comune vi sarebbe eternamente (e pubblicamente) riconoscente. Noi stessi avremmo provveduto, ma purtroppo sono richiesti computer PC Compatibili.</cite>

<cite>Nel caso, contattateci rispondendo a questa mail, vi metteremo in comunicazione diretta tra l'Assessore Alberto Ferrando del <a href="http://www.comune.quiliano.sv.it/">Comune di Quiliano</a> per verificare le modalità della 
donazione.</cite>

Ricapitolo: una banda di subumani ruba dentro una scuola elementare. La scuola ha a portata di mano non tanto il museo Apple (capisco che un Fat Mac o un Mac Portable non siano cose adatte), quanto il suo magazzino. Vi sono contenuti computer e pezzi in quantità per ricavarne sicuramente almeno una decina di Mac perfettamente in grado di collegarsi a Internet e usare programmi ragionevolmente moderni. Anche come soluzione provvisoria. Per non lasciare i bambini senza aula informatica. Da sostituire appena arrivano i computer nuovi.

No. Ci vogliono dieci PC. Devono essere PC.

Mi auguro che questo appello possa contribuire a fornire almeno uno dei mille PC che è giusto arrivino alla scuola di Quiliano. Se ne avessi uno, lo avrei già mandato.

Con un bigliettino: <cite>caro Assessore, nel regalare alla scuola elementare di Quiliano un PC per l'aula informatica depredata di recente, le chiedo: perché non provare con dieci PC e aggiungere un Mac? È meglio quando i bambini crescono imparando a scegliere.</cite>

<cite>Cordialmente,</cite>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>