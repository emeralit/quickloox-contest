---
title: "Webb.it si avvicina e i Mac si moltiplicano"
date: 2002-03-29
draft: false
tags: ["ping"]
---

Mica si può mancare all’evento informatico più interessante dell’anno

Altro che Smau! Il vero appuntamento dell’informatica è Webbit. Quest’anno si terrà a Padova, dal 4 al 7 luglio, e rappresenterà un’esperienza indimenticabile per approfondimento e divertimento quanto e più dell’anno scorso, dove hanno partecipato migliaia di persone.
Ci sono seminari condotti dalle aziende più cool della new economy, programmatori di straordinario talento disposti a scambiare conoscenze, la possibilità di portare il proprio computer e metterlo in rete con centinaia di altri, fare amicizie, trovare lavoro, imparare cose nuove, riposarsi oppure bruciare la propria candela da ambo le parti, del tutto a piacere.
Perché lo dico? Perché sarebbe bello vedere tanti Mac a <link>Webbit</link>http://www.webb.it e perché così sono sicuro di essere in compagnia.

<link>Lucio Bragagnolo</link>lux@mac.com