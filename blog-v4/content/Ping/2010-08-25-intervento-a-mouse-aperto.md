---
title: "Intervento a mouse aperto"
date: 2010-08-25
draft: false
tags: ["ping"]
---

La mia inabilità al fai-da-te è nota in tutto l'emisfero occidentale. Nonostante questo ho completato con successo la riparazione di un Mighty Mouse wireless che aveva cessato di funzionare a seguito di una caduta.

Il blocco interno della circuiteria era balzato fuori dal perno che lo tiene in posizione e assicura il contatto, ma questo l'ho saputo solo dopo. Prima ho dovuto aprire l'apparecchio e soprattutto rimetterlo a posto.

Si comincia facendo saltare con grande delicatezza e attenzione i punti di colla che tengono assicurato il guscio inferiore a quello superiore. Si parte da sotto i pulsanti laterali, dove c'è una fessura sufficiente a infilare qualcosa di molto sottile e resistente con cui fare leva gradualmente su tutto il perimetro, poco alla volta.

Una volta completata con successo la separazione dei due gusci, NON c'è da rimuovere l'anello di gomma che ammorbidisce lo scorrimento. Sembra un passo logicamente successivo, invece non serve a niente e bisognerà trovare un'idea per rimetterlo in posizione a cose fatte.

Per richiudere i due gusci ho usato comune <i>colla vinilica</i>, come direbbero su <a href="http://www.archivio.raiuno.rai.it/schede/2019/201920.htm" target="_blank">Art Attack</a>. Ottima dovesse capitare di dover riaprire l'oggetto. Con l'Attak non so come andrebbe a finire.