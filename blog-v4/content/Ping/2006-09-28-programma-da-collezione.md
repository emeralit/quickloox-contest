---
title: "Programma da collezione"
date: 2006-09-28
draft: false
tags: ["ping"]
---

Anche se il sito, devo dire, non mi ispira, può darsi che <a href="http://www.databasecreativity.com/" target="_blank">Movies Database</a> sia un buon programma di catalogazione dei film. Tutto sta a vedere se è solo americano o capisce anche il resto del mondo, che è lo stesso problema dei programmi di classificazione dei libri.