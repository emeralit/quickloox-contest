---
title: "Mannaggia a Gertrude"
date: 2005-02-02
draft: false
tags: ["ping"]
---

Mai citare senza controllare…

Come fa giustamente notare Mario rispetto al Ping di ieri, una rosa è una rosa è una rosa, ma lo ha detto Gertrude Stein e non William Shakespeare, come è venuto da scrivere a me.

Avrebbe un Mac mini, con un altro nome, lo stesso dolce profumo?

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>