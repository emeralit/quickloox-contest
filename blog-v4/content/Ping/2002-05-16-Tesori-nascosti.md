---
title: "Tesori nascosti"
date: 2002-05-16
draft: false
tags: ["ping"]
---

Che cosa ti aspetti di trovare nelle viscere di Mac OS X?

Bocciolo di melo: preferenza. Nontiscordardime: amore vero. Magnolia: dignità, perserveranza. Menta: virtù. Orchidea: bellezza, magnificenza. Petunia: la tua presenza allevia le mie pene. Tulipano giallo: amore disperato.
La rubrica Ditelo con i fiori di Donna Letizia? No; Mac OS X, usr/share/.
Curiosare dentro un sistema con base open source è una fonte inesauribile di sorprese.

<link>Lucio Bragagnolo</link>lux@mac.com