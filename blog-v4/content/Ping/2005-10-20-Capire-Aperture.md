---
title: "Capire Aperture"
date: 2005-10-20
draft: false
tags: ["ping"]
---

In attesa che arrivi Apple, spiego il posizionamento del suo nuovo programma per professionisti

Apple ha annunciato Aperture e già la gente si inventa paragoni assurdi con Photoshop. Come se fossero programmi concorrenti.

Provo a spiegarlo in modo semplice (anche se non lo sarà mai abbastanza):

iDvd… Dvd Studio Pro<br>
GarageBand… Soundtrack Pro<br>
iMovie… Final Cut Pro<br>
iPhoto… ???

Oggi come oggi, chiunque di noi può fare doppio clic su una miniatura in iPhoto e lanciare Photoshop.

Apple pensa che un fotografo professionista vorrebbe poter fare doppio clic da un programma di catalogazione professionale e lanciare Photoshop. iPhoto è un programma per tutti. <a href="http://www.apple.com/aperture">Aperture</a> è per chi ci deve lavorare seriamente. Any questions?

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>