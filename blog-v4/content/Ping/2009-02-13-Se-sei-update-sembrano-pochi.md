---
title: "Se sei update sembrano pochi"
date: 2009-02-13
draft: false
tags: ["ping"]
---

Premesso che vorrei vedere Snow Leopard girare su processori PowerPc, mi rendo conto che dell'<a href="http://support.apple.com/downloads/" target="_blank">aggiornamento di sicurezza 2009-001</a> hanno dovuto essere prodotte <em>sei varianti diverse</em>, per tutti gli incroci concepibili di Tiger, Leopard, <em>client</em>, <em>server</em>, PowerPc e Intel.