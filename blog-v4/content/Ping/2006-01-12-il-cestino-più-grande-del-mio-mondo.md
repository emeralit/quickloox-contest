---
title: "Il Cestino più grande del mio mondo"
date: 2006-01-12
draft: false
tags: ["ping"]
---

Ho l'abitudine di non vuotare mai il Cestino fino a quando non è necessario. Oggi ho vuotato il Cestino. 136.722 file. Nuovo record personale. :-)