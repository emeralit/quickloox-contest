---
title: "Chi cerca (meglio) trova (prima)"
date: 2008-08-05
draft: false
tags: ["ping"]
---

Mai provato a scrivere nel campo di ricerca di Spotlight, tipo, <em>kind:TextEdit</em> oppure <em>kind:disk image</em>?