---
title: "Ancora più Profiler"
date: 2006-04-04
draft: false
tags: ["ping"]
---

Ringrazio assai <strong>ilduca69</strong> per avermi fatto notare che, in tema di batterie e software per il conteggio, è sufficiente aprire <em>System Profiler</em> alla voce Energia (sotto Hardware) per avere i dati sul numero di cicli di carica e la capacità corrente della batteria.

Il software apposito fornisce i dati in forma più leggibile e immediata, ma non è dunque strettamente necessario.

Giurerei che le cose non stessero così, una volta, e suppongo che le nuove informazioni dipendano da qualche aggiornamento software che ha conferito nuove possibilità a System Profiler.