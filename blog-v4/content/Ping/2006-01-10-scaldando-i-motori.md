---
title: "Scaldando i motori"
date: 2006-01-10
draft: false
tags: ["ping"]
---

Questa <a href="http://www.ucomics.com/foxtrot/2006/01/09/" target="_blank">striscia di FoxTrot</a> spiega bene gli umori di prima di un keynote. :-)

Già che ci sono: per chi avesse commenti troppo lunghi, ho aperto una stanza di iChat di nome keynotemacworld (mela-g e scrivi il nome). Girano rumor che potrebbero comparire altri di Macworld… vedremo se è vero!