---
title: "My Generation"
date: 2009-03-28
draft: false
tags: ["ping"]
---

Quando scarico qualcosa via BitTorrent tengo aperto <a href="http://www.transmissionbt.com/" target="_blank">Transmission</a> anche dopo avere finito, in modo che la <em>ratio</em>, il rapporto tra quanti dati ho scaricato e quanti ne ho rimandati in rete su richiesta altrui, sia essenzialmente equa.

In poche parole, aiuto gli altri a scaricare tanto quanto gli altri hanno aiutato me.

C'è chi lo fa e ci sono invece i <em>freerider</em>, i predoni, che cercano sistematicamente il massimo vantaggio con il minimo sforzo.

Pensavo che le persone collaborative e i <em>freerider</em> fossero più o meno divise per età, con i più giovani a essere un po' meno altruisti.

Invece sono divise per autoironia. Chi si prende sul serio in modo esagerato prende e non dà niente in cambio.