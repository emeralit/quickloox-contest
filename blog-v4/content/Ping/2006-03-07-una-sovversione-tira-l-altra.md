---
title: "Una sovversione tira l'altra"
date: 2006-03-07
draft: false
tags: ["ping"]
---

Ho già raccontato di come mi è toccato scoprire il sistema di <em>versioning</em> <a href="http://subversion.tigris.org/" target="_blank">subversion</a>.

Oggi ho sbagliato ad applicare un comando e (giustamente, come ho scoperto dopo) mi si è aperto <strong>vim</strong> nel Terminale. Terrore e angosciata chiusura del Terminale come reazione immediata; a bocce ferme, timidamente come quando si saggia con l'alluce l'acqua del torrente di montagna ho riaperto il Terminale, digitato <code>man vim</code> e passato cinque minuti a imparare qualcosa di nuovo. Come minimo, a uscire da vim in modo civile.

Non ci si crede, ma leggere con calma cinque minuti di help rivela che il Terminale non è poi questa brutta bestia. &Egrave; solo questione di ignoranza, mia.