---
title: "Vendere il soft è hard"
date: 2005-08-12
draft: false
tags: ["ping"]
---

Il modello di business di Apple non è esattamente quello di una software house

A intervalli regolari salta fuori l'idea che Apple dominerebbe il mondo se diventasse un'azienda che produce solo software. L'idea è più o meno che vendere Mac OS X su tutti i computer compenserebbe le mancate vendite del Mac.

Il fatto è che Apple vende molto più software di una volta. I guadagni maggiori vengono da programmi ben più costosi di Mac OS X, come Final Cut, Shake e via discorrendo. Con tutto ciò, il fatturato software di Apple è circa un sesto del fatturato Mac (senza contare gli iPod).

Non ho voglia di fare i conti precisi, anche perché ho scritto queste cose più volte, tra mailing list e altro. La sintesi è che Apple dovrebbe vendere almeno dieci volte le copie attuali di Mac OS X dalla sera alla mattina, entrando in concorrenza frontale con una Microsoft abituata a lustri di concorrenza sleale e abuso di monopolio, pronta a buttare in marketing e in comportamenti scorretti una quantità di denaro da fare impallidire qualunque sforzo analogo da parte di Apple.

Può darsi che queste premesse siano sbagliate, ma chi me le contesta non porta mai un foglio elettronico con su un po' di numeri. Sarà un caso.

Buon Ferragosto a tutti. :-)

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>