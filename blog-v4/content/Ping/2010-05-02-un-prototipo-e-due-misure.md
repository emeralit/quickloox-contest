---
title: "Un prototipo e due misure"
date: 2010-05-02
draft: false
tags: ["ping"]
---

Nella vicenda del prototipo di iPhone venuto in possesso di Gizmodo con relativi strascichi legali, vedo che è parecchio gettonata l'idea della congiura. Apple &#8211; come se gli mancassero le opportunità di fare parlare di sé &#8211; ha fatto perdere volontariamente il prototipo, magari proprio per depistare mostrando al mondo qualcosa che poi sarà completamente diverso. Può essere, ma sono scettico. Apple vuole pubblicità positiva, non scandalistica.

Altro elemento che regge poco: Apple ordisce la congiura, ma poi permette che venga sputtanato in lungo e in largo un proprio ingegnere. Il perché sfugge.

Altra idea che circola molto è che la polizia si sia mossa su istigazione o su ordine (!) di Apple. In realtà le cose non vanno diversamente che in Italia: in tutta la vicenda esiste una ipotesi di reato penale e la polizia decide se muoversi o meno per indagare. Posso andare dalla polizia a chiedere di indagare, ma a decidere sono i poliziotti, non io.

Ma la cosa più assurda che gira è che Gizmodo sia finito nei guai per avere pubblicato le foto del prototipo. È totalmente falso: i guai di Gizmodo derivano dall'avere pagato per avere un bene che non era di proprietà di chi glielo ha venduto. Per la legge californiana è un reato abbastanza grave e il motivo è questo, non le foto.

Anche perché le stesse foto <a href="http://www.engadget.com/2010/04/19/apples-4th-generation-iphone-revealed/" target="_blank">le ha pubblicate Engadget</a> e nessuno ha battuto ciglio. Se il problema fossero le foto starebbe passando guai anche Engadget, giusto? Invece no.