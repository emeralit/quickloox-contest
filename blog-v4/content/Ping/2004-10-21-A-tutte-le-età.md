---
title: "A tutte le età"
date: 2004-10-21
draft: false
tags: ["ping"]
---

Il vero design è quello universale

Per capire se un oggetto Apple avrà successo oppure invece darà qualche problema sul mercato dispongo di una cartina di tornasole quasi infallibile, anzi due: mio padre e mio zio.

Non è che abbiano condotto studi particolari o abbiano lavorato nel marketing: semplicemente navigano sui settant'anni di età e sono cresciuti vedendo forme, figure, prodotti, design completamente diversi da quelli con cui cresce un nato nel 1984.

Un design veramente riuscito, presumo, ha dentro qualche caratteristica misteriosa e non misurabile per cui piace praticamente a tutti, indipendentemente da quasi tutte le differenze. Lo stesso fenomeno si verifica a volte nell'arte: Picasso continua a essere un maestro, Guttuso non se lo fila più nessuno.

Bene: l'iMac G5 ha ottenuto un consenso unanime, sia nel formato da 17&rdquo; che da quello da 20&rdquo;.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>