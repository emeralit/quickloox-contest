---
title: "Bassa tecnologia, alto stile"
date: 2005-07-09
draft: false
tags: ["ping"]
---

Ricetta per prevedere se una cosa che fa Apple avrà successo

Stavo pensando al podcasting.

Nella tecnologia in sé non c'è assolutamente niente di eccezionale o incredibile. È giusto uno sviluppo ragionevole intorno al concetto di Xml e di Rss.

Non è semplicissima da spiegare e però ci vuole un attimo per sviluppare dipendenza.

Per fruirne non serve praticamente niente se non il programma giusto.

Apple l'ha inserita dentro un programma di uso comune (iTunes) in modo immediato e facile (non ancora perfettamente facile, ma è già molto).

Avrà un successone.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>