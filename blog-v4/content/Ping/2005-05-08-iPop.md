---
title: "Nazional(i)popolare"
date: 2005-05-08
draft: false
tags: ["ping"]
---

Se non basta questo a spiegare che iPod ha sfondato, che cosa ci vuole?

Ho appena visto in televisione la pubblicità di una compilation musicale intitolata iPop.

Mi chiedo: ma dove mai avranno trovato questa ispirazione? :-)

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>