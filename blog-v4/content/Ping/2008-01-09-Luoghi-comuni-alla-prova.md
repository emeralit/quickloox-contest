---
title: "Luoghi comuni alla prova"
date: 2008-01-09
draft: false
tags: ["ping"]
---

<em>Non esiste</em> malware (software ostile, dai virus in avanti) <em>per Mac perché i Mac sono pochi</em>.

Adesso hanno creato <a href="http://www.ipodnn.com/articles/08/01/09/first.iphone.trojan.attack/" target="_blank">un trojan per iPhone</a>.

Colpisce solo gli iPhone sbloccati. Gli iPhone sbloccati sono circa un quarto degli iPhone totali. Gli iPhone totali sono circa un centesimo dei Nokia totali.

Ci sono pochissimi iPhone sbloccati, forse meno di un milione. Gli iPhone non sbloccati sono tre volte più diffusi. I telefoni Nokia sono quattrocento volte più diffusi di un iPhone sbloccato. Eppure qualcuno si è divertito a creare un attacco per iPhone sbloccato. Come mai, se gli iPhone sbloccati sono cos&#236; pochi?