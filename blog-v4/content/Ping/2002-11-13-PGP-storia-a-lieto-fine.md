---
title: "Una storia a lieto fine"
date: 2002-11-13
draft: false
tags: ["ping"]
---

La privacy dei dati su Mac ha compiuto un progresso importante

Quando Philip Zimmermann, il creatore del programma di cifratura PGP, ha venduto la sua creatura a NAI, non poteva sapere che la società avrebbe trattato piuttosto male la versione commerciale per Mac del prodotto.

NAI non sapeva trattare Mac ma neanche il mercato (c’è forse una lezione da imparare?) e ha ceduto a sua volta PGP a un’altra società, ma stavolta fondata da persone con la mentalità giusta e capaci di vedere che servire il pubblico significa servire tutte le piattaforme informatiche.

E così ora PGP procede di buon passo su Mac così come su Windows.

Nella pratica andare a <link>pgp.com</link>http://www.pgp.com oggi come oggi è un motivo di soddisfazione, ma c’è anche una morale. Che anche Mac sia un mercato appetibile per il software commerciale, è chiaro anche chi lavora sui cifrari.

Lucio Bragagnolo
lux@mac.com