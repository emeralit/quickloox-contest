---
title: "Servizievole Automator"
date: 2009-09-12
draft: false
tags: ["ping"]
---

I cambiamenti più importanti di Snow Leopard sono quasi tutti sottopelle e uno di questi riguarda il menu Servizi, completamente riveduto e ora molto più efficace (nonostante il mio problema di comunicazione tra BBEdit e TextEdit).

Lo prova questo signore, che ha aperto Automator e <a href="http://august.lilleaas.net/s3_upload_snow_leopard_service" target="_blank">ha costruito un Servizio</a> che gli carica a comando un file sul servizio S3 di Amazon.

Il Servizio è appunto stato costruito tramite Automator, a colpi di AppleScript e Ruby. Notevole il fatto che Automator di Snow Leopard azioni script Ruby senza battere ciglio, cosa impossibile su Leopard.

Questo settore della personalizzazione dell'interfaccia non si era mai spinto cos&#236; in avanti e, con Snow Leopard uscito da due settimane, se già si arriva a questi livelli significa che è molto promettente.

Il servizio è apribile con Automator e se ne possono visionare tutti i componenti. Lo consiglio a tutti gli interessati. :-)