---
title: "The Show Must Go On"
date: 2007-01-10
draft: false
tags: ["ping"]
---

Devo ancora digerirmi il keynote. Apple ha svelato al mondo che per anni i costruttori di cellulari ci hanno truffato, vendendoci schifezze disgustose (e adesso so perché ho sempre odiato i cellulari: interfacce di qualità inferiore). Ha cambiato identità per rimanere fedele a se stessa, inaugurando davvero una nuova era.

Eccetera eccetera. Nel mio piccolo, stasera (mercoled&#236; 10) alle 18:30 sono al Mondadori Multicenter di via Marghera a Milano (metropolitana 1 fermata De Angeli) a fare da ospite per la presentazione del libro Facce da iPod di Alberto Pucci, ovviamente presente anche lui. Special guest star Enzo Biagini, numero tutelare di Apple Italia.

Se qualcuno pensa al conflitto di interessi, beh; stavolta non è il mio libro e neanche il mio Apple Center. E insomma. :)