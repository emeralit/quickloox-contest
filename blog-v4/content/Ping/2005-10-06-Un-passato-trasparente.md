---
title: "Un passato trasparente"
date: 2005-10-06
draft: false
tags: ["ping"]
---

Ci sono cose vecchie che attendono solo di divenire nuove

A Quiliano, provincia di Savona, sorge l'All About Apple Museum: il museo Apple più grande e organizzato del mondo.

Umberto Valsasina e signora hanno donato al museo un Macintosh Portable trasparente, molto probabilmente l'unico esistente in Italia e uno di pochissimi in Europa.

Da dove arriva? Come può esistere? Ai più anziani (informaticamente) il cognome Valsasina ricorderà una certa Iret Informatica e tutta una serie di altre nozioni.

Non è tanto questo il punto, quanto il fatto che il successo del museo Apple può dipendere anche da noi. Non è necessario che sia un Mac trasparente; ma, piuttosto (o prima) che pensare alla discarica, pensiamo a Quiliano e mandiamo una email o facciamo una telefonata.

Abbiamo tutti qualcosa da dare per rendere più ricco l'<a href="http://www.allaboutapple.com/museo/museo.htm">All About Apple Museum</a>. Patrimonio che tornerà indietro con gli interessi, in cultura, storia, tradizione, informazione.

Beh. Facciamolo.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>