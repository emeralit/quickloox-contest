---
title: "Assistenza via treno"
date: 2002-11-19
draft: false
tags: ["ping"]
---

Qualcosa che con Macintosh non accadrà mai

Sono in treno verso casa. Squilla il cellulare del mio vicino di sedile.

“Ciao, sono in treno”. Pausa.
“Prova a cambiare l’IRQ del modem”.

Pausa.

“Poi devi riavviare, eh?”
“Hai disinstallato?”
“Eh, perché è una porta virtuale...”

I problemi ci sono anche su Mac, ma stai tranquillo: nessuno ti chiederà mai di cambiare un IRQ.

<link>Lucio Bragagnolo</link>lux@mac.com