---
title: "Musica in compagnia"
date: 2009-09-10
draft: false
tags: ["ping"]
---

Per ringraziare quanti hanno partecipato alla chiacchierata Irc di ieri, mentre Steve Jobs presentava nuovi iPod, nuovo iTunes, nuovo iPhone OS.

Il vecchio Irc ha tenuto ottimamente botta e come sempre grazie anche a <a href="http://freesmug.org" target="_blank">Gand</a> per l'ospitalità.

Magari si rifà la prossima volta. :-)