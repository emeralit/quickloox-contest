---
title: "Lezione dal video"
date: 2006-10-30
draft: false
tags: ["ping"]
---

Che se ne fa un sito di nome <a href="http://www.finalcutstudio.it/" target="_blank">Final Cut Studio</a> di un&#8230; redattore?

Dedicato a chi pensa che si possano fare buoni siti Internet senza saper scrivere in buon italiano e agli analfabeti di ritorno, che appestano la rete con la loro approssimazione e incuria. Dedicato anche al sito stesso, perché con queste premesse si avvia a fare davvero un bel lavoro. Bravi!