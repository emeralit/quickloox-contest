---
title: "Affare di status"
date: 2005-05-28
draft: false
tags: ["ping"]
---

iChat, AppleScript e sciocchezze in libera uscita

È notte e con un amico chiacchiero su iChat, di iChat. <cite>Sono deluso dall'iChat di Tiger</cite>, mi dice l'amico, <cite>perché non si può configurare automaticamente il messaggio di status</cite>.

Mi viene un dubbio. Da ignorante sovrano di AppleScript quale sono riesco comunque ad aprire Script Editor, aprire il dizionario di iChat e vedere che esiste un parametro Status message.

Allora do fondo a tutte le mie capacità di programmazione e dentro Script Editor scrivo:

<code>tell application "iChat"</code><br>
<code>   set status message to "un messaggio qualsiasi"</code><br>
<code>end tell</code>

Funziona. Il messaggio di status di iChat di Tiger è configurabile da AppleScript, ovvero un buon programmatore se lo può palleggiare come vuole.

L'amico, poveretto, non c'entra. Qualcuno glielo ha detto. Qualcuno che è evidentemente ignorante, perché non sa, o stupido, perché come scherzo vale veramente poco.

Esiste un pieno e totale diritto all'ignoranza. A patto di esserne consapevoli ed evitare di fare danni intorno a sé.

Il diritto alla stupidità, beh, se esiste, non mi piace.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>