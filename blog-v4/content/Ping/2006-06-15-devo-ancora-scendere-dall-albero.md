---
title: "Devo ancora scendere dall'albero"
date: 2006-06-15
draft: false
tags: ["ping"]
---

Per dare una mano al papà, che dopo avere digitalizzato tutti gli album fotografici della sua vita ora sta ricostruendo l'albero genealogico del parentado, sto iniziando a esaminare i programmi dedicati. Per ora ho lanciato <a href="http://genj.sourceforge.net/" target="_blank">GenealogyJ</a>, che è free e open source ma mi sembra poverello. Ma è ancora presto. Magari avrà un grande avvenire, per ciò che è alle mie spalle. :-)