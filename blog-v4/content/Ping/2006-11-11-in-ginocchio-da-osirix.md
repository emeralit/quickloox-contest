---
title: "In ginocchio da OsiriX"
date: 2006-11-11
draft: false
tags: ["ping"]
---

L'amico <strong>Paolo</strong> si è fatto male a un ginocchio ed è andato a farselo esaminare.

È tornato a casa con un Cd contenente tutte le immagini della risonanza magnetica&#8230; e un programma solo Windows per consultarle.

Dopo un breve attimo di smarrimento (suo) e un veloce consulto, ha scaricato tutto il malloppo nella cartella Public del mio iDisk.

Dalla quale ho rapidamente desunto che il formato Dicom delle immagini da risonanza magnetica si vede tranquillamente, in forma grezza, via <a href="http://www.lemkesoft.de" target="_blank">Graphic Converter</a>.

Rinfrancato, il buon Paolo ha anche individuato <a href="http://homepage.mac.com/rossetantoine/osirix/" target="_blank">OsiriX</a>, programma per Mac OS X che non solo legge le immagini ma fornisce anche una serie di funzioni avanzate (e funzionali alla consultazione medica) come rotazioni in 3D simulato, animazioni con immagini in sequenza e via risuonando.

Visto che OsiriX è free e pure open source, e occupa una trentina di mega e poco più, Paolo si è chiesto perché quelli dell'ospedale non mettano nel Cd anche il necessario per rimirarsi i legamenti sullo schermo del Mac.

La mia risposta è che sono bravissimi medici, con un tecnico informatico deficiente, o ignorante, o ambedue. La teoria alternativa è che si possano infortunare al ginocchio solo utenti Windows, ma Paolo ne rappresenta la smentita vivente.