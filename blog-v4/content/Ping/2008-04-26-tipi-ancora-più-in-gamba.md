---
title: "Tipi ancora più in gamba"
date: 2008-04-26
draft: false
tags: ["ping"]
---

Già che oggi si parla di font e di Microsoft, togliamo di mezzo la metà triste e passiamo invece a questo divertente (e istruttivo) <a href="http://www.youtube.com/watch?v=91smRXrEPRs">documentario Bbc su Gutenberg</a> e la stampa a caratteri mobili.

Lo dedico a <a href="http://www.scumbag.it" target="_blank">Fede</a> e ulteriore ringraziamento per gli aggiornamenti sulla <a href="http://www.macworld.it/blogs/ping/?p=1715">Tipoteca Italiana</a>. 

Il filmato dà su YouTube. Per chi vuole e sa, ce n’è una versione intera e a maggiore definizione via <a href="http://thepiratebay.org/tor/4137300/Stephen_Fry_and_the_Gutenberg_Press" target="_blank">BitTorrent</a>.