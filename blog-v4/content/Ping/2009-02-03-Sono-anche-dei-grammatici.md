---
title: "Sono anche dei grammatici"
date: 2009-02-03
draft: false
tags: ["ping"]
---

Sempre parlando di Blambot, consiglio la lettura del loro piccolissimo <a href="http://www.blambot.com/grammar.shtml" target="_blank">trattato sulla grammatica del fumetto</a>. Si capisce molto bene anche guardando le figure e non richiede più che pochi minuti al distratto passante occasionale.

Per chi invece riconosce l'opportunità, sarà una sorpresa vedere quanti e quali metodi di comunicazione ci sono in un campo apparentemente banale come il fumetto. Le sfumature e le raffinatezze di un linguaggio sono sempre più di quelle che appaiono a prima vista. Un bell'esercizio.