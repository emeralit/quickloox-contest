---
title: "I Travia(na)ti"
date: 2006-09-26
draft: false
tags: ["ping"]
---

È sempre <strong>Stefano</strong>, nominato ufficialmente il nostro agente all'eBay, a segnalarci (parole sue) che Travian è diventato <a href="http://cgi.ebay.it/ws/eBayISAPI.dll?ViewItem&amp;item=130029859313&amp;ssPageName=ADME:B:WNASIF:IT:78" target="_blank">importante</a>.

Beninteso, se qualcuno dà anche solo un euro al tizio del link gli tolgo il saluto. Gente cos&#236;, per Travian e i giocatori veri, è un problema.