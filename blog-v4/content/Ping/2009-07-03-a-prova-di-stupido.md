---
title: "A prova di stupido"
date: 2009-07-03
draft: false
tags: ["ping"]
---

Se qualcuno si è scritto a Dofus, mi spiega come si arriva a giocare?

Mi sono iscritto e ho scaricato i 168 mega di dati che si vogliono presenti sul computer. Ho scompattato lo zip ma dentro non vedo eseguibili, o almeno non capisco bene che cosa farci.

Se clicco sul pulsante Giocare dentro la finestra del gioco, non succede niente.

Accetto volentieri illuminazioni. :-)