---
title: "Mac è diverso"
date: 2005-10-05
draft: false
tags: ["ping"]
---

Può servire in modi fantastici per persone comuni e per questo assai speciali

Riporto le parole di <strong>Piergiovanni</strong>:

<cite>In una delle mie tre prime liceo, dove faccio da coordinatore, c'è un ragazzo di 20 anni, con gravi problemi dovuti a traumi neonatali, che gli hanno lasciato una semiparalisi, un ritardo psicofisico e un grave deficit nella sfera attentiva. È seguito da un pedagogista e da un'insegnante di sostegno. Ho avuto l'idea di creare lezioni speciali, molto semplici, fatte in modo da consentirgli di fare le stesse cose che fanno i suoi compagni. Ho così usato uno dei modelli di Pages, molto colorato, con una foto appropriata già presente nel modello […]</cite>

Quel <em>Think Different</em> che girava qualche anno fa forse faceva riferimento un po' anche a questo. Pages mi si è rivelato in pochi giorni programma ideale per il mio amico Jida, autore di uno splendido libro sull'uso facile di Final Cut Pro, e per un ragazzo di vent'anni, che non conosco ma è già un po' mio amico, se Piergiovanni è d'accordo. Siamo tutti diversi… e tutti uguali.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>