---
title: "Allenamento chat / 1"
date: 2008-12-06
draft: false
tags: ["ping"]
---

Non è una data qualsiasi. Esattamente tra un mese, il 6 gennaio 2009, ci sarà il <em>keynote</em> di Macworld Expo.

Come da tradizione, aprirò una stanza di <em>chat</em> per ritrovarsi, chi ha tempo e voglia, e chiacchierare in diretta di annunci, speranze, entusiasmi, delusioni, auguri per il nuovo anno e qualsiasi altra cosa.

Tuttavia l'anno scorso la <em>chat</em> ha avuto una partecipazione superiore a quanto consentito dai server di Aim che fanno da motore a iChat. Letteralmente, il salotto era pieno e qualcuno ha dovuto restare fuori.

Il mio sogno è un salotto dove ci sia posto per tutti e dunque tra un mese aprirò <em>due</em> stanze di <em>chat</em>: quella solita di iChat e una stanza su Irc. Irc sta per <em>Internet Relay Chat</em> ed è il sistema che esiste da sempre per chiacchierare liberamente su Internet. Certamente più ruspante di iChat o altro, ma libero da limiti. O almeno spero!

Il piano è accogliere tutti quanti non lo sanno nella stanza di iChat e reinvitarli nella stanza su Irc, dove andare direttamente se lo si sa già.

Irc non è immediato come iChat, ma si padroneggia rapidamente e in una manciata di post voglio mettere in grado di collegarsi tutti quelli che se la sentono.

Primo compito: procurarsi un software in grado di accendere <em>chat</em> Irc. C'è varia scelta e la spesa richiesta il più delle volte è zero. Un bravissimo lo sa direttamente da Terminale; noi normali ci colleghiamo con <a href="http://xchataqua.sourceforge.net/twiki/bin/view/Main/WebHome" target="_blank">X-Chat Aqua</a>, <a href="http://colloquy.info/" target="_blank">Colloquy</a>, <a href="http://homepage.mac.com/philrobin/conversation/" target="_blank">Conversation</a>, <a href="http://jircii.dashnine.org/index.html" target="_blank">JIrcii</a>, <a href="http://farhnware.homelinux.com/software/simple-irc/" target="_blank">simple irc</a>&#8230; e non è finita qui. Una ricerca su MacUpdate mostra <a href="http://www.macupdate.com/search.php?keywords=irc&amp;starget=google" target="_blank">un elenco consistente</a>.

Scegli quello che ti ispira di più. Al prossimo giro vediamo come ci si collega. :-)