---
title: "Birpi Goes Intel"
date: 2006-04-17
draft: false
tags: ["ping"]
---

L'amico <em>Birpi</em> ha dichiarato <cite>Per passare a Mac Intel aspettavo solo Apple Remote Desktop, che era l'ultimo programma a mancarmi, e adesso finalmente posso decidere</cite>.

Il caso di Birpi non è ovviamente universale. Ma è indicativo.

Stando alle dichiarazioni originali di Steve Jobs, a oggi la transizione non doveva neanche essere iniziata. Inizia già a esserci gente per cui si è conclusa con successo. Non male.