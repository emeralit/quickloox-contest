---
title: "Ha capito perfino l'ipermercato"
date: 2005-05-18
draft: false
tags: ["ping"]
---

I mentecatti che blaterano del tre per cento dovrebbero prendere esempio da Esselunga

Ricevo e pubblico assai volentieri da Matteo Calvi:

<em>Ti segnalerei questo <a href="http://www.esselungaacasa.it/ecommerce/reg/loginFrameset.do">link</a>:</em>

<cite>In buona sostanza la catena di distribuzione da cui ho copiato il link <em>[qui non c'è quello originale di Matteo, che riporta a una pagina più generica, ma ugualmente eloquente. N.d.lux]</em> ha cercato la compatibilità anche per Mac, malgrado siamo uno sparuto gruppo di utenti.</cite>

<cite>Posso garantire che un anno fa, il sito di e-commerce non funzionava col Mac, invece ultimamente va egregiamente, tanto che mia moglie ha ià acquistato tre volte. Dopo aver letto la pagina segnalata devo confessare che i soldi li ho anche spesi più volentieri.</cite>

<cite>Poi è segnalata anche una pagina di MacWorld. :)</cite>

Sono cliente Esselunga e sottoscrivo parola per parola l'intervento di Matteo.

Intanto, mi sa che il gruppo di utenti Mac non è più così sparuto. Secondo, compito in classe per tutti i mentecatti che blaterano di quote di mercato. Come mai una catena di ipermercati, che vende a milioni di persone, si preoccupa così tanto per un supposto tre per cento?

La verità è che Internet è un mezzo di comunicazione universale. Quindi deve servire a comunicare con tutti. E quando si fa commercio elettronico, è stupido lasciarsi scapapre anche un solo cliente.

A quelli di Esselunga scriverò per congratularmi della loro lungimiranza. E se non fossi l'unico?

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>