---
title: "Tre livelli di bellezza"
date: 2004-11-10
draft: false
tags: ["ping"]
---

Non è bello ciò che è bello, ma ciò che è design

È appena terminata al palazzo della Triennale di Milano la presentazione dell'ultimo libro di Donald Norman, <em>Emotional Design</em>.

Norman è una delle massime autorità al mondo in fatto di design e interfacce e, guarda caso, è stato per alcuni anni Apple Fellow: pagato una cifra pazzesca per pensare e raccontare in Apple quello che pensava.

I libri di Norman dovrebbero essere lettura obbligatoria nelle scuole e nelle aziende, ma purtroppo per ora è solo possibile invitarti a comprarli e leggerli attentamente tutti.

Per venire al punto, ha mostrato due esempi di design a suo avviso eccellente: una scatola di Altoids (mentine) e un iPod mini. Ben progettato in tre modi: perché è bello, perché funziona bene senza tenerci in ansia e perché è capace di farci sentire più belli usandolo.

Gli oggetti che soddisfano queste tre specifiche nella vita di tutti i giorni sono veramente pochi. Brava Apple.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>