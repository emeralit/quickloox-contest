---
title: "Il passato dà l'esempio"
date: 2004-09-26
draft: false
tags: ["ping"]
---

Un easter egg che rimanda ai tempi andati e a un amministratore delegato

Nel Terminale esiste un comando che sta andando rapidamente in disuso: appleping. In questi tempi di Tcp/Ip il protocollo AppleTalk è sempre meno diffuso, ma per quando serve ancora il comando è lì e analizza il traffico di rete.

Digitando appleping nel Terminale apparirà questo esempio di uso:

<code>examples:  appleping 'John Sculley:Macintosh SE@Pepsi' </code>

John Sculley è l'uomo assunto da Steve Jobs e che poi licenziò Steve Jobs, il futuro capo supremo di Pepsi Cola cui Jobs chiese <cite>Vuoi vendere acqua zuccherata per tutta la vita o vuoi provare a cambiare il mondo?</cite>, che diffuse il concetto del Knowledge Navigator, posò per Byte vestito da samurai e lanciò la tecnologia Newton. In modo nascosto, ma almeno da qualche parte nel sistema è stato ricordato. :-)

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>