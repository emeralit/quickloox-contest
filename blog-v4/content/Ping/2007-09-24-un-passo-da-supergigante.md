---
title: "Un passo da supergigante"
date: 2007-09-24
draft: false
tags: ["ping"]
---

Nella classifica dei siti Mac da non perdere ha fatto passi non da gigante, da supergigante, <a href="http://daringfireball.net" target="_blank">Daring Fireball</a> di John Gruber. Fa quello che faccio io dieci volte meglio con cinque volte più frequenza e precisione totale.

Una delle perle più recenti: una copia <em>retail</em> di Office su cinque <a href="http://www.news.com/Running+the+numbers+on+Vista/2100-1016_3-6207375.html?tag=nefd.pop" target="_blank">è per Mac OS X</a>.

Da non perdere, assieme a <a href="http://www.macdailynews.com" target="_blank">MacDaily News</a> e a&#8230;?