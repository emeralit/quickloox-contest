---
title: "Tutti campioni"
date: 2006-12-11
draft: false
tags: ["ping"]
---

Stai giocando ancora a Travian? E a World of Warcraft (io s&#236;, ho raggiunto il livello 60 con il mio rogue in diciannove mesi, incredibilmente prima che esca l'espansione)? In generale, quanto ti dura un gioco? C'è un gioco in particolare che dura o è durato molto più di tutti gli altri, o la media di durata è costante? Se il gioco è online, ti dura di più oppure fa come quelli tradizionali?

Sto vedendo un po' di comportamenti particolari e vorrei capirne di più allargando, come si suol dire, il campione. Grazie in anticipo. :-)