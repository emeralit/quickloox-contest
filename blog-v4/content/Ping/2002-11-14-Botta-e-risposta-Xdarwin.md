---
title: "Botta, risposta e Xdarwin"
date: 2002-11-14
draft: false
tags: ["ping"]
---

Quando la strategia Open Desktop mostra i suoi vantaggi

Bisogna ammettere che, di fronte al successo di Linux, alcuni hanno considerato Mac OS X un po’ come secondo arrivato. Casualmente, su una delle mailing list che leggo è recentemente passato un dialogo di questo tenore:

> L’esperienza che ho io di XFree è basata sull’uso di Linux col pc, la 
> cosa è lunga e contorta, non ho mai provato ad installarlo su MacOS X 

http://www.xdarwin.org e con due click tutto è pronto! :)

Xfree, per capirci, è la base dell’interfaccia grafica X Window System, usata da numerosi programmi su Unix e anche su Linux.
Anche Mac OS X può ospitare Xfree, proprio come Linux. Il fatto che sia installabile facilmente non è un merito particolare di Apple, perché tutto arriva dalla comunità del software libero.

Eppure, com’è come non è, quando si arriva su Mac è tutto sempre un po’ meno complicato.

<link>Lucio Bragagnolo</link>lux@mac.com