---
title: "L'uomo senza qualità (della vita)"
date: 2006-03-15
draft: false
tags: ["ping"]
---

Adesso che sono usciti gli iMac, il MacBook Pro e il Mac mini della nuova era, una cosa è chiara: Apple deve ancora sostituire un processore G4 con un processore Intel senza togliere qualche dotazione, alzare i prezzi, o ambedue le cose.

Giravano un sacco di anime belle convinte che il passaggio a Intel avrebbe fatto diminuire i prezzi, non si sa perch&eacute; n&eacute; come, ma convinte lo erano di sicuro.

Auspico abbiano capito che i cassoni da un tanto al chilo così agognati per risparmiare (sulla qualità della vita) non costano due stracci perchè il processore è Intel, ma perch&eacute; sono male progettati e male ingegnerizzati, oltre ad avere un sistema operativo malfatto.