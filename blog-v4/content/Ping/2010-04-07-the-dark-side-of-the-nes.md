---
title: "The Dark Side of the Nes"
date: 2010-04-07
draft: false
tags: ["ping"]
---

L'incrocio tra tecnologia moderna, retroinformatica e musica progressive può dare frutti inquietanti, affascinanti, divertenti o devastanti secondo il punto di vista.

Che cosa sarebbe successo se i Pink Floyd avessero scritto <i>The Dark Side of the Moon</i> per l'audio a otto bit del Nintendo Entertainment System?

La risposta si chiama <a href="http://rainwarrior.thenoos.net/music/moon8.html" target="_blank">Moon8</a> ed è liberamente scaricabile.