---
title: "Il gioco delle tre carte(lle)"
date: 2005-02-17
draft: false
tags: ["ping"]
---

A che cosa servono e perché non si fanno le pulizie in casa d'altri

I font. Stanno nella cartella Fonts che sta dentro la cartella Library (che vuol dire biblioteca, non libreria. Libreria, nel senso del mobile, è bookshelf, nel senso del negozio è bookshop). Di Library/Fonts ce ne sono tre.

Una sta nella tua home. Contiene i font che servono solo a te.

Una sta nella Library che si vede aprendo il disco rigido. Contiene i font che servono a tutti quelli che usano quel Mac, te compreso.

Una sta dentro la cartella System.

La cartella System è la cartella System. È quella del sistema. Contiene i font che servono al sistema. Fare cose stupide nel sistema, tipo cancellare i font che servono al sistema, significa creare problemi al sistema.

Il sistema (l'ho ripetuto un po' di volte, perché magari così scarichi il concetto, come dice Tiscali), se ha problemi, su chi li riversa? Sull'amministratore di sistema. Che sei tu.

Vuoi avere problemi con il sistema?

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>