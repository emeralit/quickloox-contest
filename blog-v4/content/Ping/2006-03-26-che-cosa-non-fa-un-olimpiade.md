---
title: "Che cosa non fa un'Olimpiade"
date: 2006-03-26
draft: false
tags: ["ping"]
---

Su <a href="http://maps.google.com/" target="_blank">Google Local</a> L'Italia non è ancora mappata fuori dalle immagini satellitari, con l'eccezione del Piemonte e di metà Liguria. &Egrave; interessante chiedersi quale evento potrebbe portare al completamento di altre porzioni di mappa. :-)