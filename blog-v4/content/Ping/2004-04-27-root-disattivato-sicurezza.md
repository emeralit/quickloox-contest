---
title: "Quella sicurezza in più"
date: 2004-04-27
draft: false
tags: ["ping"]
---

Una non funzione di Mac OS X che può essere utile

Ieri mi sono stupito di come la mia password di root non funzionasse. Eppure ero convinto di ricordarmela!

Solo dopo qualche istante mi sono reso conto che, in effetti, sul mio sistema root era disattivato. L’ho riattivato con NetInfo e tutto ha regolarmente funzionato come doveva.

Tutta sicurezza in più. Un eventuale aggressore esterno (o anche un figlioletto intraprendente che mette le mani sul portatile) ha un bell’indovinare la password di root, se root non c’è.

<link>Lucio Bragagnolo</link>lux@mac.com