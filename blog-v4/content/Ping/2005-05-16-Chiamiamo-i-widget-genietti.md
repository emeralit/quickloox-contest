---
title: "Il nome giusto per i widget?"
date: 2005-05-16
draft: false
tags: ["ping"]
---

Questo suggerimento finora mi sembra il più carino. Ma la caccia continua

Come chiamare i widget in italiano?

Filippo Cervellera mi manda una proposta che a me piace particolarmente: <em>genietti</em>.

Oltretutto evoca anche i daemon di Unix e, insomma, mi piace.

Metodo scientifico: voto questo e vediamo se arriva di meglio!

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>