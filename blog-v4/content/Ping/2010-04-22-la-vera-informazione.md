---
title: "La vera informazione"
date: 2010-04-22
draft: false
tags: ["ping"]
---

A gennaio 2009 l'amministratore delegato di Adobe <a href="http://www.bloomberg.com/apps/news?pid=newsarchive&amp;sid=aFYb.P__vEfY" target="_blank">dichiarava</a> che c'era una collaborazione con Apple per portare Flash su iPhone.

Oggi Mike Chambers, figura di spicco in Adobe per quanto riguarda Flash, dichiara che &#8211; uscita Creative Suite 5, che permette di creare applicazioni per iPhone esportando da Flash &#8211; Adobe <a href="http://www.mikechambers.com/blog/2010/04/20/on-adobe-flash-cs5-and-iphone-applications/" target="_blank">cesserà lo sviluppo ulteriore</a> di queste funzioni.

In altre parole, quello di oggi è un fatto e quella di ieri un'opinione. Ma non era stato detto con molto chiarezza.

Intanto iPhone senza Flash ha fatto l'ennesimo record di vendite, <a href="http://images.apple.com/pr/pdf/q210data_sum.pdf" target="_blank">8,7 milioni di pezzi venduti tra gennaio e marzo</a>.

Sempre intanto, riflettendoci, in questo momento <i>non esistono</i> apparecchi da tasca su cui funziona Flash come lo abbiamo su Mac. Flash Player 10.1 per Android è in beta privata e non ancora ufficiale. Milioni di telefoni e telefonini montano Flash Lite, che però è una pallida imitazione della cosa vera.

Continuiamo a essere &#8220;informati&#8221; sul fatto che <a href="http://attivissimo.blogspot.com/2010/01/aiped-aipad-aipod.html" target="_blank">manca Flash</a>.

Visto tuttavia che cos&#236; tanta gente ne fa volentieri a meno, mi chiedo: serve <i>veramente</i> Flash su un apparecchio da tasca? Forse un po' di informazione in più su questo non guasterebbe.