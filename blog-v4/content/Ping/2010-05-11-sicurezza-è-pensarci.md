---
title: "Sicurezza è pensarci"
date: 2010-05-11
draft: false
tags: ["ping"]
---

In sosta a Busto Arsizio, prendo un caffè al bar e apro il MacBook Pro.

C'è una base <i>wireless</i> chiusa. Provo la <i>password</i> <i>pippo</i> e si apre la rete!

In compenso i servizi sono disattivati e non si vedono macchine condivise o stampanti raggiungibili. Non si va neanche su Internet.

Se dovessi condividere dati con un amico seduto al tavolo di fianco sarebbe un attimo. Ma non posso approfittare della rete né, ipoteticamente malintenzionato, provocare danni.

Una <i>password</i> ovvia per una rete blindata è meglio di una <i>password</i> blindata per una rete colabrodo. Complimenti all'amministratore (e alle <i>brioche</i> con il cioccolato).