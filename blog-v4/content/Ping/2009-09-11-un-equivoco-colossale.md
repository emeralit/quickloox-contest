---
title: "Un equivoco colossale"
date: 2009-09-11
draft: false
tags: ["ping"]
---

Mi sono imbattuto nel rifacimento digitale di Titan, un <i>boardgame</i> (gioco da tavolo) affascinante e molto complicato in voga ai tempi in cui si giocava senza computer.

Si chiama <a href="http://colossus.sourceforge.net/" target="_blank">Colossus</a>. Scritto in Java, si può scaricare sul disco oppure farlo partire con Java Web Start direttamente dal sito.

Si può giocare da soli contro il Mac, oppure in più amici davanti a un computer, oppure via rete contro chi vogliamo. In quanto Java, funziona anche su Linux e Windows.

Consiglio vivamente di leggere le regole prima di cimentarsi (il gioco è davvero complesso).

Il punto è un altro: Colossus è assente, per quanto vedo, sia da <a href="http://www.versiontracker.com" target="_blank">VersionTracker</a> che da <a href="http://www.macupdate.com" target="_blank">MacUpdate</a>.

Ci sono sempre più programmi e più giochi per Mac di quanto mostri l'apparenza. Non cadiamo nell'equivoco.