---
title: "Occasioni per venture capitalist"
date: 2008-09-17
draft: false
tags: ["ping"]
---

Finanziare una versione italiana di <a href="http://www.stopforwarding.us/" target="_blank">StopForwardingUs</a>, che manda un messaggio anonimo, educato e gentile (nonché inutile, ma conta il principio) agli amici parenti e colleghi che in mancanza di impegni lavorativi reali fanno girare catene di Sant'Antonio, liste di stupidaggini e barzellette trite e ritrite che &#8220;non ho saputo resistere&#8221;.

<strong>Aggiornamento:</strong> grazie a <strong>Qrade</strong>, che ha provveduto a segnalare l’esistenza di <a href="http://hairottoleballe.com/" target="_blank">Hairottoleballe.com</a>.