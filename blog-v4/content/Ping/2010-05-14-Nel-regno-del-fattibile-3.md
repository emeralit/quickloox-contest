---
title: "Nel regno del fattibile / 3"
date: 2010-05-14
draft: false
tags: ["ping"]
---

<cite>pregasi elencare azioni fattibili su un iPad e non fattibili su un Sony Vaio W da 10&#8221;</cite>.

Usarlo come <a href="http://blog.comcast.com/2010/05/xfinity-remote-prototype-ipad-demo-at-ncta-show.html" target="_blank">telecomando per la televisione on-demand</a> e la registrazione digitale. Lo mostrato Comcast, gigante americano della televisione via cavo.

Per ora è solo un prototipo. Ma comunque, chi lo farebbe con un <i>netbook</i> in grembo?