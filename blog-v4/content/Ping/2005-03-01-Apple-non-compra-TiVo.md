---
title: "Cambia solo il senso"
date: 2005-03-01
draft: false
tags: ["ping"]
---

Finite le sciocchezze su Apple acquistata, iniziano quelle su Apple acquirente

Negli anni Apple è stata data per venduta o vendibile a Walt Disney, Sun, Sony, Hewlett-Packard, Ibm, Oracle e me ne dimentico certo qualcuna.

Tutto sciocchezze totali. Un sito italiano chiese addirittura ad Apple Italia una smentita ufficiale all'ipotesi che Steve Jobs fosse personalmente in trattative con Michael Eisner di Disney. E Apple Italia, che non sa manco quando escono i nuovi prodotti, rispose!

Ora Apple sta andando troppo bene e la storia che possa essere comprato non regge. Così nascono le storie su Apple che compra, nel caso TiVo, azienda che vende un sistema di videoregistrazione digitale molto per la quale negli States.

In fretta come è nato, il pettegolezzo si sta già sgonfiando, confortato dal fatto che Apple e il suo management mostrano di pensare ad altre cose, in questo momento particolarmente azzeccate.

Apple conta di vendere cinque milioni di iPod shuffle nel 2005 e dovrebbe mettersi a comprare gente che fa videoregistratori. Dovrebbe bastare pensarci un attimo.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>