---
title: "Licenziatari e furbi"
date: 2001-12-31
draft: false
tags: ["ping"]
---

Chi paga e chi no: un’azienda sotto accusa per presunto eccesso di furbizia

Dell Computer, nota concorrente di Apple, è stata portata in tribunale in Usa in quanto vende prodotti che utilizzano lo standard Mpeg-2, per esempio i lettori Dvd dentro le proprie macchine, ma ha deciso di non pagare i diritti di licenza al consorzio Mpeg La (mpegla.com) che amministra i brevetti relativi allo standard. Per fare un confronto, Apple, Hewlett-Packard, Compaq e Sharp pagano regolarmente i diritti a Mpeg La.
Non basta prendere atto del fatto che certe volte una macchina Windows sembra costare meno; bisognerebbe anche chiedersi il perché.

lux@mac.com