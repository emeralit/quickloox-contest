---
title: "Sempre più servizio"
date: 2004-12-18
draft: false
tags: ["ping"]
---

Sta crescendo lentamente, ma in continuità, .mac

Ogni volta che accenno a qualcuno dell'esistenza di <a href="http://www.mac.com">.mac</a> la primissima reazione è che costa 99 euro l'anno e quindi, per definizione, non è una buona cosa.

Alcuni mi fanno notare che tutti i servizi offerti da .mac sono ottenibili in altro modo gratuitamente (vero), e in generale si tratta di persone che non sono capaci di farlo.

Nessuno valuta veramente se i soldi valgano i benefici.

Secondo me sì e non solo per i servizi. Ci sono tutorial, programmi gratis, musica gratis, offerte speciali e un sacco di altre cose. In questi giorni .mac regala un gioco e due capitoli di un libro, e in più ha inaugurato una nuova feature: un elenco di programmi che collaborano con .mac, tra cui Macromedia Contribute e Allume StuffIt Deluxe.

Più prosaicamente, quest'anno invierò gli auguri di Natale via .mac e sarà più semplice, ed economico, che in qualunque altro modo.

Il mio account me lo tengo ben stretto. Anche a 99 euro.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>