---
title: "Adsl short story"
date: 2004-02-27
draft: false
tags: ["ping"]
---

Storia a lieto fine di facilità di utilizzo

Da Internet scarico assai poco e su Internet non faccio mai viaggiare carichi particolarmente ingenti. Però sentivo il bisogno di una linea always on, 24 ore su 24, con buona banda anche se senza pretese. Se volessi banda in abbondanza sceglierei FastWeb, solo che per FastWeb abitare dove abito io o su Marte è la stessa cosa.

Così ho preso la Adsl più economica che ho trovato, quella di Tele2. A canone, non a consumo (ma che senso ha prendere una Adsl a consumo?), sempre accesa, come voglio io.

Nella configurazione della base AirPort è bastato infilare nome utente e password. In quella del programma di posta sono cambiati server Pop ed Smtp. Ho fatto un errore collegando il modem Adsl alla porta Ethernet della base AirPort; l’ho ricollegato alla porta Wan e tutto ha preso a funzionare meravigliosamente.

Ora l’iMac di casa e il Titanium vivono più felici. E anch’io.

<link>Lucio Bragagnolo</link>lux@mac.com