---
title: "Dieci anni dopo"
date: 2010-09-27
draft: false
tags: ["ping"]
---

Non è la velocità. <a href="http://www.flickr.com/photos/x1brett/4742540168/" target="_blank">È la miniaturizzazione</a>.