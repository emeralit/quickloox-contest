---
title: "Sophia compie cinque anni"
date: 2005-05-09
draft: false
tags: ["ping"]
---

E la cosa si commenta da sé. O dovrebbe

Piergiovanni ha annunciato il quinto anniversario dell'entrata in funzione di Sophia. Che, a questo punto va rivelato, non è una bimba né una gatta siamese, ma un iMac Bondi.

Chi si lamenta dei prezzi, o dell'uscita continua di nuovi modelli, dovrebbe guardare i propri reali requisiti. Può darsi che anche un computer di cinque anni fa sia assolutamente ideale.

Spalmandone il costo su cinque anni, inoltre, voglio vedere chi avrà il coraggio di dire che forse, chissà, magari, rispetto a un cassone grigio brutto impersonale e tecnologicamente inferiore, Sophia costava qualcosina in più.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>