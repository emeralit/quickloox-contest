---
title: "Da Webbit al museo"
date: 2003-05-10
draft: false
tags: ["ping"]
---

Cubi vecchi, cubi nuovi

Mentre si dissipa l’intensità di Webbit 2003 a Padova e si prepara quello di Napoli, non posso non segnalare che a Padova esiste un museo dell’informatica, con tanto incredibile retrocomputing.

Parlando di Apple si può vedere un Lisa prima serie, quello con due set di fosfori (ocra per funzionare da terminale e bianconeri per l’interfaccia grafica). Poi un Macintosh Portable ma anche un Apple IIc.

Il pezzo forte però è un Cube. No, non quello di Apple, ma quello di NeXT. Fa impressione per quanto è vecchio e per quanto era innovativo. E, no, non assomiglia al Cube di Apple neanche in sogno.

<link>Lucio Bragagnolo</link>lux@mac.com