---
title: "Piccolo grande Diciassette"
date: 2005-10-12
draft: false
tags: ["ping"]
---

Ciò che è troppo grande dovrebbe rendersi evidentemente ingombrante. Invece…

Ogni tanto incontro qualcuno che guarda il mio PowerBook 17&rdquo; e dice <cite>bello, ma troppo grande</cite>.

Io rispondo che prima usavo un PowerBook 15&rdquo;. Però, nonostante i due pollici in più, uso sempre la stessa borsa di prima. Meno abbondante, chiaro, ma non ho dovuto cambiarla. Si vede che non era mica così grande da disturbare.

Oggi ho avuto una ulteriore conferma. Prima di entrare in banca ho dovuto lasciare la borsa, con dentro il PowerBook, dentro l'apposito armadietto.

Il 17&rdquo; occupa più spazio in larghezza, ma la borsa entra perfettamente nello scomparto.

Il mio PowerBook è grande per quello che fa, non per il suo ingombro.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>