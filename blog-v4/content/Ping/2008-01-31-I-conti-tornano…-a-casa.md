---
title: "Ricevo da Pierfausto:"
date: 2008-01-31
draft: false
tags: ["ping"]
---

Ciao Lux,
sto risistemando casa... la necessità è quella di riuscire a farsi un 
idea di quello che verrà fuori prima di buttare giù muri, chiudere 
porte, comprare mobili et similia.
Così sono andato alla ricerca di un software che mi constisse di 
tracciere i muri con le loro misure e avesse una visulizzazione 3d.
In commericio ce ne sono diversi che promettono co(a)se meravigliose, io 
invece ho trovato un programmino che è meraviglioso: Sweet Home 3d.
1) ovviamente Open Source, distribuito  con GPL (gas propano liquido :) ).
2) Localizzato in Italiano.
3) Comprensivo di guida.
4) Diversi accessori e mibili in 3d sono inclusi se ne possono scaricare 
centinaia.
ma soprattutto f a c i l e  d a  u s a r e.

Basta armarsi di metro per predere le misuire reali e disegnare i muri 
sullo spazio dedicato al 2d istantaneamente vine creato il disegno in 3d.
Il tempo necessario per avere la propria abitazione in 3d è direttamente 
proporsionale alle sue dimensioni, tutto il resto lo si impiega per 
arredare spostare colorare, modificare.

Il programma si scarica da questo link: 
http://sweethome3d.sourceforge.net/ sfrutta java come tecnologia e 
questo lo rende multipiattaforma.
Al geometra porterò il pdf, alla mia compagna farò vedere le nuove idee 
direttamente in 3d, io (in proporsione) ho perso più tempo a cercare, 
scaricare, installare altri programmi per il cad che disegnare la 
piantina della mia abitazione...

A presto
Pierfausto
