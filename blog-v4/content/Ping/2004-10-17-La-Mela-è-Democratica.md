---
title: "Apple pro Kerry"
date: 2004-10-17
draft: false
tags: ["ping"]
---

La Mela è (a maggioranza) democratica

Premesso che in Usa le elezioni presidenziali sono lontanissime dalla pseudoguerra civile che sono le elezioni politiche italiane, a qualcuno interesserà sapere che Apple Computer privilegia i finanziamenti ai Democratici piuttosto che ai Repubblicani.

Su Internet si possono anche scoprire i <a href="http://www.campaignmoney.com/apple_computer.asp">contributi</a> dai singoli dipendenti o da gruppi di impiegati Apple, quanto e a chi. Per esempio, Steve Jobs nel 2004 ha finanziato personalmente con mille dollari l'organizzazione Friends of <a href="http://www.house.gov/emanuel/">Rahm Emanuel</a> (Congressman democratico dell'Illinois). E si possono vedere le sue donazioni, una per una, dal 1999.

I computer di Apple e la politica americana hanno tanti difetti. Ma i computer di Apple sono più facili da usare e la politica americana è assai più trasparente della nostra, almeno per quanto attiene al vile denaro.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>