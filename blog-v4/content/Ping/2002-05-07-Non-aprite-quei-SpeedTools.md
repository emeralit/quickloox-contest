---
title: "Non aprite quei SpeedTools"
date: 2002-05-07
draft: false
tags: ["ping"]
---

Un programma da cui stare, per il momento, lontani

Se possibile, non usare Hard Disk SpeedTools di Intech in versioni inferiori alla 3.5. È accaduto che alcuni utilizzatori di Mac OS X abbiano riportato danni ai dischi rigidi dopo un boot in Mac OS 9.2.1. Il colpevole, in questi casi, è al 90% Hard Disk SpeedTools, che si prende una serie di libertà senza che la documentazione ufficiale Apple lo consenta, e al 10% Apple, che presuppone per le mappe di partizione dei dischi una regola purtroppo non scritta.
Ragione e torto non stanno mai da una parte sola; ma i dischi rovinati sì, dalla nostra. Per il momento è meglio stare lontani da Hard Disk SpeedTools.

<link>Lucio Bragagnolo</link>lux@mac.com