---
title: "Andarono per killare"
date: 2010-06-13
draft: false
tags: ["ping"]
---

Non mi basta neanche una pagina web per elencare prima tutti i prodotti descritti come <i>iPod killer</i> che oggi non esistono più, mentre iPod è arrivato alla quinta generazione.

Servirebbe poi un'altra pagina per mettere uno dietro l'altro tutti gli <i>iTunes killer</i> che, loro s&#236;, avrebbero venduto musica meglio di iTunes Store.

Forse è il caso di aprire una terza pagina. È recente infatti la notizia della <a href="http://www.slashgear.com/irex-technologies-files-for-bankruptcy-in-netherlands-0989211/" target="_blank">chiusura di iRex Technologies</a>. Il loro lettore di <i>ebook</i> Dr-800Sg, in vendita dallo scorso febbraio, non ha trovato acquirenti sufficienti. Un lettore da 1.024 x 768, a sedici toni di grigio, schermo a inchiostro elettronico, porta miniUsb e connettore per schede di memoria Sd da due gigabyte.

Non c'è da temere; i prossimi mesi saranno pieni di annunci di <i>iPad killer</i>. Intanto iPad, neanche tre mesi, deve avere passato i tre milioni di pezzi venduti. Due milioni in 58 giorni, quando il primo iPhone ne impiegò 74 per farne uno.