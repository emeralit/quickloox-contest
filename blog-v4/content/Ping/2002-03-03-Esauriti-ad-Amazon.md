---
title: "Esauriti ad Amazon"
date: 2002-03-03
draft: false
tags: ["ping"]
---

Un dato interessante per chi applicava vecchie opinioni a nuove realtà

Diceva “bello l’iMac con lo schermo Lcd, speriamo che non subisca l’effetto Cubo”.
Il Cubo, si ricorderà, era una macchina bellissima ma aveva un prezzo troppo alto e non era proprio stato capito da chi lo aveva bollato come poco espandibile, finendo per costare ad Apple un sacco di soldi e cattiva pubblicità.
Con iMac (Flat Panel) la musica, almeno inizialmente, sembra diversa. Amazon, che per esempio è “solo” il più grande venditore online al mondo, ha stravenduto ed esaurito la sua provvista di iMac in appena quattro ore.
Ora mi sa che l’esaurimento verrà ad altri, meno capaci di pensare differente.

<link>Lucio Bragagnolo</link>lux@mac.com