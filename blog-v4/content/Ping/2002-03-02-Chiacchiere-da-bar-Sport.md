---
title: "Chiacchiere da bar Sport & Computer"
date: 2002-03-02
draft: false
tags: ["ping"]
---

Mai fidarsi delle Fonti Affidabili. Ma neanche di Microsoft

Al bar Sport & Compute ho incontrato una Fonte Affidabile.
La Fonte mi ha raccontato non esiste Office 2001 in italiano perché Microsoft avrebbe comunicato esplicitamente ad Apple che, se voleva una versione italiana, Apple Italia avrebbe dovuto pagarne la localizzazione. Apple Italia ha detto no ed eccoci qui.
Da lì siamo passati a dissertare del ginocchio di Ronaldo e del fatto che, secondo Lei, Ronaldo tornerà, più forte di prima.
Per quanto Microsoft non sia esattamente una azienda nota per la correttezza della sua condotta (e già condannata a causa di questo), questa di Office non localizzato perché Apple Italia non tira fuori i soldi mi sembra troppo grossa per crederci, anche se la Fonte è Affidabile.
Ma... e se Ronaldo tornasse, più forte di prima?

<link>Lucio Bragagnolo</link>lux@mac.com