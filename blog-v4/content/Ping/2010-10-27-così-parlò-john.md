---
title: "Così parlò John"
date: 2010-10-27
draft: false
tags: ["ping"]
---

<b>Riccardo</b> ha mirabilmente preparato la <a href="http://quillink.wordpress.com/2010/10/19/intervista-sculley-1/" target="_blank">traduzione italiana integrale dell’intervista di Leander Kahney a John Sculley</a>, imperdibile perché Sculley – a capo di Apple per dieci anni, 1983-1993 – non ha mai parlato diffusamente di come funzionò il suo incarico, né di come si svolse il conflitto con Steve Jobs, da Sculley stesso cacciato fuori Apple quando proprio Jobs aveva insistito perché diventasse amministratore delegato.

Tutti i link che servono si trovano sul sito di Riccardo. A margine: il lavoro di un traduttore professionista, guarda un po’, si distingue immediatamente dalla spazzatura di Babelfish e aggeggi limitrofi.