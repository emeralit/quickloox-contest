---
title: "Datemi un programma"
date: 2005-08-29
draft: false
tags: ["ping"]
---

Ma che sia scriptabile però

Solo qui che fremo per avere Pages compatibile AppleScript. Nel frattempo scopro che Anteprima non è compatibile AppleScript. O, se è compatibile, che non l'ho ancora capito.

No, non basta avere GraphicConverter di serie con il Mac. Voglio Mac OS X compatibile AppleScript. Almeno scriptabile (per essere una persona seria lo dovrei volere recordable e attachable).

Mac OS X vuol dire tutto quello che sta sul Dvd Apple. È semplice. Apple, mi senti? Per domani è impossibile, lo so. Per quando?

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>