---
title: "¡Que viva Nove!"
date: 2002-05-07
draft: false
tags: ["ping"]
---

L’onore delle armi a Mac OS 9, avviato a un luminoso futuro

Alla conferenza mondiale degli sviluppatori Steve Jobs ha spiegato agli sviluppatori che non è più il caso di creare nuovo software per Mac OS X. Cosa che si era già capita, peraltro, mesi fa.
Qualcuno si è subito preoccupato perché usa Mac OS 9. In realtà Jobs non ha detto agli utenti di non usare 9; ha detto ai programatori di programmare per X. Mac OS 9 non sarà dismesso definitivamente prima che passino mesi, forse anni. E lo saluteremo come si conviene a chi ha accompagnato per quasi vent’anni la nostra vita informatica.
Mac OS 9 non ha dunque più futuro? Ma ce l’ha, dentro Mac OS X, naturalmente. In Mac OS X 10.2 tornano per esempio le cartelle a molla, che tanti rimpiangono. Tutte le tecnologie di Mac OS 9 che valevano qualcosa hanno trovato o troveranno posto in Mac OS X.

<link>Lucio Bragagnolo</link>lux@mac.com