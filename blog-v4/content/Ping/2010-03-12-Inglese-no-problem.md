---
title: "Inglese K&#333;'&#299; samasy&#257; nah&#299;&#7745;"
date: 2010-03-12
draft: false
tags: ["ping"]
---

Mi arrabbio sempre un po', benevolmente, di fronte alla gente che si arrende davanti all'inglese senza neanche combattere.

La prova è questo <a href="http://edumobile.org/iphone/" target="_blank">blog di consigli di programmazione per iPhone</a>.

Scritto da indiani, in inglese assai approssimativo e si vede. Ma non si vergognano e i consigli non sono neanche cos&#236; banali. Iscrivendosi, tra l'altro, inviano gratis un <i>ebook</i> sulla programmazione di iPhone.

Il titolo del <i>post</i> è una ipotetica traduzione di <i>no problem</i> da inglese a hindi secondo <a href="http://translate.google.com/" target="_blank">Google Translate</a>. Magari è pure corretto.