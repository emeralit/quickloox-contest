---
title: "Buone vacanze Mac@Work"
date: 2003-07-29
draft: false
tags: ["ping"]
---

Alimento il mio conflitto di interessi e mando tanti auguri a tutti

Nelle due settimane centrali di agosto finalmente Mac@Work si prenderà una meritatissima vacanza.
Sono socio (di estrema minoranza) di Mac@Work e questa è una sfacciata pubblicità che alimenta il mio conflitto di interessi.

Lo store ha aperto prima di Natale e ha costituito la vera novità del panorama Mac italiano. Ha venduto molto ma ha anche offerto assistenza a tanti che altrimenti non l’avrebbero trovata; ha organizzato seminari gratuiti; ha offerto corsi a pagamento; ha messo in piedi eventi di musica, software, incontri con Macworld; ha fatto da sede al <link>PowerBook Owners Club</link>http://www.poc.it, il più grande d’Italia e tra i più grandi in Europa, facendo anche da punto di riferimento per le pizzate dell’ultimo venerdì del mese; ha festeggiato Capodanno offrendo film proiettati a tutta parete, ha ospitato raduni di scacchi, ha venduto computer ma anche libri, e ha dispensato consigli, supporto, soluzioni, suggerimenti, dritte e anche succo di frutta.

Abuso del mio conflitto di interessi per augurare buonissime vacanze a quanti hanno lavorato nello store giorno dopo giorno e hanno già in cantiere le nuove iniziative di settembre.

È uno store; vive per vendere; alla fine quello che conta è il bilancio.

Però, cavolaccio, un <link>posto Mac così</link>http://www.macatwork.net in Italia non si è mai visto prima.

Buone vacanze, gente.

<link>Lucio Bragagnolo</link>lux@mac.com