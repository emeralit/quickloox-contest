---
title: "Hey, bgkem?"
date: 2008-09-14
draft: false
tags: ["ping"]
---

Quando accendo Adium, mi arriva sempre almeno un messaggio di spam, non so se sulla rete di Icq o su quella di Aim.

Il messaggio è sempre simile al titolo qui sopra. Il mittente è un nome fittizio generato automaticamente, tipo waynesmith37.

Quando mi chiedono perché detesto gli Sms pieni di abbreviazioni acronimi e sigle, rispondo che a prima vista finiscono per somigliare moltissimo ai messaggi di cui sopra. E spesso hanno il medesimo valore.