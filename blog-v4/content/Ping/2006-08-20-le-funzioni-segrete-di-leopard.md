---
title: "Le funzioni segrete di Leopard"
date: 2006-08-20
draft: false
tags: ["ping"]
---

Se le conoscessi, non sarebbero segrete. Però ho un'idea sulla direzione in cui pensare.

Questa Worldwide Developers Conference, al contrario di varie edizioni precedenti, non era centrata sul sistema operativo appena uscito, a giochi perlopiù fatti; trattava di una prossima release ancora lontana molti mesi.

Ergo: ciò che Apple teneva a mostrare agli sviluppatori era materiale particolarmente interessante&#8230; per gli sviluppatori. Spunti per utility future, o per richieste di funzioni aggiuntive, per esempio. iChat Theater, Time Machine o la rivoluzione che presumibilmente vedrà iCal sono tre ottimi esempi di nuovi campi di gioco in cui i programmatori potranno sguazzare con plugin, estensioni, utility astute e cos&#236; via.

Le cose che agli sviluppatori interessano meno sono quelle su cui hanno meno possibilità di intervento, che le vedano ora oppure tra sette mesi. E le funzioni che Steve Jobs non ha inteso mostrare, per non &#8220;ispirare&#8221; la concorrenza (o perché non erano pronte? Altro argomento intrigante, ma non divaghiamo) dovranno essere di questo tipo.

Quali siano o possano essere nel dettaglio, a parte l'ovvio pensiero dell'interfaccia utente grafica, viene lasciato come esercizio di riflessione collettiva. :)