---
title: iPhone l'africano
date: 2010-07-30
draft: false
---

In ossequio alla tendenza che vuole la contemplazione ombelicale sufficiente a sentenziare sul mondo, il cronista virtuale si è tramutato in reporter sul campo e ho passato qualche quarto di miglio per il centro di Milano a verificare la disponibilità di iPhone 4 nel primo giorno.

La nota che ho trovato peculiare non è tanto la pratica indisponibilità dell’oggetto, quanto il trattamento riservato al cliente.

Saltati a priori i rivenditori Apple (c’è chi ha messo addirittura la segreteria telefonica: iPhone 4 non lo abbiamo proprio), il primo centro Tim esponeva un cartello: I-Phone (sic) esauriti, è possibile prenotare senza impegno.

Non si capiva quale delle due parti operasse senza impegno. Certo, se fossero entrambe, farei fatica a capire il senso del prenotare.

Il centro Vodafone ha ripetuto la tattica taglieggiatrice già applicata con successo a iPad. Ti danno iPhone 4 solo se sottoscrivi l’abbonamento. Conseguita sul momento una laurea honoris causa in matematica per valutare rapidamente la giungla dei piani a disposizione, ho concluso di volere più che mai l’opzione ricaricabile. Ma Vodafone la concede solo a chi sia disposto ad attendere.

Il secondo centro Tim aveva terminato la scorta. La gentilissima commessa mi ha però spiegato che potevo prenotare… per poi ripassare a fine agosto perché sa, mi spiace, ma domani chiudiamo. Non ho chiesto se la prenotazione era senza impegno, visto che a fine agosto non avrei alcun problema a trovare disponibilità.

Se occorreva la smentita pratica della validità della contemplazione ombelicale come metro di giudizio dell’universo, eccola: a Kampala, Mogadiscio o Accra si troverebbe certamente un livello di servizio migliore di quello offerto dalle telefoniche italiane. Dove la scarsità di iPhone 4 importa nulla, ma l’inconsistenza dell’attenzione e del rispetto per chi sta davanti al commesso è ai massimi livelli planetari.
