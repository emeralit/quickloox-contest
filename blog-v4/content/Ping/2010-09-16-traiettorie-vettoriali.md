---
title: "Traiettorie vettoriali"
date: 2010-09-16
draft: false
tags: ["ping"]
---

Per quanti, come il sottoscritto, fossero da tempo alla ricerca di un programma di disegno vettoriale che non sia necessariamente mastodontico come Adobe Illustrator, non sia cervellotico come <a href="http://cenon.info/" target="_blank">Cenon</a> e funzioni nell'interfaccia standard di Mac OS X a differenza di <a href="http://inkscape.org/" target="_blank">Inkscape</a>, BohemianCoding ha annunciato <a href="http://www.bohemiancoding.com/sketch" target="_blank">Sketch 1.0</a>, oltretutto a prezzo accettabile.

Non lo giudicherei tanto dal prezzo o da quello che fa la versione 1.0, quanto dalla pulizia dell'interfaccia e dalla velocità di utilizzo. Appena ritorno con una connessione accettabile ci faccio un giro.