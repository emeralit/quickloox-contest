---
title: "Poter programmare è come avere la racchetta magica"
date: 2006-02-14
draft: false
tags: ["ping"]
---

Mi sono appena reso conto che vent'anni fa per scrivere <a href="http://www.pong-story.com/" target="_blank">Pong</a> ci voleva un'azienda. Vent'anni dopo, chiunque di noi può provarci <a href="http://www.linuxdevcenter.com/pub/a/linux/2005/12/15/clone-pong-using-sdl.html" target="_blank">da solo</a> nel giro di un pomeriggio.