---
title: "Appello per Filippo"
date: 2008-01-18
draft: false
tags: ["ping"]
---

<strong>Filippo</strong> mi sta inondando di cose interessanti e carine. Le ultime della serie sono un perfetto <a href="http://www.magnificaweb.it/files/espressioni_regolari_cheat_sheet.pdf" target="_blank">promemoria in Pdf</a> per usare le espressioni regolari (pubblico 1, critica 5) e il geniale <a href="http://www.official-linerider.com/" target="_blank">Line Rider</a> (pubblico 4, critica 3), che c'è da passarci le giornate (e soprattutto farle passare ai bimbi).

Sto consigliandolo di aprirsi un blog suo, dove potrà scatenarsi in libertà. Mi racconta che forse ci siamo, speriamo. Se non lo fa lui, qualcuno lo aiuti.

P.S.: Filippo è un maestro di ironia e anche autoironia. Lo dico non per lui. :-)