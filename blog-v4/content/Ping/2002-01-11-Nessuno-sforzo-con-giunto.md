---
title: "Nessuno sforzo con giunto"
date: 2002-01-11
draft: false
tags: ["ping"]
---

Un piccolo segreto dell’iMac delle meraviglie, quello dove ogni componente, come ha dichiarato Steve Jobs, “è fedele a se stesso” e ciò che deve essere piatto lo è (lo schermo piatto) e ciò che deve essere orizzontale per funzionare al meglio lo è (il lettore/masterizzatore/SuperDrive ottico).
Chi avesse dubbi sulla solidità o la tenuta del giunto metallico che collega lo schermo Lcd all’unità centrale, sappia che Apple raccomanda di usarlo come maniglia quando si tratta di spostare l’iMac.
Il giunto suddetto è di acciaio inossidabile e, secondo Apple, non avrà mai bisogno di manutenzione per continuare a garantire la stabilità della posizione dello schermo intanto che permette di spostare lo schermo stesso con lo sforzo minimo di due dita.
C’è dietro anche uno studio ergonomico perché la posizione dello schermo davanti all’utilizzatore sia comunque ottimale. Ma è un’altra storia.

lux@mac.com