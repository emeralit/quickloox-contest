---
title: "Mirare all'angolino"
date: 2005-08-22
draft: false
tags: ["ping"]
---

Una modifica significativa all'interfaccia di Mac OS X

Porta il mouse nell'angolo in alto a sinistra. Clicca. Scende il menu Apple.

Porta il mouse sopra la mela del menu Apple e clicca.

Il secondo compito è ben più difficile del primo. Il primo si fa anche senza guardare e non richiede alcuna precisione. È molto più facile.

Non è da tantissimo che le cose stanno come adesso. Prima bisognava centrare la mela.

A dire che Apple continua a lavorare all'interfaccia di Mac OS X. Perfino quando è solo questione di un angolo.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>