---
title: "Morali omnidirezionali"
date: 2010-04-28
draft: false
tags: ["ping"]
---

Steve Jobs e Steve Wozniak decisero che avrebbero avuto il 45 percento ciascuno delle azioni di Apple e, per avere un ago della bilancia che decidesse nei casi controversi, proposero nel 1976 a Ronald Wayne, il terzo socio fondatore di Apple, di prendere il dieci percento.

Wayne rifiutò per timore che la nuova azienda potesse fallire e rimase al proprio lavoro di contabile.

Se avesse accettato, avrebbe un patrimonio personale di circa 23 miliardi di dollari.

Oggi, settantacinquenne, vive da modesto pensionato in una cittadina del Nevada nei pressi della Valle della Morte e <a href="http://www.telegraph.co.uk/technology/apple/7624539/US-pensioner-Ronald-Wayne-gave-up-15bn-slice-of-Apple.html" target="_blank">non ha alcun rimpianto</a> del passato. È compiaciuto del successo che hanno conseguito Jobs e Wozniak, afferma che Jobs se lo meritava e cinque anni fa è stato anche invitato come Vip a un evento Apple, l'ultima volta che si sono incontrati.

Le storie interessanti hanno una morale. Quelle più interessanti hanno una morale che punta in più direzioni.