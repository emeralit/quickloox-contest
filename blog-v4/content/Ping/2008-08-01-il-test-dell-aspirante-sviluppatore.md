---
title: "Il test dell'aspirante sviluppatore"
date: 2008-08-01
draft: false
tags: ["ping"]
---

Per molti di noi va un po' troppo nel dettaglio, ma è interessante e ad agosto si fanno i test, no? Ecco perché provare le <a href="http://www.infoworld.com/article/08/07/28/31FE-programming-iq-test-tease_1.html?source=rss&amp;url=http://www.infoworld.com/article/08/07/28/31FE-programming-iq-test-tease_1.html" target="_self">domande di Infoworld</a>. Ho scoperto due o tre cose alquanto sorprendenti&#8230; guardando le risposte.

(se fai 25 o più, vanne fiero)