---
title: "No, non è la Pcmcia"
date: 2003-03-04
draft: false
tags: ["ping"]
---

AirPort Extreme e perché Apple non è cattiva come sembra

Un’altra delle follie collettive che coinvolge periodicamente la comunità Mac è quella della scheda AirPort Extreme. Tutti vorrebbero inserirla nei computer usciti prima del 2003 e si meravigliano che sia impossibile farlo. Naturalmente la colpa è di Apple, che persegue i fini più inconfessabili.

Le schede AirPort Extreme non possono funzionare se non sui Mac progettati appositamente per esse, perché non sono schede Pcmcia, bensì Pci. Anche se hanno grosso modo le stesse dimensioni delle vecchie schede – in realtà sono appena più piccole – non sono le stesse schede.

E sono Pci perché il bus Pcmcia non ha abbastanza banda per sfruttare la maggiore velocità di AirPort Extreme, non perché Apple sia cattiva o incompetente.

Augh, ho detto.

<link>Lucio Bragagnolo</link>lux@mac.com