---
title: "Stressati anche a riposo"
date: 2010-09-03
draft: false
tags: ["ping"]
---

La spia dello <i>standby</i> nei MacBook Pro si accende e spegne periodicamente allo stesso ritmo di un essere umano che respira con calma, <a href="http://floodlite.tumblr.com/post/1011047822/apples-attention-to-detail" target="_blank">tra i dodici e i venti cicli al minuto</a>. Non è un caso, ma un <a href="http://www.freepatentsonline.com/6658577.html" target="_blank">brevetto</a>.

I portatili Dell hanno una spia simile, che però è impostata a 40 cicli per minuto, equivalente di un essere umano impegnato in un esercizio fisico pesante.

Quando un portatile Dell riposa, lo segnala alla frequenza di una corsa a perdifiato. Quando riposa un MacBook Pro, lo fa come un essere umano rilassato in poltrona.

Quale delle due aziende è più capace di tradurre le soluzioni ingegneristiche in applicazioni a misura d'uomo? E dove starà la maggiore attenzione ai dettagli?

Qualcuno dirà che la frequenza della spia non ha alcuna vera utilità. Verissimo: si possono fare cose che funzionano anche lavorando con approssimazione. Solo che prima o poi l'approssimazione mostra il proprio limite e di soluzioni ingegneristiche su cui decidere, dentro un portatile, ce ne sono centinaia. Fino a dove si saranno spinte l'approssimazione di Dell e l'attenzione di Apple?