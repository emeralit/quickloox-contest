---
title: "Si vola bassi"
date: 2010-11-10
draft: false
tags: ["ping"]
---

Se n'era parlato l'anno scorso. Anche quest'anno, in occasione del periodo natalizio, Google offre <a href="http://googleblog.blogspot.com/2010/11/happy-holidays-from-google-chrome-free.html" target="_blank">Wi-Fi gratuito sui voli americani</a> marchiati Delta, AirTran e Virgin America, dal 20 novembre al 2 gennaio.

Al massimo noi si può sperare nell'abolizione del decreto Pisanu.