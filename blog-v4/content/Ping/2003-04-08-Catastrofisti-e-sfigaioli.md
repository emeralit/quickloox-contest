---
title: "Catastrofisti e sfigaioli"
date: 2003-04-08
draft: false
tags: ["ping"]
---

Una brutta notizia per chi vive per gufare su Apple

Si vocifera che in un futuro più o meno prossimo Macintosh potrebbe passare dai processori PowerPC fabbricati da Motorola ai processori Power 970, discendenti dei Power 4 da superserver, fabbricati da Ibm.

Se fosse vero, sarebbe un colpo mortale ai gufatori di Apple, quelli che perfino in ogni starnuto di Steve Jobs vedono la minaccia della fine imminente, chissà poi perché.

I portatori di disgrazia (sì, perché alla fine la sfiga sta addosso a loro) assistevano ai tagli di personale di Motorola e pensavano, predicevano, prevedevano, preannunciavano problemi per Apple. Se fallisce Motorola (certo, se una ditta licenzia fallisce, no? Come la Fiat), era il loro mantra, Apple non saprà che cosa infilare nei Mac.

Ma se domani i processori i processori li facesse Ibm... e quando fallisce, Ibm? Gli sfigaioli sono già in preda al panico.

<link>Lucio Bragagnolo</link>lux@mac.com