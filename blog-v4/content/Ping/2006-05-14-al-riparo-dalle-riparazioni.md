---
title: "Al riparo dalle riparazioni"
date: 2006-05-14
draft: false
tags: ["ping"]
---

Si riuscirà mai a convincere la gente che la riparazione dei privilegi serve solo quando ci sono problemi precisi e non è una misura di manutenzione ordinaria?

Stavolta ci prova <a href="http://www.unsanity.org/archives/000410.php" target="_blank">rosyna</a>, uno dei programmatori di Unsanity.

La riparazione dei privilegi può, a volte, servire, in tre situazioni: fare il backup dei file di Mac OS X su un disco che ignora i privilegi stabiliti; usare istruzioni non aggiornate per installare servizi di sistema; usare un installer open source per aggiungere o sostituire uno dei componenti di serie in Mac OS X, tipo Perl, Cups, Apache eccetera.

In altre situazioni in cui i privilegi di parti specifiche di Mac OS X sono corrotti, la riparazione dei privilegi può aiutare. Ma non è garantito. E farla regolarmente è <em>sbagliato</em>.