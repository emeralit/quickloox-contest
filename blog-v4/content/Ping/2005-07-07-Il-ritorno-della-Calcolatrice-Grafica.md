---
title: "Il ritorno della Calcolatrice Grafica"
date: 2005-07-07
draft: false
tags: ["ping"]
---

L'edizione freeware supporta anche Tiger

Per certi versi Grapher è meglio (l'hai guardato? È notevole), ma per altri la Calcolatrice Grafica resta un programma insuperato nella dotazione di Mac.

Ora la versione 3.0 della Calcolatrice è a pagamento, tuttavia la 1.x resta gratuita. La notizia è che la nuova <a href="http://www.pacifict.com/FreeStuff.html">versione 1.5</a> supporta anche Tiger. È uno di quei download che vanno fatti, anche solo giusto per guardare il programma e scoprire qualcosa di nuovo.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>