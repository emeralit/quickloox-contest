---
title: "L'Apprendista Stregone"
date: 2006-02-26
draft: false
tags: ["ping"]
---

Quello che gli dava fastidio avere nel Mac file invisibili. Allora ha scaricato un programma per renderli visibili. Solo che si è reso conto della stupidaggine compiuta e ha deciso di tornare indietro. Però non riesce più a ritrovare il programma che ha usato…

Mi ringrazi che non faccio il nome.