---
title: "Convertiti"
date: 2007-07-23
draft: false
tags: ["ping"]
---

Da leggere con l'accento che si vuole. Coincidenza, <a href="http://it.wikipedia.org/wiki/Sincronicita%CC%80" target="_blank">sincronicità</a>, non lo so; in due giorni ho incontrato due professionisti della scrittura, stufi marci di Word, che volevano software leggero, gratis o quasi gratis, potente quanto basta per essere davvero utile.

Tutti e due hanno adottato <a href="http://www.tex-edit.com" target="_blank">Tex-Edit Plus</a> e sono entusiasti. Io ci sono dentro dagli anni Novanta e non posso che condividere. In verità, dove gli attributi sono testo puro (ossia in Html) uso <a href="http://www.barebones.com/products/bbedit" target="_blank">BBEdit</a>; però, dove non lo sono, Tex-Edit Plus è ottimo.

No, mi sono rifiutato di titolare questo post <em>Il programma con gli attributi</em>.