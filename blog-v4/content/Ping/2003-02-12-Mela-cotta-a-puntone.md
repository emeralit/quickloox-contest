---
title: "Una mela cotta a puntone"
date: 2003-02-12
draft: false
tags: ["ping"]
---

Gli americani sono capaci di tutto, anche di parlarne

Ci sono troppe foto e troppo realistiche perché sia una montatura: il PowerBook G4 di una signora è stato per venti minuti in <link>forno</link>http://apple.slashdot.org/apple/03/02/06/1447229.shtml?tid=133&tid=180 a una temperatura ben superiore a quella raccomandata sulla documentazione Apple.

Nonostante un bel puntone marrone sull’involucro e qualche danno allo schermo Lcd, il computer funziona ancora.

Non sai se c’è da fare un monumento ad Apple o provare a mettere in forno la signora.

<link>Lucio Bragagnolo</link>lux@mac.com