---
title: "La gente non legge più come una volta"
date: 2010-08-07
draft: false
tags: ["ping"]
---

È passata nel silenzio pressoché totale la notizia di un'<i>app</i> Android scaricata da milioni di utenti (da 1,1 a 4,6 milioni, è la stima) in quanto sfondo scrivania, mentre invece avanza una richiesta generica di <i>informazioni sul telefono</i> e poi <a href="http://mobile.venturebeat.com/2010/07/28/android-wallpaper-app-that-steals-your-data-was-downloaded-by-millions/" target="_blank">invia a un server cinese di Shenzhen</a> cronologia del browser, Sms, numero della Sim, identificativo dell'abbonato e financo password della segreteria telefonica.

Sento nostalgia della vivacità con la quale si è <a href="http://blog.jakelm.it/itunes-store-cracckatoquando-il-cacciatore-mi" target="_blank">gridato alla violazione di iTunes Store quando non era vero niente</a>.

Per non parlare di quando è saltata fuori <a href="http://www.theregister.co.uk/2010/07/20/browser_info_disclosure_weaknesses/" target="_blank">la falla di Safari</a> che permette di carpire <i>nome e indirizzo</i> (informazione che si ricava, per chi non lo sapesse, a partire dalle <a href="http://www.paginebianche.it" target="_blank">Pagine Bianche</a>, molto più comodo che ingegnarsi con JavaScript) e, vai con il notizione, <a href="http://attivissimo.blogspot.com/2010/07/safari-ie-firefox-si-fanno-rubare-dati.html" target="_blank">per il pubblico italiano</a> la faccenda diventa <i>dati e password</i>. Che non è proprio la stessa cosa.

La gente non legge più come una volta. Dipende, certo, anche da quello che si scrive.