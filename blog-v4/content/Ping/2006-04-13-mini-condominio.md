---
title: "Mini-condominio"
date: 2006-04-13
draft: false
tags: ["ping"]
---

<a href="http://www.kero.it" target="_blank">Fede</a> mi ha segnalato una <a href="http://www.macminicolo.net/" target="_blank">ragione in più</a> dell'utilità di Mac mini nel listino Apple, e uno - interessante - dei <a href="http://www.airbagindustries.com/" target="_blank">clienti</a> del servizio. Grazie! :-)