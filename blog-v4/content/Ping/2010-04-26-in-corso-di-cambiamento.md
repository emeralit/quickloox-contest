---
title: "In corso di cambiamento"
date: 2010-04-26
draft: false
tags: ["ping"]
---

Ho tenuto un corso serale di utilizzo base di Mac OS X a una classe in parte di madrelingua italiana e in parte spagnola.

In considerazione di questo, la prima cosa che ho spiegato è la possibilità di avere il sistema in qualsiasi lingua si voglia.

La seconda è stata la possibilità di definire esattamente quante e quali lingue applicare nella correzione ortografica.

La terza è stata mostrare che si possono avere contemporaneamente in funzione Finder in italiano, Safari in spagnolo, TextEdit in francese, iCal in tedesco, iTunes in finlandese&#8230; il programma che si vuole nella lingua che si vuole.

Nelle pause della lezione, un manipolo di ragazzi si radunava attorno a uno dei Mac e componeva musica su GarageBand.

Al termine del corso uno dei ragazzi mi ha spiegato che si era comprato un Mac. <cite>Io vengo da Windows</cite>, ha detto, <cite>e questo sistema operativo è un altro pianeta</cite>.

Anni fa Apple scriveva in fondo ai comunicati stampa di voler cambiare il mondo, una persona per volta. Oggi scrive cose diverse, non perché si sia dimenticata del cambiamento. Perché non c'è più bisogno di annunciarlo; accade e basta.