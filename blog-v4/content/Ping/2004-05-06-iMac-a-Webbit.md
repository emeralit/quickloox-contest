---
title: "Il collo fuori"
date: 2004-05-06
draft: false
tags: ["ping"]
---

I tempi cambiano e anche a Webbit

Scrivo questo Ping da <link>Webbit</link>http://webb.it in quel di Padova e quest’anno, tra tante altre novità, ne segnalo una che forse altri non avrebbero: nell’arena è apparso anche un iMac Lcd, di Kikki.

Di Mac a Webbit se ne vedono sempre numerosi, ma è la prima volta che nella fossa dei leoni dell’open source e dell’hackeristica spunta, con il suo collo un po’ Terminator un po’ da cigno cibernetico, un mezzo panettone color neve con la mela argentata sopra.

<link>Lucio Bragagnolo</link>lux@mac.com