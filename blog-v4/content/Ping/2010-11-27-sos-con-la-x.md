---
title: "Sos con la X"
date: 2010-11-27
draft: false
tags: ["ping"]
---

Con tutto il rispetto per chi si sta impegnando in una causa degna di apprezzamento e di sostegno, credo che sarebbe più facile salvare gli Xserve <a href="http://www.apple.com/it/xserve/" target="_blank">in procinto di dismissione</a> con quattromila acquisti che con quattromila <a href="http://www.savethexserve.com/" target="_blank">firme elettroniche</a>.