---
title: "La soluzione cinque percento"
date: 2009-09-07
draft: false
tags: ["ping"]
---

A beneficio di quanti cercano test sulle prestazioni di Snow Leopard, Chris Rawson di The Unofficial Apple Weblog <a href="http://www.tuaw.com/2009/09/02/benchmarking-results-is-snow-leopard-really-any-faster-than-leo/" target="_blank">ha usato Geekbench</a> sulle macchine a propria disposizione.

Riassumo le conclusioni.

MacBook Pro Intel Core 2 Duo (inizio 2008) da 2,60 gigahertz con 4 gigabyte di Ram:

Mac OS X 10.5.2: <b>3.364</b>
Mac OS X 10.6: <b>3.511</b>

MacBook Intel Core 2 (fine 2006) da 2,16 gigahertz con 2 gigabyte di Ram:

Mac OS X 10.5.2: <b>2.773</b>
Mac OS X 10.5.8: <b>2.849</b>
Mac OS X 10.6: <b>2.977</b>

L'articolo è più dettagliato sulle cifre, che si capiscono benissimo anche senza inglese. Consiglio la lettura. In sostanza: c'è un guadagno del tre, quattro, cinque percento da Leopard a Snow Leopard, secondo come la si guarda.

Aggiungo che l'idea di usare <a href="http://www.primatelabs.ca/geekbench/" target="_blank">Geekbench</a> appena installato un sistema e appena prima di installare il sistema successivo è da considerare, cos&#236; uno ha un quadro attendibile delle prestazioni.

Geekbench ha pagna dedicate ai risultati degli utenti registrati e c'è una <a href="http://www.primatelabs.ca/blog/category/mac-os-x/" target="_blank">comparazione di Snow Leopard su Leopard</a>, ma non rispetto a Tiger, come chiedeva qualcuno.