---
title: "Nessuna notizia, due notizie"
date: 2005-08-27
draft: false
tags: ["ping"]
---

Il trucco dei siti di gossip diventa chiaro

Scrive <strong>Daniele</strong>:

<cite>Non si può dire manco sia colpa del caldo. Basta la notizia che Apple si è assicurata la fornitura di G4 da Freescale fino al 2008 (probabilmente a fini di avere le parti di ricambio per coprire le AppleCare), e un rumor sul fatto che IBM abbia messo in sviluppo un processore Cell modificato per andar bene pure per dei computer e non solo per le console, e subito si scatenano quelli che dicono &ldquo;che succede? Forse Apple sta tornando a Ibm e non passerà più a Intel?&rdquo;</cite>

<cite>Insomma, preparatevi, mi è appena arrivato il numero nuovo di MacWorld tutto dedicato alla transizione a Intel, ma i rumor dicono che per Ottobre, dopo l'Apple Expo, dovrete smentire tutto e farne uno sulla transizione da Ppc a Cell ;-)</cite>

Daniele ha capito tutto. Se ho un sito di gossip, scrivo una sparata grande così (giusto un punto interrogativo davanti, per avere la scusa pronta) e poi, quando è evidente che non sta in piedi, scrivo che le indiscrezioni non erano vere.

Non è successo niente, ma da zero notizie ne ho ricavate due.

<a href=&rdquo;mailto:lux@mac.com&rdquo;>Lucio Bragagnolo</a>