---
title: "Le estensioni che restringono"
date: 2003-05-29
draft: false
tags: ["ping"]
---

Un errore che Apple dovrebbe riconoscere, nell’interesse generale

Sono innamorato di Mac OS X e non vorrei mai tornare indietro, ma una cosa va sistemata e anche presto: Apple, ridacci i metadati.

La mia collega B. stava cercando di scaricare una immagine da un sito e, com’è come non è, si è ritrovata sulla scrivania un file con estensione .asp, proprio della (penosa) tecnologia Active Server Pages di Microsoft.

Ha fatto doppio clic sul file ed è partito... Apple System Profiler.

Il problema è che in Mac OS X si vuole presumere che i file vengano distinti e assegnati alle applicazioni secondo la loro estensione. Prima, in Mac OS 9, i file contenevano informazioni specifiche (dette metadati) che ne indicavano l’assegnazione.

È palese, dall’esempio sopra riportato, che il modello basato sulle sole estensioni fa ridere e non estende un bel niente; piuttosto, restringe la qualità della user experience di Mac OS X. Tra l’altro Mac OS X continua a supportare il vecchio sistema, solo che le specifiche Apple hanno smesso di renderlo obbligatorio.

Apple, ridammi i metadati. Fa comodo a tutti e fa parte dell’esperienza Mac. Quella di computer facili da usare. Se non lo fai, mi lamenterò pesantemente sulla tua pagina di <link>feedback</link>http://www.apple.com/macosx/feedback e magari lo farà anche qualcun altro.

<link>Lucio Bragagnolo</link>lux@mac.com