---
title: "Un Terminale per smemorati"
date: 2004-07-30
draft: false
tags: ["ping"]
---

I worksheet Unix di BBEdit: una mano santa per chi non riesce a ricordare che…

A volte il Terminale è prezioso, a volte è utile, sempre hai sulla punta del polpastrello quel comando che ti aveva salvato la pellaccia digitale l’altra volta e adesso proprio non ricordi, il tasto tab non autocompleta niente di utile e apropos non restituisce nulla di sensato.

È il momento dei worksheet Unix di BBEdit: sono file di testo dove è possibile segnare tutti i comandi utili per la prossima volta che li vuoi eseguire.
Perché non salvi una sessione del Terminale, dici? Perché è un file di testo e basta. Invece i worksheet di BBEdit riconoscono i comandi Unix e li possono eseguire, proprio come se fossi nel Terminale.

Il classico meglio dei due mondi. È un file di testo e quindi ricerche e sostituzioni, editing manuale facilissimo eccetera. È un ambiente Unix e quindi dai un comando e lui risponde. Anche se il comando era stato salvato lì lo scorso trimestre. Ecco qual era, era ovvio, a vederlo...

<link>Lucio Bragagnolo</link>lux@mac.com