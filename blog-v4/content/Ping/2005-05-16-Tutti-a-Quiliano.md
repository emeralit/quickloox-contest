---
title: "Altro che museo"
date: 2005-05-16
draft: false
tags: ["ping"]
---

C'è più vita in un Apple ][ acceso che in mille G5 spenti

Come promesso, sabato scorso sono stato a Quiliano a presenziare all'inaugurazione dell'All About Apple Museum, il primo in Europa. C'erano anche il sindaco, centinaia di persone, giornalisti ed esperti del settore, ma questo poco conta

A vedere l'allestimento ho provato ammirazione per la convinzione, la capacità la genialità con cui Alessio Ferraro e soci del club hanno superato uno dopo l'altro tutti gli ostacoli (che non sono stati né pochi né piccoli, dagli spazi ai soldi, al tempo). Poi invidia, perché i miei vecchi Mac ho dovuto darli via, per quanto ad amici e parenti. Infine sollievo per i membri del club, dal momento che le loro mogli non chiederanno più di liberare la cantina!

Battute sceme a parte, lo stupore di vedere le macchine accese e sperimentabili è senza paragoni. Non è un museo, perché nei musei si mettono le mummie, i cocci, i reperti, la storia. A Quiliano è tutto presente, anche se ha trent'anni di vita. Vedere dodicenni intraprendenti scoprire Lisa, o saggiare un NeXT funzionante, sorprende e scalda il cuore.

Senza contare le scoperte che chiunque può fare. Gli autori si scherniscono dicendo che manca un sacco di roba e sarà anche vero, ma si vedono pezzi mai arrivati in Italia, il set top box sperimentale costruito da Apple, la console Pippin, eMate e tantissime altre cose.

Stabilito che nessuno mai farebbe un museo di stupidi cassoni grigi senza bellezza e senza creatività dentro, per una persona minimamente interessata alla storia di Mac il museo, chiamiamolo così per comodità, è assolutamente da visitare, almeno una volta nella vita. Museo. È una macchina del tempo, geniale, piena di misteri da scoprire, imperdibile.

Tutti a Quiliano all'<a href="http://www.allaboutapple.com/present/sede.htm">All About Apple Museum</a>. E tanti, tanti complimenti.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>