---
title: "Il primo passo in pubblico"
date: 2011-01-14
draft: false
tags: ["ping"]
---

Per ragioni professionali dovevo presentarmi con un Mac su cui girassero almeno tre motori di pubblicazione contenuti installati localmente, affinché il client potesse ravanare e decidere quale fosse quello più indicato per i suoi gusti.

In una serata ho completato la missione installando <a href="http://wordpress.org/" target="_blank">Wordpress</a>, <a href="http://textpattern.com/" target="_blank">Textpattern</a>, <a href="http://plone.org/" target="_blank">Plone</a> e per esagerare (in realtà per fornire una pietra di paragone totalmente diversa dal resto) anche <a href="http://www.mediawiki.org/wiki/MediaWiki" target="_blank">MediaWiki</a>.

Arrivare a un sito fatto finito e funzionante con personalizzazioni arbitrarie è un'altra questione. Sono però rimasto sorpreso di averci messo cos&#236; poco e di avere risolto cos&#236; velocemente i piccoli problemi che si sono presentati.

Non è un compito <i>facile</i>. Bisogna capire un minimo di permessi, c'è sempre un passo di installazione semimanuale, almeno un file .php va aperto e personalizzato altrimenti non funziona, un'idea di come funziona un sito dinamico elementare aiuta e bla bla bla.

Un modesto ricorso ai siti di provenienza e a Google, <a href="http://www.mamp.info/en/index.html" target="_blank">Mamp</a> e un pizzico di attenzione sono tuttavia sufficienti.

