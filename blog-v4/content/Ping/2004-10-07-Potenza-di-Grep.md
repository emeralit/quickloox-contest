---
title: "Potenza di grep"
date: 2004-10-07
draft: false
tags: ["ping"]
---

Un dilettante del Terminale nel Paese delle meraviglie

Non tutti comprenderanno il mio entusiasmo; dopotutto c'è anche gente che ha studiato.
Però la notte scorsa ho dato nel Terminale il comando

<code>grep -r 'http' /iPod/Scopo\ sitografia > /iPod/nuovo/sitografia_uno_prima</code>

Improvvisamente tutti gli indirizzi di siti Web contenuti in una quindicina di file di testo da trecentomila caratteri complessivi sono finiti in un nuovo file. Per chi ne sa: un paio di comandi dopo, anche il file risultante era ripulito e conteneva esattamente e solo gli Url.

Mac OS X può tranquillamente essere usato senza Terminale, ma anche senza AppleScript, se è per quello. Tuttavia, ogni tanto, un AppleScript o un comando di Terminale valgono ore di lavoro. Non sono obblighi da imparare, ma strumenti in più.

Per chi volesse approfondire:

<code>/Library/Documentation/Commands/Grep/</code>

Sì, quello che c’è da sapere è già tutto nel sistema.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>