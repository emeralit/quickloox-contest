---
title: "Think Different"
date: 2010-01-16
draft: false
tags: ["ping"]
---

Mentre scrivo, sulla prima pagina del <a href="http://www.apple.com/" target="_blank">sito Apple americano</a> trovo cinque strilli di copertina. Uno di questi è <cite>Aiuta le vittime del terremoto a Haiti - dona su iTunes</cite>.

Mentre scrivo, sulla prima pagina del <a href="http://www.microsoft.com/en/us/default.aspx" target="_blank">sito Microsoft americano</a> trovo tre strilli di copertina. Uno di questi è <cite>Rippa. Masterizza. Sincronizza. Riproduci</cite>.

Ci sono tante ragioni per le quali preferisco Mac OS X.

<i>Aggiornamento</i>: ora nelle News della pagina Microsoft compare un messaggio microscopico che annuncia le (lodevoli) <a href="http://www.microsoft.com/about/corporatecitizenship/en-us/our-actions/in-the-community/disaster-and-humanitarian-response/community-involvement.aspx" target="_blank">iniziative di Microsoft</a>.