---
title: "Quella dell'amore"
date: 2008-06-02
draft: false
tags: ["ping"]
---

Patrick Luby e Ed Peterlin sono venuti a dare spettacolo in <a href="http://atworkgroup.net" target="_blank">@Work</a> con un&#8230; Titanium.

Promemoria: si può sviluppare <a href="http://neooffice.org" target="_blank">NeoOffice</a> con un computer di molte generazioni fa.

Loro lo amano talmente tanto che ci hanno fissato pure la maniglia.