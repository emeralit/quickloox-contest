---
title: "Consigli per i non acquisti"
date: 2011-01-08
draft: false
tags: ["ping"]
---

Vedo ampia confusione in argomento e provvedo a una chiarificazione rapida.

Mi metto nei panni di chi ha comprato un software per Mac prima che arrivasse Mac App Store.

Ora vedo quello stesso software in vendita su Mac App Store, in un numero di versione diverso.

Se provo a scaricarlo, Mac App Store mi informa che ho già il programma sul disco e NON lo scarica.

Se non provo a scaricarlo, Mac App Store NON si accorge della sua esistenza sul mio disco e non propone un eventuale aggiornamento.

Come regola generale, le cose che abbiamo comprato fuori da App Store hanno aggiornamenti per noi fuori da App Store, fino a quando non compare su Mac App Store una versione apposta a unificare il percorso di aggiornamento.

Per esempio, Pixelmator obbliga tutti a ricomprarsi il programma a 29 dollari, ma in cambio dello sforzo (a Milano si fa fatica a mangiare una pizza con il dolce, con questa cifra) <a href="http://www.pixelmator.com/transition/" target="_blank">la prossima versione 2.0 sarà gratis</a> per chi ricompra oggi. E possono approfittarne tutti, anche chi Pixelmator non lo aveva.

Si tratta di una situazione ibrida inevitabile e che si normalizzerà con il tempo. Intanto è inutile affannarsi e, se proprio siamo affamati di aggiornare un programma comprato esternamente a Mac App Store, cerchiamo lumi sul sito del produttore, che segnalerà certamente come ha deciso di procedere.

Si tenga presente inoltre che, al momento attuale, è possibile che un programma su Mac App Store sia in edizione differente da quella acquistata a suo tempo e che l'acquisto su Mac App Store non sia affatto indicato. Per esempio, BBEdit e TextWrangler sono al momento disponibili su Mac App Store <a href="http://www.barebones.com/store/macappstore.html" target="_blank">in versione priva di alcuni comandi</a> invece presenti nella versione acquistata in modalità tradizionale. Sempre al momento, chi possiede BBEdit o TextWrangler ricordi che i due programmi propongono gli aggiornamenti da soli, automaticamente, e che non c'è bisogno di fare niente su Mac App Store. A margine, <a href="http://www.barebones.com/products/textwrangler/" target="_blank">TextWrangler</a> è gratis e chi non ce l'ha perde uno dei migliori editor di testo sulla piazza.

Un po' di calma nel procedere sarà la migliore ricetta per non crearsi problemi. Ed è tutto quello che serve realmente sapere su Mac App Store al momento.

