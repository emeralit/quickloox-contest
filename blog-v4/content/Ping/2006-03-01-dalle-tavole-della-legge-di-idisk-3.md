---
title: "Dalle tavole della legge di iDisk/3"
date: 2006-03-01
draft: false
tags: ["ping"]
---

La prima cosa da fare per sfruttare al massimo il tuo iDisk è proporzionare secondo le tue esigenze lo spazio per il disco e lo spazio per la posta.