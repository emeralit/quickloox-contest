---
title: "Il sito più facile"
date: 2007-10-30
draft: false
tags: ["ping"]
---

Come segnala <a href="http://www.sinapsistudio.it/index.html" target="_blank">Giulio</a>, <cite>per una persona alle prime armi <a href="http://xtralean.com/SBOverview.html" target="_blank">Shutterbug</a> non è per niente male</cite>.

E in effetti la sua facilità d'uso è impressionante.

Dritta preziosa. Grazie. :-)