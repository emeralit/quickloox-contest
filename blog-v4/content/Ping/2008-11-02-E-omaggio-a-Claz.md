---
title: "E omaggio a Claz"
date: 2008-11-02
draft: false
tags: ["ping"]
---

Apprendo giusto ora che è scomparso <strong>Claudio Zamagni</strong>.

Tecnico impareggiabile, umanità superiore, senso dello <em>humour</em> e capacità di vedere le cose importanti della vita.

&#8220;Operava&#8221; a Torino ma per un consistente periodo di tempo venne in Mac@Work a fare assistenza al sabato mattina. Fui molto più presente del solito e uno dei motivi era la possibilità di fare colazione insieme al bar o scambiare qualche battuta, sempre splendidamente caustica e inarrivabile per profondità di giudizio, durante la mattinata.

Nessuno come lui sapeva e poteva ridare vita a un Mac più vecchio degli altri o a un apparecchio ufficialmente privo di pezzi di ricambio. Forse proprio per questo è stato chiamato ancora giovane e all'improvviso, come a volergli impedire di escogitare chissà quale miracolo dell'ultimo momento con il suo cacciavite.

Mancheranno troppe cose di lui per farne un elenco sensato. Gli sia lieve la terra.