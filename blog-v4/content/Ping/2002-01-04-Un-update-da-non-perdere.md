---
title: "Un update da non perdere"
date: 2002-01-04
draft: false
tags: ["ping"]
---

Mac OS X 10.1.2 riporta l’infrarosso sui Titanium ma risolve anche un sacco di altre questioncelle

Nel momento in cui scrivo è appena arrivata una nota tecnica di Apple sull’aggiornamento di Mac OS X 10.1.2.
Si sapeva già che le porte a infrarossi di alcuni Titanium sarebbero tornate a funzionare, ma consiglio ugualmente la lettura della nota, per scoprire per esempio che i modem interni potrebbero funzionare meglio di prima e, soprattutto, che è arrivato AppleScript 1.8.1. Dentro AppleScript, sempre per esempio, c’è un nuovo comandino che permette di eseguire un comando Unix senza dover avere aperta una finestra del Terminale.
I miei amici americani direbbero “Cool!”. Lo dico anch’io e raccomando: non importa se non hai voglia di leggere la nota. Non perderti l’aggiornamento.

lux@mac.com

<http://docs.info.apple.com/article.html?artnum=106634>
