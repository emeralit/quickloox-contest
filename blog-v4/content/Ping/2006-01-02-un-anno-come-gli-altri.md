---
title: "Un anno come gli altri"
date: 2006-01-02
draft: false
tags: ["ping"]
---

La ripetizione degli stessi gesti scaramantici dà sicurezza

Ero un po' preoccupato. Poi finalmente ho letto qualcuno predire che Apple presenterà al Macworld Expo un cellulare. Il mondo non sta per finire.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>