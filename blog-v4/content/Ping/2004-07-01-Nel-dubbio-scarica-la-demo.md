---
title: "Nel dubbio, la demo"
date: 2004-07-01
draft: false
tags: ["ping"]
---

Troppo spesso il software costa troppo, ma serve veramente comprarlo?

Ieri ero in chat con E. che stava cercando una copia di Flash MX per provare una cosa. E. lavora nella grafica ma tocca Flash solo di striscio e, nel suo caso, la spesa per comprare il programma originale non è affatto giustificata. A lui Flash capita sì e no due volte l’anno.

Che cosa ha fatto allora? È ricorso al peer-to-peer per rubare una copia? No. È andato sul sito di Macromedia e si è scaricato la demo, perfettamente funzionale, durata trenta giorni. L’ha installata e ha risolto il suo problema, nel rispetto del suo lavoro, di quello altrui e anche del proprio conto in banca.

Praticamente tutte le applicazioni costose si comportano in questo modo. E molte applicazioni costose hanno anche alternative a costo ridotto, più che adatte, se non addirittura a costo zero. Gimp, per esempio, fa l’80% di quello che fa Photoshop ed è gratis.

È sciocco comprare software che non serve. Ma anche comportarsi disonestamente.

<link>Lucio Bragagnolo</link>lux@mac.com