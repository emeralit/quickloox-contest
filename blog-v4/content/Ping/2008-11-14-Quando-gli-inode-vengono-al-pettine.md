---
title: "Quando gli inode vengono al pettine"
date: 2008-11-14
draft: false
tags: ["ping"]
---

Per vedere quanto spazio libero c'è su disco niente batte il Finder quanto a velocità.

Però il comando <code>df</code> del Terminale rilascia qualche informazione in più.

Con <code>du</code> (e il parametro <code>-s</code>, che produce lo spazio occupato dal totale, invece che quello di ogni singolo elemento) si possono fare cose simpatiche, tipo scoprire quanto spazio occupano le immagini Jpeg presenti sul disco, dovunque si trovino (e qui il Finder ha poco da dire).

I parametri <code>-h</code> e <code>-H</code> contano in gigabyte oppure in gibibyte, cosa che aiuta a chiarirsi le idee sull'occupazione effettiva del disco.

Utilissimo, infine, <code>df -i</code>, che mostra la situazione degli <em>inode</em>. In Unix ogni disco rigido ha a disposizione un numero finito di <em>inode</em>, che molto all'ingrosso corrispondono ai file e alle cartelle presenti su disco.

Un disco quasi pieno ha il conto degli <em>inode</em> quasi esaurito e va bene. Se invece il disco ha spazio libero consistente ma si trova a corto di <em>inode</em>, è un indizio forte che qualcosa non va.