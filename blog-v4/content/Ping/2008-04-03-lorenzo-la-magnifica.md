---
title: "Lorenzo, la Magnifica"
date: 2008-04-03
draft: false
tags: ["ping"]
---

Scrive <a href="http://www.lorenzopantieri.net" target="_blank">Lorenzo</a>:

<cite>Caro Lucio,</cite>
<cite>ho appena pubblicato sul Web una nuova <a href="http://www.lorenzopantieri.net/LaTeX_files/ArteLaTeX.pdf" target="_blank">guida a LaTeX</a>, rivolta sia ai principianti di LaTeX sia a coloro che già lo conoscono. La guida presenta le nozioni fondamentali del programma (operando una sintesi di concetti sparsi in svariati manuali), fornisce una vasta gamma di esempi e analizza alcuni problemi tipici incontrati durante la stesura di una pubblicazione accademica o professionale.</cite>

<cite>Il Prof. Enrico Gregorio, massima <em>auctoritas</em> italiana del mondo LaTeX, ha voluto farmi l'onore di scriverne la prefazione e di esprimere squisite parole di apprezzamento nei confronti del lavoro.</cite>

<cite>La guida è stata scritta su Mac, e analizza, fra l'altro, l'installazione e l'utilizzo di LaTeX nelle diverse piattaforme (Win, Mac e Linux): non ci crederai, ma su Mac è sempre tutto un pochino più semplice... ;-)</cite>

Il Pdf di Lorenzo è una magnifica guida, lunghetta e dettagliata. Il mio consiglio primario è di sfogliarla. Fare attenzione all'impaginazione, a come è scritto LaTeX, all'uso di margini riquadri intestazioni eccetera, al trattamento delle immagini.

Dopo averla sfogliata (bastano tre minuti), pensare a quanto ci vorrebbe per avere lo stesso risultato con Word (o anche con OpenOffice.org o Pages, se è per quello).

Se resta una pulce nell'orecchio, o se si prende sonno con leggero ritardo, dal giorno dopo è il caso di iniziare a studiarla, la guida. Si scoprirà quale sia il sistema di scrittura più potente al mondo.