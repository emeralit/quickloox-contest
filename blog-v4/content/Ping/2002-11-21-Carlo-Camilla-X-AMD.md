---
title: "Carlo, Camilla, Mac OS X e AMD"
date: 2002-11-21
draft: false
tags: ["ping"]
---

Sull’informatica il gossip da riviste popolari proprio non funziona

Hai presente il principe Carlo e Camilla Parker-Bowles? A leggere certi giornali, un mese sì e un mese no sono sul punto di sposarsi, vivere insieme, trasferirsi (lei) a Palazzo, varie ed eventuali.

A intervalli altrettanto regolari qualcuno scrive che Apple potrebbe essere in procinto di abbandonare i processori Motorola per passare a Intel o (versione aggiornata) AMD.

È una sciocchezza sesquipedale e non accadrà per almeno dieci ragioni che qualsiasi persona minimamente intelligente e a conoscenza delle cose di Apple dovrebbe essere in grado di capire in pochi istanti.

Ne cito solo una: tutti i programmi Macintosh esistenti oggi cesserebbero di funzionare dalla sera alla mattina e andrebbero riscritti da zero, oppure funzionare in un emulatore che sarebbe penosamente lento e inaffidabile.

Vuoi che tutti i programmi per Macintosh che hai in casa smettano di funzionare dalla sera alla mattina? Penso di no. Beh, neanche Apple lo vuole, e non lo farà.

Al contrario del principe Carlo, che – lui sì – un giorno o l’altro farà come scrivono i giornali.

<link>Lucio Bragagnolo</link>lux@mac.com

