---
title: "Un (grosso) libro l'anno"
date: 2009-02-08
draft: false
tags: ["ping"]
---

L'insieme dei Ping del 2008 pesa all'incirca mezzo megabyte. Praticamente un (grosso) libro.

Voglio mandare un (grosso) ringraziamento a chi passa da queste parti e mi consente di scrivere un libro l'anno quasi senza sforzo e pure divertendomi.