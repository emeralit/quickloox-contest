---
title: "Magnetismo anomalo"
date: 2006-06-15
draft: false
tags: ["ping"]
---

<strong>Fearandil</strong> segnala che il MacBook, trasportato nella borsa senza una custodia, gli si è aperto accidentalmente.

Per quanto sento è un episodio isolato. Ma è il caso di starci attenti e, come lui stesso commentava, sperare che diventino disponibili al più presto buone custodie come quelle esistenti a legioni per i modelli meno recenti.