---
title: "Ci si vede"
date: 2008-05-08
draft: false
tags: ["ping"]
---

Sempre di più. Per quella piattaforma sfigata e senza programmi che è Mac, adesso per fare videoconferenza con gente Windows c'è anche <a href="http://www.oovoo.com/" target="_blank">OoVoo</a>. Gratis.