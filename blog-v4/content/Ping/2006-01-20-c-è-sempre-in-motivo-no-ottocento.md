---
title: "C’è sempre un motivo. No, ottocento"
date: 2006-01-20
draft: false
tags: [ping]
---

Da quello che ho capito finora, Apple non ha deciso di escludere FireWire 800 dalle nuove macchine. Semplicemente, il chipset Intel attuale non la supporta.

Il che non significa niente rispetto al futuro, perché un chipset più avanzato potrebbe benissimo supportare tutto e così via. Teoricamente lo standard FireWire può spingersi fino a 3.200 megabit per secondo ed è tutt’altro che alla frutta. Un po’ come le nuove macchine Apple, appena uscite e sempre più difficili da prendere seriamente in considerazione come paradigmi di quello che ci dobbiamo attendere.
