---
title: "La regola del giorno"
date: 2006-01-29
draft: false
tags: [ping]
---
Ho dato la consueta passata giornaliera alle mailing list che frequento e, dopo la regola del minuto (prima di chiedere, osserva attentamente per un minuto quello che appare sullo schermo), vorrei introdurre la regola del giorno:

è ragionevole avere al massimo un problema al giorno.

Vedo gente che ogni giorno riesce a fare anche sette domande diverse e questo è segno che il problema è sempre uno, ma molto probabilmente esterno al Mac.