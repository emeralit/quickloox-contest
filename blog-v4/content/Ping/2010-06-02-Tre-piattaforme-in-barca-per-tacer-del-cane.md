---
title: "Tre piattaforme in barca (per tacer del cane)"
date: 2010-06-02
draft: false
tags: ["ping"]
---

Il cane sono io, alle prese con la gestione di un parco informatico informale a tre vertici, Mac-iPad-iPhone.

La prima cosa che ho scoperto: iPad è un apparecchio totalmente diverso da iPhone. Nel senso che la prima schermata di iPad sta diventando radicalmente diversa da quella di iPhone, e per forza, dirà qualcuno, visto che uno è un computer da tasca e l'altro una tavoletta.

Per questo ho impostato inizialmente iPad su iTunes come apparecchio a parte e non come specchio di iPhone. Hanno identità diverse.

Il rovescio della medaglia è che, se la stessa <i>app</i> deve contenere gli stessi dati sia su iPhone che su iPad, bisogna inserirli due volte (il che non accadrebbe se uno fosse lo specchio dell'altro).

Secondo la modalità di utilizzo e le <i>app</i> che si usano, potrebbe convenire maggiormente l'uno o l'altro approccio. Magari cambierò idea oppure la configurazione che ho scelto è la migliore per me, ma è un po' presto per dirlo.

Chi non ha ancora comprato Goodreader ha risparmiato 0,79 centesimi di euro. Uno spreco che grida vendetta: è un lettore di documenti formidabile, che eccelle di qui (iPad) e di là (iPhone).