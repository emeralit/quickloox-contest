---
title: "Chi è causa del suo mal"
date: 2003-02-15
draft: false
tags: ["ping"]
---

A farli crescere male poi si abituano peggio

Il Ping! sul tipo che chiedeva tutto e subito, a cui non risponderò mai niente ((( Claudio: link al Ping ajw “Scortesia, simmetria” se possibile, grazie :-))) ha suscitato reazioni varie comprese quelle di bravissime persone disposte ad aiutare il tipo in questione, e che lo hanno poi aiutato.

È che va in due modi: puoi rispondergli di salvare in Html con AppleWorks. Ma domani ti chiederà perché l’interlinea tra le righe è di 0,746 millimetri quando lui la voleva esattamente a 0,747, senza avere capito niente di Html.

Oppure gli si dirà di infilarsi dentro GoLive o DreamWeaver o altro programma visuale, dove da perfetto incompetente finirà per perdersi e alla prima occasione non capirà perché la sua perfetta impaginazione non funziona (in genere uno così spende una settimana per creare un rollover stupendo e poi non sa che va caricato sul server insieme alla pagina). Diventerà un seccatore e resterà incapace.

Io preferisco che rimanga nel suo brodo, perché è l’unica maniera di sperare che si decida a imparare veramente: se rimane a piedi, forse si decide a studiare l’Html di una pagina qualsiasi trovata sul Web, oppure frequenta per quattro minuti un tutorial su Html.it. Insomma, c’è rischio che una scintilla casuale accenda il cervello.

<link>Lucio Bragagnolo</link>lux@mac.com