---
title: "Ricerca Terminale"
date: 2005-05-13
draft: false
tags: ["ping"]
---

Quando una tecnologia arriva in forma grafica, deve esistere anche nella shell

Mac OS X galleggia su una base Unix ed è naturale che quanto appartiene all'interfaccia grafica abbia un contraltare nel Terminale.

Per esempio, i comandi <code>mdls</code> e <code>mdfind</code> corrispondono ai classici comandi Unix <code>ls</code> e <code>find</code>, ma riguardano non tanto i dati, quanto i metadati (md), proprio quelli che interessano a Spotlight.

Qualche esperimento aiuterà a capire come lavora Spotlight e a capire perché sta già cambiando per molti (me compreso) il modo di utilizzare Mac.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>