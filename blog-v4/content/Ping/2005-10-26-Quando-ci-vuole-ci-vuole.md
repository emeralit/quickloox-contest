---
title: "Quando ci vuole ci vuole"
date: 2005-10-26
draft: false
tags: ["ping"]
---

Qui sotto compare una parolaccia, ma c'è un perché

Sono a Cologno Monzese, sede Apple, presentazione di Aperture. Dopo tante chiacchiere sulla teoria, parte la dimostrazione pratica.

Il dimostratore parte mostrando come Aperture riconosce le sequenze contenute nella scheda di memoria proveniente dalla fotocamera, e le organizza, premettendo un'importazione selettiva automatica di gruppi di foto. Mostra come si possa importare da più schede di memoria contemporaneamente, e intanto continuare a lavorare sul materiale già presente.

Mostra la rapidità di generazione di varianti dell'immagine master e l'intangibilità del master stesso, che non può essere sovrascritto, neanche volendo. Sottolinea il peso ridottissimo delle varianti, che non sono copie modificate dell'originale ma frutto di un'elaborazione in tempo reale eseguita sull'originale, e quindi consistono in piccolissimi file contenenti le istruzioni che generano la differenza di aspetto tra un'immagine e un'altra.

Mostra le funzioni di lavoro possibili senza interfaccia utente visibile, che sorprendono, e un sacco di altre cose.

A un fotogiornalista discretamente noto presente alla dimostrazione scappa, a mezza voce ma ampiamente discernibile nella saletta, un nitido &ldquo;cazzo…&rdquo;.

Che Aperture sia un programma interessante per professionisti della fotografia?

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>