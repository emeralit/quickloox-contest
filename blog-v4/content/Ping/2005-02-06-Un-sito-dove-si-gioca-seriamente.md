---
title: "Un sito dove si gioca seriamente"
date: 2005-02-06
draft: false
tags: ["ping"]
---

Sta diventando rapidamente il punto di riferimento italiano

Ho rilasciato un'intervista a <a href="http://www.devilsgames.it">Devilsgames</a>, novello sito italiano dedicato ai giochi per Mac.

Non che sia importante la mia intervista; lo è il loro sito. Sono ragazzi ma ci mettono impegno vero e sono bravi. Secondo me stanno diventando rapidamente il punto di riferimento italiano per i giochi su Mac, così come <a href="http://www.insidemacgames.com">Inside Mac Games</a> è quello mondiale.

Vanno seguiti e, per chi sia interessato a <a href="http://www.worldofwarcraft.com">World of Warcraft</a>, seguiti con molta attenzione. Meritano.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>