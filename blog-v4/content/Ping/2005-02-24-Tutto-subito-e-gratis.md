---
title: "Tutto, subito e gratis"
date: 2005-02-24
draft: false
tags: ["ping"]
---

Una Cpu non riesce ancora a sostituirci, ma non perché sia troppo stupida

Leggo su una mailing list qualcosa del genere:

<em>Devo fare un sito amatoriale ma non ne capisco niente. Cercavo un programma capace di fare tutto al posto mio, possibilmente gratis.</em>

Giuro che ho fatto qualche modifica, ma lo spirito è esattamente questo.

Secondo me il datore di lavoro di quest'uomo sta pensando <em>chissà se c'è un programma che fa gratis le stesse cose che sta facendo xxx</em>…

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>