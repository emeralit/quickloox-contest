---
title: "Leggende dure a morire"
date: 2007-07-10
draft: false
tags: ["ping"]
---

Riporto un intervento di <strong>Roberto</strong> sulla <em>mailing list</em> di <a href="http://www.accomazzi.it" target="_blank">Akko</a>:

<cite>Su <a href="http://www.musimac.it/" target="_blank">Musimac.it</a> abbiamo preparato quattro <em>mix</em> identici, ma realizzati su quattro Daw diverse, Cubase4, Logic, Nuendo e ProTools, e li abbiamo inviati a centinaia di iscritti, che si sono offerti in veste di &#8220;orecchie d'oro&#8221;.</cite>

<cite>Gli audiofile erano sincronizzati tra di loro in modo da poter passare istantaneamente da uno all'altro.</cite>

<em>Cubase 4 escluso (aveva il motore audio più vecchio), nessuno è stato in grado di attribuire un mix alla corrispondente piattaforma, né di riconoscere le differenze sonore.</em>

Intanto è un buon pretesto per sottolineare l'autorevolezza di Musimac per chi lavora con la musica digitale.

E poi dovrebbe fare capire che, oltre quello che il nostro orecchio riconosce, per definizione non c'è niente, e occuparsi di tutto quello che dovrebbe teoricamente andare oltre il riconoscimento del nostro orecchio è una perdita di tempo.

Quando vai a casa di quello con gli altoparlanti esoterici, sostituisci la sua compilation preparata con i codec di ultimo grido e metti al suo posto una cosa fatta tranquillamente con iTunes: non se ne accorgerà.

La <a href="http://www.musimac.it/it/un-test-percettivo-sui-software-piu-diffusi-di-hd-recording" target="_blank">pagina del test</a> non sembra accessibile senza password, ma la linko lo stesso, sai mai.