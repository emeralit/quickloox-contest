---
title: "Titoli di (fuori di) testa"
date: 2003-07-18
draft: false
tags: ["ping"]
---

E poi uno si lamenta che l’informazione non è più quella di un tempo

Su un sito americano una mentecatta scrive che secondo lei Apple dovrebbe mettersi a vendere macchine G3 a prezzo stracciato e, senti questa, farlo di nascosto, senza marchio Apple oppure con un altro marchio qualunque, equipaggiando le macchine con un ipotetico OS X Lite. Mentecatta, ma le opinioni - anche quelle disastrosamente lontane da questo pianeta - vanno rispettate.

Un sito italiano legge la rispettabile opinione della mentecatta e ne fa una notizia, dal titolo che spiego con un esempio.

Supponiamo che mia suocera dica “secondo me, che lavoro alla Fiat, Umberto Agnelli dovrebbe sposare Megan Gale e mettere un cellulare su tutte le Fiat”. Va beh, è mia suocera.

Supponiamo che io pubblichi su un sito il titolo “Umberto Agnelli sposa Megan Gale?”, come se ci fosse il dubbio che potrebbe accadere davvero.

Mia suocera ok, ma io, che cosa sono? Un giornalista? Io userei un altro epiteto.

<link>Lucio Bragagnolo</link>lux@mac.com
