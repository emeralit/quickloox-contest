---
title: "Luna di miele"
date: 2005-05-01
draft: false
tags: ["ping"]
---

Lavorare con Tiger è veramente tutt'altra cosa

Confesso che Tiger per me non è una novità assoluta; ho avuto accesso alla tecnologia qualche settimana prima. Ma ho montato la cosa su un disco esterno, per non correre rischi, e ho comunque lavorato per la maggior parte del tempo su Panther.

Adesso che Mac OS X 10.4 è il mio sistema di lavoro, sono in piena luna di miele. Le prestazioni sono ottime, la stabilità impeccabile, ogni volta che si apre qualcosa c'è una sorpresa.

Anche la compatibilità Java, su cui potevano esserci dubbi, è ottima, anche se bizzarra. Ho riscontrato finora solo due anomalie: <a href="http://pcgen.sf.net">PcGen</a> (software per giocatori di ruolo) non funziona più come bundle Mac ma non perde un colpo se caricato come programma Java; analogamente, l'editor di suono <a href="http://audacity.sf.net">Audacity</a> dà un errore di Core Audio alla partenza ma poi lavora alla perfezione.

World of Warcraft poi funziona ottimamente. Sono in luna di miele anche con lui, ma è un'altra storia. Nel dubbio: il mio nome è Ithilgalad.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>