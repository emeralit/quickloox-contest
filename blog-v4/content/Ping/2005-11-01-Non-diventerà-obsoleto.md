---
title: "Non c'è rischio"
date: 2005-11-01
draft: false
tags: ["ping"]
---

Ecco perché non è prematuro spiegare che esistono gli Universal Binary

Mi è già stato chiesto un paio di volte.

<cite>Ma non corro il rischio di comprare un Mac oggi e ritrovarmelo presto obsoleto a causa della transizione ai processori Intel?</cite>

No.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>