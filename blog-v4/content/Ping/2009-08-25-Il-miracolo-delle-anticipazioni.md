---
title: "Il miracolo delle anticipazioni"
date: 2009-08-25
draft: false
tags: ["ping"]
---

L'amico <b>Guass</b> sostiene che un certo sito <cite>aveva anticipato la notizia dando la data corretta di uscita della nuova versione di Mac OS X</cite> per il 28 agosto.

Il sito in questione è stato molto più bravo. Ha scritto:

- <cite>una data di rilascio anticipata rispetto alle previsioni: il primo trimestre del 2009 invece di giugno o luglio.</cite>
- <cite>Snow Leopard in arrivo entro marzo 2009</cite>
- <cite>Snow Leopard, torna l'ipotesi di un lancio a giugno</cite>
- <cite>gli update di Mac Os e Windows arriveranno insieme: a giugno del prossimo anno.</cite>
- <cite>la presentazione dell'erede di Vista è per metà luglio, in tempo per un testa a testa con Mac Os Snow Leopard.</cite>
- <cite>Mac Os 10.6 arriverebbe nei negozi, probabilmente, nella seconda metà del mese di agosto.</cite>
- <cite>Snow Leopard il 28 agosto, Apple Store Italia conferma e poi smentisce</cite>
- <cite>Ancora voci su Snow Leopard: rilascio solo a settembre</cite>
- <cite>Snow Leopard sta rispettando la tabella di marcia per la commercializzazione annunciata da Apple per il mese di settembre.</cite>
- <cite>Snow Leopard potrebbe arrivare a fine estate o addirittura in autunno</cite>

Il sito in questione, dicevo, è stato ben più bravo: ha infatti miracolosamente moltiplicato le anticipazioni fino a coprire tutto il calendario possibile e centrare, come si vede, con estrema esattezza il momento preciso. Invece che domandarsi come abbiano fatto a individuare la data, c'è da chiedersi come avrebbero potuto mai sbagliare, a fronte di questa chirurgica precisione.

Più che bravi, Prescelti: non tutti hanno il dono della profezia.

Anche se non capisco perché scrivere <cite>Apple Store off line, arriva Snow Leopard?</cite>, con il punto interrogativo. Forse hanno voluto fare finta di non avere certezze, cinque minuti prima dell’annuncio ufficiale.

Oltre che profetici, pure signorili e rispettosi dell’altrui ignoranza. Grazie per non averci umiliato ulteriormente.