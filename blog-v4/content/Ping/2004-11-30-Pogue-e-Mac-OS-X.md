---
title: "(Ricerche minerarie e) perdite di tempo"
date: 2004-11-30
draft: false
tags: ["ping"]
---

I segreti di uno scrittore brillante e assai prolifico

Istruttivo, questo <a href="http://www.nytimes.com/2004/11/23/technology/23pogues-posts.html?adxnnl=1&oref=login&adxnnlx=1101820982-K8l660LgVvMTfrb0m2Py1Q">articolo</a> del New York Times. Chiedono a David Pogue, l’autore della serie <em>Missing Manuals</em> (Il manuale che non c’è, in Italia), come faccia a scrivere tutto quello scrive dato il tempo a disposizione.

Pogue offre una serie di risposte, una delle quali è (in traduzione fedele e rispettosa) questa:

<cite>So che questo darà fastidio a tutti gli antiApple, ma svolgo la maggior parte del mio lavoro in Mac OS X. Quindi spendo tempo uguale a zero su virus, spyware, manutenzione e tutte quelle altre perdite di tempo tipiche dei computer.</cite>

Non basta, come spiega l’articolo stesso. Ma, parafrasando una pubblicità, aiuta.

Le ricerche minerarie non c’entrano niente; solo un lontano ricordo di  gioventù fumettistica. :-)

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>