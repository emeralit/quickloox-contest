---
title: "Chi scava trova"
date: 2006-02-10
draft: false
tags: ["ping"]
---

Svolgo un lavoro in collaborazione con una società di veri <em>geek</em>. Gente che non sa che cosa sia il Terminale perch&eacute; ci lavora dentro da mattina a sera e non lo considera un programma come gli altri, ma la normalità. Ci sono file da rivedere e mi dicono di modificarli passando da <strong>subversion</strong>, l'ultimo grido dei sistemi di <em>versioning</em> open source. Un sistema di versioning permette di lavorare in tanti sullo stesso file pur lavorando ciascuno su una copia locale dello stesso.

Mi viene il tremore. Oddio, ora ci sarà una <em>tarball</em> di sorgenti Unix da compilare sa il cielo come… e sarà solo l'inizio.

&Egrave; stato l'inizio. Di qualcosa che non avrei immaginato.

Loro stessi mi passano il link a una <a href="http://metissian.com/projects/macosx/subversion/" target="_blank">immagine disco di subversion</a> pronta da installare su Mac OS X. Semplicissimo.

Mi insegnano i primi comandi, ovviamente da Terminale, e sopravvivo. Con quattro o cinque comandi si può già fare l'essenziale.

Poi scopro che O'Reilly ha pubblicato online, gratis, un <a href="http://svnbook.red-bean.com/" target="_blank">manuale d'uso di subversion</a>.

Un amico, che tocca anche a lui, mi chiede se non ci sia un'interfaccia grafica per evitare il Terminale. Trovo <a href="http://esvn.umputun.com/" target="_blank">eSvn</a>, <a href="http://www.einhugur.com/iSvn/index.html" target="_blank">iSvn</a>, <a href="http://www.smartsvn.com/smartsvn/download.html" target="_blank">SmartSvn</a>, <a href="http://subcommander.tigris.org/" target="_blank">subcommander</a> e <a href="http://www.einhugur.com/iSvn/index.html" target="_blank">SvnX</a>.

Infine, <a href="http://roderickmann.org/log/archives/2005/07/textwrangler_as.html" target="_blank">come aprire un file da Terminale direttamente in BBEdit o TextWrangler</a> in modo da poter effettuare le modifiche dentro il miglior editor possibile senza saltare al Finder.

Pensavo a una cosa poco amichevole e invece ci si può lavorare con facilità e in un ambiente veramente Mac.

Una lezione. Qualche volta, basta cercare.