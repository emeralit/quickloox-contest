---
title: "La programmazione è musica"
date: 2008-01-25
draft: false
tags: ["ping"]
---

Sul testo del grande <a href="http://www.erix.it" target="_blank">Enrico Colombini</a> hanno messo finalmente <a href="http://www.derelitti.com/ballata_del_programmatore/" target="_blank">voce e chitarra</a>.

Adesso toccherà scrivere quella del giornalista&#8230;