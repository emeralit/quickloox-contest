---
title: "300+ novità: font"
date: 2007-10-28
draft: false
tags: ["ping"]
---

Con Leopard è cambiato qualcosa anche in Font Book, a partire dal fatto che è presente un comando serio per stampare testo di prova, utile per annteprime di stampa o per confronti tra caratteri.

I font sono organizzati anche tenendo conto della lingua di chi usa il sistema.

Leopard si cautela nei confronti di quelli che, per risparmiare dieci mega su 160 giga di spazio, si mettono a cancellare i font di sistema e, quanto meno, avvisa che non sarebbe il caso.

I font inclusi nel sistema aumentano, a comprendere - tra l'altro - un Arial Unicode e un Papyrus Condensed.

I font non attivi, quando ne ha bisogno un'applicazione, vengono attivati automaticamente e restano attivi fino a che non viene spenta l'applicazione.

Il mio amico Piero sarà contento del supporto del Braille da parte di VoiceOver e della presenza dei nuovi font Apple Braille Regular, Apple Braille Outline e Apple Braille Pinpoint.

Non mi risulta che Apple abbia ancora pensato a tradurre la <a href="http://www.apple.com/it/macosx/features/300.html" target="_blank">pagina di riepilogo</a> delle novità evidenti di Leopard, per cui proseguo.