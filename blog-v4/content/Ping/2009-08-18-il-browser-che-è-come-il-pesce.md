---
title: "Il browser che è come il pesce"
date: 2009-08-18
draft: false
tags: ["ping"]
---

Apple ha da poco estratto la nuova versione del suo browser, Safari 4. Come <a href="http://www.apple.com/safari/download/" target="_blank">chiunque può vedere</a>, Safari 4 funziona su Leopard, su Tiger e ovviamente su Snow Leopard.La stragrande maggioranza degli utenti attivi di Mac OS X usa Safari 4 e, se proprio va malissimo, Safari 3. Safari 2 è ai minimi termini.

Microsoft, leggo, ha un problema con Internet Explorer 6. È il browser meno standard del mondo, è lentissimo e in rete si raccolgono <a href="http://www.ie6nomore.com/" target="_blank">le richieste per abbandonarlo</a>. Dopotutto esistono Explorer 7 ed Explorer 8. È come se Apple fosse ancora a supportare Safari 2.

Tuttavia, scrive Microsoft, fino a che non cessa il supporto per Windows Xp <a href="http://www.betanews.com/article/Microsoft-may-not-kill-IE6-until-at-least-2014/1250003327" target="_blank">non può cessare il supporto per Internet Explorer 6</a>. Più o meno fino al 2014.

Da quello che capisco, per avere Internet Explorer 7 su Xp basta installarlo, ma il 27 percento degli utenti si rifiuta ostinatamente di farlo, cifra impossibile da ignorare per gli sviluppatori web. Che fanno i salti mortali per cercare compatibilità anche con Explorer 6 oppure raggiungono quella e per incompetenza ignorano il resto. A questo si aggiunga che una parte consistente di utenza Windows si rifiuta di andare oltre Xp per manifesta inadeguatezza di Vista.

Non so chi ci marci, se Microsoft o gli utenti o gli sviluppatori web o chissà chi. So che una quintalata di problemi, lavoro extra e incompatibilità su web deriva da questa situazione e che questa situazione si protrarrà fino al 2014.

Se non dovessimo trascinarci i pesi morti, andremmo più avanti e più velocemente. Qualcuno fa gentilmente sapere a chi si trastulla con Explorer 6 che installare la versione 7 è gratuito e che sono graditi sul web un po' come il pesce, e che invece di tre giorni sono passati anni?