---
title: "Happy iPod Days"
date: 2006-06-21
draft: false
tags: ["ping"]
---

<strong>Ale</strong> segnala una tornata di novità sul suo sito di custodie <em>fashion</em> per iPod. Ho visto <a href="http://www.ivogue.it/page1/page5/files/page5-1001-full.html" target="_blank">Fonzie</a> e sono scoppiato a ridere ma, scherzi a parte, sono cose davvero carine e diverse dall'ovvio.