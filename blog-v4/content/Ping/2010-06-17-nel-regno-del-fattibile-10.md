---
title: "Nel regno del fattibile / 10"
date: 2010-06-17
draft: false
tags: ["ping"]
---

<cite>pregasi elencare azioni fattibili su un iPad e non fattibili su un Sony Vaio W da 10&#8221;</cite>.

Copia (quasi) dal vero.

Alla mia signora servivano immagini di animali cui ispirarsi. Trovate su Google Images, foglio appoggiato parzialmente su iPad e copia da iPad al foglio, come se invece della tavoletta ci fosse una stampa.

Per verifica ho chiesto se non fosse la stessa cosa disporre della stessa immagine sul suo PowerBook 12&#8221; (che tra l'altro ha identica risoluzione). <cite>Molto più scomodo</cite>, è stata la risposta.