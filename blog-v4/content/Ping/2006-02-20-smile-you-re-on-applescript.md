---
title: "Smile, you're on AppleScript"
date: 2006-02-20
draft: false
tags: ["ping"]
---

<a href="http://www.satimage-software.com" target="_blank">Satimage</a> è alla ricerca di buoni programmatori AppleScript.

La società alimenta da tempo la crescita di Smile, un editor per sviluppare AppleScript che è diversi chilometri avanti a Script Editor. Sul loro sito si trova appunto Smile, che è gratis, e diverso software di livello professionale per scienza e matematica.

Gli interessati alla programmazione possono scrivere a satimage@mac.com per descrivere le loro capacità e sapere di più su tempi e modi. Io, interessato più che altro a buon software, ne approfitto per vedere se ho uno Smile aggiornato.