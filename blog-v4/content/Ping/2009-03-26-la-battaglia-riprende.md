---
title: "La battaglia riprende"
date: 2009-03-26
draft: false
tags: ["ping"]
---

È arrivato <a href="http://wesnoth.org" target="_blank">Battle for Wesnoth 1.6</a>. Contrariamente alle tipiche nuove versioni di software <em>open source</em>, questa migliora la precedente di moltissime lunghezze.

Da provare, da riprovare, da scoprire, da riscoprire.