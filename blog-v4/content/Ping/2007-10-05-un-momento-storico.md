---
title: "Un momento storico"
date: 2007-10-05
draft: false
tags: ["ping"]
---

Di fronte alla notizia che Amazon ha aperto il suo negozio di Mp3 senza Drm, il coro della stampa ha detto la cosa più ovvia: <cite>finalmente qualcuno farà una seria concorrenza a iTunes Store</cite>.

Pochissimi hanno colto il fatto veramente fondamentale. Apple, Amazon, le case discografiche, niente più Drm&#8230; e Microsoft?

Microsoft è stata tagliata fuori. Da tutto.

Microsoft ha sostenuto il Drm più restrittivo di tutti e ha fatto una bandiera della lotta alla pirateria musicale. Steve Ballmer aveva dichiarato che iPod era uno strumento vergognoso, usato praticamente solo per piratare musica.

E adesso? La musica si vende con il Drm di Apple, oppure senza Drm. Il Drm di Microsoft non conta più niente per nessuno.

Se si venderà musica tramite iTunes Store, ne guadagnerà Apple. Se si venderà musica sul negozio di Amazon, ne guadagnerà Apple. Perché la musica di Amazon la possono usare tutti i lettori e, nella classifica dei dieci lettori Mp3 più venduti su Amazon, nove sono iPod.

La tattica usuale di Microsoft è imporre il proprio formato e poi strangolare la concorrenza. Sulla musica digitale, non potrà mai più farlo. Il formato di Microsoft non conta più niente e il suo Drm non ha più importanza per nessuno.

La tattica usuale di Microsoft è l'<em>embrace and extend</em>, adotta lo standard e &#8220;miglioralo&#8221; in modo da averne il controllo e che gli altri siano alla tua mercé. Ma Microsoft non può adottare il Drm di Apple e ben difficilmente può alterare Mp3.

Nell'hardware, Microsoft ha sempre ricattato i costruttori, minacciando di tagliarli fuori dalle novità di Windows nel caso prendessero troppa confidenza con Linux o avessero qualcosa da dire sugli accordi di licenza. Ma non può farlo con iPod. Non può ricattare i negozianti (iPod si stravende negli Apple Store). Microsoft non può adottare le sue tattiche tipiche per fermare iPod.

Nella musica digitale, Microsoft ha perso. Definitivamente, senza possibilità di recupero. È l'unico settore in cui finora sia accaduto. È un momento storico. E tutto quello che la stampa sa dire è che finalmente iTunes Store ha concorrenza. Come se ad Apple importasse qualcosa, dal momento che il 97 percento della musica sugli iPod <em>non</em> proviene da iTunes Store.