---
title: "Testa nelle nuvole"
date: 2009-05-19
draft: false
tags: ["ping"]
---

Apprendo con piacere che nella localizzazione italiana di <a href="http://icloud.com/it/" target="_self">iCloud</a> ha messo testa e lavoro anche il Filippo <em>Phil</em> Cervellera, ospite sempre gradito di <em>Ping!</em>.

Molti complimenti e i migliori auguri a iCloud.

Spostare su Internet tutto, compreso il sistema operativo, è una operazione di <em>cloud computing</em> (elaborazione a nuvola, intesa come luogo simbolico in cui stanno i dati) ambiziosa e molto interessante. Adesso si può fare anche in italiano, per quanto solo su Firefox e in versione molto provvisoria.