---
title: "Punto Niente"
date: 2010-03-19
draft: false
tags: ["ping"]
---

Mi è scappato via, ma lo scorso 15 marzo è scattato il venticinquesimo anniversario del dominio .com, che corrisponde alla registrazione di Symbolics.com.

Symbolics&#8230; corrisponde al nome. Azienda di altri tempi, che costruiva un computer apposta per eseguire programmi scritti in linguaggio Lisp. Un bel simbolo dell'informatica del 1985. Che neanche esiste più; il 27 agosto 2009 il dominio è stato venduto a Xf.com, signori nessuno che commerciano in nomi di dominio.

Nel marzo 1985 Macintosh aveva già più di un anno.

Esiste un elenco dei <a href="http://dnsknowledge.com/news/25-years-of-the-com/" target="_blank">primi cento domini .com della storia</a>. Apple c'è, al numero 64. Ci sono Xerox, Hp, Ibm, Sun, At&#38;T, Nsc, Boeing, Siemens, Nec, Adobe, Amd, 3Com, Unisys, Philips, Ncr, Cisco.

Si direbbe che qualsiasi azienda impegnata nell'informatica e con un minimo di lungimiranza sia corsa a registrare il proprio dominio .com appena possibile. Tranne una.