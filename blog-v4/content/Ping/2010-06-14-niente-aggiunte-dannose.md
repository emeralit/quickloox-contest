---
title: "Niente aggiunte dannose"
date: 2010-06-14
draft: false
tags: ["ping"]
---

Qualsiasi cuoco sa bene che non è un alto numero di ingredienti a fare un piatto saporito, ma il numero giusto, con alta qualità, assemblati con precisione, genio, fantasia e conoscenza delle regole della cucina.

Per questo la <a href="http://www.apple.com/html5/" target="_blank">pagina dimostrativa di Apple sulle possibilità di Html5</a> è un esempio.

Non tanto per i campioni di Html5 mostrati, ma per la frase <cite>gli standard non sono un'aggiunta al web. Sono il web</cite>.