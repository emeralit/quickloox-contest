---
title: "A ognuno il suo mestiere"
date: 2005-09-05
draft: false
tags: ["ping"]
---

Ma apparentemente alcune professioni non sono tali

Non farò il nome di questa persona.

Quando cambia macchina, compra Quattroruote.<br>
Si interessa di storia e legge il mensile Medioevo.<br>
Ha un figlio appassionato di osservazioni celesti, che legge Astronomia.<br>
È sposato. La moglie legge Casaviva per trovare nuove idee di arredamento.<br>
Si svaga con la fantascienza e occasionalmente divora un romanzo Urania.<br>
Tutti i lunedì compra la Gazzetta dello Sport per leggere della sua squadra del cuore.<br>
La sua rivista preferita di cinema si chiama Ciak.<br>
Si interessa saltuariamente di arte moderna e possiede qualche numero di Flash Art.<br>
Per programmare le vacanze si guarda Tuttoturismo.

Ha un Mac e un iPod. È curioso di notizie riguardanti Apple.

Le guarda sul sito di un quotidiano politico.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>