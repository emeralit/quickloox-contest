---
title: "Dentro nei guai"
date: 2008-09-05
draft: false
tags: ["ping"]
---

<a href="http://applejack.sourceforge.net" target="_blank">AppleJack</a> è straordinario per chiunque non sia un mago del Mac e si ritrovi in guai seri. Tutto quello che c'è da ricordare, ora, è che per un <em>boot</em> in Single User Mode si tiene premuto Comando-S all'avvio.

Poi si digita <code>applejack</code> e appare una interfaccia testuale, ma umana.

<code>applejack auto</code> effettua automaticamente tutte le operazioni previste dal menu e <code>applejack auto restart</code> ci aggiunge anche il riavvio automatico al termine della procedura. <code>applejack AUTO</code> (notare le maiuscole) esegue un deep cleaning e spazza via proprio tutte le <em>cache</em>, compreso il database dei Launch Services. Da usare solo ed esclusivamente come <em>ultima ratio</em>.

AppleJack va usato solo come risorsa estrema e solo in Single User Mode. In tutte le altre situazioni causerà solo problemi, anche gravi. I posseduti dal demone della &#8220;pulizia&#8221; sono avvisati.

Di solito bisogna essere cauti nell'installare software. In questo caso solo un incauto non lo farà.