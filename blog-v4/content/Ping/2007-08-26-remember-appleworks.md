---
title: "Remember AppleWorks"
date: 2007-08-26
draft: false
tags: ["ping"]
---

Non metterò un link ad AppleWorks. ora portano tutti a iWork '08.

Ma non mi sogno di cancellarlo dal disco rigido e mi tengo stretto il Cd di installazione. L'ultimo aggiornamento è già da tempo nei backup.

Aggiungo: servisse, sul sito Apple si possono ancora raggiungere gli <a href="http://www.apple.com/support/appleworks/" target="_blank">aggiornamenti</a> di AppleWorks.