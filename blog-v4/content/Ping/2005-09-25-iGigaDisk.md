---
title: "iGigaDisk"
date: 2005-09-25
draft: false
tags: ["ping"]
---

Se c'era bisogno di una ragione in più, eccola qua

La presente per informare chi non lo avesse già saputo che ora iDisk, il disco virtuale su Internet accessibile tramite il servizio di Apple .Mac, ha aumentato la sua capacità a un gigabyte.

Il giga si può scomporre a piacere in spazio disco e spazio per la posta. Per esempio, siccome ritiro la posta ogni pochi minuti mi è sufficiente poco spazio di quel tipo e allora farò, per dire, 50 mega per la posta e 950 per i file, le pagine Web, i backup e il resto. Qualcun altro, con esigenze differenti, potrebbe fare l'opposto e così via.

Inoltre Backup è arrivato alla versione 3.0 e ha compiuto notevoli progressi. Sta diventando davvero l'alternativa giusta per il backup non strettamente professionale.

La presente per informare che rinnoverò con gran piacere il mio abbonamento a .Mac.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>