---
title: "Explorer nella polvere"
date: 2003-07-31
draft: false
tags: ["ping"]
---

Safari fa saltare fuori gli scheletri nell’armadio di Microsoft

Microsoft ha annunciato la cessazione dello sviluppo di Explorer per Mac OS X (che comunque era fermo da anni). La motivazione: il fatto che Safari è integrato in Mac OS X e che quindi nessun programma esterno può validamente competere con lui.

Scheletro uno: come ha riconosciuto la stessa Microsoft, sono le stesse motivazioni che, tra le altre cose, hanno portato al processo antitrust contro Gates e compagnia. Solo che, in tribunale e fuori, Microsoft ha sempre negato che l’integrazione di Explorer in Windows fosse un vantaggio competitivo.

Scheletro due: Safari non è per niente integrato nel sistema operativo. Non ha un installer e non tocca alcuna parte del sistema, né è uscito un aggiornamento di Mac OS X apposta per lui. Come lo copi sul disco, lo togli dal disco ed è lo stesso. Microsoft, su questo, mente.

Scheletro tre: Apple ha annunciato che WebCore, il nucleo di Safari, potrebbe entrare a fare parte di Mac OS X e quindi integrare certe funzioni del browser nel sistema operativo. Ma WebCore è open source; quindi significa solo che <i>tutti</i> i programmi potranno approfittare di quelle funzioni lì. L’esatto contrario di quello che succede in Windows con Explorer. Microsoft, su questo, ha agito contro il mercato.

Quanta polvere, appena si solleva il tappeto di casa Microsoft.

<link>Lucio Bragagnolo</link>lux@mac.com