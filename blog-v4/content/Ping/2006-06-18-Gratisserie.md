---
title: "Gratisserie"
date: 2006-06-18
draft: false
tags: ["ping"]
---

L'operazione marketing di <a href="http://www.houdah.com/houdahspot/" target="_blank">HoudahSpot</a> è riuscita e MacZot <a href="http://maczot.com/blogzot/index.php" target="_blank">offre gratis il programma</a> ai primi cinquemila che si registrano.

Se il link non porta più al form giusto, significa che l'offerta è terminata ed è inutile cercare in giro. Di mio sto andando alla massima velocità possibile.