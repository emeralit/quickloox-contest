---
title: "Chi troppo e chi niente"
date: 2010-10-25
draft: false
tags: ["ping"]
---

<a href="http://e.nikkei.com/e/fr/tnks/Nni20101021D21JFN06.htm" target="_blank">Si sussurra</a> che Sharp abbia rinunciato al <i>business</i> dei personal computer, per l'azienda non più tale, per dedicarsi alle <a href="http://www.gizmag.com/sharp-galapagos-ebook-tablet-reader/16523/" target="_blank">tavolette Galapagos</a> previste in uscita da dicembre in avanti.

I pregressi del mercato mostrano che annunciare tavolette non ha niente a che vedere con la loro disponibilità effettiva, ma il punto voleva essere un altro.

In quanto si sussurra pure che ci siano dubbi sulla sopravvivenza di Symbian, specie dopo che l'amministratore delegato Lee Williams <a href="http://www.geek.com/articles/mobile/symbian-ceo-quits-for-personal-reasons-20101020/" target="_blank">ha deciso il 20 ottobre scorso di andarsene</a> per <cite>ragioni personali</cite>.

Ora, Sharp era inesistente &#8211; lato personal &#8211; da più di un anno. Symbian invece è presente, a memoria, sul 41 percento dei cellulari nel mondo, qualcosa tipo trecento milioni di apparecchi.

Se un <i>player</i> marginale e un sistema iperaffermato si trovano in situazioni simili, forse tutto questo costante parlare di quote di mercato trascura i motivi reali del successo o dell'insuccesso delle piattaforme.