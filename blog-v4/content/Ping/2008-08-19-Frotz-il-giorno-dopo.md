---
title: "Frotz, il giorno dopo"
date: 2008-08-19
draft: false
tags: ["ping"]
---

La mia copia di Frotz per iPod touch oggi non si apriva più e non sono neanche riuscito a ripristinarla da iTunes. Ho dovuto scaricarne una nuova da iTunes Store (che si ricorda dello scaricamento già effettuato e comunque permette di riscaricare, cosa che non credo avvenga con la musica).

Diagnosi: il sistema di aggiornamento mmanuale che ho usato per installare i tre primi capitoli di Zork altera la firma digitale dell'applicazione (avevo qualche sospetto, adesso ne ho di più). Non si deve.

Invece, come ha già commentato <a href="http://www.accomazzi.it" target="_blank">Akko</a>, la via giusta è passare dal browser interno del programma. Bisogna ricordarsi di toccare l'icona-lente che sta nella fascia inferiore dello schermo e scrivere l'indirizzo nel campo che appare, e non usare il campo già presente nella finestra del browser (come sbagliavo a fare). Quest'ultimo funziona solo dentro il database di Ifdb e non per siti esterni.

Sempre come ha commentato Akko, c'è già <a href="http://www.accomazzi.it/frotz" target="_blank">una pagina</a> che permette di scaricare correttamente i primi tre Zork.

Dovesse uno entrare in possesso di altri file Z-code di avventure Infocom, ho già descritto ieri - e resta valido - come e dove rintracciare i file suddetti. E se uno non ha un link da cui fare scaricare i file a Frotz, si crea una paginetta <em>ad hoc</em>.