---
title: "Segnalazione e valutazione"
date: 2009-07-09
draft: false
tags: ["ping"]
---

Si è fatto un gran parlare in questi giorni della falla di iPhone che consentirebbe attacchi pericolosi <a href="http://www.macworld.com/article/141507/2009/07/smsvulnerability_iphone.html" target="_blank">mediante l'invio di Sms</a>.

Ogni rischio di sicurezza, oltre che andare segnalato, va anche valutato. Qui siamo nella situazione in cui lo stesso scopritore della falla ammette di non avere un esempio di attacco funzionante e un'evidente difficoltà di mettere in piedi una quantità adeguata di istruzionei di programmazione a colpi di messsaggini.

La segnalazione era doverosa. Il rischio, evidente, esiste e Apple lo eliminerà. La valutazione è che, nella pratica, si tratta di un problema del tutto improbabile se non inesistente.

Adesso leggo sul Washington Post di una <a href="http://voices.washingtonpost.com/securityfix/2009/07/an_odyssey_of_fraud_part_ii.html?hpid=sec-tech" target="_blank">truffa informatica</a> che ha fruttato 415 mila dollari ai danni di poveracci con un conto in banca e un computer Windows.

I dettagli sono nell'articolo, qui riassumo brevemente: i criminali mettono in piedi una finta agenzia di correzione bozze via Internet. A chi accetta il lavoro viene proposto di collaborare anche a un'altra struttura e cos&#236; mettono venticinque complici involontari. Nel frattempo, l'ennesimo <i>trojan</i>, appena modificato, ruba le coordinate bancarie alle vittime e apre il computer Windows al collegamento remoto da parte dei criminali.

I criminali entrano nel computer Windows delle vittime e, con la connessione di queste ultime, fanno un po' di <i>home banking</i> e inviano bonifici ai complici involontari. I quali sanno che il loro compito è effettuare un money transfer verso un conto bancario ucraino e tenersi pure la commissione. I complici involontari sono sprovveduti e non conoscono la vera origine del denaro. Qualcuno ha anche avuto problemi non banali, tipo: ricevo 9.700 dollari, ne giro 9.200 in Ucraina, ne tengo 500 per me&#8230; e il giorno dopo, su richiesta del cliente truffato, le banche annullano il bonifico e dal mio conto spariscono 9.700 dollari.

Tutto questo è già accaduto (dal 22 giugno), i 415 mila dollari sono veri, c'è un giudice che se ne occupa, si sa qual è la banca e qual è il <i>trojan</i> usato, l'Fbi è al lavoro. Niente leggende urbane, niente possibilità ipotetiche, niente può darsi, niente forse, niente magari.

Il fatto è stato abbondantemente segnalato. La valutazione la lascio a te. :-)