---
title: "Panther, welcome to"
date: 2003-10-09
draft: false
tags: ["ping"]
---

L’attesa è praticamente finita e converrà non aspettare

Ho avuto il privilegio di provare Mac OS X 10.3 nella sua Developer Preview presentata al convegno mondiale degli sviluppatori Apple, tenutosi a San Francisco a maggio, e quelle poche cose che c’erano, rispetto a quanto si troverà nella versione finale, mi sono piaciute subito, a partire da Exposé per andare a un sacco di cose meno evidenti ma rilevanti.

Rilevanti al punto che, come era già successo per Mac OS X 10.2, la nuova versione del sistema è più veloce di quella precedente! Quindi l’installazione è conveniente anche su una macchina vecchia, che anziché appesantirsi si alleggerirà. Anche se è chiaro che i guadagni maggiori si vedranno sui formidabili G5 (una meraviglia di architettura informatica, avanti almeno tre anni su qualsiasi altra cosa) e che senza una adeguata iniezione di Ram non c’è aggiornamento che tenga e tutto scorre, direbbe Eraclito, ma più lentamente.

Una curiosità: se valgono le stesse regole sui marchi che valevano per Mac OS X 10.2, in Europa bisogna dire (a rigore) Mac OS X 10.3 e non Panther. Benvenuto lo stesso.

<link>Lucio Bragagnolo</link>lux@mac.com