---
title: "Sistemi a confronto"
date: 2002-06-18
draft: false
tags: ["ping"]
---

Ma è vero che si può avere un Mac che costa meno acquistando un Pc?

“Ecco; ho comprato un Power Mac Tal dei tali a tremila euro quando basta andare da Computer Discount e prendere un Wintel con processore a due gigahertz per quattro soldi”.
Come tutte le frasi fatte anche questa è un po’ troppo semplicistca per essere presa sul serio. A prescindere dal fatto che comunque i Mac sono belli, sono più facili da usare e nessuno che guida una Audi comprerebbe mai una Fiat che gli assomiglia giusto perché costa meno.
In ogni caso, prima di fare affermazioni affrettate, è bene documentarsi. Il posto migliore per farlo è <link>AAPLtalk</link>http://www.aapltalk.com/shootouts/, che mette a confronto una quantità impressionante di configurazioni tra Mac e Pc.
Siccome sono intelligenti, non mettono benchmark; confrontare la velocità di sistemi diversi non è mai veramente affidabile e chiunque faccia affermazioni assolute tipo “Questo è più veloce di quello” sottintendendo “sempre e comunque” parla solo per dare aria alla lingua.

<link>Lucio Bragagnolo</link>lux@mac.com