---
title: "Delirio iPod"
date: 2004-08-05
draft: false
tags: ["ping"]
---

Perché questa rubrica sarà fuori controllo per qualche tempo

Apple mi ha appena concesso un iPod in visione per un mese, con motivazioni che non è il caso di appofondire ora. Di fatto, per i prossimi trenta-quaranta giorni il bilanciamento degli argomenti che tratto sarà a forte rischio. Mi sono innamorato di questo oggetto semplicemente iniziando ad aprire la scatola, guardando la scritta Enjoy che appare durante l’apertura, osservando il dettaglio di mettere le quattro istruzioni di base direttamente sull’adesivo di protezione dello schermo e altre finezze. Sono felice e soddisfatto di avere un iPod, sia pure in prestito, ancora prima di averlo messo sotto carica.

<link>Lucio Bragagnolo</link>lux@mac.com