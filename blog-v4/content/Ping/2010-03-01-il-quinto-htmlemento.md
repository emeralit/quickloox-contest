---
title: "Il quinto Htmlemento"
date: 2010-03-01
draft: false
tags: ["ping"]
---

La battaglia tra Flash e Html5 ha un risvolto tecnico che cerco di approfondire e diviene sempre più complicato.

Tale Morgan Adams, <a href="http://adamsi.com/" target="_blank">sviluppatore Flash di mestiere</a>, ha scritto che gli attuali siti contenenti Flash <a href="http://www.roughlydrafted.com/2010/02/20/an-adobe-flash-developer-on-why-the-ipad-cant-use-flash/" target="_blank">non possono funzionare bene</a> sugli apparecchi con interfaccia touch per via di <i>hover</i> e <i>mouseover</i>. Sono termini tecnici che descrivono il comportamento del mouse quando si ferma sopra un elemento di interfaccia, senza cliccare. L'argomento è che ovviamente quando usiamo il mouse lo portiamo perennemente su qualcosa prima di cliccare. I siti Flash usano questo meccanismo, ovviamente, che non è replicabile se il clic è il tocco; ovviamente lo schermo non ha modo di sapere che abbiamo un dito vicino allo schermo, fino a che non lo tocchiamo. Ma toccare è già cliccare.

<b>Fearandil</b> e <b>Filippo</b> mi hanno segnalato la questione e cos&#236; ho iniziato ad approfondire.

Sembra che in realtà l'obiezione non sia pertinente e che Flash sia programmabile in modo da funzionare correttamente su schermi touch. Il problema pare essere altro: prima di tutto il codice Flash installato sui siti di mezzo pianeta non ha idea di interfacce diverse dal mouse e quindi non è programmato per essere usato a ditate. Secondo, la difficoltà più vasta e investe, per dire, anche il codice Html in certi casi.

Faccio un esempio. Ho una pagina Html che contiene un rettangolo con dentro un gioco Flash, che ha i propri comandi grafici interni. Lo carico ipoteticamente sul mio smartphone touch e, se fa come iPhone, vedo la pagina tutta intera, con dentro il rettangolo Flash. Se tocco la pagina in corrispondenza del rettangolino, sto comandando la pagina (per esempio per scorrerla) oppure sto comunicando con l'interfaccia interna del giochino?

Flash su certi telefoni funziona e qualcuno si è inventato soluzioni. Per esempio, lo sviluppatore Adobe Mike Chambers ha ricordato la tecnica del <a href="http://www.mikechambers.com/blog/2010/02/23/scrolling-html-with-flash-content-on-touch-devices/" target="_blank">doppio tocco</a>. Se tocco due volte Flash, questo va a pieno schermo e allora parlo con l'interfaccia interna. Altrimenti sto parlando con la pagina Html. Sul Nexus One di Google funziona cos&#236;. Sull'N900 Nokia però non funziona e un doppio tocco zooma la pagina Html, non il solo codice Flash.

Continuo a sfogliare pareri di programmatori e tecnici senza venire a una risposta certa. Mi par di capire che Flash può essere pienamente programmato per comportarsi bene su uno schermo touch e che però ci siano differenze tra l'interfaccia a mouse e l'interfaccia a tocco che richiederanno sempre a comunque programmazione ad hoc, che il 99 percento del Flash già esistente non ha e non avrà.

Le ragioni per fare a meno di Flash rimangono comunque altre: inefficienza del codice, rischi di sicurezza, sovraccarico del processore. E qui non ci sono questioni.

In premio a chi ha sopportato questa digressione senza capo né coda, un paio di esempi Hmtl5: <a href="http://www.fofronline.com/experiments/cube/multiCubes.html" target="_blank">Cubi tridimensionali con testo su ogni faccia</a> e <a href="http://web.me.com/pzich/3DImageFly/" target="_blank">Immagini in volo</a>.