---
title: "Sconto quindici"
date: 2005-05-26
draft: false
tags: ["ping"]
---

Mariner Software festeggia i tre lustri di attività

E sconta del 25 percento tutti i suoi prodotti fino a fine maggio.

Ossia c'è poco tempo. Ma, con quello sconto, prodotti come Write o Calc sono quasi regalati.

Una volta ci si lamentava che non c'erano alternative a Office. Ora ci si lamenta che come alternativa c'è solo OpenOffice (e derivati). Beh, non è vero.

Se <a href="http://www.marinersoftware.com">Mariner Software</a> è in vita da quindici anni, vuol dire che qualcosa di buono lo hanno fatto.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>