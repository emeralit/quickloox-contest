---
title: "Quale sarà il prossimo gioco tormentone"
date: 2006-08-18
draft: false
tags: ["ping"]
---

Se è un immersivo 3D di massa, non ci sono dubbi: o è <a href="http://www.wow-europe.com" target="_blank">World of Warcraft</a> o è una seconda scelta.

Se è uno strategico veloce e coinvolgente, non ci sono dubbi: <a href="http://www.travian.it" target="_blank">Travian</a> si è creato un seguito di appassionati a diciotto carati.

E il prossimo? Meno esigente di WoW, meno impegnativo di Travian per presenza in rete, ma assai profondo in termini di strategia e complessità? Potrebbe essere forse <a href="http://empserver.sourceforge.net/default.htm" target="_blank">Empire</a>?