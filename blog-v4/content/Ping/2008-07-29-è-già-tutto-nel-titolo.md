---
title: "È già tutto nel titolo"
date: 2008-07-28
draft: false
tags: ["ping"]
---

Grazie all'amico <strong>Fabio</strong>, mi sono ricordato che le finestre del Finder possono visualizzare l'intero percorso della cartella anziché il solo nome di quest'ultima. Si tratta di scrivere nel Terminale

<code>defaults write com.apple.finder _FXShowPosixPathInTitle -bool YES</code>

E dopo rilanciare il Finder.

Per tornare alla situazione originale,

<code>defaults write com.apple.finder _FXShowPosixPathInTitle -bool NO</code>

E dopo, ancora, rilanciare il Finder.

Dopotutto molte cose si giudicano dal loro titolo.