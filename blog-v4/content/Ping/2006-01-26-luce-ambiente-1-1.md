---
title: "Luce-ambiente 1-1"
date: 2006-01-26
draft: false
tags: ["ping"]
---

Sarò presuntuoso, ma l'adeguamento automatico della luminosità dello schermo dei PowerBook alle variazioni dell'illuminazione ambientale dovrebbe funzionare in modo leggermente diverso.
Se ho capito bene, mantiene costante il rapporto tra le due illuminazioni. E va bene sui valori medi: se la luce ambiente aumenta o diminuisce leggermente, l'illuminazione dello schermo la segue e sta bene.
Sui valori estremi, però, ha meno senso. Se la variazione è notevole (tipo accendere la luce in una stanza buia) lo schermo ha uno sbalzo ugualmente notevole, decisamente superiore all'adeguamento effettivo richiesto dalla vista.
Però faccio poco testo, dal momento che tendo a lavorare con la luminosità al minimo. Visto che parliamo di sensibilità personale, beh, parliamone.
