---
title: "Riduzionistico od olistico?"
date: 2002-01-10
draft: false
tags: ["ping"]
---

Il nuovo iMac, anche se nessuno se ne accorge, è alfiere di una rivoluzione filosofica che fa maturare il mercato

Il mestiere dei riduzionisti (filosoficamente parlando) è considerare le cose esattamente per le parti che le compongono.
Poi ci sono gli olisti. La loro missione è considerare ogni cosa dal punto di vista del suo tutto e considerare quest’ultimo maggiore della somma delle sue parti.
Finora il mercato dei computer è stato dominato dal riduzionismo: tanti megahertz, tanta Ram, tanta cache, tanti tasti sulla tastiera.
Invece i mercati maturi tendono all’olismo: a nessuno interessa il raggio di curvatura dei rebbi della forchetta, o lo spessore del volante dell’auto, bensì una forchetta funzionale ed elegante, o un’auto piacevola da guidare.
Se dobbiamo ringraziare di una cosa Steve Jobs e Jonathan Ive (il designer capo di Apple), è esattamente di presentare macchine come il nuovo iMac Lcd: potenti, eleganti e assai olistiche.

lux@mac.com