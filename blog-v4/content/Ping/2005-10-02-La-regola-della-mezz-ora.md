---
title: "La regola della mezz'ora"
date: 2005-10-02
draft: false
tags: ["ping"]
---

Gli dai un dito e ti prendono il braccio. Con soddisfazione generale

È fresco di stampa (virtuale) il Ping sulla Regola del Minuto (prima di chiedere aiuto, concentrati per un minuto sullo schermo) è già mi scrive <strong>Mario</strong>:

<cite>Qualche giorno fa ho scritto alla lista <a href="http://www.accomazzi.it/TistituzionaleI6.html">Misterakko</a> per chiedere lumi sul backup con Automator, di cui aveva scritto sul libro allegato all'ultimo Macworld. Non ho ricevuto risposte. Dopo un paio di giorni, un po' seccato, a dire la verità, ho pensato che potevo farcela da solo. Ho aperto Automator e ho spippolato una mezz'ora (un po' più di un minuto, quindi). Adesso con due clic parte un'applicazione che crea una cartella sulla scrivania e la chiama backup, sceglie le cartelle che voglio copiare, le copia nella cartella, fa uno zip del tutto, masterizza lo zip su un Cd al quale applica anche la data del backup, poi prende cartella e zip, che non mi servono più, e le schiaffa nel Cestino.</cite>

<cite>Se mi fossi seccato prima non avrei obbligato 900 persone a leggere un messaggio inutile.</cite>

<cite>Però ho imparato qualcosa. :-)</cite>

Neanche Automator è cosa per tutti e mezz'ora è ben più di un minuto. Ma vuoi mettere il gusto di realizzare qualcosa di tuo, con mezz'ora (mezz'ora, non sei settimane) di applicazione?

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>