---
title: "Convenienza e verità"
date: 2009-03-04
draft: false
tags: ["ping"]
---

Qualche mese fa, in argomento Blu-ray, Steve Jobs aveva dichiarato che ottenere la licenza era <em>un incubo</em>. Fioccarono le critiche, a dire che se Apple vuole davvero mettere un lettore Blu-ray dentro i Mac non si ferma certo davanti a qualche intoppo burocratico e che il problema doveva essere tutt'altro.

Adesso leggo una notizia di Macworld.com intitolata <a href="http://www.macworld.com/article/139055/2009/02/bluray.html?lsrc=rss_main" target="_blank">Le licenze Blu-ray diventano più semplici ed economiche</a>.

Vi si dice che per avere la licenza Blu-ray è necessario chiederla a ciascun possessore di brevetto in materia. Panasonic, Philips a Sony sono detentrici di brevetti Blu-ray e hanno deciso di unire le forze, insieme ad altri detentori, per creare un'azienda indipendente a cui sarà possibile chiedere una sola licenza valida per tutti invece che fare il giro delle sette chiese.

Come effetto collaterale ci si aspetta inoltre che il costo della licenza scenda di molto, anche del 40 percento.

Forse è vero che Apple ha altri motivi per non mettere Blu-ray dentro i Mac. Tuttavia mi sembra anche vero che Jobs, nel suo tipico esagerare, abbia comunque messo il dito su una piaga che evidentemente esiste e che ora si vuole risanare.

Mi aspetterei adesso da tutti i critici non tanto un ripensamento, ma almeno un centimetro di onesta marcia indietro.