---
title: "Nemo propheta in patria"
date: 2002-06-05
draft: false
tags: ["ping"]
---

Anche a casa Dell c’è chi preferisce i Macintosh

Secondo l’<link>Austin American-Statesman</link>http://www.austin360.com/statesman/editions/sunday/metro_state_6.html, quotidiano dell’omonima capitale texana, alla Texas University gli studenti di alcuni corsi del prossimo autunno dovranno obbligatoriamente possedere un iBook o PowerBook per poter essere ammessi. Il bello è che Austin è la città che ospita il quartier generale di Dell Computer, acerrima avversaria di Apple, e che proprio ad Austin il giovane Michael Dell iniziò a vendere computer.
Proprio vero: nessuno è profeta in patria. O forse persino là ne hanno abbastanza di ciofeche “compatibili”, nel senso di “da compatire”. Non importa se fatte, letteralmente, in casa.

<link>Lucio Bragagnolo</link>lux@mac.com