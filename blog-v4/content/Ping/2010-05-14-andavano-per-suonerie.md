---
title: "Andavano per suonerie"
date: 2010-05-14
draft: false
tags: ["ping"]
---

<a href="http://uk.ibtimes.com/articles/20091230/google-phone-nexus-one-has-iphone-killer-tech-specs.htm" target="_blank">Nick Brown su IbTimes del 30 dicembre 2009</a>:

<cite>Il telefono Nexus One di Google, il cui lancio è uno dei più attesi del 2010, vanta specifiche tecniche che fanno sembrare iPhone una femminuccia. [&#8230;] Wow! Gli altri</cite> smartphone <cite>e specialmente iPhone faranno bene a guardarsi da Nexus One perché ha l'aria di una bestia cattiva e affamata. E noi tutti odiamo gli schizzi di sangue.</cite>

In aprile Verizon Wireless &#8211; paragonabile alla nostra Tim &#8211; <a href="http://www.businessinsider.com/googles-big-move-to-disrupt-mobile-carriers-hits-a-big-snag-2010-4" target="_blank">ha rinunciato</a> a Nexus One.

In maggio Sprint Nextel &#8211; paragonabile alla nostra Wind &#8211; <a href="http://online.wsj.com/article/SB10001424052748703880304575236742923422022.html" target="_blank">ha fatto lo stesso.</a>

I giudizi dati in base alle specifiche tecniche, una volta di più, non valgono niente.