---
title: "Il modello mancante"
date: 2003-03-04
draft: false
tags: ["ping"]
---

I conti di Apple vanno benino ma potrebbero andare bene, se solo…

In attesa di vedere i risultati dell’ultimissimo trimestre fiscale di Apple, ricordo che l’ultimo si era concluso con una perdita di otto milioni di dollari, che non sono da sottovalutare ma, su un miliardo e mezzo di dollari di fatturato, sono di fatto quisquilie. Tanto più che senza alcune spese straordinarie Apple avrebbe chiuso di qualche milione in attivo.

A vedere i conti, in questo momento Apple ha un solo problema: le vendite dei Power Mac G4 desktop sono inferiori alle attese (mentre le altre macchine vendono da bene a più che bene).

Ho fatto un po’ di indagine stile salumiere: ho chiesto agli amici. Uno fa il grafico e mi ha detto che vuole Quark XPress per Mac OS X. Un altro fa il musicista e mi ha detto che ci sono problemi di software e di hardware. Uno mi ha detto che non si fida del doppio processore e preferirebbe un processore solo con un clock altissimo (sbaglia, ma la pensa così).

Apparentemente di motivazioni ce ne sono; vediamo se Apple riuscirà a eliminarle rapidamente e con efficacia. Se arrivasse il modello mancante, che il mercato tornarà ad apprezzare, la salute finanziaria di Apple potrebbe essere davvero rosea.

<link>Lucio Bragagnolo</link>lux@mac.com