---
title: "Mela Christmas"
date: 2005-12-25
draft: false
tags: ["ping"]
---

Auguri sì, ma anche un compito

Se hai un Mac, sei doppiamente fortunato. Non solo puoi permetterti un computer, che oggi è la chiave della crescita personale da molti punti di vista; puoi anche permetterti un Mac, e non ne ne sto facendo una questione di prezzo.

Almeno a Natale ricordati dei più sfortunati. Oggi lascia stare il computer e dedicati ad amici, parenti, colleghi, persone malate o sfortunate, o chi vuoi tu. Domani, o dopodomani, usa il Mac per fare qualcosa di buono per gli altri.

Tanti auguri!

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>