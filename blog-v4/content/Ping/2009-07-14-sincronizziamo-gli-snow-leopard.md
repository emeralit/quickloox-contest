---
title: "Sincronizziamo gli Snow Leopard"
date: 2009-07-14
draft: false
tags: ["ping"]
---

Non cessa la scoperta delle novità di Snow Leopard. Una novità al giorno, facendo il lavoro al posto di Apple, che lo ha <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">fatto a metà</a>.

<b>Più opzioni di sincronizzazione</b>

Ora tutti, non solo gli utilizzatori di iPhone, possono sincronizzare i propri contatti tramite MobileMe, Yahoo e Google.

Ne verranno solo guai. La sincronizzazione è lo sport preferito degli italiani. Solo che ciascun italiano vuole farla in un proprio modo specifico, come la formazione della Nazionale e il caffè. Ora la possibilità di complicarsi inutilmente la vita viene estesa a tutti e per giunta triplicata.