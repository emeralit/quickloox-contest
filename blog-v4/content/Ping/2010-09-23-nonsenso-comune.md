---
title: "Nonsenso comune"
date: 2010-09-23
draft: false
tags: ["ping"]
---

Mi hanno sempre detto e ho sempre raccontato che, quando su uno stesso iPhone/iPod touch/iPad coesistono più account iTunes, si aggiornano solo le <em>app</em> scaricate dall’account attivo in quel momento.

Non è affatto vero o almeno non lo è più. Ho distrattamente aggiornato le <em>app</em> su iPhone e mi sono state chieste le password degli account presenti sull’apparecchio, prima una poi l’altra, senza bisogno di altre manovre. Gran comodità e prova ulteriore che le cose bisogna leggerle e poi, anche, toccarle con dito.