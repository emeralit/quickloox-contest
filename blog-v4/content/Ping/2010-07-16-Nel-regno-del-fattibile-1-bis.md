---
title: "Nel regno del fattibile / 1 bis"
date: 2010-07-16
draft: false
tags: ["ping"]
---

<cite>Pregasi elencare azioni fattibili su un iPad e non fattibili su un Sony Vaio W da 10&#8221;</cite>.

Avevo <a href="http://www.macworld.it/ping/hard/2010/05/13/nel-regno-del-fattibile/" target="_blank">iniziato questa serie</a> con il <a href="http://www.youtube.com/watch?v=HvplGbCBaLA" target="_blank">bis del pianista Lang Lang a San Francisco</a>, effettuato su iPad con &#8211; si suppone &#8211; Magic Piano.

Ora possiamo aggiungere il tastierista del cantante pop inglese Squeeze, che durante un'esibizione nel serale americano <a href="http://www.latenightwithjimmyfallon.com/" target="_blank">Late Night with Jimmy Fallon</a> <a href="http://www.youtube.com/watch?v=BwJf_0xuMFw&amp;feature=player_embedded" target="_blank">esegue una parte del brano su iPad</a>, quasi per certo usando Pianist. Si vede piuttosto bene attorno al minuto 2:32.