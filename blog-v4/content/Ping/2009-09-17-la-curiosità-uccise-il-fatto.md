---
title: "La curiosità uccise il fatto"
date: 2009-09-17
draft: false
tags: ["ping"]
---

Curioso <a href="http://blogs.computerworld.com/14713/the_mystery_of_the_ipod_touch_camera" target="_blank">post su Computerworld</a>.

<cite>Di tutte le sorprese dell'evento Apple, la più grossa è stata probabilmente che Apple non ha presentato la fotocamera su iPod touch.</cite>

L'autore spiega perché: <cite>la fotocamera su iPod touch era la previsione numero uno di tutti.</cite>

<cite>Erano filtrate foto di prototipi.</cite>

<cite>C'era perfino il video di un prototipo.</cite>

<cite>Sono arrivate varie notizie da Francia e Cina, tutte concordanti.</cite>

<cite>Alcuni fabbricanti di iPod hanno persino realizzato custodie con il foro per la fotocamera.</cite>

Sarebbe stato più semplice scrivere <cite>ho creduto a cose di cui non sapevo niente e le ho ripetute senza sapere che erano finte</cite>. Ma c'è ancora cos&#236; tanta gente che crede nel mito dei rumor&#8230;

Guarda caso, la videocamera su iPod nano non l'aveva anticipata nessuno. Ed eccola l&#236;.

Se non basta a spiegare che i siti di <i>rumor</i> inventano, che cosa serve ancora?