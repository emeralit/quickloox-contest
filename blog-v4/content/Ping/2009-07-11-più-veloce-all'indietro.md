---
title: "Più veloce all'indietro"
date: 2009-07-11
draft: false
tags: ["ping"]
---

La novità di Snow Leopard di oggi riguarda, in un certo senso, il passato. Sempre presa dalla <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">pagina</a> che Apple non ha voglia di tradurre in italiano.

<b>Backup Time Machine più veloce</b>

Il backup iniziale di Time Machine è fino al 50 percento più veloce.

La mia esperienza è certo di maggiore velocità, sul backup iniziale e anche sui successivi. Non ho misurato nulla, però. Snow Leopard è in generale più veloce di Leopard e magari parte di ciò che percepisco di Time Machine è un effetto collaterale della migliore velocità complessiva.