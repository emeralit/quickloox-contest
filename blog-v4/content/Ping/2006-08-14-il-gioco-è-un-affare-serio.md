---
title: "Il gioco è un affare serio"
date: 2006-08-14
draft: false
tags: ["ping"]
---

Se qualcuno ricorda ancora quanti, all'annuncio di Boot Camp, presagirono sventura per il mondo dei giochi su Mac, può fargli presente la comparsa di <a href="http://www.transgaming.com/index.php?module=ContentExpress&amp;file=index&amp;func=display&amp;ceid=24" target="_blank">Cider</a>. Se mantiene le promesse, portare i giochi Windows su Mac - dove ci fosse qualcuno abbastanza stupido da da non sviluppare multipiattaforma in prima battuta - diventerà cosa molto veloce, quasi quanto ci ha messo <strong>Alan</strong> ad accorgersi della notizia e segnalarmela.