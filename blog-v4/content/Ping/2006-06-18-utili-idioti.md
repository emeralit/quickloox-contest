---
title: "Utili idioti"
date: 2006-06-18
draft: false
tags: ["ping"]
---

Adobe, a torto o a ragione, ha imposto una consuetudine (e chiamiamola standard quanto si vuole, ma non lo è) mondiale per i documenti. Un Pdf lo leggono tutti, lo scrivono tutti, si vede su tutto, ha specifiche tecniche ragionevoli, è funzionale, è efficiente almeno quanto basta.

Poi c'è Microsoft che deve avere, per disposizione interna, il monopolio su tutto. Quindi che fa? Fa la sua versione di Pdf, la chiama Xps e la mette in Windows Vista. Chiaramente non è compatibile con Pdf.

Si ripete il fimm già visto con QuickTime (Apple) e con Java (Sun): la tecnologia migliore viene messa a repentaglio dalla volontà monopolista di un'azienda dal comportamento scorretto (come sancito in tribunale).

Nel caso di Office, sono milioni i complici che accettano passivamente la dittatura. Vedremo quante teste si chineranno, sempre pronte a obbedire a Microsoft, nel caso di Pdf.