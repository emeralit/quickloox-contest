---
title: "La ricreazione del mondo"
date: 2006-05-09
draft: false
tags: ["ping"]
---

Apple venderà contenuti digitali in modo che non risiederanno mai sul disco rigido, ma verranno resi disponibili solo sugli iDisk degli utenti.

<a href="http://www.thinksecret.com/news/0511contentdist.html" target="_blank">Think Secret del 2 dicembre 2005</a>.

Le notizie oramai nascono dal nulla assoluto. Perfino nella Genesi, prima c'era almeno il caos.