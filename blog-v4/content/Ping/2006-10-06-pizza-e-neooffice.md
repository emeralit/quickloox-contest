---
title: "Pizza e NeoOffice"
date: 2006-10-06
draft: false
tags: ["ping"]
---

Stasera tutti da Mac@Work alle 19:30. Incontro con il FreeSmug e, alle 20, videoconferenza con gli sviluppatori di <a href="http://www.neooffice.org/" target="_blank">NeoOffice</a>. Distribuzione, forse a sorteggio forse a pagamento, di Cd di NeoOffice e consulenza sull'installazione per chi arriva in portatile.

Alle 20:30 transumanza verso la pizzeria <a href="http://maps.google.com/maps?f=q&amp;hl=en&amp;q=via+terraggio+20+milan+italy&amp;ie=UTF8&amp;z=17&amp;ll=45.465353,9.17588&amp;spn=0.001166,0.007735&amp;om=1&amp;iwloc=A" target="_blank">Bio Solaire</a> di via Terraggio 20, a due passi da <a href="http://www.macatwork.net" target="_blank">Mac@Work</a>. Per questioni di prenotazione in pizzeria, meglio <a href="http://www.poc.it/meeting.php" target="_blank">preiscriversi</a> alla pizzata.

Lo so, l'ho già detto. Se è per quello ho mangiato la pizza anche l'altroieri, ma stasera mi piacerà ugualmente. :-D