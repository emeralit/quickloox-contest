---
title: "Cameriere, un altro bianchino per favore"
date: 2006-03-03
draft: false
tags: ["ping"]
---

Il premio <em>argomento su cui non c'è assolutamente niente da dire e cui si sprecano fiumi di parole inutili peggio che al bar, senza aggiungere alcunch&eacute; di utile al dibattito, ma in compenso alzando una cortina di fumo che impedisce di capire l'essenziale</em> per il 2006 è assegnato d'autorità al Trusted Computing. Che Mac OS X non usa in alcun modo significativo per l'utente finale, bombardato tuttavia da articoli terroristici in quantità.