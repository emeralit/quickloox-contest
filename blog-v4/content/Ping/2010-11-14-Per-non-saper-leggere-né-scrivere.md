---
title: "Per non saper leggere né scrivere"
date: 2010-11-14
draft: false
tags: ["ping"]
---

Adesso che <a href="http://statsheet.com/" target="_blank">StatSheet</a> ha lanciato 345 siti (più canale Twitter e presenza Facebook) dedicati a ciascuna squadra di basket di prima fascia del campionato universitario americano, e che gli articoli su tutti i 345 siti sono <i>generati in modo automatico</i>, il tuo giudizio sui comunicati stampa e sul materiale di comunicazione della tua azienda è in qualche modo cambiato?

Voglio dire che se avrebbe potuto scriverlo un computer attingendo a un <i>database</i> di frasi fatte e informazioni interne, forse non è cos&#236; interessante. Se invece dovrebbe essere interessante per chi dovrà leggerlo, forse è meglio che venga scritto in forma umana da umani capaci di renderlo interessante e farlo spiccare nel mucchio.

Oppure, se bastano un computer e un <i>database</i> più un buon algoritmo di generazione automatica di comunicati, c'è da riorganizzare l'ufficio stampa.

<i>Quartum non datur</i>.