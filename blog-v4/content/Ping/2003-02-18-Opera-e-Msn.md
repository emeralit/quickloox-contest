---
title: "Ci sono e ci fanno"
date: 2003-02-18
draft: false
tags: ["ping"]
---

Storia di un browser libero e di un monopolista criminale

Sai già che Microsoft è stata condannata in tribunale per abuso di posizione dominante e concorrenza sleale. Forse non sai i sistemi che usano.

Per esempio, il sito Msn di Microsoft è scritto in modo che, se usi il browser libero Opera, la pagina viene resa male. Non è un errore, ma – come si dimostra mediante alcuni semplici esperimenti – viene fatto apposta, al solo scopo di fare danno a chi voglia visitare le pagine Microsoft con un software che non garba a Microsoft. Sottinteso: sei tu a non essere libero di poter usare il browser che vuoi. Microsoft è libera di produrre pessimo software, ma non di discriminare l’utenza in modo arbitrario e gratuito. Quelli di Opera se la sono presa, ma si sono anche fatti due risate, e hanno creato una versione del browser che su Msn <link>distorce il contenuto delle pagine</link>http://www.opera.com/pressreleases/en/2003/02/14/ parafrasando il famoso cuoco svedese del Muppet Show.

Apple ha creato Safari per avere il browser più veloce sul mercato, ma c’è una buonissima ragione in più per usarlo: la soddisfazione di cestinare Explorer ed esercitare così una sana libertà di scelta.

<link>Lucio Bragagnolo</link>lux@mac.com