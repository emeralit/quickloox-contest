---
title: "Vendesi a peso"
date: 2006-02-28
draft: false
tags: ["ping"]
---

Il mio PowerBook costa, di listino Apple Store, 84 eurocentesimi al grammo.

Da notare che, se fosse costruito peggio, costerebbe di più. Se, infatti, a parità di prestazioni pesasse di più, il suo costo per grammo sarebbe inferiore.