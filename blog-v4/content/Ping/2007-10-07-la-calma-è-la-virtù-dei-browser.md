---
title: "La calma è la virtù dei browser"
date: 2007-10-07
draft: false
tags: ["ping"]
---

Erano settimane che non lo facevo. Avevo bisogno di un documento condiviso. Sono andato su <a href="http://docs.google.com" target="_blank">Google Docs</a>, ho aperto il documento, ho cominciato a riempirlo con le prime informazioni, ho invitato chi dovevo, ho controllato l'elenco dei documenti condivisi&#8230; e improvvisamente mi sono reso conto che stavo usando Safari.

Fino a ieri avevo sempre usato Firefox, per sicurezza e perché Safari era quasi compatibile, ma ogni tanto inciampava su qualcosa. Non ho ancora provato il foglio di calcolo in effetti; ma, se Safari ha funzionato coon il word processor sull'uso di impulso, funzionerà anche su un uso ragionato. :-)