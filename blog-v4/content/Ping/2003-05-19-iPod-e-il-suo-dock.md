---
title: "Un dock facoltativo"
date: 2003-05-19
draft: false
tags: ["ping"]
---

Qualcuno pensa che il nuovissimo iPod sia un po’ meno portatile. E sbaglia

Ma lo hai visto il nuovo iPod? È semplicemente fantastico. Sensibilmente più piccolo del precedente, eppure ancora più capace nel disco rigido e raffinato nelle linee, nell’interfaccia, nei particolari (i pulsanti visibili anche al buio, per esempio). Con il nuovo piccolissimo dock per tenerlo sulla scrivania intanto che si ricarica.

Già, il dock. Qualcuno si è scandalizzato: forse il nuovo iPod si carica solo stando dentro il dock, con tutte le conseguenze del caso?

Ma no; il dock è un di più. Il cavo FireWire che porta la corrente al dock può portare la corrente direttamente all’iPod.

Quindi l’iPod è portatile esattamente come prima e, per caricarlo via FireWire, basta portarsi dietro il suo cavo. Stavolta non può essere un cavo FireWire qualunque, perché le dimensioni dell’aggeggio hanno imposto un connettore particolare e quindi il cavo deve essere proprio il suo, ma per il resto iPod è sempre lui. Anzi: ancora più bello.

<link>Lucio Bragagnolo</link>lux@mac.com