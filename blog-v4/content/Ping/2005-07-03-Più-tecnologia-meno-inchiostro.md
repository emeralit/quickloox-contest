---
title: "Alta tecnologia per basso inchiostro"
date: 2005-07-03
draft: false
tags: ["ping"]
---

Una estensione interessante ai driver di stampa Apple

A partire da Tiger, le stampanti collegate in rete con un numero Ip sono in grado di <a href="http://developer.apple.com/technotes/tn2005/tn2144.html">comunicare lo stato delle riserve di inchiostro</a>, grazie al comando snmpInk presente in /System/Library/Printers/Libraries/.

Il comando usa il protocollo Snmp per rispondere con un complesso di dati Xml che il programmatore può decidere di trasformare in una estensione della finestra di dialogo di stampa, che mostra lo stato delle cartucce.

Spero che questa possibilità venga usata presto e da tutti. Sapere a che punto è l'inchiostro, come dire, non secca.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>