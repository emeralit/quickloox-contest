---
title: "C’è sempre una ragione"
date: 2003-02-03
draft: false
tags: ["ping"]
---

Quando non sappiamo perché, non vuol dire che manchi un perché

La lettera della settimana è di chi ha scritto, più o meno, “ho installato l’aggiornamento a Carbon Sound Manager 6.0.2 e, subito dopo avere installato, la macchina non montava più i Cd né i Memory stick della fotocamera. Dopo vari tentativi ho lanciato Disk Copy e a quel punto sia i Cd che i Memory Stick hanno ripreso a montarsi regolarmente sulla scirivania”.

Uhm. Dentro i package c’è sempre un Bill of Materials, un file .bom che spiega il contenuto dell’update. Questo update installa solo due cose: un nuovo Sound Manager Component per QuickTime e un nuovo framework Carbon Sound per i programmi Carbon. Impossibile che abbia cambiato, in un qualsivoglia modo, le routine di trattamento dei volumi.

L’unica ipotesi possibile è che Disk Copy abbia casualmente rimesso a posto qualcosa che sarebbe andato a posto comunque con un logout-login o un riavvio della macchina; qualcosa che solo per coincidenza è andato storto dopo l’update.

Due gli insegnamenti. Il primo: mai esprimere giudizi affrettati. Secondo: c’è sempre una ragione. Terzo (omaggio): un logout-login certe volte risolve.

<link>Lucio Bragagnolo</link>lux@mac.com