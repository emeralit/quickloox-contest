---
title: "Il contributo di Mac"
date: 2003-10-06
draft: false
tags: ["ping"]
---

Se il computing diventa sempre più facile lo si deve a quei folli della Mela

Dice in mailing list: come mai quel tale programma mi permette con facilità di compiere un’operazione pericolosa?

Domanda piccola ma che spiega molto. Spiega come l’informatica sia, nonostante tutto, appena nata. Deve ancora venire il tempo in cui useremo il computer come possiamo usare un macinacaffè.

Ma spiega anche come esista un’aspettativa. Si parla molto di intelligenza artificiale, ma il nostro software è talmente primitivo che in gran parte non riesce a fare fronte neanche all’errore umano.

Dobbiamo l’esistenza di questa aspettativa a un pugno di lucidi folli che nei primi anni Ottanta hanno sacrificato vita, sonno, alimentazione e affetti alla creazione di un computer talmente diverso dagli altri che aveva un’interfaccia umana.

Quel poco di facilità che hanno i computer di oggi lo dobbiamo a Macintosh.

<link>Lucio Bragagnolo</link>lux@mac.com