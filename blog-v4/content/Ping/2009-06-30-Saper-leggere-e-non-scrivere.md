---
title: "Saper leggere e non scrivere"
date: 2009-06-30
draft: false
tags: ["ping"]
---

Apple dovrebbe tradurre più in fretta la pagina sulle novità di Snow Leopard. In sua latitanza si fornisce un servizio sostitutivo, una novità al giorno.

<b>Supporto in lettura di Hfs+ dentro Boot Camp</b>

Ora Boot Camp sa leggere il formato Hfs+ e questo consente di arrivare ai file sulla partizione Mac OS X da quella Windows. Il supporto è a sola lettura per evitare che i virus per Pc possano colpire Mac OS X; d'altronde si può registrare sulla partizione Windows il lavoro compiuto sotto Windows e accedervi più tardi da Mac OS X.

<cite>Il supporto è a sola lettura per evitare che i virus per Pc possano colpire Mac OS X</cite>. Una frase che ne vale, più o meno, centomila, una per virus.