---
title: "Avviso ai simulatori (di circuiti)"
date: 2006-02-14
draft: false
tags: ["ping"]
---

Qucs, il Quite Universal Circuit Simulator, è abbastanza universale da funzionare anche su Mac.

Bisogna avere Xcode (o i Developer Tools, come si chiamavano installato. Poi serve installare le librerie <a href="http://naranja.umh.es/~atg/software-qt3.html" target="_blank">Qt/Mac versione 3</a> (non la versione 4, la più avanzata disponibile, ma troppo avanzata per Qucs) nella variante giusta per il proprio Mac.

Fatto questo, è sufficiente scaricare Qucs da <a href="http://www.versiontracker.com/dyn/moreinfo/macosx/22859" target="_blank">VersionTracker</a> oppure dal <a href="http://qucs.sourceforge.net/" target="_blank">suo sito</a>.

Rispondendo alla richiesta di un lettore di Macworld mi sono incartato nel tentativo inutile di fare funzionare il tutto con Qt/Mac 4. Con la 3, però, va tutto e il programma, per quanto un po' rozzo da guardare in perfetto stile open source, è davvero potente.