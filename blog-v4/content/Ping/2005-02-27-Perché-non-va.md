---
title: "No, io sono puntuale"
date: 2005-02-27
draft: false
tags: ["ping"]
---

Almeno nel presentare la scusa

Ogni tanto c'è un giorno senza Ping.
Quando accade e in uno dei giorni precedenti ci sono più Ping, significa che avevo predisposto la pubblicazione differita (inserisco il pezzo martedì, per dire, e predispongo la pubblicazione automatica per venerdì).

Solo che il sistema non funziona. Non è colpa del motore, eccellente; è che i tecnici addetti non hanno tempo a sufficienza per occuparsi del problema e sistemarlo.

Questo Ping è stato scritto domenica 27 per essere pubblicato in data lunedì 28, per esempio.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>