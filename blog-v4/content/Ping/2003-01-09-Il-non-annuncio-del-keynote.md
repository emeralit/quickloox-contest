---
title: "Il non annuncio del keynote"
date: 2003-01-09
draft: false
tags: ["ping"]
---

Una delle novità importanti che Steve Jobs non ha detto

Il discorso di apertura del Macworld Expo di San Francisco tenuto da Steve Jobs, gran maestro di Apple, è stato ricco di sorprese. I nuovi PowerBook G4 con l’oltraggiosamente comodo schermo da 17”, le iApps tutte rinnovate, AirPort Extreme, la sfida folle e meravigliosa di Safari e tutto il resto.

Ma c’è una cosa che Jobs non ha detto e forse avrebbe dovuto: Apple sta lavorando alla versione 2.0 di AppleScript, che risulterà molto più utile, semplice e potente di adesso.

AppleScript non porterà la stessa integrazione di iLife, ma sta diventando un collante sempre più efficace per fare lavorare più programmi insieme, o fare lavorare un programma in un modo che lui non si aspetta. AppleScript 2.0 è davvero una bella aspettativa. È in beta, arriva presto.

<link>Lucio Bragagnolo</link>lux@mac.com