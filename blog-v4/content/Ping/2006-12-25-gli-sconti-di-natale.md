---
title: "Gli sconti di Natale"
date: 2006-12-25
draft: false
tags: ["ping"]
---

Entro oggi: <a href="http://www.macgamestore.com/detail.php?ProductID=702" target="_blank">dieci giochi per Mac</a> a soli 25 dollari totali.

Si possono masterizzare e poi appendere i Cd sull'albero. Vale lo stesso.

Ah, e c'è anche <a href="http://macsanta.com/" target="_blank">MacSanta</a>. Sconto del 20 percento.