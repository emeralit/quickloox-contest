---
title: "Azione dimostrativa"
date: 2006-01-11
draft: false
tags: ["ping"]
---

Ci sarà tempo per dissezionare fino all'ultimo chip MacBook Pro. La prima impressione che ho, a caldo ma dopo qualche ora di riflessione, è che si tratti di un'azione dimostrativa.

Mentre iMac è effettivamente la gamma di macchine scelta per iniziare la transizione (sorry, siti di rumor. Notato che, tra l'uno e l'altro, sono state candidate a iniziare la transizione <em>tutte</em> le macchine? Come gli oroscopi, in qualche modo ci si prende sempre…), MacBook Pro serve ad Apple per mostrare che la tecnologia è pronta, che la stasi causata dal mancato (e mai esistito) G5 è terminata, che un Mac portatile con tecnologia Intel è competitivo eccetera.

Non darei troppo peso alle prime notizie, che non sono notizie ma sciocchezze. Sapere che l'autonomia dei prototipi in mostra all'Expo dia due ore non serve a niente. Potrebbe essere, oppure no. Sono prototipi.

Prosegue l'eliminazione delle tecnologie secondarie. Non c'è più il modem esterno. Non c'è più la porta S-video. Non c'è più FireWire 800. Non c'è più la porta Pcmcia, sostituita da una tecnologia nuova. Ma sembra avere un senso. &Egrave; dal 1997 che non uso la porta modem; certamente ci sarà chi ne ha bisogno, ma non è più indispensabile.

Chi parla di abbandono di FireWire da parte di Apple, perch&eacute; manca la porta FireWire 800, non sa quello che dice.

E tu che ne pensi? Facciamo un bel giro di commenti sul MacBook Pro?