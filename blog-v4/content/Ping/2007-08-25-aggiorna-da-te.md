---
title: "Aggiorna-da-te"
date: 2007-08-25
draft: false
tags: ["ping"]
---

<a href="http://www.adiumx.com/" target="_blank">Adium</a>, <a href="http://cyberduck.ch/" target="_blank">Cyberduck</a> e <a href="http://www.opencommunity.co.uk/vienna2.php" target="_blank">Vienna</a> (per quanto avviene in casa mia) si aggiornano e si rilanciano da soli. Basta essere d'accordo e dare l'ok. Delizioso.

Per confronto, <a href="http://www.lemkesoft.de" target="_blank">GraphicConverter</a>, pure uno dei programmi più utili per Mac, è all'età della pietra. <em>C'è una versione nuova disponibile, la vuoi scaricare lanciando la cinquecentomillesima finestra del browser e scaricando l'ennesima immagine disco da andare a recuperare nel Finder per poi sostituire a mano la vecchia copia del programma mediante una procedura noiosa e che potrebbe benissimo fare il computer al posto tuo, se solo il programmatore avesse voglia?</em>