---
title: "Pigro e non pigro"
date: 2005-11-16
draft: false
tags: ["ping"]
---

Come mi comporto di fronte a un aggiornamento

Ho appena saputo di un aggiornamento a <a href="http://cyberduck.ch">Cyberduck</a>, il mio programma favorito di Ftp e Sftp. L'ho scaricato immediatamente e l'ho già installato al posto della versione vecchia.

Con i programmi indipendenti faccio sempre così. Appena vedo che c'è un aggiornamento, lo installo subito.

Con Mac OS X sono molto più rilassato. Ho impostato l'aggiornamento automatico, su base settimanale, e lascio fare a lui. Altrimenti che automatico è?

A volte è passata anche una settimana da un update di Mac OS X a quando effettivamente è avvenuto l'aggiornamento sul mio Mac. Per quanto siano sempre migliorie benvenute, il nucleo del sistema funziona già molto bene e quindi vado avanti a lavorare senza preoccuparmi e perderci tempo sopra. Il caso in cui viene risolto un bug di cui soffrivo personalmente gli effetti è raro o inesistente e, per questo, non ho nessuna urgenza di aggiornare.

È certamente un caso, ma nessun aggiornamento mi ha mai dato problemi.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>