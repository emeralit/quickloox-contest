---
title: "Pro (e contro)"
date: 2007-11-26
draft: false
tags: ["ping"]
---

Mi infastidiscono le nozioni di <em>basic</em> e <em>pro</em> che girano intorno al Mac e all'informatica in generale.

Il marketing aggiunge la parola <em>pro</em> per vendere configurazioni più ricche e potenti, ma finisce l&#236;. La maggior parte del lavoro di sviluppo di <a href="http://php.net/" target="_blank">Php 5</a> è stata svolta su un MacBook. Il vero <em>pro</em> vuole un Mac potente per stare comodo e spremerlo al massimo, ma spreme al massimo qualunque cosa abbia sotto le mani. Prima di arrivare a giocare negli stadi della Champions League, su quell'erba perfetta, qualunque <em>pro</em> si è prima smazzato il campetto dell'oratorio.

Sembra anche a volte che un <em>pro</em> sia uno che sa usare molte applicazioni, e un <em>basic</em> solo poche. Col cavolo: <a href="http://www.martinrainone.com" target="_blank">Martin e Rainone</a> lavorano per il 90 percento del tempo in Photoshop e sono eccellenza. Ma chiedergli di usare <a href="http://www.guit.sssup.it/" target="_blank">TeX</a> oppure <a href="http://cran.r-project.org/" target="_blank">R</a> porterebbe a poco.

Un altro mito è che un <em>pro</em> usi per forza applicazioni molto specifiche, le cosiddette <em>verticali</em> di cui si riempiono la bocca i venditori.

Non sono d'accordo. Un asso del Mac lo metti l&#236; davanti alla dotazione standard del sistema e farà meraviglie. Anzi, conosco <a href="http://www.sourcesense.com/it/home" target="_blank">pro dell'open source</a> che il Mac lo fanno partire direttamente dal Terminale e usano solo quello, altro che le applicazioni verticali. Certe volte quello che ha bisogno del superprogramma per ottenere un risultato è uno che deve affidarsi al software perché da solo non ce la fa.

Alla fine, il <em>pro</em> è uno che ha grande conoscenza e fa grandi cose. Il computer è uno strumento, il software pure. Un righello è sempre un righello, in mano a me e in mano a Renzo Piano. Un righello costa niente. Lui sa usarlo, però.