---
title: "Un minuto di silenzio"
date: 2011-02-09
draft: false
tags: ["ping"]
---

Si è spento Ken Olsen, uno dei protagonisti della rivoluzione informatica dello scorso secolo, che nessuno ricorderebbe &#8211; al contrario di un Jobs, un Gates, uno Zuckerberg &#8211; perché le fortune di Digital Equipment Corporation hanno declinato da ben prima che arrivasse Internet.

Ricordo che avevo da poco il mio Macintosh Plus e lo portai con fierezza a casa di un amico, che possedeva un Apple II e lavorava proprio in Digital, conoscendo cos&#236; tutti i segreti di quei minicomputer e del loro sistema operativo. Passammo una notte a entrare in sistemi Digital attraverso il mio Macintosh e scoprire server da tutto il mondo, con dentro meraviglie per allora.

Oggi esistono migliaia di canali televisivi, tutti a colori. Quella era l'epoca in cui ne esistevano una manciata e l'era del bianco e nero non era finita da moltissimo.

Una ricerca su Google porterà a galla innumerevoli necrologi e non c'è bisogno che li segnali. Tengo invece a un <a href="http://money.cnn.com/magazines/fortune/fortune_archive/1986/10/27/68216/" target="_blank">articolo di Fortune</a> del 1986, che racconta la storia di Olsen e di Digital ai tempi del pieno splendore. Erano epoche in cui si poteva fare fortuna, per esempio, ordinando che tutti i sistemi prodotti da un'azienda avessero lo stesso tipo di spina elettrica.

Essendo materiale di archivio ha una formattazione infame e leggerlo richiede pazienza. Vale, però.