---
title: "L’utente A e l’utente B"
date: 2003-03-31
draft: false
tags: ["ping"]
---

La critica che serve e quella di cui si farebbe anche a meno

L’utente A si chiede “Perché iChat non ha un dizionario AppleScript quando lo stesso Aol Messenger ne ha uno ottimo?” Questa è una critica sacrosanta verso Apple. L’utente A manda un <link>feedback</link>http://www.apple.com/it/macosx/feedback e fa bene a se stesso, e ad Apple.

L’utente B is chiede “Perché Script Editor non registra gli script come faceva in Mac OS 9?” Questa è una critica sciocca, perché dipende dal programma, non da Script Editor. L’utente B non sta muovendo una critica costruttiva e farebbe meglio a informarsi un po’ meglio. Non è difficile, basterebbe l’aiuto di AppleScript e una visitina al sito Apple.

Poi c’è l’utente C. Ha scaricato la beta pubblica di Safari e passa mezze giornate a cercare di avere una versione un po’ meno beta, di quelle che Apple passa solo agli sviluppatori fidati, perché se trova una 1.0b67 gli cresce l’autostima; l’amico ha una 1.0b66 e quindi fa la figura dello zotico.

L’utente C si lamenta perché la beta non pubblica, trovata chissà dove, che contiene chissà cosa, non funziona con un sito scritto con i piedi (sinistri), usualmente di nessuna rilevanza.

Non è un critico di Apple. È critico lui.

<link>Lucio Bragagnolo</link>lux@mac.com