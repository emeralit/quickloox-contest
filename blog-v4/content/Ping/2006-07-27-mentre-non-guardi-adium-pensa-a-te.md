---
title: "Mentre non guardi Adium pensa a te"
date: 2006-07-27
draft: false
tags: ["ping"]
---

Segnala <strong>Jakaa</strong>:

<cite>Ci ho appena fatto caso: l'icona del papero di <a href="http://adiumx.com/" target="_blank">Adium</a> apre gli occhi solo se è connesso, altrimenti&#8230; dorme.
</cite>
Sono sfumature che bisogna essere svegli per cogliere. Complimenti allo spirito d'osservazione!