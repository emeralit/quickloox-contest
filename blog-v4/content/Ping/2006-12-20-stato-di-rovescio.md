---
title: "Stato di rovescio"
date: 2006-12-20
draft: false
tags: ["ping"]
---

Il diritto è oramai morto e non tennisticamente. Il Ministero della Giustizia ne ha fatta una che, informaticamente parlando, veramente merita ogni e qualunque aggravante, come ha ben documentato <a href="http://www.afhome.org/2006/12/14/il-mistero-della-giustizia/" target="_blank">Francesco</a>.

Poi la gente sta a litigare, destra contro sinistra, come se cambiasse veramente qualcosa. Quello che resta sempre uguale è il <em>match</em> tra capaci contro incapaci. Negli uffici del Ministero il risultato è sempre quello: non ci sono abbastanza capaci per mettere su neanche una squadra di <em>curling</em>.