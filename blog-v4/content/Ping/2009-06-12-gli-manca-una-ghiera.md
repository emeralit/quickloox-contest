---
title: "Gli manca una ghiera"
date: 2009-06-12
draft: false
tags: ["ping"]
---

L'amministratore delegato di SanDisk ha messo la parola fine alla questione. <a href="http://money.cnn.com/2009/06/02/technology/sandisk_flash_memory.fortune/index.htm?postversion=2009060305" target="_blank">Ha vinto iPod</a> e la concorrenza è quella per diventare numeri due. Ok.

A tutti è, letteralmente, mancata una ghiera (non dico rotella) per battere iPod e lo capisco.

Non capisco un'altra cosa.

Apple è entrata nel campo di Nokia e Samsung con iPhone e adesso fa concorrenza agli altri con iPhone.

Ma nessuno di questi fa concorrenza ad Apple con iPod touch. Che è molto più di un accessorio; dei 75 milioni di utenti attivi OS X annunciati da Phil Schiller alla Wwdc, più di venti milioni arrivano da iPod touch.

L'unica che fa concorrenza a iPod touch, Microsoft con il nuovo Zune Hd in uscita a settembre, non fa concorrenza a iPhone (quindici milioni di utenze e anche di più). Da una parte o dall'altra, la coperta è corta.

O sono tutti rimbambiti, o davvero Apple è più avanti e non di poco.