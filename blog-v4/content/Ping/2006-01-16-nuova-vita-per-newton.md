---
title: "Nuova-vita-per-newton"
date: 2006-01-16
draft: false
tags: [ping]
---

L’amico Riccardo non è l’unico ad amare il caro vecchio Newton. Sono ancora in molti e hanno appena terminato la WorldWide Newton Conference 2006, dove è stato mostrato il mitico Newton Os funzionare su un palmare Linux.

Se vanno avanti così potrei quasi pensare a un palmare persino io. Il Newton vero me lo tengo ben stretto e i palmari di oggi sono macchine a dire poco deludenti per scarsità di progressi tecnologici da dieci anni a questa parte. Però avere Newton Os sarebbe avere un prodotto buono almeno al 50 percento.
