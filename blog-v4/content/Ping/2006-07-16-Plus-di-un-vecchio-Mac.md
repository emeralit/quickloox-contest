---
title: "Plus di un vecchio Mac"
date: 2006-07-16
draft: false
tags: ["ping"]
---

<strong>Stefano</strong> mi ha appena segnalato un piccolo capolavoro di ingegnosità: <a href="http://www.ipodpalace.com/view.php?ID=1307" target="_blank">trasformare un Mac Plus</a> in una coppia di diffusori per iPod.

Segno dei tempi&#8230;