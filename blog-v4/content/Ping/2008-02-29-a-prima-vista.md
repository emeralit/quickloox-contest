---
title: "A prima vista"
date: 2008-02-29
draft: false
tags: ["ping"]
---

Un buon sistema per farsi un'idea è guardare alle pagine iniziali dei forum <a href="http://discussions.apple.com/" target="_blank">di Apple</a> e <a href="http://forums.microsoft.com/" target="_blank">di Microsoft</a>.

Quale delle due aziende, secondo te, produce l'interfaccia utente migliore?