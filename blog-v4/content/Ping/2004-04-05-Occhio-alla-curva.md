---
title: "Occhio alla curva"
date: 2004-04-05
draft: false
tags: ["ping"]
---

Come distinguere un giornalista esperito da uno che dovrebbe sparire

Viviamo in tempi di decadenza del linguaggio, dove la gente pensa che “to realize” significhi “realizzare” ed “eventually” voglia dire eventualmente.

Una delle mostruosità tipiche delle riviste di informatica è la famosa curva di apprendimento, quella che dovrebbe spiegare se un programma è semplice da imparare o meno.

Se costruisco un grafico cartesiano mettendo in ordinata la quantità di apprendimento e in ascissa il tempo, scopro che una curva di apprendimento ripida è una curva molto positiva: si impara tanto in poco tempo. Tutto il software dovrebbe essere così.

Ricordatene, la prossima volta che leggi un articolo.

<link>Lucio Bragagnolo</link>lux@mac.com