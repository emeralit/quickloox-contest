---
title: "Essere alieno oggi"
date: 2006-06-07
draft: false
tags: ["ping"]
---

<strong>Mario</strong> chiedeva di precisare meglio in che modo Gimp ha un feeling alieno rispetto a Photoshop e al comportamento tipico dei programmi di fotoritocco su Mac.

Non c'è spiegazione migliore di una <a href="http://www.gimpshop.net/" target="_blank">edizione di Gimp</a> fatta apposta per somigliare il più possibile a Photoshop. Basta confrontarle. :-)