---
title: "Lunghetti i brevetti"
date: 2005-09-02
draft: false
tags: ["ping"]
---

Perché non basta ripetere le notizie come i pappagallini

Ti ricordi di un'epoca lontana, in cui i siti di sedicente informazione sbraitavano di una guerra tra Apple e Microsoft per il brevetto di iPod (nientemeno)? E Microsoft aveva battuto Apple? E il brevetto Apple che era stato respinto perché c'era già quello Microsoft

Adesso si legge di un brevetto Creative che metterebbe nei guai Apple.

Ma perché nessuno ne parlava prima? Non era una guerra tra Apple e Microsoft? Se Microsoft è arrivata prima di Apple, perché il brevetto di Creative mette nei guai Apple, e non invece Microsoft?

Forse che quelle scritte prima erano sciocchezze? E allora, perché quelle scritte adesso dovrebbero essere più sensate?

C'è troppa gente in giro che scrive ripetendo cose dette da altri, senza capirle. Sbagliare per sbagliare, meglio farlo in modo originale.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>