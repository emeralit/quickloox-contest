---
title: "Piccolo schermo grande feticcio"
date: 2005-12-04
draft: false
tags: ["ping"]
---

Sarà che ormai ho un'età, ma le risoluzioni contano

Lancio una modesta provocazione, che però mi sorge prepotente dall'animo e dal polpastrello.

Gli iPod. Che non si chiamano iPod video, ma solo iPod e basta.

Posso arrivare a capire il riversamento su iPod dei filmati personali. Quelli che amano atterrire gli amici e i parenti con ore di noiosissimo girato dell'ultima vacanza a Malindi o la macroripresa del parto della cognata hanno a disposizione, con i nuovi iPod, un'arma devastante.

Posso vagamente intuire il perché uno voglia mettersi su iPod il Dvd del film che gli è piaciuto tanto, un po' per backup perché è meno costoso che masterizzare un Dvd (sicuro? Fatto due conti? Io ho qualche dubbio) un po' per impressionare i colleghi in ufficio, un po' qui, un po' là.

Però ho visto, per la prima volta, temo non l'ultima, una persona stare a guardare un film sullo schermo dell'iPod. Sarà che ho un'età, sarà che sono un bastian contrario, sarà che ho una vista pressoché perfetta e posso godermi uno schermo grande, ma a me sembra un'aberrazione.

Ultimamente mi è capitato di guardare una quindicina di minuti de <em>Il Ritorno del Re</em> sul PowerBook 17&rdquo;. Per me è il minimo sindacale di dimensione per la visione di un film. Almeno di un film che va anche visto.

Sullo schermo dell'iPod potrei guardarne a malapena quindici secondi. In qualsiasi altro caso non sto guardando il film, ma prendendo a pretesto quel pollice di diagonale per rivivere l'emozione. Bello, intendiamoci; ma fa tanto Proust e <em>madeleinette</em>, alla ricerca del tempo perduto.

Sono vecchio e arretrato io, oppure il film visto sullo schermo di iPod è un feticcio? Il dibattito è aperto.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>