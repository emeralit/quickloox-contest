---
title: "La rivincita della fotografia"
date: 2006-07-17
draft: false
tags: ["ping"]
---

La fotografia digitale si rivelerà una delle grandi conquiste dell'umanità, ma ha come sottoprodotto una legione di coatti che credono di immortalare grandi momenti con il cellulare da due soldi e altrettanti megapixel, a gonfiarsi la bocca con il <em>mega</em>.

Qualcuno scatta foto da <a href="http://www.engadget.com/2005/06/24/the-gigapxl-project-images-of-up-to-four-billion-pixels/" target="_blank">due gigapixel</a>. Tiè.