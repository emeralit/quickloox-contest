---
title: "Partenza in discesa"
date: 2005-08-12
draft: false
tags: ["ping"]
---

Si può sperimentare un mondo virtuale in Internet spendendo zero

Sony ha reso disponibile come <a href="https://store.station.sony.com/digitalDistribution/base/detail.jsp?SKU=EQMAC-DD-SW0605-EQMACD">download gratuito</a> il software per giocare a Everquest su Mac.

Everquest è uno dei mondi online di maggior successo, con un seguito fedele di numerosissimi appassionati.

Il download è di ben due gigabyte, ma dà diritto a giocare gratuitamente per quindici giorni a Everquest, per poterlo valutare.

È un'occasione per provare, senza spendere, un'esperienza di gioco nuova e coinvolgente. Anche se è meglio <a href="http://www.worldofwarcraft.com">World of Warcraft</a>. :-)

<a href="mailto:ithlgalad.the.rogue@gmail.com">Ithilgalad</a>