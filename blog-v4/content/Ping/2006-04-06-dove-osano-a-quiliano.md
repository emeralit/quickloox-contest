---
title: "Dove osano a Quiliano"
date: 2006-04-06
draft: false
tags: ["ping"]
---

Non posso proprio non fare notare che Macworld Italia e All About Apple hanno unito le forze per organizzare una <a href="http://www.allaboutapple.com/ospiti/naviglio_viaggi.htm" target="_blank">gita al museo Apple di Quiliano</a>?

Il museo è il più grande al mondo e ha ricevuto le lodi della stessa Apple, ch&eacute; neanche in California esiste qualcosa del genere. Della gastronomia ligure non starò a dire. Insomma, una gran bella occasione.