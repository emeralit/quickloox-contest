---
title: "Ecco perché non c’è streaming"
date: 2006-01-10
draft: false
tags: ["ping"]
---

Una cosa che pochi hanno scritto: non si può vedere più il keynote in diretta perché, da qualche tempo, alla presentazione sul palco fa seguito un evento musicale. E per evitare problemi di diritti – da Internet il keynote si potrebbe vedere in tutto il mondo – la presentazione viene editata, e <em>poi</em> messa in rete.