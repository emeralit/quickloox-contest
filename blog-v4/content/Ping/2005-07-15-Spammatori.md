---
title: "Spammatori…!"
date: 2005-07-15
draft: false
tags: ["ping"]
---

Il grido di Alberto Sordi reinterpretato per i nostri giorni

Mi arriva una mail da support@mac.com, intitolata Members Support. Con essa mi si informa che il mio indirizzo @mac.com (quello che compare qui sotto) è stato usato per inviare ingenti quantità di spam e mi si invita a dare conferma al contenuto dell'allegato, altrimenti il mio account verrà cancellato.

L'allegato è un file di nome important-details.zip. Lo decomprimo e dentro c'è un file di nome <code>important-details.htm                                                                      .pif</code> (spazi compresi).

Messaggio finto, spam inutile, probabilmente classico virus per Windows. Butto via tutto, fra il divertito e lo scocciato. Ma l'importante è che non ho corso rischi, con il mio Mac, neanche per un minuto.

Bello pensare che spammatori e scrittori di virus sudano come matti per fare danni su milioni di computer Windows, mentre io posso starmene in poltrona a vedere quanto sono ridicoli i loro sforzi, se solo si manovra un Mac.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>