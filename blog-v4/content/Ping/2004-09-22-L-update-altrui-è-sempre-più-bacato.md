---
title: "L'update degli altri è sempre più bacato"
date: 2004-09-22
draft: false
tags: ["ping"]
---

Guardi che cosa succede con Windows Service Pack 2 e sei felice di non essere lì

A chi si lamenta dei Security Update di Mac OS X, o del peso degli aggiornamenti da scaricarsi via Internet, consiglio la cura Service Pack 2 di Windows.

Sono da settanta a oltre duecento mega che essenzialmente aprono di serie il firewall che era chiuso di serie, impediscono a chi usa Outlook di ricevere in posta file eseguibili e, dice la propaganda, rafforzano la sicurezza globale del sistema.

Piccoli effetti collaterali: duecento programmi commerciali non secondari hanno smesso di funzionare. Circa sessanta falle di sicurezza note non sono state rimediate e l'aggiornamento resta vulnerabile a un simpatico baco per cui si possono provocare catastrofi nel sistema guardando una immagine Jpeg corrotta in modo adeguato.

Salvo tappare occasionalmente buchi, resterà tutto essenzialmente com'è fino a 2006 inoltrato, quando se non ci saranno ritardi uscirà Longhorn. Senza il nuovo innovativo filesystem, perché non fanno in tempo.

Viva Mac OS X e viva gli aggiornamenti di sicurezza.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>