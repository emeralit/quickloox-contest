---
title: "Gli svantaggi del sovrappeso"
date: 2008-05-21
draft: false
tags: ["ping"]
---

Sto accumulando evidenze empiriche del fatto che sul mio Mac Time Machine incontra problemi quando il disco di avvio è prossimo all'essere completamente pieno. Il backup mostra di partire ma non succede niente e l'icona gira all'infinito. Talvolta ci sono problemi con il Finder o alcuni programmi, legati al filesystem; per esempio non riuscivo ad avere il prompt del Terminale né ad avere la finestra dialogo di registrazione documento su BBEdit o altri.

E non si riesce in alcun modo a smontare pulitamente il disco di backup, né a interrompere Time Machine, con le buone o con le cattive.

Molto a occhio mi sembra che la soglia dei possibili problemi sia sotto i due/tre gigabyte di spazio libero. In genere il primo backup va perfettamente e poi, dal secondo in poi, c'è il rischio-impallamento.