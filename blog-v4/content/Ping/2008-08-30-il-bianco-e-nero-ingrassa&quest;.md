---
title: "Il bianco e nero ingrassa?"
date: 2008-08-30
draft: false
tags: ["ping"]
---

Ci sono scacchisti nella comunità di Ping? Ci sono scacchisti che cercano di recuperare spazio su disco in ogni modo possibile?

Se s&#236;, dovrebbero stare attenti a Chess, gli scacchi di serie in Mac OS X. Il file <code>standard.lrn</code>, dove il gioco memorizza le lezioni che apprende giocando, situato in <code>~/Library/Application Support/Chess/</code>, può raggiungere dimensioni anche ragguardevoli.

C'è gente che lo ha trovato grande <a href="http://www.tuaw.com/2008/08/22/csi-mac-how-did-that-file-get-so-big/" target="_blank">un giga e mezzo</a>. Con buone ragioni. Ma non si sa mai che passa per la testa dei giocatori.