---
title: "La resa dell'Lcd"
date: 2005-02-23
draft: false
tags: ["ping"]
---

Un esperimento di luddismo intenzionale giunge a conclusione

Ho ceduto. Mi serviva la condivisione della connessione Internet, che Panther ha ma Puma no, e ho aggiornato a Panther l'iMac Lcd 15&rdquo; di casa.

Da almeno due anni l'iMac in questione funzionava tranquillo, regolarmente aggiornato… a Mac OS X 10.1.5. Ciononostante ha masterizzato tutti i miei backup (lui è il SuperDrive ufficiale) e ha svolto perfettamente tutte le sue funzioni. Accessorie, certo, ma non era certo una macchina trascurata.

In quanto macchina secondaria aveva uptime formidabili, di mesi addirittura, escludendo gli update di Apple.

Dopo avere installato Panther ho scaricato anche quei 150 mega di aggiornamento che si sono accumulati e l'iMac va tranquillo come sempre, forse anche un pizzico più veloce.

Morali: macchina vecchia fa comunque buon brodo; aggiornare una macchina non pasticciata è un'operazione di routine.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>