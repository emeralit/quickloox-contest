---
title: "L'etimologia del portatile"
date: 2011-02-11
draft: false
tags: ["ping"]
---

<i>Portatile</i> viene da <i>portare</i>. Qualcuno si comporta come se venisse da <i>porte</i>. Invece che pensare ai propri obiettivi e scegliere una macchina che li soddisfa, elenca le caratteristiche e più caratteristiche ci sono migliore sarebbe la macchina.

È un altro dei deliri tipici dell'informatica. Nella vita normale nessuno cerca lavatrici con due cestelli, auto a sette porte, cucine a dieci fuochi.

Ricordo le discussioni avute con chi sosteneva che Adamo di Dell fosse migliore di MacBook Air. Migliore per via di una progettazione superiore? Di un sistema operativo su misura? Una interfaccia utente capace di velocizzare il lavoro rispetto alle altre? No. Migliore perché aveva più porte.

La teoria è stata verificata. Dopo vari abbassamenti di prezzo, Adamo è uscito di produzione. MacBook Air è ancora l&#236; e la percezione di tutte le persone che sento è che venda perfino più di prima.

Il prezzo di Adamo 13&#8221;, originalmente attorno ai duemila dollari a marzo 2009, è sceso <a href="http://news.cnet.com/8301-13924_3-20028576-64.html" target="_blank">fino a 799</a> dollari. MacBook Air 13&#8221; parte da 1.299 e perfino MacBook Air 11&#8221; parte da 999 dollari.

Non preoccupiamoci: <a href="http://www.ubergizmo.com/2011/02/dell-axes-its-super-slim-adamo-notebook/" target="_blank">corre voce</a> che la linea Adamo stia venendo riprogettata e che torni tra sei mesi.

E qui si potrebbe parlare di design (forma più funzione) talmente ben fatto che dura per anni e di design a basso valore, che si esaurisce e si butta via in poco tempo. Se non fosse che la gente sa parlare solo di prezzi. E di porte.

