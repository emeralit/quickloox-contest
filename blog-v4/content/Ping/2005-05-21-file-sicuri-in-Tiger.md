---
title: "Sicurezza è saperlo"
date: 2005-05-21
draft: false
tags: ["ping"]
---

File sicuri. Ma esistono? Quali sono?

Come è noto, nelle preferenze di Safari esiste la possibilità di inibire o meno l'apertura automatica dei file sicuri. Ma che concetto ha Safari di file sicuro?

La risposta è relativamente semplice:

/System/Library/CoreServices/CoreTypes.bundle/Contents/Resources/System

È un file Xml molto istruttivo da cui si evincono mille cose, compresi i tipi di file considerati sicuri. In Tiger originale la lista è la seguente:

com.apple.mach-o-binary<br>
com.apple.pef-binary<br>
public.elf-binary<br>
com.apple.application<br>
com.apple.framework<br>
public.shell-script<br>
com.microsoft.windows-executable<br>
com.sun.java-class<br>
com.sun.java-archive<br>
com.apple.quartz-composer-composition

Mac OS X 10.4.1 aggiunge alla lista un elemento in più:

com.apple.dashboard-widget

Altre due variazioni sono aggiungere .key e .pages all'elenco delle estensioni relative a file sicuri.

Chi se la sentisse potrà anche personalizzare l'elenco a suo completo piacere. A patto di non lamentarsi se, non sapendo quello che sta facendo, combina guai.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>