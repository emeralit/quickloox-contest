---
title: "Semestre bianco"
date: 2010-06-27
draft: false
tags: ["ping"]
---

Sono stato in visita al Mondadori Informatica di via Marghera a Milano e ho condotto un frettoloso censimento dei dischi rigidi colà in vendita.

Il conto approssimativo è di un modello di disco Usb3 e ventiquattro modelli di dischi Usb2.

È da dicembre che leggo disapprovazione per la scelta di non includere Usb3 nelle recenti infornate di Mac.

Ma a dicembre la disapprovazione si accompagnava regolarmente alla predizione di come l'offerta Usb3 sarebbe esplosa entro breve.

Sono passati sei mesi e vedo che, chi più chi meno, un modello Usb3 lo hanno tutti. Gli altri modelli però sono Usb2 e sono venti volte più numerosi.

Usb3 diventerà certamente ubiqua. Dicembre 2009 si è rivelata tuttavia data assai prematura e giugno 2010 non è da meno. Può darsi che se ne parli dal prossimo Natale, ammesso che Intel (inventore di Usb!) inserisca il supporto diretto di Usb3 sui propri chip, cosa che non ha ancora fatto né si sa con precisione quando farà.

Archiverei la questione almeno per un semestre.