---
title: "Quello che c'è e quello che no"
date: 2006-05-07
draft: false
tags: ["ping"]
---

Non è interessante che il novanta per cento dei confronti tra Mac e Pc si faccia tra un Mac che c'è, pronto da usare appena esce dalla scatola, e un Pc che non c'è e va configurato esattamente in quel modo, ammesso che non dia problemi di alcun tipo e il tempo richiesto per la configurazione sia zero?