---
title: "Disconoscimento del riconoscimento"
date: 2007-12-03
draft: false
tags: ["ping"]
---

Mi scrive <strong>Filippo</strong>:

<cite>all'atto dell'installazione di Readiris, è comparsa la schermata che ti allego.</cite>

<cite>Certo, il software sarà pure di &#8220;riconoscimento caratteri&#8221; ma chi ha localizzato il prodotto non (ri)conosce la lingua italiana&#8230;</cite>

<cite>Comunque, se il tizio scrive &#8220;Il quale ti permette di lanciare automaticamente un'applicazione associata, allorché la RICONOSCENZA è terminata&#8221;, possiamo scegliere se rallegrarci per le funzionalità del programma o piangere per la morte dell'idioma italico.</cite>

Di sicuro, conosco molti traduttori che si rallegreranno. Se c'è una professione sicura, visti i &#8220;progressi&#8221; dell'intelligenza artificiale in materia, è la loro.