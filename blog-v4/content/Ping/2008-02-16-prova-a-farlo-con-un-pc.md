---
title: "Prova a farlo con un Pc"
date: 2008-02-16
draft: false
tags: ["ping"]
---

Cos&#236; scrisse <a href="http://www.bottegascientifica.it" target="_blank">Carlo</a>:

<cite>Avere un Mac porta in dote delle macchine che grazie a Mac OS X ti semplificano la vita</cite>.

<cite>Un esempio spicciolo ma non banale</cite>.

<cite>Mia suocera mi prega di controllarle una sfilza di biglietti della Lotteria di Capodanno</cite>.

<cite>Da brava donna di una volta ha pensato bene di riportare i numeri dei singoli biglietti su un foglietto di carta</cite>.

<cite>Mi ha chiesto la cortesia di controllare se ve ne fossero di vincenti</cite>.

<cite>Mi ha detto</cite> col computer fai prima<cite>.</cite> Ok<cite>, le ho risposto</cite>.

<cite>Mi sono messo al Mac e in un attimo ho trovato la lista dei biglietti tramite Google</cite>.

<cite>Poi - e qui viene il bello - ho pensato ad un sistema che mi semplificasse la ricerca</cite>.

<cite>Semplice, ecco la soluzione:</cite>

<cite>- ho inviato tramite i servizi la selezione in un nuovo messaggio in Mail (ma questa è una finezza, avrei fatto lo stesso col vecchio copia-incolla);</cite>
<cite>- ho inserito il mio stesso indirizzo come destinatario della email e ho salvato il messaggio in bozze;</cite>
<cite>- ho selezionato la casella delle bozze e poi mi sono messo nella finestrella del campo di ricerca a inserire i numeri dell'elenco della suocera.</cite>

<cite>Ogni tanto la serie c'era, e magari i primi due o tre numeri poi - purtroppo&#8230; - niente. Insomma, in pochi attimi ho verificato la lista senza noiose ricerche a mano!</cite>

<cite>Mi sono detto:</cite> Avrei potuto farlo con un Pc?

Non saprei, probabilmente s&#236;. Di sicuro mancherebbe qualcosa, se non di evidente di indefinibile. Quella sensazione di quando il locale è gradevole, la pizza è buona, il prezzo è giusto, ma manca qualcosa.

A margine: sarebbe stato peggio se la suocera avesse vinto!