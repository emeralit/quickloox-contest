---
title: "L'uomo giusto al posto giusto"
date: 2010-03-12
draft: false
tags: ["ping"]
---

Ha fatto notizia la partecipazione di Steve Jobs alla notte degli Oscar.

L'unica notizia che ci vedo, indirettamente, è che sta bene.

Per il resto si tratta di quel tipico errore di valutazione per cui Apple sarebbe quella degli anni novanta, quando faceva titoli se usciva una nuova stampante.

Apple Computer, Inc. è diventata Apple, Inc. da cinque anni. Vende musica e vende film. iPod, un lettore digitale per musica e film, è una delle tre fonti di fatturato fondamentali. Produce software professionale per il video e per l'audio.

Nello specifico, durante la notte degli Oscar è andato in onda <a href="http://www.apple.com/ipad/gallery/#hardware06" target="_blank">il primo <i>spot</i> in assoluto su iPad</a>, un'ennesima piattaforma dove guardare film e ascoltare musica. Delle dieci <i>nomination</i> complessive tra i documentari, nove sono state realizzate con <a href="http://www.apple.com/it/finalcutstudio/finalcutpro/" target="_blank">Final Cut Pro</a> (si stima che il programma sia usato da metà dei professionisti, ma le <i>nomination</i> sono il novanta percento) e la decima <i>nomination</i>, quella fatta con Avid, non ha vinto. Non è monopolio, è dominio.

Intorno a Jobs circolava evidentemente la crema cinematografica di produttori, registi, tecnici, distributori, attori. Mentre per Dell o per Hewlett-Packard si tratta di alieni, per Apple sono partner, collaboratori, consiglieri, acquirenti, <i>testimonial</i>, magari concorrenti, sempre però persone attinenti al proprio <i>business</i>.

Come individuo, infine, Jobs è il maggiore azionista privato di Disney, che con il cinema ha qualcosa a che fare. Lo è diventato con la vendita della &#8220;sua&#8221; Pixar, che ha portato ai vertici assoluti intanto che faceva la stessa cosa con Apple. Incidentalmente, <a href="http://adisney.go.com/disneyvideos/animatedfilms/up/" target="_blank"><i>Up</i> di Disney/Pixar</a> ha vinto l'Oscar come migliore cartone animato.

Credo che Jobs avesse più ragioni di essere agli Oscar di tre quarti di tutti gli altri partecipanti. Ci sarebbe stato da stupirsi se non fosse andato.