---
title: "Continuo a volere il notbook"
date: 2009-07-27
draft: false
tags: ["ping"]
---

Ad aprile citavo Tim Cook, <i>chief operating officer</i> di Apple, che <a href="http://www.macworld.it/blogs/ping/?p=2280" target="_self">parlava di <i>netbook</i></a> durante la presentazione dei risultati finanziari:

<cite>Per noi il punto è creare grandi prodotti. E quando guardo a ciò che viene venduto nell'area dei</cite> netbook <cite>oggi, vedo tastiere sacrificate, software terribile, hardware d'accatto, schermi molto piccoli e di fatto una esperienza [&#8230;] su cui francamente non metteremmo il marchio Mac. Cos&#236; come è oggi non è un'area cui siamo interessati, né crediamo vi siano interessati a lungo termine i consumatori.</cite>

<cite>Ciò detto, teniamo d'occhio l'area e siamo interessati a vedere come rispondono i consumatori. </cite>[&#8230;] <cite>E se troviamo un modo per realizzare un prodotto innovativo che realmente porta qualcosa di nuovo, lo faremo.</cite>

Oggi cito Tim Cook, che <a href="http://seekingalpha.com/article/150291-apple-f3q09-qtr-end-6-27-09-earnings-call-transcript?page=-1&amp;find=netbook" target="_blank">parla di netbook</a> durante la presentazione dei risultati finanziari:

<cite>Non voglio mai escludere niente per il futuro e non voglio mai rispondere specificamente a una domanda su nuovi prodotti.</cite> [&#8230;] <cite>Penso che alcuni dei netbook in vendita siano molto lenti. Hanno tecnologia software che è vecchia.</cite>[&#8230;] <cite>Mancano di potenza. Hanno piccoli schermi e tastiere sacrificate.</cite> [&#8230;]
<cite>Credo che molti non ne saranno contenti e per parte nostra ci occupiamo solo di cose che possano essere innovative e di cui possiamo andare fieri.</cite>

<cite>Non vediamo il modo di realizzare un grande prodotto che costi 399 dollari.</cite>

Passati tre mesi, Cook non ha avuto bisogno di rimangiarsi neanche una virgola. Apple cresce in vendite e fatturato, sfiorando il record assoluto di vendite di Mac.

Resto convinto che, qualsiasi cosa presentasse domani, non sarà un <i>netbook</i> ma qualcosa d'altro, proprio come ha presentato iPhone al posto di un cellulare. E sarà molto meglio.