---
title: "Muoversi con grazie"
date: 2003-05-21
draft: false
tags: ["ping"]
---

Nasce una nuova attività commerciale di Apple

Forse qualcuno se ne sarà già accorto: Apple sta per mettersi a vendere <link>font</link>http://www.apple.com/fonts/buy/.

Forse non è qualcosa di affascinante come Applemusic.com, ma se avesse un cinquantesimo del successo di quest’ultima sarebbe un bel successo commerciale. Inoltre per Apple significa offrire un servizio più completo a un gruppo di utenza dove Macintosh ha una bella penetrazione. E forse non solo a loro, perché non sono un grafico ma un paio di font che mi piacciono io li comprerei, per avere un tratto distintivo in più o anche solo per piacere estetico.

Visto che da un po’ non si fanno battute particolarmente cretine, eccone una: è da queste cose che si capisce come Apple abbia ancora assi nella manica e come, soprattutto, dimostri carattere.

<link>Lucio Bragagnolo</link>lux@mac.com