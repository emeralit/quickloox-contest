---
title: "Bla bla bla bla cha cha cha"
date: 2009-06-09
draft: false
tags: ["ping"]
---

Organizzata in quattro e quattr'otto, la chiacchierata a contorno della partenza della <a href="http://www.apple.com/wwdc" target="_blank">Wwdc</a> ha reso per quanto mi riguarda quattro per quattro per tre e quattordici, o quasi.

Ci si è trovati infatti in quaranta e per qualche momento anche di più, a commentare la <a href="http://www.apple.com/it/macbookpro/" target="_blank">rivisitazione dei portatili</a>, le certezze su <a href="http://www.apple.com/it/macosx/" target="_blank">Snow Leopard</a>, l'arrivo imminente di <a href="http://www.apple.com/it/iphone/softwareupdate/" target="_blank">iPhone OS 3.0</a> e anche di <a href="http://www.apple.com/it/iphone/" target="_blank">iPhone 3Gs</a>, i dati di Apple (negli ultimi due anni gli utilizzatori di una forma qualunque di OS X sono schizzati a 75 milioni ed è una cifra notevole; un miliardo di scaricamenti da App Store è una cifra incredibile).

L'importante non era essere d'accordo o avere ragione. Era che fosse interessante e divertente e lo è stato.

Un grosso grazie a <b>gand</b> che ha reso ancora una volta disponibile il canale Irc di <a href="http://freesmug.org" target="_blank">FreeSmug</a> e un abbraccio ideale a tutti quanti hanno partecipato con allegria, informazione, spirito, entusiasmo, domande e quant'altro. È stato bello e si rifarà.