---
title: "Rassegnazione stampa"
date: 2006-04-20
draft: false
tags: ["ping"]
---

Per sapere che Microsoft ha rubato l'ennesimo brevetto per l'ennesima volta ed è stata ennesimamente condannata in tribunale bisogna consultare il <a href="http://www.nytimes.com/2006/04/20/technology/20soft.html?_r=1&amp;oref=slogin" target="_blank">New York Times</a> con la pazienza di Mario (grazie!). Può darsi che occorra registrarsi. Per i pigri: sanzione di 115 milioni di dollari.

Intanto il <a href="http://www.corriere.it/Speciali/Extra/2006/magazine/index.shtml" target="_blank">Corriere della Sera</a> pubblica su Magazine un pezzo dal quale estrapolo quanto segue: lasciate che i vosti figli vengano a me, chiesa, semidio venerato da una setta di adepti, fedeli, evangelisti, culto, profeti, martire, religione di massa, templi, santuari, cambio di religione, blasfema.

Il pezzo non è dedicato a Benedetto XVI, ma a Steve Jobs. Adesso aspetto una bella difesa di ufficio da tutti quelli così ben disposti verso Zucconi quando queste cose le scrive lui.