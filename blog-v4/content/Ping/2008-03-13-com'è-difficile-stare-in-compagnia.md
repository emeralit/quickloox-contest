---
title: "Com'è difficile stare in compagnia"
date: 2008-03-13
draft: false
tags: ["ping"]
---

Da membro del <a href="http://www.poc.it" target="_blank">Poc</a> faccio il possibile per abitare più a lungo che posso la chat <em>pocroom</em> e, per facilitarmi il compito, ho impostato l'<em>autojoin</em>. iChat dovrebbe ricollegarsi automaticamente alla chat quando ritorno al computer.

Non lo fa. Devo aspettare diversi minuti (altrimenti si rifiuta comunque) e poi dare manualmente l'ordine di riconnessione, con Comando-R (prima di Leopard sarebbe stato Comando-Maiuscole-G, ma non c'era l'<em>autojoin</em>).

Qualcuno ha avuto esperienze diverse?