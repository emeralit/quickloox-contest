---
title: "All'ultimo stadio"
date: 2010-02-07
draft: false
tags: ["ping"]
---

Una delle mie tante debolezze: ogni anno resto incantato a guardare il <a href="http://www.nfl.com/superbowl/44" target="_blank">Super Bowl</a>.

Ci fosse qualcuno che la condivide e magari ne ha pure di peggiori, tipo interessarsi al lato tecnico oppure invitare a casa la tribù degli amici e tirare mattina, ricordo l'esistenza di <a href="http://itunes.apple.com/it/app/playmaker-football/id348781059?mt=8" target="_blank">PlayMaker Football per iPhone e iPod touch</a> e, per chi è proprio all'ultimo&#8230; stadio, l'esistenza di <a href="http://goallineblitz.com/" target="_blank">Goal Line Blitz</a>.