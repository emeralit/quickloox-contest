---
title: "Il valore dell'esperienza altrui"
date: 2006-06-17
draft: false
tags: ["ping"]
---

Gli si è rotto il disco rigido LaCie (sostituire a piacere la marca) e ha deciso che vuole cambiare marca. Chiede se qualcuno ha avuto esperienze positive con altri modelli.

Io una volta ho giocato al Superenalotto ma ho fatto uno. Qualcuno ha avuto esperienze migliori e può consigliarmi una ricevitoria più fortunata?