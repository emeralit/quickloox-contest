---
title: "Facce vede iChat"
date: 2005-04-10
draft: false
tags: ["ping"]
---

Quei minieffetti speciali che non te ne accorgi fino a che non fanno una differenza

Come mi ha fatto notare Riccardo, quando si minimizza una finestra di chiacchiere in iChat, la finestra stessa va nel Dock con l'icona del personaggio con cui si chiacchiera. Se il personaggio ha scelto un'icona rappresentativa, anche in un Dock assai affollato sarà molto semplice ritrovarlo.

Uso iChat tutto il giorno ma finora non me ne ero accorto. Improvvisamente, ieri, ha fatto la differenza e mi sono reso conto che a volte nei prodotti Apple ci sono sfumature di interfaccia che non ti aspetti, e semplificano la vita alla tastiera.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>