---
title: "Nel regno del fattibile / 9"
date: 2010-06-13
draft: false
tags: ["ping"]
---

<cite>pregasi elencare azioni fattibili su un iPad e non fattibili su un Sony Vaio W da 10&#8221;</cite>.

Distribuirlo come <a href="http://www.news.com.au/technology/ipad/sydney-restaurant-replaces-menus-with-ipads/story-fn5knrwy-1225874904671" target="_blank">menu in un ristorante</a>. E probabilmente l'ordinazione è pure più veloce. Mi viene in mente quel ristorante cinese di San Francisco, che era <i>veramente</i> cinese e dove il menu era scritto solo in cinese. Toccai con il dito ideogrammi a casaccio e il cameriere, cinese che parlava solo cinese, annotò diligentemente. Il cameriere resta &#8211; iPad non serve a tavola &#8211; e sparisce unicamente l'annotare.