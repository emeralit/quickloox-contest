---
title: "Più trasparenza per tutti"
date: 2009-06-23
draft: false
tags: ["ping"]
---

In Italia predomina l'interesse attorno al Presidente del Consiglio. La stampa, giustamente, pretende trasparenza.

In Inghilterra il quotidiano <i>Guardian</i> ha chiesto l'aiuto dei lettori per <a href="http://mps-expenses.guardian.co.uk/" target="_blank">visionare oltre 700 mila note spese</a> dei parlamentari del Regno.

Stante che la trasparenza sul Presidente del Consiglio è sacrosanta, e stante che in Inghilterra le spese della Regina e del governo sono trasparenti per definizione e tradizione, vorrei l'Inghilterra. :-)