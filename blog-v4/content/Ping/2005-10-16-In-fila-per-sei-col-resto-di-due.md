---
title: "In fila per sei col resto di due"
date: 2005-10-16
draft: false
tags: ["ping"]
---

Il gusto di usare un sistema in modi non ortodossi

Non fare domande. Mi è toccato di creare, al volo, un gioco enigmistico. Scrivere una frase su più righe, prendere ogni colonna di caratteri, mescolare la successione delle colonne e chiedere al lettore di rimettere tutto a posto per ricostruire la frase originale.

Normalmente i giochi enigmistici si realizzano con programmi appositi, appoggiati a dizionari. Questa era una eccezione; sporco, maledetto e subito.

Pages. La gestione delle tabelle di Pages è molto pratica ed efficiente. Una frase scritta su più righe, di cui vogliamo mischiare le colonne, è nient'altro che una tabella con una lettera per casella.

In più Pages pesa niente rispetto a elefanti come Word o Excel.

Una volta di più mi ha sorpreso. Positivamente.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>