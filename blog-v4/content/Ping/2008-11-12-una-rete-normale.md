---
title: "Una rete normale"
date: 2008-11-12
draft: false
tags: ["ping"]
---

Normalità nel mondo Windows: un <em>trojan</em> per Windows <a href="http://news.bbc.co.uk/2/hi/technology/7701227.stm" target="_blank">trafuga mezzo milione di conti bancari</a> e carte di credito.

Se visiti la pagina puoi notare in apertura la foto di un Pc.

In origine la foto era diversa e raffigurava un PowerBook 12&#8221; con tanto di schermo nastrato e scritte PERICOLO, come <a href="http://macdailynews.com/index.php/weblog/comments/18959/" target="_blank">mostra MacDailyNews</a>.

Solo che non si trattava di un articolo generico sulla sicurezza. Il <em>trojan</em> funziona specificamente su Windows e si potrebbe anche argomentare che un Mac moderno può fare funzionare Windows in modo nativo, ma un PowerBook 12&#8221; proprio no, a meno di penalizzanti emulazioni che di fatto oramai nessuno usa più, salvo eccezioni che non riguardano certo la navigazione su Internet.

Qualcuno ha fatto sentire la propria voce presso la rete inglese attraverso il <a href="http://news.bbc.co.uk/newswatch/ifs/hi/newsid_4000000/newsid_4000500/4000545.stm" target="_blank">modulo per l'invio di feedback e opinioni</a>.

E la foto è cambiata, come appunto si vede nella edizione finale dell'articolo. Adesso raffigura un soggetto pertinente alla notizia.

Adesso pensa alla Rai e se sarebbe mai successa una cosa del genere.

Grazie a <a href="http://ipodpalace.com" target="_blank">Stefano</a> per la segnalazione!