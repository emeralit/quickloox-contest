---
title: "Cad non da Cd"
date: 2006-09-06
draft: false
tags: ["ping"]
---

Uno dei programmi che non hanno conquistato l'ingresso sul Cd di Macworld ottobre è <a href="http://brlcad.org/" target="_blank">Brl-Cad</a>.

Le ragioni contro sono che è un programma per X11; che richiede una procedura di installazione da Terminale; e che sono cinquanta mega e passa.

Le ragioni pro però erano notevoli. È un modellatore solido totalmente open source usato nientepopodimeno che dall'esercito americano. L'interfaccia è aliena, ma per un professionista è ottima. E bisogna passare dal Terminale, ma si installa e funziona al primo colpo. Al solito: ce l'ho fatta io, per cui è una cosa normale.

Quando ti dicono che mancano i programmi per Mac, poi scopri che esiste un modellatore solido gratuito di livello professionale. E scommetterei tranquillamente sul fatto che nessuno, su Macworld o altrove, ne ha mai parlato.