---
title: "Cassetti senza sogni"
date: 2004-11-25
draft: false
tags: ["ping"]
---

Computer fatti per sognare, altri per…

Mi scrive Andrea Fistetto di Manduria. No comment.

<cite>Venerdì sera, fine settimana, treno regionale pieno di gente di ritorno a casa, io e il mio PowerBook 15&rdquo;.</cite>

<cite>Vicino a me, manager con Pc portatili delle marche più disparate. Una cosa che ho notato e che nessuno ha mai fin adesso sottolineato è il lettore ottico <em>slot in</em> del PowerBook… tutti gli altri sono con i cassettini…</cite>

<cite>Uno dei suddetti signori alle prese con l'inserimento di un cd è stato sorpreso da un sobbalzo del treno e <em>crack</em>! è rimasto con il cassettino del cd in mano, subito santi dal cielo… io nello stesso momento stavo mettendo un film in dvd nel mio, di lettore… nessun problema…</cite>

<cite>I Mac sono gli unici che ad oggi (e da un anno ormai) hanno solo portatili con lo slot in… perché?</cite>

<cite>Perché sono esseri umani che li usano, quindi devono essere pratici e magari belli… e se sobbalza il treno non si rompono…</cite>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>