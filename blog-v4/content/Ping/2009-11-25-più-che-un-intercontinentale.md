---
title: "Più che un'intercontinentale"
date: 2009-11-25
draft: false
tags: ["ping"]
---

Il radiotelescopio di Arecibo, a 35 anni di distanza dal celebre messaggio concepito dagli astronomi Frank Drake e Carl Sagan, pochi giorni fa ha inviato nello spazio un altro messaggio. Ecco <a href="http://www.centauri-dreams.org/?p=10283" target="_blank">come</a> (altri dettagli <a href="http://www.centauri-dreams.org/?p=10346" target="_blank">in un post successivo</a>):

<cite>Il</cite> coder <cite>di Arecibo (un apparecchio che interrompe con intervalli precisi il segnale radar da due megawatt) era inattivo. Il suo uso sarebbe stato la maniera più ovvia di inserire dati destinati alla riflessione nello spazio da parte del radar di Arecibo. Serviva una soluzione creativa. [&#8230;] abbiamo convertito la sequenza proteica in un file audio analogico, che abbiamo registrato sul mio iPhone. Poi abbiamo interfacciato iPhone con il radar e abbiamo trasmesso approssimativamente dalle 23:30 alle 00:45. La durata di ciascuna emissione è stata approssimativamente cinque volte più lunga della trasmissione di Sagan e Drake del 1974.</cite>

<cite>Ora ho l'iPhone più fico del mondo.</cite>

E dagli torto. L'unico che possa dire di avere &#8220;chiamato&#8221; Kappa Ceti.