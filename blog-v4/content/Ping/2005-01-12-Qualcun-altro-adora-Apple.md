---
title: "Qualcun altro adora Apple"
date: 2005-01-12
draft: false
tags: ["ping"]
---

Se c'è gente soddisfatta degli annunci di Macworld Expo, ci sarà un motivo

Scrive Jakaa:

<em>Ho 17 anni e adoro Apple. Il perché? Sono le 22.31 di martedì 11 gennaio 2005, giornata clou del Macworld Expo, e ho appena visitato il sito ufficiale di Apple. Consiglio a tutti di farlo, sono rimasto senza parole… buon lavoro e tanti saluti.</em>

Ricambio di cuore e penso che tanto entusiasmo sia una cosa positiva. Vuol dire che il verso delle cose è quello giusto.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>