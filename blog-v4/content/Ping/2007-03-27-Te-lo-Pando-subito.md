---
title: "Te lo Pando subito"
date: 2007-03-27
draft: false
tags: ["ping"]
---

<a href="http://multifinder.wordpress.com" target="_blank">Mario</a> mi ha mostrato una alternativa intelligente al problema dello scambio di file molto grossi: Pando.

È gratis se si accettano i banner, a pagamento per il servizio premium. Un vantaggio decisivo sugli altri concorrenti di questo tipo: è basato su tecnologia P2P.

In pratica, riesci a scaricare intanto che il mittente sta ancora caricando il file su Internet. In questo modo i tempi di trasferimento si avvicinano, anche se non possono assolutamente essere uguali, a quelli di una sessione <code>sftp</code>.

E l'uso è semplicissimo, più di quanto abbia visto finora. Raccomandato.