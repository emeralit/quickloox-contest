---
title: "Tutte insieme appassionatamente"
date: 2009-06-28
draft: false
tags: ["ping"]
---

A un certo punto era aperto un buon numero di finestre di Safari, nella <i>beta</i> di Snow Leopard. Ho voluto eternare il momento.

Chi avesse pazienza le può anche contare. Io ho rinunciato.