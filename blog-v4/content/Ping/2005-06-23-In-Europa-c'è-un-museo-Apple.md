---
title: "In Europa c'è un museo Apple"
date: 2005-06-23
draft: false
tags: ["ping"]
---

Se n'è accorta perfino Apple. A Cupertino

Giovedì scorso un senior manager di Apple ha telefonato da Cupertino, California, alla sede dell'<a href="http://www.allaboutapple.com/museo/museo.htm">All About Apple Museum</a>, primo museo Apple in Europa, per congratularsi e chiedere un contatto per poter spedire materiale.

I ragazzi del club omonimo non volevano crederci, ma la chiamata è rimasta registrata in segreteria e hanno dovuto arrendersi all'evidenza. Anche il file audio della telefonata è diventato parte del patrimonio museale.

Hanno messo in piedi una cosa dannatamente bella, che ora è diventata dannatamente seria.

Se tanto mi dà tanto, tempo due anni e arriva Steve Jobs in persona in visita a Quiliano, Savona.

Precediamolo.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>