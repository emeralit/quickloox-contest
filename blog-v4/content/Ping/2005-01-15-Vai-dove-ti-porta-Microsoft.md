---
title: "Vai dove ti porta Microsoft"
date: 2005-01-15
draft: false
tags: ["ping"]
---

Software che ti guida passo per passo? Ecco quello giusto

È notorio che Microsoft non possa accettare l'idea di non essere monopolista in qualcosa, per cui offre anche <a href="http://mappoint.msn.com/DirectionsFind.aspx">mappe e itinerari</a>.

Mi segnala l'amico Arnaldo:

<em>Come città di partenza metti Haugesund in Norvegia</em>
<em>Come destinazione metti Trondheim, Norvegia</em>

<em>Ora calcola il percorso cliccando su &ldquo;Get Directions&rdquo;…</em>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>