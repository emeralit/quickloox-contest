---
title: "La nuova stella di David"
date: 2010-05-16
draft: false
tags: ["ping"]
---

David Hockney, settantaduenne artista inglese, ha smesso di <a href="http://www.macworld.it/ping/nomi/2009/05/04/quando-arte-la-vedi/" target="_self">dipingere su iPhone</a>.

La ragione è che <a href="http://www.thisislondon.co.uk/standard/article-23832611-david-hockney-swaps-a-sketch-pad-for-ipad.do" target="_blank">è passato a iPad</a>. Così Hockney spiega in poche parole ciò che per un esperto di interfaccia vale mezza giornata di consulenza:

<cite>È la cosa più vicina che io abbia mai visto a quella che chiamerei una macchina universale. […]</cite>

<cite>iPhone metteva in relazione mano e orecchio, iPad mano e occhio. […]</cite>

<cite>Un altro aspetto unico è che uno può guardare come in un film la nascita del proprio disegno. Non mi ero mai guardato disegnare, prima.</cite>