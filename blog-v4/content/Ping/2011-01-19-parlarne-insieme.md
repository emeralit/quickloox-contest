---
title: "Parlarne insieme"
date: 2011-01-19
draft: false
tags: ["ping"]
---

Adesso che è partito Mac App Store, si sente la mancanza di una funzione di prova gratuita a tempo. Sono programmi complessi e costosi, che impegnano, e pare giusto che uno possa avere bene idea di che cosa si accinge a installare su disco.

Portato su App Store, il discorso vale molto meno. I programmi sono semplici e costano poco. Ho sempre detto, su App Store, di giudicare i programmi da come è fatto il sito dei produttori e se i produttori danno retta.

Sperimentato e confermato in queste settimane con <a href="http://itunes.apple.com/it/app/id383577124?mt=8" target="_blank">Textastic</a> e <a href="http://itunes.apple.com/it/app/drums-of-war/id384086275?mt=8" target="_blank">Drums of War</a>. Il primo è un <i>editor</i> di testo, il secondo un programma per giocare a Dungeons &#38; Dragons. In comune il fatto che i programmi erano buoni, ma alcune cose andavano sistemate.

Ho inviato <i>feedback</i> chiedendo certe modifiche e certe aggiunte. Non conta però il mio intervento. Conta che il programmatore autore di <a href="http://www.textasticapp.com/" target="_blank">Textastic</a> abbia un sito adatto al supporto e alla gestione dei <i>bug</i>, oppure che l'autore di <a href="http://www.drumsofwarapp.com/" target="_blank">Drums of War</a> sia attivo su Twitter e non solo abbia risposto alle mie sollecitazioni, ma che si sia mosso lui per avvisarmi di quello che aveva fatto, in un momento dove non riuscivo a occuparmi di una cosa tutto sommato secondaria.

Ora Textastic è a un livello superiore, con l'integrazione di Dropbox, maggiore rispetto delle impostazioni di sistema e un sacco di altre cose. Se prima lo raccomandavo, ora lo raccomando il doppio. Drums of War ha compiuto grandi progressi e durante le sessioni di gioco risulta molto più utile ed efficace di prima (se un programma per iPhone non sta dietro al tempo reale, di qualsiasi cosa, vale poco).

Una prova gratuita avrebbe evidenziato certi difetti e forse magari i programmi neanche li avrei comprati. Invece non avrebbe mostrato niente dell'intenzione dei programmatori di fare crescere le loro creazioni e dare ascolto agli acquirenti, variabile che sta dando valore crescente al mio &#8211; piccolo &#8211; investimento. Cosa che su Mac conta relativamente meno, a causa della maggiore complessità dei programmi e della struttura di chi li produce.

Ecco: su Mac App Store vogliamo una prova gratuita. Su App Store la cosa migliore è guardare che gente c'è dietro

