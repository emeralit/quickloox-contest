---
title: "Fiducia in Apple(Script)"
date: 2007-11-09
draft: false
tags: ["ping"]
---

Ma non è che iPhone li ha distratti da Mac OS X? E se fossero più interessati a vendere iPod che a vendere Mac? La qualità dell'hardware sta salendo, sta scendendo o varie ed eventual?

Per quanto mi riguarda, quando vedo cose come il nuovo <a href="http://www.apple.com/applescript/" target="_blank">sottosito su AppleScript</a>, che non veniva spolverato dal Pleistocene, sto (più) tranquillo.