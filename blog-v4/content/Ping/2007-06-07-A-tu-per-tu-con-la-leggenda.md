---
title: "A tu per tu con la leggenda"
date: 2007-06-07
draft: false
tags: ["ping"]
---

Sono fresco reduce da una cena in compagnia di Dave Winer. Come ha esemplificato più che efficacemente <strong>Mario</strong>, <cite>senza di lui non avremmo <a href="http://www.opencommunity.co.uk/vienna2.html" target="_blank">Vienna</a>. E rimarremmo al buio</cite>.

Dal vivo, il <a href="http://www.scripting.com/" target="_blank">pioniere di blog e Rss</a> è un grande e instancabile conversatore, curioso e vivace. Tiene anche fede al suo cognome, pardon, nome. :-)