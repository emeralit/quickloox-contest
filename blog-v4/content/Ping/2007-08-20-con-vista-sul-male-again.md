---
title: "Con Vista sul male (again)"
date: 2007-08-20
draft: false
tags: ["ping"]
---

Scrivevo diversi mesi fa su Macworld carta:

<cite>Se [il film riprodotto su Vista] è protetto, o se non viene ritenuto autentico, e se il componente video del Pc non è all'altezza, il video verrà intenzionalmente degradato.</cite>

Sono stato criticato, anche su questo blog, e le mie fonti sono state ugualmente criticate in giro per Internet. Praticamente stavo raccontando una balla o ero male informato.

Mesi dopo, PcWorld scrive <a href="http://www.pcworld.com/article/id,135814-c,windowsbugs/article.html" target="_blank">le stesse cose</a>, pari pari, dopo una presentazione tenuta nel corso dello Usenix Security Symposium una decina di giorni fa.

Le mie fonti non erano poi cos&#236; malvagie.