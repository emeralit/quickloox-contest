---
title: "Due lezioni in una spremuta"
date: 2004-12-16
draft: false
tags: ["ping"]
---

Più tante utility, gratis, con buoni motivi

<strong>BackLight</strong> fa una cosa che ho visto fare a quindici altri programmi: trasforma il salvaschermo in uno sfondo scrivania. Controllare la funzione tramite un menu extra, però, l'ho visto fare solo a BackLight. Che è freeware.

Detto tra parentesi che se il tuo Mac non supporta Quartz Extreme non è proprio il caso, ho visto poche utility semplici ed eleganti come BackLight. Poi vado sul sito e scopro un sacco di altre utility, grafica giusta come BackLight, tanto buon <a href="http://freshlysqueezedsoftware.com/products/freeware">freeware</a> e un paio di ragionamenti assai sensati.

Più la piccola ma azzeccata trovata del software appena spremuto. Per sostenere che manca bel software per Mac bisogna proprio stare eccessivamente lontani da Internet.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>