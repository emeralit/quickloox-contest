---
title: "Esce d'aprile"
date: 2011-01-13
draft: false
tags: ["ping"]
---

Nel mio libriccino <a href="http://www.tilibri.com/libri/tutti_pazzi_per_ipad.html" target="_blank">Tutti pazzi per iPad</a> accennai a <a href="http://www.thinkgeek.com/electronics/retro-gaming/e762/?icpg=Carousel_iCade_1" target="_blank">iCade</a>, iPad Arcade Cabinet, una ricostruzione formato iPad dei vecchi videogochi da bar, realizzata come pesce d'aprile. Difatti iPad non era neanche in vendita.

Adesso Ion lo ha creato veramente, in collaborazione con Atari, e lo ha dimostrato al Consumer Electronics Show 2011 di Las Vegas. Da pesce d'aprile a prodotto autentico non capita a tutti e testimonia che un certo impatto iPad lo ha proprio causato.

Lo si può trovare indicato come <i>presto in arrivo</i> sulla <a href="http://www.ionaudio.com/ces2011" target="_blank">pagina delle proposte Consumer Electronics Show</a>. Da guardare perché, iCade a parte, presenta una carrellata di periferiche per iPhone e iPad da lustrarsi le orecchie.

S&#236;, Ion lavora nell'audio.

