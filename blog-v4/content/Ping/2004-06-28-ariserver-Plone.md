---
title: "Facciamo nascere un Mug online"
date: 2004-06-28
draft: false
tags: ["ping"]
---

C’è da guadagnarci, in possibilità di imparare e nuovi amici

Mi ha scritto il gentilissimo Carlo Gandolfi ringraziandomi per la segnalazione del server Plone <link>FreeSmug</link>http://freesmug.mine.nu/ dedicato all’approfondimento delle tematiche inerenti software libero e open source.

Il sito intende diventare centro di raccolta di un Macintosh User Group ufficiale regolarmente registrato presso Apple e come tale, avendo sede virtuale e non fisica, abbisogna di un certo numero di utenti registrati prima di poter fare la richiesta ufficiale.

Se la cosa interessa, registrati (io l’ho fatto, per esempio). Ci sono vantaggi come quello di disporre di un account sul sistema Plone-based del sito e naturalmente il fatto di restare aggiornato sugli sviluppi, oltre che poter imparare cose in più e conoscere gente nuova e potenzialmente interessante.

Un gruppo Mac in più fa sempre bene. :-)

<link>Lucio Bragagnolo</link>lux@mac.com