---
title: "Il piacere della lettura"
date: 2010-03-04
draft: false
tags: ["ping"]
---

Attenzione a una nuova tendenza che inizia a farsi largo. Si parla tanto di libri elettronici, negozi per la vendita dei libri elettronici, problemi dovuti alle protezioni anticopia per i libri elettronici bla bla bla.

Ma stanno nascendo realtà che consentono di leggere e organizzare libri elettronici senza neanche scaricare niente, in forma di applicazione web. Qualunque sia l’apparecchio, ci si collega a un sito ed è già tutto pronto. Sugli apparecchi che lo consentono – come iPhone e iPod touch e certamente iPad – si può usare l’applicazione per leggere <i>offline</i>, senza collegamento.

Esempi: <a href="http://dbelement.com/apps/reader" target="_blank">Reader</a> e <a href="http://ibisreader.com/" target="_blank">Ibis Reader</a>.

Si fa sempre più interessante e c’è un sacco da imparare. Manca il fruscio della carta, sì, l’ho capito. Non è un <i>invece</i>… è un <i>in più</i>.