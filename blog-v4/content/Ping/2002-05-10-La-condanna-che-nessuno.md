---
title: "La condanna che nessuno dice"
date: 2002-05-10
draft: false
tags: ["ping"]
---

Se io dicessi che Microsoft è un’azienda colpevole di pirateria del software, ci crederesti?

Il 27 settembre 2001 il tribunale di Nanterre, in Francia, ha multato Microsoft per tre milioni di franchi per avere illegalmente incluso nel programma Softimage codice che era proprietà di un’altra azienda.
Tuttavia la notizia, dopo essere stata pubblicata sul giornale francese Le Monde Informatique e su PcWorldMalta (!), è stata completamente insabbiata da tutti i mezzi di informazione, dentro e fuori dal Web.
Oltre ad abusare della propria posizione dominante (ed essere condannata in tribunale per questo), Microsoft commette anche pirateria di software. E viene condannata.
Quando la grande utenza capirà che Office non è indispensabile alla vita sulla Terra, forse Microsoft comincerà a comportarsi più correttamente. Quando si comincia?

<link>Lucio Bragagnolo</link>lux@mac.com