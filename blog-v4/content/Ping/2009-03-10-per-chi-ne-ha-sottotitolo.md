---
title: "Per chi ne ha sottotitolo"
date: 2009-03-10
draft: false
tags: ["ping"]
---

<a href="http://freesmug.org" target="_blank">Gand</a> ha lanciato una bella iniziativa: la sottotitolazione collettiva in italiano dei filmati introduttivi alle applicazioni Apple, come quelle di iLife '09 o iWork, che sono solo in inglese e qualcuno può fare più fatica del lecito.

Chiunque si può registrare liberamente sul sito e dare il proprio contributo. Magari tradurre una singola frasetta, magari coprire un filmato intero; è uno di quei casi in cui <em>veramente</em> un sassolino da parte di ciascuno genera la montagna.

Il sito internamente è in inglese, ma non può essere un problema; evidentemente tutta l'operazione è rivolta a persone in grado di ascoltare l'audio inglese del filmato originale Apple, capirlo e saper formulare una traduzione valida. Il sapersi orientare su <a href="http://macvideo.wikidot.com/" target="_blank">Mac Video Tutorial</a> è il minimo sindacale per contribuire.

Quelli che possono, che aspettano ancora? :-)