---
title: "Due anni ad avere ragione"
date: 2004-12-17
draft: false
tags: ["ping"]
---

Come al solito, il resto del mondo si mette in coda

Circa diciotto mesi fa i massimi dirigenti di Intel sostenevano che non ci sarebbe stato bisogno di processori a 64 bit in sistemi desktop circa fino al 2008.

Settimana scorsa hanno dichiarato che inizieranno a fornirli nel 2005.

Nel frattempo una oscura aziendina di Cupertino, California, vende computer desktop a 64 bit da un paio di anni.

E c'è ancora gente che fa paragoni.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>