---
title: "Elogio dell'arretratezza"
date: 2004-10-01
draft: false
tags: ["ping"]
---

Nel mondo Mac la gallina vecchia continua a fare buon brodo

Lo confesso. Il mio Mac attuale l'ho comprato usato, vecchio di un anno, per quanto in buone condizioni.

Da quando lo uso ha lavorato (troppo) per altri due anni, è caduto due volte, ma fa tutto quello che mi serve senza perdere un colpo. Tre giorni fa ho riavviato per via dell'ultimo update di Java e da allora non lo spengo; più o meno starà acceso senza riavviarsi mai per altre tre o quattro settimane, aggiornamenti esclusi.

In questo momento ho aperti trenta programmi, sono su Internet via wireless e perfino in poltrona, perché un portatile consente questi e altri lussi.

Ogni tanto mi chiedono perché non cambio computer, che è così vecchio.

Fa esattamente tutto quello che mi serve, e lo cambierò (all'istante) solo quando smetterà di farlo.

Giusto per dire che un Titanium 500 è tuttora una macchina usabile e professionale. Tutti quelli che conosco e hanno comprato, per così dire, altre marche, sono già stati forzati ad aggiornarsi da un bel po'…

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>