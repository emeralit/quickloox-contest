---
title: "Nel regno del fattibile / 13"
date: 2010-08-05
draft: false
tags: ["ping"]
---

<cite>pregasi elencare azioni fattibili su un iPad e non fattibili su un Sony Vaio W da 10&#8221;</cite>.

<a href="http://9to5mac.com/bring_me_wine_now?utm_source=feedburner&amp;utm_medium=feed&amp;utm_campaign=Feed%3A+9To5Mac-MacAllDay+%289+to+5+Mac+-+Apple+Intelligence%29" target="_blank">Consultare la carta dei vini</a> in un ristorante di alta classe, il South Gate situato al 154 di Central Park South tra la Sesta e la Settima Avenue di New York.

Il tutto governato dalla app SmartCellar di Incentient, da noi sfortutamente non disponibile. C'è Cadent wineCellar, 1,59, che sembra eccellente a dispetto del nome dell'azienda produttrice.