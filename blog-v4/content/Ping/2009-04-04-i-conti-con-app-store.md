---
title: "I conti con App Store"
date: 2009-04-04
draft: false
tags: ["ping"]
---

Avevo riferito giorni fa di come venisse annunciato con un certo compiacimento il <a href="http://www.macworld.it/blogs/ping/?p=2234" target="_self">rallentamento delle nuove applicazioni su App Store</a>, prima che marzo fosse terminato.

Marzo è terminato e il rallentamento c'è stato: le nuove applicazioni di marzo sono state 5.539 contro le 5.897 di febbraio, il sei percento in meno.

Era successo anche a ottobre: 2.658 applicazioni nuove contro le 2.944 di settembre (-9,7 percento), ma nessuno l'aveva messa giù cos&#236; dura.

Per curiosità, le applicazioni totali a fine marzo sono 31.141. O <a href="http://148apps.com/10000/" target="_blank">cos&#236; dice 148Apps</a>.