---
title: "Divertirsi in compagnia"
date: 2008-03-27
draft: false
tags: ["ping"]
---

Ho pensato al Ping per il primo di aprile e mi sono abbastanza divertito. Vuoi scriverne uno anche tu? <a href="mailto:lux@mac.com">Mandamelo</a>. Se ne arrivano, li pubblico. :)
