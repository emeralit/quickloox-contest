---
title: "Poi dicono che sono equivalenti"
date: 2007-05-29
draft: false
tags: ["ping"]
---

Compulsando <a href="http://www.freeciv.org/wiki/Download" target="_blank">la pagina di download</a> del sito di FreeCiv, vedo questo messaggio accoppiato alla versione Windows del programma:

<cite>Please note that these [files] do not contain a Trojan horse. Your virus checker is wrong about this.</cite>

Mi vengono in mente <a href="http://www.campanile.it/" target="_blank">Campanile</a>, <a href="http://en.wikipedia.org/wiki/Euge%CC%80ne_Ionesco" target="_blank">Ionesco</a> e i <a href="http://it.wikipedia.org/wiki/Fratelli_Marx" target="_blank">fratelli Marx</a>, insieme a commedia dell'arte, sceneggiata napoletana e farse plautine.

Su Mac OS X c'è molto da migliorare e nessuno ne discute. Avvisare la gente che il tuo programma non è un virus checché ne dica l'antivirus, però, non l'ho proprio mai visto.

E poi Windows e Mac OS X oramai sarebbero equivalenti.