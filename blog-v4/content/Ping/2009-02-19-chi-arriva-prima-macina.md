---
title: "Chi arriva prima macina"
date: 2009-02-19
draft: false
tags: ["ping"]
---

Cos&#236; dicevano le nonne, quando per avere la farina bisognava andare al mulino con la materia prima grezza.

Nokia <a href="http://www.macworld.it/showPage.php?template=notizie&amp;id=16438" target="_self">apre a maggio</a> il proprio <em>store online</em> di applicazioni. Orange <a href="http://www.macworld.it/showPage.php?template=notizie&amp;id=16439" target="_self">fa la stessa cosa</a>. Sony Ericsson prova a farsi <a href="http://www.macworld.it/showPage.php?template=notizie&amp;id=16435" target="_self">il proprio iTunes</a>.

iTunes Store è l&#236; dal 2003 e oggi raccoglie 75 milioni di <em>account</em> paganti. App Store è nato a luglio e ha quasi ventimila applicazioni, scaricate più di mezzo miliardo di volte.

Ho l'impressione che il mulino stia macinando già da un po'. Peccato per chi è arrivato tardi.