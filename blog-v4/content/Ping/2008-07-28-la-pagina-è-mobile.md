---
title: "La pagina è mobile"
date: 2008-07-28
draft: false
tags: ["ping"]
---

Voglio provare a vedere se riesco a parlare ogni luned&#236; di qualcosa di specifico per iPod touch, che sta diventando a tutti gli effetti una piattaforma <em>hardware</em> e <em>software</em> gemella e parallela a Macintosh.

SafariMobile è il meglio che c'è al mondo per visionare pagine web su schermi piccoli, solo che a volte la pagina è talmente elaborata e pesante che si fa fatica lo stesso.

In questi casi si può approfittare del fatto che Google ha una pagina apposta per tradurre le pagine web in versione più appropriata per lo schermo di iPod touch.

Si crea un bookmarklet su Safari Mobile che contiene questo codice:

<code>javascript:location.href='http://www.google.com/gwt/n?u='+encodeURIComponent(location.href);</code>

Registrato tutto ciò nei <em>bookmark</em> (una volta per tutte), si carica normalmente la pagina che vogliamo vedere. A caricamento iniziato, appena il suo Url compare nel campo testo di Safari, andiamo nei bookmark e azioniamo il nostro <em>bookmarklet</em> appena creato.

La pagina verrà filtrata dal servizio di Google e dovrebbe arrivare più leggibile e anche in meno tempo.

Se il sito ha già una propria versione mobile ottimizzata, il <em>bookmarklet</em> è abbastanza astuto da non filtrare la pagina originale ma caricare direttamente la versione <em>mobile</em>.

iPod touch è davvero uno splendido computer da tasca.