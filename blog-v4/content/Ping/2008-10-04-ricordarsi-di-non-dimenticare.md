---
title: "Ricordarsi di non dimenticare"
date: 2008-10-04
draft: false
tags: ["ping"]
---

In quest'epoca priva di memoria è ammirevole il festeggiamento di Google, che celebra i suoi primi dieci anni con la ripubblicazione - per un mese - del suo <a href="http://www.google.com/search2001.html" target="_blank">indice di gennaio 2001</a>.

Prova a cercare <em>iPod</em>, <em>iPhone</em>, <em>MobileMe</em>, <em>Apple Tv</em> o <em>iWork</em> (parlo di Apple, ma vale per un mucchio di altre cose, per esempio <em>Firefox</em> o <em>WebKit</em> o <em>Ubuntu</em> o <em>web 2.0</em>, o <em>Ruby</em> e chi più ne ha).

Dedicato a tutti quelli che una volta era diverso e la tecnologia non è più quella di una volta e non ci sono più rivoluzioni. Gli smemorati, insomma. :)