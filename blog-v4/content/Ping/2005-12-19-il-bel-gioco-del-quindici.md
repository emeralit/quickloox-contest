---
title: "Il bel gioco del Quindici"
date: 2005-12-19
draft: false
tags: ["ping"]
---

Il momento di cambiare computer, se è Mac, è un po' speciale

Scrive <strong>Flavio</strong>:

<cite>mi permetto di inviare a te la mia prima mail col nuovo PowerBook 15 alluminio.</cite>

<cite>Non riesco a capacitarmi. Mia moglie mi ha trovato alla scrivania in mansarda, bocca aperta in una evidente forma avanzata di catalessi. In realtà stavo cercando di capire come  
caspita avevo fatto a ritrovarmi in rete senza aver fatto nulla. Giuro: nulla!</cite>

<cite>Eppure ero in Internet col mitico Safari. Il primo sito visitato dopo <a href="http://www.loscrittoio.it">Lo Scrittoio</a> è stato <a href="http://www.apple.com">Apple</a> naturalmente, in segno di omaggio per aver prodotto un oggetto assolutamente meraviglioso.</cite>

<cite>Adesso mollo la presa per non tediarti. Continuo a bearmi e crogiolarmi con un prodotto assolutamente fantastico.</cite>

Che dire altro, alla vigilia di Natale? Certi regali non sono da tutti. Mica per questione di soldi. Per la soddisfazione non esistono tassi zero.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>