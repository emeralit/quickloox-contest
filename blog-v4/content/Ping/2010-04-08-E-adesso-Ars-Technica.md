---
title: "E adesso Ars Technica"
date: 2010-04-08
draft: false
tags: ["ping"]
---

Dopo la <a href="http://www.macworld.it/ping/soft/2010/04/08/da-leggere-e-da-guardare/" target="_self">recensione di iPad di John Gruber</a>, è apparsa quella di <a href="http://arstechnica.com/apple/reviews/2010/04/ipad-review.ars" target="_blank">Ars Technica</a>.

È immensamente lunga, diciotto pagine. Nel contempo evidenzia alla perfezione pregi, difetti, potenzialità e limiti dell'apparecchio.

È &#8211; ripeto &#8211; lunghissima, diciotto pagine. Lo ripeto ancora. Diciotto pagine.

Ciononostante va letta, tutta, con attenzione. Prendersi una serata, un <i>weekend</i>, stamparla, quello che serve. Chi ha bisogno di aiuto su qualche passaggio, chieda, farò il possibile.

Capitasse un so-tutto-io che snocciola l’ennesimo parere da aperitivo, gli va chiesto con gentilezza se ha letto la recensione ed eventualmente sondarne la sincerità (<i>che ne dici in particolare di quel pezzo dove scrivono&#8230;?</i>, cos&#236; si vede se l'ha letta veramente o sta facendo il brillante).

Uno che non ha letto questa recensione e pontifica, da oggi si qualifica come parolaio da tenere in nessun conto e compatire per pochezza.

Riporto un paragrafo della conclusione di Ars Technica.

<cite>In verità questo apparecchio è uno di quelli che può essere capito solo provandolo di persona. Non importa quante parole vengono versate su iPad, davvero non c'è un modo semplice per descrivere l'esperienza e come essa sia differente da quella di un tipico computer o smartphone. Quelli del nostro staff che erano più scettici su iPad prima di averlo preso in mano ne hanno avuto una comprensione estremamente diversa dopo averci passato sopra una seria quantità di tempo. Probabilmente succederà lo stesso alla maggioranza degli utenti.</cite>