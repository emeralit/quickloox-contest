---
title: "Non li fermeremo sul bagnasciuga"
date: 2008-04-13
draft: false
tags: ["ping"]
---

Molti complimenti agli appassionati del <a href="http://www.calmug.org/" target="_blank">Calabria Macintosh Users Group</a>, che sono in prima linea nel progetto per <a href="http://www.macworld.it/showPage.php?template=notizie&amp;id=14627">dotare di wireless le spiagge di Reggio Calabria</a>.

Pensando che a Milano bisogna tuttora sperare di trovare la base aperta per sbaglio o il negozio con persone illuminate, è una conferma che il progresso non ha niente a che vedere con la latitudine.