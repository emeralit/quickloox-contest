---
title: "Sentite in negozio"
date: 2008-02-22
draft: false
tags: ["ping"]
---

<cite>Buongiorno, siamo una piccola azienda di una decina di persone e volevamo comprare due iMac per i grafici, un paio di MacBook Pro per i commerciali e un MacBook Air per le presentazioni</cite>.

Della serie: c'è chi ha capito MacBook Air.

<cite>Buongiorno&#8230; ma che bello, è quello nuovo sottile? Ma che bello&#8230;</cite> (tasteggia). <cite>È anche veloce&#8230;</cite> (soppesa) <cite>è veramente leggero. Non pensavo&#8230;. Ce lo avete già disponibile?</cite> S&#236;. <cite>Quanto costa?</cite> 1.699 euro.

(silenzio)

<cite>Beh&#8230; certo che però potevano mettergli la Ethernet&#8230; mi sa che per me il disco è troppo piccolo&#8230; poi mi serve uno schermo più grande&#8230; vabbeh, arrivederci</cite> (esce, aria delusa).

Della serie: gli svantaggi di MacBook Air sono molto, molto relativi.