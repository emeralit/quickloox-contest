---
title: "For Your Mails Only"
date: 2007-01-25
draft: false
tags: ["ping"]
---

In tutt'altre faccende affacendato, ho scovato un sistema per cifrare, e anche in modo rispettabile, la posta in partenza e in arrivo da Gmail, con un sistema di cifratura a chiave pubblica sviluppato in&#8230; JavaScript.

L'affare è disponibile come <a href="http://www.langenhoven.com/code/emailencrypt/gmailencrypt.php" target="_blank">script</a> dell'estensione <a href="http://greasemonkey.mozdev.org/" target="_blank">GreaseMonkey</a> per Firefox, o <a href="http://www.langenhoven.com/code/encryptthis/encryptthis.php" target="_blank">estensione</a> per Firefox <em>tout court</em>, o in modo forse più dimostrativo ma comunque funzionale come pagina web <a href="http://www.langenhoven.com/code/emailencrypt/standalone.php" target="_blank">a sé</a>. La mia chiave pubblica, a questo punto tanto vale, è <code>pub:4263653:43</code>.

Fuori dall'attualità, è un bel pezzettino di informatica degli anni Duemila: ingegnoso, robusto, semplice, scriptato. Sempre più conta avere le idee <em>e</em> saperle mettere in pratica, perché gli strumenti a disposizione sembrano superare veramente qualsiasi limitazione. Non realtà non è vero&#8230; ma fa impressione lo stesso.