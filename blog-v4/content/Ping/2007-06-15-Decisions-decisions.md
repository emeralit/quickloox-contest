---
title: "Decisions, decisions"
date: 2007-06-15
draft: false
tags: ["ping"]
---

Ho trovato un minuto per sperimentare ed effettivamente Safari 3 beta fa a pugni con il plugin Pdf di Adobe.

Cos&#236; ho tolto il plugin da <code>/Library/Internet Plug-ins</code>. Cosa che tutto sommato avrei dovuto fare da tempo anche con Safari 2.

Adesso la beta non crasha proprio più e ho provato tutti i siti che strapazzo quotidianamente, senza altri problemi che non quelli già riferiti.