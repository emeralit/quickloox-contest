---
title: "La guerra di Piero"
date: 2003-04-09
draft: false
tags: ["ping"]
---

Un aiuto per gli ipovedenti che completa la dotazione di Mac OS X

Il mio amico Piero vede poco ed è contentissimo delle funzioni di Mac OS X pensate per i portatori di handicap. Con una eccezione: ha bisogno di un cursore più grande di quello standard, cosa che Mac OS X non fornisce..

Finalmente ha trovato <link>Mighty Mouse</link>http://www.unsanity.com/haxies/mightymouse/ di Unsanity, che costa solo dieci dollarucci e funziona esattamente come deve.

Così Piero vede bene il cursore e, per esempio, ora può sparare per primo. In <link>Ghost Recon</link>http://www.ghostrecon.com/, va da sé.

<link>Lucio Bragagnolo</link>lux@mac.com