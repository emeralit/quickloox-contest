---
title: "Partirono in due ed erano abbastanza"
date: 2009-06-07
draft: false
tags: ["ping"]
---

Chiacchierata in <i>chat</i> Irc in diretta per la Wwdc? Ma s&#236;, dai!

Come a gennaio, collegarsi a <code>irc.freenode.net</code> e da l&#236; collegarsi al canale <code>#freesmug</code> con il comando <code>/join #freesmug</code>.

Mi farò trovare l&#236; per certo dalle 18:30 in avanti e probabilmente sarò dentro anche da prima. Il <i>keynote</i> inizia alle 10 della California, quindi le 19 italiane.

Per istruzioni e supporto, sono a disposizione sulla stanza <code>pocroom</code> di iChat e sempre su iChat come <i>lux@mac.com</i>. Gli arretrati di Ping! contengono le istruzioni complete su <a href="http://www.macworld.it/blogs/ping/?p=2074" target="_self">che programmi usare</a>, <a href="http://www.macworld.it/blogs/ping/?p=2103" target="_self">come e dove collegarsi</a> e <a href="http://www.macworld.it/blogs/ping/?p=2121" target="_self">come ricevere supporto</a>.

Un grosso grazie a <b>gand</b> per l'ospitalità su <a href="http://freesmug.org" target="_blank">Freesmug</a>!