---
title: "La nota che (quasi) nessuno ha visto"
date: 2001-12-03
draft: false
tags: ["ping"]
---

Apple dirama regolarmente quelle che sono chiamate note tecniche, contenenti informazioni di ogni tipo che aiutano programmatori, rivenditori tecnici e utenti finali a ottenere il massimo dai loro Mac.
Ma la nota tecnica 2034, pubblicata pochi giorni fa, non è più visibile: è infatti stata ritirata da Apple, presumibilmente in seguito a un’ondata di dissenso da parte dei lettori.
La nota contiene per metà una serie di consigli utili ai programmatori e per l’altra metà una serie di imposizioni che i programmatori ex NeXT ora al lavoro su Mac OS X vorrebbero far passare come progressi (si veda l’ultimo Clipboard del bravissimo Luca Accomazzi).
Entrerò nel merito più avanti. Per il momento si sappia che in Apple c’è un conflitto interno tra quelli che vogliono un sistema Unix facile come un Mac (e ci mancherebbe) e quelli che vogliono un sistema Mac che segua le leggi di Unix.
Bene: Unix non è fatto per i comuni mortali. La gente normale, the rest of us, vuole un Mac con basi Unix, non un sistema Unix che somiglia a Mac.
Da ricordare e da inserire a ripetizione tra i feedback su Mac OS X.