---
title: "Il .regalo di Natale"
date: 2003-12-23
draft: false
tags: ["ping"]
---

.mac continua a fare promozione, io continuo ad approfittarne

Non vorrei continuare a tornare sull’argomento .mac, ma è colpa un po’ anche sua, dal momento che non la smette di offrire regali e offerte speciali.

Per inveterata abitudine considero solo i primi e mai le seconde, e mentre scrivo sto scaricando (e userò) gratis PhotoStudio X 4.3 di ArcSoft, che costa 79 dollari.

Considerato che finora mi sono portato a casa gratis Everquest, iBlog, MarbleBlast e intanto approfitto di alcuni servizi (come Backup e la pubblicazione dei miei iCal) ho la sensazione che il mio abbonamento sia stato abbondantemente recuperato. Tanto più a settembre prossimo, quando scade, mancano ancora nove mesi.

Sì, .mac costa. Ma vale. E regalarlo a un amico porta anche a uno sconto sul prossimo abbonamento. Quasi quasi…

<link>Lucio Bragagnolo</link>lux@mac.com