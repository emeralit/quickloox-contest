---
title: "Tre giorni all'ora N"
date: 2010-01-07
draft: false
tags: ["ping"]
---

Il 10 gennaio il calendario interno dei Newton si resetterà, a meno di non applicare la <a href="http://40hz.org/Pages/Patch%2071J059" target="_blank">patch</a>.

Tenuto conto che si tratta di tecnologia antecedente a iPhone di oltre dieci anni, è sorprendente vedere quanto possa fare un Newton ancora oggi e quanto fosse avanti. Se ha insegnato qualcosa ad Apple, è proprio che il mercato non si merita tecnologia troppo avanzata.