---
title: "Unicode e geek editor"
date: 2006-05-05
draft: false
tags: ["ping"]
---

Invece di <em>text editor</em>… comunque sia: ho scaricato <a href="http://www.gigamonkeys.com/lispbox/" target="_blank">Lispbox</a> nel corso di uno dei miei ennesimi tentativi di imparare un po' di Lisp.

Nel pacchetto c'è una edizione di Emacs che è miracolosamente già configurata per accettare tranquillamente tutti i caratteri accentati e tipografici che ho tentato finora.

Per quanto riguarda il Lisp, sto seguendo i tutorial e (defun)gendo, per chi coglie. :-)