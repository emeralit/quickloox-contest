---
title: "Fase uno, inizio"
date: 2005-06-14
draft: false
tags: ["ping"]
---

D'ora in poi si starà ancora più attenti a come va il mercato. Ma serve?

Sto seguendo in modo molto empirico e approssimativo, mediante la vetrina di <a href="http://www.macatwork.net">Mac@Work</a>, com'è la reazione del grande pubblico di fronte all'annuncio della Terza Transizione e se si notano segni di problemi.

L'impressione (impressione e non più di questo, data l'esiguità e l'irregolarità del campione statistico) è che sia cambiato poco o niente. Anzi, gran parte dei clienti a momenti non ha quasi presente la notizia, e la vendita dei Mac non ha subito nessun rimbalzo, almeno non in forma apprezzabile.

Mentre è ancora prestissimo per dare giudizi e naturalmente gli unici numeri che conteranno sono quelli di Apple, una piccola riflessione: forse diamo troppa importanza alla questione. Per lo meno in questo momento.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>