---
title: "Con il Pdf quadrano i conti"
date: 2005-10-23
draft: false
tags: ["ping"]
---

Una opzione in più per creare documenti di livello, e gratis

Adesso che il Pdf è praticamente pervasivo in Mac OS X, completare un documento inserendovi del Pdf non è una cosa così improbabile come sarebbe stata una volta.

Per questo è carina l'idea di <a href="http://ktd.club.fr/">LaTeXiT</a>: fornire unicamente, al posto di un intero sistema di tipografia avanzato come LaTeX, un modulo per equazioni.

Le equazioni vengono prodotte in Pdf e da lì possono essere importate in qualunque programma capace di trattare il formato.

C'è sempre di mezzo il motore di LaTeX, per cui bisogna avere installate cosette come GhostScript e Pdflatex. Non è ancora cosa per tutti, ecco. Poi, però, è davvero una comodità.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>