---
title: "Il vecchio è ancora nuovo"
date: 2006-01-11
draft: false
tags: ["ping"]
---

Forse in seguito lo spiegherò con maggiore dettaglio, ma in estrema sintesi, posseggo un PowerBook G4 17&rdquo; acquistato a gennaio 2005.

Primo, non ho la minima intenzione di venderlo. Secondo, tutto il software che Apple presenterà nei prossimi anni potrà essere usato sul mio portatile.