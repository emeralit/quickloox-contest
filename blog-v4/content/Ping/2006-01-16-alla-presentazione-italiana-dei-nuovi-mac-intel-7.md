---
title: "Alla-presentazione-italiana-dei-nuovi-mac-intel-7"
date: 2006-01-16
draft: false
tags: [ping]
---

iWeb è quello che ci si aspettava: Pages sposa Homepage (quello in .mac). Testimoni, gli altri programmi di iLife.

Facilissimo per fare veramente in fretta siti personali, pubblicare blog, mandare in rete podcast.

Non sarà difficile trovare limitazioni a non finire, perché l’Html è una brutta bestia. Però chi lavora sul già fatto ci lavora dieci minuti e ha davvero finito.
