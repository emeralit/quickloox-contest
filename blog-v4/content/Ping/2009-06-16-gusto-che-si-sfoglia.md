---
title: "Gusto che si sfoglia"
date: 2009-06-16
draft: false
tags: ["ping"]
---

La cronologia di <a href="http://www.apple.com/safari/" target="_blank">Safari 4</a>, che su richiesta si può sfogliare in stile Cover Flow e si comanda anche da tastiera, mi ha fatto guadagnare settimane di vita. E soprattutto di gusto.