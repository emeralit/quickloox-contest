---
title: "Tra parentesi: basta l'interprete"
date: 2006-07-05
draft: false
tags: ["ping"]
---

Non gli ho chiesto autorizzazione e quindi non dirò chi è, ma è un amico che cercava un programma per risolvere espressioni, di quelle da scuola media.

Ha scoperto che, a parte naturalmente il foglio elettronico, per avere un risolutore di espressioni capace di rispettare le priorità delle operazioni e le parentesi è sufficiente il Terminale e uno dei linguaggi interpretati, come Python o Perl.

Non si tratta di tradire l'interfaccia grafica per il malvagio Unix: ci sono solo da inserire numeri, segni e parentesi, e quello è il sistema più veloce e risparmioso.

Alla fine si è optato per Python, che è il più immediato (a parte scoprire che l'elevazione a potenza per lui non è <code>^</code>, ma <code>\*\*</code>). Ed è anche venuto fuori che c'era un errore nel libro.

Io ho provato privatamente a riprodurre l'espressione in Lisp e ho capito perch&eacute; lo amo. Ti costringe a capire la struttura globale dell'espressione, se non vuoi barare. Più difficile e, al solito, si impara qualcosa in più.

