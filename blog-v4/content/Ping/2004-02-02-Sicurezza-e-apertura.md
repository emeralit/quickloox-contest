---
title: "Sicurezza e apertura"
date: 2004-02-02
draft: false
tags: ["ping"]
---

Perché gli aggiornamenti di sicurezza sono sempre discutibili

C’è chi si lamenta perché gli aggiornamenti di sicurezza a Mac OS X arrivano troppo frequentemente. C’è chi si lamenta perché Apple aspetta troppo tempo a pubblicarli, a volte anche un mese o più, dalla segnalazione del problema.

C’è chi si lamenta perché, nel descrivere ai problemi, si forniscono nuove conoscenze ai pirati e ai criminali informatici. C’è chi si lamenta perché, se Apple sceglie di non descrivere i problemi per non rivelare informazioni utilizzabili a scopo malevolo, nasconde queste informazioni anche ai legittimi utenti e complica i loro sforzi di autodifesa.

Che può fare Apple se non cercare ogni volta il miglior compromesso, sapendo che accontentare esigenze diametralmente opposte è perfettamente impossibile?

Ecco il motivo per cui gli aggiornamenti di sicurezza non accontenteranno mai nessuno al 100 percento. Anche se Mac OS X, ogni volta, diventa più sicuro.

<link>Lucio Bragagnolo</Link>lux@mac.com