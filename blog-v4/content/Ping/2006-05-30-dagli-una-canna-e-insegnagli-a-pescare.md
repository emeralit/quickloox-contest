---
title: "Dagli una canna e insegnagli a pescare"
date: 2006-05-30
draft: false
tags: ["ping"]
---

<cite>Un mio amico sta cercando un programmma per…</cite>

Un <a href="http://www.garzantilinguistica.it" target="_blank">dizionario inglese-italiano gratuito</a>, un sito pieno di <a href="http://www.versiontracker.com/macosx" target="_blank">programmi da trovare</a>. La ricerca si può espandere ai <a href="http://sf.net" target="_blank">repository di software open source</a> o anche limitare a <a href="http://www.google.it" target="_blank">Google</a>, ma se entriamo nel merito ci sono possibilità infinite.

Il punto è che imparare a cercare, oggi, è come imparare a leggere e scrivere cinquant'anni fa. Non è una spiacevole incombenza, bensì la base da cui partire. Si impara a cercare cercando. Facciamola cercare, 'sta gente. Per il loro bene, non per la nostra pigrizia.