---
title: "Historia antica"
date: 2007-05-14
draft: false
tags: ["ping"]
---

Dany: <cite>sarebbe bello se si potessero ricercare i comandi già dati nel Terminale quando non ti ricordi più la sintassi di un comando!</cite>

Il sottoscritto: <cite>c'è</cite> <code>history</code>.

L'unica cosa è che lo help è sepolto in quello della shell. C'è da passarci un weekend. Che sarebbe ben investito, sia chiaro.

Sia chiaro anche che il tonto, nella vicenda, sono io. Dany ha montato un client <code>ssh</code> sul cellulare e stava collegandosi al suo Mac. Già che c'eravamo, è venuto fuori anche che <code>less</code> è uno dei comandi più utili nell'universo dei minischermi. E ha pure il suo <code>man</code> privato.