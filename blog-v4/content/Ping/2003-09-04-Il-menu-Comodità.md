---
title: "Il menu Comodità"
date: 2003-09-04
draft: false
tags: ["ping"]
---

Quando ti serve qualcosa, è probabile che sia lì

Da utente Mac della prima ora, faccio una maledetta fatica a ricordarmi del menu Servizi. Nei primi tempi di Mac OS X era praticamente inutile; oggi a sistema ben più maturo scoprio un sacco di volte come la funzione che mi tornerebbe comoda in quel momento è lì, pronta, che aspetta.

Adesso, per esempio, selezionerò il testo di questo Ping e poi farò Tex-Edit Plus -> Servizi -> Mailsmith -> Send Selection con un solo rapido colpo di mouse e avrò già pronto il messaggio di posta da spedire alla redazione di Macworld online.

Quando l’ho scoperto, dopo settimane di vai su Mailsmith - crea un nuovo messaggio - vai su tex-Edit Plus - seleziona tutto - trascina, mi sono sentito un illetterato.

Invece Mac OS X possiede, più semplicemente, un mare di funzioni interessanti che aspettano solo di essere scoperte.

<link>Lucio Bragagnolo</link>lux@mac.com
