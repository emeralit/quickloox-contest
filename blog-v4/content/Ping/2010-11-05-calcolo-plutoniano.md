---
title: "Calcolo plutoniano"
date: 2010-11-05
draft: false
tags: ["ping"]
---

<a href="http://itunes.apple.com/it/app/keynote/id361285480?mt=8" target="_blank">Keynote</a> per iPad tutto sommato è quello che ci si aspettava.

<a href="http://itunes.apple.com/it/app/pages/id361309726?mt=8" target="_blank">Pages</a> è interessante e apre nuovi orizzonti potenziali.

<a href="http://itunes.apple.com/it/app/numbers/id361304891?mt=8" target="_blank">Numbers</a> è la fine del mondo. Lo hanno rivoltato come un guanto per adattarlo a un uso in ambito <i>multitouch</i> e l'interfaccia, per i primi cinque minuti, sembra importata da Plutone.

Dal sesto minuto in poi uno crede di esserci arrivato, su Plutone, e di non avere alcuna voglia di tornare indietro. Veramente un altro pianeta e adesso auspico che Apple metta al più presto a disposizione di iWork un sistema eccellente di condividere i dati via Internet con un Mac. Quello che c'è ora è buono per una versione 1.0 e inadeguato al livello generale delle prestazioni.