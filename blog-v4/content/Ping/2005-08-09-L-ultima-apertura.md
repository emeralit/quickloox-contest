---
title: "L'ultima apertura"
date: 2005-08-09
draft: false
tags: ["ping"]
---

Quel file è già nel Cestino, ma gli si voleva dare l'ultima occhiata…

Un file nel Cestino non si può aprire con un doppio clic. Bisogna tirarlo fuori per aprirlo. Se poi era proprio l'ultima occhiata, per essere sicuri, c'è da rimetterlo nel Cestino. Noioso.

Ma c'è qualche trucco. Lancia un programma in grado di aprire il file desiderato, che so, Anteprima se si tratta di un'immagine. Dai il comando Apri e, quando appare la finestra di dialogo, premi Comando-Maiuscolo-G. Scrivi <code>~/.Trash</code> e clicca Vai. Appare l'elenco dei file nel Cestino. Scegli quelli che vuoi ed ecco che si aprono.

Variante: trascina l'icona del file sul Dock, sull'icona di un programma che lo possa aprire. Stesso risultato.

Niente di che, ma perché sprecare tempo, anche se poco?

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>