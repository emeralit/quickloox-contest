---
title: "La tensione del cambio batterie"
date: 2002-04-02
draft: false
tags: ["ping"]
---

Quando si cambia la batteria a un PowerBook in Stop che monta Mac OS X...

Sui PowerBook attuali è possibile sostituire la batteria in assenza di alimentazione di rete e computer in Stop, a patto di impiegarci meno di dieci secondi e di stare usando Mac OS 9.
Se il PowerBook è in Stop sotto Mac OS X, Apple raccomanda vivamente di cambiare la batteria a computer alimentato, pena il rischio di conseguenze terribili come spegnimento, reset della Pram, reset dell’impostazione di disco di avvio, problemi con le pagine Web sicure e forse, chissà, anche cecità se lo si fa tutti i giorni.
In rete girano resoconti di gente che ignorava bellamente <link>l’avvertimento</link>http://docs.info.apple.com/article.html?artnum=106216 e non ha mai avuto alcun problema, per cui il tema è controverso.
Chi decide di provare, lo faccia - a suo rischio - e poi racconti, che qui su Ping lo Stop non esiste. :-)

<link>Lucio Bragagnolo</link>lux@mac.com