---
title: "Se i prezzi Apple li fa il pusher hardware"
date: 2004-09-25
draft: false
tags: ["ping"]
---

Seguito tutto italiano al dibattito sul prezzo di iMac G5

Ho citato da poco Walt Mossberg del Wall Street Journal, che evidenziava come il prezzo di iMac G5 fosse perfino inferiore a modelli Pc equivalenti. L'amico Sergio Leone mi ha inviato (e io cito fedelmente qui sotto) la sua esperienza personale, tutta italiana, che è un sequel perfetto. E istruttivo.

<cite>Dialogo col mio pusher hardware (che ovviamente assembla PC)…</cite>

<cite>Lui: Eeeeh, ma i mac, bla bla bla… poi sono cari, per esempio quell'iMac, un PC con maaaaassimo 800 euro te lo porti a casa.<br>
Io (rassegnato): OK, mi hai convinto, fammi un preventivo.</cite>

<cite>Lui: Mumble, mumble, AMD, mumble, motherboard, mumble ram, scheda grafica, HD, mumble, floppy, lettore CDRom…<br>
Io: Eh? CDRom? Lettore? Sei matto? Anche la scheda grafica $immonda_schifezza non è che... e poi la ram, fratello, 1 giga è il minimo per avviare Windows, non barare… il processore, dai, non puoi rifilarmi quello più scarso, metticene uno un po' più decente…</cite>

<cite>Lui: Vuoi anche tastiera e mouse?<br>
Io: Eh? Certochessì, USB.</cite>

<cite>Lui: Monitor, va bene $orrida_ciofeca_17” CRT?<br>
Io: Stai scherzando vero?</cite>

<cite>Lui: Uh? Ehm... vuoi un LCD? 17” va bene?<br>
Io: Sì.</cite>

<cite>Lui: Va bene un $orrida_ciofeca_17” LCD?<br>
Io: No, lo voglio decente.</cite>

<cite>Lui: Ehm… ehrrrr… così su due piedi i prezzi non li so… mi chiedono solo quelli, costano meno, mi devo informare, poi ti faccio sapere.<br>
Io: OK, fammi sapere.</cite>

<cite>Lui: Ci vuoi anche Windows?<br>
Io: Eh? Beh, sì.</cite>

<cite>Lui: Non è che, sai, ci sarebbe Linux…<br>
Io: No, se voglio me lo installo poi per conto mio. Lo vedi quel PC? Non il tower, l'altro, quello sotto al monitor, ci gira una RedHat adesso, quindi so di cosa parli. Tu?</cite>

<cite>Lui: Ehm… ehrrrr, OK (scribble, scribble, tira una una somma): 1.350 € più IVA, più ovviamente la differenza con $monitor_decente_17”LCD che_non_so_quanto_costa, più eventualmente il software, l'antivirus (obbligatorio)… lo vuoi, ehm… legale, vero?<br>
Io: Aspé… Mac -> Safari -> Applestore - tickete tackete - iMac, combo, 1 GB Ram = 1.365 € più IVA, siccome lo compro per mio figlio che è studente - tickete tackete - educhescional, 1.284 € più IVA.</cite>

<cite>A parte il fatto che il coso è una specie di creatura di Frankenstein accroccata alla sperindio.<br>
A parte il fatto che il mac quasi sicuramente funzionerà appena disinballato, “creatura” quasi sicuramente no.<br>
A parte il fatto che ci gira quella specie di virus chiamato Windows.<br>
A parte il po' po' di software che uno si ritrova preinstallato sul mac.<br>
A parte il fatto che su quel coso non ci gireranno GarageBand e FinalCutExpress con buona pace dei milionidimilioni di programmi che girano sui PC.<br>
A parte il fatto che mi sono dimenticato di chiedere se “creatura” è dotato di firewire, ma *so* che pur avendole fargli vedere la mia telecamera e tutto il resto sarà la solita appassionante avventura…</cite>

<cite>A parte tutto, il Mac costa meno!!!</cite>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>