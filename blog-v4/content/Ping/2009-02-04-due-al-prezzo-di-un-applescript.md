---
title: "Due al prezzo di un AppleScript"
date: 2009-02-04
draft: false
tags: ["ping"]
---

Spettacoloso John Gruber: si è creato un set di AppleScript che lo aiutano con Safari e vuole usarli anche con <a href="http://nightly.webkit.org/" target="_blank">WebKit</a>, senza però doverli duplicare su disco.

WebKit è la copia in via di sviluppo della prossima versione di Safari.

Per questo <a href="http://daringfireball.net/2009/01/applescripts_targetting_safari_or_webkit" target="_blank">ha scritto un AppleScript</a> che fa lavorare Safari oppure WebKit sulla cartella giusta, senza inutili duplicazioni e senza creare conflitti.

Lo script è semplice e ingegnoso e il suo principio risolve un sacco di situazioni in cui la soluzione più semplice sarebbe duplicare un gruppo di dati su disco, mentre quella più elegante è invece usare uno solo in tutti i modi che sembrano appropriati.