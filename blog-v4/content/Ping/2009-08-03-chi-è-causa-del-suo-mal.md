---
title: "Chi è causa del suo mal"
date: 2009-08-03
draft: false
tags: ["ping"]
---

Mi scuso per l'insistenza, ma la situazione sta diventando sempre più evidente per accumulo di prove a sostegno: uno studente americano <a href="http://www.prnewschannel.com/pdf/Amazon_Complaint.pdf" target="_blank">ha citato in tribunale Amazon</a> per la <a href="http://www.macworld.it/blogs/ping/?p=2638" target="_self">cancellazione di libri venduti illegalmente</a> dai Kindle di chi li aveva comprati.

Lo studente argomenta che il proprio lavoro di annotazione, compiuto sul libro a fini di studio, è andato in fumo; dovunque la nota non faccia riferimento esplicito a contenuto del libro, diventa inutile.

La causa potrebbe diventare <i>class action</i>, consentendo ad altri cittadini di associarsi alla denuncia e moltiplicando la cifra di eventuali risarcimenti.

Per Amazon è un ennesimo male di testa e l'ennesimo problema di immagine e di comunicazione. Altri soldi persi, altri clienti persi.

Sicuri che la capacità di mettere le mani sugli apparecchi che hai venduto, quando gli apparecchi sono già sulle mani dei clienti, sia davvero un odioso potere di violare la <i>privacy</i> e non un macigno di responsabilità con cui rischi di farti molto male se non sei più che bravo?