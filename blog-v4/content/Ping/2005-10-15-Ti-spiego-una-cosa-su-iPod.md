---
title: "Ti spiego una cosa su iPod"
date: 2005-10-15
draft: false
tags: ["ping"]
---

Come funziona la tecnologia per le persone normali

Una cosa che non sapevo assolutamente fino a ieri è che alle Gallerie dell'Accademia di Venezia, dal 24 Ottobre 2003 al 22 Febbraio 2004, si tenne una mostra dedicata a Zorzi da Castelfranco, altrimenti detto Giorgione, uno dei più grandi pittori del Rinascimento.

La <a href="http://www.teleart.org">Teleart</a> di Venezia ebbe l'idea, e si aggiudicò l'incarico, di predisporre un servizio di percorso audioguidato. Utilizzò ottanta iPod, caricati con i percorsi in cinque lingue sotto forma di file Mp3. Gli iPod vennero usati da 35 mila visitatori della mostra.

Gli iPod erano quelli di terza generazione, con la clickwheel e i quattro pulsanti sotto lo schermo. Per alcuni visitatori, specie quelli più anziani, risultarono troppo difficili da usare.

Un sacco di gente si chiede come mai gli iPod vendono così tanto. Dopotutto costano più di altri modelli e contengono pochissime funzioni. Sul mercato ci sono aggeggi dotatissimi, che svolgono decine di funzioni, molte più di qualunque iPod.

Invece Apple crea prodotti semplici.

Ecco perché.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>