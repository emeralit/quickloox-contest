---
title: "Giocare senza i giochi"
date: 2003-07-04
draft: false
tags: ["ping"]
---

Le risorse senza fine nascoste nella parte Unix di Macintosh

Ho rivolto un piccolo quiz ad Ale e a Davor: riuscire a giocare a Tetris avendo a disposizione solo un Macintosh con Mac OS X 10.2 e i Developer Tools (non sono sicuro che siano necessari; giusto per sicurezza). Niente Internet, niente reti, niente trucchi da prestigiatore o giochi di parole, niente di niente; solo quello che viene installato da Apple sul disco rigido.

Loro ci sono riusciti e hanno scoperto che, tra l’altro, i giochi sono molti più del solo Tetris.

Se ci riesci e ti va di farmelo sapere, sono qui che aspetto. :-)

<link>Lucio Bragagnolo</link>lux@mac.com