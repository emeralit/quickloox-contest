---
title: "L'attenzione agli altri"
date: 2007-11-24
draft: false
tags: ["ping"]
---

Connessione in rete a un Mac remoto con i dischi in condivisione. Un disco montato e viene voglia di montarne un altro.

Con Leopard, in vista come Colonne, è veramente un attimo e non c'è davvero paragone rispetto a Tiger.