---
title: "01100100 di questi giorni"
date: 2008-09-07
draft: false
tags: ["ping"]
---

E non solo. <a href="http://desktoptales.blogspot.com/" target="_blank">Desktop Tales</a> ha tagliato trionfalmente il traguardo delle tre cifre (decimali).

Moltissimi complimenti a Marc&#8230; <em>er</em>, Kappa e buon lavoro. Ne vogliamo altre 1111101000.