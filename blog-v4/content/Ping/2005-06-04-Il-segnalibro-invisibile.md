---
title: "Il segnalibro invisibile"
date: 2005-06-04
draft: false
tags: ["ping"]
---

Mi aiuta molto, grazie a Tiger

Non sono in grado di documentarlo con precisione, ma è un fatto che vari file di una certa lunghezza si aprono in Anteprima sulla pagina in cui avevo smesso di leggerli la volta prima.

Per me è una funzione di importanza notevole, per la quale avevo anche inviato feedback ad Apple.

Prima tenevo nota in un file di testo a parte di dove mi trovavo su ciascun file. Ora, finalmente e come è giusto che sia, ci pensa il sistemea operativo.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>