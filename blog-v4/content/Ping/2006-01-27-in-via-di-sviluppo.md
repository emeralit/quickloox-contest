---
title: "In via di sviluppo"
date: 2006-01-27
draft: false
tags: [ping]
---

La cifra più importante degli ultimi fatturati finanziari di Apple è 50. L’investimento in ricerca e sviluppo, la famosa R&amp;D, è cresciuto del 50 percento sull’anno scorso e lo trovo assai confortante.