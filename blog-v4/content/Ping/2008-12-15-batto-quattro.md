---
title: "Batto quattro"
date: 2008-12-15
draft: false
tags: ["ping"]
---

Cercare di copiare un file di dimensioni superiori ai quattro gigabyte su iDisk dà come risultato un errore zero.

Nel Finder. Invece il comando <code>cp</code> del Terminale non ha fatto una piega.

Adesso voglio vedere quanto ci mette.