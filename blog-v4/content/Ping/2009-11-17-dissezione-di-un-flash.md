---
title: "Dissezione di un Flash"
date: 2009-11-17
draft: false
tags: ["ping"]
---

Devo questo <i>post</i> a <a href="http://multifinder.wordpress.com/" target="_blank">Mario</a>, interessato a isolare i singoli componenti (immagini, suoni eccetera) presenti dentro un documento Flash trovato dentro una pagina web.

Firefox permette di chiedere le informazioni sulla pagina (Comando-I) e, nella sezione <i>Media</i>, registrare su disco i singoli componenti della pagina, compreso un eventuale documento Flash.

<a href="http://afu.jf.land.to/index-en.html" target="_blank">Flaex</a> estrae tutto l’estraibile dal documento Flash.

Presumo che ci siano modi più evoluti e che un buon programma di <i>authoring</i> Flash faccia tutto ciò in modo più elegante. Ma è stato più divertente fare senza. :-)