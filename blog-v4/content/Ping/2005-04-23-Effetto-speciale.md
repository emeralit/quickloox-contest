---
title: "Effetto speciale"
date: 2005-04-23
draft: false
tags: ["ping"]
---

Evangelismo involontario e però efficace

(Niente di blasfemo; la parola evangelismo è gergo del marketing e deriva dall'inglese <em>evangelism</em>, che è la perorazione appassionata di una causa commerciale.)

Supponi di andare al raduno dei vecchi compagni di classe del liceo. Arrivi direttamente da una giornata di lavoro e quindi, tuo malgrado, ti presenti con annessa borsa portaPowerBook.

In uno degli accessi di entusiasmo senile tipici delle rimpatriate qualcuno propone di mettere in comune tutti gli indirizzi elettronici e creare una mailing list per tenersi tutti in contatto qualsiasi direzione abbiano preso le nostre vite attuali.

Qualcuno tira fuori un foglio di carta e tu dici <cite>aspetta, ho il computer, li inserisco direttamente in rubrica e poi penso io a tutto</cite>.

Estrai il PowerBook e inizi a raccogliere nomi e indirizzi.

Esattamente in quel momento l'imbrunire raggiunge la soglia giusta e, come un effetto speciale hollywoodiano, si accende automaticamente come per magia la retroilluminazione automatica della tastiera.

Meraviglia generale, a momenti manca solo l'applauso.

A fine serata sono in quattro a chiederti dove lo hai comprato, quanto costa, se conosci un negozio dove ti trattano bene, che modello è meglio per loro eccetera.

I Mac si vendono da soli. Basta mostrarli. Persino in buona fede.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>