---
title: "Rubini e Rotaie"
date: 2006-02-16
draft: false
tags: ["ping"]
---

Uno degli svilupppi più interessanti della programmazione negli ultimi anni è il proliferare dei linguaggi di scripting. Forse meno performanti della programmazione vera in C++, ma più accessibili a un non superesperto e molte volte comodissimi per risolvere in fretta un problema relativamente semplice, oppure per realizzare dei prototipi di software.

Uno dei linguaggi di script più interessanti è Ruby e a <a href="http://tryruby.hobix.com/" target="_blank">questa pagina</a> lo si può trovare in versione interattiva da browser. Ossia si può provare a sperimentarlo senza dover scaricare niente sul computer.

Se convince e lo vuoi scaricare, c'è <a href="http://locomotive.raaum.org/home/show/HomePage" target="_blank">Locomotive</a>, già pronto per Mac OS X.

<strong>Neko</strong>, l'autore delle segnalazioni sopra, mi ha anche guidato verso un <a href="http://media.rubyonrails.org/video/rails_take2_with_sound.mov" target="_blank">filmato</a> in cui si mostra il primo uso di Ruby.

Grazie mille a Neko, che è poi uno degli animatori di un sito molto frizzante, <a href="http://www.numerozero.com" target="_blank">NumeroZero</a>. DISCLAIMER: NumeroZero contiene una notevole quantità di materiale totalmente <strong>per adulti</strong>. Qualcuno potrebbe trovarlo riprovevole o diseducativo, o peggio. Non cliccare se non sei consapevole, o se non sei maggiorenne. Non dire che non ti ho avvisato.