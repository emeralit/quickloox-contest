---
title: "D-i-c-i-a-n-n-o-v-e-m-i-l-a"
date: 2008-04-30
draft: false
tags: ["ping"]
---

Sto compulsando <a href="http://osx.hyperjeff.net/" target="_blank">Hyperjeff.net</a> alla ricerca del mio possibile <a href="http://www.macworld.it/blogs/ping/?p=1742">sostituto di Tex-Edit Plus</a>.

Ne approfitto per ringraziare quanti mi hanno dato suggerimenti e spiegare meglio che cosa cerco.

Deve essere un <em>text editor</em>, ossia avere righe di lunghezza indefinita se necessario (niente margini di pagina).

Deve essere <em>styled</em>: permettere l'uso di grassetti, corsivi e altri attributi normali di elaborazione del testo.

Deve permettere l'uso di espressioni regolari, contare i caratteri in tempo reale ed essere eccezionalmente scriptabile, soprattutto AppleScriptabile. Se non è almeno <em>recordable</em> (faccio cose e Script Editor traduce le mie azioni in linguaggio AppleScript) non va bene.

Per esempio, Textmate è sostanzialmente un alter ego di BBEdit e non va bene, perché BBEdit non è <em>styled</em> e neanche Textmate. Nisus è un programma eccellente ma non fa, credo, da text editor e non usa le espressioni regolari (non ho ancora controllato e accetto volentieri la smentita).

A parte tutto ciò, HyperJeff conta più di <em>diciannovemila</em> programmi nativi per Mac OS X. E i programmi per Mac sarebbero pochi.