---
title: "Il secondo venticinquesimo"
date: 2010-09-20
draft: false
tags: ["ping"]
---

Microspot ha presentato una nuova versione,, la sesta, di <a href="http://macnn.com/rd/174138==http://www.microspot.com/products/macdraft/index.htm" target="_blank">MacDraft Professional e MacDraft Personal Edition</a>, per l'illustrazione tecnica e la progettazione 2D.

La cosa speciale è che si tratta di versioni del venticinquesimo anniversario: MacDraft è nato nel 1985 e Microspot lo ha acquistato tempo fa dagli autori di Innovative Data Design.

Di programmi commerciali da tutto questo tempo sulla piazza e tuttora supportati mi viene in mente solo Excel e scusa se è poco. Ovviamente sarò molto lieto di essere corretto.