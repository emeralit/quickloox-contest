---
title: "La sete di Rosetta"
date: 2006-03-20
draft: false
tags: ["ping"]
---

Un MacBook Pro imbottito di Ram fino al limite massimo dei due gigabyte esegue Photoshop meglio di un PowerBook 17&rdquo; ugualmente imbottito. Su certe operazioni c'è sostanziale parità, su altre il MacBook Pro vince nettamente.

La discriminante è la Ram. Più ce n'è, meno influisce il collo di bottiglia di Rosetta.