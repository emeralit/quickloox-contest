---
title: "Il paradosso degli U2"
date: 2004-10-28
draft: false
tags: ["ping"]
---

Gli scherzi che causa il destino sono niente, in confronto a quelli della tecnologia

Finalmente Apple ha aperto l'iTunes Music Store in tutta Europa, con un evento che ha coinvolto nientepopodimeno gli U2.

Chissà che cosa avrà detto Bono nello scoprire che iTunes Music Store è attivo in tutta l'area dell'euro e naturalmente nel Regno Unito, ma <em>non</em> in Irlanda.

La bizzarria pare chiarita. Lo Store di san Patrizio è rimasto indietro all'ultimissimo momento a causa di un problema tecnico, ora risolto, e dovrebbe regolarmente aprire entro qualche giorno.

Se non altro, per una volta non è l'Italia quella che arriva sempre per ultima.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>