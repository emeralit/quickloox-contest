---
title: "Citofonare Gianni"
date: 2004-10-04
draft: false
tags: ["ping"]
---

Altra testimonianza sul prezzo di iMac G5

Daniele Savi vuole rubarmi il posto (scherzo, lo ringrazio molto, invece!) e mi ha inviato un altro messaggio che veramente non ha bisogno di commenti:

<cite>Ieri leggevo un depliant dell'Unieuro e ho beccato una cosa molto divertente: in offerta promozionale un PC Sony &ldquo;All in one&rdquo;, solo schermo e mouse/tastiera, praticamente identico come &ldquo;idea&rdquo; al nuovo iMac, solo molto più triste come design. Beh, il prezzo in offerta era di quasi 1.400 Euro… e ovviamente non c'è Mac OS X, ma Windows. :-D</cite>

<cite>Ah, e ovviamente c'è su un Pentium 4, non un processore a 64bit…</cite>

<cite>Tira tu le conclusioni. :-D</cite>

Si tirano da sole. L'ottimismo è il profumo della Mela.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>