---
title: "Risorge il tastierino"
date: 2009-09-04
draft: false
tags: ["ping"]
---

Quando gioco ad <a href="http://rephial.org" target="_blank">Angband</a> mi manca un po' il tastierino numerico virtuale, quello che fino a qualche tempo fa si otteneva con il tasto Fn e i tasti 7, 8, 9, U, I, O, J, K e L.

Mi mancava. Tale Takayama Fumihiko ha pubblicato <a href="http://www.pqrs.org/tekezo/macosx/keyremap4macbook/index.html" target="_blank">KeyRemap4MacBook</a>. Lo si installa, si riavvia e &#8211; su Snow Leopard &#8211; il tastierino torna magicamente.

Su Tiger e Leopard c'è una manovra in più da compiere: aprire il programma nelle Preferenze di Sistema, selezionare il comando Rename Fn Key e abilitare l'opzione Fn to Fn (with NumLock). Ecco fatto.

Occhio a NON installare KeyRemap4MacBook se sono già presenti altri rimappatori di tastiera. La tastiera potrebbe smettere di rispondere normalmente e in quel caso, per rimediare, bisognerebbe riavviare in modalità Single User. Poco simpatico.