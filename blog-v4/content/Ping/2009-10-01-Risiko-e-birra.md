---
title: "Risiko e birra"
date: 2009-10-01
draft: false
tags: ["ping"]
---

È iniziata oggi la <a href="http://sillysoft.net/contests/" target="_blank">Luxtoberfest</a>.

Tornei e premi di ogni tipo per giocatori di Lux da qui a fine ottobre, conoscendo gente da ogni parte del mondo e divertendosi su uno dei Risiko digitali migliori, se non il migliore.

Occhio che Risk, da cui discende <a href="http://sillysoft.net/lux/" target="_blank">Lux</a>, è sbilanciato a favore degli attaccanti e quindi le strategie vanno adattate di conseguenza.

La birra, a imitazione della tedesca Oktoberfest in via di conclusione, è a discrezione dell'individuo. :-)