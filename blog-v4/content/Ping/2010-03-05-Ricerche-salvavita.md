---
title: "Ricerche salvavita"
date: 2010-03-05
draft: false
tags: ["ping"]
---

Una riflessione fuori luogo, non richiesta e collegata molto marginalmente a Ping!. Tuttavia mi sta a cuore e in amicizia voglio condividerla.

I recenti terremoti a <a href="http://www.wolframalpha.com/input/?i=haiti+earthquake" target="_blank">Haiti</a> e in <a href="http://www.wolframalpha.com/input/?i=chile+earthquake" target="_blank">Cile</a> hanno provocato devastazioni e vittime in quantità. Il nemico peggiore non è veramente il terremoto, ma l'arretratezza sociale e tecnologica che prima mette a maggior rischio le vite e poi ne complica il soccorso, per non parlare di un ritorno alla normalità che per molti neanche c'è mai stata.

Il <a href="http://www.wolframalpha.com/input/?i=l%27aquila+earthquake" target="_blank">terremoto in Abruzzo</a>, lievemente meno forte, ha creato morte e disagi in quantità. Pochi mesi dopo, tuttavia, migliaia di senzatetto hanno smesso di esserlo, grazie a case costruite con tecnologie avanzate e innovative.

C'è appena stato un <a href="http://www.wolframalpha.com/input/?i=taiwan+earthquake" target="_blank">terremoto a Taiwan</a>, di intensità lievemente minore a quello abruzzese.

Ci sono notizie di feriti, interruzioni di corrente e in particolare si ipotizza che l'incendio di una fabbrica di pannelli di cristalli liquidi possa provocare rialzi dei prezzi, ma poco più. Il <i>boom</i> dell'elettronica ha portato da molti anni una ricchezza che ha consentito di erigere costruzioni antisismiche e attivare tecnologie di sorveglianza e soccorso.

Non siamo con i nostri Mac solo perché ci piacciono, ma anche perché complessivamente la tecnologia ci aiuta e contribuisce al progresso e al benessere.

Lo scienziato e scrittore Arthur C. Clarke scrisse <cite>La creazione di ricchezza non va disprezzata, ma nel lungo termine le sole attività umane che valgano davvero qualcosa sono la ricerca della conoscenza e la creazione della bellezza.</cite>

La tecnologia, per quanto a volte capricciosa e volubile, è figlia della prima ricerca.