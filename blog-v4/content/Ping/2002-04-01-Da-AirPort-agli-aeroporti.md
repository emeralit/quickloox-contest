---
title: "Da AirPort agli aeroporti"
date: 2002-04-01
draft: false
tags: ["ping"]
---

Presto, molto presto, la rete via cavo diventerà solo un ricordo

Adesso che Apple ha annunciato il suo Bluetooth Adapter i due grandi standard di connessione via radio sono entrambi coperti. Come oggi si può entrare al Mondadori Informatica di via Marghera a Milano, o a uno Starbucks di New York, e collegarsi gratuitamente e liberamente a Internet via AirPort, così entro poco basterà l’Adapter per usufruire delle reti Bluetooth che sono in via di allestimento in numerosi aeroporti.
E finalmente, nelle riviste di informatica con redattori di scarsa originalità, titoli come “la rete corre sul filo” saranno solo un lontano ricordo.

<link>Lucio Bragagnolo</link>lux@mac.com