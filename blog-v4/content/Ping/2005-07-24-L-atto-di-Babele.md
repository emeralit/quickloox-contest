---
title: "L'atto di Babele"
date: 2005-07-24
draft: false
tags: ["ping"]
---

Dall'alto della torre la visibilità è maggiore e le decisioni sono meditate

Testuale. <cite>Babele Dunnit ammette la superiorità di Apple</cite>.

Per chi conosce la persona è un momento significativo. Per chi non la conosce, è il momento di una esperienza di <a href="http://www.babeledunnit.org">allargamento delle vedute</a>.

Grazie a <a href="http://www.maestrinipercaso.it">Mafe e Vanz</a> per la segnalazione!

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>