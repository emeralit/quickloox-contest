---
title: "Fette di torta"
date: 2008-11-22
draft: false
tags: ["ping"]
---

Sempre perché c'è sempre uno svanito che si mette a parlare di quote di mercato: OpenOffice.org ha un buon indicatore indiretto, che è il numero di <a href="http://marketing.openoffice.org/marketing_bouncer.html" target="_blank">scaricamenti di OpenOffice.org per sistema operativo</a>.

Mac OS X ha il 5,3 percento. E si pensi che attualmente, seguendo i link normali del sito, c'è qualche problema con le versioni PowerPc. Per esempio, è impossibile a un utente non smaliziato scaricare una 2.4.0 italiana. Il che vuol dire che bisogna ancora aggiungere qualcosa.