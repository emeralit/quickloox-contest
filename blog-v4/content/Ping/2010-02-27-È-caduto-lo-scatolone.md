---
title: "È caduto lo scatolone"
date: 2010-02-27
draft: false
tags: ["ping"]
---

A MacHeist sono geni del <i>marketing</i> e hanno una programmazione Html spettacolare. Giocano anche a World of Warcraft, a giudicare dal punto interrogativo giallo.

Il prossimo <i>bundle</i> comincia fra tre giorni, ma la <a href="http://www.macheist.com/" target="_blank">home di adesso</a> è da non perdere.