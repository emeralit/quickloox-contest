---
title: "Chi ben ricomincia"
date: 2002-02-11
draft: false
tags: ["ping"]
---

Non si vive di solo Explorer.
Bellissimo browser, eh? Solo un po’ troppo invasivo, invadente e gocciolante abusi e condotte scorrette.
Le alternative non mancano, dal versatile <link>iCab</link>http://www.icab.de al sempreverde <link>Netscape</link>http://www.netscape.com (o <link>Mozilla</link>http://www.mozilla.org) fino all’avveniristico <link>Omniweb</link>http://www.omnigroup.com.
E adesso è arrivato anche Opera, browser storico di chi preferisce sempre che esista un’alternativa, dato che la libertà significa poter scegliere. Dopo un lunghissimo periodo di beta testing esiste ora una versione finale che, sono sicuro, molti apprezzeranno.
<link>Opera</link>http://www.opera.com/mac/ è uno dei tanti modi per iniziare a navigare senza monopolio. O, per qualche vecchietto che si ricorda l’Internet di una volta, per ricominciare.

<link>Lucio Bragagnolo</link>lux@mac.com