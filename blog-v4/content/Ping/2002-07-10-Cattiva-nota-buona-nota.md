---
title: "Cattiva nota, buona nota"
date: 2002-07-10
draft: false
tags: ["ping"]
---

Non era vero che un Cd irregolare in un lettore invalida la garanzia. Se qualcuno lo facesse sapere...

Aveva sollevato un discreto vespaio la nota tecnica di Apple che pareva affermare l’invalidamento della garanzia nel caso di Cd protetti contro la copia che fossero rimasti incastrati nel lettore.
Chiaramente si trattava di una svista dell’estensore della nota o di una parafrasi errata e difatti Apple ha sostituito la nota incriminata con <link>un’altra</link>http://docs.info.apple.com/article.html?artnum=106882 più rassicurante.
Solo che nessuno, o i soliti quasi, lo ha scritto. Valgono solo le note tecniche cattive?

<link>Lucio Bragagnolo</link>lux@mac.com