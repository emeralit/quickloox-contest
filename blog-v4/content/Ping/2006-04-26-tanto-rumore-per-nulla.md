---
title: "Tanto rumore per nulla"
date: 2006-04-26
draft: false
tags: ["ping"]
---

Quando chiudo volontariamente la connessione Bluetooth appare un messaggio semiterroristico a dirmi che il collegamento si è interrotto inaspettatamente. Zelante, ma fuori luogo.