---
title: "AppleScript e i modificatori (mai abbastanza)"
date: 2008-07-21
draft: false
tags: ["ping"]
---

Questi AppleScript, ricordo, hanno valore didattico. A volte non è necessario usarli per raggiungere lo scopo che si prefiggono. Sono però sempre utili per capire il linguaggio, il suo funzionamento e le sue possibilità.

Questo è <a href="http://blog.realityspline.net/2008/01/30/applescript-snippet/" target="_blank">un caso esemplare di AppleScript inutile e ugualmente produttivo</a>. Realizzato da uno <em>switcher</em> che, proveniendo da Linux, desiderava poter cambiare schermo in Exposé con la pressione di <em>due</em> tasti modificatori, anziché il modificatore singolo di serie in Mac OS X.

<code>tell application "System Events"
 tell expose preferences
  tell spaces preferences
   tell arrow key modifiers
    set properties to {key modifiers:{control, option}}
    get properties
   end tell
  end tell
 end tell
end tell</code>

La parte interessante è il comando <code>set properties</code>, che imposta il nuovo parametro; e il comando <code>get properties</code>, che riceve dal sistema le impostazioni appena modificate. È paradossale, che sia AppleScript a impostare il parametro e poi a dover richiedere la modifica per poterla usare, e anche programmazione molto tipica. Il resto è infrastruttura e s&#236;, dentro Script Editor il tutto risulta anche elegante.