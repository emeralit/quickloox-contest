---
title: "Grafica da mostri"
date: 2006-09-14
draft: false
tags: ["ping"]
---

Scrivo in tempo reale dalla presentazione milanese dei nuovi annunci Apple e, in qualità di irregolare delle truppe elfe in quel di Azeroth, devo sottolineare che l'illustrazione a contorno delle specifiche grafiche del nuovo <a href="http://www.apple.com/it/imac" target="_blank">ù</a>; è una megaschermata di <a href="http://www.wow-europe.com" target="_blank">World of Warcraft</a>.