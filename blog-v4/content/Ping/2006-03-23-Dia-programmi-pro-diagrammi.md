---
title: "Dia programmi pro diagrammi"
date: 2006-03-23
draft: false
tags: ["ping"]
---

Sarà il momento primaverile, ma ogni tre secondi scopro un programma nuovo. <a href="http://www.aisee.com/" target="_blank">Questo</a> è una delle soluzioni più complete al problema della rappresentazione diagrammatica che abbia mai incontrato.

Per ora si usa su X11 ma la versione nativa Aqua è in lavorazione (e annunciata). aiSee è libero per uso non commerciale, anche se c'è da litigare un po' con le procedure per registrarlo. Da tenere presente.