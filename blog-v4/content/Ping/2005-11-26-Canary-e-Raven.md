---
title: "Un canarino che canta ancora"
date: 2005-11-26
draft: false
tags: ["ping"]
---

La gabbietta è vecchia, ma potrebbe tornare utile

L'amico <strong>Flavio</strong> mi ha raccontato una serie di cose interessanti su Canary e Raven. Sono programmi per l'analisi scientifica del suono realizzati dai ricercatori della Cornell University negli Stati Uniti. Con la transizione da Mac OS 9 a Mac OS X, in Cornell hanno proceduto alla transizione da Canary a Raven e questo apre una interessante finestra di opportunità.

Mi spiego. Intanto che Raven, il nuovo prodotto, raggiunge il livello di potenza e funzionalità di Canary, quest'ultimo, che gira solo in Mac OS 9, viene reso scaricabile gratuitamente. E può essere lanciato dall'ambiente Classic di Mac OS X.

Il software è di valore professionale eppure l'unico costo è quello della banda per scaricarlo. Per chi è interessato all'argomento, può essere una bella idea per capire che cosa lo attende (a pagamento, ma nativamente su Mac OS X, in edizione Full e Lite) con <a href="http://www.birds.cornell.edu/brp/Raven/Raven.html">Raven</a> e intanto, per dire, sfruttare da subito <a href="http://www.birds.cornell.edu/brp/CanaryInfo.html">Canary</a>.

Il canarino ha la sua età e presto verrà soppiantato da un volatile più prestante, ma il suo trillo è limpido come una volta.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>