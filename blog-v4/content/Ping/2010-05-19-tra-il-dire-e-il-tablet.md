---
title: "Tra il dire e il tablet"
date: 2010-05-19
draft: false
tags: ["ping"]
---

Adesso sappiamo che Courier, il <i>concept</i> presentato da Microsoft di tavoletta-quaderno con due schermi, era &#8211; almeno a oggi &#8211; un <i>pour parler</i>; Microsoft <a href="http://www.wired.com/gadgetlab/2010/04/microsoft-cancels-courier-tablet/" target="_blank">non ha in programma</a> di produrlo veramente.

Invito a guardarsi in rete immagini e filmati di Courier (basta cercare <i>courier tablet</i> in Google ed esce tutto). Attraente, immaginifico, sexy; è proprio bello, fa venire voglia, ispira moltissimo. Ottima realizzazione.

Di un <i>concept</i>. Il dramma nasce quando qualcuno ci crede e pensa <i>lo faccio anch'io</i>.

Engadget <a href="http://www.engadget.com/2010/03/12/entourage-edge-review/" target="_blank">riferiva prima di iPad</a> di EnTourage Edge, <i>tablet</i>-quaderno con uno schermo E-ink per la lettura e un altro Lcd per usare la tavoletta con lo stilo, che già uno vede lo stilo e pensa <i>ancora?</i>.

Costa esattamente come iPad modello base negli Stati Uniti, 499 dollari. Però ha molte cose in più.

Per esempio è spesso come un grosso libro, il triplo di un Kindle Dx. E pesa il doppio di iPad, criticato perché sette etti si faticano a tenere in mano.

Però ci sono due porte Usb, una microUsb, un lettore di schede Sd. E c'è la <i>webcam</i>! A parte il fatto che il software non consente di usarla, almeno fino a che non arriverà un aggiornamento appropriato, è davvero un vantaggio.

Il sistema operativo, Android di Google, ha a disposizione Android Marketplace, un equivalente di App Store con dentro credo quarantamila applicazioni. Edge ha quattro gigabyte di disco, che iniziano a stare un po' stretti se appena uno comincia a caricare un po' di musica, i libri e poi si dà anche alle <i>app</i>. Fortunatamente, per modo di dire, il problema non si pone: il <i>tablet</i> non dà accesso a Android Marketplace. Come se Apple facesse un iPhone che non va su App Store. L'azienda che produce Edge aprirà in futuro un negozio <i>online</i> in proprio, dice. Intanto c'è quel che c'è.

Lo schermo è a tecnologia resistiva anziché capacitiva. Tradotto, costa meno ed è più scomodo e meno reattivo. Nel caso l'apparecchio fatto per essere usato con le dita fosse sgradevole da usare con le dita, c'è una <i>trackball</i>.

Vogliamo parlare della cattiva Apple che ha bandito Flash? Edge, di tutt'altra pasta, promette il supporto Flash Lite. Engadget però riferisce che il <i>browser</i> non mostra i filmati YouTube e manca una <i>app</i> YouTube in dotazione. Tradotto: la cattiva Apple mostra YouTube, la buona EnTourage Edge invece no.

Le prestazioni sono nella media, riferisce Engadget, durante l'uso in modalità Wi-Fi. In modalità cellulare, una volta inserita la Sim nel connettore, si constata che il connettore non è ancora attivo.

E la batteria? Certamente non c'è da attendersi molto da un apparecchio con due schermi: se sono ambedue accesi, dopo quattro ore si è a secco, il 40 percento di quello che dura iPad. Meglio spegnere lo schermo Lcd, quello che consuma di più, cos&#236; da ottenere con l'E-ink una durata superiore a un quinto di quello che fa Kindle con lo stesso schermo nella stessa situazione. Pazienza, ci si abituerà a usare la tavoletta &#8211; lo strumento portatile per eccellenza &#8211; con dietro l'alimentatore, <cite>più grande e meno portatile di quello della maggior parte dei</cite> laptop, ammonisce Engadget.

L'ultimo paragrafo della recensione si conclude cos&#236;.

<cite>S&#236;, Edge è una combinazione di</cite> e-reader <cite>e</cite> tablet Android <cite>&#8212; solo che non funziona bene in nessuno dei due modi. Per 499 dollari ci sono semplicemente troppi problemi, dal corpo massiccio alla scarsa scelta di</cite> ebook <cite>al</cite> touchscreen <cite>frustrante alla poca autonomia fino alla mancanza di applicazioni Android. Mentre gli aggiornamenti</cite> software promessi<cite> potrebbero risolvere abbastanza problemi e abilitare abbastanza funzioni da farne un decente rimpiazzo del libro di testo per qualche studente, ora come ora non possiamo dire che valga il suo prezzo.</cite>

Passare da un'idea a un prodotto usabile non è uno scherzo e Apple va fin troppo apprezzata per quello che riesce a eliminare dai progetti, in modo da presentare l'essenziale e poi costruirci sopra iterazioni successive di nuove funzioni e miglioramenti. Le liste di specifiche, invece, valgono poco e non significano niente, perché a contare è come funziona il prodotto quando lo si ha in mano.

A margine, chi si ricorda di Paolo Attivissimo e del suo <a href="http://attivissimo.blogspot.com/2010/01/aiped-aipad-aipod.html" target="_blank">commento su iPad</a>?

<cite>Niente Flash. Niente</cite> multitasking<cite>. [&#8230;] Niente</cite> webcam <cite>per Skype e simili. Batteria sigillata. Niente USB [&#8230;]. Dipende da un PC per i</cite> backup<cite>. Niente software non autorizzato da Apple. DRM come se piovesse. [&#8230;] E cos'è questa storia della micro-SIM?</cite>

Edge ha Flash, ha il <i>multitasking</i>, ha la <i>webcam</i>, la batteria asportabile, le porte Usb, non dipende da un Pc per i backup, ha software liberissimo, nessun Drm, un connettore per Sim normali. Quindi è superiore, giusto? Oppure è sbagliato l'approccio, giusto?