---
title: "Il braccio robusto dell’iMac"
date: 2003-08-05
draft: false
tags: ["ping"]
---

Non fa solo stile ma è anche tanto comodo per chi il computer lo usa davvero

Mi scrive Riccardo:

“Il braccio snodato dell'iMac flat: nessuno ne parla, ma secondo me il fatto di poter orientare al millimetro la posizione dello schermo è una cosa grandiosa per chi ci siede davanti tutto il giorno: sai, noi programmatori ci sbraghiamo sulla scrivania, una volta da un lato una volta a testa in giù, e avere sempre a puntino lo schermo…! Te ne rendi conto quando ti siedi mezz’ora davanti a un Tft con la base fissa o davanti al portatile”.

Condivido <i>in toto</i> e aggiungo: quando uscì l’iMac Lcd, qualche scettico disse che prima di acquistarlo avrebbe aspettato un test sulla durata del braccio. Ho a casa un iMac Lcd da oltre un anno e il braccio funziona perfettamente, senza un millimetro di gioco o di inerzia.

<link>Lucio Bragagnolo</link>lux@mac.com