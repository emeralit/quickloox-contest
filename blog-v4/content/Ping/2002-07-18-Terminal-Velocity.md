---
title: "Terminal Velocity"
date: 2002-07-18
draft: false
tags: ["ping"]
---

Un banale comando da Terminale per avere un test di velocità del Mac

I benchmark, essenzialmente, sono una minchiata.

Non esistono macchine realmente uguali, che si trovino esattamente nello stesso stato, che funzionino esattamente nelle stesse condizioni in un tempo ragionevole dato.

Inoltre “velocità” è un termine relativo: il mio Mac è veloce perché, se un programma è impegnato, con un clic passo istantaneamente a un altro programma. Per un grafico il suo Mac è veloce perché Photoshop macina filtri che è una bellezza e degli altri programmi non gliene importa nemmeno un po’.

Detto questo, un comandino semplice semplice da dare nel Terminale di Mac OS X:

openssl speed

e Mac inizia a triturare crittografia, riportando i tempi di esecuzione di ciascun algoritmo.

Se proprio misuri il tuo Mac da quanto è veloce, invece che da quanto fa per te. :-)

<link>Lucio Bragagnolo</link>lux@mac.com