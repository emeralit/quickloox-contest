---
title: "Hacker rosiconi"
date: 2004-10-22
draft: false
tags: ["ping"]
---

Minacciano, minacciano e intanto non c'è niente

Un buffo articolo di <a href="http://www.theinquirer.net/?article=19159">The Inquirer</a> lascia intendere che gli hacker finora hanno trascurato Mac OS X ma che adesso se ne stanno accorgendo, che tutto sommato le capacità richieste per violarlo non sono tanto diverse rispetto ad altri sistemi Unix e che nel primo pomeriggio piovoso qualche hacker annoiato produrrà qualcosa di devastante.

Eppure, se Mac OS X venisse seriamente violato da qualcuno, basterebbe per passare alla storia dell'informatica. Visto che in quattro anni non ci è riuscito nessuno, e che basta un pomeriggio di pioggia e di noia, bisogna concludere che a casa degli hacker splende sempre il sole?

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>