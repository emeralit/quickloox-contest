---
title: "Scaricare e le responsabilità"
date: 2009-07-15
draft: false
tags: ["ping"]
---

I forum del sito Apple mi hanno risolto un problema per la seconda volta.

I dettagli della cosa sono vergognosamente ludici e li lascio a disposizione tramite il link alla <a href="http://discussions.apple.com/thread.jspa?threadID=2079244&amp;tstart=0" target="_blank">discussione</a>.

La parte utile della vicenda è che, su Leopard, fare decomprimere un file Java bin.zip a Safari oppure procedere a mano facendo doppio clic sul file dentro il Finder sono due operazioni diverse nel risultato. Se ti aspetti di ottenere un file .jar e, via Finder, non lo ottieni, prova ad abilitare l’apertura automatica dei file “sicuri” tramite Safari (Preferenze, Generali).

Si allenta leggermente la sicurezza del sistema. Tuttavia, magari per un singolo file proveniente da fonte fidata, è una responsabilità che vale la pena di prendere.