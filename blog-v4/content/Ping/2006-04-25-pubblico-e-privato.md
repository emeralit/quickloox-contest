---
title: "Pubblico e privato"
date: 2006-04-25
draft: false
tags: ["ping"]
---

Negli ultimi giorni la mia agenda ha risolto due problemi praticamente da sola. Chi lavora con me ha il mio <a href="http://ical.mac.com/lux/Work" target="_blank">calendario pubblico</a> di iCal, dove può vedere quando ho impegni e quando invece, pur lavorando, sono potenzialmente disponibile. Così, se c'è un consiglio di amministrazione di Mac@Work oppure una riunione con un editore, mi vengono proposti già in partenza una data e un orario che hanno buone probabilità di funzionare.

Si risparmiano così penosi giri di telefonate e di posta elettronica. E l'abbonamento a .mac si è già quasi ripagato solo per questo.