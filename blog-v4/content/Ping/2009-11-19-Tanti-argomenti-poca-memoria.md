---
title: "Tanti argomenti, poca memoria"
date: 2009-11-19
draft: false
tags: ["ping"]
---

Ringrazio e saluto con grande piacere i partecipati alla Pingpizza 3000.

Grande successo perché ogni partecipante ha portato con sé un'ora di gradevole conversazione e abbiamo tirato molto più tardi di qualsiasi previsione.

Faccio fatica a riprendere tutto quello che è saltato fuori, durante e prima, perché c'è chi è arrivato in anticipo.

In ordine sparsissimo, <a href="http://www.tug.org/mactex/2009/" target="_blank">Latex</a>, <a href="http://itunes.apple.com/it/app/supernova-blast/id312782169?mt=8" target="_blank">Supernova Blast</a>, <a href="http://itunes.apple.com/it/app/fling-free/id325825863?mt=8" target="_blank">Fling</a>, <a href="http://itunes.apple.com/it/app/webank/id306283651?mt=8" target="_blank">Webank</a>, un gioco spettacolare di tower defense che assolutamente non ricordo, l'impaginazione dei libri con (e senza) Word ed è davvero solo l'inizio

Con mia sorpresa e delizia, tutti hanno portato almeno un libro e ce li siamo scambiati. Bello e bellissimi i libri che mi sono portato via.

Ci si rivede venerd&#236; 4 dicembre, per motivi che pubblicherò domani o dopo.

Ancora grazie. :)