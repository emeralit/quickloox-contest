---
title: "Tipi in gamba"
date: 2008-04-04
draft: false
tags: ["ping"]
---

<a href="http://www.scumbag.it" target="_blank">Federico</a> mi scrive per rincarare la dose sulle questioni tipografiche:

<cite>Girovagando alla ricerca di informazioni sui caratteri ho trovato un bel resoconto circa la <a href="http://www.tipoteca.it/it_informazioni.html" target="_blank">Tipoteca Italiana</a>, un museo dedicato alla tipografia nel quale, a quanto ho capito, oltre ad ammirare la collezione è possibile prendere parte a lezioni e prove di stampa con veri caratteri di piombo.</cite>

<cite>Qui <a href="http://bnkr.it/misc/tipoteca/1" target="_blank">le foto</cite> [imperdibili] <cite>di una visita</a>.</cite>

<cite>Sembra interessante (per un</cite> nerd <cite>come me, almeno). In più si trova in Veneto, quindi oltre al piombo ci sono concrete possibilità di assaggiare buon vino e buon cibo. ;)</cite>

Ribadisco che le foto sono imperdibili e ho ripensato a MacWrite sul primissimo Mac, quando per la prima volta si vedeva su una scrivania qualunque tutto quel gran daffare, improvvisamente, digitalizzato.