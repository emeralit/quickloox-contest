---
title: "Sindrome cinese"
date: 2006-02-03
draft: false
tags: ["ping"]
---

Chi mi conosce sa che sono un fiero avversatore di Microsoft tanto sui fatti (software di scarso merito anche quando sia di valore) quanto sui princip&icirc; (monopolisti abusatori condannati in tribunale, concorrenti sleali condannati in tribunale, retrogradi responsabili di venti anni di mancati progressi informatici anche se è solo un mio giudizio personale). In altre parole, sono sensibile alle battaglie ideali.

Detto ciò, non capisco le sollevazioni periodiche in occasione di qualche annuncio che riguarda di volta in volta Google, la stessa Microsoft, Ibm o altri quando queste aziende scendono a patti con il totalitarismo cinese per promuovere il proprio business (che c'entra Apple? C'entra. Ho visitato di persona fabbriche cinesi, per quanto di proprietà taiwanese, di quelle che Apple mette sotto contratto per produrre certi Mac).

Mi spiego. Ritengo che Microsoft sia un fattore di regresso. Allora ne faccio a meno. Il mio computer è <em>Microsoft free</em>. Che io faccia a meno di Microsoft non cambia l'universo di una virgola. Microsoft se ne strafrega e fa ugualmente soldi a palate. Però sono a posto con la mia coscienza e coerente con le mie idee.

Molti boicottaggi integrali sono realisticamente impossibili, ma questo non è un alibi per disfarsi della coerenza individuale. Vedere gente protestare contro Google e intanto cercare su Google i dati di supporto alla protesta fa ridere. La stessa gente compra tonnellate di vestiti made in China, va tranquillamente al ristorante cinese e a momenti uccide pur di risparmiare un centesimo di euro su qualunque cosa. Comprando quindi cinese senza battere ciglio.

Sarebbe prioritario, invece che prendersela con Google, protestare contro il governo cinese, totalitario, antidemocratico e sanguinario come pochi. Ma non è che debbano farlo solo gli altri. La responsabilità personale, in quanto tale, è di ciascuno e non delegabile, n&eacute; scaricabile.

Boicottare i tiranni cinesi implica qualche azione pratica, che potrebbe perfino avere - ohibò! - ricadute sul tenore di vita. Invece abbaiare contro Google in un forum costa poco e, come certe radio, fa chic e non impegna.