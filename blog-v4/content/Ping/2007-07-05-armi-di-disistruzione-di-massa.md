---
title: "Armi di disistruzione di massa"
date: 2007-07-05
draft: false
tags: ["ping"]
---

<strong>Mario</strong> le chiama <a href="http://multifinder.wordpress.com/2007/07/02/armi-improprie/" target="_blank">armi improprie</a> e, come al solito, la più letale la produce Microsoft, società che si è assegnata la missione di condurre in prima fila la battaglia per la marmellatizzazione dei cervelli.