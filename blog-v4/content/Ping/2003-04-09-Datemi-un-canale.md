---
title: "Datemi un canale"
date: 2003-04-09
draft: false
tags: ["ping"]
---

Sto scoprendo che cosa c’è dietro Sherlock 3 e mi piace

Sherlock 3 sfrutta i Web service: chiedi un dato a un server e lui ti passa il dato, il tutto in formato Xml e in modo molto più efficace che non andare a scavare in Html.

Per ora Sherlock 3 in italiano è poco sfruttato. Ma le possibilità sono innumerevoli. Anche se in inglese, il canale Sherlock di Versiontracker è di una semplicità estrema ed evita di passare dal browser ogni volta che si cerca qualcosa (se vai sul sito trovi subito il necessario per attivare i canali).

Aprire un canale Sherlock è solo questione di avere dati potenzialmente interessanti, o sapere che esistono, e conoscere benino Xml.

Allora, perché non colmiamo la lacuna e non apriamo tanti canali Sherlock per gli italiani? Sono sicuro che si venderebbero tanti Macintosh in più, tra l’altro. Nel mondo Windows anche una semplice ricerca di file è la solita cosa complicata e dal risultato imprevedibile.

<link>Lucio Bragagnolo</link>lux@mac.com