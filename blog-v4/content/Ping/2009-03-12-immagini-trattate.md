---
title: "Immagini trattate"
date: 2009-03-12
draft: false
tags: ["ping"]
---

Mi trovavo nel Passante ferroviario di Milano e alla stazione Repubblica c'era una ragazza seduta, al lavoro con un MacBook Pro in grembo.

La banchina era deserta e, nella poca luce della stazione sotterranea, la luce del portatile creava le condizioni per una fotografia suggestiva.

La ragazza però ha declinato la richiesta di poterla inquadrare e cos&#236; l'immagine posso solo ricordarla.

<strong>Kyara</strong> è stata ben più abile. Non solo è riuscita a ottenere il consenso da due amici (meglio, mici), ma li ha anche introdotti alle buone letture!

Bisogna dire che, per un gatto, studiare da Leopard è un bell'avanzamento.