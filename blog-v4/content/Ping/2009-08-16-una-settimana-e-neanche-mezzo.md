---
title: "Una settimana e neanche mezzo"
date: 2009-08-16
draft: false
tags: ["ping"]
---

Andre Torrez, <i>chief technology officer</i> in <a href="http://www.federatedmedia.net/" target="_blank">Federated Media</a> e utilizzatore di iPhone, ha deciso di <a href="http://notes.torrez.org/2009/08/google-phone-day-1.html" target="_blank">provare per un mese</a> un G1 Android in alternativa al computer da tasca di Apple.

Sei giorni dopo e ventiquattro giorni prima della scadenza, <a href="http://notes.torrez.org/2009/08/android-g1-and-day-7.html" target="_blank">si è stufato</a>.

Solo un pezzettino di tutta la storia:

<cite>Mi arrendo. Pensavo che sarebbe stato divertente provare una piattaforma differente, ma penso di avere visto più che abbastanza su questo hardware. È definitivamente troppo lento per qualsiasi attività e mi sono ritrovato a <b>non</b> usare l'apparecchio in situazioni dove ero abituato a controllare la mia posta o tenermi al passo con Twitter.</cite>

<cite>Sabato arrivava la mia famiglia in visita e mi sono ritrovato ad afferrare iPhone per trovare un ristorante, raccogliere indicazioni stradali ed effettuare un ordine online. Potendo scegliere tra i due, non potevo trattenermi dall'abbandonare Android sapendo di avere a disposizione un altro computer perfettamente adeguato.</cite>

<cite>Ho portato il G1 al lavoro ma sono tornato a casa sapendo che cosa fare. Ho rimesso la Sim su iPhone. Continuo a installare applicazioni e a portarmi dietro Android, ma la Sim sta su iPhone. Credo che G1 sia semplicemente non ancora pronto per essere lo hardware di Android.</cite>

Oggi, qui e ora, iPhone è complessivamente il meglio. Si può discutere all'infinito su questa o quella funzione, non sul complesso.