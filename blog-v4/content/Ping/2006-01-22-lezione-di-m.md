---
title: "Lezioni di m"
date: 2006-01-22
draft: false
tags: [ping]
---

Ci si creda o no, un mio carissimo amico lavora come coltivatore diretto e, l’ultima volta a cena, mi ha intrattenuto con una lunga digressione sui fertilizzanti.

Lui decisamente non ha peli sulla lingua e mi ha spiegato come, alla fine, quando si parla di concime c’è sempre di mezzo la m…, eppure non tutti i concimi sono uguali e alla fine la frutta e la verdura spuntano con mille qualità e mille sapori diversi. La m…, concludeva con un certo compiacimento, è sempre m…, ma chi la sa trattare meglio ottiene risultati migliori.

Ho pensato a certi ragionamenti che sento, per i quali un Mac e un Pc sono uguali perché i componenti sono tutti uguali (in parte, in verità), e ho sorriso compiaciuto anch’io.