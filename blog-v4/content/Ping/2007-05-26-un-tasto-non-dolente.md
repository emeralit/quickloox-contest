---
title: "Un tasto non dolente"
date: 2007-05-26
draft: false
tags: ["ping"]
---

In tre giorni ho digitato a grande velocità e discreta violenza circa 110 kilobyte di testo.

Per BBEdit sono noccioline. Per la tastiera di un PowerBook, invece, non è detto. E devo dire che la prova è stata superata alla grande.