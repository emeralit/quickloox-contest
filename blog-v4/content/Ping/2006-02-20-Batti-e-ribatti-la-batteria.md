---
title: "Batti e ribatti la batteria"
date: 2006-02-20
draft: false
tags: ["ping"]
---

Come promesso, la situazione aggiornata al capitolo batterie:

<table>
	<tr>
		<th></th> <th>Months</th> <th>Loadcycles</th> <th>Percentage</th>
	</tr>
	<tr>
		<td>Lux</td> <td>13</td> <td>123</td> <td>84</td>
	</tr>
	<tr>
		<td>Francos</td> <td>4</td> <td>74</td> <td>99</td>
	</tr>
	<tr>
		<td>Jacopo</td> <td>13</td> <td>181</td> <td>49</td>
	</tr>
	<tr>
		<td>Daniele</td> <td>38</td> <td>1193</td> <td>50</td>
	</tr>
	<tr>
		<td>Giancarlo</td> <td>13</td> <td>118</td> <td>19</td>
	</tr>
	<tr>
		<td>Matteo</td> <td>33</td> <td>192</td> <td>78</td>
	</tr>
	<tr>
		<td>Andrea</td> <td>31</td> <td>285</td> <td>75</td>
	</tr>
	<tr>
		<td>Miki</td> <td>12</td> <td>131</td> <td>98</td>
	</tr>
	<tr>
		<td>Citrullo</td> <td>21</td> <td>177</td> <td>85</td>
	</tr>
	<tr>
		<td>Luca</td> <td>22</td> <td>203</td> <td>90</td>
	</tr>
	<tr>
		<td>Elio</td> <td>24</td> <td>372</td> <td>55</td>
	</tr>
	<tr>
		<td>Vincenzo</td> <td>8</td> <td>95</td> <td>96</td>
	</tr>
	<tr>
		<td>Andrea2</td> <td>48</td> <td>337</td> <td>81</td>
	</tr>
	<tr>
		<td>Caruso_G</td> <td>2</td> <td>75</td> <td>101</td>
	</tr>
	<tr>
		<td>Pierpa</td> <td>15</td> <td>229</td> <td>80</td>
	</tr>
	<tr>
		<td>Mario</td> <td>28</td> <td>643</td> <td>0</td>
	</tr>
	<tr>
		<td>Fabrich</td> <td>28</td> <td>159</td> <td>84</td>
	</tr>
	<tr>
		<td>Armando</td> <td>9</td> <td>33</td> <td>100</td>
	</tr>
	<tr>
		<td>Armando2</td> <td>17</td> <td>37</td> <td>85</td>
	</tr>
	<tr>
		<td>Todi</td> <td>37</td> <td>204</td> <td>77</td>
	</tr>
	<tr>
		<td>Sergio</td> <td>47</td> <td>88</td> <td>18</td>
	</tr>
	<tr>
		<td>Paolo</td> <td>27</td> <td>48</td> <td>59</td>
	</tr>
	<tr>
		<td>AndreaR</td> <td>21</td> <td>143</td> <td>92</td>
	</tr>
	<tr>
		<td>Gimli</td> <td>20</td> <td>434</td> <td>61</td>
	</tr>
	<tr>
		<td>Ellebi</td> <td>38</td> <td>502</td> <td>77</td>
	</tr>
	<tr>
		<td>Sistovmac</td> <td>9</td> <td>152</td> <td>96</td>
	</tr>
	<tr>
		<td>Telemaco</td> <td>31</td> <td>17</td> <td>98</td>
	</tr>
	<tr>
		<td>Fede</td> <td>50</td> <td>36</td> <td>100</td>
	</tr>
	<tr>
		<td>Daniele</td> <td>15</td> <td>704</td> <td>73</td>
	</tr>
	<tr>
		<td>Salvo</td> <td>10</td> <td>117</td> <td>91</td>
	</tr>
	<tr>
		<td>Fearandil</td> <td>33</td> <td>496</td> <td>99</td>
	</tr>
	<tr>
		<td>Antonio</td> <td>21</td> <td>144</td> <td>93</td>
	</tr>
	<tr>
		<td>Andrea f</td> <td>22</td> <td>410</td> <td>85</td>
	</tr>
	<tr>
		<td>Davide</td> <td>7</td> <td>47</td> <td>100</td>
	</tr>
	<tr>
		<td>Franco</td> <td>59</td> <td>126</td> <td>31</td>
	</tr>
	<tr>
		<td>Andy-r</td> <td>20</td> <td>323</td> <td>100</td>
	</tr>
	<tr>
		<td>Francesco</td> <td>1</td> <td>8</td> <td>99</td>
	</tr>
	<tr>
		<td>Niguli</td> <td>35</td> <td>1421</td> <td>33</td>
	</tr>
	<tr>
		<td>Madhunt</td> <td>11</td> <td>165</td> <td>93</td>
	</tr>
	<tr>
		<td>Kiza</td> <td>29</td> <td>384</td> <td>82</td>
	</tr>
	<tr>
		<td>Claudio</td> <td>25</td> <td>428</td> <td>35</td>
	</tr>
	<tr>
		<td>Marco</td> <td>4</td> <td>18</td> <td>97</td>
	</tr>
	<tr>
		<td>Vippo</td> <td>13</td> <td>135</td> <td>95</td>
	</tr>
	<tr>
		<td>Roberto</td> <td>16</td> <td>105</td> <td>97</td>
	</tr>
	<tr>
		<td>Palmy</td> <td>12</td> <td>291</td> <td>97</td>
	</tr>
	<tr>
		<td>Claudio c</td> <td>25</td> <td>180</td> <td>67</td>
	</tr>
	<tr>
		<td>Gianluigi</td> <td>22</td> <td>235</td> <td>100</td>
	</tr>
	<tr>
		<td>Ugo</td> <td>5</td> <td>130</td> <td>96</td>
	</tr>
	<tr>
		<td>Madhunt</td> <td>11</td> <td>165</td> <td>93</td>
	</tr>
	<tr>
		<td>Mac Frank</td> <td>13</td> <td>152</td> <td>94</td>
	</tr>
	<tr>
		<td>Francesco p</td> <td>29</td> <td>631</td> <td>35</td>
	</tr>
	<tr>
		<td>Ayrtonoc</td> <td>7</td> <td>24</td> <td>98</td>
	</tr>
	<tr>
		<td>Silvia</td> <td>30</td> <td>262</td> <td>76</td>
	</tr>
	<tr>
		<td>Cristiano</td> <td>45</td> <td>217</td> <td>13</td>
	</tr>
	<tr>
		<td>Cristiano2</td> <td>22</td> <td>386</td> <td>79</td>
	</tr>
</table>
