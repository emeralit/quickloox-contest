---
title: "L'Internazionale di Quiliano"
date: 2005-05-29
draft: false
tags: ["ping"]
---

Piovono riconoscimenti per il primo museo Apple in Europa

Alessio Ferraro dell'All About Apple Club segnala la pubblicazione di una bella <a href="http://www.reuters.com/locales/c_newsArticle.jsp?type=internetNews&localeKey=it_IT&storyID=8589745">intervista</a> riguardante l'<a href="http://www.allaboutapple.com/museo/museo.htm">All About Apple Museum</a>, pubblicata da Reuters. E anche un articolo apparso su Diario, sempre relativo al museo.

Personalmente non vedo l'ora che a Quiliano finisca per arrivare Steve Jobs a rendere al museo, nonché a tutti quelli che ci hanno lavorato dietro come matti, l'onore meritatissimo.

Nel frattempo, tanti complimenti ancora. <em>Seriously</em>.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>