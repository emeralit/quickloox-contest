---
title: "Io lo avrei fatto meglio"
date: 2009-12-19
draft: false
tags: ["ping"]
---

<i>Wired</i> ha pubblicato una <a href="http://www.wired.com/gadgets/mac/news/2004/07/64286?currentPage=all" target="_blank">retrospettiva della nascita di iPod</a> con molti particolari inediti.

Tony Fadell ebbe intorno al 2001 l'idea di creare un lettore musicale legato a un servizio di vendita di musica e costruirvi attorno un'azienda. Esperto di hardware e già progettista di varie apparecchiature digitali presso General Magic, Fadell lasciò il suo contratto con Philips e iniziò a bussare alla porta delle aziende per vendere la propria idea. Tutte lo ignorarono eccetto una, che gli affidò un team di progetto di trenta persone.

I prototipi realizzati da PortalPlayer, società esterna contattata da Fadell per sviluppare materialmente il prodotto, erano sostanzialmente orribili, pieni di pulsanti, ma c'era già dentro un sistema operativo e questo era un punto interessante per chi aveva creduto nella sua idea.

PortalPlayer era stata approcciata da dodici aziende diverse, che partivano dal progetto iniziale per elaborare un proprio lettore. Tra queste Teac e Ibm; quest'ultima aveva un bellissimo <i>concept</i>, con schermo circolare (<i>wow</i>!) e auricolari Bluetooth (doppio <i>wow</i>!).

A un certo punto si fece viva l'unica azienda che Fadell era riuscito a convincere e, da quel momento, le altre vennero abbandonate.

L'interfaccia utente del nascente iPod era insoddisfacente e venne rifatta da zero, in tre mesi, dall'azienda in questione.

L'amministratore delegato dell'azienda iniziò a dedicarsi a tempo pieno al progetto. Si arrabbiava se ci volevano più di tre passaggi per raggiungere un brano, o se la comparsa dei menu non era abbastanza veloce.

L'azienda non aveva minimamente in programma di aggiungere la protezione anticopia ai brani; quella fu imposta più tardi dalle case discografiche al momento di avviare iTunes Music Store.

I prototipi venivano fatti collaudare nella massima segretezza, nascosti in scatole di plastica grandi come scatole da scarpe, su cui venivano montati controlli in posizioni sempre diverse, cos&#236; che fosse impossibile immaginare il <i>design</i> finale dell'oggetto.

Se avessi un soldino per tutti quanti ho sentito descrivere soluzioni facili ai difetti presunti di iPod, sarei ricco. Chissà dov'erano quando si trattava di crearlo.