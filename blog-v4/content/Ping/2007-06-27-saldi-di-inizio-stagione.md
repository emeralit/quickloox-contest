---
title: "Saldi di inizio stagione"
date: 2007-06-27
draft: false
tags: ["ping"]
---

Come mi ha fatto notare <a href="http://www.accomazzi.it" target="_blank">Akko</a>, nella pagina dove Amazon Uk vende a prezzo stracciato le <a href="http://www.amazon.co.uk/gp/feature.html/ref=amb_link_41373965_10/203-7411815-7904759?ie=UTF8&amp;docId=1000068893&amp;pf_rd_m=A3P5ROKL5A1OLE&amp;pf_rd_s=center-1&amp;pf_rd_r=1CQ6RJ3FXWT7AJ8PNXHF&amp;pf_rd_t=101&amp;pf_rd_p=139964591&amp;pf_rd_i=468294" target="_blank">eccedenze di magazzino</a>, fino a un momento fa spiccava la confezione Upgrade di <a href="http://www.amazon.co.uk/Microsoft-Windows-Vista-Basic-Upgrade/dp/B000KCIA2I/ref=br_lf_m_1000068893_1_3_ttl/026-9506597-0502034?%5Fencoding=UTF8&amp;s=software&amp;pf_rd_m=A3P5ROKL5A1OLE&amp;pf_rd_s=center-2&amp;pf_rd_r=1QEF8JXA2BTHF3P04JMK&amp;pf_rd_t=1401&amp;pf_rd_p=141370191&amp;pf_rd_i=1000068893" target="_blank">Microsoft Windows Vista Home Basic Edition</a>.

Per un oggetto che ha richiesto cinque anni di ritardi nella messa a punto, finire svenduto nel giro di pochi mesi non è male.

O sarà il titolo, di ben sei parole. Non so se perfino Adobe abbia tutta questa fantasia.