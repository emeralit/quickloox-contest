---
title: "Un'avventura appena diversa dalle altre"
date: 2006-06-20
draft: false
tags: ["ping"]
---

Digita <code>emacs -batch -l dunnet</code> nel Terminale e prova l'unica <a href="http://www.rickadams.org/adventure/a_history.html" target="_blank">avventura testuale</a> che conosca a prevedere l'uso di una scheda logica di <a href="http://www.webmythology.com/VAXhistory.htm" target="_blank">Vax</a> per arrivare fino in fondo.

Non è difficile ma discretamente istruttiva e comunque, se sei disperato, c'è anche la <a href="http://www.gamesover.com/walkthroughs/Dunnet.txt" target="_blank">soluzione</a>.