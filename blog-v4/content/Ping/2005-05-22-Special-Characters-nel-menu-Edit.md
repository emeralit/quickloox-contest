---
title: "Caratteri facili"
date: 2005-05-22
draft: false
tags: ["ping"]
---

È facile non accorgersi di tutte le migliorie di Tiger

Mi serviva una schermata della palette Unicode di scelta dei caratteri. Ho pasticciato con le Preferenze di Sistema per inserirla nei menu grafici in alto a destra e, alla fine, mi sono accorto che adesso, nei programmi Cocoa, l'ultimo elemento del menu Composizione è sempre esattamente quello lì.

Le novità di Tiger sono talmente tante e talmente pervasive che alla fine uno ce la ha anche sotto il naso e non se accorge.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>