---
title: "Restauri stellari"
date: 2004-09-24
draft: false
tags: ["ping"]
---

Per girare un capolavoro della fantascienza un Mac non basta. Ma seicento per salvarlo, sì

Il Dvd della prima trilogia di Guerre Stellari, che impazza per la distribuzione in questi giorni, è stato realizzato a prezzo di un faticoso e sapiente restauro delle pizze originali dei film (dove si sono nascosti tutti quanti sostengono che i media analogici si conservano meglio?).

La faccio breve: è tutto merito di una <a href="http://www.dvdweb.it/index.mv?1095613695_414DBCFF0000EEC700007FAD00000000_80.117.33.177+View_News+20040914161740">quantità spaventevole di Macintosh</a>.

La Mela dà Forza.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>