---
title: "Victor Victorius"
date: 2010-10-20
draft: false
tags: ["ping"]
---

Mi scrive <b>Victor</b> proponendomi l'inserimento nel Cd di Macworld di Vogue Mk2, sintetizzatore di sua produzione, giusto alla versione 1.3.

<cite>Dotato di un'architettura ibrida (Virtual Analog e Wavetable) con tre oscillatori indipendenti e funzionalità microtonali, questo</cite> synth <cite>è semplicissimo da usare perché si autoconfigura in base agli ingressi Midi disponibili e trasforma il Mac in un potente sintetizzatore a otto voci.</cite>

<cite>È anche il primo sintetizzatore non sperimentale che permette di definire l'escursione del</cite> pitch-bend <cite>in toni della scala microtonale di riferimento anziché in</cite> cents<cite>, e il</cite> package <cite>include una</cite> library <cite>di oltre tremila file di intonazione comprendente intonazioni classiche &#8220;storiche&#8221; e scale microtonali contemporanee. I file di intonazione sono editabili con un altro software di mia produzione che ha ormai più di sette anni (<a href="http://www.apple.com/downloads/macosx/audio/maxmagicmicrotuner.html" target="_blank">Max Magic Microtuner</a>), citato un paio di volte da John Gruber su Daring Fireball come</cite> user interface of the week <cite>(!).</cite>

<cite>Qui si trovano <a href="http://groups.yahoo.com/group/16tone" target="_blank">supporto e download page "ufficiale" su Yahoo</a>, nonché <a href="https://files.me.com/cerullo/3zd1u6" target="_blank">direct download URL della versione più recente (1.3)</a>.</cite>

Di sintetizzatori non sono esperto e gradirò molto il parere di chi ne sa. Grazie in anticipo.