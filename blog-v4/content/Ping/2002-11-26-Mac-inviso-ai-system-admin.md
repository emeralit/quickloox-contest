---
title: "La verità sul Mac in azienda"
date: 2002-11-26
draft: false
tags: ["ping"]
---

Perché l’amministratore di sistema fa la guerra a Macintosh

Spesso ci si chiede perché nelle aziende, fuori dai reparti grafici, sia così difficile che venga usato Mac. Una delle ragioni l’ha vissuta Pino:

“Per la prima volta oggi ho fatto la presentazione di un sito internet con un iBook nuovo di pacca con Jaguar installato, invece della solita carta. Mi sono presentato in un posto dove c’è un esercito di computer impressionante, tutti PC. L’avvento di questo coso lattiginoso ha provocato uno scompiglio incredibile. Aspettando il tizio con cui avevo appuntamento, mi sono intrufolato nell’area dove lavorano i sistemisti. Chiacchierando, ho preso un cavetto libero della LAN. l’ho attaccato all’iBook e questo, in un picosecondo, ha configurato tutta la rete. Sul monitor sono comparsi tutti i computer, le stampanti, gli scanner, i plotter, insomma senza installare un accidenti ero in grado di scambiare dati con chiunque in tutto il bacino del Mediterraneo, entrando direttamente nei computer di centinaia di ricercatori sparsi in cinque sedi diverse, e stampare sulla laser del presidente in Francia. Il commento preoccupato dell’amministratore della rete è stato: ‘se questo coso fa il nostro lavoro, noi ce ne possiamo andare a casa’”.

<link>Lucio Bragagnolo</link>lux@mac.com