---
title: "Quei comandi diversi"
date: 2008-04-05
draft: false
tags: ["ping"]
---

Meraviglioso pezzo di Macworld Usa su alcuni comandi di Terminale <a href="http://www.macworld.com/article/132556/2008/04/geekfactor2504.html" target="_blank">arrivati nuovi nuovi in Leopard</a> (e neanche elencati nelle famose trecento novità).

<code>dot_clean</code> sistema i file che iniziano con <code>._</code> e potrebbe fare la felicità di chi lavora spesso su reti miste.

<code>kextfind</code> elenca le estensioni di sistema e tornerà utile in caso di problemi con le periferiche, o kernel panic inspiegabili.

<code>pkgutil</code> fruga dentro i pacchetti di installazione. Perfetto per quanti non credono se non toccano con mano.

<code>systemsetup</code> e <code>networksetup</code> mostrano e configurano vari parametri di rete e di Preferenze di Sistema. Erano anche in Mac OS X 10.4, ma andavano trovati con impegno. Adesso sono stati resi accessibili a chiunque.

Ampiamente consigliato lo studio del manualetto online per ciascun comando, con <code>man <nomecomando></code>.