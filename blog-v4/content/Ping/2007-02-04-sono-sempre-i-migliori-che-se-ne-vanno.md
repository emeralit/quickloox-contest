---
title: "Sono sempre i migliori che se ne vanno"
date: 2007-02-04
draft: false
tags: ["ping"]
---

Ma quelli veramente eccellenti ritornano. Blue ha ripreso a <a href="http://www.maclovers.bluebottazzi.com/" target="_blank">bloggare</a> (lo faceva già, ma non si chiamava bloggare) e in men che non si dica ha anche intervistato <a href="http://www.erix.it/" target="_blank">Enrico Colombini</a>.

Voglio molto bene a Blue e per questo discutiamo spesso (le persone intelligenti, come è lui, sanno distinguere tra le idee e le persone). Voglio bene a Enrico, una delle poche persone veramente geniali che conosco, anche se mi deve una cena.

Se proprio dovessi rimproverargli qualcosa, è che <a href="http://www.erix.it/avventure.html" target="_blank">Avventura nel castello</a> non esiste in Lisp. Tanto so che non la fa, voglio solo vedere se sono il primo a chiedergliela in Lisp.