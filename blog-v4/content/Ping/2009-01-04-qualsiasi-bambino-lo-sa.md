---
title: "Qualsiasi bambino lo sa"
date: 2009-01-04
draft: false
tags: ["ping"]
---

È commovente vedere i concorrenti di iPod touch e iPhone sfornare oggetti tutto schermo con i pulsanti software e pensare che basti somigliare al modello per averne le qualità.

Steven Shankland, autore del blog Unexposed, racconta di come <a href="http://news.cnet.com/8301-13580_3-10128407-39.html?part=rss&amp;subj=news&amp;tag=2547-1_3-0-20" target="_blank">suo figlio di tre anni abbia acquisito padronanza di iPhone</a> proprio grazie all'interfaccia utente superiore e che sia il miglior collaudatore del software prodotto per l'apparecchio, proprio perché un bambino piccolo fa solo cose che siano perfettamente intuitive e semplici e rispondano ai criteri ideali di interfaccia.

Cos&#236;, quando il piccolo Levi prova a usare l'accelerometro in <a href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=293886459&amp;mt=8" target="_blank">JellyCar</a> e non riesce a ottenere il risultato voluto, si capisce rapidamente se sia lui troppo piccolo oppure il programma mal progettato.

Gli altri pensano che basti aggiungere la dizione <em>touch</em> sulla scatola. Non basta e lo capisce persino un bambino.