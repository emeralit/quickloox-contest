---
title: "La Divina Commedia del software"
date: 2003-06-10
draft: false
tags: ["ping"]
---

Diviso tra il fascino perverso del peccato e le delizie del paradiso della produttività

Wacom e Corel, con la collaborazione di Fleishman-Hillard (grazie Stefania, grazie Francesca!) mi hanno concesso di provare una tavoletta-schermo grafico Wacom Cintiq 18SX con Painter 8 e una Corel Graphics Suite.

Della tavoletta-schermo (non so come chiamarla, visto che si lavora su un Lcd 18” a mo’ di tela su cavalletto) parlerò diffusamente nei prossimi appuntamenti, perché merita davvero. Rispetto al software, ho due primissime impressioni.

La prima è che sono rimasto indietro un bel po’. Compro e uso solamente i programmi che mi servono e sono un professionista del testo, non della grafica. Ma la mia signora (grazie – un’altra! – Stefania!), che frequenta l’Accademia delle Belle Arti a Milano Brera, è letteralmente rimasta conquistata dal ben di Dio che sta dentro i programmi succitati. Davvero, c’è progresso, e tanto.

La seconda è che Macintosh è una piattaforma straordinaria (ossia fuori dall’ordinario) perché da una parte mi attira nei sotterranei di Unix , dove giacciono tesori incredibili e inesplorati; dall’altra mi offre un ambiente amico, semplice e funzionante su cui appoggiare suite come quella di Corel, con un potenziale pazzesco di produttività. E non ditemi che l’insieme funziona come su Windows: vedo un sacco di gente che usa Windows e non è proprio vero.

Tirato per una manica dagli inferi – ma quanto affascinanti – di Unix e per l’altra dalle delizie e le letizie delle Applicazioni Professionali, mi godo l’attimo, confuso e felice, stilo in mano e una tela da 18” davanti.

<link>Lucio Bragagnolo</link>lux@mac.com