---
title: "Q prodest?"
date: 2006-03-23
draft: false
tags: ["ping"]
---

Ci sono persone che agognano il giorno in cui potranno sprecare una fetta di disco sul Mac per farne una partizione Windows e così iniziare felicemente a sprecare tempo e qualità della vita per combattere contro malfunzionamenti e virus allo stato dell’arte.

Fossi loro, lancerei <a href="http://www.kberg.ch/q/" target="_blank">Q</a>.