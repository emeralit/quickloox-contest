---
title: "Fai da te ad alto rischio"
date: 2002-03-03
draft: false
tags: ["ping"]
---

Chi ha il coraggio di osare può risolvere da solo un bug di Mac OS X

Mac OS X va in kernel panic quando si cerca di accedere a un programma nativo Mac OS X dall’ambiente Classic via Tcp/Ip.
La soluzione esiste; ma solo in Darwin, la parte open source di Mac OS X. Non è ancora stata inserita nel sistema operativo, ma chi ha fegato e capacità di usare i Developer Tools può provare da solo.
Ecco come: ci si collega al <link>sito Darwin</link>http://www.publicsource.apple.com/ e si reperisce il progetto SharedIp dal server Cvs. Si esegue un build del pacchetto con i Developer Tools e si ottiene una kernel extension da mettere in /System/Library/Extensions. Si riavvia e il gioco è fatto.
Chi non ha capito bene tutti i termini e le operazioni qui menzionate, per favore, non provi a fare pasticci sulla propria macchina o quella di altri, e si accontenti di constatare che l’open source ha i suoi vantaggi. Grazie. :-)

<link>Lucio Bragagnolo</link>lux@mac.com