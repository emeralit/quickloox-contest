---
title: "O cavo, cavo cavo, cavo cavo…"
date: 2004-12-08
draft: false
tags: ["ping"]
---

L'attenzione di Apple ai particolari… e agli accessori

Ho appena aperto, per curiosità, la scatola di un iPod photo, e ho constatato la presenza di un cavo video. Sembra ovvio, a posteriori, dato che iPod photo è previsto anche per proiettare uno slideshow.

Ma il cavo video è proprio bello. Non comprerei un iPod photo a minimo 559 euro per avere il cavo, ma insomma, persino quello dà soddisfazione.

<em>*Elio e le Storie Tese. Hanno un iPod ciascuno.</em>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>