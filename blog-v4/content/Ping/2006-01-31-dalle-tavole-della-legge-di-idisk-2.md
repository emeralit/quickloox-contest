---
title: "Dalle tavole della legge di iDisk/2"
date: 2006-01-31
draft: false
tags: ["ping"]
---

Lavora quando gli americani dormono. Spesso iDisk è più reattivo quando in Nordamerica è notte e, presumibilmente, una quantità di abbonati consistente è a dormire invece che a usare .mac.