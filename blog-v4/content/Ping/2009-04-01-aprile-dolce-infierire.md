---
title: "Aprile, dolce infierire"
date: 2009-04-01
draft: false
tags: ["ping"]
---

Si approssima l'ora di pranzo.

Approfittane per scaricare <a href="http://www.macupdate.com/info.php/id/21204" target="_blank">Popcorn Kernel</a>, incollargli sopra l'icona di una applicazione molto usata, cambiare anche il nome, recarsi sul Mac di un amico/collega/parente/compagno e sostituire discretamente l'applicazione vera con quella fittizia.

Attendere che la vittima caschi nello scherzo e vedere se capisce, o se invece prepara il suicidio. Nel caso, salvarla in tempo.

Declino ogni responsabilità. :-)