---
title: "Stiamo lavorando per voi"
date: 2006-06-16
draft: false
tags: ["ping"]
---

&Egrave; importante (certamente non rispetto all'ordine universale delle cose eppure altrettanto certamente guardando la piccola quotidianità del nostro Mac) che scriva in fretta questa piccola recensione di HoudahSpot e che altri la leggano. il programma appartiene alla classe dei miglioramenti alle funzioni di Spotlight e appare semplice (forse ancora un po' troppo spartano nell'interfaccia) ma interessante. Consente una flessibilità notevole nell'effettuare le ricerche, che restano vive e si aggiornano automaticamente fino a quando non stoppiamo l'applicazione di quei criteri di ricerca. Il risultato è una finestra tipo inspector, un po' da programmatore ma veramente completa nel raccontare tutto quello che è metadati riguardanti il file o i file trovati.

Lo raccomando e raccomando anche di bloggare al più presto <a href="http://www.houdah.com/houdahspot/" target="_blank">HoudahSpot</a>. E poi visitare <a href="http://www.maczot.com/" target="_blank">MacZot</a>. Stiamo lavorando anche per quelli che non lo faranno. :-)