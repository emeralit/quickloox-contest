---
title: "Non solo sprovveduti"
date: 2003-06-12
draft: false
tags: ["ping"]
---

Ognuno ha il diritto di farsi male usando Windows, ma non di mettere a rischio gli altri

Negli ultimi giorni mi sono arrivate una fattura, un paio di password di registrazione a servizi e varie comunicazioni confidenziali.

Non sono diventato una succursale della Cia ma, semplicemente, il mio indirizzo risiede nella casella postale di qualche utente Windows.

Da giorno un nuovo virus, Bugbear.B o qualcosa del genere, impazza sui computer Windows di tutto il mondo e spara pezzi di posta elettronica del contagiato a tutti quelli che si trovano sul suo indirizzario.

Per essere vittima di Bugbear.B bisogna usare Windows e usare Outlook Express configurato in modo da aprire automaticamente gli allegati, oppure usare Windows con un programma di posta qualunque e aprire manualmente gli allegati senza attenzione e senza criterio.

Chi usa Mac OS X o, se è per quello, qualsiasi cosa diversa da Windows, non corre nessun rischio.

Magari tra poco mi arriveranno un estratto di conto corrente, o una password di home banking, o una lettera d’amore. Certo non li userò, ma un sacco di malintenzionati là fuori sanno bene che cosa farne. E la colpa di tutto quello che potrà succedere sarà di sprovveduti che usano un sistema operativo da sprovveduti

Quando vedi uno sprovveduto che usa Windows, è ora di dirglielo: non solo sei uno sprovveduto, ma ora stai mettendo a repentaglio la privacy di gente che non c’entra. Se non sei capace, togliti di torno, che complichi la vita agli altri. O cambia sistema operativo e prendine uno più sicuro e più semplice da usare.

<link>Lucio Bragagnolo</link>lux@mac.com