---
title: "Stampo che mi passa"
date: 2007-11-10
draft: false
tags: ["ping"]
---

Mi aspettavo che l'installazione di Leopard azzerasse le mie impostazioni di stampa, perché mi era stato confermato da un sacco di gente. Mi andava benissimo perché avevo accumulato fin troppa spazzatura.

Ma che andasse cos&#236; bene non me lo aspettavo.

Il servizio di stampa ha visto subito la stampante condivisa (attaccata al vecchio iMac con Tiger), ha cercato da solo automaticamente eventuali driver e di fatto per stampare mi è bastato Comando-P.