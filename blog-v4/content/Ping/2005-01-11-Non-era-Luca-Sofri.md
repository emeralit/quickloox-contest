---
title: "Castronerie da Macworld Expo"
date: 2005-01-11
draft: false
tags: ["ping"]
---

Quando Apple sta per annunciare novità nessuno capisce più niente, neanch'io

Entro poche ore Steve Jobs avrà deliziato la platea del Moscone Center di San Francisco con il suo <em>keynote</em> in occasione del Macworld Expo di gennaio 2005.

Sempre entro poche ore un sacco di persone dovranno rimangiarsi decine di scempiaggini raccontate credendo a siti di dubbia reputazione ma più che altro poco (e male) informati.

Io faccio la mia parte in anticipo: l'articolo <em>Orgoglio Apple</em> sulla prima pagina de Il Foglio di sabato non era di Luca Sofri. La fonte della correzione è ineccepibile. Sempre che Luca Sofri non mi stesse prendendo in giro… :-)

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>