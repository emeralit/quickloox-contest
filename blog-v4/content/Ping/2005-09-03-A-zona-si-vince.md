---
title: "A zona si vince"
date: 2005-09-03
draft: false
tags: ["ping"]
---

Dove trovare i programmatori Mac italiani

Grazie a Luca Accomazzi per avermi rivelato l'esistenza della Macintosh Italian Programmers Zone, o <a href="http://www.mipz.it">Mipz</a>.

Uno dei fondatori è il grande Alessandro Levi Montalcini, uno degli autori italiani più geniali e prolifici in campo Mac. Il sito appare fresco di nascita, ma mi stupirei se non crescesse in fretta.

A tutti noi sta innaffiarlo di attenzioni e sostegno. Che ci siano tanti programmatori e buoni programmatori fa solo bene a tutti.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>