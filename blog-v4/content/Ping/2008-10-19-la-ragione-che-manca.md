---
title: "La ragione che manca"
date: 2008-10-19
draft: false
tags: ["ping"]
---

Ho letto numerose critiche alla mancanza di FireWire sui nuovi MacBook.

Eppure nessuno ha citato una mancanza fondamentale: senza FireWire non si può allestire una rete FireWire, comoda e veloce in molte situazioni.

Qualcuno dirà <em>mai allestita una rete FireWire</em>. Occhio, che qualcuno potrebbe dire <em>mai usato il</em> target mode. :-)

In altre parole, il mio schema di utilizzo del Mac non è necessariamente il migliore possibile, ma uno dei tanti.