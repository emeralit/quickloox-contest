---
title: "Visita infinita"
date: 2006-01-28
draft: false
tags: [ping]
---

Una delegazione dell’All About Apple Museum di Quiliano ha visitato Cupertino distribuendo gadget e attestati di stima a tutti, compresi Steve Jobs e Steve Wozniak a cui sono stati recapitati pacchi extra.

Hanno avuto il coraggio non di filmare, ma di pubblicare il filmato del loro arrivo al campus Apple. Mancava solo che prendessero in parola la dizione Infinite Loop!