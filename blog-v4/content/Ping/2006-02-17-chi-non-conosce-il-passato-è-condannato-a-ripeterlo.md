---
title: "Chi non conosce il passato è condannato a ripeterlo"
date: 2006-02-17
draft: false
tags: ["ping"]
---

Magistrale questa <a href="http://homepage.mac.com/riccardo.mori/blogwavestudio/LH20050218022524/LHA20060210142004/index.html" target="_blank">bloggata di Riccardo</a>. Niente di meglio, per capire le problematiche della transizione da PowerPc a Intel, che leggersi gli articoli sulle riviste, che spiegano tutto. Chiaro: le riviste del 1993.

Altrimenti si finisce come quanti oggi non fanno che ripetere le stesse stupidaggini che si sono sentite allora. Avessero vent'anni sarebbero scusati. Ma se sono venti per gamba, c'è da riflettere.