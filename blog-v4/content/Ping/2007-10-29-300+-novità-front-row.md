---
title: "300+ novità: Front Row"
date: 2007-10-29
draft: false
tags: ["ping"]
---

Il telecomando di Apple, in Leopard, ha acquisito la stessa interfaccia di Apple Tv (o forse il contrario).

Front Row consente il playback dei Dvd, per la serie: il film me lo guardo <em>veramente</em> dalla poltrona.

Come pare ovvio, si fa la stessa cosa anche con iTunes, e anche con i <em>trailer</em> dei film di cassetta, direttamente in arrivo dal sito di QuickTime.

La resa sintetica della <a href="http://www.apple.com/it/macosx/features/300.html" target="_blank">pagina delle trecento</a> (e oltre) migliorie di Leopard prosegue.