---
title: "Finalmente ci hanno pensato"
date: 2009-04-06
draft: false
tags: ["ping"]
---

Intendo partecipare attivamente all'attività di <a href="http://wrongtomorrow.com/category/technology" target="_blank">Wrong Tomorrow</a>: segnarsi le predizioni tecnologiche (il sito lavora anche in altri campi peraltro) e vedere se si avverano.

Manca però l'indicazione percentuale, una specie di indice di affidabilità, delle predizioni esatte e di quelle sbagliate. Sbagliare fa parte della vita; l'importante è funzionare meglio di una moneta lanciata per aria, che sbaglia la metà delle volte.