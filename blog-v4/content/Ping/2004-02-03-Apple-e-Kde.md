---
title: "Quando si dice open source"
date: 2004-02-03
draft: false
tags: ["ping"]
---

Apple insegue il giusto profitto, ma senza scordarsi della comunità

Sembra una contraddizione, un’azienda come Apple, la cui ragione dell’esistenza è fatturare e crescere, che fa software open source, come Darwin. Può sembrare una scelta di comodo, o di facciata. Anche perché nella comunità open source vige il principio del give back: bisognerebbe sempre impegnarsi per dare alla comunità in cambio di quanto la comunità dà a noi.

Poi uno legge (grazie, Fearandil!) del rilascio di <link>Kde 3.2</link>http://dot.kde.org/1075813576, una delle interfacce grafiche open source più di tendenza. E, in una pagina non certo opera del marketing di Apple, vede scritto (traduco):

In collaborazione con il team Safari di Apple Computer, il supporto web di Kde ha visto consistenti incrementi di prestazioni così come migliore aderenza a standard ampiamente accettati su Internet.

Apple fa open source. E fa give back.

<link>Lucio Bragagnolo</link>lux@mac.com