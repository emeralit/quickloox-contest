---
title: "Lunga vita alle RIMM"
date: 2003-02-24
draft: false
tags: [ping]
---

Sis si è accodata alla cordata di Samsung, Asus e Rambus per aiutare nella diffusione della prossima generazione di chipset in grado di supportare le memorie RDRAM. Il Sis 659 supporterà le RIMM a 1,2 GHz che saranno in grado di garantire una banda passante di 9,6 gigabyte al secondo, ovvero circa il doppio di quella garantita dai modelli in commercio. Il chipset di Sis sarà in grado di supportare fino a 16 gigabyte di RAM, ma l’accordo delle quatro società potrebbe essere "molto di più di un semplice accordo di marketing", almeno secondo alcuni analisti. Ma né Samsung né Asus producono chipset per proprio conto, perciò Sis rappresenta il tassello mancante. Asus infatti potrebbe mettere a disposizione il reparto di produzione delle schede madri, Samsung la capacità di stampare RAM, Rambus la tecnologia e Sis la piattaforma. I primi sample del 659 dovrebbero arrivare nel terzo trimestre dell’anno.
