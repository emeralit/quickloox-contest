---
title: "Tutto va ben, madama Panther"
date: 2006-04-23
draft: false
tags: ["ping"]
---

L'iMac di emergenza, equipaggiato con Mac OS X 10.3.9, ha superato in perfette condizioni l'ultimo aggiornamento di sicurezza e l'update di iTunes.