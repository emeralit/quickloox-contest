---
title: "Leggere fa sempre bene"
date: 2006-07-20
draft: false
tags: ["ping"]
---

Una delle risorse web meno frequentate da chi usa Mac è il sito Apple. Eppure ci si trovano diverse cose utili. Per esempio i <a href="http://www.apple.com/it/pro/tips/" target="_blank">consigli avanzati</a>. Basta togliere il <code>/it</code> per averli in inglese.