---
title: "Sviluppatori degni del loro nome"
date: 2005-04-30
draft: false
tags: ["ping"]
---

La prima vera soddisfazione di Tiger arriva da un programma non Apple

Di Tiger puoi leggere in abbondanza nel libriccino allegato al prossimo numero di Macworld in edicola.

Ma non poteva esserci scritto che il primo programma a permettermi di usare Spotlight nei dialoghi di apertura e salvataggio di file è stato… BBEdit.

Come è uscito Tiger, è infatti arrivato pressoché in contemporanea l'update alla versione 8.2, pronto per Mac OS X 10.4.

<a href="http://www.barebones.com">BBEdit</a> è software a pagamento. Ma pagare per queste cose è quasi un piacere.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>