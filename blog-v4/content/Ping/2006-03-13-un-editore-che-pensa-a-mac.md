---
title: "Un editore che pensa a Mac"
date: 2006-03-13
draft: false
tags: ["ping"]
---

Occhio alla homepage del sito di <a href="http://www.fag.it" target="_blank">Edizioni Fag</a>: c'è in bella evidenza il link al libro Video Digitale con il Mac dell'amico <a href="http://www.spobe.com" target="_blank">Jida</a>.

Fag è una casa editrice piccola ma emergente, cui fare attenzione. Hanno rinnovato il sito da poco e sembrano fare (positivamente) sempre più sul serio. Speriamo compaiano tanti altri libri per lettori con un Mac. :-)