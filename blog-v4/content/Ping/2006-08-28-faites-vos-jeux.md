---
title: "Faites vos jeux"
date: 2006-08-28
draft: false
tags: ["ping"]
---

Ho un pizzico di voce in capitolo sul Cd-Rom di Macworld ottobre. Vuoi trovarci dentro qualcosa che ti fa comodo? Fammelo sapere entro domani a mezzanotte.

I requisiti sono i seguenti:

<ul type="disc">
	<li>deve stare nel Cd, considerato che la precedenza va a software attinente agli argomenti trattati</li>
	<li>deve essere pubblicabile (niente oltraggi al pudore e violazioni di copyright)</li>
	<li>deve essere possibilmente freeware (per lo shareware e i demo commerciali bisogna chiedere autorizzazione all'autore e lo faccio, ma ho tempi molto corti e non so quanto ce la si farà)</li>
</ul>

Farò tutto quello che può essere fatto. Tieni conto che mercoled&#236; sera devo dare il rien ne va plus e devo avere già scaricato scaricato tutto lo scaricabile.

<em>Perché non lo hai detto prima?</em> Lo avessero detto prima anche a me&#8230; :-D