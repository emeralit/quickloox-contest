---
title: "Quando conta la mole"
date: 2010-04-29
draft: false
tags: ["ping"]
---

Sempre per la raccolta di gemme software realizzate in Html5 e visibili dentro un browser moderno senza altri orpelli, ecco <a href="http://alteredqualia.com/canvasmol/" target="_blank">CanvasMol</a>, un disegnatore e animatore tridimensionale di molecole semplici e complesse.

È interattivo; le molecole possono essere girato e rigirate anche con il mouse ed esistono possibilità di esportazione. Inoltre si possono aggiungere pulsanti aggiuntivi relativi alle molecole preferite semplicemente inserendo la definizione della molecola stessa.

Insomma, una applicazione, per quanto semplice.