---
title: "La banda dei presunti garantiti"
date: 2005-12-13
draft: false
tags: ["ping"]
---

Il mistero di un prodotto che nessuno può vendere

Leggo con molta frequenza di persone che si mettono a misurare la velocità della loro Adsl sulla base dei dati che gli arrivano. Cercano qualcosa di simile alla banda garantita.

Mi chiedo come facciano a considerare attendibili queste cifre. Lo stato di Internet cambia in continuazione. Il carico di lavoro dei server cambia in continuazione. La strada che percorrono i pacchetti è imprevedibile da un millisecondo a un altro.

Come uno che dica di avere fatto Milano-Roma in cinque ore. Ma era domenica o lunedì? Pioveva o faceva bello? E se adesso hanno aperto un cantiere? Se ne hanno chiuso uno? Se era una Smart? Se era un autotreno? Se all'autogrill c'era più o meno coda per il caffè?

Francamente, mi sembra come misurare la certezza che a Ferragosto farà bello. Di solito fa bello, ma potrebbe piovere. Ci sono alberghi che vendono il bel tempo garantito?

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>