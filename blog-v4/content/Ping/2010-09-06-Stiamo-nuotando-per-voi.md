---
title: "Stiamo nuotando per voi"
date: 2010-09-06
draft: false
tags: ["ping"]
---

Ringrazio quanti mi stanno invitando a partecipare a Ping, il social network basato su iTunes.

Assicuro che accetterò a tutti gli inviti, appena sarò in grado di scaricare iTunes 10. Attualmente mi trovo in località balneare, in luogo dove la ricezione rivaleggia quella dei rifugi antiatomici e delle cantine medievali.

Appena recupero la connettività normale mi porterò in pari e nessuno resterò senza risposta. :)