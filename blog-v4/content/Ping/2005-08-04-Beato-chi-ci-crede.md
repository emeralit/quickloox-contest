---
title: "Beato chi ci crede (ancora)"
date: 2005-08-04
draft: false
tags: ["ping"]
---

Ennesimo prodotto Apple che viene annunciato, giustamente, senza guastare la festa

Apple ha annunciato il suo Mighty Mouse. Ai commentatori un tanto al chilo sembra una banalità. Invece quella sferetta, al posto della rotella, segna una differenza importante, quella tra pensare a una dimensione e pensare a due (che non è il doppio, ma il quadrato).

Più di questo, i tanto strombazzati siti di insider non sono stati capaci di anticipare l'annuncio neanche di un'ora. Qualcuno, pateticamente, ha presentato le &ldquo;prime&rdquo; foto del mouse tirato fuori dalla scatola quando già sul sito Apple c'era tutta la documentazione che uno possa mai desiderare di ottenere su un prodotto del genere.

Il bello è che, per gente specializzata in pettegolezzi, c'era materiale in abbondanza. L'aggiornamento a Mac OS X 10.4.2, per esempio, cambiava una serie di sequenze di testo presenti nel codice da &ldquo;Pulsante destro del mouse&rdquo; a &ldquo;Pulsante secondario del mouse&rdquo;, o simile. C'era tutto, a pensarci.

La realtà è che i siti di pettegolezzi non pensano.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>