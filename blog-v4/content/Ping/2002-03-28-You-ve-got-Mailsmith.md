---
title: "You’ve got Mailsmith"
date: 2002-03-28
draft: false
tags: ["ping"]
---

When a program delivers, deserves. Even if you have to pay

I’m glad to announce that in two days I’m going to abandon, forever or something like that, Mac OS Classic. You don’t care and instead should, because the reason is the availability of Mailsmith for Mac OS X.
Mailsmith is a professional program for email management, made by Bare Bones, the authors of BBEdit: one of the best Mac programs ever. And Mailsmith is on par with that. Available for Mac OS since years and quite tested, Mailsmith comes to Mac OS X with some quite interesting new features: in this small space I can just hint about AppleScript recordability, SpamCop integration to fight undesired messages, multiple clipboards, and text management that is on par with BBEdit’s (excellent).
What is worth to me speaking so well about Mailsmith? Nothing. I’m a longtime Mailsmith user and will pay my regular upgrade, as everyone else.
Yes, Mailsmith has to be paid and that’s fair. First of all, is produced by a software house that makes a living by writing excellent software for Macintosh only. Second, it’s a program worth every dollar it costs. Opposite to others, that are worth not even for a closeout sale and are given for free just to nurture a monopoly.