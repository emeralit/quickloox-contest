---
title: "Password tira e pacco"
date: 2010-07-14
draft: false
tags: ["ping"]
---

Scrivevo in <a href="http://www.macworld.it/ping/life/2010/07/09/password-tira-e-molla/" target="_self">Password tira e molla</a>:

<cite>È un sentimento del tutto irrazionale e ingiustificato, ma se fatico a prendere sonno la notte non sto pensando alla mia carta di credito su iTunes Store; sto pensando a quella sul sito Adobe, dove se decido di chiamarmi <i>acquacedratario</i> si offendono. Ho pagato, ma domani riscarico <a href="http://www.scribus.net" target="_blank">Scribus</a>.</cite>

Oggi è arrivato puntuale il corriere a consegnare il mio aggiornamento a InDesign Cs5 nuovo fiammante. Dentro il pacco c'è una confezione ben studiata, elegante ed essenziale. Il piacere di spacchettare rimane fondamentale.

Inserisco il Dvd e avvio l'installatore. L'installatore mi segnala che non trova un prodotto adeguato da aggiornare.

Perplesso, contemplo l'InDesign Cs2, regolarissimo e a suo tempo pagatissimo, che fa bella mostra di sé nella cartella Applicazioni e che l'installatore non vede. Controllo il riepilogo dell'ordine e la fattura di acquisto, tutto normale: ho ordinato un aggiornamento da Cs2, niente errori.

Poco male; posso benissimo inserire il numero di serie del vecchio prodotto, anche se l'installatore non lo vede sul disco. Adobe ha pensato proprio a tutto.

Inserisco il numero di serie del vecchio InDesign e non viene riconosciuto. La perplessità aumenta.

Tengo i numeri di serie in un file apposito e può darsi che abbia commesso un errore di trascrizione. Mi imbarco in una spedizione archeologica e da uno scaffale remoto viene fuori la confezione originale, con i Cd originali, con il numero di serie originale. Confronto con il mio file e la trascrizione è perfetta. Riprovo a inserire il numero di file perché noi umani a volte preferiamo pensare che il computer possa cambiare idea. Ma non è questo il giorno, <a href="http://it.wikiquote.org/wiki/Il_Ritorno_del_Re" target="_blank">commenterebbe Aragorn</a>.

Ho a destra una scatola Adobe originale e a sinistra pure e da due scatole non riesco a ricavare la mia installazione. Perplesso e un tantino nervoso, cerco il numero del supporto tecnico italiano. Chiamo, ma sono le 19: una voce madrelingua marziana mi informa che, bontà sua, il supporto tecnico è disponibile <cite>luned&#236; al venerd&#236;, dal nove al cinque</cite>.

L'assurdo mi rincuora sempre. Dopotutto, ascoltato alla lettera, il supporto da Marte sarebbe disponibile per tutta notte fino quasi all'alba. In realtà intendono <i>las cinco de la tarde</i>, alla spagnola, viva i Campioni del mondo. Su Marte la giornata d'altronde dura <a href="http://library.thinkquest.org/J0112188/mars.htm" target="_blank">trentasette minuti in più</a>, tutta fatica supplementare.

In serata, diversamente dal supporto di Adobe, mi tocca lavorare. La soluzione è semplice. Un po' di pazienza, guanti sterili alle mani e mi addentro nel sottobosco di Internet.

Recupero un programma di generazione di numeri seriali. Ne produco uno buono per un vecchio InDesign <i>et voilà</i>, il programma si installa che è una bellezza. Non scaricavo da anni un programma di generazione di seriali. In effetti da anni non pagavo Adobe in cambio del privilegio di averne bisogno.

D'altronde ho pagato solo trecento euro. Mica posso pretendere chissà che, addirittura che un aggiornamento riconosca l'originale &#8211; ugualmente pagato &#8211; da aggiornare.

Conosco amici che risolvono il problema alla radice e oltre al programma di generazione chiavi recuperano in giro per la rete pure InDesign, a costi assai inferiori rispetto a quelli da me sostenuti. Non li giustifico, solo che questo sistema favorisce loro.

<a href="http://www.scribus.net" target="_blank">Scribus</a> deve crescere ancora, ma per cose semplici non è poi cos&#236; male.