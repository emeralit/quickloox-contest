---
title: "Quota di viaggio"
date: 2008-07-19
draft: false
tags: ["ping"]
---

Ci sarebbero diverse considerazioni da fare su <a href="http://www.lastampa.it/_web/cmstp/tmplrubriche/giornalisti/grubrica.asp?ID_blog=31&amp;ID_articolo=138&amp;ID_sezione=39&amp;sezione=" target="_blank">questa esperienza di Bruno Ruffilli de La Stampa</a> (mi scuso in anticipo con chi me l'ha segnalata; ho perso il riferimento ed è tutta colpa mia).

Ne sviluppo solo una. Sono abituatissimo a estrarre il portatile in viaggio. Fin dagli anni Novanta mi sono tuttavia abituato a stare in compagnia di altri Mac, o di un Pc, o magari due&#8230; ma non molto oltre.

La fatidica e famigerata quota di mercato Mac a volte è stata del tre, a volte dei cinque, a volte del sette, ma mai del cinquanta o del trentatré. Dove spariscono tutti gli altri portatili quando si sale sul treno?