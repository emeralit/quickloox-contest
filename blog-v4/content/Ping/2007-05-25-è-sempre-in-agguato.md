---
title: "È sempre in agguato"
date: 2007-05-25
draft: false
tags: ["ping"]
---

L'imprevisto. C'è un rischio che oggi arrivi nella stanza di iChat <code>gamefriday</code> in ritardo, non so quanto leggero. Se c'è qualcuno che ha voglia di giocare, cominci quando vuole. Io arriverò scusandomi. :)

