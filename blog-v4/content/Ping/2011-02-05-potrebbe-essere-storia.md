---
title: "Potrebbe essere storia"
date: 2011-02-05
draft: false
tags: ["ping"]
---

Non posso garantirlo. Ho però la sensazione che <i>Gold!</i> di Michael Schact sia il primo gioco della storia che esce contemporaneamente in <a href="http://boardgamegeek.com/boardgame/90474/gold" target="_blank">edizione da tavolo</a> e come <a href="http://itunes.apple.com/us/app/michael-schachts-gold/id415014558?mt=8" target="_blank"><i>app</i> per iOS</a>.

Magari è pure divertente.

