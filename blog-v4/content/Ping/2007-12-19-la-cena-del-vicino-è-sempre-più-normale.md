---
title: "La cena del vicino è sempre più normale"
date: 2007-12-19
draft: false
tags: ["ping"]
---

<em>Mi si è rotto l'hard disk della marca Tale. Ma non sono il solo, eh? Anche mio cugggino*&#8230; certamente la qualità non più quella di un tempo. Non può essere un caso. State lontani dalla marca Tale!</em>

Quella di trovarsi sempre al centro di eventi significativi è una presunzione molto umana e comprensibile. Il più delle volte, fallace.

Qualche sera fa eravamo in nove alla cena di compleanno indetta dal vicino di casa. A un certo punto è saltato fuori che c'erano tre persone dello stesso segno zodiacale. E qualcuno ha espresso meraviglia.

In realtà, se non sto facendo errori grossolani, si tratta di una situazione assolutamente comune, che con questi numeri ha luogo più di tre volte su quattro. Eppure a qualcuno sembrava una incredibile combinazione.

Di più. Il mio vicino ha amici suoi, fa tutt'altro lavoro, frequenta altri ambienti. Eppure, tolta dal gruppo la mia signora per non alterare il campione, eravamo in due su otto a usare Mac. Il 25 percento.

Dovrei trarne indicazioni sulla quota di mercato del Mac? Evidentemente no. Anche questo, dunque, era un evento incredibile e singolare? Evidentemente, no.