---
title: "Alla-presentazione-italiana-dei-nuovi-mac-intel-2"
date: 2006-01-16
draft: false
tags: [ping]
---

Ci è stato chiesto di non fare foto centrate sulle nuove macchine, perché quelle mostrate sono ancora preproduction unit.

Ecco il valore che hanno tutti i giudizi di chi lo ha preso in mano al Macworld Expo e ha già spiegato quanto durano le batterie del MacBook Pro, quanto scalda e tutto quanto il resto.
