---
title: "It doesn't suck&#174;"
date: 2008-09-03
draft: false
tags: ["ping"]
---

È uscito <a href="http://www.barebones.com/products/bbedit" target="_blank">BBEdit 9</a>.

Leggere le <a href="http://www.barebones.com/support/bbedit/arch_bbedit9.html" target="_blank">Release Notes</a>, nuove funzioni e <em>bug</em> sistemati, è un esercizio di disciplina. Però frutta una sorprendente comprensione di quanto Bare Bones sia attenta ai particolari e di quante e quali funzioni sia capace BBEdit.

Ho già aggiornato.