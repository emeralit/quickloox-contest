---
title: "Quando sei materiale didattico"
date: 2005-07-18
draft: false
tags: ["ping"]
---

Ritornare ragazzi per un attimo, con la tecnologia in mano, è stato divertente

Non vorrei scendere eccessivamente nei dettagli, ma nel corso di una sessione d'esame all'Accademia di Brera di Milano sono stato chiamato in causa come materiale di contorno alla dispensa dell'esame stesso.

E così mi sono presentato nei locali della prestigiosa istituzione armato di PowerBook.

A un certo punto i corridoi si sono completamente svuotati; tutti gli studenti si sono buttati nell'aula a vedere che cavolo stava succedendo, attirati dall'audio a tutto volume e poi dal resto degli effetti speciali.

Non era certo merito mio (chiunque avrebbe ottenuto lo stesso effetto) ma posso garantire che l'hardware ha suscitato interesse almeno quanto l'oggetto della dimostrazione.

A dire che Apple, o chi per lei, dovrebbe stipendiare gente che vada in giro per le università con un PowerBook in mano.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a><a href="mailto:lux@mac.com">Lucio Bragagnolo</a>