---
title: "Font, non fontina"
date: 2007-11-23
draft: false
tags: ["ping"]
---

Leopard è più rigoroso di Tiger nel riconoscere e utilizzare i font. Libro Font di Leopard individua errori in font che Libro Font di Tiger invece lascia passare.

È possibile verificare l'attendibilità della cosa mediante l'uso di utility indipendenti, come <a href="http://www.linotype.com/fontexplorerX" target="_blank">FontExplorerX</a>.

Un bene perché i font diventati fontina, a furia di copie e stracopie, passaggi per mille mani e stazionamenti su dischi di dubbia reputazione, diventano a volte un problema serio di stabilità, per giunta difficile da individuare.

Certo, a volte tocca trovare font buoni (e magari legalmente posseduti) per portare a termine un lavoro. Sembra solo un bene però.