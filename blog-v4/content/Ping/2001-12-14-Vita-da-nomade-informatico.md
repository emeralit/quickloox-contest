---
title: "Vita da nomade informatico"
date: 2001-12-14
draft: false
tags: ["ping"]
---

Comprare un portatile giusto allunga la vita. E i pezzi

Su Milano si è abbattuta una nevicata che il vento siberiano ha trasformato in bufera. Sono sul treno di tutti i giorni, ma - come sempre in questi casi - gli inetti ferrovieri sono incapaci di fare partire alcunché da più di un’ora (chissà come fanno a funzionare le Ferrovie Nord Finlandia). La luce sul treno è spenta; impossibile leggere. Imperturbabile, tiro fuori il mio PowerBook e inizio a scrivere questa rubrica. Poi, se il treno davvero fa molto tardi, giocherò un po’ a Pillars of Garendall di Ambrosia Software (raccomandato).
Metà degli occupanti il vagone fa il diavolo a quattro con il cellulare; sembra che, dopo avere avvisato la mamma la moglie e tutti i parenti in Europa, non possano altro che continuare a chiamare all’infinito. Ma le batterie presto finiscono e restano nervosi a mandare manciate di Sms a chissà quale mondo.
C’è un signore, con un grigio portatile Windows, che scrive - mi sembra - appunti per una riunione. Usa Word 2000. Per prendere appunti. Giustamente, dopo mezz’ora, la batteria lo abbandona.
Vado avanti, tranquillo. So di avere almeno altre due ore di autonomia, perfino contando gli accessi al disco di Pillars of Garendall e il fatto che Mac OS X deve ancora essere ottimizzato per il risparmio energetico. Il tempo passa, anche gli ultimi telefonini si rassegnano e muoiono, il signore con il computer Windows (spento) guarda con un po’ di fastidio la mia melina, unica fonte di luce nel vagone. Mi chiedo quanto avrà risparmiato, comprando quel computer che non può usare quando ne ha bisogno.
Non mi sento superiore, né più furbo; semplicemente, ho in mano tecnologia a misura d’uomo, che funziona. Potrei andare avanti ancora per molto, ma ho già superato da un bel po’ i limiti di lunghezza di questa rubrica. Ma quando ci vuole ci vuole e comunque mi manca solo la morale della storia: comprare un PowerBook allunga la vita.

<http://www.ambrosiasw.com/pog>