---
title: "La solita vecchia faccia"
date: 2002-11-13
draft: false
tags: ["ping"]
---

Ci sono i nostalgici del piccolo Mac sorridente. Basta un po’ di attenzione per far sorridere anche loro

La scomparsa del piccolo Mac sorridente che si mostrava accendendo un Mac ha dispiaciuto qualcuno. C’è chi ci ha pensato e messo in Rete le <link>istruzioni</link>http://www.ryandesign.com/jagboot per rimettere il piccolo Mac al suo posto.

Una sola raccomandazione: seguire alla lettera le istruzioni dell’autore. Anche la prima parte, che ha un’aria più facoltativa. La materia, dal punto di vista della stabilità del software, è delicata e non va presa troppo alla leggera.

<link>Lucio Bragagnolo</link>lux@mac.com