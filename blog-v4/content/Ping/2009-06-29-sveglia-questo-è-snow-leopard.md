---
title: "Sveglia, questo è Snow Leopard"
date: 2009-06-29
draft: false
tags: ["ping"]
---

Sveglia Apple, sarebbe meglio tradurla, questa <a href="http://www.apple.com/macosx/refinements/enhancements-refinements.html" target="_blank">pagina delle novità del nuovo sistema operativo</a>. Ma ti si fa supplenza volentieri, una novità al giorno, qui sotto.

<b>Chiusura e risveglio più veloci</b>

Snow Leopard si spegne fino al 75 percento più velocemente, è veloce il doppio a risvegliarsi dallo stop se è abilitato il blocco dello schermo ed è fino a 55 volte più rapido nel collegarsi a una rete <i>wireless</i>.

Posso confermare il primo e il terzo dato, specialmente il primo (i miei rari riavvii sono epocali e manca poco che vengano i vicini a vedere, da tutto quello che va chiuso). Per il secondo mi darò da fare.