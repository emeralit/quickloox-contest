---
title: "Automedicazione"
date: 2006-05-23
draft: false
tags: ["ping"]
---

Console e Monitoraggio Attività sono utility pienamente Mac, con icona, interfaccia utente, doppio clic, menu e quant'altro. Sono impagabili per capire le ragioni di un programma che si chiude senza motivo oppure per verificare se c'è un programma impazzito che sta predando memoria agli altri. Permettono di risolvere situazioni difficili da soli, ed è anche una soddisfazione.

Trovassi qualcuno che li usa.

Si capisce che non dobbiamo essere tecnici per forza, eppure - quando compriamo un Mac - passare un'ora a vedere che cosa c'è dentro, cartella Utility compresa, non mi sembra un'enormità.