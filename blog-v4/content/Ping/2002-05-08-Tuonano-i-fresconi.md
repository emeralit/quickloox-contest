---
title: "Tuonano i fresconi"
date: 2002-05-08
draft: false
tags: ["ping"]
---

Su Mac OS X 10.2, prima di parlare a vuoto, leggere bene e accertarsi che il cervello sia acceso prima di parlare

Frequento un po’ di mailing list Mac e, a seguito dell’annuncio di Mac OS X 10.2 (nome in codice: Jaguar), si è scatenato il panico.
Dopo quasi un anno da 10.1, Apple fa uscire un aggiornamento significativo di Mac OS X: ottimo, viene da dire. Il quale aggiornamento, se nel computer è presente una nuovissima scheda video ad alte prestazioni, la sfrutta per essere ancora più veloce. Ottimo, viene da dire.
Invece i fresconi stanno tuonando contro Apple: inconcepibile, dicono, che Mac OS X 10.2 non funzioni con le macchine odierne.
Non hanno capito che la scheda, se c’è, velocizza; ma se non c’è, non cambia niente, se non che 10.2 sarà comunque più veloce anche sui Mac vecchi, grazie all’uso di un compilatore più veloce ed efficiente.
Il bello è che non serve capire; basterebbe leggere. Fresconi.

<link>Lucio Bragagnolo</link>lux@mac.com