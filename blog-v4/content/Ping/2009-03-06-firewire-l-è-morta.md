---
title: "FireWire l'è morta"
date: 2009-03-06
draft: false
tags: ["ping"]
---

Il nuovo Mac mini ha <a href="http://store.apple.com/it/browse/home/shop_mac/family/mac_mini?mco=NDE4MzgzNg" target="_blank">una porta FireWire</a>.

Il nuovo iMac ha <a href="http://store.apple.com/it/browse/home/shop_mac/family/imac?mco=NDE4Mzg1OA" target="_blank">una porta FireWire</a>.

Il nuovo Mac Pro ha <a href="http://store.apple.com/it/browse/home/shop_mac/family/mac_pro?mco=NDE4Mzg0MQ" target="_blank">quattro porte FireWire</a>.

Adesso tocca leggere ancora per un anno che Apple ha deciso di dismettere FireWire.