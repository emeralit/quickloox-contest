---
title: "Errore reiterato, errore fortunato"
date: 2005-01-11
draft: false
tags: ["ping"]
---

Bello poter essere felici dei propri errori

Ricordo una sola volta come questa: il giorno in cui mi sono avviato alla conferenza stampa Apple pensando <em>presenteranno tutto, ma certamente non iPod per Windows</em>.

Avevo tortissimo. Ma la cosa bella è che Apple aveva ultraragione e con iPod per Windows sta incassando cifre strepitose.

Spero che perseverare nell'errore (mio) abbia lo stesso effetto. Sarebbe proprio un bel viatico per il 2005 della Mela.

Adesso vado a studiare.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>