---
title: "Chi s’accontenta gode. Così così"
date: 2003-07-11
draft: false
tags: ["ping"]
---

Apple vende godimento, non elenchi di funzioni

Ho capito una cosa importante. Apple non vende funzioni: vende esperienza. Esperienza d’uso, che a Cupertino chiamano user experience e riguarda quello che proviamo, sentiamo, gradiamo oppure ci scoccia di un oggetto, hardware o software.

Provo a spiegarmi concretamente. iPod.

iPod ha molti concorrenti. Non è difficile trovarne uno che costi meno, oppure uno che abbia una funzione in più, oppure uno con software aggiuntivo che lui non ha, oppure uno con un disco rigido più grande.

Ma quello che fa grande iPod non è questa o quella funzione. È un oggetto splendido già guardando la sua scatola, invece. Lo tiri fuori e sta in qualunque tasca, veloce, facilissimo, con un’interfaccia utente studiata nei minimi dettagli, il display informativo, i tasti retroilluminati e tutti a stato solido eccetera eccetera.

Il concorrente che costa meno - o altro - ha i pulsanti fatti di plastica triste e grigia, oppure sostituisce FireWire con una altrettanto triste Usb 2, oppure vuola una tasca più grande, insomma, si vede che non è fatto per dare un piacere e un gusto a chi lo usa, ma solo per essere venduto. Una cosa che dà piacere solo quando la si compra, e poi più, è triste, appunto.

iPod - e il resto della produzione Apple - non sono fatti solo per essere venduti, ma per essere goduti. L’elenco delle funzioni è tutt’altra cosa. Parafrasando Ligabue, chi si accontenta gode. Ma così così.

Scelgo Apple.

<link>Lucio Bragagnolo</link>lux@mac.com