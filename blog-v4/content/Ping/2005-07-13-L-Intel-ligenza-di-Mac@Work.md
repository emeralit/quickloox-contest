---
title: "L'Intel-ligenza di Mac@Work"
date: 2005-07-13
draft: false
tags: ["ping"]
---

Almeno un Mac Intel-based in Italia settentrionale

Qualcuno si ricorderà dell'idea che aveva avuto Sergio: raccolta fondi tra gli interessati e acquisto di un Mac con processore Intel, di quelli che Apple riserva agli sviluppatori che vogliono iniziare da subito a convertire il loro software nel formato Universal Binary, che gli permetterà di girare su qualsiasi Mac a prescindere dal processore.

Io avevo aderito. Purtroppo l'idea non ha attecchito. Al che l'ho girata a <a href="http://www.macatwork.net">Mac@Work</a>. Dove è piaciuta ed è stata ordinata una macchina.

Il Mac è arrivato ed è a disposizione gratuita di tutti gli sviluppatori italiani che desiderassero fare prove ed esperimenti.

Non so quanti Mac Intel-based esistano fuori da Apple, in Italia, in questo momento. Spero tanti. Credo pochissimi.

Complimenti a Sergio per un'ottima idea e complimenti a Mac@Work per una iniziativa intelligente e meritevole. Verrà seguita da altri? Io sono a disposizione per chi volesse segnalare.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>