---
title: "Per un pugno di giochi in più"
date: 2006-01-17
draft: false
tags: [ping]
---

Nella dotazione software di MacBook Pro e iMac Core Duo, così come li ho visti alla presentazione ufficiale di Milano, c’è anche Big Bang Board Games, una bellissima raccolta di Freeverse che comprende i classici scacchi, dama, reversi e via discorrendo, ma con un approccio grafico molto bello e un sacco di piccole aggiunte che ingolosiscono.

Come già detto, le unità mostrate erano preliminari e quindi non possiamo sapere se si tratti di scelte definitive, o se siano scelte valide anche per l’Italia (le cose sono ancora talmente provvisorie che ci hanno chiesto perfino di non fotografare le macchine esposte). Intanto, però, una partitina…
