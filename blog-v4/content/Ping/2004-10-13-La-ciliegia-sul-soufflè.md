---
title: "La ciliegia sul soufflè"
date: 2004-10-13
draft: false
tags: ["ping"]
---

L'ennesima storia di emulatori-miracolo

Adesso è arrivato Cherry OS, che secondo il suo autore consente di fare funzionare Mac OS X su un Pc all'80 della velocità di un Mac originale.

Capiamoci: VirtualPC, che viene ottimizzato da anni e anni per recuperare prestazioni in ogni angolo del codice, forse non arriva a questo traguardo.

Per chi crede a tutto quello che viene scritto e si entusiasma subito, un consiglio: provare Cherry OS.

Quando il processore del Pc si sgonfierà come un soufflè malriuscito, non dire che non eri stato avvisato.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>