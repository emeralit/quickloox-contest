---
title: "Giù la maschera, per gioco"
date: 2005-05-20
draft: false
tags: ["ping"]
---

Ennesima dimostrazione di quale processore abbia davanti il futuro migliore

Si sta tenendo a Los Angeles l'E3, una delle maggiori manifestazioni al mondo nel campo dei videgiochi. Nello stand Microsoft è possibile provare i prototipi della nuova Xbox 360.

O così sembra. Perché, a guardare dietro le mascherine metalliche di protezione, si nota che le console sono spente e a fare andare i giochi sono coppie di Power Macintosh G5!

Si sa da tempo che la prossima Xbox monterà processori simili ai PowerPc. Gli unici che hanno davanti prospettive di crescita ragionevoli senza sciogliersi a causa del calore da essi generato.

Se qualcuno avesse dubbi, c'è anche la <a href="http://anandtech.com/tradeshows/showdoc.aspx?i=2420&p=5">documentazione fotografica</a>.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>