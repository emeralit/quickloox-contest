---
title: "Una barra proprio trash"
date: 2007-05-06
draft: false
tags: ["ping"]
---

Complimenti a Mat Lu dell'Unofficial Apple Weblog per avere spiegato un sistema elegante di <a href="http://www.tuaw.com/2007/05/03/tuaw-tip-the-best-way-to-add-the-trash-to-your-finder-sidebar/" target="_blank">mostrare il Cestino</a> nella barra laterale del Finder. Ma è meglio non provarci; è come certe donne, unicamente da ammirare. :-)