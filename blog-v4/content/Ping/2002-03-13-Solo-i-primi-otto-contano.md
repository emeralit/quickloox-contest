---
title: "Solo i primi (otto) contano"
date: 2002-03-13
draft: false
tags: ["ping"]
---

Un piccolo segreto per avere password realmente segrete

Le password di login in Mac OS X possono essere lunghe a piacere; ma il sistema utilizza solamente i primi otto caratteri e il resto è come se non ci fosse.
Per questo motivo le cose che rendono sicura una password (numeri, caratteri insoliti e sequenze cacofoniche) devono stare nei primi otto caratteri; dopo sono inutili, esattamente come usare come password stupidaggini come l’anno di nascita o il proprio nome a rovescio.

<link>Lucio Bragagnolo</link>lux@mac.com