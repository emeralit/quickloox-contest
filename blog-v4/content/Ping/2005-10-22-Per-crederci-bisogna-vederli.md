---
title: "Grandi schermi, piccoli giudizi"
date: 2005-10-22
draft: false
tags: ["ping"]
---

Per crederci occorre vederli

In <a href="http://www.macatwork.net">Mac@Work</a> ho avuto occasione di toccare con mano i nuovissimi PowerBook e Power Mac G5.

Sul secondo l'aumento di prestazioni si vede eccome. Sui primi è relativo, perché di fatto la parte interna del sistema non è cambiata, ma i nuovi schermi fanno veramente impressione e la durata della batteria, cresciuta circa del 20 percento, si apprezza veramente.

Sui G5 c'è poco da discutere. C'è chi ha messo in discussione la validità dell'aggiornamento dei PowerBook, invece.

Mai come in questo caso, prima di parlare occorrerebbe guardarli.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>