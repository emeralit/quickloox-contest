---
title: "Panorami vacanzieri"
date: 2008-08-18
draft: false
tags: ["ping"]
---

In agosto ti puoi anche permettere di perdere del tempo. Io ero su Safari con il menu Develop attivato (dalle Preferenze, sezione Avanzate, l'ultima casella in basso).

Dal menu Develop ho attivato la Network Timeline (Comando-Opzione-N) e sono stato a guardare in quanto tempo si era caricata l'ultima pagina che ho visitato, quando sono stati caricati uno per uno gli elementi della pagina e quanto tempo ci ha messo ciascun elemento, con la successione degli elementi caricati.

E ho pensato: se avessi un sito, mi aiuterebbe non poco nell'ottimizzazione dello stesso.

Quasi mi veniva voglia di farmi il sito apposta.