---
title: "A casa gli incapaci"
date: 2007-07-04
draft: false
tags: ["ping"]
---

<cite>Gentile Cliente,</cite>

<cite>siamo spiacenti di informarla che la richiesta di attivazione del servizio sulla Sua linea telefonica è stata respinta da Telecom Italia.</cite>

<cite>Telecom ha dichiarato che la Sua linea non è di qualità sufficiente per poter supportare il servizio Fastweb; quindi, non risulta possibile attivare il servizio sulla Sua linea telefonica nonostante risulti coperta dal servizio.</cite>

<cite>L'esito negativa della Sua richiesta di attivazione del servizio è dunque da considerarsi definitivo.</cite>

<cite>Le comunichiamo, pertanto, che il Contratto per la fornitura dei servizi richiesti non si è perfezionato, ai sensi di quanto disposto dall'articolo 3.3 f, delle Condizioni Generali di Contratto.</cite>

<cite>Distinti Saluti</cite>

<cite>Servizio Clienti FastWeb</cite>

Non è interessante che riguardi me o qualcun altro.

Non è interessante se a fare la manfrina sia Telecom, FastWeb o tutti e due.

Dovrebbe interessare che, se questo è il sistema, è perdente, e finiamo per perderci tutti, anche quelli con la linea di alta qualità.

A casa gli incapaci. A partire dai monopolisti incapaci. Doppiamente colpevoli, in quanto monopolisti. Almeno fossero <em>solo doppiamente</em> incapaci.