---
title: A Lucca tutto uk
date: 2010-10-31
draft: false
---

Il capoluogo toscano è piccino e si gira agevolmente in un paio d’ore di buon passo.
Trasformato in fiera del fumetto e dei giochi, una delle poche fiere dove è più interessante il pubblico degli espositori e dove da un giorno all’altro calano cinquantamila e più persone, fa intrecciare percorsi interminabili, entusiasmanti ed estenuanti. Al termine dei quali si imbocca una galleria sotterranea scavata dentro mura antiche e viene da sperare che i giocatori di ruolo live non si siano dimenticati qualche mostro in un anfratto.
Il dibattito su iPad e fumetti è stato coinvolgente e ha lasciato molti più dubbi che chiarimenti, buon segno, indice che la questione è importante. Chi di fumetto vive realmente chiede a voce alta certezza sugli strumenti di creazione dei contenuti e su quelli per leggere, legittimamente, solo che è un pochino presto. Devo loro dire con grande affetto che una certa loro pretesa di restare lontani dal fatto tecnico per pensare solo alla storia, o al disegno, è foriera di sole delusioni e che un bravo autore non sarà certo chiamato a padroneggiare linguaggi di marcatura o tecnologie di animazione, ma certamente dovrà averne comprensione, altrimenti gli mancheranno il controllo della produzione e la capacità di sapere ciò che può essere fatto e ciò che deve ancora essere inventato (leggi: opportunità creative).
Secondariamente, qualunque cosa valga per gli autori di oggi non varrà allo stesso modo per quelli di domani. Un nato nel 1969 e uno nel 1999 non vedranno il fumetto nello stesso modo.
Terzo e per ora ultimo: i programmi per gestire fumetti sul computer esistono da secoli. Ma solo iPad provoca discussioni e dibattiti. L’apparecchio ha peculiarità intrinseche tutte ancora da scoprire.
Torno in argomento appena ho digerito le foto e il resto del materiale raccolto.
