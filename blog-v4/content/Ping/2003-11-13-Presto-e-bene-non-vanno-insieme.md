---
title: "Presto e bene non vanno (subito) insieme"
date: 2003-11-13
draft: false
tags: ["ping"]
---

Ecco perché Panther va più veloce di Mac OS X 10.2

Uno dei motivi della superiorità di Mac OS X su Windows è che, di aggiornamento in aggiornamento, Mac OS X guadagna velocità, anche sulle macchine più vecchie. È contrario al senso comune, ma c’è una spiegazione.

Mac OS X 10.2 ha subito un lungo lavoro di programmazione per adeguare le routine di tracciamento del testo alle esigenze di Unicode e dei meccanismi di tipografia avanzata che oggi sono indispensabili a un sistema operativo come si deve.

Ma in programmazione presto e bene, quelli del proverbio, non vanno insieme. E la prima preoccupazione dei programmatori è stata che le routine appena scritte funzionassero. Così il loro tempo se ne è andato in controllo dei bug.

Pubblicato Mac OS X 10.2, i programmatori hanno ripreso in mano le routine, funzionanti, e hanno passato il loro tempo a ottimizzarle per aumentarne la velocità. Risultato: il tracciamento del testo, su Panther, è più veloce anche del 50 percento rispetto a Mac OS X 10.2. Basta guardare una schermata qualunque per accorgersi di quanto testo ci sia, e quanto contino questi miglioramenti.

È solo uno dei segreti, in realtà. Panther è stato ottimizzato in molti altri aspetti; ma questo è particolarmente eclatante.

Per i programmatori Apple presto e bene non vanno insieme subito, ma ci arrivano; per i programmatori Windows esiste solo “presto”. E si vede.

<link>Lucio Bragagnolo</link>lux@mac.com