---
title: "Divide et impera"
date: 2007-07-13
draft: false
tags: ["ping"]
---

Avevo un'immagine disco bootabile da masterizzare e poi installare&#8230; ma non avevo un Dvd a portata di mano.

Fortunatamente avevo un disco esterno FireWire da venti giga, perfettamente sacrificabile.

Ho montato l'immagine disco sul computer.

Poi, con Utility Disco, ho diviso in due partizioni il disco esterno.

In una partizione ho fatto il Restore (in italiano credo sia Ripristina) dell'immagine disco montata. Poi ho fatto il boot da l&#236; e ho installato sull'altra partizione.

Non è farina del mio sacco; su Internet le spiegazioni in merito abbondano (per esempio <a href="http://gwhiz.wordpress.com/2006/09/21/installing-leopard-from-dmg/" target="_blank">questa</a>, applicata a Leopard). Un po' elaborato (e si deve sacrificare qualche giga di disco esterno), ma assai comodo per non dover passare da un Dvd.