---
title: "Prove tecniche di pubblicazione immagini"
date: 2004-10-12
draft: false
tags: ["ping"]
---

Sito nuovo, collaudi da fare

Non ha giovato che scrivessi un pezzo dedicato al confronto a iMac G5 e Sony Vaio, con immagine, di domenica. Qualcosa è andato storto e l'immagine alla fine non è stata pubblicata.

Ci riprovo questa volta. Ricordo che tutto nasceva da una segnalazione di Andrea Fistetto di Manduria (TA), con il quale mi scuso.

Anche riguardandola, l'immagine, proprio non c'è partita. Spero possa vederla anche tu.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>