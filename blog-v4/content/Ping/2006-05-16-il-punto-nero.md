---
title: "Il punto nero"
date: 2006-05-16
draft: false
tags: ["ping"]
---

Sale sul treno una ragazza, probabilmente universitaria. Tiratissima, truccata, pettinata con cura, scarpe alla supermoda, non c'è un accessorio fuori posto, unghie di precisione. &Egrave; studiata anche nell'atteggiamento. La camicetta è chiusa e aperta con criterio scientifico e sicuramente collaudato con attenzione allo specchio.

Con affettazione apre lo zainetto, ovviamente griffato. Tira fuori un portatile. Gonfio, grigio, sformato, anonimo, rozzo, fuori proporzione. Non sto esagerando; se fosse un Thinkpad o un Vaio non ne parlerei certo in questi termini. Una persona che avrà passato un'ora a prepararsi per l'uscita e poi, in pubblico, tira fuori un computer <em>brutto</em>. <a href="http://www.mondourania.com/urania/u741-760/urania758.htm" target="_blank">Aldo Palazzeschi scrisse</a> che dentro ogni persona c'è un punto nero, sempre nascosto. Come mai, se è il computer, la gente lo mostra?