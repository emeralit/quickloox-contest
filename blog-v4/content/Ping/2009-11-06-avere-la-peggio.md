---
title: "Avere la peggio"
date: 2009-11-06
draft: false
tags: ["ping"]
---

Flora Graham su Cnet pensa che iPhone sia <a href="http://crave.cnet.co.uk/mobiles/0,39029453,49303754,00.htm" target="_blank">il telefono peggiore del mondo</a> e tutti hanno diritto al proprio parere.

Ma per sostenerlo spiega, tra l'altro, che <cite>non puoi rispondere se non suona</cite>. Lo svolgimento si articola attorno a <cite>forse il peggiore dei problemi di iPhone, la sua abilità di stare in silenzio e ignorare le chiamate in arrivo, senza suonerie o vibrazioni ad avvisare</cite>. A parte che un iPhone completamente silenziato mostra comunque la chiamata in arrivo, la premessa a me sembra falsa. Tengo permanentemente iPhone senza suoneria, con la sola vibrazione, e rispondo benissimo. La vibrazione c'è, basta impostarla.

Una falsità però, in un articolo pieno di altre cose, si può perdonare. È la parte in cui spiega che <cite>iPhone potrebbe bruciarti la faccia</cite> (<cite>estremamente improbabile, ma è caldo e ti fa sudare la pelle</cite> eccetera eccetera) che mi pare si inizi a esagerare.

A leggere tutto l'articolo, direi che Flora Graham è il peggiore recensore del mondo e a Cnet non fa proprio onore averla tra i ranghi.