---
title: "Tiger e il backup quotidiano"
date: 2005-05-03
draft: false
tags: ["ping"]
---

Devo ancora verificare se funziona. Ma se funziona…

Le mie politiche di backup sono molto semplici. Dvd del lavoro fatto e in corso, uno al mese.

In più approfitto dell'abbonamento a .Mac per effettuare un backup automatico su Internet del lavoro effettuato nella giornata. Anche questo è semplice, ma a volte un po' macchinoso.

Tiger e la smart folder, però, cambiano tutto. Se posso creare una smart folder che si aggiorna dinamicamente, giorno per giorno, con il lavoro di &ldquo;oggi&rdquo;. E la collego a Backup, per dire, posso ritrovarmi automaticamente backuppato il lavoro delle 24 ore.

Per quanto riguarda il Dvd è ancora più facile: attivo una cartella di masterizzazione selezionando i documenti salvati nell'ultimo mese e fa tutto lui.

Tiger mi piace proprio. :-)

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>