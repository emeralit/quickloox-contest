---
title: "Non si è mai abbastanza chiari"
date: 2006-04-15
draft: false
tags: ["ping"]
---

Paolo Attivissimo, che è anche bravissimo (bisticcio voluto) e uno dei pochi giornalisti italiani capaci di fare informazione informatica (altro bisticcio voluto) ha pubblicato un <a href="http://attivissimo.blogspot.com/2006/03/palladium-in-apple.html" target="_blank">eccellente articolo</a> sul Tpm, Palladium, Drm o come cavolo si preferisce chiamare la possibilità eventuale per il computer di applicare la sua idea di contenuti protetti indipendentemente da quello che ne pensa il suo proprietario.

Leggersi l'articolo, che è da premio. Dimenticare i commenti. Decine di persone stanno prendendo l'articolo a pretesto per spargere Fud (Fear, Uncertainty, and Doubt) sul tema. L'articolo stabilisce, sembra ombra di dubbio, che il chip c'è (e si sapeva, ma nessuno aveva guardato veramente). I fatti sono che Apple, il chip, non lo usa. E non lo usa da un anno, non da ieri. Lanciare l'allarme perch&eacute; Apple non fa una cosa ma potrebbe farla, visto che c'è il chip, non è il massimo. Se non ci fosse il chip e Apple volesse usarlo, ce lo metterebbe lei. Quindi la presenza del chip su un chipset che è preso così com'è da Intel non significa assolutamente niente dal punto di vista del Grande Fratello Prossimo Venturo.