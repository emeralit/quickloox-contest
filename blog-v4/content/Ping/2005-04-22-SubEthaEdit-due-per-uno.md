---
title: "Due al prezzo di uno"
date: 2005-04-22
draft: false
tags: ["ping"]
---

Un editor di testo fuori dal comune e un'offerta tipica ma sempre interessante

SubEthaEdit è gratuito per uso non commerciale ma, se lo usassi per lavoro, dovrei acquistarlo.

Fino al 29 aprile, i 35 dollari (28,54 euro per l'esattezza) richiesti per una licenza di SubEthaEdit valgono ben due licenze, una delle quali possiamo regalare a chi vogliamo.

L'offerta è meno bizzarra di quello che sembra, perché la caratteristica eccezionale di <a href="http://www.codingmonkeys.de/subethaedit/">SubEthaEdit</a> è proprio permettere a più persone di scrivere in contemporanea sullo stesso documento, anche se sono separate da migliaia di chilometri.

E le nuove opzioni del programma, dall'utility per eseguire automaticamente comandi di Terminale fino all'esportazione in Html, ne aumentano il valore. Da scaricare. Come minimo.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>