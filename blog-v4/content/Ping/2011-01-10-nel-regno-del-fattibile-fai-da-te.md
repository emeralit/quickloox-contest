---
title: "Nel regno del fattibile fai-da-te"
date: 2011-01-10
draft: false
tags: ["ping"]
---

Ho smesso di raccogliere post per questa serie a fine anno. Tuttavia accetto più che volentieri segnalazioni!

Ricordo che il presupposto della serie è smentire la temeraria sfida mostratemi qualcosa che si può fare su iPad e non su un Sony Vaio W (in sostanza un tradizionale <i>netbook</i>).

Mi è piaciuta questa sequenza di <b>Paolo</b>:

<cite>1) progettare al Mac un mobile per televisore Ikea <a href="http://www.idea.it/planner" target="_blank">sul loro sito</a> (ahimè, solo in flash);</cite>
<cite>2) farsi compilare automaticamente la lista della spesa con tanto di posizione negli scaffali e disponibilità al negozio più vicino;</cite>
<cite>3) farsela spedire in email in Pdf;</cite>
<cite>4) andare direttamente al <i>self service</i> mobili con la lista della spesa in <a href="http://itunes.apple.com/it/app/iannotate-pdf/id363998953?mt=8" target="_blank">iAnnotate</a> su iPad e spuntare/cancellare ogni volta che si mette nel carrello.</cite>

<cite>Saluti dalla coda alla cassa dell'Ikea, scrivendo email dall'iPad appoggiato comodamente sulle scatole del carrello&#8230;</cite>

Sto cercando disperatamente un'altra mail a tema, persa nel marasma della posta natalizia. Mi scuso, spero provvisoriamente, e conto che possa saltare fuori. Se qualcuno si riconosce, mi rimandi gentilmente la mail e prometto che non commetterò due volte lo stesso errore. :-)

