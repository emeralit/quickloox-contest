---
title: "La lunga mano della censura di regime"
date: 2006-06-14
draft: false
tags: ["ping"]
---

Ho cancellato un commento completamente inutile, totalmente scollegato dall'argomento e pieno di insulti, non a me. Lo considero un successo, nel senso che è il secondo dopo mesi e ci sono quasi tremila commenti intelligenti e centrati a osservarlo, ma al tempo stesso mi spiace un po'.

Si può benissimo sostenere che una idea è stupida. Affermare che una persona è stupida, invece, no. Qui almeno.