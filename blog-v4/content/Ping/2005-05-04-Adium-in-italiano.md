---
title: "L'Italia delle chiacchiere"
date: 2005-05-04
draft: false
tags: ["ping"]
---

Se si parla di chat, è sempre la benvenuta!

Parlando di buon software open source (quindi gratuito) e anche in italiano, l'amico Mario mi segnala che la nuova versione di Adium adesso include anche il supporto per la lingua di Dante.

Doppia buona notizia perché <a href="http://adium.sourceforge.net/">Adium</a> riconosce praticamente tutti i protocolli di chat in giro e in teoria potrebbe anche essere l'unico pezzo di software da mettere sul Mac per stare in contatto con chiunque su Icq, Aim, Yahoo Messenger, Msn, Jabber e se possibile altro ancora.

L'Italia delle chiacchiere, ma quelle utili e costruttive, con cui si combina davvero qualcosa, ha uno strumento in più.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>