---
title: "Un altro modo di divertirsi"
date: 2005-07-10
draft: false
tags: ["ping"]
---

Dal mondo immersivo alla simulazione spaziale, le occasioni ludiche per Mac si moltiplicano

In questi periodi un po' balneari non si offenderà nessuno se si sta più del solito attenti ai giochi. E poi, intanto che molti vanno in vacanza, c'è chi sostiene la carenza di giochi per Mac, per cui è meglio dare loro nozioni su cui… lavorare.

Per questo cedo volentieri la parola a Daniele:

<cite>Visto che parli sempre bene di <a href="http://www.worldofwarcraft.com">WoW</a>, volevo segnalarti un altro gioco online che funziona su Mac, e che nonostante sia diversissimo da WoW, prende molto. Spero che tu non lo conosca già :-)</cite>

<cite>Il gioco (in italiano) è <a href="http://www.ogame.it">OGame</a>. È una simulazione strategica di gestione di un pianeta, con relative azioni militari (difesa e espansione coloniale), commerciali e scientifiche. Funziona direttamente dal browser, per cui si può usare con qualunque computer (anche vecchio), sia con Windows, che con Mac, che con Linux. In particolare funziona benissimo sul nostro Safari.</cite>

<cite>Abbiamo già provveduto a creare un'alleanza di macusers, che si chiama appunto <a href="http://www.forumfree.net/?c=68452">M.A.C</a>.</cite>

<cite>È un gioco più &ldquo;lento&rdquo; di altri, che richiede spesso giocate brevissime ma frequenti, perché le attività di produzione continuano anche mentre si è offline. :-)</cite>

<cite>Chi dice che con il Mac non ci si diverte?</cite>

OGame non lo conoscevo. Mille grazie, Daniele!

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>