---
title: "Per favore, leggi"
date: 2007-09-02
draft: false
tags: ["ping"]
---

Ognuno avrà i suoi propositi classici da rientro dalle vacanze. Mi permetto di suggerirne uno, che arriva dall'esperienza di questa vacanza e dei temi che l'hanno vivacizzata, lato informatico.

Leggi la carta.

Su Internet ci sono un sacco di cose. C'è da divertirsi ogni dieci secondi. Qualsiasi voce, qualsiasi peto, qualsiasi ruttino, anche l'informazione più insignificante trova decine di magnifici interpreti ansiosi di gonfiarla, espanderla, specularci sopra, inventare storie meravigliose, placare l'ansia di novità di ognuno e regalare a tutti dieci minuti di distrazione favolistica analoga a quella che dà la velina con il calciatore o il principe Harry nell'esercito di Sua Maestà.

Ma non impari un cavolo.

Internet sarebbe il massimo per imparare, a patto di dedicargli tempo e impegno. Invece si usa come se fosse il massimo per riempire la frustrazione di tutti gli afflitti da deficit di attenzione.

Ergo. Leggi la carta. È l'unico modo che hai per imparare qualcosa di come funzionano i computer o i Mac. O la vita, magari.

Se pensi che questo sia uno spot per Macworld, hai proprio sbagliato indirizzo. Vai su qualche forum. C'è il <em>rumor</em> del prossimo mezzo secondo, mica vorrai perdertelo.