---
title: "Con Aspyr si gioca seriamente"
date: 2006-04-29
draft: false
tags: ["ping"]
---

<cite>Molti ci hanno chiesto che impatto avrà Boot Camp sul gioco per Mac e sulla nostra azienda.. La nostra posizione è semplice e si basa sull'esperienza: più hardware vende Apple, più giochi Mac vendiamo. […] Aspyr continuerà a concentrarsi su tre cose:</cite>

<cite>
<ol type="1">
	<li>Presentare giochi nativi Mac il più rapidamente possibile.</li>
	<li>Offrire la migliore esperienza di gioco e il miglior supporto possibile ai nostri clienti Mac.</li>
	<li>Dare ancora più scelta ai giocatori Mac.</li>
</ol>
</cite>
A quanto pare, non sembra che Boot Camp stia avendo questo impatto sui giochi Mac nativi. In <a href="http://www.macsonly.com/index.html#_211" target="_blank">Aspyr</a>, certo.