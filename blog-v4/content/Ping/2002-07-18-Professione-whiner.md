---
title: "Professione whiner"
date: 2002-07-18
draft: false
tags: ["ping"]
---

Al termine di ogni Macworld Expo c’è chi si lamenta. Ecco come

Whiner: lamentoso.
Ventcinque anni che Apple è all’avanguardia e venticinque anni che i lamentosi si lamentano sempre allo stesso modo. Ecco la guida definitiva.

Sono utente professionale e hanno aggiornato la gamma consumer.
Sono utente consumer e hanno aggiornato la gamma professionale.
Ho comprato un Mac il mese scorso e ora è già obsoleto.
Volevo comprare un Mac il mese prossimo ma ora costa troppo.
Un sito di pazzi creativi aveva annunciato un prodotto impossibile e Apple non lo ha presentato veramente.
Avevo bisogno di un prodotto su misura e Steve Jobs ha presentato prodotti di massa.
Avevo bisogno di un prodotto di massa e Steve Jobs ha presentato prodotti su misura.
Ho comprato un Mac cinque anni fa e Apple non mi dimostra riconoscenza con uno sconto che nessun altro potrebbe avere tranne me.
Ho venduto azioni Apple prima del keynote e il titolo è salito.
Ho comprato azioni Apple prima del keynote e il titolo è sceso.
Volevo un palmare e Apple non ha presentato un palmare.
Volevo un Mac e Apple ha presentato una cosa che non è un Mac.
Ho bisogno di sentirmi importante e allora scrivo su una mailing list che Apple mi ha deluso, a prescindere da qualsiasi annuncio Apple.

Da rileggersi prima di ogni Macworld. Per vaccinarsi.

<link>Lucio Bragagnolo</link>lux@mac.com