---
title: "Controllo cellulare"
date: 2007-05-17
draft: false
tags: ["ping"]
---

<strong>Pierfausto</strong> chiedeva quali erano i programmi usati da <strong>Dany</strong> per collegare cellulare e Mac via ssh. Sono <a href="http://s2putty.sourceforge.net/" target="_blank">Putty</a> e <a href="http://www.idokorro.com/products/ssh-features.shtml" target="_blank">Mobile Ssh</a> di Idokorro.

Uno è <em>free</em> e <em>open source</em>, l'altro è a pagamento con versione <em>trial</em> disponibile.

Sono già ben oltre i confini della mia competenza in questa regione. Quindi niente domande, al massimo posso accettare buone risposte. :-)