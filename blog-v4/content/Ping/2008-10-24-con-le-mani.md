---
title: "Con le mani"
date: 2008-10-24
draft: false
tags: ["ping"]
---

Oggi momento di smanazzamento di un MacBook nuova edizione.

Il monoblocco di alluminio è un <em>plus</em> notevole, per estetica e sostanza. È resistente, sottile, leggero. Niente cuciture, niente saldature, niente fessure, niente scanalature, niente giunture.

La tastiera è molto buona, risponde bene e i tasti sono distanziati come piace a me (cosa che non soddisfa alcuno standard, però mi sono trovato bene). La tolleranza tra i tasti e i loro alloggiamenti è minima, quasi perfetta.

Call For Duty si gioca. Su un MacBook di dieci giorni fa non si sarebbe giocato. Si gioca con soddisfazione. Attendo l'occasione di provare World of Warcraft, che - dice Apple - guadagna meno dalla nuova Gpu.

La velocità percepita è normale, o meglio non saprei che dire. È un MacBook, non ho grandi esperienze di lavoro continuato con i MacBook. Mi sembra tutto in linea con i MacBook che ho già conosciuto.

L'ambiente era chiuso, con luci basse e calde. Lo schermo <em>glossy</em> non ha dato impacci particolari, ma devo sospendere il giudizio perché è stato tutto tranne che un test specifico.

Il MacBook era in assistenza presso Mac@Work (l'assistenza più veloce del mondo direi). Insieme a me c'era un amico, che alla fine ne ha ordinato uno.

Io aspetto e spero nel 17&#8221;.