---
title: "Siti con il condizionale"
date: 2005-02-22
draft: false
tags: ["ping"]
---

Quando un sito fa un'illazione, è perché non ha notizie vere

Ultimamente qualcuno si è messo a parlare dell'ipotesi di una fusione, o acquisizione, o forse emulsione, chissà, tra Apple e Sony.

Il motivo? Uno dei signori Sony è salito sul palco del Macworld Expo a parlare a fianco di Steve Jobs. Si sono sorrisi, hanno fatto battute, hanno mostrato ognuno la propria merce fino a quando il signor Sony ha abbandonato il palco, tra applausi e manifestazioni di simpatia.

Immaginati se avvenisse una fusione tra nazioni tutte le volte che due plenipotenziari si incontrano e posano sorridenti mano nella mano davanti ai fotografi.

Qualcuno, evidentemente amante dei paradossi, ha tenuto a rimarcare che le ipotesi di fusione si erano sentite anche nel 1990, quando Sony aveva costruito il PowerBook 100 per conto di Apple. Implicando che, siccome si erano dette sciocchezze allora, si possano ripetere le stesse sciocchezze adesso.

Diffida dei siti dove si usano troppi condizionali. Lavorano tanto, ma di fantasia.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>