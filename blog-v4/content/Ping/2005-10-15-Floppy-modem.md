---
title: "Floppy modem"
date: 2005-10-15
draft: false
tags: ["ping"]
---

Un altro vecchio protagonista dell'informatica si prepara a lasciarci

Era ancora il secolo scorso. Apple presentò un computer che nessuno aveva previsto: tondeggiante, colorato e senza lettore di floppy disk.

Lesa maestà.

Non sarebbe andato da nessuna parte, dicevano. Un grave handicap, scrivevano. Un giocattolo, commentavano.

Se ne vendettero milioni.

È cambiato il secolo. Apple ha appena presentato un iMac che nessuno ha previsto: senza modem interno. È solo opzionale e costa cinquanta euro.

Sacrilegio.

C'è il digital divide, stanno dicendo. Da tante parti non arriva l'Adsl, protestano. L'ennesimo furto di Apple, si lamentano.

Dopo che fu uscito l'iMac senza floppy, uscirono vari modelli di lettore esterno Usb.
Adesso che è uscito l'iMac senza modem, usciranno vari modelli di modem esterno Usb.

Più che altro, il lettore di floppy disk non ritornò mai più. Segno che Apple aveva ragione. Vediamo se torna il modem. Se non torna, Apple ha ragione anche stavolta.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>