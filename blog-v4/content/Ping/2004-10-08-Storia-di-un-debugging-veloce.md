---
title: "Storia di un debugging veloce"
date: 2004-10-08
draft: false
tags: ["ping"]
---

Mac OS X ha il grande vantaggio che possiamo chiedergli perché sbaglia

Ho bisogno di un certo dato e lancio Monitoraggio Attività. Parte… e si chiude. Ohibò. Tutto il resto funziona. Riprovo. Uguale. Logout/login. Uguale. Provo da un'altra utenza. Parte e si chiude. Cancello le sue preferenze in Home/Library/Preferences. Niente.

Il mio sapere si esaurisce. Non ho un problema io, ma il sistema. Che risponda lui, allora.

Apro la Console, che registra l'attività del sistema, e lancio Monitoraggio Attività. Lui si Apre e si chiude, ma in Console resta un messaggio di errore! I permessi del programma si sono rovinati e bisognerebbe riparare i privilegi.

Ok. Lancio Utilità Disco e riparo i privilegi. Rilancio Monitoraggio Attività e parte bello felice, che neanche il giorno dell'installazione.

Tutto a posto. Grazie Mac OS X, che come tutto il software non è perfetto, ma almeno sa quando e perché.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>