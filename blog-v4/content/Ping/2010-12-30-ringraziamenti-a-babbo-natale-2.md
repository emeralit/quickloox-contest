---
title: "Ringraziamenti a Babbo Natale / 2"
date: 2010-12-30
draft: false
tags: ["ping"]
---

Babbo Natale, che colgo l'occasione per ringraziare, mi ha generosamente rifornito di un Magic Trackpad Apple, ora in pieno funzionamento accanto alla mia tastiera Bluetooth.

Ho sempre Magic Mouse sulla scrivania, ma non lo sto usando e ancora non ho trovato situazioni in cui sembra avere un vantaggio decisivo rispetto a Magic Trackpad. Tranne, forse, i momenti in cui si seleziona qualcosa da un lungo elenco di file o di cartelle.

Per il momento i due apparecchi mi sembrano abbastanza equivalenti e non ho una preferenza netta. Il periodo di spaesamento nell'adozione di Magic Trackpad dura pochi minuti; la differenza più grande è che, quando la mano lascia la tastiera alla ricerca del puntatore, Magic Mouse è più o meno dove lo avevo lasciato l'ultima volta e il corpo lo &#8220;ricorda&#8221;. Magic Trackpad invece sta sempre l&#236;. È pur vero che la sua superficie è grande abbastanza da farsi centrare senza problemi anche quando lo sguardo è fisso sullo schermo.

La risposta della superficie è assolutamente perfetta e il tocco al posto del clic infallibile. Da raccomandare pienamente al posto del clic, che trovo faticoso e scomodo rispetto a Magic Mouse. Non è un oggetto da usare per cliccare. I gesti invece sono agevoli e comodissimi e, da utilizzatore di portatile, mi trovo ad agio nel momento in cui i movimenti delle dita diventano identici sia quando sono alla scrivania, e uso Magic Trackpad, sia quando sono in giro e uso il <i>trackpad</i> del portatile.

Mi sembra che con Magic Trackpad risulti appena più lento puntare con precisione qualcosa sullo schermo, specie schermo grande. Ininfluente nell'uso normale, forse è qualcosa che potrebbe fare la differenza su applicazioni specifiche, per esempio (azzardo) il fotoritocco. Selezionare testo è del tutto equivalente. Ho fatto una prova in World of Warcraft e l'usabilità è identica (attenzione, però: non uso il mouse per correre come fanno molti).

L'impressione immediata è che per una persona normale Magic Trackpad e Magic Mouse siano perfettamente intercambiabili e serva uno dei due a piacere, non necessariamente tutti e due. Devo dire che, a questo punto, dovrebbero offrire un Magic Mouse in alluminio, o l'immagine della scrivania comincia a risentirne.

