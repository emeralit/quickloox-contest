---
title: "Rosa, rosae"
date: 2010-10-30
draft: false
tags: ["ping"]
---

Grazie a <b>Daniele</b> ho scoperto l'esistenza di <a href="http://www.cinder.hk/LatinEdit/Overview.html" target="_blank">LatinEdit</a>, programma gratuito impagabile per chi debba effettuare trascrizioni da testi antichi, principalmente disponibili solo come file grafici, nella nostra lingua.

A che serve? A nulla, per quasi tutti. A salvare il mondo anche questa settimana è quel <i>quasi</i>.