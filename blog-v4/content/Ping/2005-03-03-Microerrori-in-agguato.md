---
title: "Microerrori in agguato"
date: 2005-03-03
draft: false
tags: ["ping"]
---

Presto e bene non vanno insieme neanche sul Mac

Non posso parlare di evidenza statistica, ma comincio a sospettare pesantemente che la mia tesi sia giusta.

Da Carbon Copy Cloner in poi sono apparsi vari programmi che semplificano il problema del trasferimento dei dati da un Mac a un altro e, banalmente, riescono a eseguire la copia di massa di tutto il materiale desiderato. Oramai lo fa anche Mac OS X al momento di installarsi.

Con i dischi di oggi, però, il numero di file da spostare è enorme, spesso oltre duecentomila o trecentomila. È probabilissimo che un numero anche molto piccolo di file, diciamo anche uno su trecentomila, non venga spostato come si deve. E causi bug anche veramente strani.

Morale: sono sistemi che fanno risparmiare tempo. Ma bisogna essere pronti a ripetere un'installazione, dovesse non andare bene.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>