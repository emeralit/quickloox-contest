---
title: "Cose da Prima repubblica"
date: 2009-08-09
draft: false
tags: ["ping"]
---

C'è stato un tempo in Italia in cui molte decisioni più impopolari di altre venivano fatte passare a Ferragosto. Meno giornali, meno attenzione, più indifferenza, più voglia di pensare ad altro e cos&#236; un aumento delle tasse o della benzina faceva meno clamore.

È un caso l'ottenimento del <a href="http://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO1&amp;Sect2=HITOFF&amp;d=PALL&amp;p=1&amp;u=%2Fnetahtml%2FPTO%2Fsrchnum.htm&amp;r=1&amp;f=G&amp;l=50&amp;s1=7,571,169.PN.&amp;OS=PN/7,571,169&amp;RS=PN/7,571,169" target="_blank">brevetto dei documenti di word processing in Xml</a> da parte di Microsoft proprio in questi giorni.

Però è un bel caso, perché sembra la stessa situazione. I linguaggi di marcatura sono usati da quarant'anni per innumerevoli scopi e di documenti i Xml ne esistono già milioni. Applicare Xml a una categoria di documenti non è un'invenzione. È il centomillesimo uso diverso di Xml, che certamente richiede perizia tecnica e lungo lavoro, ma non contiene in sé alcuna originalità né evidenzia alcun progresso.

Come brevettare l'uso del sapone per lavarsi le orecchie. Il sapone c'è da sempre, le orecchie pure. Mettere insieme le due cose richiede destrezza, ma non certo genialità, semmai buone abitudini del mattino e costanza quotidiana.