---
title: "La naturale regolarità"
date: 2007-09-10
draft: false
tags: ["ping"]
---

Ho ripreso in mano, per piacevole necessità, le espressioni regolari. Due piccole novità almeno per me, in materia: <a href="http://tearlachsoftware.net/home/node/12" target="_blank">RegExpress</a> e la <a href="http://regexlib.com/default.aspx" target="_blank">Regular Expression Library</a>.

Per il curioso, un esempio banalissimo: hai un file di testo, che so, di un milione di righe. Ogni riga contiene una o più date, scritte in molti formati differenti: 10/9/2007, 10/09/2007, 10-09-2007, 10-9-2007, 10/9/07, 10/09/07 per esempio.

Ti tocca uniformare il formato, in modo che tutte le date siano (per dire) 11/09/2007.

Con una espressione regolare puoi farlo in un colpo solo.

E questo è un esempio <em>semplice</em>.