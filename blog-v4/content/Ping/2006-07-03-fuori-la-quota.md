---
title: "Fuori la quota"
date: 2006-07-03
draft: false
tags: ["ping"]
---

Apple ha inanellato numerosi trimestri positivi. La crescita è complessivamente superiore alla media dell'industria, iPod è il player musicale per definizione, la transizione a Intel per ora sta andando bene, fatturato in crescita, utili sostanziosi, piani di crescita per il futuro. Sembra che le cose stiano andando bene.

Eppure la quota di mercato di Apple è praticamente la stessa di pochi anni fa, punto più, punto meno.

C'era un tempo in cui non si parlava d'altro. Senza quota di mercato Apple doveva scomparire, non aveva futuro, era condannata. Alla prova dei fatti, sembra che Apple possa durare o scomparire, ma certamente non per via della quota di mercato.

Dove è finita, la quota di mercato?