---
title: "Il plurale di aneddoto"
date: 2005-09-30
draft: false
tags: ["ping"]
---

La statistica funziona per numeri abbastanza grandi

Ha deciso che per il momento non passa a Tiger. Non funziona tanto bene.

Gli chiedo che cosa ha scoperto. Uso Tiger da cinque mesi ma c'è sempre da imparare.

Risponde che glielo ha detto un suo amico.

Capisco. Quindi Tiger non funziona a qualche milione di persone perché l'amico ha detto bla bla bla.

Prossimamente, due righe sugli esperti di dischi rigidi che schifano Maxtor, o Seagate, o Western Digital, o Ibm, o Hitachi, o boh, perché se ne sono rotti due, tre, cinque.

È una reazione umana. Non comprerò mai più un cordless Siemens. Ma dire che i cordless Siemens non funzionano, perché non ha funzionato il mio, è una scempiaggine.

Le statistiche si fanno su campioni significativi. E il plurale di aneddoto non è dati.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>