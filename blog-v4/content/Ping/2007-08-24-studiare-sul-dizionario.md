---
title: "Studiare sul dizionario"
date: 2007-08-24
draft: false
tags: ["ping"]
---

Pungolato da <strong>effedieffe</strong>, ritorno brevemente sul confronto tra <a href="http://www.barebones.com/products/bbedit" target="_blank">BBEdit</a> e <a href="http://macromates.com" target="_blank">TextMate</a>.

Definire <em>migliore</em> questo o quello non è alla mia portata. Le necessità e le funzioni sono talmente tante che fuori dal mio orticello non saprei mai giudicare, che so, la qualità del supporto dei linguaggi da parte dell'uno o dell'altro. Per esempio BBEdit supporta Lua e Textmate no, ma TextMate prevede comandi speciali per Rails e BBEdit no. Questo da solo potrebbe portare a decisioni differenti diverse persone (sviluppare un plugin per World of Warcraft può essere molto redditizio e si fa in Lua. Analogamente, Ruby on Rails è una piattaforma molto apprezzata per lo sviluppo web).

Superficialmente, resto per ora dell'opinione che BBEdit sia fantastico per gente che scrive e programma, mentre TextMate sia una ottima alternativa per gente che programma e scrive.

Una cosa la posso dire per certo: il dizionario AppleScript di BBEdit è superiore a quello di TextMate. E a me la cosa fa una differenza notevole.

TextMate, di suo, è molto più personalizzabile graficamente. Vogliamo fare un elenco di confronti per vedere dove è mmeglio uno o l'altro? Sono qui disponibile. :)