---
title: "Nel regno del fattibile / 7"
date: 2010-06-03
draft: false
tags: ["ping"]
---

<cite>pregasi elencare azioni fattibili su un iPad e non fattibili su un Sony Vaio W da 10&#8221;</cite>.

<a href="http://mleddy.blogspot.com/2010/06/ipad-and-dolphins-for-real.html" target="_blank">Addestrare un delfino chiamato Merlin</a>.

Gli viene mostrato un oggetto reale e gli viene chiesta di &#8220;toccarne&#8221; la raffigurazione sullo schermo di iPad.

A volte la realtà supera persino la satira. Il caustico The Onion <a href="http://www.theonion.com/articles/do-the-new-tablets-own-up-to-the-hype,16938/" target="_blank">scriveva</a>, a firma <i>Beepo il delfino</i>:

<cite>&#8230;devo chiedermi se i geni al servizio di Steve Jobs si siano fermati almeno una volta a pensare che cosa potrebbe accadere, per esempio, se un mammifero acquatico volesse usare la propria tavoletta per giocherellare in una graziosa baia sull'oceano</cite>.

Forse s&#236;. Ci sono anche <a href="http://speakdolphin.com/pressRelease/Press_Release_iPad_1.pdf" target="_blank">le foto</a>.