---
title: "Non ti leggo e ti scarico"
date: 2005-12-28
draft: false
tags: ["ping"]
---

Trucchetto per decidere che cosa fare di un Pdf presente sul Web

Chiudi Safari e lancia il Terminale. I comandi da dare sono <em>esattamente</em> (niente errori, niente variazioni di maiuscole e minuscole) questi.

<code>defaults write com.apple.Safari WebKitOmitPDFSupport -bool YES</code>

per scaricare i Pdf sul disco invece che aprirli dentro una finestra di Safari.

Apri il pannello QuickTime nelle Preferenze di Sistema, clicca sulla linguetta Avanzate e poi sulle impostazioni Mime. Assicurati che nell'elenco delle immagini la voce Pdf Image, o Immagine Pdf, <em>non</em> sia selezionata. Altrimenti Safari tratterà il Pdf alla stregua di un documento QuickTime e sarai daccapo.

Chiudi le Preferenze di Sistema e sei a posto. Nel caso cambiassi idea, il comando da Terminale per rimettere le cose come erano prima è

<code>defaults delete com.apple.Safari WebKitOmitPDFSupport</code>

Tutto questo vale se non hai già installato plugin di Adobe o di altri che provvedono da soli.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>