---
title: "Al massimo si azzoppa"
date: 2003-03-25
draft: false
tags: ["ping"]
---

Microsoft non ucciderà Virtual Pc; al massimo lo azzopperà involontariamente

C’è una sola cosa peggio di Microsoft quando abusa del suo monopolio: la gente che ne parla senza sapere perché.

Da quando Bill Gates e ciurma hanno acquisito Connectix, si sprecano illazioni e fantasie sul futuro di Virtual Pc. Molti sostengono che Microsoft potrebbe uccidere il popolare emulatore, perché... perché... in realtà senza un perché.

La verità è che Microsoft non era interessata a Virtual Pc, ma a Virtual Server, il nuovo prodotto Connectix. E le faceva gioco, per tutta una serie di ragioni, acquisire l’azienda anziché il prodotto.

Virtual Pc produce profitti e nessuno uccide un programma che rende. Piuttosto, essendo Microsoft interessata a Virtual Server, metterà su quest’ultimo i programmatori migliori. Essendo l’emulazione più arte che scienza, Virtual Pc potrebbe soffrire di mancanza di talenti a esso dedicati e azzopparsi.

Ma è ancora tutto da vedere e Virtual Pc resterà sul mercato.
Mi tocca perfino difendere Microsoft. Roba da non credere.

<link>Lucio Bragagnolo</link>lux@mac.com