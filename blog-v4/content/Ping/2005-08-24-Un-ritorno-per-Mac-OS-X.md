---
title: "Un ritorno per Tiger"
date: 2005-08-24
draft: false
tags: ["ping"]
---

Fine dell'incompatibilità tra Mac OS X e uno dei suoi antivirus

Dal 29 agosto sarà di nuovo disponibile <a href="http://www.mcafeesecurity.com/it/products/mcafee/smb/antivirus/virex_smb.htm">McAfee Virex</a> compatibile Mac OS X. O meglio, lo è sempre stato, solo che il programma non funzionava più con Tiger, al punto che Apple lo ha tolto dalla dotazione di base del servizio .Mac.

Ora ritorna e si può solo esserne contenti.

Non sto dicendo che tornerà anche su .Mac. Apple non ha dichiarato niente e non si sono ragioni evidenti per cui dovrebbe avvenire.

Se siamo fortunati, basterà questa frase per fare partire un telefono senza fili pazzesco sui siti-spazzatura.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>