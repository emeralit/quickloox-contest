---
title: "La visita di Hassan"
date: 2006-04-03
draft: false
tags: ["ping"]
---

Sbagliando sicurezza si impara

Sto viaggiando in tram verso la redazione di Macworld e sono in ritardissimo con una scadenza. Morale: sto finendo il lavoro sul tram, collegato a Internet attraverso il cellulare Bluetooth.

Però mi sono dimenticato di deselezionare la voce Discoverable nella mia connessione Bluetooth. Per tre volte arriva il messaggio di un certo Hassan, che mi offre di scaricare un certo file dal nome incomprensibile.

Siccome ne ho letto, so che si tratta di un virus per cellulari, e mi guardo bene dall'accettare la caramella di uno sconosciuto. Ma avrei rifiutato anche se lo sconosciuto si fosse chiamato Samantha e se mi avesse offerto un file sexy.jpg. Nel caso di Hassan non era certo lui a darsi da fare, ma il virus.

Però: lezione. Se non ti tieni defilato, il mondo sa che esisti. E non tutti sono buoni.

Di buono c'è una notizia. Sono stato sprovveduto, ma il Mac mi ha protetto a sufficienza.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>