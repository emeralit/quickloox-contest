---
title: "La resa degli account"
date: 2006-05-06
draft: false
tags: ["ping"]
---

Sono un po' indietro con la posta e diverse persone mi hanno scritto in arrivo da Ping!. Prego tutti di avere un pizzico di pazienza e arriverò a ognuno. :-)

Nel frattempo mi sto chiedendo perch&eacute;, dei numerosi account <a href="http://mail.google.com" target="_blank">Gmail</a> che mantengo, alcuni permettono il pilotaggio da tastiera e gli altri no. Forse sto tralasciando qualche preferenza, o chissà.

Se qualcuno ancora non avesse un account Gmail, si faccia invitare. Se è solo al mondo, lo invito volentieri io. Entro qualche giorno. :-)