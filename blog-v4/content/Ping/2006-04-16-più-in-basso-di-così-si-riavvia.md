---
title: "Più in basso di così si riavvia"
date: 2006-04-16
draft: false
tags: ["ping"]
---

Se qualcuno avesse mai avuto la curiosità di sapere perch&eacute; 10.4.6 fa riavviare due volte i Mac PowerPc dopo l'aggiornamento, sappia che è dovuto alla presenza di una versione pesantemente rinnovata della libreria <code>/usr/lib/libSystem.B.dylib</code>.

&Egrave; talmente vitale al funzionamento del sistema che, dati i cambiamenti, non la si può cambiare al volo e serve una sostituzione molto prudenziale della libreria stessa in due passi distinti.

Su Intel non serve perch&eacute; i cambiamenti apportati sono di entità molto minore.