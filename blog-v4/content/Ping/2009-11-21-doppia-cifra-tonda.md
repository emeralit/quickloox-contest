---
title: "Doppia cifra tonda"
date: 2009-11-21
draft: false
tags: ["ping"]
---

Chiunque volesse dire la propria sul numero cartaceo di Macworld numero 200, datato per giunta gennaio 2010, <a href="http://www.macworld.it/showPage.php?template=approfondimento&amp;id=574" target="_blank">può farlo</a>.

Sarebbe bello creare per l'occasione un numero davvero collettivo. :-)