---
title: "Imitatori che non fanno ridere"
date: 2005-09-15
draft: false
tags: ["ping"]
---

E c'è persino qualcuno che dà loro soldi

Sono anni che esiste Konfabulator e mesi che esiste Dashboard. È prossimo l'avvento di una grande novità: i <a href="http://microsoftgadgets.com/">Microsoft Gadgets</a>.

Brutta cosa l'invidia. Specie quando sei cronicamente in ritardo.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>