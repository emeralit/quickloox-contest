---
title: "La strana coppia"
date: 2009-10-04
draft: false
tags: ["ping"]
---

In questi giorni il lavoro mi obbliga ad avere tonnellate di pagine web aperte, anche duecento per volta, e basta una pagina più pesante delle altre, o un sovraccarico del plugin di Flash, a creare problemi.

Cos&#236; spesso uso un browser ausiliario per sbrigare le cose non necessariamente di lavoro e la scelta migliore si è dimostrata&#8230; WebKit. S&#236;, il Safari in via di sviluppo, che adesso permette l'aggiornamento quotidiano con un clic.

Dal mio punto di vista è scelta fantastica perché WebKit recupera le scelte di interfaccia che ho fatto in Safari e risponde agli stessi comandi. In altre parole posso usarlo esattamente come uso Safari, anche se Safari è impegnato su altre cose e anche se si è bloccato.

Pensavo che il motore Html interno non sopportasse questa condizione e invece ho dovuto ricredermi.

Qualche intoppo curioso però succede: l'altro giorno Safari si è impallato su una pagina che non voleva aprirsi. Ho lanciato WebKit (si distingue per il bordo dorato sull'icona, ma il menu è identico a quello di Safari) e mi sono messo a fare altro in attesa che Safari si sbloccasse. Dopo un po' non si sbloccava e l'ho terminato con un'uscita forzata.

Ma vedevo ancora la pagina impallata. Difatti ho rilanciato Safari e avevo lui, WebKit e un terzo incomodo con la pagina in questione. Monitoraggio Attività ha mostrato che era in funzione <i>una terza istanza</i> di Safari. Terminata quella (e chissà come è partita), il sistema è tornato a posto.

Il bello è venuto subito dopo. Tornato su Safari, gli ho chiesto di riaprire le pagine della sessione precedente. Diligemente, ha aperto&#8230; la pagina caricata appena prima da WebKit.

<a href="http://www.apple.com/it/safari/" target="_blank">Safari</a> e <a href="http://webkit.org/" target="_blank">WebKit</a> convivono molto bene sotto lo stesso tetto. Ogni tanto, come in tante convivenze, ne combinano qualcuna buffa.