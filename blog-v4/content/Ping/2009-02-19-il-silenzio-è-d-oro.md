---
title: "Il silenzio è d'oro"
date: 2009-02-19
comments: true
tags: [Rai, Silverlight, Microsoft]
---
Mi segnala **Iruben** che <a href="http://www.rai.tv/dl/RaiTV/programmi/media/ContentItem-262d13ee-b9bf-440b-a448-9683abf798a1.html?p=0" target="_blank">i video di Sanremo sul sito Rai</a> sono (scandalosamente) in formato Silverlight Microsoft.<!--more-->

Scandalosamente ma prevedibilmente. &#200; notorio che i rapporti tra Rai e Microsoft non hanno niente a che vedere con fatti tecnici e la questione del servizio pubblico che si adagia sugli interessi privati &#232;, pi&#249; che annosa, pluridecennale.

La cosa veramente incredibile &#232; che sulla pagina non c&#8217;&#232; neanche un avviso. *Se non vedi i video, scarica l&#8217;apposito* plugin.

La Rai dovrebbe essere un sito per gli spettatori; invece &#232; definitivamente un sito per Microsoft. Non devono parlare pi&#249; a nessuno, solo incassare.