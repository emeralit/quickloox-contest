---
title: "Metodi di cattura"
date: 2008-01-21
draft: false
tags: ["ping"]
---

Hai provato a usare i comandi di acquisizione di schermata tradizionali di Mac OS X (in special modo Comando-Maiuscole-4) e fare entrare in gioco <a href="http://www.macworld.com/article/131404/2008/01/screencapopts.html" target="_blank">barra spazio, Opzione e/o Maiuscole</a>?

È uno spasso.