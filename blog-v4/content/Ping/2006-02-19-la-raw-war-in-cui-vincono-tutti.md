---
title: "La Raw War in cui vincono tutti"
date: 2006-02-19
draft: false
tags: ["ping"]
---

Guerra per modo di dire. Competizione salutare e benefica per tutti, piuttosto. L'amico <strong>Flavio</strong> de <a href="http://www.loscrittoio.it/" target="_blank">Lo Scrittoio</a> segnala RAW Developer, programmma serio per la conversione del formato Raw, prodotto da una software house indipendente e disponibile solo su Mac OS X. Niente versione Windows. Perfetto per sfottere l'amico. Ma è un'altra questione.

Non sono programmi totalmente sovrapponibili, ma avere Aperture, il prossimo Lightroom di Adobe e anche <a href="http://www.iridientdigital.com/" target="_blank">RAW Developer</a> è indizio di qualcosa che ha detto bene Flavio: <cite>credo che il vicino futuro degli appassionati di fotografia digitale sia roseo davvero</cite>.