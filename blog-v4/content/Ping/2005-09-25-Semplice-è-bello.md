---
title: "Semplice è bello"
date: 2005-09-25
draft: false
tags: ["ping"]
---

I maestri della facilità di utilizzo non dovrebbero snobbare il galateo di Internet

Una volta ignoravo le comunicazioni dirette da Apple, un po' perché c'è stato un periodo in cui i comunicati stampa li traducevo io e un po' perché erano invariabilmente cose su cui ero già aggiornato.

Ora la situazione è cambiata. La newsletter di .mac è piuttosto utile. Così quella per sviluppatori. E se si compra dall'iTunes Music Store, anche quest'ultima.

Ma sono quasi tutte in Html. Grrr. I messaggi in Html sono più pesanti, comportano rischi per la sicurezza e oltretutto a me interessano i contenuti, non il loro tasso di glamour.

Non pretendo che il mondo debba essere come lo voglio io. In giro incontro continuamente gente che se riceve una mail solo testo, come è giusto galateo di Internet, si sente sminuita.

Benissimo. Però, cara Apple, dammi almeno la possibilità di scegliere senza andare a frugare sul tuo sito alla ricerca delle configurazioni. Basta una frasetta a fine newsletter.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>