---
title: "Clima di tensione"
date: 2004-12-05
draft: false
tags: ["ping"]
---

Ad Apple non piace troppo la globalizzazione

In generale la gente che si lamenta della poca iniziativa di Apple Italia poi cerca sistemi per comprare le macchine in America, dato che grazie al cambio dollaro-euro i prezzi negli States sono molto più bassi.

Peccato che questo strangolerebbe Apple Italia, e qualunque altra filiale locale.

È allora normale che si arrivi al fatto del giorno: gli iMac G5 venduti in Usa, al contrario di quanto accade nel resto del mondo, non hanno più l'alimentatore universale e funzionano solo con la tensione di rete Usa.

Ovviamente è solo questione di attaccarci un trasformatore… ma la notizia è più nel principio che nel fatto.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>