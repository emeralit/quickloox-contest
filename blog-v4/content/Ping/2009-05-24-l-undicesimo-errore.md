---
title: "L'undicesimo errore"
date: 2009-05-24
draft: false
tags: ["ping"]
---

Ai tempi di System 7 l'errore 11 era una piaga che non venne eliminata prima di molto tempo.

Oggi è un errore fastidioso di Time Machine. Mac OS X Hints ha pubblicato un tutorial su <a href="http://www.macosxhints.com/article.php?story=20090515063602219" target="_blank">come risolverlo</a>&#8230; e spero ardentemente che non mi capiti.