---
title: "Un uomo solo al comando (del suo Mac)"
date: 2004-11-14
draft: false
tags: ["ping"]
---

Siamo quasi al punto di poter girare un kolossal in salotto

Leggo dall'edizione cartacea di <a href="http://www.film.tv.it">Film Tv</a>, settimanale di cinema e programmi televisivi:

<cite>Il regista Kerry Conran ci aveva mostrato un provino di cinque minuti realizzato sul suo Mac di casa: una cosa da rimanere a bocca aperta […] Il regista e autore assoluto di <em>Sky Captain</em>, Kerry Conran, 37 anni, non aveva mai diretto un film. Per anni ha vissuto davanti al suo Mac e a un fondale blu montato nel suo piccolo appartamento di Los Angeles covando un sogno rivoluzionario: girare un kolossal di fantascienza senza mai uscire di casa.</cite>

La prima parte della citazione è una dichiarazione di Jude Law, protagonista maschile del film. La seconda fa parte del redazionale. Il tutto dovrebbe farti guardare con più rispetto a quella copia di iMovie suo tuo Mac… e al tuo Mac.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>