---
title: "Un grand blog"
date: 2008-11-01
draft: false
tags: ["ping"]
---

Finalmente <strong>Andre</strong> si è messo a bloggare e, come era naturale aspettarsi, si è messo a <a href="http:///www.grand.it" target="_blank">bloggare alla grande</a> (basta che tolga quel <cite>beta!</cite>, che si può permettere solo Google e forse neanche, e che non abbocchi troppo alla tentazione di parlare di politica, che noiosizza i blog ancora più del minimalismo autobiografico).

Per esempio: <a href="http://www.grand.it/2008/typecasting-tipografia-sbagliata-nei-film/" target="_blank">font anacronistici dentro film e telefilm</a>. Fantastico.