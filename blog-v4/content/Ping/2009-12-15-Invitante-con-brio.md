---
title: "Invitante con brio"
date: 2009-12-15
draft: false
tags: ["ping"]
---

Sono lieto di annunciare di avere potuto invitare <i>tutti</i> i richiedenti del secondo giro di Google Wave. Questione di qualche giorno e Google attiverà gli <i>account</i>.

Ancora una volta, chi si sentisse di condividere inviti propri, commenti pure questo messaggio indicando un indirizzo email cui scrivere. Gli altri ricordino che viene fatto per gentilezza e altruismo e chiunque è libero di tirarsi indietro in qualsiasi istante senza dovere alcunché ad alcuno.

Anche Wave, dopo Gmail, sta probabilmente entrando a regime e progressivamente sarà sempre più facile trovare inviti, fino a quando non serviranno più. Attualmente Gmail è aperto a tutti, ma nominalmente ho 85 inviti alla posta di Google rimasti sul mio account principale.

Nel frattempo, se serve ancora qualche invito rimango disponibile fino a esaurimento. La regola rimane la stessa: se sono costretto a rispondere no, bisogna riscrivere.

Esaurite tutte le lungaggini burocratiche, una confessione: tutta la faccenda è uno spasso e mi ero divertito, su scala persino più ampia, già ai tempi di Gmail.