---
title: "Stampa che non passa"
date: 2002-03-15
draft: false
tags: ["ping"]
---

Apple è sempre all’avanguardia tra i sistemi da scrivania con velleità tipografiche

C’era una volta QuickDraw Gx.
Lo hanno visto in pochi e usato ancora meno, ma è stata la massima espressione mai raggiunta dall’informatica in fatto di sistemi di imaging, come dicono in Usa, cioè la resa sullo schermo e in stampa delle istruzioni impartite dal processore per il disegno di testo e grafica.
Atsui, il sistema tipografico di Mac OS X, non è evoluto come QuickDraw Gx, ma non ci possiamo lamentare. Ieri ho prodotto il mio primo file Pdf con Mac OS X. Ho chiesto una stampa e, nelle opzioni di output, ho specificato che volevo un file Pdf.
In un attimo il Pdf era pronto, immodificabile, leggibile universalmente, fedele al millimetro alle mie specifiche.
E ancora una volta ho ringraziato Apple per il vantaggio competitivo che mi dà nei confronti di chi non conosce Mac.

<link>Lucio Bragagnolo</link>lux@mac.com