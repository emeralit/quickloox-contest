---
title: "Questa sì che è una scusa"
date: 2009-09-09
draft: false
tags: ["ping"]
---

Si è conclusa a lieto fine la telenovela di Amazon, che per un problema di <i>copyright</i> aveva messo le mani nei Kindle dei suoi clienti e <a href="http://www.macworld.it/blogs/ping/?p=2638" target="_self">aveva cancellato a loro insaputa</a> libri (ironia) di Orwell distribuiti illegalmente.

Jeff Bezos, l'amministratore delegato, si era scusato con una lettera aperta in cui la parola più gentile con la quale definire l'operato dell'azienda era stupido.

Per farsi perdonare definitivamente, Amazon <a href="http://scitech.blogs.cnn.com/2009/09/04/amazon-returns-deleted-kindle-books/" target="_blank">regala ai clienti colpiti una copia, legittima, dei libri cancellati</a> e restituisce ai clienti in questione tutte le note che loro avevano creato e se ne erano andate assieme a ciascun libro.

Se non sono più interessati ai libri, sempre i clienti hanno diritto a un buono acquisto di trenta dollari su Amazon.

Intanto è cos&#236; che si fa. Brava Amazon. Secondo, assodato che Amazon può mettere le mani nei Kindle dei clienti, è altrettanto assodato che non lo farà più, salvo catastrofi. È un potere che c'è per non essere esercitato, o porta solo guai e alla perdita di clienti, o di denaro.

Per questo Apple, che teoricamente può mettere le mani negli iPhone degli utenti, non lo farà mai, salvo catastrofi. E tutti i discorsi di <i>privacy</i> che si fanno attorno a questo argomento sono aria fritta.