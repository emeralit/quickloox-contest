---
title: "Auguriamocelo / 2"
date: 2010-12-24
draft: false
tags: ["ping"]
---

Non amo diffondere il solito video divertente, irresistibile e geniale. Non lo è.
Come eccezione, il mio augurio vero di Buon Natale sta su [YouTube](https://web.archive.org/web/20101226122327/http://www.youtube.com/watch?v=GkHNNPM7pJA). L’ho trovato bella congiunzione tra una storia molto antica (poco importa se storia nel senso di favola o storia nel senso di resoconto) e una realtà molto moderna.
Di quella storia e di questa realtà, bene usate, ne auguro tanta a tutti, con grande affetto e immensa gratitudine per la compagnia.
