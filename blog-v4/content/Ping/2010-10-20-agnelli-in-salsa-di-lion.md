---
title: "Agnelli in salsa di Lion"
date: 2010-10-20
draft: false
tags: ["ping"]
---

Condizionato nel titolo dalla prima lettura de <a href="http://www.viennepierre.it/cgi-bin/vnpr/default.asp?db=68-1&amp;TabId=3&amp;IdCat=11&amp;TMId=5" target="_blank">La cucina futurista</a>, ricevuto in gradito regalo ieri sera, indico una pantagruelica chat per stasera a chiacchierare in libertà intanto che Apple fa Back to the Mac e, unica previsione certamente vera al 100 percento, presenta Mac OS X 10.7, alias Lion (ma se fosse Cougar non mi meraviglierei).

Ci troviamo come già altre volte ospiti di <a href="http://freesmug.org" target="_blank">FreeSmug</a> (Grazie <b>Gand</b>!), ottimo sito dedicato al software libero per Mac, in chat Irc sul canale <code>#freesmug</code>. Indicativamente dalle 19. Sarò di rientro da Milano e posso tardare di qualche minuto. Se invece arrivassi in anticipo, mi collegherò prima.

Come collegarsi è descritto in <a href="http://www.macworld.it/ping/life/2009/01/02/allenamento-chat-3/" target="_blank">questo vecchio post</a>.

Se serve aiuto, collegarsi con iChat alla stanza lionawakening. Sarò l&#236; a fare da buttadentro appena mi è possibile. Sorry, niente chat dentro quella stanza, lo scopo è favorire l'ingresso in #freesmug.

Per iPhone, iPod touch e iPad, una ricerca di <i>Irc</i> dentro App Store restituirà diverse alternative possibili. Ho visto che Irc999 è gratis e che altre (per esempio Colloquy, LimeChat, Rooms) hanno prezzi modici.

A stasera!