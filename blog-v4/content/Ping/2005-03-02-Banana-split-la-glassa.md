---
title: "Banana split, la glassa"
date: 2005-03-02
draft: false
tags: ["ping"]
---

Mai sottovalutare gli stupidi. Ce la fanno sempre

Due settimane fa scrivevo che qualcuno, di fronte allo split delle azioni Apple, avrebbe sicuramente pensato a chissà quale crollo delle quotazioni.

Come mi segnala Jakaa (che ringrazio), nelle sue pagine di economia e mercati il Corriere della Sera del primo marzo pubblica la quotazione di Apple Computer: 44,86 dollari, meno 49,59 percento.

Manca l'articolo preoccupato sui destini dell'azienda e siamo al completo.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>