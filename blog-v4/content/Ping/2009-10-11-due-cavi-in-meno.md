---
title: "Due cavi in meno"
date: 2009-10-11
draft: false
tags: ["ping"]
---

In casa si sentiva il bisogno di una multifunzione <i>inkjet</i> e c'era la voglia di sperimentare qualcosa di non ovvio. Cos&#236; sono state escluse le varie Hewlett-Packard, Canon, Epson, Lexmark e compagnia per acquistare una <a href="http://store.kodak.com/store/ekconsus/en_US/DisplayProductDetailsPage/productID.145084800" target="_blank">Esp-7 Kodak</a>.

L'acquisto data a ieri ed è presto per dare giudizi sulle prestazioni o sui consumi. Intanto ci sono due cavi in meno; la precedente <a href="http://h10025.www1.hp.com/ewfrf/wc/document?docname=bid08952&amp;cc=it&amp;dlc=it&amp;lc=it&amp;jumpid=reg_R1002_ITIT" target="_blank">DeskJet 995C Hewlett-Packard</a> aveva Bluetooth ma non sono mai riuscito a farci niente senza il cavo Usb e lo scanner <a href="http://www.usa.canon.com/consumer/controller?act=ModelInfoAct&tabact=DownloadDetailTabAct&fcategoryid=351&modelid=8007" target="_blank">N1220U Canon</a> viene da un'epoca che obbligava al collegamento fisico.

Eps-7 invece è <i>wi-fi</i> e ha riconosciuto istantaneamente la rete AirPort. Il PowerBook 12&#8221; con Leopard, installato il <i>driver</i> incluso nella confezione, è diventato subito operativo in stampa e in scansione. MacBook Pro 17&#8221; con Snow Leopard, stessa installazione, pochi risultati. Nel senso che la stampante veniva rilevata sulla rete e niente più.

Ho scaricato dal <a href="http://www.kodak.com/global/mul/service/downloads/DownloadLookup.jhtml?pq-path=13657/13660&product=EKN035329" target="_blank">sito Kodak</a> un driver aggiornato a inizio settembre e ora l'apparecchio ubbidisce anche a Snow Leopard con <i>boot</i> a 64 bit. Nella stessa pagina è presente anche un aggiornamento <i>firmware</i> della stampante che però è datato giugno e dunque potrebbe già essere presente nella macchina. L'idea di aggiornare il <i>firmware</i> mi piace poco, specialmente quando tutto funziona già e quindi per ora, tentennando, lascio le cose come sono.

Per domande o curiosità, a disposizione. :)