---
title: "Nasce un piccolo blog"
date: 2005-12-16
draft: false
tags: ["ping"]
---

Mi tocca dare la notizia, il parere lo darà qualcun altro

Ping.

Una piccola rubrica di opinione sul sito di Macworld Italia. Con un tocco di originalità: quotidiana. È poco, ma nessuno aveva osato farlo prima.

Partita ben prima che scoppiasse la mania dei blog. Ho classificato ogni pezzullo con un codice di tre lettere alfabetiche. aaa, aab, aac e così via. Questo Ping è il &ldquo;numero&rdquo; bmk. Basta fare i conti per capire quanto ci possa essere da dire ogni giorno su Macintosh e dintorni.

Ora Ping sta per diventare un blog. Il primo collegato alle riviste Idg.

Sembra poco, eh? Non sai quanto può essere conservatore un editore di informatica.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>