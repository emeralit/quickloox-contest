---
title: "Goner, l’ennesimo"
date: 2001-12-13
draft: false
tags: ["ping"]
---

I virus Microsoft-only non fanno più notizia

Non sono preoccupato di arrivare in ritardo con questo commento; di virus che colpiscono solo Windows o solo programmi Microsoft ne esce uno a settimana e uno più uno meno si capisce che non fa più differenza per nessuno. La gente, più che vaccinata, è mitridatizzata e un ennesimo macrovirus di Word (ma perché non salvare in Rtf?) o Excel viene accolto come ineluttabile.
Adesso è arrivato Goner, un worm che si propaga attraverso Icq e, manco a dirlo, Outlook Express (ma perché non usare Eudora?). I dettagli si trovano su questa pagina Web. Io mi limito a domandarmi: chi si ostina, nonostante le alternative, a usare programmi soggetti a virus, può definirsi malato?

http://www.securiteam.com/securitynews/6H0001P3GU.html
