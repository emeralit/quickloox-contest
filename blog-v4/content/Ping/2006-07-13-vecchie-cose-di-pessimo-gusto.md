---
title: "Vecchie cose di pessimo gusto"
date: 2006-07-13
draft: false
tags: ["ping"]
---

Spieghi pazientemente al windowsiano fresco di <em>switch</em> che può tranquillamente vedere la cartella in cui stanno le due foto, ma non ne ha nessun bisogno, dal momento che iPhoto ti permette di gestirle a prescindere.

Spieghi con ancora maggiore pazienza che la deframmentazione è un concetto vecchio e che le operazioni di manutenzione di routine su Mac OS X le effettua automaticamente il sistema, altrimenti non sarebbero di routine.

Spieghi sempre più pazientemente che non ha bisogno di un antivirus, ma solo di una normale attenzione.

Il vero virus sono le vecchie abitudini.