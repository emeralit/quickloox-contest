---
title: "Poesia Mac"
date: 2005-01-19
draft: false
tags: ["ping"]
---

Il portale più grande d'Italia sta cambiando atteggiamento

Ho lavorato per un po' a Virgilio, alla vigilia del nuovo millennio. Dentro il Mac era snobbato assai e si vedeva anche fuori. Le pagine Mac venivano aggiornate poco e neanche benissimo.

Nel nuovo millennio un'amica mi ha mostrato il prototipo di una striscia settimanale che comparirà su <a href="http://212.48.1.23/183448.content?mmc=39&mm=30204">Virgilio</a>. È solo un fumetto, ma un bel segno dei tempi che cambiano.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>