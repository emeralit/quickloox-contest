---
title: "Che annuncio sarebbe"
date: 2006-04-01
draft: false
tags: ["ping"]
---

Avevo pensato di annunciare il passaggio a Windows.

Avevo intenzione di proclamare l'acquisto di Microsoft Office.

Avevo pensato di prospettare il mio ingaggio da parte di qualche sito di pseudoinformazione.

Ma è il primo di aprile. Apple compie trent'anni.

E qualunque annuncio sarebbe niente di fronte a Steve Jobs che calca trionfante un palco in zona Cupertino, si avvicina ai microfoni di trecento emittenti di ogni tipo e scandisce:

<cite>Grazie a tutti. Ma sappiatelo. Abbiamo scherzato.</cite>