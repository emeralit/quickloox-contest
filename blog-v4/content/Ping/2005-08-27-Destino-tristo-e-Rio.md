---
title: "Destino tristo e Rio"
date: 2005-08-27
draft: false
tags: ["ping"]
---

La cecità commerciale paga. Chi vede lontano

D&M Holdings, società giapponese con sede a Tokyo, ha annunciato che dalla fine di settembre cesserà di vendere i suoi lettori di Mp3 della serie Rio.

I Rio sono stati i primi lettori Mp3 ad arrivare sul mercato, ben prima di iPod. Sono sempre costati meno di iPod e avevano un sacco di bellissime funzioni in più di iPod, dalla registrazione dell'audio ai sintonizzatori radio incorporati.

Ma, avrebbe detto mia nonna materna, erano brutti come il peccato e non erano quello che la gente voleva, ossia un bel lettore facile da usare.

La logica dell'assemblato orrendo pieno di tutto e in vendita a prezzo stracciato, che tanti danni fa tra i computer, fuori da quel mercato ha dimostrato di non valere una cicca.

Sulla pagina dove ho letto la notizia c'era un'inserzione del negozio di musica online eMusic: vende brani Mp3 per tutti i player.

A contorno dell'annuncio, la foto di un iPod. Il lettore è quello.

R.I.P, Rio.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>