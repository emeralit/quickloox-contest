---
title: "Fumo e arrosto"
date: 2005-11-16
draft: false
tags: ["ping"]
---

Una contraddizione che si fa sempre più evidente

I siti specializzati in pettegolezzi a bassissimo valore aggiunto cominciano ad anticipare una presentazione dei Mac Intel a gennaio, magari proprio in occasione del Macworld Expo di San Francisco. La voce ufficiale di Apple ha detto entro metà 2006 e nient'altro.

Non riesco a capire una cosa.

Sento gente dire che creare il software Universal Binary, che funziona su ambedue i processori, è un problema, che Rosetta è al massimo un pannicello caldo e così via. Che quindi manca il software per i Mac Intel e ci saranno molte difficoltà prima di avere una conversione accettabile del parco software. E diamola per buona.

Ma se è così, che interesse ha Apple ad anticipare la comparsa di computer per i quali non c'è software?

Sento gente dire che oramai bisogna aspettare i Mac Intel e non ha più senso comprare quelli attuali. A parte che non si capisce perché, ma come può valere la pena di acquistare un computer che non ha programmi?

A meno che il software, invece, ci sia. Ma allora i venditori di fumo sono quelli delle difficoltà di riprogrammazione.

Insomma, uno dei due gruppi non me la racconta giusta. Qualcuno mi spiega da che parte si cucina arrosto e dove invece si alza solo fumo?

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>