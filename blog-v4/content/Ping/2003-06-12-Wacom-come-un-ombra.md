---
title: "Come un’ombra, o, meglio"
date: 2003-06-12
draft: false
tags: ["ping"]
---

La perfetta risposta dei nuovi schermi grafici

Il montaggio e l’avvio della tela grafica (pensa a uno schermo da 18” che sta in posizione verticale, come una tela su un cavalletto, su cui si usa uno stilo per disegnare e pilotare i programmi) Cintiq 18SX di Wacom sono stati uno scherzetto. Tutto va al suo posto e funziona subito senza bisogno di dover capire qualcosa attraverso il manuale.

Piacevole, ma non una sorpresa. Invece è stata una sorpresa (piacevole) la risposta immediata e precisa della tela rispetto all’input.

Devi sapere che ho un’età e quindi ho visto l’infanzia delle tavolette grafiche. Erano bellissime, ma con un difetto: spesso mancava potenza di elaborazione e tu tiravi il pennello, ma la tavoletta ti seguiva con qualche istante di ritardo, o peggio ancora recuperava il ritardo disegnando una bella linea retta là dove tu avevi tratteggiato una curva vellutata che nemmeno Picasso nella campagna Think Different (ricordi quella fantastica schiena di toro, con un singolo stupefacente tratto di pennello?).

Ma sto divagando. La Cintiq 18SX di Wacom è un prodotto ben più maturo e progredito.
Painter di Corel (sto provando anche quello) riconosce la tela grafica ed è un programma orientato al pittorico. Puoi cambiare la carta, il tipo di colore usato, il pennello, tutto. Naturalmente lo schermo è sensibile alla pressione e quindi, quando lo stilo impersona un pennello di pelo di bue intriso di tempera all’uovo, la traccia della tempera si fa più intensa e più marcata.

Se provi a tracciare i segni molto velocemente, più velocemente di Picasso ma sicuramente non di Jackson Pollock (quando non era abbastanza veloce lui lanciava direttamente il colore sul dipinto), vedi che la tua azione viene seguita come un’ombra e tradotta in colore digitale fedelmente, senza perdere niente del segno. Da questo punto di vista la Cintiq rivaleggia perfettamente con le vere tele sui veri cavalletti e questo è un segno di grande progresso.

Stefania sta improvvisando pseudofumetti, accennati con pennellate brevi e rapide, e si diverte come una bambina. Quasi quasi mi informo sul prezzo. Con me un acquisto così è sprecato, ma con lei no.

<link>Lucio Bragagnolo</link>lux@mac.com