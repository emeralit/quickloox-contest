---
title: "Fare un libro sulla musica"
date: 2010-05-05
draft: false
tags: ["ping"]
---

Ho terminato di leggere <a href="http://www.hoepli.it/libro/fare-musica-con-il-mac.asp?ib=9788820337940" target="_blank">Fare musica con il Mac</a> dell'amico Giuliano. Ci ho messo tanto perché l'ho proprio letto (la gente che scrive recensioni dà una sfogliata, scorre il sommario, legge la quarta di copertina e poi scrive. C'è gente che neanche apre il libro e copia la quarta di copertina). Letto dalla prima all'ultima pagina, con attenzione financo eccessiva, perché la mia attenzione al mondo della musica è tangenziale e non ho realmente bisogno di tutto il sapere che Giuliano ha riversato in queste pagine.

Un sacco di gente invece ce l'ha. Credono che fare musica significhi attaccarsi a un amplificatore e schitarrare, o che, come certi cattivi fotografi che scattano spazzatura perché tanto poi c'è Photoshop, si possa produrre qualunque cosa perché tanto poi c'è la postproduzione.

No, Giuliano è un musicista vero. Pardon, un vero tecnico del suono. Ripardon, tutt'e due. Come accade raramente, ha nelle mani la poesia e la tecnica insieme. Come accade rarissimamente, è riuscito a trasferire tutto questo nel suo libro.

Che prima spiega il Mac ai musicisti e poi spiega a chi usa Mac la musica, o meglio il fare musica. Ci sono tutti i dettagli tecnici, tutti i numeri da conoscere, tutti i segreti per fare le cose bene, tutti i prodotti cui fare mente locale. Se manca qualcosa, non me ne sono accorto e probabilmente se ne accorgerebbe solo gente già brava a sufficienza da non avere bisogno del libro. A occhio, pochi, pochissimi. C'è anche un autoritratto dell'autore, <i>à la Hitchcock</i>, che compariva sempre in tutti i suoi film per qualche fotogramma.

Auguro a Giuliano di vendere poche copie. Ogni mese. Con&#8230; ritmo regolare. Fino a che non diventa un classico e gli impongono di scrivere la seconda edizione, giusto per aggiornare i prodotti hardware e software e perfezionare qualcosina.

Mi è già capitato di recensire positivamente un libro e sentirmi dire che <i>per forza, è un tuo amico</i>. Spiacente: con gli amici sono più severo che con gli estranei. Il peggior servizio che potrei rendere loro è fare il tappetino e andare di piaggeria. Cos&#236; la gente compra libri mediocri, loro fanno brutta figura e io divento inaffidabile. Agli scettici consiglio piuttosto l'iscrizione a <a href="http://www.musimac.it/" target="_blank">Musimac</a>, comunità in cui Giuliano è molto attivo. Cos&#236; toccano con mano.

Per <a href="http://www.hoepli.it/libro/fare-musica-con-il-mac.asp?ib=9788820337940" target="_blank">fare musica con il Mac</a> questo libro è, a dire poco, necessario. Colma splendidamente il divario tra strumentistica musicale e strumentistica informatica, con tutto il contorno di acustica, accorgimenti pratici e suggerimenti concreti. Ed è anche scritto in italiano.