---
title: "Momenti di grande giornalismo"
date: 2010-07-21
draft: false
tags: ["ping"]
---

È passato oltre un mese dai tre anni dopo che Apple Insider <a href="http://www.appleinsider.com/articles/07/05/24/closing_the_book_on_apples_mac_mini.html" target="_blank">ha dichiarato morto Mac mini</a>.

Dichiarerei inconcludente e inconsistente Apple Insider, per quanto sia battaglia persa. In realtà non si tratta più di notizie, ma di intrattenimento genere <i>fiction</i>.

Nessun problema, il dubbio è quanti ne siano veramente consapevoli e quanti invece finiscano prima o poi per farsi male, con acquisti incauti o decisioni professionali prese sulla base delle fantasie di qualcuno che ha deciso di guadagnare sulla pelle di chi crede a tutto.