---
title: "Soluzioni anticrisi"
date: 2009-01-28
draft: false
tags: ["ping"]
---

Tripod mi ha informato via email che il servizio chiude il 15 febbraio 2009, come il resto delle operazioni di Lycos Europe.

Tripod era esploso assieme al web e dava a tutti la possibilità di farsi la propria pagina web personale.

Poi si è seduto e non ha più innovato, né è migliorato. È sempre rimasto il servizio che ti faceva costruire la pagina web.

Intanto sono arrivati Google Pages, iWeb, MySpace, le installazioni monopulsante di Apache/MySql/Php, mentre il costo di uno hosting è costantemente diminuito e, con tutti i distinguo possibili, la conoscenza media di Internet da parte delle persone è cresciuta in continuazione.

La pagina di <a href="http://www.tripod.lycos.it/" target="_blank">Tripod</a>, nella sostanza, è la stessa del 1999.

Non è un effetto della crisi. Tripod ha fatto per dieci anni il compitino sempre uguale, senza accorgersi che i tempi cambiavano, i comportamenti cambiavano, le persone cambiavano.

Un bel rimedio alla crisi: osservare i tempi, i comportamenti e le persone, poi chiedersi con sincerità davanti allo specchio se stiamo facendo il compitino uguale da dieci anni. Se è cos&#236;, chiudersi in <em>brainstorming</em> e uscirne con tante idee.