---
title: "Oltre il calcolo"
date: 2008-08-09
draft: false
tags: ["ping"]
---

Si sono esercitati in molti nella realizzazione di calcolatrici straordinarie, ma dubito si possa andare oltre <a href="http://futureboy.us/frinkdocs/" target="_blank">Frink</a>.

Per spiegare tutto quello che fa serve una dispensa universitaria. Riassumo rapidamente dal sito.

Riconosce le unità di misura e le interpreta anche se vengono mescolate, per esempio se in una operazione compaiono sia litri che galloni.

Riconosce e converte migliaia (migliaia) di unità di misura diverse.

Traduce frasi in linguaggio naturale.

Calcola il valore delle valute nel tempo (e converte le quotazioni aggiornate).

Calcola il tempo, riconoscere espressioni regolari, valuta stringhe come espressioni, bla bla bla.

La pagina descrittiva è incredibile. La parte più entusiasmante è quella sugli esempi di calcolo. C’è la spiegazione del perché Superman salva la vita a un sacco di gente ma non può salvare le vite di tutti dovunque. Siccome Superman si carica a energia solare, sapendo quanto è alto e quanto pesa, possiamo stimare quanta energia incamera e quindi che sforzi si può permettere. Una delle formule coinvolte è

<code>(225 + 135) pounds 15000 feet gravity / chargerate -> minutes</code>

E voglio proprio vedere un’altra calcolatrice dove si può lavorare allo stesso modo.