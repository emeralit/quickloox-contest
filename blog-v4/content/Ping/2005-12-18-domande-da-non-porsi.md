---
title: "Domande da non porsi"
date: 2005-12-18
draft: false
tags: ["ping"]
---

Tipo: dove trovo tutti i comandi Unix per Mac OS X?

Non chiedere un manuale di istruzioni per usare il Terminale. Ti ritrovi con un elenco di ottocento pagine, necessariamente imcompleto. Molto meglio un sito che riporta i <a href="http://www.westwind.com/reference/OS-X/commandline/">comandi veramente utili</a>.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>