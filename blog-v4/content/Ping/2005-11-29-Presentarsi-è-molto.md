---
title: "Presentarsi è molto"
date: 2005-11-29
draft: false
tags: ["ping"]
---

Potrebbe anche essere Zen

Da quando è uscito il libro <em>Lo Zen e l'arte della manutenzione della motocicletta</em>, la filosofia buddista viene citata troppo spesso e troppo a sproposito. Non toglie che nel mare magnum delle sciocchezze siano nascoste le perle.

Una di esse è questo <a href="http://presentationzen.blogs.com/presentationzen/2005/11/the_zen_estheti.html">confronto</a> tra lo stile di Steve Jobs e quello di Bill Gates nel tenere le presentazioni. Non tanto per vedere cosa abbiano da vendere, quanto per gettare l'occhio su ciò che potrebbero avere dentro, che li agita, o li calma, o li sostiene.

Mi sto avvicinando seriamente al momento in cui la scelta di comprare un Mac dipenderà non solo dalla sua superiorità tecnologica ma anche dalla maggiore limpidezza di pensiero di chi se ne occupa.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>