---
title: "Salvato il soldato Ryan"
date: 2005-06-18
draft: false
tags: ["ping"]
---

Preoccupati dal porting del software Mac su processore Intel? Ecco una buona notizia

Ryan Gordon è il programmatore che ha portato, da solo, il gioco Unreal Tornament su piattaforma Mac.

Alla Wwdc Ryan ha affrontato il problema della conversione dei suoi giochi e del software che gli serve in formato Universal Binary. La conversione di Unreal Tournament 2004 ha richiesto l'aiuto di un ingegnere Apple e ha portato via tre giorni. La conversione di Duke Nukem 3D è stata molto più semplice. Il Torque Game Engine gli ha richiesto &ldquo;letteralmente minuti&rdquo; per essere messo a posto. Il giochino Feeding Frenzy di GameHouse ha richiesto la modifica di una manciata di righe di codice. Delle varie librerie open source che Ryan usa, nessuna ha richiesto cambiamenti. È bastato ricompilarle.

Tutto questo da un programmatore solo, nella settimana della Wwdc.

Una rondine non fa primavera, ma sono notizie che numerosi sviluppatori dovrebbero trovare confortanti. Ryan ha anche dichiarato che gli strumenti di conversione a disposizione sui Mac Intel-based riservati ai programmatori sono ancora rozzi, ma che Rosetta gli è sembrato molto più promettente di quello che pensava.

Il file .plan di Ryan viene costantemente aggiornato e non riporta più il suo racconto della vicenda, ma qualcosa si può ancora leggere su <a href="http://www.macworld.com/news/2005/06/14/unreal/index.php?lsrc=mwrss">Macworld Usa</a>.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>