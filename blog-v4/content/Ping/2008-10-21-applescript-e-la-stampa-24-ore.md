---
title: "AppleScript e la stampa 24 ore"
date: 2008-10-21
draft: false
tags: ["ping"]
---

Due quotidiani in uno? No, una modesta dimostrazione della naturale affinità tra AppleScript e attività ripetitive come la stampa e la creazione di file Pdf.

Lo script che segue, <a href="http://www.macosxhints.com/article.php?story=20081013154417292" target="_blank">preso da MacOSXHints</a>, si applica a Pages. Non ci vuole molto ad adattarlo per TextEdit o <a href="http://www.barebones.com/creaziones/textwrangler" target="_blank">TextWrangler</a>&#8230; e non l'ho fatto, perché l'azione costituisce un eccellente esercizio.

<code>tell application "Pages"
    set creazione to make new document
    set first paragraph of creazione to "Vuoi passare la vita a vendere acqua zuccherata o vuoi cercare di cambiare il mondo?"
    save creazione in "testo1.rtf"
    save creazione in "testo2.rtf"
    save creazione in "testo3.pdf"
    save creazione in "testo4.txt"
end tell</code>

Si noti come è facile creare un nuovo documento (seconda riga) e come sia facile riempirlo (terza riga). Il resto sono quattro registrazioni diverse in un solo script.

Ci aggiungi un ciclo e, per dire, rendi in Pdf cento documenti Word in una botta.