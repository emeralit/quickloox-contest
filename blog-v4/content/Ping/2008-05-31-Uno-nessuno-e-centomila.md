---
title: "Uno nessuno e centomila"
date: 2008-05-31
draft: false
tags: ["ping"]
---

Ritorno un po’ stordito e un po’ felice dall’evento NeoOffice.

Lezione importante: è l’era delle cose che uno le fa e basta, senza alibi. <a href="http://freesmug.org" target="_blank">Carlo</a>, professione insegnante, padre di famiglia, ha diffuso in diretta mondiale audiovideo un evento utilizzando software a costo zero, un Mac e una videocamera qualunque. Niente studi esoterici, niente magie informatiche per pochi, niente giornate vuote che uno per impiegarle si inventa qualcosa: voglia, impegno e passione.

I due autori di <a href="http://www.neooffice.org/neojava/it/index.php" target="_blank">NeoOffice</a> fanno <em>open source</em> e vivono di donazioni ma dentro  e fuori sono professionisti: hanno parlato senza interruzione per una giornata intera. A volte in sala c’era quasi nessuno, a volte c’erano quasi tutti, però sapevano di essere ripresi e che, anche con una sola persona davanti, ce n’erano a mucchi dietro ai browser. Così hanno mantenuto entusiasmo ed energia fino a che veramente tutte le curiosità e tutte le domande non sono state soddisfatte.

<a href="http://atworkgroup.net" target="_blank">@Work</a> ieri è stato un ritrovo con una sala capiente e ospitale, che ha dato modo a persone da tutto il mondo di incontrarsi e condividere vita lavoro ed esperienze, senza che nessuno dovesse spendere un soldo. Se il prezzo per poter fare questo è vendere le borse per i portatili, firmo subito. Anzi, rifirmo, perché l’ho già fatto anni fa condividendo un sogno insieme ad amici.

Alla fine pizzata per tutti, membri del Poc, geni del software, rappresentanti autorevoli del <a href="http://www.plio.it/" target="_blank">Plio</a>, <a href="http://antoniodini.nova100.ilsole24ore.com/" target="_blank">giornalisti</a>, entusiasti e amici (Carmelo: <em>favolosa</em>). Sono stato bene come poche altre volte e ho conosciuto anche <a href="http://www.emaskew.it" target="_blank">uno</a> dei frequentatori di questo blog. Sono contento come se ne avessi incontrati centomila.