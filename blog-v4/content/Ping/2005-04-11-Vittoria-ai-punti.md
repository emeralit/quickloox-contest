---
title: "Vittoria ai punti"
date: 2005-04-11
draft: false
tags: ["ping"]
---

Dinamiche psicologiche che Freud neanche le sognava

Ricevo e pubblico da Massimo:

<cite>Una famigliola in un punto vendita Pc/Mac osserva il Mac mini per il figlio adolescente, indecisi tra questo ed un nuovo Pc; discutendo di pro e contro mi immischio e partecipo alla discussione in modo molto neutrale, diciamo <em>politically correct</em>, senza partigianerie.</cite>

<cite>Il ragazzo pare molto orientato al Pc (i giochi prima di tutto…), ma poi, nel ravvedimento finale, gli scappa di dire: &ldquo;Mah! Se però prendiamo questo (il mini) gli metterò comunque una emulazione Pc&rdquo;.</cite>

<cite>Forse un segno di timore reverenziale.</cite>

<cite>Ho commentato: stai dicendo che compri la Bmw e gli metti l'emulazione della Brava (con il rispetto dovuto alla umile e capace Brava, che almeno non se la tira come la Bmw!)</cite>

<cite>Non so se questa storiella (vera!) può insegnare qualcosa, comunque potrebbe essere uno spunto per il marketing Apple e per i suoi distributori.</cite>

Più che altro, per gli scettici.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>