---
title: "Curve non pericolose"
date: 2005-11-11
draft: false
tags: ["ping"]
---

Un particolare atteggiamento da frequentatore dei forum

I problemi accadono e ogni tanto anche i Mac hanno problemi. È interessante l'atteggiamento di chi ha un problema e decide di parlarne su un forum.

È l'applicazione dei principio mal comune mezzo gaudio. Sapere che altri hanno lo stesso problema, a parte la possibilità che qualcuno possieda la soluzione, se non altro consola.

Ma non finisce qui. Si instaura un senso di appartenenza elitaria e, contemporaneamente, l'idea che il problema abbia dimensioni maggiori di quelle visibili sul forum (c'è una contraddizione, ma fa niente).

Si pensi a uno stadio pieno di gente con un Mac. Cinquantamila, ottantamila, centomila. In una curva ci sono un centinaio di spettatori che fanno molto rumore e sostengono che, in fondo, nello stadio ci debbano essere tantissimi come loro.

Che però sono a godersi la partita ed evidentemente, quel problema, non ce l'hanno.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>