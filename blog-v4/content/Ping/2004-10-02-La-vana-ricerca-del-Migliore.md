---
title: "La vana ricerca del Migliore"
date: 2004-10-02
draft: false
tags: ["ping"]
---

Nessun parere qualificato può sostituire una sana autoanalisi dei propri desideri

Non lo so.
Non so quale sia il miglior router per l'Adsl. Se ce ne fosse uno penso che tutti userebbero solo quello. E migliore su che cosa? Sul prezzo? Sul numero di funzioni? Sulla qualità dei componenti? Sul colore dei led?

Se esistesse l'auto Migliore, guideremmo tutti quella. Ci affolleremmo tutti intorno alla donna Migliore, o all'uomo Migliore.

Qual è il Migliore word processor? Word? Sicuro? Come gestisce le espressioni regolari? È quello che occupa meno spazio su disco? E a virus come stiamo? Prezzo? Lo fa una società onesta o una condannata in tribunale per concorrenza sleale?

Non esiste il Migliore. Esiste il migliore. Per me, per te, per ognuno di noi. Ma è relativo e quello che va bene a me probabilmente non andrà bene a te.

Oltretutto nessuno ha provato tutto. Tutte le stampanti, tutti gli scanner, tutti i giochi. Nessuno ha provato cento esemplari di uno stesso hard disk e ognuno pensa che, se ha avuto un guasto, tutti avranno quel guasto. Poi guidi in autostrada e vedi uno con una gomma a terra, ma stranamente non pensi che tutte le gomme di quella marca siano bucate.

E più uno non ne sa niente, più cerca il Migliore. L'ha detta giusta un giornalista in tivù qualche giorno fa: invece che prendere un'idea e adattarla ai fatti, prendere i fatti e da quelli crearsi le idee. Guarda i fatti, documentati, e poi trai le conclusioni. Le tue, perché sei tu che userai il tuo mouse Bluetooth, mica chissà chi.

Mac è migliore di Windows. Quale Mac è il Migliore? Nessuno, Ce ne sono tanti, ognuno con vantaggi e svantaggi.

Quello che ti serve non è il Migliore. È quello che serve a te. È difficile, lo so. La libertà di scelta ha un costo, anche se si parla di chiavette Usb.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>