---
title: "Il mezzo, il fine e la giustificazione"
date: 2003-08-04
draft: false
tags: ["ping"]
---

Come si distingue un prossimo compratore di Macintosh da uno di Windows

Il test è semplice: si chiede alla cavia perché comprerà il computer.
Se considera il computer come un fine (“voglio una scheda audio a 256 bit”) comprerà Windows. Se considera il computer come un mezzo per arrivare a qualcosa (“Ho bisogno di più spazio per riversare i miei film”) comprerà un Macintosh.

Se comincia a parlare della mappa delle partizioni userà Linux, ma è un’altra storia.

<link>Lucio Bragagnolo</link>lux@mac.com