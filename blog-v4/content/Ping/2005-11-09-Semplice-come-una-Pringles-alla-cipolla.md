---
title: "Semplice come le Pringles alla cipolla"
date: 2005-11-09
draft: false
tags: ["ping"]
---

C'è davvero bisogno di spiegare le differenze di prezzo sui Mac?

Ogni tanto un bello spirito grida allo scandalo perché, cambio alla mano, un Mac comprato in Italia ha un prezzo superiore a un Mac comprato in America.

Una bella truffa, vero?

Adesso, leggiti questo <a href="http://www.wittgenstein.it/post/20051104_47347.html">intervento</a> del bravo Luca Sofri. E rifletti. Sgranocchiando una Pringles, se aiuta.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>