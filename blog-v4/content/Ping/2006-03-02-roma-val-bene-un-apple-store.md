---
title: "Roma val bene un Apple Store"
date: 2006-03-02
draft: false
tags: ["ping"]
---

Da un thread sulla lista MacLovers riporto qui una domanda a cui mi sono dato diverse risposte ipotetiche ma che resta argomento intrigante. <strong>Perch&eacute; Apple apre un Apple Store a Roma?</strong>

Ci sono città europee con un bacino di utenza assai più grande, come Parigi. Città ben più ricche, come Berlino. Città più attive culturalmente e socialmente, come Madrid.

Contemporaneamente esiste un Apple Store in una città come Edimburgo, che con tutto il rispetto, confrontata con Vienna o Praga, è un buco fuori mano.

Quindi il criterio di apertura di nuovi Apple Store in Europa è diverso da quelli immediati, o più complesso. Qual è?