---
title: "Chi siamo e dove andiamo"
date: 2010-01-01
draft: false
tags: ["ping"]
---

Chissà quando apparirà questo <i>post</i>; io lo scrivo per il primo giorno degli anni Dieci del XXI secolo.

In cui è bello ricordarsi <a href="http://www.techradar.com/news/internet/getting-connected-a-history-of-modems-657479" target="_blank">come erano i primi modem</a>, quando oggi nella maggior parte delle case entrano megabit come se piovesse.