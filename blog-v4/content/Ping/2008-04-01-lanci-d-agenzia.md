---
title: "Lanci d'agenzia"
date: 2008-03-29
draft: false
tags: ["ping"]
---

Svelati i piani del pensionato Bill Gates dopo aver lasciato la sua creatura Microsoft; entrerà nel consiglio d'amministrazione di Apple.

Anche Oracle di Larry Ellison, l'(ex?) amico di Jobs, si lancia nel settore degli smartphone per le utenze aziendali. Assieme a Nokia sarebbero già pronti palmari ottimizzati con una versione lite del loro database.

Google sfida Steve Jobs a tutto campo ed annuncia che dopo i GPhone verranno lanciati entro l'anno i Gnotebook, portatili con software e sistema operativo generato da Google.

<em>&#8212; Cristiano</em>