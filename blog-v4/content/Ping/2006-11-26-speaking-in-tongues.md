---
title: "Speaking in tongues"
date: 2006-11-26
draft: false
tags: ["ping"]
---

Sono già al secondo straniero sconosciuto che si manifesta in Icq e chiede consigli sull'italiano scritto perché ha un esame a breve.

Spiegare la nostra grammatica in inglese è, per me almeno, tutt'altro che semplice. Ma è appassionante. C'è ancora speranza. :-)