---
title: "Un consiglio da amico"
date: 2002-04-23
draft: false
tags: ["ping"]
---

La politica dell’upgrade può funzionare con il software, ma è sconsigliabile per l’hardware

Gli upgrade del processore? Solo quando abbiamo una macchina veramente vecchia cui vogliamo prolungare la vita.
Inseguire il sogno di trasformare la macchina di sei mesi fa nella macchina che uscirà tra sei mesi è inutile, rischioso e costoso.
Le ragioni di questo invito sono strettamente tecniche, ma spiegarle tutte richiederebbe ben altro spazio di questo. Mi limiterò a chiosare che, personalmente, mi fido molto più del testing di Apple che del testing dei fabbricanti di upgrade del processore: schede che il sistema su cui verranno montate non è nato esplicitamente per supportare.

<link>Lucio Bragagnolo</link>lux@mac.com