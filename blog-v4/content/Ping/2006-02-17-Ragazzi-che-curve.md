---
title: "Ragazzi che curve"
date: 2006-02-17
draft: false
tags: ["ping"]
---

La raccolta dati sulle batterie dei portatili ha superato quota cinquanta. Comincia a esserci la massa critica per elaborare le informazioni in modo vagamente intelligente.

L'idea è arrivare a una curva che descriva con precisione ragionevole il rapporto tra età, cicli di carica e capacità residua della batteria. Non ripubblico ora la tabella aggiornata per mantenere la leggibilità del blog, ma lo farò lunedì. Nel frattempo chi vuole può comunque inviare i suoi dati.

&Egrave; già detto nei post precedenti, ma mi raccomando ulteriormente di usare <a href="http://www.coconut-flavour.com/coconutbattery/" target="_blank">Coconut Battery</a> con Tiger e <a href="http://www.branox.com/Battorox/Battorox.html" target="_blank">Battorox</a> per Panther e altre versioni di Mac OS X.

Un grosso grazie a tutti quelli che hanno partecipato e a quanti lo faranno. :-)