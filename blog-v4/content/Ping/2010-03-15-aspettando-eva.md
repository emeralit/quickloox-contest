---
title: "Aspettando Eva"
date: 2010-03-15
draft: false
tags: ["ping"]
---

Potrebbe essere semplicemente l'attesa di una nuova versione. Al momento tutto lascia intendere che Adamo Xps di Dell sia stato <a href="http://www.electronista.com/articles/10/03/08/adamo.xps.cannot.be.bought.anywhere/" target="_self">messo fuori catalogo</a>.

Adamo Xps era uscito a dicembre ed era stato acclamato come Air killer, un vero ultrapiatto dotato di interfacce e porte in quantità, con <i>optional</i> come un lettore Blu-ray. Insomma, una bella lista di specifiche.

Se davvero è uscito di produzione due mesi e mezzo dopo la disponibilità, ecco una bella prova di quanto valgono le liste di specifiche.

Magari da un costola di Adamo Xps nascerà qualcosa di meglio quanto a <i>design</i>. Conoscendo la competenza di Dell in materia, appare difficile.