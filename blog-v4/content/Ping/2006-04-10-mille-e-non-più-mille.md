---
title: "Mille e non più mille"
date: 2006-04-10
draft: false
tags: ["ping"]
---

&Egrave; un Mac PowerPc o un Mac Intel? Non è vero che basta guardarlo. Intanto non tutte le angolazioni sono buone e poi esiste il controllo remoto, quindi il Mac potrei anche non vederlo.

Soluzione: guardare <a href="http://docs.info.apple.com/article.html?artnum=303315" target="_blank">il numero di build di Mac OS X</a>. Se è minore di mille, PowerPc. Se è maggiore di mille, Intel.

Qualcuno dirà che non si può sempre aprire il menu Apple di un Mac remoto. Vero, ma si può sempre dare da Terminale il comando <code>sw_vers</code>. Già che ci sono, <code>uname -a</code> svela un po' di informazioni sulla versione di Darwin.