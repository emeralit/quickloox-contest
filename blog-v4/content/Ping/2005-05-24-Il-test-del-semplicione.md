---
title: "Il test del semplicione"
date: 2005-05-24
draft: false
tags: ["ping"]
---

Come riconoscere i media di contenuto da quelli di chiacchiere

Consulta un sito, una rivista, un quotidiano, quello che vuoi.

Se anche solo solleva il dubbio che Apple possa sostituire i processori di Macintosh attuali con processori Intel, i casi sono due: o non ha capito che cosa ha letto, o non sa che cosa sta scrivendo.

In ambedue i casi si può apporre il timbro <em>semplicione</em> e passare a fonti di informazione più affidabili.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>