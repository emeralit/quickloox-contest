---
title: "Niente di speciale"
date: 2009-07-02
draft: false
tags: ["ping"]
---

La <b>beta</b> di Snow Leopard fa venire voglia di installarla sul disco principale. È più veloce, è stabile, dovunque si clicchi salta fuori una miglioria, una raffinatezza, una cosetta da niente che insieme a tante altre diventa un bel lavoro (per esempio è cosa già nota che, se si apre il menu grafico di ora e data, Snow Leopard mostra giorno e ora corrente, cosa che Leopard non fa).

Manca però il fuoco d'artificio, quello che fa vendere, il titolo di copertina.

Finalmente, quando mi chiedono che cosa c'è di veramente nuovo nel nuovo sistema operativo, come se si dovesse per forza giustificare l'acquisto con una motivazione pedestre, posso rispondere con serenità <cite>niente, assolutamente niente</cite>.

E la sera, prima di prendere sonno, penso segretamente che forse una nuova versione cos&#236; soddisfacente di Mac OS X non si era ancora vista.