---
title: "Programmi inutili per le vacanze"
date: 2005-08-09
draft: false
tags: ["ping"]
---

Per quando il periodo ispira tutto, tranne che il lavoro

Che programmi si vogliono fare in agosto? O uno pensa alle vacanze o pensa alle vacanze. Fatte, da fare o in corso. Qualche piccolo <em>divertissement</em>, allora, ci sta anche bene.

Quello di oggi è <a href="http://home.comcast.net/%7Ejeff.ulicny/software/apps/SimR2D2.zip">Sim R2D2</a>. Un utilissimo e indispensabile simulatore di droide, che emette con frequenza casuale i suoni tipici del simpatico robottino della saga di Guerre Stellari (quella vecchia prima di tutto).

L'autore specifica: serve a infastidire i colleghi. E che cosa volere di più in una indolente giornata di agosto?

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>