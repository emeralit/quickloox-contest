---
title: "Regalo non a sorpresa"
date: 2009-08-19
draft: false
tags: ["ping"]
---

Anche se l'opzione è gratuita, suggerisco di non richiedere la confezione regalo nel caso di ordine a <a href="http://www.ibs.it" target="_blank">Internet Bookshop Italia</a>.

Il libro &#8211; nel caso - non arriva già incartato, bens&#236; insieme alla carta. Ancora comprensibile.

Ma il biglietto regalo, oltre che essere un po' banale, contiene anche il dettaglio dell'ordine. Insomma, un rischio in più che capitando in mani sbagliate al momento sbagliato rovini la sorpresa.

Ah, se sul sito c'è scritto <i>disponibilità in 24 ore</i>, nel mio caso ha significato <i>arriva in ottanta</i>.