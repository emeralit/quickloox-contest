---
title: "Leggenda commerciale"
date: 2003-07-08
draft: false
tags: ["ping"]
---

“I Mac costano di più”. Certo, e Jim Morrison è ancora vivo

Una delle leggende urbane più dure da eradicare è che Mac costi più di Windows.

Tanto per cominciare nessuno fa i conti come dovrebbe, su una distanza di tre o cinque anni, mettendo insieme fermi macchina, espansioni, software e tempo di apprendimento (però appena Apple abbassa i listini strillano come aquile perché “l’acquisto si svaluta”. Come fossero marenghi svizzeri).

Secondo, i confronti si fanno tra macchine di marca, non con i cassoni degli assemblatori. Chi ruba il software spende meno di chi lo compra e un comò preso al mercato delle pulci costa meno di quello preso dal mobiliere, ma questi non sono confronti seri.

Se ne stanno accorgendo annche in Usa dove <link>Oscast</link>http://www.oscast.com/stories/storyReader$340, sito non certo sospettabile di simpatie per la Mela, ha preso i portatili Windows più economici (Dell) e li ha confrontati con i portatili Apple, a parità di prestazioni.

Sorpresa: i portatili Apple costano meno. E certa gente, più che parlare, ripete la lezione che gli ha messo in testa qualcun altro, ugualmente incompetente ma più furbo.

<link>Lucio Bragagnolo</link>lux@mac.com