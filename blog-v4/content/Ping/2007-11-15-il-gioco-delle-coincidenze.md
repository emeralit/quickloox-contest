---
title: "Il gioco delle coincidenze"
date: 2007-11-15
draft: false
tags: ["ping"]
---

Esce Leopard e arriva una notizia su Windows 7, previsto per il 2010.

iPhone debutta in Europa e si viene a sapere che comincerà una tournèe mondiale del Surface Computer, il tavolo <em>multitouch</em> di Microsoft. Domani? No, in primavera.

Era più bello quando la concorrenza si faceva prodotto contro prodotto. Ora si fa prodotto contro promesse e a Redmond, a due passi dall'oceano, abbondano i marinai.