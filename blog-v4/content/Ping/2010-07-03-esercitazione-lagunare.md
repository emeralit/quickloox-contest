---
title: "Esercitazione lagunare"
date: 2010-07-03
draft: false
tags: ["ping"]
---

iPad ha ricevuto il battesimo dei fuoco con la mia partecipazione a Venezia Camp.

Due ore e mezzo di treno all'andata e altrettante al ritorno, più un pomeriggio di eventi, interventi, chiacchiere, note, Internet e navigazione varia.

Sono partito solo ed esclusivamente con iPad. La batteria è bastata ampiamente e oltre mezzanotte, rientrato a casa, ce n'era ancora in abbondanza. Durante il ritorno ho persino buttato giù l'inizio di un articolo con Pages e chiaramente non è la stessa cosa che avere una tastiera vera, ma la situazione è accettabile e soprattutto la tastiera virtuale non disabitua le dita una volta tornati su Mac.

Una custodia come <a href="http://www.macally-europe.com/productpage.php?product=1926" target="_blank">Bookstand di Macally</a> è una mano santa al momento della digitazione, inclinando iPad di quel tanto che basta. Avrei preso quella Apple, ma nei tempi e negli spazi che avevo è risultata introvabile.

Soddisfazione a mille e <a href="http://www.veneziacamp.it/" target="_blank">Venezia Camp</a> è stata pure una bella esperienza.

Ah, avendo un iPhone vecchio stile, in un paio di occasioni ho usato iPad anche per orientarmi in mezzo alle calli e girare con Maps in mano certamente è impegnativo. È altrettanto certamente efficientissimo e mi sono colati addosso gli sguardi di qualche giapponese alle prese con la cartina cartacea.