---
title: "Quanto è bello a 56k"
date: 2004-01-19
draft: false
tags: ["ping"]
---

In tempi di fibra è giusto ricordare anche la tecnologia povera

Sto aspettando l’Adsl e, nel frattempo, vivo e mi collego a 56K. Non è questo gran problema, perché tutto sommato do a Internet più o meno quello che prendo da Internet, ma certo uno deve fare i conti con la banda passante.

Eppure quest’anno, per la prima volta, sono riuscito a guardare a 56K l’intero keynote di Steve Jobs al Macworld di San Francisco. Cosa che gli anni scorsi, con una Isdn al massimo della potenza, non mi era mai riuscita.

La finestra era un francobollo, naturalmente, e i fotogrammi al secondo erano una quindicina. Ma ho visto tutto con chiarezza senza perdere neanche un secondo di audio, dall’inizio alla fine, con la più economica delle connessioni free.

Merito di Apple? O di Akamai, che lavora per ottimizzare la ricezione dei dati inviati da Apple su Internet? Non lo so. Però ho guardato con affetto la mia vecchia base AirPort e il suo modemino 56K incorporato.

<link>Lucio Bragagnolo</link>lux@mac.com