---
title: "Impariamo da Maya"
date: 2003-02-15
draft: false
tags: ["ping"]
---

Risolto il problema tra Mac OS X 10.2.3 e Maya 4.5 Learning Edition

Il mio carissimo amico Stefano mi informa che ora il Maya in oggetto funziona tranquillamente con Mac OS X.

Complimenti ad Alias/Wavefront che lo produce e ad Apple che ha fornito evidentemente tutta la collaborazione che serviva.

Ora è uscito Mac OS X 10.2.4 e. ottimisticamente, sono convinto che abbiano già pensato pure a quello. Incrocio le dita e il cavetto del mouse.

<link>Lucio Bragagnolo</link>lux@mac.com