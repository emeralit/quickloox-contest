---
title: "Chi ha bisogno di Office?"
date: 2004-09-15
draft: false
tags: ["ping"]
---

La differenza tra un mastodonte utile per le lettere condominiali e un programmino professionale per lavorare seriamente

Quando racconto che vivo di scrittura mi chiedono invariabilmente come mi trovo con Office. Si sorprendono sempre un po' quando gli racconto che non lo uso da oltre dieci anni.

Prendiamo il caso di questo rissoso, irascibile, carissimo nuovo sito di Macworld Italia. Questo pezzullo viene scritto, come gli oltre cinquecento che lo hanno preceduto, con <a href="http://www.tex-edit.com">Tex-Edit Plus</a>. Lo sottopongo a controllo ortografico con <a href=" http://www.eg.bucknell.edu/~excalibr/excalibur.html">Excalibur</a>, gratuito ed eccellente (il programma usa il correttore di sistema, ma Excalibur lo uso da anni ed è ormai personalizzatissimo). Poi, siccome certe parti (ma non tutte!) del pezzo vanno accatiemmellizzate, clicco su un paio di pulsanti che attivano alcuni brevi ma preziosi AppleScript adibiti ad automatizzare le operazioni noiose. Vengono inseriti automaticamente i tag dove ci vogliono (non scrivo nemmeno una virgola di Html) e poi un ultimo clic aggiorna automaticamente il sito.

Qualcuno si ingegnerà a dimostrarmi che si possono automatizzare le stesse operazioni con Word. Vero, ma Tex-Edit Plus costa quindici dollari (e alla bisogna produce file Rtf). Office è per chi ama spendere.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>