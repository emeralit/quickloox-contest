---
title: "Le buone e le cattive"
date: 2006-01-27
draft: false
tags: ["ping"]
---

Le buone: questo modesto blogghino ha trovato uno spazietto sul Venerdì di Repubblica (grazie a <strong>Mario</strong> per la dritta!).

Le cattive: credono che sia il blog di Donna Letizia che si è comprata un Mac.

Ma va bene così e un grosso grazie a loro, ma soprattutto a tutti gli ospiti graditi che ne fanno un luogo interessante (spero) e divertente (merito di tutti).