---
title: "Life-is-(comic)-life"
date: 2006-01-16
draft: false
tags: [ping]
---

Alla fine della presentazione alla stampa dei nuovi Mac e delle nuove suite iLife e iWork abbiamo potuto toccare con mano le macchine, un MacBook Pro e un iMac Core Duo.

L’impressione è di una notevole velocità. I programmi si avviano come razzi e iPhoto 06, con dentro oltre seimila foto, scorreva con rapidità eccellente.

Una curiosità: in ambedue le macchine c’era una copia di Comic Life, programma per produrre fumetti. Ho chiesto ai dimostratori americani, ma non è ancora stato deciso che cosa sarà presente nelle macchine fornite all’Italia.

Il che conferma una volta i più le mie impressioni iniziali: inutile cercare di spremere informazioni da modelli totalmente provvisori, le cui caratteristiche potrebbero variare una volta veramente in vendita.

Comunque sia, va un bel grazie a Tiziana Scanu di Apple e a tutto il reparto relazioni pubbliche per avere organizzato a tempo di record una presentazione interessante, in molti momenti, anche per chi aveva già visto il keynote. Non è poco.
