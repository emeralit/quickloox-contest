---
title: "Carbon o Cocoa?"
date: 2005-03-29
draft: false
tags: ["ping"]
---

Un test facile facile per togliersi uno sfizio

C'è chi non riesce a vivere senza sapere se un programma per Mac OS X è realizzato con le interfacce di programmazione Carbon oppure con Cocoa.

Di test ce ne sono numerosi, ma uno è forse più semplice di altri. Si prova a registrare un file e si prova a mettere nel nome del file uno slash.

Se il programma fa <em>beep</em> è Carbon; se accetta lo slash, oppure lo cambia in un trattino, è Cocoa.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>