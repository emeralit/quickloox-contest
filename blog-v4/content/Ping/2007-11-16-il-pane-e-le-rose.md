---
title: "Il pane e le rose"
date: 2007-11-16
draft: false
tags: ["ping"]
---

Sono cose che si sanno, eppure constatarle nella pratica di tutti i giorni solleva: Spotlight, su Leopard, ha indicizzato e &#8220;vede&#8221; certi file dentro certe immagini disco che ho nel computer e mi aiutano nel lavoro di tutti i giorni.

Su una nota più leggera, invece, World of Warcraft funziona su Leopard senza il minimo problema. Mi spingerei a dire che forse OpenGl funziona persino un po' meglio, ma potrebbe essere semplicemente che il server di gioco ieri sera era benevolo.