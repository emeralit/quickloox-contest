---
title: "Kill Bill Switch"
date: 2009-09-27
draft: false
tags: ["ping"]
---

Dovesse capitare che un'applicazione venisse cancellata da Windows Marketplace for Mobile, nella maggior parte dei casi gli utilizzatore dell'applicazione continuerebbero a usarla sui loro <i>smartphone</i> Windows Mobile.

Nel caso improbabile che su Windows Marketplace for Mobile si scoprisse software nocivo o pericoloso, <a href="http://arstechnica.com/microsoft/news/2009/09/windows-marketplace-for-mobile-kill-switch-details-clarified.ars" target="_blank">Microsoft potrebbe cancellarlo remotamente dagli smartphone di chi lo ha scaricato</a>.

Quando lo si è saputo di iPhone è venuto giù il mondo. Vediamo come va con Microsoft.