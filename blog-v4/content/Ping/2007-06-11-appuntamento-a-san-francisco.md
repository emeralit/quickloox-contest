---
title: "Appuntamento a San Francisco"
date: 2007-06-11
draft: false
tags: ["ping"]
---

Luned&#236; 11 giugno 2007, apertura della Wwdc e presentazione di Leopard. Il keynote inizia alle 10 ora di San Francisco, 19 italiane. Direi che alle 18:30 si apre una stanza iChat di nome <code>wwdc07</code> e si chiacchiera tra chi ha voglia, per dirla alla Ligabue, finché ce n'è.

Per entrare, Comando-Maiuscole-G e digitiamo il nome della stanza, <code>wwdc07</code>.

Posterò il postabile anche qui, intanto che chiacchieriamo. E vediamo che cosa salta fuori. :-)