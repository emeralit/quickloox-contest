---
title: "Bastano due cavi"
date: 2003-06-10
draft: false
tags: ["ping"]
---

Le nuove tavolette, no, tavolozze, che non hanno bisogno di un manuale

Sconcerta un po’, quando si apre lo scatolone, notare la qualsivoglia assenza di manuale cartaceo per la tavoletta grafica Cintiq 18SX Wacom.

Che poi è una tavolozza, no, una tela, perché misura ben 18 pollici.

In ogni caso, l’appoggio - robustissimo e molto bene ingegnerizzato - può stare in piedi in un modo solo. Lo schermo si inserisce nell’appoggio in modo elementare, che anche un bimbo capirebbe. Ci sono tutti gli adattatori possibili per arrivare a tutte le uscite video possibili, comprese le comuni Vga e le porte digitali Dvi esclusive di Apple. Si intuisce che il cavo Usb va inserito, proprio in quanto tale. E, dei due Cd, uno contiene Painter Classic e l’altro il software Wacom, per cui si capisce subito da dove cominciare.

Un trucchetto per chi utilizza solo Mac OS X. Il programma installatore, per non sbagliare, parte comunque in Classic. Ma basta fare doppio clic sulla cartella di installazione presente nel disco per notare che ci sono un installatore Classic e quello per Mac OS X (con estensione .mkpg). Un doppio clic diretto su quello e non c’è bisogno di fare partire Classic per installare il software Wacom.

In ogni caso, anche senza il manuale tutto è chiaro. E dentro il CD c’è un bel Pdf in italiano con tutto quello che il peggiore degli scettici può desiderare. Brave, le tavolozze del XXI secolo.

<link>Lucio Bragagnolo</link>lux@mac.com