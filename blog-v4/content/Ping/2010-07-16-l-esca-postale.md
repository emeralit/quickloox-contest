---
title: "L'esca postale"
date: 2010-07-16
draft: false
tags: ["ping"]
---

Suggerimento inusuale per tentare di rientrare in possesso del proprio computer rubato.

Gmail, la posta di Google, riepiloga e mostra gli ultimi dieci accessi effettuati a un account, con tanto di indirizzo Ip. Provare per credere, con un clic sul link Dettagli appena sotto l'indicazione dello spazio utilizzato, in coda all'elenco dei messaggi.

Se il malfattore si collega a un indirizzo Gmail del legittimo proprietario, magari nel tentativo di scoprire qualche segreto interessante, si può vederne il numero Ip. Dato in pasto alla polizia postale a seguito di una regolare denuncia, non è impossibile che possano almeno tentare qualcosa e magari recuperare il computer.

Anche se opzioni aggiuntive, come Torna al mio Mac, o programmi appositi come <a href="http://www.mark-up.com/store/MacBook-Call-Home/" target="_blank">Mac Calls Home</a>, non guastano.