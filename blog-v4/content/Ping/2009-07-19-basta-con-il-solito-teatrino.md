---
title: "Basta con il solito teatrino"
date: 2009-07-19
draft: false
tags: ["ping"]
---

Tradurre una novità di Snow Leopard al giorno, per sopperire alla pigrizia di Apple non è impegnativo: è illuminante. Ti viene da pensare a quante novità neanche si vedono, sotto il cofano, e quanto sono cos&#236; piccole da non parlarne, ma ci sono ugualmente.

Un sistema operativo è diventato oggi una meraviglia della tecnologia, anche indipendentemente da che lo faccia Apple; un oggetto incredibilmente complesso e sfaccettato, che richiede una quantità incredibile di anni uomo.

Ma si parlava delle novità di Snow Leopard.

<b>iChat Theater ad alta risoluzione</b>

iChat ora può mostrare iChat Theater a risoluzione superiore, fino a 640 x 480 pixel, o quattro volte la risoluzione massima su Leopard.

Diminuiscono i pretesti per creare brutti contenuti.