---
title: "De video iPodibus"
date: 2005-11-10
draft: false
tags: ["ping"]
---

Precisazioni, dati e una sorpresa, anzi due, da chi ne sa (più di me)

Mi scrive l'amico <strong>Alex</strong> a proposito delle mie intemerate su iPod e video:

<cite>320x240, come definizione è paragonabile a VIDEO CD (NTSC, però, il PAL è un po' meglio e dovrebbe essere 320x288, ma vabbè…).</cite>

<cite>Per stabilità di immagine però H.264 uccide a mani basse VIDEO CD, ponendosi a metà strada tra questo e un DVD di buona qualità.</cite>

Uno si chiederà chi sia Alex. È l'autore di Skarr, un lungometraggio prodotto con l'uso esclusivo di Macintosh. Ed è al lavoro sul progetto successivo. Ho l'onore di dare in anteprima assoluta i link al trailer, in <a href="http://www.deltaframe.com/mediacenter/media.asp?id=49&pwd=zerozero">H.264 ad alta definizione</a> e, guarda un po', a <a href="http://www.deltaframe.com/mediacenter/media/zerosigma/teaser_trailer_43.m4v.zip">risoluzione iPod</a>.

Complimenti ad Alex e buona visione a tutti.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>