---
title: "Compagni di merenda"
date: 2005-10-02
draft: false
tags: ["ping"]
---

Ogni tanto ci si lascia prendere un po' la mano

Scrive <strong>Alessandro</strong>:

<cite>L'altro giorno da un cliente ho visto un esempio di tecnologia informatica applicato alla merenda: nella saletta di ricreazione c'erano macchinette del caffè e distributori di patatine e merendine collegate in rete. La gettoniera solo sulla prima macchina, che pilota il credito delle altre.</cite><br>
<cite>Mi sono ammazzato dalle risate…</cite><br>
<cite>Ah, l'informatica…</cite>

In effetti ogni tanto si esagera. Anche a me è capitato, nell'altro senso, di collegare allo stesso Mac due tastiere per giocare in due. Ma sono cresciuto e da allora prediligo il gioco online. :-)

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>