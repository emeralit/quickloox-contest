---
title: "Senza parole (ma con l'orologio)"
date: 2006-03-23
draft: false
tags: ["ping"]
---

<a href="http://www.macitynet.it/macity/aA24197/index.shtml" target="_blank">Macity</a> si è accorta che Steve Jobs sta vendendo parte delle sue azioni Apple e scrive che la notizia è apparsa on line <em>qualche minuto fa</em>.

Peccato che l'indirizzo presente sul mio Ping sia consultabile da martedì 21. Non bastasse, questo <a href="http://finance.messages.yahoo.com/bbs?action=m&amp;board=4686874&amp;tid=aapl&amp;sid=4686874&amp;mid=748622" target="_blank">commento</a> è datato alla mattina americana, quindi al pomeriggio italiano, di mercoledì 22.

Arrivare in ritardo capita a tutti, ma dire le bugie… e neanche spiegano il perch&eacute; della vendita.