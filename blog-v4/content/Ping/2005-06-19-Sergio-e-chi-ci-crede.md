---
title: "Sergio e chi ci crede"
date: 2005-06-19
draft: false
tags: ["ping"]
---

Storia di una iniziativa e di chi crede in quello che dice

Sergio ha avuto - e mi ha comunicato - una grande idea: fare una colletta e ordinare un Power Macintosh sperimentale con processore Intel, quelli che Apple noleggia agli svilupppatori fino a fine 2006 per collaudare il loro codice e favorire la Transizione.

Avere anche solo una macchina disponibile significa molto, pure oltre i confini geografici, perché un Mac può essere utilizzato anche remotamente. 999 dollari, per un piccolo sviluppatore, possono essere una cifra consistente.

Lui era disposto a metterci cento euro. A me l'idea piaceva e ho impegnato cento euro. Purtroppo, sui canali contattati, non c'è stato seguito.

Allora ho parlato dell'idea di Sergio ai miei soci di <a href="http://www.macatwork.net">Mac@Work</a>. L'hanno trovata buona e hanno già ordinato la macchina, a disposizione di tutti gli sviluppatori impegnati nella transizione che troveranno comodo approfittarne.

Come minimo, in Italia ci sarà una postazione in più per gli sviluppatori (non credo ce ne siano tantissime altre). L'idea di Sergio ha portato a fatti concreti e a una situazione migliore di quella che c'era prima.

Altre idee, invece, lasciano il deserto che trovano. A me piace più quella di Sergio.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>