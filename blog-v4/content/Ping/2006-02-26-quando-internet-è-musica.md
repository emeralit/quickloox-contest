---
title: "Quando Internet è musica"
date: 2006-02-26
draft: false
tags: ["ping"]
---

Grazie a <strong>Jacopo</strong> che segnala <a href="http://www.smokinggun.com/projects/soundoftraffic/" target="_blank">Sound of Traffic</a>. Questo programma sorprendente analizza il traffico di rete e lo trasforma in musica. A ogni porta logica possiamo assegnare uno strumento diverso e ci sono diverse possibilità di personalizzazione.

Consiglio l'ascolto per almeno un minuto prima di tirare conclusioni, perch&eacute; il software introduce gli strumenti uno per volta. Il risultato finale è piuttosto piacevole e ineffabile, a suggerire quasi una dose di mistero nel flusso incessante di cambiamenti di voltaggio che chiamiamo segnali.

Quando vediamo un sito brutto si sentirà musica brutta? Spero di no; si andrebbe troppo sul morale. :-)