---
title: "E il suo contrario"
date: 2010-11-04
draft: false
tags: ["ping"]
---

Dedicato a chi si basa sulle recensioni per giudicare i prodotti. Questa è <a href="http://www.techradar.com/reviews/pc-mac/laptops-portable-pcs/laptops-and-netbooks/samsung-galaxy-tab-903545/review?artc_pg=3" target="_blank">una recensione di Galaxy Tab Samsung</a>:

<cite>Non è svelto come speravamo fosse. In effetti non è proprio svelto. Nonostante il processore da un gigahertz, ci sono problemi significativi di prestazioni che in molti casi castrano usabilità e prestazioni di Tab. I problemi sono evidenti soprattutto navigando il web. Il fare scorrere in basso il sito medio va piuttosto a scatti. La fluidità di iPad non si vede proprio e le nostre dita avevano spesso già terminato di strisciare lo schermo e lo avevano già abbandonato prima che l'apparecchio rispondesse e scorresse la pagina.</cite>

E adesso <a href="http://www.engadget.com/2010/11/01/samsung-galaxy-tab-review/" target="_blank">una recensione di Galaxy Tab Samsung</a>:

L'esperienza della navigazione è perlopiù robusta; le pagine si sono scaricate velocemente via Wi-Fi e scorrere o zoomare sulla maggior parte delle pagine è un attimo.

Ecco, quando uno parla di un prodotto in base a una recensione che ha letto, diciamo che vale la metà di uno che ne ha lette due. E un terzo di uno che ne ha lette tre.