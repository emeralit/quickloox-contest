---
title: "Altro che palmare"
date: 2004-09-18
draft: false
tags: ["ping"]
---


 Se può farlo perfino il vecchio Newton, dove sta il merito?



 Basta una scheda Pc Card apposita che funzioni e, visto che è stato scritto un driver Bluetooth per Newton, anche il primo assistente digitale personale della storia, vecchio di dieci anni, si collega tranquillamente alla Nuova Rete Wireless Di Tendenza.

 Ulteriore dimostrazione che gli odierni palmari fanno solo malinconia e sarebbe ora che qualcuno ricominciasse a pensarci in termini di innovazione.

Lucio Bragagnolo