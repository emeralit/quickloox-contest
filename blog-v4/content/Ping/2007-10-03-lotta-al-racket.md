---
title: "Lotta al racket"
date: 2007-10-03
draft: false
tags: ["ping"]
---

Se doveva esserci una riprova al fatto che Apple vende prodotti e cambia le cose, mentre gli altri vendono prodotti e basta, ecco questa storia delle suonerie.

iTunes Store offre condizioni di acquisto delle suonerie vantaggiose, per quel poco che ne so (ho abolito qualunque manifestazione sonora dal mio cellulare, che è volgare già di per sé e manca solo che si metta a suonare). Acquistare una suoneria tramite iTunes costa meno che acquistarla in altro modo.

Eppure è proprio tramite il servizio più vantaggioso che tutti ora iniziano a denunciare lo scandaloso racket delle suonerie.

Che è scandaloso. Prima, però, quando si pagavano di più, nessuno aveva niente da ridire.

Se c'è una minima idea di emancipazione dell'utenza, arriva tramite Apple, nel bene o nel male.