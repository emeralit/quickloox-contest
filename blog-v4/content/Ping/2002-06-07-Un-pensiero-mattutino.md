---
title: "Un bel pensiero"
date: 2002-06-07
draft: false
tags: ["ping"]
---

Quanti iMac produce Apple ogni giorno?

Devo dire che l’iMac (Flat Panel) che ho acquistato tempo fa è una bella soddisfazione. È una di quelle macchine che ti fa sentire speciale perché hai una macchina speciale.
D’altra parte è utile mettere le cose in prospettiva: ogni mattina, quando mi sveglio e bevo un caffè, Apple sta mettendo in produzione dai cinquemila ai diecimila iMac.
Sì, mi sento unico ma in effetti non lo sono proprio. In compenso, sono in buona e numerosa compagnia. :-)

<link>Lucio Bragagnolo</link>lux@mac.com