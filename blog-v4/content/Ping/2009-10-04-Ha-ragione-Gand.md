---
title: "Ha ragione Gand"
date: 2009-10-04
draft: false
tags: ["ping"]
---

<cite>Parlando di</cite> tutorial <cite>vi segnalo che sono disponibili tutti i video di iPhoto, iMovie, iWeb, Pages, Numbers e Keynote con i sottotitoli in inglese ed italiano al sito <a href="http://mactutorial.wikidot.com" target="_blank">http://mactutorial.wikidot.com</a>.</cite>

<cite>È disponibile anche una piccola applicazione per visualizzare in modo più comodo i</cite> tutorial<cite>. Un grande aiuto nella sua realizzazione viene proprio dal Salento tramite i preziosi consigli di Paolo Portaluri.</cite>

Questo lo commentava appunto <a href="http://freesmug.org" target="_blank">Gand</a> e, siccome merita, meglio dargli più evidenza. Oltetutto glielo avevo promesso e ogni tanto sono proprio inaffidabile. Fortuna che lui è paziente. :-)