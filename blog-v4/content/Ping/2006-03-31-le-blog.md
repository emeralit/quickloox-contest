---
title: "Le Blog"
date: 2006-03-31
draft: false
tags: ["ping"]
---

Ha aperto un blog anche <a href="http://gassee.blog.20minutes.fr/" target="_blank">Jean-Louis Gass&eacute;e</a>, il manager prodigio che ha portato Apple in Francia a sfiorare il cinquanta percento della quota di mercato e contemporaneamente l'egocentrico responsabile principale del disastro che fu Mac Portable.

Una versione o l'altra, sta di fatto che come lettura non si può proprio ignorare.