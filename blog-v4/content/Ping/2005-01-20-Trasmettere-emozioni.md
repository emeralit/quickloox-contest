---
title: "Trasmettere emozioni"
date: 2005-01-20
draft: false
tags: ["ping"]
---

Non ci sono parole da aggiungere a un commento che dice tutto

Scrive Daniele Savi:

<em>Non so come faccia Apple… davvero. Ho sempre ritenuto i player mp3 &ldquo;a chiavetta&rdquo; un'inutile spesa, principalmente perchè hanno una memoria limitata. Ho sempre pensato che la cosa migliore fosse acquistare un bell'iPod da 40 Gb, sul quale traslocare tutta la musica che ho su vari cd e hard disk.</em>

<em>Qualche giorno fa è uscito iPod shuffle… la prima chiavetta USB targata Apple. Ieri sera, l'ho ordinata da AppleStore.</em>

<em>Scegliamo gli oggetti per le emozioni che sanno trasmetterci. Apple questo lo sa fare.</em>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>