---
title: "Che rottura queste riparazioni"
date: 2006-01-08
draft: false
tags: ["ping"]
---

Da un'inchiesta malfatta possono arrivare buone conclusioni

Non abbiamo dati ufficiali di Apple sulla vita media dei Mac portatili.

Uno dei luoghi comuni tipico dell'informatica (e molto altro) è non li fanno più come una volta.

Macintouch ha realizzato una <a href="http://www.macintouch.com/reliability/laptops.html">inchiesta</a> sui guasti ai PowerBook condotta su oltre diecimila portatili dal 2001 al 2005.

Mentre non c'è motivo di dubitare dei numeri portati da Macintouch, il campione è assolutamente arbitrario e la loro suddivisione della casistica dà a dire poco il male di testa.  Due conclusioni sommarie sono che: 1) lo studio è piuttosto utile per azzardare quali siano i guasti più frequenti, ma dice praticamente niente sulla percentuale dei Mac che si guastano; 2) se invece uno prende per buoni quei dati rispetto al totale generale dei Mac venduti, ne viene fuori che il luogo comune che li facevano meglio una volta non sta in piedi.

Ambedue le conclusioni hanno risvolti positivi. Basta scegliere. :-)

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>