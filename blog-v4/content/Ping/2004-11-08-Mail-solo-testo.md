---
title: "Testualmente integralista"
date: 2004-11-08
draft: false
tags: ["ping"]
---

La posta elettronica non è un fine, è un mezzo

Oggi come oggi si pretende di saper usare un programma senza neanche aprire i menu e guardare che cosa c'è dentro, e va bene. Si fanno domande senza prima cercare la risposta su Internet (c'è, sempre) e va bene. Si saccheggiano liberamente Internet e l'open source senza mai dare niente in cambio, anzi comportandosi come se tutto fosse dovuto, e va bene.

Ma la posta elettronica con l'Html dentro è insopportabile. Fatta per gente che non ha niente da dire, ma vuole distinguersi nel dirlo.

Un breve invito a costoro: trova la pace che cerchi nella tua autoradio full surround, o nella borsetta griffata. La posta elettronica non serve a parlare di te; serve a parlare e basta.

Ma, dirai, perché Apple ha messo dentro Mail la messaggistica in Html? Beh, perché ha messo in Mac OS X la possibilità di toglierla. Apri il Terminale (con Mail chiuso) e scrivi

<code>defaults write com.apple.mail PreferPlainText -bool TRUE</code>

Sarà la pace, estetica e semantica. Per me, ovvio.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>