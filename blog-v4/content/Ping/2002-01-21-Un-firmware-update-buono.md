---
title: "Un Firmware Update che non era cattivo"
date: 2002-01-21
draft: false
tags: ["ping"]
---

Una piccola differenza di specifiche sulla Ram non è più in grado di causare il blocco o il malfunzionamento di un Mac

Si ricorderà il panico suscitato dal Firmware Update 4.1.7, che senza spiegazione disattivava interi banchi di Ram. Fortunatamente, dopo poco, era apparsa una soluzione.
L’autore di essa è Glenn Anderson, ventinovenne programmatore neozelandese, autore di Eudora Internet Mail Server e sviluppatore dietro le quinte di software vitale per la realizzazione della trilogia cinematografica de Il Signore degli Anelli.
Per chi fosse rimasto indietro, Anderson ha pubblicato un piccolo programma, Dimm First Aid, che risistemava le cose riprogrammando le Ram difettose dove necessario. Come avrebbe scritto volentieri il matematico Pierre de Fermat, avrei una bellissima spiegazione della situazione ma qui non c’è abbastanza spazio. Magari in una prossima puntata, se interessa.

lux@mac.com

<http://www.mactcp.org.nz/dimmfirstaid.html>
