---
title: "Cose da guardare"
date: 2008-08-14
draft: false
tags: ["ping"]
---

<a href="http://sourceforge.net" target="_blank">Sourceforge</a>, che ha rifatto per intero l'organizzazione del sito e secondo me ci ha guadagnato non poco. Intendendo che è più facile consultare e trovare cose belle, anche per Mac OS X.

La rubrica di Enrico su Macworld di agosto. Parlo raramente della rivista cartacea qui, ma queste due pagine da sole valgono il prezzo. Un piccolo estratto:

<cite>Quando poi l'evento arriva, Apple presenta qualcosa d'altro, che nessuno di noi aveva anticipato [&#8230;] e allora noi diciamo che gli operatori sono rimasti delusi, amareggiati [&#8230;]. Io intanto ho fatto un po' di</cite> pageviews <cite>prima, durante e dopo l'evento, porto a casa qualche soldino con la pubblicità sul sito e magari trovo pure il pollo [&#8230;]</cite>

Nel <em>Pendolo di Foucault</em> Umberto Eco spiegava mirabilmente come funziona il mercato-truffa degli <em>autori a proprie spese</em> e come esistono case editrici che fanno i soldi a spese della gente disposta a pagare pur di farsi pubblicare. Ecco, <a href="http://applemania.blogosfere.it/" target="_blank">Enrico</a> spiega altrettanto mirabilmente (più brillantemente) e con finale a sorpresa come funzionano i siti-spazzatura che pretendono di dare sedicenti notizie su Mac e come fanno i soldi a spese dei polli che li guardano.

App Store con iTunes. Se non hai un iPod touch, ti viene voglia subito.