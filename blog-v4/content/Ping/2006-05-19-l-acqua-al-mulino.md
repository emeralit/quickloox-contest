---
title: "L'acqua al mulino"
date: 2006-05-19
draft: false
tags: ["ping"]
---

Cercherò di non essere polemico e spiegare semplicemente come stanno le cose. Apple non recensisce i widget presenti nell'apposita <a href="http://www.apple.com/downloads/dashboard/" target="_blank">sottosezione del suo sito</a>. Se un programmatore invia come descrizione del proprio widget <em>questo è il miglior widget del mondo</em>, e Apple approva il widget, appare quella descrizione.