---
title: "Premio stupidità 2006"
date: 2006-06-01
draft: false
tags: ["ping"]
---

Si lamenta che la sua stampante a otto colori, pagata ben seicento euro, ha iniziato a stampare malissimo.

Viene aperta la stampante e si verifica che le cartucce colore sono state montate in posizioni scambiate, con ovvi effetti sull'uso dei colori stessi.

Quanto si può addebitare a un cliente come diritti di stupidità?