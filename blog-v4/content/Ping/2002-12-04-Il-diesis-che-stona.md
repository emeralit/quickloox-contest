---
title: "Se è diesis, stona"
date: 2002-12-04
draft: false
tags: ["ping"]
---

Una ragione importante per aggiornarsi a Mac OS X 10.2.2

Se non lo hai ancora fatto e usi il Mac in rete, fallo subito. L’aggiornamento a Mac OS X 10.2.2, intendo.

10.2.2 risolve un bug particolarmente fastidioso presente fino alla versione 10.2.1, che può provocare danni quando viene copiato via AppleShare un file il cui nome termina con “#02”. Il bug in realtà può venire attivato da qualunque nome di file che finisca con un diesis (cancelletto, box, come vuoi) e un numero esadecimale; ma “#02” attiva il bug sempre e comunque.

Si possono avere kernel panic, se va bene, altrimenti perdere dati sul server. Meglio 10.2.2.

<link>Lucio Bragagnolo</link>lux@mac.com