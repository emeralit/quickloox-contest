---
title: "Viennese con gusto"
date: 2006-05-21
draft: false
tags: ["ping"]
---

<a href="http://www.opencommunity.co.uk/vienna2.html" target="_blank">Vienna</a>, appena lanciato, mi ha avvisato che c'era un update pronto da scaricare. Era una delle cose che mi mancavano. Soddisfazione.