---
title: "Cellulare Connection"
date: 2005-08-12
draft: false
tags: ["ping"]
---

Mi ricredo, a denti stretti, su Bluetooth

Fino a ieri il rapporto quotidiano tra Mac e cellulare, nella mia vita, era zero assoluto. Odio i cellulari e non mi piace interfacciarli con i computer. So di sbagliare, ma lo vedo necessario quanto l'interfacciamento tra frigoriferi e ferri da stiro.

Ieri la pigrizia ha generato un'esigenza nuova e mi sono ritrovato in casa un cellulare Bluetooth, preso apposta perché si collegasse al Mac.

È stata una sorpresa. Computer e telefonino si sono visti al volo tramite la connessione Bluetooth. Un clic e iSync ha portato sulla rubrica del cellulare tutti i miei contatti. Due clic ed ero collegato a Internet via cellulare.

Ancora non mi piace, ma devo ammettere che è diventato maledettamente facile.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>