---
title: "Quattro per Quad uguale sedici"
date: 2005-10-26
draft: false
tags: ["ping"]
---

Non ci si rende conto di quanto video possano mettere a disposizione i nuovi Power Mac G5 Quad

Solo i fatti. Un Power Mac G5 Quad di punta può pilotare contemporaneamente quattro Cinema Display da 30&rdquo;. Nelle migliori tradizioni Apple, i quattro schermi possono formare un'unica scrivania. Da sedici milioni di pixel. Un iMac G5 orbita intorno a un milione e mezzo di pixel e a me sembra un sacco di spazio.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>