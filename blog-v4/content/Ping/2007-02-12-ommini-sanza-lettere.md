---
title: "Ommini sanza lettere"
date: 2007-02-12
draft: false
tags: ["ping"]
---

Se Steve Jobs non avesse scritto la lettera su musica digitale e sistemi Drm di protezione, nessuno avrebbe detto niente. Siccome l'ha scritta, è nato un putiferio di gente che altrimenti, senza lettera, stava zitta. Putiferio di ovvietà ed enormità.

Apple viene accusata di avere tratto vantaggio dal Drm. Certo che ne ha tratto vantaggio. Che doveva fare, trarne svantaggi? Ma non lo ha mica voluto Apple, il Drm.

Poi viene accusata di essere a favore dell'abolizione del Drm solo adesso che è in posizione di preminenza. Che doveva fare, rinunciare ad aprire iTunes Store, giusto per lasciare l'occasione ad altri? Lasciare che il Drm leader sul mercato diventasse quello Microsoft? Il Drm non lo ha mica voluto Apple.

Come al solito, la cosa giusta l'ha scritta <a href="http://morrick.wordpress.com/2007/02/08/davanti-agli-occhi/" target="_blank">Riccardo</a>, con il suo supremo dono della sintesi. La gente non vede l'ora di leggere tra le righe, quando invece&#8230;