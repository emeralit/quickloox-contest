---
title: "La strategia del web"
date: 2006-01-10
draft: false
tags: ["ping"]
---

Se si produce un software che facilita veramente al massimo la pubblicazione di pagine web, significa aumentare il numero di pagine web <em>Made on a Mac</em>. E questo ha più senso e significato di un miliardo di chiacchiere sulle quote di mercato dell'hardware.