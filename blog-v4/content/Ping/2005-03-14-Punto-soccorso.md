---
title: "Punto soccorso"
date: 2005-03-14
draft: false
tags: ["ping"]
---

L’importanza di misurare la febbre prima di andare dal medico

È quasi mezzanotte, arriva in iChat, non lo conosco. Saluta e chiede, puoi aiutarmi?

Non lo so, rispondo. Non conosco il problema.

Ho un problema con un hard disk esterno, mi dice. Mi dispiace molto, rispondo, ma non credo di poter essere di molto aiuto.

Allora mi “spiega”. In pratica riesco a copiare nel disco esterno, ma non riesco a copiare nel disco interno, secondo te cosa vuol dire?

Vuol dire che continuo a non poterti aiutare, penso io. Senza nessuna informazione concreta al massimo posso recitare un esorcismo.

Me lo immagino in ambulatorio. Dottore, sto male. Sì, mi dica, che sintomi ha? Eh, non mi sento bene…

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>