---
title: "Terremoto annunciato"
date: 2006-04-21
draft: false
tags: ["ping"]
---

Adesso che i Mac possono ridursi a fare il boot da Windows, si rischia che non escano più giochi, chiacchierano.

Intanto Aspyr ha mantenuto le promesse e fatto uscire <a href="http://www.aspyr.com/games.php/mac/quake4/" target="_blank">Quake 4 per Mac</a>, pure Universal.

Lora, più che chiacchierare, sembra che lavorino.