---
title: "Giustificazione firmata"
date: 2010-09-24
draft: false
tags: ["ping"]
---

Invado indebitamente lo spazio di Ping con le mie faccende private del <a href="http://www.poc.it" target="_blank">Poc</a>, per avvisare chi leggesse ora che non riuscirò stasera a essere presente alla pizzata come invece da programma.

Niente di grave, solo problemi organizzativi. Appuntamento alla prossima e mille scuse a tutti! 