---
title: "L'iPadrino e altre storie"
date: 2006-02-13
draft: false
tags: ["ping"]
---

<strong>Jacopo</strong> mi ha segnalato (grazie!) il <a href="http://www.mikeindustries.com/blog/archive/2006/01/ipod-winner-7" target="_blank">concorso</a> a chi creasse la migliore falsa locandina cinematografica avente come tema iPod.

La gara si è conclusa da tempo ma riguardarsi le opere è un vero piacere. Davanti all'iPodfather (Godfather nell'originale) non riesco a trattenere le risate, ma c'è tanto altro materiale interessante.