---
title: "Occhi vigili"
date: 2010-10-10
draft: false
tags: ["ping"]
---

Riferivo di Jimmy Schulz, parlamentare tedesco che ha provocato l'approvazione ufficiale di iPad al banco degli oratori da parte del Bundestag, ed ecco <b>Fabio</b> che mi linka <a href="http://www.setteb.it/lipad-nei-parlamenti-tentativi-di-modernizzazione-9160" target="_blank">le immagini di Schulz</a>. E pure l'italianissimo Guardasigilli Alfano che accende iPad a Montecitorio.

Non me fanno più passare una. :-)