---
title: "Quello che i blogger non dicono"
date: 2006-01-25
draft: false
tags: ["ping"]
---

L'amico <strong>Mario</strong> segnala <a href="http://journler.phildow.net/" target="_blank">Journler</a>, eccellente programma di registrazione organizzazione e archiviazione delle proprie note giornaliere.

No, non è un software per aprire blog. Serve per le nostre cose, quelle che diremo a qualcuno solo quando sarà il tempo, testo, audio, video, non importa.

Considerato il prezzo, non scaricarlo e darci un'occhiata è un'occasione persa. Potresti scoprire di avere molto più da dire e da dare di quello che pensavi.