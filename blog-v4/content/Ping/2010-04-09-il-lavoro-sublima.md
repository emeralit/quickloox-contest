---
title: "Il lavoro sublima"
date: 2010-04-09
draft: false
tags: ["ping"]
---

Mi scrive <b>Marco</b> e dice già tutto:

Seguo con interesse la tua battaglia di sostegno a Html5 &#8220;contro&#8221; Flash.

Ho trovato <a href="http://jilion.com/sublime/video" target="_blank">questa pagina</a> e trovo perlomeno sorprendente il risultato del lavoro di questi signori.

Di mio aggiungo che Apple elenca i più prestigiosi siti <i>iPad-ready</i> in <a href="http://www.apple.com/ipad/ready-for-ipad/" target="_blank">una pagina apposta</a>. Dare un'occhiata, prego.

Marco ha fatto bene a metterci le virgolette. Non sono affatto <i>contro</i> Flash. Se però il lavoro altrui porta a progressi superiori all'esistente, non vedo perché trascurarlo.