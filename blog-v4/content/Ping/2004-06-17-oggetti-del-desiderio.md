---
title: "Oggetti del desiderio"
date: 2004-06-17
draft: false
tags: ["ping"]
---

L’hardware è bello, il software è meglio

Mentre scrivo Apple annuncia a sorpresa AirPort Express, come sempre un’idea del tutto ovvia, solo che lo diventa sempre dopo che l’hanno pensata a Cupertino e il mondo inizia a imitarla in peggio.

Sentire novità da Apple è sempre un’ottima cosa, specie se sono interessanti e golose.

Se non dispiace a nessuno, tuttavia, preferisco essere soddisfatto dell’eccellente update di sicurezza che mette fine, in modo elegante e pulito, al buco di sicurezza emerco in queste settimane su Mac OS X.

L’aggiornamento è perfino abbondantemente documentato, anche questa una gradita novità.

Brava Apple.

<link>Lucio Bragagnolo</link>lux@mac.com