---
title: "Punti di vista"
date: 2007-01-09
draft: false
tags: ["ping"]
---

Se lo <em>stack</em> Bluetooth di Mac OS X saltasse in aria un quarto delle volte che lo fa lo <em>stack</em> Bluetooth del mio cellulare, sarei veramente insoddisfatto di Mac OS X.

Scommetto che lo stesso stack, sullo stesso cellulare, salta in aria le stesse volte a un sacco di altre persone. Eppure sembra una cosa che non dà fastidio a nessuno.