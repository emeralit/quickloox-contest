---
title: "Avventure nella Terra di Mezzo"
date: 2008-12-02
draft: false
tags: ["ping"]
---

Aggiornamento doveroso per ringraziare tutti quanti hanno dimostrato solidarietà per il mio PowerBook G4.

Egli, ahimé, non è più. Sulla scheda logica c'è tutto un grumo che nell'aspetto somiglia vagamente al fuoriuscito delle pile scariche. Si avvia, ma sta per un tempo convincente in animazione sospesa senza dare segni di <em>boot</em>.

Egli, ohibò, è risorto. Nel mio negozio ho trovato, pronto a essere venduto come usato, ce n'era uno esattamente uguale. Stesso disco, stesso processore, stessa scheda video, un giga di Ram. Ne ho rapidamente trapiantato un secondo giga dal fu PowerBook e ora ho un PowerBook che è esattamente lo stesso di prima, solo meno vissuto. La migrazione assistita dei dati ha anche depurato il sistema di un po' di sconcezze e cos&#236; sul disco c'è un po' più di spazio, e tutto va un filo meglio.

Il costo della riparazione, il mercato non mente mai, è praticamente uguale a quello dell'usato e cos&#236; mi tengo questo fino al momento in cui, continuo a sognare, Steve annuncerà il 17&#8221; <em>unibody</em>.

Informazione personale per <strong>Phil</strong>: abbiamo cambiato per sempre la marca del caffè.