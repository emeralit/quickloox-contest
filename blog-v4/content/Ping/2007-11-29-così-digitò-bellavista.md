---
title: "Così digitò Bellavista"
date: 2007-11-29
draft: false
tags: ["ping"]
---

È il titolo della mail che mi ha inviato <strong>Carmelo</strong>, dopo la Ping-pizzata 2.0, dove abbiamo anche chiacchierato di Luciano De Crescenzo e dei trascorsi lontanissimi in cui appariva su Italia 1 (quando non era ancora rete Mediaset) nella trasmissione Bit e mostrava nientemeno che un Macintosh, appena uscito o quasi.

Carmelo ha rintracciato un <a href="http://www.torinointernational.com/spot80/?carica=nonsolospot-detail&amp;id=745" target="_blank">annuncio di Gabriella Golia</a> in cui si cita la trasmissione e <a href="http://www.lucianodecrescenzo.net/de_crescenzo/uomo/uomo.html" target="_blank">la pagina del sito di De Crescenzo</a> in cui appare visibilissima la foto dell'uomo e del computer.

Con questo insignisco Carmelo del titolo di re del retrocomputing Mac per questa settimana!