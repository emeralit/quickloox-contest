---
title: "Glossy Evil Nine, 1st Impression"
date: 2006-05-25
draft: false
tags: ["ping"]
---

(ognuno ha i suoi vizi musicali)

Arrivati in Mac@Work MacBook Pro 17&rdquo; e i modelli bianco e nero di MacBook. Sul 17&rdquo; c'è poco da dire, si sa già tutto. Le prestazioni sono quelle che ci si aspetta, ampiamente superiori, e via.

I MacBook. Test di un'oretta, sulle cose essenziali, senza pretese. Prima di tutto: chi contesta la differenza di prezzo non ha idea. Il MacBook nero è <em>mooolto</em> più bello e si vede a prima vista che il materiale dello chassis è diverso rispetto al MacBook bianco.

La tastiera è positiva. Apparentemente il meccanismo di pressione dei tasti è rimasto lo stesso. Familiarizzare è questione di pochi minuti. Il tocco è buono, la risposta è giusta, il leggero rimbalzo superfluo che constato sul mio 17&rdquo; e ogni tanto mi triplica una doppia qui sembra mancare. Sempre al primo test ho l'impressione che il nuovo arrangiamento dei tasti protegga l'interno della tastiera molto più di prima. Ma non avevo briciole per fare la prova.

Lo schermo glossy. Frontalmente, nella posizione su cui si lavora, con la direzione dello sguardo perpendicolare al piano dello schermo, i riflessi sono praticamente assenti e di fatto non esiste problema. Più la direzione dello sguardo tende a mettersi in parallelo con il piano dello schermo, più c'è riflessione. Una persona che guarda lo schermo da posizione molto laterale rispetto a chi sta lavorando osserva una riflessione notevole.

I problemi dello schermo glossy per chi ci lavora vengono dalla presenza di illuminazione puntiforme alle proprie spalle. Non so se ci si abitua o se ero prevenuto, ma di fatto il riflesso c'è e a me dava fastidio. Per avere una impressione definitiva dovrei lavorarci seriamente e fare anche qualche prova <em>en plein air</em>.

Le prestazioni. L'effetto è quello del Mac mini Intel: veloce che fa impressione, filmati in alta definizione e Dvd senza problemi, l'assenza della scheda grafica in hardware non si nota. Tranne che per il 3D, naturalmente. World of Warcraft è giocabile, intorno ai 17 fotogrammi per secondo, ma niente di più.

La batteria è sorprendente. Fab, che ne ha adottato uno e ci sta lavorando da un paio di giorni, riporta tre ore tranquille di utilizzo intenso e ben oltre cinque ore di utilizzo normale, sulle sei dichiarate da Apple. Dubito fortemente che esistano Pc di pari configurazione con la stessa autonomia effettiva.

&Egrave; tutto. Se ci sono domande specifiche, nei limiti della mia presenza in Mac@<a href="http://www.macatwork.net" target="_blank">Work</a>, nel giro di qualche giorno rispondo a tutto quello che posso.