---
title: "Chat a comando"
date: 2007-05-15
draft: false
tags: ["ping"]
---

Complimenti a <strong>Pierfausto</strong>, che trova sempre spunti interessanti. Questa volta mi ha segnalato un <a href="http://www.adiumxtras.com/index.php?a=xtras&amp;xtra_id=1255" target="_blank">extra di Adium</a> che permette nientemeno di eseguire comandi di shell dal campo testo del programma. Diversamente da mille altre minchiate interessanti, riesco anche a vederne qualche utilità.