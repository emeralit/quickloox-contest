---
title: "La rivincita del floppy"
date: 2003-07-22
draft: false
tags: ["ping"]
---

Inutile, accademica ma spettacolare e divertente esercitazione di tecnologia su Mac

Chi dice che il mercato Mac è una nicchia? È abbastanza popolato perché ci sia almeno una persona a cui viene voglia di chiedersi se sia possibile realizzare una configurazione Raid basata su lettori di floppy disk.

Il risultato è <link>qua</link>http://ohlssonvox.8k.com/fdd_raid.htm. Non ha nessuno scopo pratico reale, ma attesta quanto possa essere flessibile Mac OS X.

Di passaggio: su Windows, anche se è accademia, non si fa.

<link>Lucio Bragagnolo</link>lux@mac.com
