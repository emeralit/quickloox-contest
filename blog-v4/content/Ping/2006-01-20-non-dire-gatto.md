---
title: "Non dire gatto se non l’hai nel sistema"
date: 2006-01-20
draft: false
tags: [ping]
---

Big Cat permette a tutti i programmi che supportano i menu contestuali (control-clic) di attivare un menu contestuale dal quale attivare Applescript, script di shell o altro. In pratica è un modo astuto di aggiungere comandi AppleScript ai programmi che non sono attachable.

È un trucchetto a livello di sistema operativo, quindi va applicato con prudenza e solo da persone consapevoli. Se non hai idea di che cosa significhi attachable, sei candidato a procedere con massima cautela. :-)
