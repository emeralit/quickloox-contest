---
title: "Un pensiero per la minoranza di sinistra"
date: 2005-06-02
draft: false
tags: ["ping"]
---

Appello ad Apple perché non dimentichi una parte importante della propria utenza

Sacrosanta mail da Sergio, che chiede: ma perché Apple non permette di invertire la direzione della freccina del mouse ai mancini?

È vero che esiste <a href="http://www.unsanity.com/haxies/mightymouse">Mighty Mouse</a>, ma nel primo sistema al mondo installabile anche dai non vedenti non dovrebbe essere impossibile inserire una preferenza piccola così.

Il ragionamento vale a maggior ragione guardando al passato. Quasi tutte le tastiere Apple permettono di tenere il mouse dalla parte preferita, i mouse sono universali, la società è stata la prima a mettere le trackpad al centro dei portatili. Manca una piccola attenzione e poi c'è tutto.

Esortiamo Apple a ricordarsi dei mancini mandando tanti bei <a href="http://www.apple.com/macosx/feedback/">feedback</a>. Già che ci siamo, chiediamo che VoiceOver supporti la lingua <a href="http://www.apple.com/it/macosx/feedback/">italiana</a>. Se anche Apple non avesse una coscienza, mettiamo a posto almeno la nostra.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>