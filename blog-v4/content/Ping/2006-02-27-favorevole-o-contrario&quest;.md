---
title: "Favorevole o contrario?"
date: 2006-02-27
draft: false
tags: ["ping"]
---

Non sono un grande cultore di Dashboard. Meglio: lo uso per quattro cose che reputo molto comode e niente più.

Però darò sicuramente un'occhiata ai <a href="http://www.google.com/macwidgets/" target="_blank">widget di Google</a>.