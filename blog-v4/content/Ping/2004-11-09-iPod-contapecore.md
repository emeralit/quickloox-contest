---
title: "Il beep inverso"
date: 2004-11-09
draft: false
tags: ["ping"]
---

Un uso alternativo di iPod che non avevo ancora visto, anzi, sentito

iPod ha la sveglia e, se c'è un altoparlante collegato, ti può svegliare con la musica, bellissimo, ok.

Ma qualcuno ha pensato a farne una macchina per conciliare il sonno. Ha preso  un <a href="http://www.dogstar.dantimax.dk/testwavs">campione</a> di rumore bianco, lo ha copiato e incollato in QuickTime fino a farne un file sufficientemente grande e lo &ldquo;suona&rdquo; con iPod nell'andare a dormire.

Bisogna essere svegli per pensare una cosa del genere. Ma è una contraddizione apparente.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>