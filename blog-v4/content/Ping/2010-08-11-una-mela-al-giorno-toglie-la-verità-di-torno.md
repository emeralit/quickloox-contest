---
title: "Una mela al giorno toglie la verità di torno"
date: 2010-08-11
draft: false
tags: ["ping"]
---

Giorni fa Hexus <a href="http://www.hexus.net/content/item.php?item=25790" target="_blank">ha scritto</a> di un principio di incendio all'Apple Store di Regent Street a Londra, uno dei negozi-vessillo di Apple. O meglio, hanno scritto di un incendio che <i>sembra</i> essere divampato all'Apple Store e ha determinato l'evacuazione del negozio.

A illustrazione c'è una foto sfocata, presa da lontano, che non mostra assolutamente niente.

Nel testo si fa riferimento al fatto che il governo giapponese ha chiesto ad Apple informazioni sul surriscaldamento di alcuni iPod nano venduti tra il 2005 e il 2006 &#8211; notizia vera, priva di ogni connessione con la questione &#8211; e si sottolinea che, comunque, le ragioni dell'incendio restano misteriose.

Quanto occorre perché il lettore distratto e occasionale ne esca con la vaga impressione che un iPod o, boh, qualcosa abbia preso fuoco all'Apple Store di Londra. Sai, l'ho letto su Internet, sarà vero.

La &#8220;notizia&#8221; viene &#8220;<a href="http://www.techeye.net/business/apple-store-in-london-evacuated" target="_blank">ripresa</a>&#8221; da TechEye.net. Viene commentata da tre lettori:

<cite>L'incendio era nel negozio QuickSilver. Non aveva niente a che vedere con Apple.</cite>

<cite>Lavoro sul lato opposto della strada. Il fuoco era nel negozio accanto, non nell'Apple Store.</cite>

<cite>Um&#8230; l'Apple Store non è stato evacuato! L'incendio era nel negozio Quicksilver, che è accanto, all'angolo di Hanover Street. Oltre al negozio Quicksilver è stato evacuato il palazzo al numero uno di Hanover Street, che è stato preso da Apple in affitto per i propri uffici. Non c'è stata evacuazione dell'Apple Store.</cite>

Certo. Ma il fatto è che titolare <i>incendio al negozio Quicksilver di fianco all'Apple Store</i> non&#8230; accende abbastanza le fantasie.