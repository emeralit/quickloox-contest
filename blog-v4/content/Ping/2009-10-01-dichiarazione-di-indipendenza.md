---
title: "Dichiarazione di indipendenza"
date: 2009-10-01
draft: false
tags: ["ping"]
---

Situazione curiosa: se guardo nel System Profiler di Snow Leopard vedo chiaramente indicata la velocità di rotazione del disco.

Tuttavia vale solo per il mio sistema o, meglio, altri riferiscono di non avere a disposizione il dato, sempre in Snow Leopard.

Sto cercando di capire da che cosa dipenda ma non ho ancora il tempo di affondare quanto dovuto nella documentazione tecnica.

Qual è la tua esperienza diretta?