---
title: "Il mio proposito"
date: 2006-01-01
draft: false
tags: ["ping"]
---

Guardo il Mac e penso che…

A parte gli aggiornamenti di sistema che lo richiedono o una disgraziata morte del disco rigido, voglio arrivare a fine 2006 senza mai avere bisogno di riavviare il Mac.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>