---
title: "Tutto sommato un bel Panorama"
date: 2005-02-26
draft: false
tags: ["ping"]
---

Per capire che siamo noi a doverci muovere, non Apple

Il settimanale omonimo ha iniziato non so quale raccolta di dischi ottici, compatibili sia Mac che Windows. Disgraziatamente gli ultimi due dischi si sono rivelati solo compatibili Windows.

Ci sono due scuole di pensiero. Una sostiene che Apple dovrebbe intervenire dovunque e comunque, tempestando di telefonate ogni organizzazione che commette un errore (dato che perde clienti).

L'altra dice che chi vende lo fa agli utenti, non ad Apple, e quindi ha senso se si muovono gli utenti.

In questo caso si sono mossi gli utenti, che hanno scritto in massa a Panorama chiedendo cortesemente spiegazioni. Il settimanale ha promesso che a tutti sarà mandata la versione compatibile Mac dei due dischi incriminati.

Uno a zero per la seconda scuola.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>