---
title: "Troppo serio per prendersi sul serio"
date: 2005-01-14
draft: false
tags: ["ping"]
---

Chi è il serioso traduttore che si è accanito su iPod Shuffle?

Apple fa cose talmente belle che può permettersi anche di non prendersi sul serio, come quando dice sul Web <a href="http://www.apple.com/ipodshuffle">Non mangiatevi iPod Shuffle</a>.

Ma il traduttore italiano non è evidentemente un uomo Apple: deve essere uno serio, professionale, tutto d'un pezzo, che guai a fare un pizzico di ironia, metti mai che il Capo si arrabbi e poi ti guarda male in pausa caffè. Così nella <a href="http://www.apple.com/ipodshuffle">pagina italiana</a> lo ha tolto. Chissà, dopo sarà andato a chiedere un aumento, per evitare la brutta figura che altrimenti avrebbe fatto la sua serissima azienda.

Non è un po' triste essere così seri?

Grazie a Dani per la segnalazione!

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>