---
title: "Riferimento veloce"
date: 2010-03-10
draft: false
tags: ["ping"]
---

Spettacolare la <a href="http://www.primatelabs.ca/blog/mac-benchmarks/" target="_blank">tabella di Primate Labs</a> che mette in fila le prestazioni di tutti i Mac da Power Mac G3 in su fino al Mac Pro (Early 2009), &#8220;solo&#8221; settantacinque volte e mezzo più veloce.

Risponde a tutti i dubbi riguardanti le prestazioni, in un attimo.

Conferma anche quanto scrivevo in giro giorni fa: cercare oggi i sedici gigabyte di Ram sui portatili in nome delle prestazioni è assurdo. Il mio MacBook Pro 17&#8221; (Early 2009) può stare dietro a un iMac, ma mangia la polvere se confrontato con un Mac Pro. E se avesse sedici gigabyte anziché otto non cambierebbe una virgola.