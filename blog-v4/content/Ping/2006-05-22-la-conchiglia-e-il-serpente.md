---
title: "La conchiglia e il serpente"
date: 2006-05-22
draft: false
tags: ["ping"]
---

Non chiedermi perch&eacute;, ma ho installato <a href="http://ipython.scipy.org/" target="_blank">IPython</a> sul PowerBook.

<em>Ho installato</em> è dirla grossa. Ho chiesto a <a href="http://fink.sourceforge.net/" target="_blank">Fink</a> di farlo e lui, dopo avere macinato chissà cosa nel Terminale, ha compiuto un lavoretto di fino, perfettamente funzionante.

I motivi per i quali mi serviva una shell interattiva in Python sono assai limitati, ma concreti. Il bello delle installazioni Unix è che portano via sempre pochissimo spazio. Il bello aggiuntivo di farlo con FInk è che non rischi niente e al massimo butti via i problemi da una cartella ben visibile oltre che separata dal sistema.