---
title: "Il silenzio è (un vitello) d'oro"
date: 2010-06-29
draft: false
tags: ["ping"]
---

Bei tempi, quelli del <i>kill switch</i>. Quando si scoprì che Apple aveva l'infrastruttura per cancellare <i>app</i> installate sugli iPhone degli utenti, dàgli alle minacce alla libertà, dàgli alle violazioni della <i>privacy</i>, dàgli alla crudeltà delle multinazionali. Era il 2008.

Poi si scoprì che <a href="http://www.macworld.it/ping/nomi/2009/09/27/kill-bill-switch/" target="_blank">Windows Mobile aveva il <cite>kill switch</cite></a> e che <a href="http://www.macworld.it/ping/soft/2008/10/27/la-misura-del-successo/" target="_blank">Android pure</a>. Nel silenzio tombale e nella più totale assenza di proteste.

Aggiornamento: Google ha notificato di <a href="http://android-developers.blogspot.com/2010/06/exercising-our-remote-application.html" target="_blank">avere effettivamente usato il <i>kill switch</i> nei confronti di due applicazioni</a>. Apple non lo ha mai usato. Anche in questo caso, il silenzio sembra d'oro.

Poi mi accusano di essere della <cite>chiesa di Apple</cite>. Può darsi; certo non adoro il vitello d'oro con su scritto <cite>se non è Apple va bene tutto</cite>.