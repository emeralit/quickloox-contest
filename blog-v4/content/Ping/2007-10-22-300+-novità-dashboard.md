---
title: "300+ novità: Dashboard"
date: 2007-10-22
draft: false
tags: ["ping"]
---

Questa si è già vista: i Web Clip. si può tagliare una parte di una pagina web e farla diventare un <em>widget</em>. C'è un'icona nuova apposta in Safari, non presente nella beta caricabile su Tiger. Il widget è vivo (si autoaggiorna) e se ne può personalizzare la cornice.

Dashboard permette anche di guardare i <em>trailer</em> dei film e comprare i biglietti con un clic. Dubito che sarà di qualche uso in Italietta.

Inoltre si possono sincronizzare i widget su più Mac.

Sempre dalla <a href="http://www.apple.com/it/macosx/features/300.html" target="_blank">pagina Apple</a> dedicata a tutte le modifiche presenti in Leopard.

