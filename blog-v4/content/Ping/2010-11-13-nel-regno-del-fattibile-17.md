---
title: "Nel regno del fattibile / 17"
date: 2010-11-13
draft: false
tags: ["ping"]
---

<cite>pregasi elencare azioni fattibili su un iPad e non fattibili su un Sony Vaio W da 10&#8221;</cite>.

<a href="http://jesserosten.com/2010/ipad-photoshoot" target="_blank">Illuminare un set fotografico</a>.

Certo, sono stati usati nove iPad e non uno solo. D'altronde non si sarebbero comunque potuti usare nove Sony Vaio W.