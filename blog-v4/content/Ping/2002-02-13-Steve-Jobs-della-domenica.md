---
title: "Steve Jobs della domenica"
date: 2002-02-13
draft: false
tags: ["ping"]
---

Quando Apple la si ama perché permette di sentirsi importanti muovendole critiche a capocchia

Ormai è passato un po’ di tempo dal più recente Macworld Expo e ho avuto modo di leggere un po’ di commenti su mailing list e newsgroup di appassionati Mac.
Non manca mai lo Steve Jobs della domenica: un personaggio che la mattina si sveglia e sa spiegare ad Apple che cosa dovrebbe e non dovrebbe fare per conquistare il mondo. Inevitabile, nel Paese dove tutti dalla sera alla mattina si scoprono esperti del tema del momento.
Ovviamente sono sempre idee semplicissime ed efficacissime, prive di difetti.
Un comportamento peculiare dello Steve Jobs della domenica è il suo sfasamento temporale: quando presentano nuovi iMac critica Apple per la scarsa attenzione alla linea professionale; quando presentano i nuovi Power Mac, critica Apple perché servirebbero macchine per i consumatori.
Mi sa che lo sfasamento non è solo temporale.

<link>Lucio Bragagnolo</link>lux@mac.com