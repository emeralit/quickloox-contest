---
title: "Programmazione oscura"
date: 2006-08-06
draft: false
tags: ["ping"]
---

Girano molti più programmi di quelli che rivela una visita frettolosa a VersionTracker. Per esempio, in tutt'altre faccende affaccendato, mi sono imbattuto in <a href="http://mute-net.sourceforge.net" target="_blank">Mute</a>, peer-to-peer semplice e basato su un sistema ingegnoso per la tutela della privacy dei singoli nodi, e <a href="http://monolith.sourceforge.net/" target="_blank">Monolith</a>, programmino che esplora in modo intrigante i problemi e i confini del copyright applicato ai media digitali.

Questa è una delle mille punte di un iceberg mostruoso. Ripetendo l'esperienza in altri campi scommetterei tranquillamente sull'emersione di altro software Mac semisconosciuto eppure accessibile.

Quando sento uno sproloquiare che esistono pochi programmi per Mac, vorrei potergli dare i compiti delle vacanze e dirgli <em>beh, se è cos&#236; poco, fammene l'elenco</em>.