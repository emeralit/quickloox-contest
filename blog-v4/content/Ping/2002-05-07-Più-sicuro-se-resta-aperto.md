---
Ètitle: " più sicuro se resta aperto"
date: 2002-05-07
draft: false
tags: ["ping"]
---

Un update significativo rende Mac OS X più sicuro è il merito non è Apple. O sì?

Il Security Update rilasciato qualche tempo fa da Apple rendeva Mac OS X ancora più sicuro di quanto già non sia. Solo gli specialisti hanno bisogno di interessarsene a fondo, ma della sua installazione in un modo o dell’altro beneficiano tutti.
Ma il punto interessante è un altro: tutti gli update riguardano Darwin, la parte open source di Mac OS X, e sono stati messi a punto dalla comunità open source. Apple non ha mosso un dito, se non per realizzare l’update e <link>documentarlo</link> http://www.apple.com/support/security/security_updates.html.
Allora Apple è approfittatrice? Non esattamente; ha allestito un sistema operativo pensando al valore e ai vantaggi di un approccio open source.
Ad approfittarne siamo tutti noi, che usiamo un sistema aperto e, proprio per questo, più sicuro.

<link>Lucio Bragagnolo</link>lux@mac.com