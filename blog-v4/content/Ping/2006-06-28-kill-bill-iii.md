---
title: "Kill Bill III"
date: 2006-06-28
draft: false
tags: ["ping"]
---

Visto che è appena passato l'entusiasmo planetario per la notizia che Bill Gates intende dedicare da qui al 2008 più tempo alla beneficenza e meno a Microsoft, commenterei con l'attenzione su qualcosa di molto piccolo: la chat di <a href="www.scubaportal.it/" target="_blank">Scubaportal</a>.

Funziona solo in Internet Explorer. Con Safari non funziona, con Firefox non funziona, con Omniweb non funziona, solo con iCab è vagamente usabile (ma compare solo un messaggio per volta, almeno con le impostazioni di default).

Ecco, di questo tipo è il lascito di Gates all'informatica. Ora lo attendono bambini, poveri e malati. Saranno scintille.