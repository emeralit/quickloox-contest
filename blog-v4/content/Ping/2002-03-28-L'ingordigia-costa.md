---
title: "L’ingordigia costa"
date: 2002-03-28
draft: false
tags: ["ping"]
---

Forse il modello informazioni-per-pubblicità non funziona. Forse non ce lo meritiamo

MacFixIt, una delle migliori fonti informative su Mac, è diventato a pagamento.
Forse è vero che la pubblicità non basta più a sostenere un sito grosso e impegnativo. Ma è ancora più vero che su Internet vincono le comunità.
Comunità significa partecipare; tutti danno qualcosa in modo che ognuno riceva dieci volte tanto.
Era così. Sta cambiando. Sulle mailing list che frequento i cittadini di Internet diminuiscono costantemente, a sfavore degli ingordi: quelli che se vedono i banner subito pensano a eliminarli; spolpano tutto senza pietà e senza rispetto per gli altri; scaricano, scaricano, scaricano, ma non caricano mai niente.
Con più collaborazione, con più partecipazione, con più comunità forse (forse, per carità) <link>MacFixIt</link>http://www.macfixit.com sarebbe ancora gratis.
Per avere le cose gratis dobbiamo anche meritarcele. Non facciamo gli ingordi. Costa troppo.

<link>Lucio Bragagnolo</link>lux@mac.com