---
title: "Traduttori traditori"
date: 2005-10-01
draft: false
tags: ["ping"]
---

Come si fa a dare retta alla sostanza, se la forma è distorta?

Ugo Foscolo sbeffeggiava Vincenzo Monti chiamandolo <cite>gran traduttor dei traduttor d'Omero</cite>. Praticamente uno che non traduceva dall'originale, ma da un'altra traduzione. Roba di terza mano, insomma.

Non dirò che sito è, perché non va di moda. È un'epoca in cui gli incapaci vengono difesi come mai prima. Forse per solidarietà di casta.

Comunque, è gente che ha avuto il coraggio di tradurre una dichiarazione inglese nel modo seguente:

[il supposto problema degli schermi di iPod nano] <cite>è un problema reale, ma di scarsa rilevanza, che riguarda problemi di qualità del venditore.</cite>

L'ipotesi è che iPod nano abbia problemi allo schermo. E dipenderebbe dalla qualità del… venditore. Come dire che forse, se l'insegna dell'Apple Center non è abbastanza luminosa, gli schermi del nano non funzionano.

In chiaro: <em>vendor</em> non significa venditore. Peccato che non saperlo renda incomprensibile tutto il resto.

C'è gente che non sa che cosa scrive. Questi non sanno che cosa leggono ed è peggio.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>