---
title: "Bello e istruttivo"
date: 2008-04-18
draft: false
tags: ["ping"]
---

La procedura per avere un Terminale funzionante su iPod touch è stata estremamente istruttiva.

L'istruzione niente ha a che vedere con ssh, sftp, i pacchetti scaricati da Google Code, le password di root.

È che negli anni ho sentito tanta gente dire che un computer in casa era fin troppo.

Adesso le stesse persone hanno in casa un Mac e un iPod touch. E credono di avere un computer, più un simpatico <em>gadget</em> per ascoltare musica in modo <em>cool</em> e mostrare le foto delle vacanze sempre nel verso giusto (<em>lo giri, fa tutto lui</em>).

Marketing geniale o prossima rivoluzione? Forse tutt'e due.