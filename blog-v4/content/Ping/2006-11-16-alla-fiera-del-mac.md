---
title: "Alla fiera del Mac"
date: 2006-11-16
draft: false
tags: ["ping"]
---

Quelli dell'All About Apple Club e titolari dell'omonimo museo sono troppo buoni. Hanno allestito una pagina di <a href="http://www.allaboutapple.com/market/market.htm" target="_blank">mercatino</a> in cui i privati possono vendere Mac e accessori vecchi e nuovi e poi, se la vendita va a buon fine, lasciano al venditore libertà di decidere se fare o meno una donazione al museo (il più bel museo Apple del mondo, per modestia).

Facciamo che chi acquista si impegna a rompere le scatole al venditore fino a quando quest'ultimo non effettua una donazione? :-)