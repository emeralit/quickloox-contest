---
title: "Esperienze naturali"
date: 2010-02-01
draft: false
tags: ["ping"]
---

Questo blog non diventerà, probabilmente, ostaggio di iPad. Anche se ho un sacco di cose da dire e devo ancora parlare un bel po' di come ne ha non-parlato, in modo davvero riduttivo per una persona delle sue competenze e della sua intelligenza, <a href="http://attivissimo.blogspot.com/2010/01/aiped-aipad-aipod.html" target="_blank">Paolo Attivissimo</a>.

Il problema, e se ne riparlerà, è che ci sono davvero un mondo nuovo e un mondo vecchio e che pensare in modo vecchio non aiuta a comprendere il mondo nuovo.

Ciò che ha smesso di contare, nel mondo nuovo, è la lista della spesa. Per capire come vanno le cose nel mondo reale e come si comportano le persone vere nella vita vera, riporto molto volentieri e con il suo permesso questa esperienza di <b>Marco</b>:

<cite>Voglio condividere solo un piccolo fatto che mi ha fatto estremamente piacere e coinvolge il sottoscritto, una mia amica e un MacBook.</cite>

<cite>Cerco di riassumere l'intera faccenda in una manciata righe: ho la fama di saperne abbastanza sui computer e a S*** (l'amica in questione) serviva un nuovo portatile. Andiamo al MediaWorld e adocchia diversi Acer e Asus dai prezzi appetibili (S*** aveva un</cite> budget <cite>limitato) con davvero buone caratteristiche hardware. S*** ha sempre ammirato i Mac, ma li aveva esclusi a priori a causa del costo. La convinco a chiedere alla commessa se per caso hanno qualche vecchio MacBook in magazzino, tanto al massimo dice no. Puoi immaginare la nostra reazione quando ci dice</cite> s&#236;, mi pare di averne uno bianco <cite>(quello precedente all'</cite>unibody<cite>). Controllo, se c'è chiedo a quanto te lo vendono!</cite>

<cite>S*** era un po' titubante (&#8230;non ha il tastierino numerico&#8230;), ma riesco ad aver la meglio sostenendo che questa è un'occasione unica e che può avere il computer desiderato da mesi, tra l'altro scontato. Lo blocca, torna al pomeriggio ad acquistarlo (nel frattempo si era già formata una lista di persone decise a comperarlo!), lo porta da me per aggiornamenti e altro, le faccio una breve lezione introduttiva e la lascio una settimana per abituarsi a Mac OS X.</cite>

<cite>Sono appena tornato da lei ed era al settimo cielo.</cite> Marco, sono felicissima. Mac OS X è la cosa migliore che abbia mai visto e alcune operazioni sono cos&#236; ovvie che mi chiedo perché non le hanno fatte cos&#236; anche su Windows! Poi è cos&#236; pieno di dettagli da lasciar a bocca aperta ogni volta che lo accendo: Magsafe, la struttura delle finestre, i riflessi in iTunes, le ombre, i drag'n'drop!&#8221;. <cite>Quando le ho fatto leggere ciò che è scritto nell'icona di TextEdit, non ci poteva credere ed è rimasta di sasso quando le ho mostrato Pixelmator, cosa può fare Anteprima (sottovalutato a mio avviso!).</cite>

<cite>E sai perché sono davvero felice? Non perché lei ora è convinta che Mac OS X sia una delle cose più belle a livello di ergonomia, ma perché le ho fatto scoprire una cosa nuova e questa l'ha letteralmente entusiasmata. Parole sue:</cite> ti fa cambiare l'approccio che hai con i computer! È&#8230; una figata, un altro mondo! Non voglio tornare indietro!

<cite>Vedere S*** felice, sapendo che ha fatto moltissimo per me, è una delle cose più belle che potesse capitarmi!</cite>

<cite>Sul lato più strettamente Mac, sta giocando a indovinare le scorciatoie da tastiera senza guardare dai menu, seguendo la logica (perché ha capito subito che non erano messe a caso (Alt+F4?) e ne ha già scovate parecchie! ;)</cite>

<cite>La morale informatica invece è sempre quella alla fine: puoi costruire un computer pompato, ma se non riesci a creare quel</cite> feeling <cite>con l'utente finale, quell'uniformità, è tutto inutile.</cite>

<cite>Scusami per la mail lunga, mi son lasciato trasportare dall'entusiasmo.</cite>

Molto meglio, credimi Marco, che lasciarsi trasportare da una macina legata al collo, quella di chi ai-miei-tempi-era-cos&#236;-e-tutto-il-resto-è-sbagliato.