---
title: "l’iMac alieno"
date: 2002-02-19
draft: false
tags: ["ping"]
---

Qualunque essere con due antenne va guardato con sospetto. Ma ci sono eccezioni

Pochi lo sanno, ma il nuovo iMac ha ben due antenne AirPort, una su ciascun lato dello schermo. Il computer sceglie con quale antenna ricevere e trasmette invece sempre con la stessa antenna (quale delle due è lasciato come esercizio al lettore).
Un consiglio per vivere meglio? AirPort. Anche in casa, anche sul desktop, si dimenticano i cavi e la loro polvere, si sposta il computer senza problemi... quando finalmente si riuscirà ad avere wireless anche l’alimentazione di rete ci sarà la seconda rivoluzione del computer.
Ah, il salutista non tema: le onde radio di AirPort viaggiano a una potenza assolutamente irrisoria. Prima di preoccuparsi di quelle sarebbe il caso di costruire una calotta di piombo sopra casa per proteggersi dai raggi cosmici.
AirPort è buona e l’iMac con due antenne assomiglia a un alieno, ma è buono anche lui.

<link>Lucio Bragagnolo</link>lux@mac.com