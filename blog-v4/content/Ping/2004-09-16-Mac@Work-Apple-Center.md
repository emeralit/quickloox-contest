---
title: "Nascono gli Apple Center, a partire da"
date: 2004-09-16
draft: false
tags: ["ping"]
---

Comunicato biecamente pubblicitario viziato da conflitto di interessi

In Europa non arrivano gli Apple Store, almeno per ora (salvo Londra, Regent Street). Ma ci sono gli Apple Center, punti vendita indipendenti che si sono rinnovati in collaborazione con Apple.

Il primo in Italia è <a href="http://www.macatwork.net">Mac@Work</a>, di cui mi onoro di essere socio, per quanto di infima minoranza. Sabato dalle 18 si fa festa, si assegna in premio un iPod mini e poi si va tutti assieme a mangiarsi una pizza dopo avere visto lo store tutto nuovo, sempre uguale però nella voglia di fare e nell'entusiasmo.

Ci sarò anch'io, per tutti quelli che vorranno rinfacciarmi di persona il conflitto di interessi. :-)

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>