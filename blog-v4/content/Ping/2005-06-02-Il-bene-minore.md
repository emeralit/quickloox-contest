---
title: "Il bene minore"
date: 2005-06-02
draft: false
tags: ["ping"]
---

Scoprire le scorciatoie di Panther grazie all'adozione di Tiger

Dopo avere installato Tiger noti subito che qualcosa è cambiato, per esempio dall'icona di Spotlight, che cambia la vita.

Andando avanti a usarlo scopri che è cambiato tutto e a un certo punto ti metti persino a cercare cose che non avresti mai pensato. Se è cambiato tutto è molto probabile scoprire qualcosa.

Uso molto spesso Comando-Tab per passare rapidamente, in avanti, tra un programma e l'altro. Il comando per muoversi indietro è Comando-Maiuscolo-Tab, che è piuttosto scomodo.

Ho scoperto che per tornare indietro è molto più comodo Comando-minore (il simbolo <). Scorciatoia che però esisteva già in Panther, e chissà, magari anche in Jaguar.

Se Tiger non mi avesse risvegliato la curiosità, però, non so quando me ne sarei accorto.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>