---
title: "Un Finder più sveglio"
date: 2005-05-08
draft: false
tags: ["ping"]
---

Adesso si rende conto di stare seduto sopra un sistema Unix

Prima di Tiger accadeva spesso che un cambiamento apportato al sistema attraverso il Terminale passasse inosservato al Finder, anche per alcuni minuti o indefinitamente, salvo logout.

Adesso il Finder è molto più all'erta e si accorge sempre e subito di che cosa hai combinato lavorando nella shell. Apparentemente, un file cambiato assume prima l'aspetto di un'icona generica, mentre in pochi istanti il Finder controlla le informazioni a sua disposizione e assegna un'icona specifica.

C'è da meravigliarsi che il sistema lavori tanto più di prima, su questa e mille altre cose, eppure Tiger sia più veloce di Panther, anche sulle macchine più vecchie.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>