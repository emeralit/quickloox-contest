---
title: "Ultime noiosità"
date: 2009-02-27
draft: false
tags: ["ping"]
---

Poche altre notazioni sul nuovo 17&#8221; e poi smetto pure, per non annoiare o dare impressioni sbagliate.

Il collaudo della iSight integrata ha funzionato alla perfezione e come del resto avevo già visto persino sui MacBook. I processori sono di potenza tale che la videoconferenza è un fatto acquisito.

Tutto a posto sull'audio, via auricolari (vanno quelli di iPod, vanno quelli di iPhone) e via diffusori incorporati. La qualità dell'audio, sempre tenendo conto che sono cassettine messe dentro un portatile, è decisamente superiore a quella del PowerBook G4.

Come sospettavo, non cercandole, non avrò grandi soddisfazioni dalla batteria. Le specifiche Apple (sette/otto ore) sono quelle di un modello base con la Gpu; le mie sono pompate al massimo, con la scheda video. L'autonomia dichiarata sul mio 17&#8221; con una ventina di applicazioni aperte (Apple ne usa due) inizia dalle quattro ore e poco di più. Se accendo World of Warcraft, che adesso è impostato al massimo su tutti gli effetti video, riesco a dimezzare l'autonomia senza difficoltà.

Tutto ampiamente previsto, però, e il PowerBook da nuovo non ha mai raggiunto questi valori. I mille cicli di carica dovrebbero preludere a una speranza di vita di almeno tre anni; se sia vero lo saprò dire tra un po'.

La nuova <em>trackpad</em>-pulsante è ineccepibile. La mancanza del pulsante visibile non è un problema e la risposta è, credo, leggermente migliorata. Le gesture di nuova concezione le uso poco (mi concentro al massimo sulla tastiera) e però tornano utili. In particolare le quattro dita che liberano la scrivania sono immediate ed entrano subito nel bagaglio memorizzato. Apple su una cosa non è riuscita a fare comunicazione; un mare di gente pensa che, tolto il pulsante della <em>trackpad</em>, la <em>trackpad</em> non sia cliccabile. La clicco sempre meno, ma è cliccabile.

Le cose ancora da provare sono Ethernet (a breve), SuperDrive (a medio) e ingresso microfono (a lungo).

Per finire, visto che la mortalità infantile nell'elettronica è subito altissima e poi decade, ho lasciato la macchina accesa a scaricare roba ininterrottamente da mercoled&#236; dopo pranzo a venerd&#236; mattina presto (poi dovevo uscire). È ancora vivo.

Oggi lo sto provando in @Work. Essendo il primo a mostrarsi in pubblico (le unità demo arrivano luned&#236;), mi è toccato farlo vedere un po' dovunque, in condizioni di illuminazione molto variabili.

La diatriba tra lucido e opaco ha qualche senso, ma è ampiamente sopravvalutata. I punti dove c'è qualche riflesso sono gli stessi dove avevo i riflessi sull'opaco. Ieri sera a casa ho acceso una certa luce in cucina che sul vecchio PowerBook ammazzava la visibilità e il nuovo 17&#8221; se la cava meglio. Di poco, però meglio.

In prestazioni, complessivamente, stimo un guadagno 5x-10x tra vecchio G4 e nuovo Core 2 Duo.

Per ora insisto nella mia valutazione a pollici alzati.