---
title: "Un gioco per l’estate"
date: 2004-04-05
draft: false
tags: ["ping"]
---

Piccolo consiglio ludico per il Mac

Per una volta consiglio un gioco: piuttosto semplice da imparare, difficile da padroneggiare, vario, poco impegnativo o molto impegnativo, a piacere, che si può giocare anche tutti insieme in rete, oppure uno alla volta a turno davanti allo stesso computer, anche con amici un po’ miopi che non usano Mac OS X.

Si chiama <link>Battle for Wesnoth</link>http://www.wesnoth.org e, a dirla come gli espertoni, è un gioco strategico-tattico a livello di singola unità, a turni, con mappa a esagoni e ambientazione fantasy.

La violenza esplicita è pressoché inesistente; ci sono combattimenti ma nessuna concessione allo splatter.

Ultima nota: Battle for Wesnoth è open source, parla inglese ma anche un po’ di italiano (anzi: se qualcuno ha voglia di scrivere la documentazione in italiano è benvenuto), ed è gratis. Più di così.

<link>Lucio Bragagnolo</link>lux@mac.com