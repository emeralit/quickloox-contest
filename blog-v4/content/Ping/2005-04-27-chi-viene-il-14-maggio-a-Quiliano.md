---
title: "Chi viene a Quiliano il 14?"
date: 2005-04-27
draft: false
tags: ["ping"]
---

La data corretta per l'inaugurazione del primo museo europeo di Apple è questa

Ieri ho scritto una scempiaggine. La data esatta dell'inaugurazione dell'All About Apple Museum in quel di Quiliano (Savona) è sabato 14 maggio 2005.

Potevo guardare iCal, ma anche scrivere <code>cal 5 2005</code> nel Terminale, o usare il menu grafico di data e ora. La precisione è tutto.

Ah… provato a scrivere nel Terminale <code>cat /usr/share/calendar/calendar.computer</code>?

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>