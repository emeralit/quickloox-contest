---
title: "Attento a come parli, Steve"
date: 2004-01-19
draft: false
tags: ["ping"]
---

Lo slogan peggiore della storia recente di Apple

Su certe mailing list passo per integralista e difensore di Apple a oltranza. Eppure sono il primo a sostenere che, nel suo discorso al Macworld Expo di San Francisco pochi giorni fa, una Steve Jobs non doveva proprio dirla:

iLife is like Microsoft Office for the rest of your life.

Grazie al cielo vivo una vita felice e prospera il giusto senza alcun bisogno di Office: quel software elefantiaco, inefficiente, pieno di bachi, veicolo preferito dai virus, creato da una azienda condannata in tribunale per abuso di monopolio e concorrenza sleale.

Voglio sperare che iLife sia una cosa proprio diversa e non, alla Jobs, come Office.

<link>Lucio Bragagnolo</link>lux@mac.com