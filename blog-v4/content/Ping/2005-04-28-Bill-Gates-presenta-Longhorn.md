---
title: "Chiacchiere e distintivo"
date: 2005-04-28
draft: false
tags: ["ping"]
---

Almeno De Niro recitava. Bill Gates fa tragicamente sul serio

Sono stato l'altroieri alla presentazione in anteprima per la stampa di Tiger. Un sistema operativo dotato di una nuova funzione per la ricerca semplicemente sensazionale e una serie di modifiche e migliorie che per descriverle tutte non basteranno tre mesi. Tiger è qui, oggi, pronto nei negozi, a disposizione di chi lo vuole comprare.

Conflitto di interessi in funzione: Mac@Work organizza una serata speciale Tiger a partire dalle 18 di oggi, con aperitivo, seminari, pizzata e serata Tiger, e tutto il sabato con seminari e dimostrazioni, sempre sulla tigre. Fine conflitto di interessi.

Nel frattempo Bill Gates si è presentato alla stampa per annunciare che nel dicembre 2006 (giusto per non dire 2007, che fa due-anni-invece-di-uno) si avrà la prossima versione di Windows. I ritardi si devono al fatto che i programmatori Microsoft non sono stati capaci di rendere funzionante il nuovo filesystem, che avrebbe dovuto rendere le ricerche più veloci. Ma, tranquillo, Longhorn avrà una funzione di ricerca tutta nuova, molto efficace, ben diversa da quella penosa di oggi.

Di più: Microsoft ha deciso di dichiarare guerra al formato Pdf. Ovviamente con un suo mirabolante nuovo formato.

Tiger è qua ed è sei volte meglio di quello che promette Gates per dicembre 2006. Tutto chiacchiere e Windows. Ricordo De Niro nella parte di Al Capone, quando urlava al poliziotto<em> sei solo chiacchiere e distintivo!</em>

Ma De Niro almeno sa recitare.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>