---
title: "Meglio subito che tardi"
date: 2010-07-21
draft: false
tags: ["ping"]
---

Apple non investe più sui computer, sostiene chi pensa a basso costo.

Dovrebbe prendere esempio da Dell, il cui supporto ha inviato a vari clienti <a href="http://en.community.dell.com/support-forums/servers/f/956/t/19339458.aspx" target="_blank">schede logiche già infette da software ostile</a>.

Forse nell'opinione che, dovendo prima o poi accadere, tanto vale togliersi il pensiero.

<cite>I sistemi privi di Windows</cite>, precisa il supporto tecnico Dell, <cite>non sono vulnerabili a questo problema.</cite>