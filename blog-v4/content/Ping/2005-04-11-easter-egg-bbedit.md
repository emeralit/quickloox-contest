---
title: "Il numero dell’easter egg"
date: 2005-04-11
draft: false
tags: ["ping"]
---

Omaggio a un piccolo segreto e a una nuova funzione

Il programma Macintosh per antonomasia è BBEdit: uno strumento inarrivabile per trattare il testo all’interno – sublime paradosso – dell’interfaccia grafica.

È da poco uscito BBEdit 7, come al solito una bellezza, e vale quindi la pena di ricordare la vecchia 6, soprattutto per l’indimenticata pagina 666 dell’help Html presente nella documentazione. Chi può vada a vederla, è – forse – una piccola sorpresa.

Trovare se sia nascosto qualcosa del genere in BBEdit 7 è meno facile, perché molto materiale che prima stava in cartelle a parte è stato integrato nell’applicazione.

Ma Mac OS X permette di curiosare dentro un’applicazione semplicemente control-cliccandola e seguendo l’equivalente italiano della voce Show Package Contents, e chissà che non si riesca a scovare qualcosa...

<link>Lucio Bragagnolo</link>lux@mac.com