---
title: "Voto provvisorio"
date: 2009-03-05
draft: false
tags: ["ping"]
---

In un test-batteria del tutto estemporaneo e non previsto, il MacBook Pro 17&#8221; ha preso quattro e mezzo (nel senso delle ore).

Diciassette applicazioni aperte, pasticci su file Dicom con OsiriX, impaginati in InDesign, scaricamenti insistiti di file via Safari e Pando, più la consueta attività di scrittura, posta, Rss, navigazione, visione Pdf e cos&#236;.

Considerato che al momento di attaccare l'alimentazione la macchina sosteneva di averne per un'altra mezz'ora e che la macchina in questione è configurata per le prestazioni e non per il risparmio, sembra un risultato realistico.

In attesa naturalmente di un test scientifico; qui semplicemente si è lavorato più o meno dalle due alle sei e mezza.