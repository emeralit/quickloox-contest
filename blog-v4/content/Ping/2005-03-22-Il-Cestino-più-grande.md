---
title: "Riciclaggio infinito"
date: 2005-03-22
draft: false
tags: ["ping"]
---

Non è una cosa seria, ma è un record (personale)

Ho vuotato un Cestino da 49.346 elementi.
La qual cosa è assolutamente irrilevante, chiaro, se non che non mi era mai capitato di arrivare a una cifra del genere.

Viene voglia di adottare utility come <a href="http://homepage.mac.com/gweston/compost/index.html">Compost</a>. E, dopo avere letto qualche libro in eccesso sull'elaborazione dell'informazione, viene da chiedersi dove finiscano, e che cosa rappresentino, tutti quei miliardi di bit che se vanno senza neppure un pigolìo.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>