---
title: "Difetto di pronuncia"
date: 2009-05-17
draft: false
tags: ["ping"]
---

Gmail non attivava la <em>chat</em> GTalk. L'avviso diceva circa <cite>o ci sono problemi di collegamento o il tuo amministratore ha disattivato GTalk</cite>.

Ho seguito la guida di Google (il sistema migliore che abbia visto finora in Rete) e alla fine ho provato il collegamento con https://mail.google.com. Ha funzionato.

Un caso in cui per parlare si deve mettere l'enfasi sulla esse!