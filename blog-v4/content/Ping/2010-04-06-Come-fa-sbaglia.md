---
title: "Come fa, sbaglia"
date: 2010-04-06
draft: false
tags: ["ping"]
---

L'analista di Piper Jaffray, Gene Munster, aveva previsto una vendita nel primo giorno di 200-300 mila iPad, che poi ha corretto a <a href="http://www.macworld.it/news/2010/04/04/600-700-mila-ipad-venduti-il-primo-giorno/" target="_self">600-700 mila</a>.

Al che ha scritto una nota ai suoi seguaci investitori per spiegare come si sia determinato l'errore, dal momento che l'annuncio ufficiale di Apple parla di 300 mila unità.

Cioè ha sbagliato la prima volta, sottostimando clamorosamente la vendita (adesso arrivo al perché); poi ha sbagliato la seconda volta, correggendo la sua stima precedente e ora ha sbagliato a spiegare che ha commesso un errore.

La stima di 200-300 mila iPad era sbagliata, perché Apple ha comunicato le vendite dei soli modelli Wi-Fi. È sempre stato possibile preordinare il modello 3G e nessuno oggi sa dire quanti 3G già acquistati siano in attesa di consegna per <cite>fine aprile</cite>. Fossero uno ogni tre Wi-Fi, saremmo già a quattrocentomila unità preacquistate fino al 3 aprile. La stima precedente al 3 aprile è dunque troppo bassa.

Nelle sue rilevazioni del giorno di apertura delle vendite, Munster ha corretto il tiro e raddoppiato la stima. Dimenticandosi dei 3G e pensando solo ai Wi-Fi, molti meno delle sue nuove cifre. Secondo errore.

Il terzo errore, più grave di tutti, è non avere proprio capito il meccanismo (il <a href="http://www.apple.com/pr/library/2010/04/05ipad.html" target="_blank">comunicato di Apple</a> è chiaro come acqua di fonte). Munster non si è ancora accorto che la cifra ufficiale di Apple riguarda il <i>consegnato</i>, non il <i>venduto</i>.

Analista: persona che lavora con quella parte del corpo l&#236;.