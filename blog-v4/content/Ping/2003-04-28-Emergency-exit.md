---
title: "Emergency exit"
date: 2003-04-28
draft: false
tags: ["ping"]
---

Come cavarsela con il Terminale, parte prima

L’amico Orio mi ha suggerito di scrivere un articolo “sulle uscite di emergenza dal Terminale”. C’è voluta poca riflessione per decidere che era una buona idea, perché mi trovo spesso nelle condizioni in cui qualcosa del genere fa solo bene: sono entrato nel Terminale quasi senza volere e, realizzato o meno il mio scopo originario, vorrei solo filarmela senza fare danni.

Una premessa: quando proprio sei alle strette, chiudere la finestra del Terminale (o chiudere il Terminale, con mela-q) è poco elegante e anche scorretto, ma si può fare.

Un modo elegante di uscire, invece, è digitare exit e poi andare a capo.

Quando si fa? Prima di chiudere il Terminale (se si fa, attendere il messaggio [process completed]). Se sei root, un primo exit ti fa tornare utente normale; il secondo chiude la sessione.

Se ci sono aperte più finestre Terminale, digita exit in ciascuna.

Chissà, Orio: alla fine magari ne esce una serie di consigli utili. Intanto, a consigli, grazie per il tuo. :)

<link>Lucio Bragagnolo</link>lux@mac.com