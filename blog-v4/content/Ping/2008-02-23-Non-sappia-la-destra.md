---
title: "Non sappia la destra"
date: 2008-02-23
draft: false
tags: ["ping"]
---

Sono proprietario provvisorio (per un mese) di iPod touch e oggi ho iniziato a conoscere il nuovo animaletto in affido.

Le primissime impressioni.

L'oggetto è fantastico.

Ho sincronizzato tutto il sincronizzabile, una cosa che non farei mai se l'oggetto fosse veramente mio, ma questa è l'ora dei collaudi. Il touch ha assorbito senza problemi tutti i miei contatti, tutta la musica, tutte le foto, tutti i <em>bookmark</em> e non ricordo che altro.

L'intuitività è quasi indecente. C'è ancora chi si stupisce del successo degli iPod e questo è dieci volte più semplice, più immediato, più appagante. La risposta al tocco è di una naturalezza sconvolgente. La qualità dello schermo è ottima, la luminosità eccellente. Io l'ho impostata al minimo assoluto ed è più che sufficiente.

Ho configurato tutto, dalla posta elettronica in giù, tenendo e usando il touch solo con la sinistra e con il pollice della stessa. Nessun problema, ottima velocità, tastiera più che adeguata. Il mio pollice sinistro è lievemente meno snodato del destro, per via di un piccolo capolavoro compiuto anni fa con un coltello da parmigiano.

Mano sinistra appagata; adesso devo riuscire a mettere nella destra un iPhone.