---
title: "Studiare, studiare, studiare"
date: 2008-07-14
draft: false
tags: ["ping"]
---

Questo blog rimarrà in gran parte Mac. Tuttavia, di tanto in tanto, sarà anche iPhone.

Perché iPhone è un computer, che condivide la base software con Mac.

E perché nel lungo periodo finiremo per parlare comunque della stessa cosa. Il trittico Mac-iPhone-MobileMe è solo l'inizio di quello che sul Sole 24 Ore definirebbero <em>cambiamento di paradigma</em> e qui, più modestamente, chiamerei sovversione delle abitudini.

Apple sta investendo pesantemente su un'architettura di realizzazione di applicazioni con JavaScript che si chiama <a href="http://www.sproutcore.com/" target="_blank">SproutCore</a> e che permetterà di allestire <em>web application</em> molto più veloci e versatili delle attuali. iPhone con le web application è a casa propria. MobileMe potrebbe diventare un deposito di <em>web application</em> utili per il computing del prossimo decennio e ovviamente il browser su Mac con le <em>web application</em> ci va a nozze.

È solo l'inizio, dico. Quello che capisco, mentre tutto quello che sembra importare alla gente è riuscire a usare iPhone come un cellulare (come se io mi impegnassi per fare con la bicicletta quello che faccio a piedi), è che devo rimettermi a studiare il mondo Apple, come se lo incontrassi oggi per la prima volta.

Lo consiglio a tutti. Apparentemente, lo sport del weekend è fare con grande convinzione affermazioni su iPhone che erano vere l'altroieri e non lo sono più da oggi.