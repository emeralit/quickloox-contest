---
title: "Scuola di giornalismo"
date: 2006-06-14
draft: false
tags: ["ping"]
---

Dave Winer ha girato una <a href="http://www.scripting.com/2006/06/13.html#videoAtVloggercon" target="_blank">videointervista</a> a John Dvorak, il controverso giornalista che da anni predice tragedie e distruzioni per Apple, si augura che l'azienda venga comprata da Ibm, sostiene che Apple dovrebbe adottare Windows come sistema operativo e altre cose del genere. Dvorak, in video e audio, svela il suo metodo di lavoro (riassumo e sintetizzo):

<cite>Questa è la formula per infastidire gli utenti Macintosh e ottenere in cambio un sacco di attenzione.</cite>

<cite>Per prima cosa scrivo materiale che sia semiinnocuo, contenente materiale insultante a sufficienza per richiamare un sacco di attenzione dalla comunità Macintosh. Così iniziano a reagire. In questo modo faccio un sacco di ascolti.</cite>

<cite>Allora reagisco come se fossi stupito e come se nessuno avesse capito che cosa intendevo dire. Mi lamento e mi chiedo che cosa ci sia di sbagliato in queste persone. Il che le infastidisce ancora di più. E gli ascolti salgono alle stelle!</cite>

<cite>A questo punto lascio decantare le cose per un po' e poi, qualsiasi posizione abbia assunto in precedenza, la rovescio per affermare l'esatto opposto. Racconto al pubblico Macintosh che mi ero completamente sbagliato e che avevano ragione loro, così gli ascolti sfondano qualsiasi tetto!</cite>

Chi non ci credesse, può scaricare il <a href="http://s3.amazonaws.com/scripting/dvorak.mov?torrent" target="_blank">filmato</a> (è un link BitTorrent, quindi serve un client BitTorrent, per esempio <a href="http://www.bittorrent.com" target="_blank">BitTorrent</a>).

Si noti che uno dei nomi più in vista nel mondo del giornalismo Macintosh ammette che, da dieci anni come minimo, alza polveroni invece di informare seriamente. E la cosa viene ignorata dai siti cosiddetti di informazione Macintosh italiani.

Non li biasimo, peraltro. Metti che si dimentichino le virgolette e potrebbe sembrare una confessione.