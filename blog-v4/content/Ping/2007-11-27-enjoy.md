---
title: "Enjoy! (godiamocela)"
date: 2007-11-27
draft: false
tags: ["ping"]
---

Faccio da ripetitore: venerd&#236; prossimo, 30 novembre, ci sono almeno due Mac-ritrovi in giro per l'Italia: la pizzata mensile del <a href="http://www.poc.it" target="_blank">PowerBook Owners Club</a> a Milano e il Calmug Meeting 2007, di cui riporto la comunicazione ufficiale (per quanto serve) qui sotto.

<cite>Avremo a disposizione del buon cibo e la connessione Wi-Fi gratuita durante l'intera serata, cos&#236; potremo portare il nostro Mac.</cite>

<cite>Saranno presentato ai Soci il nuovo sistema operativo Mac Os X Leopard, l'iPod touch e in anteprima l'iPhone.</cite>

<cite>Saranno, inoltre, consegnate le tessere per l'anno 2008 e verrà effettuato un sorteggio a premi.</cite>

<cite>Come ad ogni incontro ci sarà anche una piccola mostra di retrocomputing, dall'Apple II Lcd (il primo portatile Apple con monitor Lcd), il Mac Plus, il Powerbook 520C fino ad arrivare all'iBook conchiglione, Apple Cube e iMac lampadone e anche quelli più recenti.</cite>

<cite>L'incontro si svolgerà presso il <strong>Mamas Club</strong> (sito in via Quattronari n&#176; 14 a Pellaro, Reggio Calabria).</cite>

<cite>L'appuntamento è alle 20.30 in punto; sarebbe gradita la <a href="mailto:segreteria@calmug.org" target="_blank">conferma via posta della partecipazione</a> a questo indirizzo  
o <a href="http://www.calmug.org" target="_blank">visitando il (forum del) sito</a>.</cite>

<cite>Enjoy!</cite>

Vedersi bene e spesso, tra gente Mac e non, è sempre una bellissima cosa. :-)