---
title: "Traduzione cercasi"
date: 2005-05-04
draft: false
tags: ["ping"]
---

Constatato che Dashboard serve, come li chiamiamo quei cosi?

Quando Tiger è stato annunciato, ero molto scettico su Dashboard. Ho già abbastanza stupidaggini sullo schermo senza che debba aggiungerne Apple.

Ma la carta vincente di Dashboard è proprio lo sparire a comando. Serve la calcolatrice? F12, calcolo, F12 e ho risolto il problema, senza fastidi sullo schermo (per la cronaca, la Calcolatrice di Tiger non è semplice come quella di Dashboard e merita davvero un sopralluogo). Mi serve un indirizzo che so essere presente nella rubrica? F12, prendo l'indirizzo, un altro F12 ed è fatta.

Fantastico. Ma i widget, come li chiamiamo? Accrocchi? Cazzilli? Aggeggi? Manzanilli (grazie, Eta Beta)?

Non ho astio verso l'inglese e anzi il sistema operativo lo uso in inglese… ma non mi dispiace avere una traduzione in italiano per quando serve.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>