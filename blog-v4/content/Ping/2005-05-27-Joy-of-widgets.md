---
title: "Joy of Widgets"
date: 2005-05-27
draft: false
tags: ["ping"]
---

A qualcosa, i genietti (cosi? Manzanilli? Nosotti?) servono

Luca segnala il widget di <a href="http://www.geekculture.com/joyoftech/joystuff/widgets.html">Joy of Tech</a>, una spassosa striscia a fumetti dedicata alla tecnologia nella quale (la striscia) il Mac compare spesso e volentieri.

Rispetto al sistema antiquato di ricevere una mail (che magari oggi non mi va) o di doversi collegare a una pagina Web, il widget è quanto di meglio possa esserci per consultare a volontà una striscia quotidiana a fumetti.

Magari anche altro. Devo pensare al widget di Ping…

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>