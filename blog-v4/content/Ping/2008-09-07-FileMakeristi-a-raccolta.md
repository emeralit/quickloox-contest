---
title: "FileMakeristi a raccolta"
date: 2008-09-07
draft: false
tags: ["ping"]
---

La parola a <strong>Giulio</strong>:

<cite>Eccoci qui: il 17, 18 e 19 settembre andiamo in scena a Rimini. E indovina un po' qual era l'unico paese dell'Europa occidentale senza conferenza sviluppatori (e non rispondere</cite> la Spagna<cite>, perché hanno tenuto la loro prima conferenza in primavera)?</cite>

<cite>Devo dire che è una bella soddisfazione, soprattutto perché FileMaker spesso è considerato un programma semiprofessionale, a causa della facilità di uso.</cite>

<cite>Mettere insieme quasi un centinaio di professionisti interessati all'argomento e far venire il Technical Manager for Southern Europe, Middle East and Africa di FileMaker, Inc. per una sessione sul funzionamento di FileMaker dall'interno (i diversi motori che regolano i vari tipi di dato, il funzionamento dei</cite> lock<cite>, i file e gli indici, il comportamento in rete&#8230;) è stato veramente&#8230; godurioso, si può dire? faticoso, sicuramente, ma dovrebbe proprio valerne la pena.</cite>

<cite>Se ti interessa, trovi tutto su <a href="http://www.fmdevcon.it" target="_blank">http://www.fmdevcon.it</a>.</cite>

Non so se potrò presenziare. Molti, in compenso, dovrebbero.