---
title: "Escursioni termiche"
date: 2007-06-22
draft: false
tags: ["ping"]
---

Non ho sottomano la fonte, ma non è il calore che fa fuori i dischi rigidi. Ci sono un paio di studi, uno condotto su un numero impossibile di cluster aziendali e uno sui dischi di&#8230; Google, che lo attestano su un campione di alcuni <em>milioni</em> di dischi, di tutte le marche (Google ha anche compilato una classifica di affidabilità dei dischi per costruttore ma la tiene segreta, ahimé).

Invece, a fare fuori i dischi rigidi sono i cambiamenti troppo repentini e troppo ampi di temperatura, tipo una stanza calda che viene improvvisamente condizionata a livelli semipolari e cose cos&#236;. Altre cose da sapere: la speranza di vita media di un disco rigido è tre anni. La mortalità più alta, tuttavia, si verifica nel quarto anno&#8230; e nel primo.

Leggo troppe scempiaggini di gente che non ne sa niente e si basa dell'esperienza del cugino per dedurre leggi universali. Bisogna stare attenti alle escursioni termiche, non al caldo in sé. Vale per i dischi, ma anche per la materia grigia.