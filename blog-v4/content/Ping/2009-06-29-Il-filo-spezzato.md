---
title: "Il filo spezzato"
date: 2009-06-29
draft: false
tags: ["ping"]
---

L'ultimo pezzo della nuova postazione di lavoro allestita è stato un Mighty Mouse <i>wireless</i> Apple.

Il mouse mi serve solamente nelle situazioni estreme dove la tastiera non è conveniente o non è supportata e di conseguenza sul Mighty Mouse in sé non ho da dire molto. Per dire, non ho nemmeno configurato i pulsanti laterali, né quello corrispondente alla pressione della sferetta.

Invece voglio dire qualcosa sulla mancanza del filo. Non ci avrei messo un centesimo, mentre la differenza tra il mouse con il filo e quallo senza si sente. Il mouse <i>wireless</i> è davvero più libero. Ci sono situazioni nelle quali il filo si fa sentire e in quei momenti il mouse senza fili vince nettamente.