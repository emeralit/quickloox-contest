---
title: "AppleScript, non è quello che sembra"
date: 2008-09-21
draft: false
tags: ["ping"]
---

Specie quando è un alias.

Per creare un alias in AppleScript basta pochissimo codice, assai autoesplicativo:

<code>tell application "Finder"
	make new alias file to nomefile at folder nomecartella with properties {name:nomealias}
end tell</code>

Ci sono numerose ragioni per voler usare AppleScript allo scopo di creare alias. Già per molti il semplice esercizio di scegliere un file, una cartella di destinazione, un nome per l'alias e lanciare lo script, in modo che crei veramente l'alias, è già una tappa importante per capire che anche noi possiamo agire nel sistema, e non solo il sistema.

Un altro esercizio: modificare lo script in modo che chieda in corsa il file di cui creare l'alias, la cartella dove metterlo e il nome dell'alias. La risposta è abbondantemente contenuta nei <em>post</em> su AppleScript già pubblicati. Hanno tutti la parola <em>AppleScript</em> nel titolo.