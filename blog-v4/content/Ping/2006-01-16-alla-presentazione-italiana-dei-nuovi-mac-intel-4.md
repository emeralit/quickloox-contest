---
title: "Alla-presentazione-italiana-dei-nuovi-mac-intel-4"
date: 2006-01-16
draft: false
tags: [ping]
---

iPhoto è una scheggia.

Su una macchina da dimostrazione, certo. Ma ho visto iPhoto di iLife ’05 funzionare su macchine pompate al massimo.

Vedere come funziona questo, con in più le nuove funzioni, significa che devo preventivare l’acquisto di iLife ’06 per mio padre e le sue diecimila diapositiva digitalizzate.
