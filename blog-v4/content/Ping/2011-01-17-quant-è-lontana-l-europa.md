---
title: "Quant'è lontana l'Europa"
date: 2011-01-17
draft: false
tags: ["ping"]
---

Senza commento.

<cite>Cedars è una piccola scuola indipendente di Greenock, Scozia, con 105 bambini dal prescuola fino alla terza media. […]</cite>

<cite>Abbiamo messo a disposizione della scuola 115 iPad, uno per studente e per membro dello <i>staff</i>. iPad sta con gli studenti per tutto il giorno e i ragazzi dai dieci anni in su possono anche portarlo a casa. Abbiamo detto ai docenti che volendo potevano avere tastiere esterne; con mia sorpresa, nessuno ha recepito l’offerta. […]</cite>

<cite>Avevo in mente un requisito preliminare: non avremmo detto a nessuno a che cosa servivano gli iPad. […] Li abbiamo semplicemente resi disponibili, senza dire ad alcuno che cosa ci avrebbe dovuto fare. È stato interessante vedere come insegnanti e studenti hanno risposto a questa libertà.</cite>

<cite>Il cambiamento maggiore è stato nell’insegnamento delle arti. App come Brushes, MoodBoard Pro e Photoshop Express aiutano i ragazzi a sperimentare e prendere confidenza senza paura di sbagliare. Gli insegnanti di inglese si trovano a dover introdurre limiti di lunghezza ai compiti perché i ragazzi stanno producendo scritti sempre più lunghi e di migliore qualità. […]</cite>

<cite>Non avevamo piani particolari di adozione di</cite> ebook<cite>, ma adesso li usiamo perché i docenti hanno voluto installare iBooks. Abbiamo dato vita a una banda musicale basata su iPad previo acquisto di poche app capaci di simulare strumenti.</cite>

<cite>Siamo alla fase in cui iPad fa parte integrante del funzionamento della scuola. […] iPad ha trasformato la nostra scuola e abbiamo cominciato solo ora a scoprire che cosa è possibile fare.</cite>

—Fraser Speirs, responsabile <i>computing</i> e It presso la Cedars School of Excellence, su <a href="http://www.macworld.com/article/157013/2011/01/ipadintheschool.html" target="_blank">Macworld.com</a>, 10 gennaio 2011

