---
title: "Un ennesimo record"
date: 2006-01-21
draft: false
tags: [ping]
---

Apple ha annunciato di avere fatturato 5,75 miliardi di dollari nel trimestre natalizio.

C’è stato un periodo in cui Apple faceva quella cifra in un anno e gli esperti prevedevano che sarebbe scomparsa. Tutto è sempre possibile, ma di questo periodo appare assai poco probabile.
