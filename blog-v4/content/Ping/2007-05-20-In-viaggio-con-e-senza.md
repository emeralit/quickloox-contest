---
title: "In viaggio con (e senza)"
date: 2007-05-20
draft: false
tags: ["ping"]
---

Era veramente molto tempo (anni) che non prendevo un treno. Questione di lavoro, cos&#236; ho avuto il privilegio della prima classe. Una sorpresa gradita vedere le prese accanto ai tavolini.

Noi abbiamo esagerato; eravamo in tre e abbiamo passato più che comodamente l'ultima ora del viaggio, ognuno sul proprio portatile, a preparare il lavoro che ci attendeva all'arrivo. Situazione inusuale, ma è bello vedere qualche passo in avanti nel settore servizi.

Niente wireless in carrozza. Ma sono le Ferrovie dello Stato. Tra cinque anni, probabilmente, entreranno nel Duemila.