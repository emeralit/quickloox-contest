---
title: "Quando il Trusted Computing era lontano"
date: 2006-03-09
draft: false
tags: ["ping"]
---

Non tutto l'archivio di Ping è stato travasato nel blog. In questi tempi di polemica sulle protezioni hardware e sul Drm mi sento di ripubblicare questo pezzettino scritto il 22 novembre 2002.

Non ho apportato la minima modifica. Tre anni e mezzo dopo, Apple ha cambiato fornitori di processore. Eppure, al massimo, aggiornerei la dizione <em>Palladium</em>, non più usata.

Tutto il resto lo rifirmo tale e quale, compresa (specialmente) la conclusione.

<em>Io speriamo che passino a Mac</em>

<em>Un domani non lontano Macintosh potrebbe diventare un bastione di libertà personale</em>

<em>Se non hai ancora sentito parlare di Palladium, meglio.</em>
<em>Quando tutti sapranno di Palladium sarà più vicino il giorno in cui i computer conterranno componenti hardware dotati di una chiave cifrata collegata al sistema operativo e accessibile solo al costruttore, o a Microsoft.</em>

<em>Per semplificare, domani un computer Palladium potrebbe dirti &ldquo;No, non puoi suonare questo Mp3 che ti sei fatto per uso personale da un Cd audio che hai regolarmente acquistato, perch&eacute; il file non è stato autorizzato. Puoi disattivare Palladium, ma al prezzo di non usare il lettore Cd&rdquo;.</em>

<em>Tuttavia restano speranze. La prima è che Palladium sia la goccia che fa traboccare il vaso e faccia capire finalmente alla gente che sta usando un prodotto inferiore; la seconda è che Apple non si è assolutamente pronunciata ma, al contrario di Ibm, Intel e altri costruttori che lo hanno fatto, non sembra incline a trattare i propri clienti in questo modo.</em>

<em>Domani Apple potrebbe produrre gli unici computer liberi esistenti al mondo. Forse è il caso di fare la cosa giusta da subito.</em>