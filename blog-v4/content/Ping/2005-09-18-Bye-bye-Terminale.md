---
title: "Bye bye Terminale"
date: 2005-09-18
draft: false
tags: ["ping"]
---

C'è di più, di meglio e di più facile

Uso poco il Terminale. Per metà è un oggetto alieno e affascinante, da esplorare; per metà è il coltellino svizzero quando si sta in campeggio. Il 96 percento rimanente della mia vita informatica si svolge altrove.

Per questo, anche quando occorre, il Terminale non lo uso più. BBEdit da tempo offre i worksheet Unix: documenti dove se scrivi un comando Unix funziona proprio come nel Terminale, ma con tutti i vantaggi del text editor. Puoi fare ricerche e sostituzioni, salvare il documento e ritrovartelo pronto la volta dopo e così via. Da un po' di tempo accumulo i comandi Unix che non ricordo, ma mi servono, in un worksheet di BBEdit. Così quando occorre apro il file ed è tutto lì.

L'ultima volta che ne ho parlato a un amico mi ha risposto <cite>ma <a href="http://www.barebones.com/products/bbedit">BBEdit</a> si paga</cite>. In un certo senso, gli ho risposto, perché con il tempo che mi fa risparmiare su questa cosa ci ho abbondantemente guadagnato. Se il tuo tempo è gratis, chiaro, è un'altra questione.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>