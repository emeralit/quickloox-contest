---
title: "Quello che le recensioni non dicono"
date: 2009-10-14
draft: false
tags: ["ping"]
---

<b>Samucito</b> chiedeva qualche giorno fa se le aziende non adottano Mac, o se succede e non se ne parla.

Per esempio, l'esercito americano ha iniziato a installare <a href="http://www.securitysystemsnews.com/?p=article&amp;id=ss200910a0EEOE" target="_blank">sistemi di videosorveglianza basati su Mac OS X</a>.

Riporto una parte dell'articolo, in cui si fa riferimento a Chris Gettings, Ceo e president di VideoNext, l'azienda che offre il prodotto.

<cite>La ragione per la quale Gettings preferisce</cite> hardware <cite>Apple è la coerenza. Ha raccontato che può ordinare due server Dell a due settimane di distanza e scoprire piccole cose, per esempio il cambiamento di un</cite> chip <cite>particolare sulla scheda logica, che nelle specifiche non compare ma può influire leggermente sulle prestazioni del software.</cite>

<cite>Con Apple</cite> sembrano le solite chiacchiere<cite>, dice, ma l'azienda lavora su ogni componente</cite> hardware<cite> che deve collaborare sulla piattaforma e ottiene come risultato l'efficienza. Cos&#236;, sostiene Gettings, può arrivare a gestire 60 videocamere su un</cite> server <cite>Apple di specifiche pari a un</cite> server <b>Dell o Hp, che però arriva a sole 50 videocamere.</b>

Sono cose che non vengono scritte molto spesso nelle recensioni.