---
title: "Da leggere e da guardare"
date: 2010-04-08
draft: false
tags: ["ping"]
---

L'ho commentato altrove e lo riporto qui: per chi fosse realmente interessato a limiti e possibilità di iPad, la cosa da leggere è la <a href="http://daringfireball.net/2010/04/the_ipad" target="_blank">recensione di John Gruber su Daring Fireball</a> e la cosa da vedere è il video sulla realizzazione di The Elements: A Visual Exploration of Every Atom in the Universe, <a href="http://periodictable.com/theelements/" target="_blank">libro</a> e ultimamente app per iPad che ha già fatto un certo rumore, tanto da finire su <a href="http://www.popsci.com/gadgets/article/2010-04/exclusive-making-elements-one-ipads-most-magical-apps" target="_blank">Popular Science</a>.