---
title: "Seconda pelle"
date: 2009-07-07
draft: false
tags: ["ping"]
---

Per il trasporto abituale del portatile ho un vecchio zaino-universo Kensington. Ci stanno dentro portatile, almentatore, cavi, dischi rigidi, quintali di carta, un <a href="http://www.leatherman.com/multi-tools/full-size-tools/default.aspx" target="_blank">multiuso Leatherman</a>, tutti gli effetti personali, i dadi per giocare di ruolo, un maiale con Led al posto degli occhi e altro di cui ho perso nozione. In sostanza, lo zaino abituale mi serve per le assenze prolungate.

Mi mancava qualcosa per le situazioni mordi e fuggi nelle quali serve solo il portatile e nient'altro. Ora che il 17&#8221; ha la nuova batteria, può anche voler dire un pomeriggio.

È stato in base a queste considerazioni che a Parigi, travolto dalle sirene consumistiche, ho acquistato uno <a href="http://www.builtny.com/showPage.php?pageID=1628" target="_blank">zaino Built</a>, esattamente quello che si vede in grande nella pagina. Devo ancora collaudarlo ma è leggerissimo, si indossa come una seconda pelle e ha perfino un'altra tasca laterale oltre a quella che ospiterà il portatile.

Saprò dire a giorni gli esiti di una prova sul campo. :)