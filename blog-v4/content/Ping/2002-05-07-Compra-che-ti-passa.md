---
title: "Compra che ti passa"
date: 2002-05-07
draft: false
tags: ["ping"]
---

L’insostenibile angoscia, per taluni, di ogni nuovo annuncio Apple

Si è svolto qualche giorno fa il raduno mondiale annuale degli sviluppatori Apple. Jobs gli ha mostrato l’anteprima di Jaguar, alias Mac OS X 10.2, e sulle mailing list Mac-dedicate si è scatenato il finimondo; decine di compratori della settimana o del mese scorso angosciati dal timore che le nuove funzioni della nuova versione rendano obsoleto il loro Mac che sentono già vecchio.
Il paradosso: c’è gente che si arrabbia perché Apple fa uscire una nuova versione di Mac OS X. L’assurdità: c’è gente che spende migliaia di euro per farsi il Mac nuovo, sapendo benissimo che da qui a sei mesi ne uscirà uno più nuovo, e compra una macchina che non risponde alle sue necessità. Deve essere così perché, se rispondesse, uno non dovrebbe preoccuparsi dell’obsolescenza, no?

<link>Lucio Bragagnolo</link>lux@mac.com