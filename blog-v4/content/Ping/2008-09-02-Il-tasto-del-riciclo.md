---
title: "Il tasto del riciclo"
date: 2008-09-02
draft: false
tags: ["ping"]
---

Finalmente una buona idea per <a href="http://www.wired.com/culture/art/multimedia/2008/09/gallery_typewriter" target="_blank">riutilizzare tutte quelle vecchie macchine per scrivere</a>.