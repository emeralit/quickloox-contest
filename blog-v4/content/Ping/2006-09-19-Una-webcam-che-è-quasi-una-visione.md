---
title: "Una webcam che è quasi una visione"
date: 2006-09-19
draft: false
tags: ["ping"]
---

L'eccezionale <strong>Alessio</strong> di Mac-Community ha pubblicato il test approfondito di una <a href="http://www.mac-community.it/AreaSupporto/HardwareZ/webcam/webcam.html" target="_blank">nuova webcam</a> da usare su Mac OS X. La camera è Usb, ma il prodotto sembra all'altezza di un uso soddisfacente.

Il giudizio agli interessati e un grosso grazie ad Alessio!