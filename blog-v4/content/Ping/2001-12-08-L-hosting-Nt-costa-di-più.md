---
title: "Quando l'host ha un maggior cost"
date: 2001-12-08
draft: false
tags: ["ping"]
---

“Il sovrapprezzo richiesto per la piattaforma Windows NT è dovuto all’elevato TCO (Total Cost of Ownership, costo totale di possesso e gestione) causato dalla sua scarsa stabilità e dalla maggiore manutenzione richiesta durante il funzionamento nonché dalla difficile scalabilità ed aggiornabilità della piattaforma.
Sconsigliamo l’uso di tale piattaforma anche a causa della sua natura fortemente proprietaria assolutamente non adatta all’uso su Internet.”

Quanto sopra si può trovare scritto, riga più riga meno, sul sito di un fornitore di hosting in cui mi sono imbattuto e di cui non farò il nome.
Sempre sicuri di voler usare proprio e per forza Windows Nt come il resto del gregge, quando ci sono sistemi operativi molto più adatti a operare su Internet, da Linux fino a Mac OS X Server?
Il fornitore, cercalo. È uno bravo.

lux@mac.com
