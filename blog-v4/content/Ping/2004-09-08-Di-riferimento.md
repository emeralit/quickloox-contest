---
title: "Di riferimento"
date: 2004-09-08
draft: false
tags: ["ping"]
---

Buone notizie per tutti noi che amiamo leggere

Non posso fare nomi, ma un editore librario di importanza nazionale ha dichiarato in una riunione di vertice <cite>vogliamo diventare gli editori di riferimento del mondo Mac.</cite>

Vuol dire due cose: primo, Macworld resta di gran lunga la mia rivista preferita ma saprò dove andare a prendere un libro che mi serve; secondo, le scelte Mac a mia disposizione aumenteranno.

Sono notizie che fanno piacere.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>