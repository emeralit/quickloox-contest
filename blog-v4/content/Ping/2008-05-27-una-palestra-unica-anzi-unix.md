---
title: "Una palestra unica. Anzi, Unix"
date: 2008-05-27
draft: false
tags: ["ping"]
---

Installo un ennesimo programma su iPhone e incontro uno strano messaggio di scarsità di spazio su disco. Strano, ci sono quasi quattro giga di musica e dovrebbero essercene altrettanti di spazio vuoto.

Indago e scopro che iPhone è partizionato. Il sistema operativo sta in una partizione ridotta all'essenziale e il resto è a disposizione dei dati.

Sempre indagando, scopro una soluzione: spostare i programmi di troppo nella partizione dati e creare un alias ai programmi dentro la cartella standard, in modo che il software di installazione pensi che i programmi siano l&#236; (invece sono altrove e la partizione del sistema operativo ritorna ad avere abbastanza spazio).

Mac OS X ha una interfaccia grafica per creare un alias, ma iPhone proprio no, e chiaramente digitare comandi Unix sulla tastiera virtuale del cellulare non è come farlo su quella fisica del Mac.

Cos&#236; abilito il server ssh su iPhone, apro la connessione dal Terminale del Mac e opero:

- <code>mkdir</code> <directory> per creare la cartella nella quale sposterò i programmi. Questo comando serve una sola volta per tutte;
- <code>cp -pr</code> <partenza destinazione> per copiare il programma nella nuova directory;
- <code>mv</code> programma.app programma.old per cambiare nome alla copia del programma dentro la directory di partenza.

Su un Mac la struttura dei permessi avrebbe consentito di risparmiare sui passaggi e dare un singolo comando mv, ma non su iPhone.

Infine cancello il programma vecchio con <code>rm</code> e stabilisco l'alias con <code>ln -s</code>.

Sto spostando un programma alla volta, e non usando utility che promettono di fare tutto in un colpo solo, perché immagino che non tutti sopportino di lavorare da una directory che non è quella che si aspettano (dipende da come sono scritti).

La fusione tra Mac e iPhone dà vita a uno strumento didattico eccellente. Per manipolare iPhone in modo potente fa comodo imparare quei comandi Unix che su Mac sono (fortunatamente) inutili. Mac consente di agire su iPhone in modo potente grazie a una tastiera vera e al Terminale.

Si possono anche fare esperimenti abbastanza a piacere. Mettere fuori uso iPhone non è come mettere fuori uso il Mac e rimetterlo in funzione è molto più semplice, veloce e indolore.

Praticamente iPhone è una comoda palestra Unix, oltre che un nanocomputer spettacolare. E voglio vedere quale altro cellulare offra vantaggi equivalenti.