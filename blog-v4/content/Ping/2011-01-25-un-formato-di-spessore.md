---
title: "Un formato di spessore"
date: 2011-01-25
draft: false
tags: ["ping"]
---

Un amico mi confida:

<cite>MacBook Air è cos&#236; sottile che mia moglie lo ha perso in casa.</cite>

Il giorno che uscisse un'edizione 17&#8221; di MacBook Air potrei fare pazzie.

