---
title: "Lezione di creatività"
date: 2007-05-25
draft: false
tags: ["ping"]
---

Ho avuto l'occasione di passare una grande quantità di tempo di fianco a un creativo che scriveva testi creativi in Word.

Il compito non prevedeva nessuna formattazione. Il testo da consegnare poteva essere Unicode puro, senza neanche i grassetti e i corsivi. Io lo avrei scritto con BBEdit o TextWrangler, per dire.

Bene, anzi, male. Word si intromette con la sua interfaccia ingombrante, invasiva e ridondante. Il creativo ha dedicato almeno il venti percento del tempo a cliccare su comandi di formattazione dei quali non aveva alcun bisogno. Il suo tempo creativo è andato per l'80 percento in creatività e per il 20 in formattazione, ossia è andato perso.

Mi si dirà che magari lui cos&#236; lavora meglio. È proprio questo il problema! Word lo ha abituato a lavorare cos&#236;. A sprecare il venti percento del suo tempo creativo.

La dizione di word processor è fuorviante. Word è un word formatter. La creatività è un'altra cosa. Stai lontano da quell'affare, se vuoi veramente valorizzare il tuo talento.