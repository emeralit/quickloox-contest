---
title: "L'occhio del padrone ingrassa iDisk"
date: 2009-01-20
draft: false
tags: ["ping"]
---

Anche stavolta il tentativo di copiare un file da quattro gigabyte su iDisk via Finder è fallito. La connessione è venuta a mancare per qualche secondo ed è stato sufficiente a mandare in fumo l'operazione.

Ne concludo che con queste quantità di dati, per ora, si lavora sopra le possibilità offerte realmente dalla tecnologia. O si procede in modo più astuto, con sistemi che permettono di riprendere una copia da dove si era interrotta, o si usano protocolli diversi da quello del Finder, o si fa a meno.