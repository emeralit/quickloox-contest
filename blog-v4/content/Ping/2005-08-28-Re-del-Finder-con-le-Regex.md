---
title: "Re del Finder con le regex"
date: 2005-08-28
draft: false
tags: ["ping"]
---

Dove l'interfaccia grafica non arriva, supplisce AppleScript

Il Terminale è… interminabile per quanto lo si può si può esplorare e rimanerne affascinati. Ma il Mac è fatto per essere facile e non c'è nessun bisogno di Terminale per usarlo alla grandissima.

Eppure taluni sostenitori del Terminale ti dicono cose come <em>prova a cancellare da quella finestra i soli file che contengono la data 1995 in un solo comando!</em> e cominciano a digitare roba incomprensibile.

Stanno usando le cosiddette espressioni regolari, o regex, roba complicata ma di potenza estrema. Beh, basta un <a href="http://www.red-sweater.com/blog/?p=26">AppleScript</a> per poter selezionare file nel Finder basandosi su espressioni regolari.

Certo bisogna sapere come funzionano le <a href="http://fido.altervista.org/RegExp/regex.html">regex</a>. Ma non è più questione di Terminale.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>