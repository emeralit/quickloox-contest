---
title: "Sapere è potere"
date: 2007-02-14
draft: false
tags: ["ping"]
---

L'ho già detto che il Massachusetts Institute of Technology mette a disposizione gratuitamente e senza iscrizioni <a href="http://ocw.mit.edu/index.html" target="_blank">materiali dei propri corsi</a> universitari e che non si prende una laurea studiando su quei materiali, ma la valgono comunque?

E poi trovi la gente che si preoccupa di Internet perché ci sono i siti porno. Beh, non solo.