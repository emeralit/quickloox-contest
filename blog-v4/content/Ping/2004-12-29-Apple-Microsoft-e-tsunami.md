---
title: "Tsunami e festicciole"
date: 2004-12-29
draft: false
tags: ["ping"]
---

Piccole differenze tra grandi siti

Sarà ipocrita e cattivo, troppo facile, una stupida questione di stile, ma è un fatto e i fatti restano, anche nel mondo virtuale. Scrivo questo Ping! alle 18 del 29 dicembre 2004.

Apple.com titola all'incirca <em>I nostri cuori sono con le persone colpite dagli tsunami dell'Oceano Indiano</em>. Ci sono link a Croce Rossa, Unicef e altro.

Microsoft.com titola all'incirca <em>Schiaccia lo spam</em> e <em>Dà inizio alla tua festa con Windows XP Media Center Edition</em>. Ci sono link a prodotti Microsoft e nient'altro.

Tutto molto facile e molto ipocrita, ma io preferisco comunque avere un Mac.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>