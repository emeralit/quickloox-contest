---
title: "Uno styled editor per me"
date: 2008-04-19
draft: false
tags: ["ping"]
---

Dopo molti anni, sto pensando a un sostituto per <a href="http://www.tex-edit.com" target="_blank">Tex-Edit Plus</a>.

Il programma non si aggiorna da luglio. In sé non sarebbe niente, tuttavia ho l'impressione che si stiano perdendo per strada le possibilità offerte da Leopard e in prospettiva quelle delle prossime versioni di sistema.

Soprattutto la comunità dello sviluppo AppleScript per Tex-Edit Plus langue nettamente. L'ultima aggiunta al deposito di script risale al 2005.

Detto ciò, Tex-Edit Plus mi offre quattro vantaggi che tuttora sono decisivi per gran parte del mio lavoro:

<ul compact="compact" type="circle">
	<li>Possibilità di lavorare liberamente con gli attributi di testo</li>
	<li>Conteggio dei caratteri in tempo reale e sempre visibile</li>
	<li>Scriptabilità totale</li>
	<li>Espressioni regolari</li>
</ul>

Sono disposto a prendere in considerazione la sostituzione di Tex-Edit Plus con qualcos'altro, purché valgano queste quattro premesse.

E che sia un editor di testo; di word processor è pieno il mondo, come il mio desktop, e non sono l'oggetto della discussione.

Se hai consigli da darmi, li accetto molto volentieri. Grazie in anticipo. :)