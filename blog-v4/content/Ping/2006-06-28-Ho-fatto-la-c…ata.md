---
title: "Ho fatto la c&#8230;ata"
date: 2006-06-28
draft: false
tags: ["ping"]
---

Causa manovra distratta e stupida ho perso un commento, inviato da Kastainer o Karstainer (me ne sono accorto nella frazione di secondo in cui era già troppo tardi per tornare indietro e ho a malapena visto il nickname).

Mi scuso molto. Non posso promettere che non succederà più, ma prometto di impegnarmi al massimo perché accada il meno possibile.