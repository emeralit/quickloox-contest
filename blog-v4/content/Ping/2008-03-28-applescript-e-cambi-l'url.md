---
title: "AppleScript. E cambi l'Url"
date: 2008-03-28
draft: false
tags: ["ping"]
---

Non tutti sanno che certi video di YouTube sono visibili in qualità più alta di quella standard, se solo si aggiunge <code>&#38;fmt=18</code> in coda all'indirizzo del video stesso.

Certo che aggiungerlo a mano è una noia pazzesca. AppleScript può venire in aiuto.

Innanzitutto apriamo un dialogo con Safari:

<code>tell application "Safari"</code>

Inseriamo in una variabile l'indirizzo caricato sulla finestra di Safari in primo piano:

<code>set current_url to the URL of current tab of window 1</code>

Adesso cambiamo il contenuto della variabile, che diventa uguale all'Url di prima, più la sequenza di cui sopra:

<code>set current_url to current_url &#38; "&#38;fmt=18"</code>

Fino qui tutto bene, solo che l'Url adattato sta nella memoria di Script Editor e non aiuta fino a che non viene caricato in Safari. Carichiamolo allora:

<code>set URL of current tab of window 1 to current_url</code>

(notare la distinzione tra <code>current tab of window 1</code>, il vero Url presente dentro Safari, e <code>current_url</code>, un contenitore che serve a eseguire i maneggi necessari).

A lavoro compiuto, si chiude il dialogo con Safari e si completa lo script:

<code>end tell</code>

Questo lo script completo:

<code>tell application "Safari"</code>
<code>	set current_url to the URL of current tab of window 1</code>
<code>	set current_url to current_url &#38; "&#38;fmt=18"</code>
<code>	set URL of current tab of window 1 to current_url</code>
<code>end tell</code>

Questo script arriva da <a href="http://www.macosxhints.com/article.php?story=20080316111042524" target="_blank">Macosxhints.com</a>, dove si trovano anche una versione dello stesso script per OmniWeb (da confrontare&#8230; si impara un sacco) e un sistema molto più rapido e sintetico per ottenere lo stesso risultato via JavaScript.

Qui naturalmente interessava il valore didattico del lavoro con AppleScript. L'ottimizzazione sfrenata lasciamola ai programmatori veri.

I Ping dedicati ad AppleScript continuano, in programma nei giorni del mese divisibili per 7 (7, 14, 21, 28).