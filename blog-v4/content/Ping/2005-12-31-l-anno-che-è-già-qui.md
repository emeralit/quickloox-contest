---
title: "L'anno che è già qui"
date: 2005-12-31
draft: false
tags: ["ping"]
---

Niente auguri, ma qualche constatazione ottimista

Da anni e anni ho cessato di spendere tempo e soldi in antivirus, dal momento che non ne ho bisogno.

È ormai molto tempo che ho comprato SpamSieve e di posta indesiderata ne vedo pochissima, in percentuale ridicola rispetto al totale.

Con l'arrivo di OpenOffice (e Keynote, che considero un gentile omaggio per l'acquisto di Pages) ho potuto finalmente buttare alle ortiche anche l'ultimo programma Microsoft che infettava i miei dischi (per la cronaca era PowerPoint Player).

Ogni fine settimana il computer effettua automaticamente un backup delle mie cose essenziali. Ogni giorno ne faccio uno del solo lavoro in corso, nel giro di pochi minuti.

Grazie a Gmail ho alcune caselle di posta elettronica che,quando ho finito di ritirare la posta stessa, sono più grandi di quando avevo cominciato.

A casa, e in ufficio, sono collegato 24 ore su 24. In qualunque altro posto mi trovi, se il cellulare ha campo, mi posso collegare.

Mac OS non è mai stato così stabile e continua a migliorare. I PowerBook non sono mai stati migliori.

L'offerta di programmi open source sta diventando veramente ricca e di buona qualità.

Volere auguri per il 2006 è veramente esagerare. Informaticamente parlando, mai stato meglio di così. Spero valga per tutti.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>