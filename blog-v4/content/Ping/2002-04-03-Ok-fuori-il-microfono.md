---
title: "Ok, fuori il microfono"
date: 2002-04-03
draft: false
tags: ["ping"]
---

La funzione che non ti aspetti potrebbe essere disponibile, se...

Apple non ne approfitta e neanche lo dice, ma iPod, volendo, può funzionare come encoder di Mp3, ovvero creare file Mp3 a partire da un file Aiff originale come lo troveremmo su un normale Cd audio.
Perché sia veramente possibile farlo, mancano però un aggiornamento del software di iPod - per dare un’interfaccia utente alla funzione - e un microfono FireWire.
Speriamo che qualcuno si metta al lavoro.

<link>Lucio Bragagnolo</link>lux@mac.com