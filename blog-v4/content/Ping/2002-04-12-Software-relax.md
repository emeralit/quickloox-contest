---
title: "Software relax"
date: 2002-04-12
draft: false
tags: ["ping"]
---

Un criterio di valutazione del programma giusto che non si vede mai sulle riviste

Ho avuto la folgorazione in chat con Erica.
Dei programmi si valuta tutto tranne una cosa: la capacità di rilassare e di mettere a proprio agio l’utilizzatore.
Me lo diceva, senza saperlo, un mio amico che fa molto Html: “Quando uso BBEdit sono tranquillo e rilassato, perché fa quello che mi aspetto, funziona bene, ha le funzioni giuste nel modo giusto. Quando uso DreamWeaver, per carità, ottimo, ma sono sempre sul chi vive perché non sai che cosa può succedere”.
Verissimo. BBEdit rilassa, DreamWeaver stressa.
Potrebbe essere un bel gioco estendere il criterio agli altri programmi che conosciamo: Tex-Edit Plus contro AppleWorks, Mariner contro Excel, Mozilla contro Netscape, Mailsmith contro Eudora... qual è il miglior programma per usare Mac e rilassarsi?

<link>Lucio Bragagnolo</link>lux@mac.com