---
title: "Siccome non durerà"
date: 2010-10-22
draft: false
tags: ["ping"]
---

Asymco ha prodotto un <a href="http://www.asymco.com/2010/10/19/60-percent-of-apples-sales-are-from-products-that-did-not-exist-three-years-ago/" target="_blank">grafico impressionante</a> dell'andamento di Apple.

Bisogna tenere conto del fatto che il trimestre estivo, che ha fatto sfondare i venti miliardi di dollari di fatturato, non è il trimestre vacanziero, quello più forte dell'anno (la variazione stagionale è chiarissima nel grafico).

È altrettanto chiaro che neanche un successo interplanetario permetterà ad Apple di superare certi limiti di crescita. A un certo punto qualche statistica si livellerà, qualche curva si appiattirà, qualche prodotto risentirà della concorrenza o riscuoterà meno favore dal mercato.

In quel momento sarà d'uopo ricordarsi dell'oggi. Nessuna azienda, in nessun campo, in nessun modo, è riuscita a fare quello che ha fatto Apple dal 2001 al 2010. Quanto è tornato Steve Jobs nel 1997, il piano era consentire la sopravvivenza di Apple con sei miliardi l'anno.

Oggi nel trimestre estivo se ne sono incassati venti e l'azienda ha cinquanta miliardi di dollari nel cassetto. Come dire che se domani tutte le vendite crollassero istantaneamente a zero Apple potrebbe tranquillamente tirare avanti con i risparmi fino al 2020 o giù di l&#236;.