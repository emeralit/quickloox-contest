---
title: "Avanti al prossimo"
date: 2008-03-11
draft: false
tags: ["ping"]
---

Riferisce <a href="http://web.mac.com/brethil/Brethils_Page/Thoughts/Thoughts.html" target="_blank">Brethil</a> che il problema relativo all'uso dei servizi di Microsoft Messenger con un indirizzo @mac.com <a href="http://discussions.apple.com/thread.jspa?messageID=6783445#6783445" target="_blank">è stato risolto</a>.

Evidentemente il supporto Microsoft ha ricevuto un numero sufficiente di lamentele.

Ora non ci riproveranno più. Per un po'.