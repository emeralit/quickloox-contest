---
title: "Emulate, emulate, qualcosa resterà"
date: 2009-10-27
draft: false
tags: ["ping"]
---

Ho appena scoperto <a href="http://www.emaculation.com/doku.php/mac_emulation" target="_blank">E-Maculation</a>, sito dedito a spiegare come emulare i vecchi Macintosh, da Classic a System 6, sui nuovi sistemi, da Windows a Snow Leopard.

Niente che non si trovi facendo ricerche a tappeto su Internet, in dozzine di siti diversi, mettendo insieme informazioni a volte contrastanti.

Solo un'unico punto di riferimento con tutti i <i>link</i> giusti. Che non è poco, per chi ha bisogno o si diverte a tenere in vita certe applicazioni storiche, da HyperCard in poi.