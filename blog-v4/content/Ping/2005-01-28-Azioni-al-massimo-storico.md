---
title: "Il lavoro degli altri"
date: 2005-01-28
draft: false
tags: ["ping"]
---

Della serie: fortuna che ci sono io a pensarci

Sono tutti i bravi a raccontare che cosa presenterà Apple al Macworld Expo e a guardare il codice Html delle pagine del sito Apple in cerca di indicazioni dell'arrivo dei PowerBook G5.

Nel frattempo le azioni Apple sono arrivate al massimo storico di tutti i tempi, a quota 72,64 dollari. Oggi.

Guardo Macity, guardo Tevac, guardo Applicando, guardo… Macworld. Non lo dice nessuno. Non sarà una grande notizia, ma diamine, si occupa spazio per cose ben più piccole.

Se devo mettermi anche a dare le news dimmelo, che mi organizzo. Parafrasando Bennato, <em>io più di tanto non posso fare</em>.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>