---
title: "Giochi che hanno rifatto la storia"
date: 2008-05-26
draft: false
tags: ["ping"]
---

Per celebrare i sedici anni di Wolfenstein 3D, qualcuno ha <a href="http://www.mocpages.com/moc.php/17215" target="_blank">rifatto la scena finale</a> con il&#8230; Lego.

Mi è tornato in mente il 1982. Terminato il lavoro ci fermavamo, un mio collega e io, e giocavamo a Castle Wolfenstein su un Apple II. Niente tre dimensioni, invece <a href="http://en.wikipedia.org/wiki/Castle_Wolfenstein" target="_blank">stanze a quadratoni</a> e processore che nello sforzo di riuscire a produrre sonoro decente dava un retrogusto di elettricità statica al secco tedesco delle guardie.

Si giocava via tastiera e c'erano comandi a sufficienza per stare alla tastiera in due, uno che badava al movimento e uno alle armi.

Una sera c'era Italia-Brasile del Mondiale e giocavamo nella redazione deserta, intorno un silenzio surreale. Non vedemmo la partita ma Castle Wolfenstein, però ritornando a casa sapevamo perfettamente il risultato: analizzando i boati dai palazzi vicini si capiva chi aveva segnato e riuscimmo anche a decifrare il gol annullato all'Italia.

Arriveranno gli Europei e io sarò a intrufolare il mio irregolare elfo in qualche fortezza di World of Warcraft. Il passato è stato grandioso, ma di progresso ce n'è stato.