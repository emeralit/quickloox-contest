---
title: "A volte ritornano"
date: 2002-03-13
draft: false
tags: ["ping"]
---

Un vecchio virus può fare nuovi danni, anche su Mac

C’era una volta il virus AutoStart 9805, che attaccava i Mac per mezzo della funzione di autoplay di QuickTime. Per evitarlo basta tenere l’autoplay di QuickTime spento (Mac OS 9; Mac OS X non ha autoplay di QuickTime). Ma il virus è ugualmente tornato, sfruttando una combinazione tra Mac OS X e Classic.
Infatti è stato
<link>mostrato</link>http://homepage.mac.com/vm_converter/mac_autoexec_vuln.. html come si possa usare una pagina Web per provocare lo scaricamento automatico di una immagine disco di Mac OS X e, in presenza di StuffIt Expander nativo, il montaggio automatico dell’immagine sul dektop.
Ecco: se è attivo Classic, se l’autoplay QuickTime di Classic è attivato e se l’immagine disco contiene un virus pronto a partire, l’attacco può avere successo.
Difficile (troppe variabili in gioco), ma non impossibile, con milioni di Mac sul mercato e infinite configurazioni.

<link>Lucio Bragagnolo</link>lux@mac.com