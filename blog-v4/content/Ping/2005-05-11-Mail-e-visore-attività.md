---
title: "Perché cambiare Mail?"
date: 2005-05-11
draft: false
tags: ["ping"]
---

Tiger ha portato grosse novità nella posta elettronica via Mac. Non sempre gradite

Mi scrive Stefano:

<cite>Niente da dire su tiger, ogni giorno una scoperta.</cite>

<cite>Ma perché cambiare le cose quando funzionano? in Mail il Visore attività era integrato nella finestra principale. oggi devi aprire Visore attività per valutare a che punto è il tuo invio. Tutto bene, ma è una finestra in più da gestire.</cite>

Se c'è una cosa che non seguo di Mac OS X è esattamente Mail. Vivo felice con Malismith di Bare Bones da quasi dieci anni e non penso di potere avere di più. Quindi, se ci sono segnalazioni ulteriori e commenti interessanti su Mail, ben vengano.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>