---
title: "Nuova chat, nuova vita"
date: 2007-03-28
draft: false
tags: ["ping"]
---

Dopo non eccessiva insistenza, <strong>Paolo</strong> mi ha convinto ad abbandonare Proteus e passare ad Adium.

Usavo Proteus da molti, molti anni e si è sempre comportato benissimo, anche se cominciava a sentire i segni dell'età come l'impossibilità pratica di effettuare trasferimenti di file. Nato come applicazione a pagamento, non viene più supportato da poco dopo l'uscita di Mac OS X.

Installare <a href="http://www.adiumx.com/" target="_blank">Adium</a> è stato semplice e il cambio si è compiuto in pochi minuti. Effettivamente è più fresco e certe possibilità, per esempio i plugin, la cifratura della chat e AppleScript, sono cose che proprio a Proteus mancano.

Non ho cancellato Proteus. Mi ha fatto compagnia per tanti anni e mi ha fatto parlare con mezzo mondo.

Non è detto neanche che sia abbandonato per sempre. Il codice sorgente è diventato open source e pare stia nascendo un <a href="http://www.proteusapps.com/blog/" target="_blank">team di sviluppatori</a> che se ne vuole occupare.

Forse Proteus vivrà. Non dovrà cambiare nome in Phoenix. :-)