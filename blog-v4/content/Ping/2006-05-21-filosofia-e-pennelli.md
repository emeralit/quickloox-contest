---
title: "Filosofia e pennelli"
date: 2006-05-21
draft: false
tags: ["ping"]
---

Sempre sul gioco di ruolo, sto aggiornando turno per turno una mappa da mostrare ai giocatori (in questi giorni si sta giocando per posta, in attesa dell'incontro prefissato) e sto dandomi da fare con <a href="http://gimp-app.sourceforge.net/" target="_blank">Gimp</a>.

&Egrave; incredibile come sia alieno nel modo di ragionare, ma assai efficiente una volta che lo si capisce. E, quando non capisco come fare una cosa, invariabilmente sono io che non l'ho ancora scoperto. Lui, di sicuro, la può fare.