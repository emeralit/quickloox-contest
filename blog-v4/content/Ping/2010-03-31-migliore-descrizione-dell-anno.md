---
title: "Migliore descrizione dell'anno"
date: 2010-03-31
draft: false
tags: ["ping"]
---

Anche se siamo in primavera. Autore <a href="http://www.accomazzi.it" target="_blank">Misterakko</a>, nel descrivere l'<a href="http://blogs.zdnet.com/security/?p=5602" target="_blank">ennesimo problema di sicurezza per Windows</a> (solo Windows), causato da un&#8230; caricabatterie:

<cite>Grande! Compri questo caricabatterie Usb, lo attacchi al Pc, carichi le pile e il tuo Pc diventa uno</cite> zombie.