---
title: "Due posti per ogni software"
date: 2005-08-27
draft: false
tags: ["ping"]
---

E ogni software, chiaro, al suo posto

Due piccole segnalazioni in un agosto che parlando di Mac resta sempre un po' sonnacchioso (e va benissimo così): la prima è <a href="http://www.freemacware.com">FreeMacWare</a>, sito dedicato al software completamente gratis per Mac. Ce n'è più di quanto si immagini ed è meglio di quello che si può pensare.

La seconda è la pagina di <a href="http://www.osx86.theplaceforitall.com/software/">OS X x86 (Intel)</a> dedicata al software già pronto per funzionare sui Mac con processore Intel che usciranno l'anno prossimo. L'elenco cresce in continuazione e si arricchisce di nomi sempre più importanti. Per essere in anticipo di un anno sulle prime necessità non è poco.

All'indomani dell'annuncio della Terza Transizione, qualcuno ebbe il coraggio di scrivere che se Apple non faceva uscire subito hardware Intel era per la mancanza di software pronto a girarci sopra. I programmi stanno uscendo dodici mesi prima delle macchine. E non c'è altro da dire.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>