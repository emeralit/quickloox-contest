---
title: "Avanti e retro"
date: 2007-06-20
draft: false
tags: ["ping"]
---

A parte una gradita visita di <a href="http://www.deathlord.it/court/" target="_blank">Carlo</a>, venerd&#236; mi sono rifatto un giretto in autonomia in <a href="http://www.leu.it" target="_blank">Lumen et Umbra</a>. Non ho mai approfondito il gioco; ci vogliono impegno e tempo paragonabili a quelli per <a href="http://www.wow-europe.com" target="_blank">World of Warcraft</a> o <a href="http://angband.rogueforge.net" target="_blank">Angband</a>, quindi diventerò grande in LeU in una prossima vita. Però lo raccomando a chi apprezza il genere. Come ha potuto vedere Carlo, all'interno di LeU c'è una comunità viva e pulsante. Il genere è retro, ma il fascino è senza tempo. E il gioco in compagnia, attraverso la rete, piaccia o meno, è il futuro.