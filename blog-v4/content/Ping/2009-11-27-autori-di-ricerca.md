---
title: "Autori di ricerca"
date: 2009-11-27
draft: false
tags: ["ping"]
---

Tempo fa <b>mAx</b> mi ha scritto:

<cite>La mia amica R., che di lavoro fa la traduttrice, mi dice che i programmi che lei e i suoi colleghi sono abituati ad usare girano solo su Window e sarebbe interessata al mondo Mac, ma si trova nell'impossibilità di cambiare. La cosa mi pare molto strana.</cite>

Due cose giuste: considerare Mac e trovare strano che manchino programmi adatti a traduttori professionisti. Una cosa migliorabile: pensare che i programmi esistenti per Windows siano gli unici programmi esistenti.

Nella fattispecie, la questione era principalmente disporre di programmi capaci di gestire i cosiddetti <i>corpus</i>, in lingua volgare archivi di vocaboli e frasi già tradotte, che permettono di procedere spediti senza ridigitare ogni volta quello che si è già fatto.

Senza entrare nel merito della qualità dei singoli programmi, che per un professionista vanno provati e straprovati prima di dare giudizi, mi sono concentrato sulla mera esistenza dei programmi, per passare qualche indicazione all'amica di mAx.

Ne sono usciti <a href="http://www.babylon.com/mac/" target="_blank">Babylon</a>, <a href="http://www.cafetran.republika.pl/index.html" target="_blank">Cafetran</a>, 
<a href="http://www.heartsome.net/EN/home.html" target="_blank">Heartsome Translation Studio</a>, <a href="http://www.omegat.org/en/omegat.html" target="_blank">OmegaT</a> e <a href="http://www.wordfast.com/index.html" target="_blank">Wordfast</a>, tra quelli più probabilmente utilizzabili.

<a href="http://www.maxprograms.com/" target="_blank">Maxprograms</a> e <a href="http://www.morphologic.hu/en/" target="_blank">MorphoLogic</a> offrono linee di prodotti utili, tutti compatibili Mac, seppure a una prima occhiata meno allettanti di quelli già elencati.

<a href="http://anaphraseus.sourceforge.net/" target="_blank">Anaphraseus</a> è un <i>plugin</i> che introduce le funzioni richieste in OpenOffice.org e potrebbe essere di interesse per quanti già sono legati al software libero.

Poi ci sono vari programmi potenzialmente interessanti e che ho messo in secondo piano, principalmente per le competenze tecniche che richiedono. Tuttavia in un ambito di persone con la giusta padronanza degli strumenti possono essere soluzioni: 

<a href="http://www.apertium.org/" target="_blank">Apertium</a>, <a href="http://www.cunei.org/" target="_blank">Cunei</a>, <a href="http://www.localeminder.com" target="_blank">LocaleMinder</a>, <a href="http://ilk.uvt.nl/mbmt/" target="_blank">Mbmt</a>, <a href="http://www.statmt.org/moses/" target="_blank">Moses</a>, <a href="https://open-language-tools.dev.java.net/" target="_blank">open-language-tools</a>, <a href="http://logos-os.dfki.de/#mozTocId323314" target="_blank">OpenLogos</a>,  <a href="http://translate.sourceforge.net/wiki/" target="_blank">Translate Toolkit &#38; Pootle</a> e <a href="http://transolution.python-hosting.com/" target="_blank">Transolution</a>.

Ho escluso a priori, per ragioni tecniche o di specifiche di prodotto, <a href="http://freshmeat.net/projects/gnutran" target="_blank">gnutran</a> e <a href="http://traduki.sourceforge.net/" target="_blank">Traduki</a>. Però esistono e forse un criterio di giudizio differente li includerebbe nella scelta.

Infine ho segnalato un caso particolare: Apple ha sviluppato internamente alcuni <a href="http://developer.apple.com/internationalization/localization/tools.html" target="_blank">strumenti per la localizzazione del software</a> che prevedono anche la lavorazione di <i>corpus</i>. Un traduttore non ci lavora. Però arrivasse domani la commessa di una localizzazione software, un pensiero ci starebbe.

La morale: non so se R. avrà convenienza a cambiare. Bisogna vedere che cosa usa e che cosa potrebbe usare, se ha specifiche particolari, che tipo di lavori svolge più frequentemente eccetera eccetera. Cambiare non è automaticamente conveniente. Certo non è impossibile come lascerebbe pensare la banale constatazione dell'inesistenza su Mac del programma cui si è abituati.

Nell'epoca dei motori di ricerca, a volte basta sporcarsi un po' le mani e la libertà di scelta, bene impagabile, decolla.