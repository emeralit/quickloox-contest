---
title: "Sesso e giochi"
date: 2005-03-28
draft: false
tags: ["ping"]
---

Raccomandazione banale ma essenziale per chi vuole giocare, non solo su Mac

È un'esperienza che per chi non è abituato può anche diventare impegnativa, ma ti esorto di cuore a dedicare il tuo tempo di gioco a cose che siano multiplayer.

Da solo ci si diverte, ma non è la stessa cosa che in compagnia.

Il campo di quello che è possibile fare è sterminato, dall'open source ai mondi a pagamento, dalla grafica fantastica al solo testo, dal tempo reale ai turni, dal fantasy al fantascientifico… non c'è gusto che non possa essere accontentato.

Se non ti piace giocare è un'altra questione. Ma se ti piace, gioca online. Per posta, se necessario. Si può fare anche così!

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>