---
title: "Non siamo più indiani"
date: 2003-05-10
draft: false
tags: ["ping"]
---

I vantaggi di un sistema operativo su cui è di casa Apache

Sono a Webbit 2003 per assistere l’amico Gianugo, che deve tenere un workshop su Cocoon, un complesso e potentissimo sistema di gestione di contenuti open source.

Gianugo è un sistemista esperto, profondo conoscitore delle tematiche di sicurezza informatica e anche sviluppatore. Dentro un sistema Unix si trova come al luna park e vive nella shell: fa tutto, posta, web, chat, database, scrittura, presentazioni, da dentro una sessione di terminale.

Non ha mai considerato il Mac, perché lui vive di Unix. Fino a quando non è uscito Mac OS X.

“Vedi”, mi dice mentre ci avviamo verso l’aula, “sono stato all’ultima ApacheCon e tutti quelli della comunità Apache ormai usano Mac”.

Cinque anni fa non lo usava nessuno e, dal punto di vista degli sviluppatori di software libero, Mac era una riserva indiana. Ora non è più così: Mac OS X sta crescendo e conquistando nuovi spazi.

<link>Lucio Bragagnolo</link>lux@mac.com