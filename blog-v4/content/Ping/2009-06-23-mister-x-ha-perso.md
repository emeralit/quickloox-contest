---
title: "Mister X ha perso"
date: 2009-06-23
draft: false
tags: ["ping"]
---

Mi riferisco al gioco da tavolo <a href="http://en.wikipedia.org/wiki/Scotland_Yard_(board_game)" target="_blank">Scotland Yard</a>, nel quale Mister X fugge attraverso Londra e cerca di fare perdere le proprie tracce alle forze dell'ordine.

<b>Federico</b> mi ha passato il <i>link</i> alla prima cronistoria di <a href="http://happywaffle.livejournal.com/5890.html" target="_blank">iPhone rubato e poi ritrovato</a> tramite il servizio Find My iPhone di iPhone OS 3.0. L'adrenalina è la stessa. E Mister X non ce l'ha fatta.