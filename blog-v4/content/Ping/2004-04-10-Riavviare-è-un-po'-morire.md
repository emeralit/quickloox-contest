---
title: "Riavviare è un po’ morire"
date: 2004-04-10
draft: false
tags: ["ping"]
---

Con Mac OS X la stabilità è diventata un valore vero

Amo gli update di Mac OS X. Finora hanno fatto solo guadagnare in termini di velocità. Le opzioni a disposizione hanno continuato a crescere. Ogni mese circa arriva un update di sicurezza, ben diversamente da quello che accade sull’altra sponda, dove certi bug critici sono rimasti scoperti per sei mesi e oltre. Si può continuare a lavorare intanto che gli update vengono scaricati e installati e in generale ne vale sempre la pena.

Odio gli update. Quando vanno a toccare il kernel (quasi sempre) costringono a riavviare, magari dopo tre settimane o più di funzionamento ininterrotto. Su Mac OS classico il riavviare ogni tanto capitava ed era un fatto della vita. Con Mac OS X coglie quasi di sorpresa, tanto uno ci aveva perso l’abitudine.

Meglio gli update, intendiamoci. Ma riavviare, da Mac OS X, è un po’ morire.

<link>Lucio Bragagnolo</link>lux@mac.com