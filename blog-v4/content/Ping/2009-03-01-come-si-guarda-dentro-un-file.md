---
title: "Come si guarda dentro un file"
date: 2009-03-01
draft: false
tags: ["ping"]
---

iWork '08 non crea file, ma cartelle. Il file .pages, .numbers, .keynote, in realtà è una cartella. Un clic destro, o Ctrl-clic, per scegliere Mostra contenuti pacchetto, mostra il trucco.

MacOSXHints ha appena spiegato che <a href="http://www.macosxhints.com/article.php?story=20090225034801527" target="_blank">su iWork '09 è diverso</a>. Il file è sempre una cartella; ma questa viene trasformata in un file unico con il programma Unix <code>tar</code> e successivamente all'estensione .tar viene sostituita l'estensione .pages, .numbers, keynote.

Sostanzialmente, il file è un file .tar che ha un'altra estensione. Se si sostituisce all'estensione .pages (poniamo) l'estensione .tar, il file diventa un normale file tar e un doppio clic ne svela tutti i contenuti.

Quanto sopra è utile perché a volte, magari in emergenza, sorge il bisogno di recuperare qualcosa presente in un documento iWork ma non è possibile aprirlo regolarmente.

La cosa da ricordare è che non si torna indietro. Se riapplichiamo <code>tar</code> alla cartella ottenuta e rimettiamo l'estensione originale al file ottenuto, iWork non lo riconoscerà. Dunque è imperativo lavorare su una copia del file e non sull'originale.