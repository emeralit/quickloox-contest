---
title: "Alla-presentazione-italiana-dei-nuovi-mac-intel-5"
date: 2006-01-16
draft: false
tags: [ping]
---

Tutto quello che fa in più iMovie Hd di iLife ’06 si può chiaramente vederlo sul sito Apple. Ma vedere dal vivo l’applicazione dei nuovi temi e delle nuove funzioni fa effetto. Voglia di videocamera, saltami addosso…
