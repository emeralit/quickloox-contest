---
title: "Un po' troppo virtuale"
date: 2006-04-17
draft: false
tags: ["ping"]
---

Incoraggiato da <strong>Paolo</strong> ho iniziato i miei primi esperimenti di virtualizzazione, allo scopo di avere una istanza di Linux funzionante sotto <a href="http://www.kberg.ch/q/" target="_blank">Q</a>.

Ma non vado da nessuna parte. Il meglio che ho ottenuto è fare eseguire alla macchina virtuale il boot da un'immagine disco di <a href="http://www.gentoo.org/" target="_blank">Gentoo</a>. Alla fine ottengo però solo un pinguino in alto a sinistra e, per il resto, schermo nero e silente.

Suppongo, da un primo e superficiale approfondimento, di dover inserire qualche comando modificatore per l'emulatore Qemu che sta sotto Q, per risolvere un (suppongo sempre) problema video. Non è colpa di Q, che è dichiaratamente in stadio di sviluppo alpha. Tuttavia, ecco, ancora non è una cosa immediata. Per me, intendo.

La virtualizzazione, per ora e per me, è un obiettivo virtuale.