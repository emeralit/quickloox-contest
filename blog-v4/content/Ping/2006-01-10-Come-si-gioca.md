---
title: "Come si gioca"
date: 2006-01-10
draft: false
tags: ["ping"]
---

Grazie a tutti per la calorosa accoglienza. :-)

Come tutti i pazzi, collaudiamo la macchina nuova gettandoci a centoquaranta sulla statale di notte. Fuori di metafora, lo strumento è ufficiale da poche ore, ci stiamo prendendo confidenza tutti e tra breve lo seppelliremo di notizie e commenti relative a Macworld Expo.

Per sicurezza, rientrando in metafora, ecco dove si trovano gli incroci. Così si evitano gli incidenti. Non ci tengo particolarmente io; è questione di rispetto dell'editore, che mette a disposizione lo spazio.

0) Per desiderio esplicito dell'editore, i commenti sono moderati. Sono io che li modero. In pratica nessuno verrà censurato, basta non dare fuori di matto e non insultare chicchessia. Però passa del tempo da quando arrivano a quando li modero. Se mi trovo sotto la doccia, o a pranzo, aspettano. Per qualunque problema, scrivi a <a href="mailto:lux@mac.com">Lucio Bragagnolo</a>.

1) non è una chat. Sono commenti. Se qualcuno commenta il tuo commento, è legittima al massimo una replica. Poi basta. Vale anche per me. :-)

2) i commenti riguardano l'oggetto del <em>post</em> (quello che scrivo io, insomma), non quello che passa per la testa rispetto a qualunque altro tema. Per quello, ognuno di noi ha il nostro blog personale. Sono prontissimo a fare <em>trackback</em>, basta segnalare. Se non sai che cos'è un trackback non ti preoccupare… e apri un tuo blog. Sarò felicissimo di segnalare i link ai blog degli amici di Ping!

3) il commento migliore è quello lungo, al massimo, quanto il messaggio di partenza. Sii sintetico. Se il commento è più lungo del mio post, invece di scrivere un commento, <a href="mailto:lux@mac.com">scrivimi</a>.

Ancora benvenuto.