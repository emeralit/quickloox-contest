---
title: "Da Seti a Boinc"
date: 2004-07-08
draft: false
tags: ["ping"]
---

Una transizione con supporto di Mac OS X

Devo ammettere che seguo poco gli sviluppi del progetto Seti@home. È affascinante ma preferisco dedicare il mio tempo di salvaschermo agli scopi più concreti di <link>Folding@home</link>http://www.stanford.edu/group/pandegroup/folding/.

Per questo ho scoperto con grosso ritardo che il progetto Seti sta <link>abbandonando</link>http://setiweb.ssl.berkeley.edu/transition.php la propria piattaforma di elaborazione distribuita, per adottare la tecnologia <link>Boinc</link>http://boinc.berkeley.edu/.

A parte il fatto che le statistiche dei vecchi client vengono congelate e per chi ci tiene è opportuno passare al più presto ai client nuovi, la cosa importante è il supporto di Boinc per Mac OS X da subito.

Ancora una volta, anni fa, senza quella X nel nome, non sarebbe stato così ovvio.

<link>Lucio Bragagnolo</link>lux@mac.com