---
title: "Lettera aperta agli adoratori di HyperCard"
date: 2002-01-04
draft: false
tags: ["ping"]
---

Il massimo della libertà è poter scegliere

Caro amico adoratore di HyperCard,
sono un amico, perché sono un adoratore di HyperCard, L’ho comprato appena Apple ha smesso di regalarlo; uso almeno uno stack tutti i giorni, seppure su un sistema operativo che due anni fa non esisteva, su un Mac che due anni fa non esisteva.
Un mese sì e uno no si vocifera di un HyperCard 3.0. Anche quando ero militare un mese sì e uno no usciva la notizia che il mio scaglione sarebbe stato congedato con largo anticipo. Non era vero.
Meno ovviamente, la mia opinione consolidata è che non esista alcun HyperCard 3.0.
Vuoi cambiare? A gennaio, per esempio, uscirà AppleScript Studio. Ricordi che da anni HyperCard è compatibile con AppleScript? Se hai visto lungo, molto del tuo lavoro è salvo. Non vuoi cambiare? Beh, HyperCard funziona in Classic e continuerà a farlo. Si può cambiare in avanti, o continuare a usare lo stesso programma del 1985. Ehi, questa è libertà.

<http://www.apple.com/applescript>