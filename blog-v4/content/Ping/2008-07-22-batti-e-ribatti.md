---
title: "Batti e ribatti"
date: 2008-07-22
draft: false
tags: ["ping"]
---

Imperdibile <a href="http://gizmodo.com/5026981/windows-95-lead-architect-is-a-mac-convert-launches-first-iphone-app" target="_blank">segnalazione</a> da <a href="http://www.accomazzi.it" target="_blank">MisterAkko</a>:

<cite>Dopo avere lasciato Microsoft, Satoshi Nakajima, architetto capo di Windows 95 e </cite>forza risolutiva<cite> nella creazione di Internet Explorer 3.0, voleva capire perché Apple gode di tanto favore. Ha scelto un Mac due anni fa e ha deciso che non avrebbe mai più usato un Pc. Oggi la sua azienda, Big Canvas, produce programmi per iPhone.</cite>

Dagli abbastanza tempo e capiscono tutti. È solo questione di avere intelligenza sufficiente.