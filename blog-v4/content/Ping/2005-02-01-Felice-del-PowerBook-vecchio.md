---
title: "Quello che serve, quando serve"
date: 2005-02-01
draft: false
tags: ["ping"]
---

Guida al saggio acquisto di un computer a fine produzione

Sapevo benissimo che c'erano i PowerBook nuovi in arrivo ma ho approfittato dei vantaggiosi leasing di fine 2004 e ho acquistato un PowerBook 17&rdquo; di quelli che stavano per venire sostituiti.

Volevo uno schermo più grande di quello attuale e lo schermo è rimasto invariato. Avevo ordinato l'hard disk opzionale più veloce, e adesso è di serie, ma è veloce uguale. Sempre opzionalmente avevo chiesto la scheda video più performante e adesso è di serie, ma è quella lì.

Non uso Bluetooth e uso la trackpad il meno possibile, quindi le migliorie arrivate non sono particolarmente interessanti. Per farla breve, sto comprando consapevolmente un PowerBook vecchio mentre escono quelli nuovi e sono felice come una Pasqua.

A comprare quello che serve, quando serve, non si sbaglia mai.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>