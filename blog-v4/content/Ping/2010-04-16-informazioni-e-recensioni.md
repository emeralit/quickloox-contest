---
title: "Informazioni e recensioni"
date: 2010-04-16
draft: false
tags: ["ping"]
---

Nei numerosi commenti che hanno fatto seguito a <a href="http://www.macworld.it/ping/blob/2010/04/14/cronache-del-bar-sport/" target="_self">Cronache del Bar Sport</a> qualcuno ha  ha parlato di <i>informazione</i>.

Provo a distinguere tra <i>informazione</i> e <i>recensione</i> e scendo in campo neutro: BetaNews ha parlato della tavoletta <a href="http://www.betanews.com/article/Handson-with-the-WebStation-Android-Tablet/1271281002" target="_blank">WebStation</a> con sistema operativo Android.

Questa, secondo alcuni, è <i>informazione</i>.

<cite>Ha una porta Usb, ha una porta miniUsb, ha uno stilo.</cite>

Questa invece è una <i>recensione</i>.

<cite>Si può usare con le dita, ma è molto più precisa con lo stilo e non la si può toccare con le dita se si usa lo stilo. [&#8230;] Le porte Usb sono interessanti e si possono collegare varie periferiche, come una tastiera e un mouse; ma una</cite> webcam<cite>, un convertitore Usb-Dvi e una cuffia Skype non sono state riconosciute.</cite>

Nessuno si offenda se dichiaro che una recensione, quando è tale, aiuta a capire il valore di un prodotto, mentre certa informazione ha il valore della lista della spesa (per la quale è meglio <a href="http://itunes.apple.com/it/app/shopper/id284776127?mt=8" target="_blank">Shopper</a>).