---
title: "Com'è misteriosa la vita"
date: 2006-06-16
draft: false
tags: ["ping"]
---

<strong>Stefano</strong> mi ha fatto notare il problema di uno che all'improvviso non sentiva più l'audio nei filmati Flash e si è &ldquo;rivolto ad Apple&rdquo;, che gli ha consigliato di <a href="http://www.applematters.com/index.php/forums/viewthread/768/" target="_blank">aprire e chiudere GarageBand</a>.

Problema risolto. Anche se per me è un mistero che si apre.