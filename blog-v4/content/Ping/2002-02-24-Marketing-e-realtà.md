---
title: "Marketing e realtà"
date: 2002-02-24
draft: false
tags: ["ping"]
---

Non bisogna mai credere a quello che dice la pubblicità. Tuttavia si può sempre sperare nel meglio

Quando uscì il primo Power Mac G4 il marketing Apple fece leva sul picco teorico dei quattro gigaflop, o miliardi di operazioni su numeri con la virgola eseguibili ogni secondo dal processore. C’era un pizzico di esagerazione: per raggiungere questo valore il processore avrebbe dovuto ripetere <i>ad libitum</i> una specifica istruzione AltiVec su un PowerPC a 500 Mhz.
Ora che sono arrivati i Power Mac G4 con due processori da un gigahertz, Apple proclama il picco teorico a 15 gigaflop. Facendo le moltiplicazioni, in base al criterio di prima dovrebbero essere sedici; a patto però che i processori facciano solo quello e assolutamente nient’altro. Togliendo un gigaflop Apple riconosce, più realisticamente di prima, che oltre a calcolare a velocità smodata occorre anche, letteralmente, pensare a che cosa fare con quei risultati.
Apple è davvero speciale; in quale altra azienda gli addetti al marketing hanno mai sviluppato un minimo di onestà?

<link>Lucio Bragagnolo</link>lux@mac.com