---
title: "Una celebrazione da veri <em>geek</em>"
date: 2006-02-02
draft: false
tags: ["ping"]
---

Anche un quesito per veri conoscitori <em>profondi</em> della materia.
Penso che ne farò l'argomento del prossimo Preferenze su Macworld carta e quindi rivelerò l'arcano solo a tempo debito. Poi, per qualche giorno posso pure giocare al matematico snob che scrive <em>ho una dimostrazione bellissima ma mi manca lo spazio sul foglio</em>.

Si è recentemente verificato un cambiamento epocale. Il Mac è entrato nell'era della C.