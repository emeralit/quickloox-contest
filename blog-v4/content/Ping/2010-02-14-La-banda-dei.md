---
title: "La banda dei"
date: 2010-02-14
draft: false
tags: ["ping"]
---

<a href="http://www.accomazzi.net" target="_blank">Misterakko</a> mi segnala la <a href="https://twitter.com/officeformac/status/9075963547" target="_blank">smentita ufficiale di Microsoft</a> al logoro argomento che <i>tutti usano Word</i>.

La spiegazione è che a Macworld Expo 2010 i primi quattro a esclamare I love Office for Mac davanti allo stand avrebbero vinto una copia di Office for Mac 2008 Business Edition. Ma può restare un segreto fra noi. Oppure pensiamo a un lapsus freudiano. :-)