---
title: "Thanks God It'll Be Friday"
date: 2007-05-07
draft: false
tags: ["ping"]
---

Venerd&#236; prossimo iniziano i venerd&#236; del gioco! Dalle 13 alle 14 attenderò chiunque abbia voglia.

In omaggio a Tommaso, che è stato il primo a proporre, il gioco di questo venerd&#236; è <a href="http://www.wesnoth.org" target="_blank">Battle for Wesnoth</a>, strategico tattico a turni per grandi e piccini.

Se non c'è già, aprirò un thread sui forum di Macworld.

Per venerd&#236;, consiglio vivamente un collegamento iChat/Aim. Il mio nickname è lux@mac.com; il primo che ha voglia apra pure una stanza <strong>gamefriday</strong>, in cui possiamo chiacchierare e sincronizzarci al margine del gioco stesso.

Venerd&#236; dopo venerd&#236; passeranno tutti i generi. Accetto costantemente proposte e suggerimenti. Se qualcuno cerca compagni di merenda ludici per avviare iniziative simili, il forum di Macworld potrebbero divenire un posto giusto per dirlo. Io mi faccio volentieri carico di diffondere quello che vengo a sapere di mio.

Ultima cosa: conosco vagamente Battle for Wesnoth e forse avrò un leggero vantaggio su qualcuno. La cosa è puramente incidentale e, venerd&#236; dopo venerd&#236;, si passeranno robe che non ho manco mai guardato. Nel senso che lo scopo è divertirsi, non primeggiare. :-)