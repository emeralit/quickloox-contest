---
title: "Signorina, grandi firme"
date: 2006-06-25
draft: false
tags: ["ping"]
---

La mia signora ha deciso di convertirsi al banking online e l'ho debitamente accompagnata per vedere l'effetto che fa.

La procedura ha richiesto almeno un quarto d'ora. Quattro firme su quattro documenti diversi (lei è già correntista da anni, per la cronaca) e tre buste contenenti ognuna una chiave diversa, in funzione dell'operazione da effettuare.

L'ho già avvisata che nel momento in cui smarrisse una o più chiavi non dovrà contare su me. Per il resto mi chiedo che cosa abbia lei fatto di male e chioso, con permesso, che non c'è da stupirsi se come Italia siamo un fanalino di coda.