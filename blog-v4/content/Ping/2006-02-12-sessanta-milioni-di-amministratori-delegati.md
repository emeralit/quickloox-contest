---
title: "Sessanta milioni di amministratori delegati"
date: 2006-02-12
draft: false
tags: ["ping"]
---

Tutti bravi a fare la formazione della Nazionale, il Presidente del Consiglio, il giurato di Sanremo e il Ceo Apple.

Il giorno dopo il keynote di Macworld Expo sono tutti lì a spiegare perch&eacute;, alla Bartali, <cite>gli è tutto sbagliato</cite> (e da rifare). Il gusto di dire al mondo che cosa dovrebbe fare Apple, invece di comportarsi come si comporta, non lo si nega a nessuno.

Però mi piacerebbe poter mettere, come tassa di ingresso nel club degli amministratori delegati da salotto, un quarto d'ora di gioco a questa simulazione di <a href="http://www.intel.com/cd/corporate/europe/emea/eng/114556.htm" target="_blank">It Management</a>.