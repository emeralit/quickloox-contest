---
title: "Fatti i canali, van fatti gli italiani"
date: 2003-02-03
draft: false
tags: ["ping"]
---

Ascesa, caduta e forse decollo di Sherlock

Tre incarnazioni di Sherlock. La prima, un mela-effe con un nome artistico. La seconda, una bella soluzione espandibile di ricerca su Internet, se non fosse che Apple l’ha soffocata impedendo lo sviluppo di canali indipendenti.

Apple ha però imparato dai propri errori e Sherlock 3 è una bella sorpresa. Come macchina erogatrice di Web service, ora accetta Web service indipendenti e stanno cominciando ad arrivare cose interessanti. Su <link>VersionTracker</link>http://www.versiontracker.com basta un clic per installare un nuovo canale dedicato. È nato uno <link>Sherlock Weather Channel</link>http://www4.ncsu.edu/~domitten/. <link>iCalShare</link>http://www.icalshare.com è un altro nuovo comodissimo canale per i calendari di iCal, e sono solo esempi.

Insomma, la terza incarnazione di Sherlock, al suo arrivo pressoché inutilizzabile soprattutto in Italia, rischia di diventare veramente uno strumento imperdibile. A chi risponde che mancano canali italiani, una correzione: mancano italiani che aprano canali, che è un’altra cosa.

<link>Lucio Bragagnolo</link>lux@mac.com