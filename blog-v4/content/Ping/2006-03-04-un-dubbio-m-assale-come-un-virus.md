---
title: "Un dubbio m'assale, come un virus"
date: 2006-03-04
draft: false
tags: ["ping"]
---

Ma quelli che montano un antivirus sul Mac per intercettare i virus Windows in transito, almeno si fanno pagare come coamministratori di rete?