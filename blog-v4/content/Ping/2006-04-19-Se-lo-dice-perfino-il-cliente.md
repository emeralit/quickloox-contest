---
title: "Se lo dice perfino il cliente"
date: 2006-04-19
draft: false
tags: ["ping"]
---

Mi trovo in un famoso Apple Center di Milano e un cliente in attesa dell'approvazione di un finanziamento chiacchiera a ruota libera:

<cite>Il problema è che con tutti questi rumors non si capisce più niente e uno non sa più se e quando conviene comprare.</cite>

Dedicato a chi crede negli oroscopi. :-)