---
title: "Un tasto e… funzione"
date: 2006-01-03
draft: false
tags: ["ping"]
---

Un caso particolare, una soluzione generale

Un nuovo Mac e, per mille motivi, una vecchia tastiera Usb, priva del tasto per aprire e chiudere l'unità Dvd. Ci si arriva lo stesso in mille altri modi, ma se si volesse usare idiosincraticamente solo e soltanto la tastiera, che fare?

Risposta semplice. F12. :-)

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>