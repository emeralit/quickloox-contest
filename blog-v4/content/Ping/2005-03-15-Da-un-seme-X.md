---
title: "Da un seme X"
date: 2005-03-15
draft: false
tags: ["ping"]
---

Per fare un supercomputer, canterebbe Sergio Endrigo, ci vogliono…

…o meglio, bastano Xserve G5 in sufficiente quantità. La Bowie State University americana ha lavorato in collaborazione con Apple per dare vita a un supercomputer del valore di un milione di dollari. La creatura si chiama Xseed ed è composta da un cluster di 224 nodi. Ogni Xserve G5 è operativo con Mac OS X Server e due processori da 2 gigahertz. A collegare tutto, uno switch Myrinet da quattro gigabyte per secondo.

Quanto sopra è sufficiente a fare entrare Xseed nella top 100 dei supercomputer di tutto il mondo.

E c'è ancora chi sta lì a fare i discorsi sul clock.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>