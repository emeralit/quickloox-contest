---
title: "Ricerche minerarie e perdite di tempo"
date: 2007-08-02
draft: false
tags: ["ping"]
---

Grandissimo <strong>Pierfausto</strong>. Lo cito come merita:

<cite>Ho fatto un po' di progressi con <a href="http://jocr.sourceforge.net/" target="_blank">gocr</a>; l'altra notte mi serviva riconoscimento ottico dei caratteri per un totale di 5-6 pagine di testo. Come al solito, piuttosto che perdere un'ora a trascrivere, ho perso un'ora (anche due) per fare funzionare gocr.</cite>

<cite>[&#8230;]</cite>

<cite>3) Tra le altre cose, gocr non riconosce le colonne e lascia solo un certo numero di spazi tra il testo di una colonna e quello dell'altra. Ma noi abbiamo un Mac: apro un editor di testo (in questo caso ho usato <a href="http://www.barebones.com/products/textwrangler" target="_blank">TextWrangler</a> per facilitare le sostituzioni e alre piccole cosette), premo Opzione, il cursore diventa una crocetta e seleziono il testo che voglio come se stessi lavorando su un'immagine. Ed ecco sistemate anche le colonne.</cite>

Il passaggio chiave è <cite>ho perso un'ora a fare funzionare gocr invece che un'ora a trascrivere a mano il testo</cite>. Non è un'ora persa. Il ritorno di investimento di quell'ora, passata a imparare anziché produrre, è incommensurabile. Una pepita di intelligenza, apprendimento, scoperta.