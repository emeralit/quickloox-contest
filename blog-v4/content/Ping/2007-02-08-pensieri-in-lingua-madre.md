---
title: "Pensieri in lingua madre"
date: 2007-02-08
draft: false
tags: ["ping"]
---

Nel momento in cui scrivo questo Ping, sono ancora l'unico ad avere pubblicato la <a href="http://www.macworld.it/showPage.php?template=notizie&amp;id=11710" target="_blank">traduzione integrale</a> della lettera di Steve Jobs su musica protetta e Drm.

Per eccesso di tempo libero, &#231;a va sans dire.