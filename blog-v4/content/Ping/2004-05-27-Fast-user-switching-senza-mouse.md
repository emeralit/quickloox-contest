---
title: "Fastest User Switching"
date: 2004-05-27
draft: false
tags: ["ping"]
---

Piccola sfida per fanatici della tastiera

Problema. Dato Panther, passare da un utente a un altro senza usare il mouse e senza passare dalla lista degli utenti (in pratica, ruotare da un utente all’altro, oppure scegliere l’utente con un meccanismo simile a quello di Comando-Tab).

Ho trovato su Versiontracker.com un paio di utility come WinSwitch e FusKey che risolvono la prima metà del problema, nonché svariati <link>script</link>http://www.macosxhints.com/article.php?story=20031102031045417
interessanti. Ma la seconda metà della sfida è ancora irrisolta.

Se qualcuno ha la risposta, questa era la domanda.

<link>Lucio Bragagnolo</link>lux@mac.com