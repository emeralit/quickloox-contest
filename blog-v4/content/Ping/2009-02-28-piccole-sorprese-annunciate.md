---
title: "Piccole sorprese annunciate"
date: 2009-02-28
draft: false
tags: ["ping"]
---

Scrivo distrattamente in Skype, inverto due caratteri e sto per fare noiosamente cancella-cursore-riscrivi.

Poi mi viene in mente che le applicazioni Cocoa possiedono le scorciatoie di tastiera di <code>emacs</code> e cos&#236; mi metto tra i due caratteri, <code>Ctrl-T</code>, fatto.

Consiglio vivamente il tutorial di emacs. Lanciare il Terminale, digitare <code>emacs</code>, premere <code>Ctrl-h</code> e poi premere <code>T</code>. Parte un interessante tutorial di dimensioni contenute e in italiano. Quanto detto nel <em>tutorial</em> di <code>emacs</code> funziona quasi senza eccezioni in TextEdit, per esempio, e in tutti i programmi scritti con Xcode di Apple.

Se <code>emacs</code> mette un po' di timore, passa subito: per uscire dal programma si premono prima <code>Ctrl-X</code> e poi <code>Ctrl-C</code>. Ancora in crisi? basta chiudere la finestra del Terminale e l'incubo sparisce.