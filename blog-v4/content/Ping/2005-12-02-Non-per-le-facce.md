---
title: "Non per le facce"
date: 2005-12-02
draft: false
tags: ["ping"]
---

Ma per il posto

Insisto con Quiliano e con il suo meraviglioso museo Apple, perché in realtà ci si dovrebbe fare una rubrica quotidiana a parte e invece non posso fare più di una dozzina di interventi l'anno.

Lo staff del museo si è divertito a mettere sul Web un <a href="http://www.allaboutapple.com/movies/seratina.htm">filmato</a> di momenti catturati all'interno del museo. Le facce, come accade in qualunque filmato di entusiasti, è meglio perderle che trovarle (scherzo!).

Invece lo spirito, l'entusiasmo, sono tutti da godere. E soprattutto la <em>location</em>. È perfetto per farsi un'idea di che cosa si trova e come è strutturato il museo. Una scusa in meno per rinviare la visita!

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>