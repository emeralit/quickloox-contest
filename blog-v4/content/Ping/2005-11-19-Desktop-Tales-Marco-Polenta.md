---
title: "Desktop Tales"
date: 2005-11-19
draft: false
tags: ["ping"]
---

Una striscia a fumetti da vedere. Con un occhio al computer di casa

I Desktop Tales (Racconti di scrivania, ma il riferimento va probabilmente ai Duck Tales disneyani) sono un modo inventato di fresco dal bravissimo Marco Polenta per ridere, sorridere e riflettere sul nostro approccio alla tecnologia e ai computer.

O, più che altro, al loro approccio. Quello dei computer. Con riferimento particolare e voluto a quei computer di razza un po' speciale, che hanno un nome, una personalità, perfino un umore. Diversi da quegli altri, grigi, grigi, anonimi, freddi, che nell'animo del proprietario valgono quanto una grattugia o una forchetta. Spesso c'è da dire qualcosa anche sull'animo del proprietario.

Ecco. A quella gente, per cui tutto deve essere grigio e noioso, molto probabilmente i Tales non piaceranno. Troppo ritmo, troppa fantasia, troppa mancanza di mortale serietà.

A me sono piaciuti e spero che Marco vada avanti a lungo. Non è per dire che io non sia noioso, tutt'altro: è per dire che i <a href="http://www.monipodio.net/index.php?option=com_content&task=blogcategory&id=30&Itemid=57">Desktop Tales</a> brillano di spirito e inventiva. A dare un nome al mio computer ci arrivo, e allora forse ho superato il test, ecco.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>