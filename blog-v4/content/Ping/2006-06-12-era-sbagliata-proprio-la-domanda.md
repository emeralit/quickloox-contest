---
title: "Era sbagliata proprio la domanda"
date: 2006-06-12
draft: false
tags: ["ping"]
---

Il pallino mi gira da tanto tempo e forse un giorno lo farò pure. Fare un sito delle previsioni dei cosiddetti esperti e riderci sopra, nel momento in cui si scopre che sono tutte scemate.

Frugando nel box è saltato fuori uno studio del <a href="http://www.gartner.com/" target="_blank">Gartner Group</a> del 1989, intitolato <em>Apple Macintosh Futures</em>. Devo premettere che il Gartner Group non è un sito di smandrappati che abboccano a qualunque idiozia scriva <a href="http://www.theregister.co.uk/" target="_blank">The Register</a>, ma una prestigiosa organizzazione che spilla milioni di euro a fior di aziende.

Il documento, a diciassette anni di distanza, è una mistura di ovvietà da doposcuola e di minchiate. Lo avrebbe potuto scrivere mio fratello, quello che vive benissimo senza computer.

La prima pagina pone cinque domande, alle quali il resto del documento intende rispondere. La seconda domanda è <cite>How will OS/2 and Macintosh System 7 affect Apple's competitive position?</cite> Come influiranno System 7 e OS/2 sulla competitività di Apple?

Rispetto a System 7 non sapre dire, così sui due piedi. La mia risposta per quanto riguarda OS/2 è <em>zero, perch&eacute; la sua esistenza non ha cambiato una virgola nell'andamento di Apple, e non solo: OS/2 è <a href="http://www-306.ibm.com/software/os/warp/" target="_blank">sparito nel quasi nulla</a> (salvo qualche banca) ed è stato spazzato via dall'arrivo di un sistema operativo chiamato Windows che, se se ti chiami Gartner Group e vali veramente i soldi che costi, avresti dovuto almeno considerare come possibilità, o è circonvenzione di incapaci.</em>

Puoi scommettere che la risposta dentro il documento è ben diversa.

Mi sa che ci tornerò su. Quasi quasi ci scrivo un Preferenze.