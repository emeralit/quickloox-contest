---
title: "Esercizi di estensione"
date: 2003-06-16
draft: false
tags: ["ping"]
---

Vuoi perdere tempo in Mac OS X come una volta? Si può

Ai tempi di Mac OS 9 c’era gente che infilava nella Cartella Sistema ogni tipo di estensione per poter raccontare di avere quattro file di icone sullo schermo durante il boot. Poi invariabilmente si verificava qualche conflitto e passavano i pomeriggi a fare prove su prove.

Con Mac OS X non c’è più tutto questo divertimento (chiamiamolo così) ma, volendo, ci sono vari modi di pastrugnare le estensioni.

Quelle di Mac OS X stanno in /System/Library/Extensions e, per esempio, si caricano con il comando

sudo kextload -nt nomeestensione

Per esempio: sudo kextload -nt MacWireless80211bPCCard.kext

Guardando l’elenco delle estensioni si scoprono un sacco di componenti intriganti. Che consiglio di lasciare stare; per perdere tempo, meglio una bella campagna stellare di Ev Nova oppure di Uplink, tutti e due ottimi giochi targati <link>Ambrosia Software</link>http://www.ambrosiasw.com.

<link>Lucio Bragagnolo</link>lux@mac.com