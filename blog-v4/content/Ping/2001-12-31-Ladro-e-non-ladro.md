---
title: "Ladro e non ladro"
date: 2001-12-31
draft: false
tags: ["ping"]
---

Il mistero del mancato boicottaggio dei criminali più famosi del mondo

Prendiamo la peculiare situazione politica italiana. Una parte consistente dell’elettorato (non importa, qui, se a ragione o a torto) pensa che l’attuale Presidente del consiglio sia un ladro. Se dice qualcosa mente di sicuro; essendosi arricchito, si sostiene, nell’illegalità bisogna isolarlo, evitare di guardare le sue televisioni, delegittimarlo, e avanti così.
Adesso prendiamo l’informatica. Una nota azienda di software, ha stabilito un tribunale, ha abusato illegalmente del suo monopolio danneggiando la concorrenza e il mercato, e facendoci sopra una quantità di soldi astronomica. Eppure un mare di gente continua tranquillamente a usare il software di quell’azienda, a diffondere il suo monopolio e quindi ad avallarne i comportamenti che nuocciono al mercato e alla concorrenza.
Due pesi e due misure. Ma che cos’ha Silvio Berlusconi che non abbia Bill Gates?

lux@mac.com