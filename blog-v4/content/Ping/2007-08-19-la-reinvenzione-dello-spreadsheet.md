---
title: "La reinvenzione dello spreadsheet"
date: 2007-08-19
draft: false
tags: ["ping"]
---

Con il tempo che fa devo stare lontano dai giri di parole e dire le cose come stanno a proposito di <a href="http://www.apple.com/iwork/trial" target="_blank">iWork '08</a> (linko la <em>trial</em> che altrimenti non si trova).

Keynote era già il leader assoluto e ha solo incrementato il vantaggio. Vale il prezzo dell'intera suite. Se uno non aveva capito prima, neanche adesso.

Pages si è di molto arricchito ma non sposta una virgola.

Numbers è la reinvenzione del foglio elettronico. Cos&#236; com'è è proprio bello. Una 1.1, a eliminare qualche peccatuccio di gioventù, lo renderà irresistibile. Una 1.5, che inserisse quelle funzioni più professionali e specifiche tralasciate (giustamente, per una 1.0) a favore dello spettacolo, farebbe il botto e bello grosso.