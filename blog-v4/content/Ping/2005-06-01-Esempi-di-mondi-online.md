---
title: "Quant'è concreto il gioco virtuale"
date: 2005-06-01
draft: false
tags: ["ping"]
---

Anche chi è contrario al gioco dovrebbe provare un mondo online almeno per un'ora

Sto giocando da un po' con grande soddisfazione a <a href="http://www.worldofwarcraft.com">World of Warcraft</a>. La soddisfazione viene dal fatto che riesco a giocare per il tempo che voglio, senza sentirmi pressato a essere sempre in rete. È possibile giocare con gusto anche per quindici/venti minuti arrivando a un punto fermo o raggiungendo un traguardo. Contemporaneamente ho conosciuto un sacco di amici virtuali, ma reali ugualmente, e abbiamo compiuto insieme imprese epiche.

Non è una cosa che faccio abitualmente, ma mi sento di raccomandare caldamente l'esperienza, su cui io stesso avevo qualche notevole perplessità iniziale.

World of Warcraft va comprato e poi si paga anche un canone mensile. Capisco benissimo che qualcuno voglia saperne di più sui mondi online senza affrontare la spesa.

Beh, ci sono mondi gratuiti oppure in demo quasi a volontà. Sono tutti graficamente meno belli di World of Warcraft e anche il sistema di gioco sarà meno brillante, ma per capire e spendere poco va benissimo. Qualche nome: <a href="http://www.apple.com/downloads/macosx/games/role_strategy/graal.html">Graal</a>, <a href="http://www.apple.com/downloads/macosx/games/role_strategy/planeshift.html">PlaneShift</a>, <a href="http://www.apple.com/downloads/macosx/games/role_strategy/clanlord.html">Clan Lord</a>.

Se il computer non ha una buona scheda video, beh, si può vivere in un mondo online anche con interfaccia solo testuale, come in <a href="http://www.leu.it">Lumen et Umbra</a> (ma ce ne sono centinaia).

Davvero da provare.

<a href="mailto:lux@mac.com">Ithilgalad, irregolare elfo della gilda Heroes of Freedom</a>