---
title: "Novità pulsanti"
date: 2005-10-13
draft: false
tags: ["ping"]
---

Molto più che <em>one more thing</em>

Tutti sono in fibrillazione per l'iPod video o per FrontRow. Non vedo perché guardare il computer dal divano con il telecomando in mano, con tutto quello che si può fare stando davanti al computer con in mano mouse e tastiera. Ma <em>de gustibus</em>.

Più che altro, è la prima volta dal 1984 che Apple annuncia un computer equipaggiato di serie con un dispositivo di input che ha più di un pulsante, comunque lo si voglia guardare.

Dopo ventuno anni c'è una rivoluzione. Davanti a cui non c'è Mpeg-4 che tenga.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>