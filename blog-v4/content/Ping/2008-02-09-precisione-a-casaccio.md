---
title: "Precisione a casaccio"
date: 2008-02-09
draft: false
tags: ["ping"]
---

Nella vita c'è bisogno delle cose più assurde. A me ogni tanto serve software che prenda un elenco e ne rimescoli a caso le righe.

Sulla mailing list di supporto a BBEdit hanno pubblicato uno scriptino in Perl elegante e delizioso:

<code>#!/usr/bin/perl -w</code>

<code>@content = <>;</code>
<code>while (@content) { print splice(@content,rand(@content),1); }</code>

Va registrato come file testo, con un nome qualunque, nella cartella Unix Filters di BBEdit (nel mio caso) oppure in quella di <a href="http://www.barebones.com/products/textwrangler" target="_blank">TextWrangler</a>, l'editor di testo più straordinario del pianeta tra quelli gratuiti. La cartella si trova in <code>~/Libreria/Supporto Applicazioni/TextWrangler/Unix Support/Unix Filters</code>.

A seconda di come si sta lavorando, <code>Libreria</code> potrebbe essere <code>Library</code> e <code>Supporto Applicazioni</code> potrebbe essere <code>Application Support</code>. <code>~</code> è il modo veloce di dire la mia cartella Inizio.

Il file testo di cui scombinare le righe deve essere senza <em>soft wrap</em>, ossia la riga non va a capo quando incontra il margine destro della finestra, ma scorre liberamente (e bisogna scorrere noi la finestra in orizzontale per vedere dove va a finire). Il controllo del <em>soft wrap</em> sta nella barra strumenti standard di TextWrangler, indicato dall'icona di un minipannello di preferenze con sopra una <em>T</em>.

Mi scuso per avere mancato la &#8220;lezione&#8221; di AppleScript che avrei voluto erogare il giorno 7 (e questo trucchetto serva a parziale riparazione). Si riprende il 14 prossimo venturo.