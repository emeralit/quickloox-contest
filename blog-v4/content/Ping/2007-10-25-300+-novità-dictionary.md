---
title: "300+ novità: Dictionary"
date: 2007-10-25
draft: false
tags: ["ping"]
---

A un italiano serve relativamente (a meno che non voglia imparare l'inglese&#8230;), ma le modifiche sono anche l&#236;.

Dentro il Dizionario è ora presente anche una raccolta di termini riguardanti Apple e il Mac.

Se qualcuno usa il giapponese, c'è la traduzione automatica. Ed è possibile scrivere le ricerche in giapponese.

Non è molto. Del resto è il Dizionario.

La <a href="http://www.apple.com/it/macosx/features/300.html" target="_blank">pagina sul sito Apple</a>, comunque, ha ancora molto da dare.