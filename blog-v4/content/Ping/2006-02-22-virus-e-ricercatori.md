---
title: "Virus e ricercatori"
date: 2006-02-22
draft: false
tags: ["ping"]
---

Ti riempi le tasche di soldi. Ti rechi nel quartiere più malfamato della città, avvicini la faccia da malvivente meno rassicurante che trovi e gli mostri il portafogli gonfio.

Nel caso ti rapinasse, è colpa del Comune che non garantisce la tua sicurezza, vero?

Un po' come certi <a href="http://www.ambrosiasw.com/forums/index.php?showtopic=102379" target="_blank">trojan</a> (non virus) che vanno di moda ultimamente. Scarichi chissà che da chissà chi, lo decomprimi, lo apri con un doppio clic invece che con un drag and drop, e poi Mac OS X non è sicuro.

C'è chi se le cerca. Ma dirlo non va di moda.