---
title: "Un secondo, per favore"
date: 2005-10-08
draft: false
tags: ["ping"]
---

Campagna a favore della biodiversità del software

Sul mio computer ho cinque browser Web: Firefox, iCab, Lynx, OmniWeb e Safari. Vivo nel Mac e ho consistenti deformazioni professionali, per cui spero di essere capito. Non è necessario avere cinque browser per vivere.

Ma sento gente che inorridisce all'idea di averne due. Per esempio, uso Safari per quasi tutto e poi certe cose vanno su Firefox. Non solo perché magari funzionano lì e non su Safari; a volte l'interfaccia migliore è quella di un programma, a volte è quella di un altro.

L'idea di avere due programmi per fare una stessa cosa (e approfittare del meglio che c'è da tutte e due le parti) viene vista come fumo negli occhi. <cite>Non ho spazio</cite> (ragazzi, su un disco da cinquanta giga stanno mille programmi da cinquanta mega… non scherziamo), <cite>troppo difficile</cite> (?), <cite>mi scoccia</cite>, <cite>ne vorrei uno solo</cite> (sì, ma la ragione qual è?).

Poi pranzi a casa loro e apparecchiano con un coltello per la carne, uno per il pesce, uno per il dolce, uno per tagliare il pane eccetera.

L'unico posto dove tutto si deve fare con uno strumento solo è il computer. Ma perché?

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>