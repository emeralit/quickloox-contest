---
title: "Signorino grandi firme"
date: 2006-04-03
draft: false
tags: ["ping"]
---

Ho deciso che la gestione delle signature postali di <a href="http://www.barebones.com/products/mailsmith" target="_blank">Mailsmith</a> non è abbastanza flessibile per come la voglio io, che gestisco una marea di account e voglio diversi insiemi di signature random secondo l'account e secondo la situazione.

Ho trovato un thread di una mailing list americana dove si racconta come riuscire a fare una cosa del genere pasticciando con Perl. Mailsmith è limitato nella gestione delle signature ma molto potente nel glossario, quindi mi ci metterò.

Date le mie competenze di Perl, necessito di tifo e sostegno morale. :-)