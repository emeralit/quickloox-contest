---
title: "Scriviamo tutti di Tiger"
date: 2005-08-06
draft: false
tags: ["ping"]
---

Internet dà il meglio quando scatena la collaborazione. Come in questo caso

Mentre i parolai si avvitano intorno alla supposta scarsa innovatività del Mighty Mouse (ne avessi sentito uno solo proporre, prima, di estendere lo scorrimento a direzioni diverse da quella verticale), c'è chi fa cose utili e concrete.

Per esempio, <strong>Lele</strong> segnala <a href="http://www.tigerwiki.com/index.php/Main_Page">TigerWiki</a>. Una pagina wiki dedicata a tutte le novità di Mac OS X 10.4.

Wiki è una delle tecnologie che senza Internet sarebbero state impossibili. Usando un insieme di semplici comandi di testo, chiunque può modificare la pagina e aggiungere i suoi contributi, mantenendo un corretto aspetto in Html, link ipertestuali e tutto quello che fa Web.

Così si può raggiungere il massimo del risultato con il minimo sforzo e le nozioni di ognuno concorrono a creare un vantaggio per tutti.

Capisco che in Italia un concetto del genere faccia sollevare il sopracciglio: a forza di chiedersi <em>e io che ci guadagno?</em> nessuno muove un dito e si resta indietro.

Io posso solo ringraziare Lele e vedere se le mie modeste conoscenze mi permettono di inserire qualche riga per contribuire al progetto.

Per inciso: fare <a href="http://en.wikipedia.org/wiki/Wiki">wiki</a> non è difficile.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>