---
title: "Occasioni imperdibili"
date: 2007-03-26
draft: false
tags: ["ping"]
---

<em>Venghino venghino siore e siori</em> a fare un'offerta per questa <a href="http://cgi.ebay.it/ws/eBayISAPI.dll?ViewItem&amp;item=140100394825&amp;ssPageName=ADME:B:SS:IT:1" target="_blank">meravigliosa imperdibile asta</a> segnalatami da <strong>Stefano</strong>.

A capire che cosa è veramente in vendita, lo ammetto, ci ho messo un po'.