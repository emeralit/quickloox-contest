---
title: "Viaggi e armeggi nel tempo"
date: 2009-02-03
draft: false
tags: ["ping"]
---

In curiosa coincidenza con una interessante discussione che ho in corso, Mac OS X Hints ha appena <a href="http://www.macosxhints.com/article.php?story=20080128003716101" target="_blank">pubblicato un trucco</a> attraverso il quale riuscire a tenere attivo il backup incrementale di Time Machine anche quando si cambia Mac o si cambia scheda logica.

Infatti ogni backup di Time Machine identifica se stesso attraverso l'indirizzo Mac del computer (dove Mac è cosa diversa dall'abbreviazione di Macintosh) e attraverso l'identificatore dell'utenza in Unix, roba più complicata del semplice nome utente.

Non intendo semplificare in alcun modo la procedura. È sulla pagina, in inglese, con i suoi bei comandi Unix. Se uno non si sente di procedere è meglio che non proceda. Time Machine non è un sistema di archivio ma di backup; il mio consiglio a noi persone normali è semplicemente spianare il disco di backup di Time Machine, reimpostarlo come disco di backup della &#8220;nuova&#8221; macchina e vivere felici.

Se Time Machine viene usato con lo scopo precipuo di rimettere le mani con sicurezza di file di chissà quanto tempo fa, è un errore. Questa possibilità è un regalo extra, spesso gradito, di Time Machine. Non il suo scopo.