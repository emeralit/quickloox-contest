---
title: "X11 da Terminale"
date: 2005-02-04
draft: false
tags: ["ping"]
---

Un comando c'era. Con il senno di poi

I programmi X11 si possono lanciare anche dal Terminale di Mac OS X, usando il comando

<code>/usr/bin/open-x11 /sw/bin/xgalaga</code>

dove tutto è fisso fino a xgalaga, che in questo esempio stupido è un clone open source di un glorioso videogioco da bar.

Lo so che bastava andare a guardare per scoprirlo. Sono fatto così.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>