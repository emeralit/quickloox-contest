---
title: "10x"
date: 2006-11-25
draft: false
tags: ["ping"]
---

Ho guardato il Dock e mi sono chiesto: quanti di questi trentadue programmi usavo già dieci anni fa?

Due: <a href="http://www.barebones.com/products/mailsmith" target="_blank">Mailsmith</a> e <a href="http://www.tex-edit.com" target="_blank">Tex-Edit Plus</a>.

Nel valutare il prezzo di un software andrebbe considerato anche quanto dura. Tex-Edit Plus l'ho pagato quando ancora costava dieci dollari. È costato dunque finora un dollaro l'anno. Otto centesimi (di dollaro) al mese.