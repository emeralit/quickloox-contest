---
title: "La vinilità del maschio"
date: 2009-03-11
draft: false
tags: ["ping"]
---

Ho scoperto finalmente che cosa alimenta la passione degli affezionati alla musica su vinile (tutti uomini; mai trovata una ragazza che discetta su che cosa suoni meglio. Le ragazze sono più sensate e semplicemente mettono su quello che gli piace).

Un professore di Stanford effettua ogni anno una serie di <a href="http://radar.oreilly.com/2009/03/the-sizzling-sound-of-music.html" target="_blank">prove tra le classi</a>, che ovviamente si rinnovano costantemente, per capire i formati musicali di preferenza, sui generi più disparati.

Anno dopo anno, sempre più i giovani che sono cresciuti ascoltando gli Mp3 preferiscono gli Mp3. Anche se suonano peggio di formati più fedeli e la qualità è misurabilmente minore.

Ma è la loro musica e il loro ascolto si è formato sui pregi e sui difetti degli Mp3, come l'ascolto dei vinilisti si è formato sul vinile e quello di mezzo si è formato sui Cd.

Chi sostiene la superiorità d questo o di quel formato non sta parlando veramente del formato. Sta dicendo <em>non toccatemi l'adolescenza</em>.