---
title: "Un'ipotesi sul Think Different"
date: 2006-06-04
draft: false
tags: ["ping"]
---

<strong>Gabriele</strong> chiedeva (grazie!) un mio punto di vista sul concetto di <em>Think Different</em>. Che è un punto di vista, chiaro; qui non si sono datasheet e specifiche tecniche.

Una cosa che ritengo importante è quella impercettibile differenza tra <em>Think Different</em> e <em>Think Differently</em>, che è la stessa in italiano tra pensare differente e pensare differentemente dagli altri. Non è, ovvero, così automatico che <em>Think Different</em> sia fare diversamente da quello che fanno gli altri.

Per me un esempio di <em>Think Different</em> è esattamente la cosa che sembra la più commerciale di tutti: iPod. Non passa giorno senza sentire una critica su quello che manca a iPod. Chi vuole il cellulare, chi la radio, chi il registratore, chi questo, chi quello. E intanto iPod fa il vuoto.

Che cosa meglio di iPod riassume il senso dello spot? I <em>misfits</em>, i <em>rebels</em>, i <em>troublemakers</em>, i <em>round pegs in the square holes</em>. Non è forse iPod? Non è forse il grattacapo, la seccatura, il problema più grosso per Bill Gates, per Microsoft, per il 90 percento del software mondiale, per Sony?

Già, e Macintosh?

Mac è stata per l'appunto creato da gente <em>not fond of rules</em> senza <em>respect for the status quo</em>. Hanno inventato, immaginato, ispirato, creato e soprattutto spinto in avanti l'umanità. Non sembri una cosa piccola, l'interfaccia grafica. Ha davvero cambiato il mondo.

Lo spot dice <em>We make tools for these kinds of people</em>. Facciamo strumenti per queste persone. <em>Because the ones who are crazy enough to think that they can change the world, are the ones who do</em>.

C'è gente che crede che cambiare il mondo significhi occuparsi con molte parole e pochi fatti di cause lontanissime nel tempo e nello spazio e che più grandi sono le cause, più si riuscirà a incidere. &Egrave; l'opposto, sorry.

Si cambia il mondo a partire dalla propria famiglia, dal proprio lavoro, dal proprio divertimento, dal proprio sapere. A partire dalla cose più piccole, da cui nasce l'effetto valanga. Apple fa strumenti per questa persone. Chi vuole un cellulare su iPod non vuole cambiare niente; vuole un ennesimo cellulare. Come se avesse una vera utilità.

Chi di noi usa un Mac e lavora o ha lavorato in ambienti misti sa benissimo che cosa voglia dire essere un piolo rotondo in un foro quadrato. Un ribelle. Un rompiscatole. <em>Think different</em> è diventare più produttivi con Mac OS X più di quanto chiunque potrà mai essere con Windows. Chi vuole Windows per accontentarsi di quello che sa invece che scoprire qualcosa di nuovo, beh, forse <em>thinks</em>, ma sicuramente non <em>different</em>. Il lamentoso <em>perch&eacute; non ho il programma così bello che ha il mio amico?</em> non è certo Think Different. &Egrave; solo incapacità di usare un browser e consultare <a href="http://www.versiontracker.com" target="_blank">Versiontracker</a>, oltre che un triste complesso di mancata omologazione al gregge.

Questo è il mio punto di vista.

Una piccola annotazione finale di puro marketing, che nessuno si ricorda. Lo slogan tipico di Ibm, ai tempi, era <em>Think</em>. <a href="http://www.edork.com/Words/ThinkDifferent.asp" target="_blank">Think Different</a> riprende lo slogan del gigante dell'informatica di una volta, e fa notare che c'è qualcosa di diverso. :-D