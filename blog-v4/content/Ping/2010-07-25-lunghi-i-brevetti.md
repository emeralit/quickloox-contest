---
title: "Lunghi i brevetti"
date: 2010-07-25
draft: false
tags: ["ping"]
---

A vent'anni dalla loro approvazione sono scaduti tre brevetti Apple riguardanti certi aspetti dei font TrueType. Cos&#236; il progetto FreeType, che realizza un motore open source di gestione font, può inserirli nel proprio codice invece di cercare soluzioni alternative.

<a href="http://www.freetype.org/" target="_blank">FreeType</a> è tutt'altro che un giocattolo, tanto che sue parti funzionano nel sistema operativo di iPhone. Per gli interessati al tema la visita è preziosa.

Ora il progetto attende la scadenza di certi brevetti Microsoft impiegati nella tecnologia ClearType. O forse no, perché sono brevetti stile Microsoft. Come si legge nella <a href="http://www.freetype.org/patents.html" target="_blank">pagina apposita</a> del progetto FreeType, <cite>l'algoritmo di filtraggio dei colori per la resa dei subpixel nella tecnologia ClearType di Microsoft è coperto da brevetti. Si noti che la resa dei subpixel in sé e per sé è</cite> prior art <cite>[è stata ideata e messa in opera da altri prima che subentrasse il brevetto]; usare un filtraggio differente dei colori aggira il problema.</cite>