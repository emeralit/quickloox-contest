---
title: "Pulizie non primaverili"
date: 2006-02-07
draft: false
tags: ["ping"]
---

Più o meno una volta al mese do una pulita allo schermo del PowerBook e l'ho fatto anche questo mese. Panno in microfibra, acqua corrente, una bella passata ed ecco fatto.

Intanto si è reso necessario riesumare una vecchia tastiera esterna, sporca e dimenticata. Per rimuovere lo sporco superficiale un comune puliscivetro ha funzionato ottimamente. E per l'interno? Idea alternativa: l'aspirapolvere di casa. Con davanti una spazzola molto fitta, l'ideale per smuovere tutte quelle cose terribili che si nascondono sotto i tasti.

E i nemici dell'igiene sono sconfitti. :-)