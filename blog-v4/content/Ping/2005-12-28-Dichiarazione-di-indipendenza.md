---
title: "Dichiarazione di indipendenza"
date: 2005-12-28
draft: false
tags: ["ping"]
---

Mai più legati da alcun filo… se non per volontà

Qualche giorno di mare, metà in vacanza metà al lavoro, ma da remoto. Questo Ping va in onda da una casa priva di qualunque presa telefonica e computer desktop. Ma il mio PowerBook e il cellulare Bluetooth superano qualunque ostacolo.

Si arriva in rete da dovunque e spero vorrai scusare l'entusiasmo del neofita, per una cosa che dovrebbe essere ovvia. Lo è, ma al solito toccare con mano è tutt'altra cosa che leggerlo sui libri.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>