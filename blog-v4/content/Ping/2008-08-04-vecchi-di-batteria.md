---
title: "Vecchi di batteria"
date: 2008-08-04
draft: false
tags: ["ping"]
---

Come certi (fantastici) vecchi che sono tutto il giorno a insegnare i segreti della briscola e dello scopone, abbiamo una vasta popolazione di vecchi di batteria, che hanno letto o creduto di leggere una cosa tanti anni fa e continuano a ripeterla, ignari che la tecnologia avanza.

In particolare, non trovo nessuno che sappia della <a href="http://support.apple.com/kb/HT1490" target="_blank">ricalibrazione delle batterie</a>.

I (fantastici) vecchi della briscola hanno il vantaggio fondamentale che la briscola è sempre quella di una volta. I vecchi di batteria è meglio che stiano aggiornati, perché le batterie non sono più quelle di una volta. O che stiano zitti.