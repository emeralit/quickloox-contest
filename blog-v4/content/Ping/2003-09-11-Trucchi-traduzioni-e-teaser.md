---
title: "Trucchi, traduzioni e teaser"
date: 2003-09-11
draft: false
tags: ["ping"]
---

Un momento di autopubblicità, come sempre nell’interesse del lettore

Ho appena terminato la traduzione italiana di Mac OS X Hacks, testo piuttosto interessante di O’Reilly, che verrà pubblicato nelle prossime settimane.

Sono cento consigli, da come scattare una buona foto digitale per usarla in iPhoto a come collegare cron a uno script Perl che faccia parlare il Mac a orari prefissati e un sacco di altre cose.

Ora ho imparato a usare cron, ho capito meglio la logica del Terminale, sfrutto meglio iCal e ho scoperto un mare di shareware di cui non sospettavo l’esistenza.

Sono così contento che regalo a tutti un piccolo AppleScript. Assegna a un’immagine Gif i metadati di Mac OS classico (type e creator) in modo che venga riconosciuta dal programmi intelligenti anche se non ha l’estensione .gif:

tell application "Finder"
  set filelist to selection as list
end tell

tell application "Finder"
  repeat with i in filelist
    set file type of i to "GIFf"
    set creator type of i to "ogle"
  end repeat
end tell

Nel libro ci sarà molto di più, per chi ama programmare e per chi non ne vuole sapere.

<link>Lucio Bragagnolo</link>lux@mac.com
