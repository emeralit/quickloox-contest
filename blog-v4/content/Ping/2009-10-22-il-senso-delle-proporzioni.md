---
title: "Il senso delle proporzioni"
date: 2009-10-22
draft: false
tags: ["ping"]
---

Se le regole contabili americane non fossero complicate come le attuali, che cambieranno entro un anno, Apple <a href="http://www.apple.com/pr/library/2009/10/19results.html" target="_blank">avrebbe chiuso i tre mesi dell'estate</a> con un <i>profitto</i> netto di 2,85 miliardi di dollari, quasi tre miliardi.

Otto anni fa, ad Apple <a href="http://images.apple.com/pr/pdf/q401data_sum.pdf" target="_blank">occorrevano sei mesi</a> per totalizzare tre miliardi di dollari in <i>vendite</i>.

Quale altra azienda è stata capace di fare lo stesso, nelle stesse proporzioni, in un mercato congestionato in cui i prezzi dei prodotti continuano a scendere e la base dell'offerta arriva a prezzi che più stracciati non si può? Credo nessuna.