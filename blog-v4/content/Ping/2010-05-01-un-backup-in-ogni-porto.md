---
title: "Un backup in ogni porto"
date: 2010-05-01
draft: false
tags: ["ping"]
---

<b>Fearandil</b> mi segnala un articolo di estremo interesse su <a href="http://blog.liip.ch/archive/2009/01/20/automatically-switching-between-different-time-machine-disks.html" target="_blank">come cambiare disco Time Machine a seconda di dove ci troviamo</a>.

La procedura è al tempo stesso assai semplice e assai complicata, dipende da chi prova a fare cosa, e non l'ho ancora testata personalmente.

Raccomando di provarci solo&#8230; con un buon backup disponibile in caso di emergenza.