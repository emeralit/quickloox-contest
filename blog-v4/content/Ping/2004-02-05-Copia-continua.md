---
title: "Copia continua"
date: 2004-02-05
draft: false
tags: ["ping"]
---

Non c’è limite a quello che Microsoft non sa inventare

Ho in mano una copia di un nostro quotidiano nazionale. Le manchette pubblicitarie ai lati della testata sono del sedicente Microsoft Office System. Il logo, per quanto colorato, spezzato e distorto, è indubitalmente lo pseudoquadrifoglio che dal 1984 campeggia sulle tastiere di Mac e, negli articoli, chiamiamo Comando.

Scommetto che non lo hanno neanche fatto apposta. O gli viene naturale, o ce l’hanno nel Dna. Gli riesce solo quello che è stato già fatto da altri.

<link>Lucio Bragagnolo</link>lux@mac.com