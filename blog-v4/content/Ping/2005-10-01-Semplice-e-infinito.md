---
title: "Spazio di proprietà"
date: 2005-10-01
draft: false
tags: ["ping"]
---

Sistema semplice, possibilità infinite: questo è Macintosh

Le opzioni di serie di Mac sono configurabili con semplicità relativa. Si può fare meglio, ma nessuno lo ha ancora fatto e tipicamente, quando accade, è Apple che lo fa per prima.

Tuttavia, nulla è impossibile a una persona sufficientemente motivata e capace di pensare a quello che fa mentre lo fa.

Enzo aveva un'esigenza insolita: voleva uno spazio come separatore delle migliaia per i numeri. Non è uno standard e Mac OS X non permette di farlo, dall'interfaccia.

Ha lanciato Property List Editor (/Developer/Applications/Utilities) e con quello ha aperto il file .globalpreferences.plist in ~/Library/Preferences.

(Sono volutamente stringato. Chi non sta capendo, meglio che non ci provi. Per esempio, si tratta di un file invisibile.)

Nel file ha aggiunto quanto segue:

<code>&lt;key&gt;AppleICUNumberSymbols&lt;/key&gt;</code><br>
<code>&lt;dict&gt;</code><br>
<code>&lt;key&gt;0&lt;/key&gt;</code><br>
<code>&lt;string&gt;,&lt;/string&gt;</code><br>
<code>&lt;key&gt;1&lt;/key&gt;</code><br>
<code>&lt;string&gt; &lt;/string&gt;</code><br>
<code>&lt;key&gt;10&lt;/key&gt;</code><br>
<code>&lt;string&gt;,&lt;/string&gt;</code><br>
<code>&lt;key&gt;8&lt;/key&gt;</code><br>
<code>&lt;string&gt;&euro;&lt;/string&gt;</code><br>
<code>&lt;/dict&gt;</code>

E adesso ha il suo separatore su misura.

Un sistema semplice per chi vuole usarlo senza pensieri. Fondamenta aperte per chi non si accontenta mai. Che volere di più?

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>