---
title: "In principio era il domani"
date: 2011-01-09
draft: false
tags: ["ping"]
---

Bella coincidenza, che al debutto di Mac App Store &#8211; discreta rivoluzione copernicana nella distribuzione del software sul desktop &#8211; ci sia <a href="http://itunes.apple.com/us/app/baycard/id407869079?mt=12" target="_blank">BayCard</a>, emulo di <a href="http://c2.com/cgi/wiki?HyperCard" target="_blank">HyperCard</a> che è ancora presente nei cuori di molti che lo videro nascere non meno di venticinque anni fa, quasi ventisei.

A differenza di HyperCard, BayCard non è scriptabile. Tuttavia la sua interfaccia è widget è intrigante e il prezzo, per quanto superiore a quello del primo HyperCard, è assolutamente accessibile.

Accessibile beninteso a chi corre su Snow Leopard. Probabilmente Mac App Store è uno dei maggiori incentivi all'aggiornamento del software di sistema (più eventuale hardware) che Apple abbia sfoderato finora.

