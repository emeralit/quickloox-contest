---
title: "Uno Spectrum si aggira per il browser"
date: 2009-10-02
draft: false
tags: ["ping"]
---

Hanno migliorato <a href="http://jsspeccy.zxdemo.org/" target="_blank">JsSpeccy</a>, emulatore di Spectrum scritto integralmente in JavaScript.

Proibitivo per chi non ricorda a memoria la tastiera. Però comprende qualche giochino e mi sono ritrovato in Manic Miner come d'incanto.

Tutta dimostrazione di tecnologia fine a sé stessa. Però, accidenti, che tecnologia. E ora la velocità è plausibile anche su un iPhone o iPod touch.