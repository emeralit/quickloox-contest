---
title: "Dimostrazione di insicurezza"
date: 2006-03-02
draft: false
tags: ["ping"]
---

Assolutamente da consultare la pagina realizzata da Akko contenente una ottima <a href="http://www.accomazzi.net/TnotizieI74.html" target="_blank">dimostrazione</a> di come possano piovere gli aggiornamenti di sicurezza, ma il cervello deve restare acceso comunque.
