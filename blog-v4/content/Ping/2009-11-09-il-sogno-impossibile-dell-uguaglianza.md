---
title: "Il sogno impossibile dell'uguaglianza"
date: 2009-11-09
draft: false
tags: ["ping"]
---

Quando uno liquida un confronto tra computer dicendo che tanto tutti i computer sono uguali, significa che tutti i suoi neuroni sono uguali.

È successo ai tempi, quando Windows 95 <cite>era uguale</cite> a Mac OS, succede oggi quando si dice che lo hardware dei Mac <cite>è uguale</cite> a quello dei Pc (superstizione assoluta, perché basta aprirli per rendersi conto di quante sciocchezze si possono dire) e si sta arrivando ai computer tascabili: Android <cite>è uguale</cite> a iPhone OS.

A Engadget hanno provato la tastiera virtuale di iPhone e quella di Android in merito al <i>multitouch</i>. Quando tieni in mano un oggetto e scrivi con due pollici, è indispensabile che il software abbia una certa tolleranza verso lo scrivente, altrimenti l'uso diventa pena.

Tutto da guardare il confronto; <a href="http://www.twitvid.com/55199" target="_blank">in un minuto e sedici</a> si capisce molto sulla gente che dice che <cite>tanto è tutto uguale</cite>.