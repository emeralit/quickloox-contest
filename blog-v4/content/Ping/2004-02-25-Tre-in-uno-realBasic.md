---
title: "Tre in uno"
date: 2004-02-25
draft: false
tags: ["ping"]
---

Lo strumento di programmazione più versatile è...

<link>RealBasic</link>http://www.realbasic.com ha sicuramente i suoi limiti e non è adatto a scrivere il prossimo Photoshop, ma realizza programmi per Mac, Windows e, adesso, anche Linux.

In più è piuttosto facile e svelto da utilizzare.

Come rapporto prestazioni/prezzo non c’è davvero male.

<link>Lucio Bragagnolo</link>luxòmac.com