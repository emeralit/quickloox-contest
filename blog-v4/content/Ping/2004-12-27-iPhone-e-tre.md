---
title: "La morte, le tasse e iPhone"
date: 2004-12-27
draft: false
tags: ["ping"]
---

Un'anticipazione che si ripete da tre anni non è più tale

Puntuale come i primi due oggetti del titolo è arrivato il <em>rumor</em> su <a href="http://www.mobilewhack.com/handset/apple_iphone.html">iPhone</a>, il fantastico cellulare che Apple non può non avere in produzione, giusto?

Se non ricordo male è il terzo anno che si parla di iPhone, sempre a ridosso del Macworld Expo di San Francisco. Sarà il terzo anno in cui Apple <em>non</em> presenterà iPhone.

A parte il fatto che per Apple non ha senso presentare iPhone, basta guardare a quanto è lunga l'argomentazione per capire che è una sciocchezza. iPod lo spieghi in una riga e infatti vende l'impossibile.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>