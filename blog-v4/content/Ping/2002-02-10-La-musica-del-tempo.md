---
title: "La musica del tempo"
date: 2002-02-10
draft: false
tags: ["ping"]
---

iPod interessa. Lo dice Google. O meglio, chi usa il miglior motore di ricerca del mondo

I motori di ricerca sono interessanti perché rivelano il mutare dei gusti e delle tendenze del popolo di Internet. Il miglior motore di ricerca è Google (mai provato a digitare <link>http://www.google.com/mac</link>http://www.google.com/mac ?) e, nel suo riassunto del 2001, si può notare come iPod sia stato uno dei <link>dieci prodotti più cercati</link>http://www.google.com/press/zeitgeist2001.html.
Per qualcosa utilizzabile solo con un Mac e che per un sacco di volpi (quelle di Esopo, della volpe e dell’uva) costa troppo, non è affatto male.

<link>Lucio Bragagnolo</link>lux@mac.com