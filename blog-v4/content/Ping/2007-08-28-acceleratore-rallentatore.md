---
title: "Acceleratore, rallentatore"
date: 2007-08-28
draft: false
tags: ["ping"]
---

Apple ha rallentato il ritmo del rinnovo della gamma prodotti Mac, hardware e software. Evidente e già detto.

Apple rallenta il ritmo perché le vendite vanno a gonfie vele. Evidente e già detto.

Meno evidente (a me) e non ancora detto (da me): non è che la gente in realtà non abbia sete di tutte queste novità ma voglia cose certamente all'avanguardia, ma solide e collaudate, senza troppi rischi e troppe incognite di cui doversi preoccupare?

Se dovessi cambiare computer domani, comprerei un MacBook Pro di cui si sa tutto, i cui eventuali difetti di gioventù sono stati risolti da tempo, le cui prestazioni sono note eccetera eccetera.

Compro i computer quando mi servono e non quando escono/non escono, per cui mi è capitato di avere in mano il modello appena uscito e per me non è un problema. Ma per il grande pubblico forse può esserlo.

Riassumendo: e se attuare una strategia di rallentamento dell'innovazione fosse pagante sul piano delle vendite? Se non fosse un problema di ingegneri a disposizione, ma una strategia perseguita scientemente? Ovvio che non potrà continuare all'infinito. Ma adesso sta pagando e molto. Il rallentatore sui rinnovi è un acceleratore di fatturato.