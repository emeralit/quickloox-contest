---
title: "Giochi di società"
date: 2008-04-29
draft: false
tags: ["ping"]
---

Ibm ha creato un <a href="http://www.roughlydrafted.com/2008/04/16/ibm-launches-pilot-program-for-migrating-to-macs/" target="_blank">programma pilota</a> che prevede la diversità di sistemi operativi all'interno dell'azienda e come tale promuove l'adozione di computer Mac. Al termine del programma pilota l'86 percento dei dipendenti che avevano provato un Mac ha chiesto di poter continuare a usarlo.

Poca roba, ventidue dipendenti. Tipo <em>uno su mille ce la fa</em>. Però, in Ibm.

Salesforce.com a noi non dice niente. Ma ha quattromila dipendenti e in America è un nome assai noto.

Circola voce che Salesforce.com sia in procinto di <a href="http://www.alexcurylo.com/blog/2008/04/23/switcher-salesforcecom/" target="_blank">passare integralmente a Mac</a>, per ragioni di sicurezza. Anche se un Mac costasse di più, mantenerlo costa di meno e alla fine con i Mac si è risparmiato.

Ricordo di quando lavoravo presso un sito italiano molto importante. Usavano Pc comprati un tanto al chilo. Un giorno arrivò il virus ILoveYou e cinquanta persone restarono ferme per un pomeriggio, perché non era possibile lavorare senza avere bonificato tutti i computer.

Dicono che il Mac non sia adatto alle aziende. Secondo me ci sono aziende che non sono adatte al Mac. Quelle miopi.