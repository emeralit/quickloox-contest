---
title: "Un buon uso di Windows"
date: 2008-07-04
draft: false
tags: ["ping"]
---

<a href="http://readatwork.com/" target="_blank">Fare finta di lavorare</a> intanto che si leggono i classici (in inglese, certo, ma vuoi mettere imparare la lingua con Lewis Carroll invece che con SpeakUp?).

Il bello è che Windows nemmeno serve veramente.