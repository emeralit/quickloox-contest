---
title: "Benedetto il wireless"
date: 2005-12-17
draft: false
tags: ["ping"]
---

A volte lavorare senza fili mette a disposizione un'ultima via di scampo

Succede una volta su cinquemila, ma succede. La mail stava partendo, per il destinatario sbagliato.

Mi sono salvato disattivando AirPort in una frazione di secondo. Ci sarei riuscito ugualmente, con il cavo? Viva il wireless.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>