---
title: "Novità al 50 percento"
date: 2009-07-27
draft: false
tags: ["ping"]
---

La navigazione quotidiana delle <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">novità di Snow Leopard</a> inaugura la sezione Safari!

<b>Una nuova e più veloce versione di Safari</b>

Safari 4 è la versione più recente del velocissimo browser web. Con Snow Leopard, il supporto dei 64 bit alza del 50 percento le prestazioni di JavaScript.

In effetti Safari 4 ce lo abbiamo già tutti, anche senza Snow Leopard. Ma quel 50 percento l&#236; farà la differenza e la farà sempre più.