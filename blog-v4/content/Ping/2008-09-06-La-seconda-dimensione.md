---
title: "La seconda dimensione"
date: 2008-09-06
draft: false
tags: ["ping"]
---

Nella mia vita non ho mai ordinato per dimensioni i risultati di una ricerca da Finder. Però c'è gente che lo vuole fare e, con Leopard, non può.

Sapevo che esiste <a href="http://www.houdah.com/houdahSpot/" target="_blank">HoudahSpot</a>, 15 euro. Ignoravo che esiste <a href="http://code.google.com/p/spotlook/" target="_blank">SpotLook</a>, gratis e <em>open source</em>.

SpotLook, a vederlo, sembra geniale, una ottima alternativa. Però sul mio PowerBook non funziona e non effettua ricerche. Ho prova provata che su altre macchine funziona, ma non saprei dire altro fino a che non capisco il problema della mia.