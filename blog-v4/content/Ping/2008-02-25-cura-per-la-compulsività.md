---
title: "Cura per la compulsività"
date: 2008-02-25
draft: false
tags: ["ping"]
---

Quando Apple Store viene chiuso, e specialmente se è marted&#236;, comincia la frenesia da nuovo prodotto. Il bello è che c'è gente che fa soldi scrivendolo sul suo sito, come se fosse una notizia.

È invece una banalità. Pingdom ha <a href="http://royal.pingdom.com/?p=251" target="_blank">poche linee di codice</a> che basta incollare in una propria pagina web (su Google, sul proprio blog, in uno spazio web gratuito, dovunque) per controllare <em>ogni minuto</em> lo status di Apple Store.

A margine: Apple è l'unica che riesca a <a href="http://royal.pingdom.com/?p=238" target="_blank">trasformare in <em>marketing</em> positivo</a> perfino la chiusura per aggiornamento del negozio online.