---
title: "Stai al tuo posto"
date: 2007-01-26
draft: false
tags: ["ping"]
---

Dammi un programma che, al termine dell'installazione, vuole riavviare obbligatoriamente il computer (invece di avvisare, semplicemente, che sarà pienamente operativo solo dopo un riavvio, e stare al suo posto) e ti indicherò un progettista software disturbato.