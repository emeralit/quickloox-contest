---
title: "La parabola del buon provider"
date: 2002-11-20
draft: false
tags: ["ping"]
---

Finché c’è spazio per un’antenna satellitare c’è speranza, pure su Mac

Internet via satellite in Italia non è un business di dimensioni colossali, eppure, da Tiscali a Netsystem, i provider ci sono. Peccato che si ostinino pervicamente a non accettare la realtà e, cioè, che alcuni loro clienti potenziali lo sarebbero davvero e pagherebbero per esserlo, se Macintosh fosse supportato. La scusa che il supporto a Mac costerebbe troppo rispetto al fatturato generato è palesemente una scusa: l’unica motivazione è l’ignoranza.

A sentirli, i satellitari, sembra che ci sia un trend verso l’adozione definitiva di Windows. Se esiste un trend è semmai l’opposto: <link>Skycasters</link>http://www.skycasters.com non esisteva. Adesso sì.

Speriamo arrivi presto anche in Europa e in Italia a predicare il vangelo della convivenza tra le piattaforme e della diversità come stimolo per l’evoluzione. Credo che a suon di parabole raccoglierebbe un numero di seguaci significativo.

<link>Lucio Bragagnolo</link>lux@mac.com