---
title: "Un ultimo sforzo"
date: 2009-06-14
draft: false
tags: ["ping"]
---

Non sto neanche a linkare; il mondo intero sa che Microsoft venderà Windows 7 in Europa senza Internet Explorer.

Manca ancora un passo: la vendita di Windows 8 senza il sistema operativo. Poi è perfetto.