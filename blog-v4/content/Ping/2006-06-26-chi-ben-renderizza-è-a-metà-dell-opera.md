---
title: "Chi ben renderizza è a metà dell'Opera"
date: 2006-06-26
draft: false
tags: ["ping"]
---

Chi mi conosce sa come diffidi con forza di tutti i benchmark. Perfetti per compilare classifiche astratte, inutili nella vita reale. Come dire: guardo la Formula 1 e mi convinco che la mia auto andrebbe più veloce se fosse colorata di celeste.

Eppure mi sento di segnalare un <a href="http://www.howtocreate.co.uk/browserSpeed.html#thetests" target="_blank">gruppo di test sui browser</a> che, quanto meno, sembra improntato a un metodo minimamente scientifico. Inoltre ci sono diversi link interessanti che chi sviluppa seriamente Html potrebbe prendere in considerazione per i suoi test.

Sarà un caso che il software apparentemente più prestante è quello più costoso?