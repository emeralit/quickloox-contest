---
title: "Parlare per dire qualcosa"
date: 2010-09-25
draft: false
tags: ["ping"]
---

Constatato nei giorni scorsi che AutoCad ritorna su Mac dopo molti anni, aggiungiamo che arriva l'edizione per Mac di <a href="http://www.macspeech.com/pages.php?pID=143" target="_blank">Dragon Dictate</a> di Nuance Communications, il programma di riconoscimento vocale forse più praticato in ambito Windows.

È possibile, con Dragon Dictate, anche qualche trucchetto come effettuare ricerche Spotlight a voce oppure cercare su Google.

Non sono riuscito a capire dal sito se c'è anche il supporto italiano. Ma arriverà (c'è per gli altri loro prodotti).

Intanto chi sostiene la scarsità di software per Mac, ancora una volta, dice qualcosa per parlare.