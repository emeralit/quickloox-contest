---
title: "Da tre a mille"
date: 2004-02-16
draft: false
tags: ["ping"]
---

Viaggio nei misteri della compressione e delle emulazioni Ia-32

La compressione è una virtù ma anche un rischio. Ho scaricato Bochs e mi è arrivata una immagine disco da due o tre mega. Ho installato il programma sul disco, l’ho avviato un paio di volte e la sua cartella è a quota 993,2 megabyte!

Tenuto conto che il programma non ha scaricato alcunché da Internet di sua volontà, è un bel rapporto di uno a trecento tra file compresso e file eseguibili. Complimenti!

In compenso Bochs è una scoperta affascinante. Un emulatore open source, aperto a gratuito, di una architettura x86 completa, dal processore Intel al Bios fino ai dispositivi di input/output. Praticamente senza averlo toccato mi sono trovato di fronte a una schermata di Dos Ibm un po’ arrabbiata per non avere trovato un disco di boot.

Sono molto tentato di provare a toccarlo un po’ per vedere se riesco a ricavarne un funzionamento utile. Da quando Microsoft ha ingoiato Virtual Pc, l’idea di emulare Intel senza dare soldi a Bill Gates e ai suoi abusi di monopolio è assai ghiotta.

<link>Lucio Bragagnolo</link>lux@mac.com