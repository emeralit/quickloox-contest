---
title: "La cura dei dettagli"
date: 2010-08-01
draft: false
tags: ["ping"]
---

Il mio sofisticatissimo InDesign 5 per Mac sostiene, ogni volta che parte, di avviare il <i>registro dei servizi di Windows</i>.

L'incuria nel software è enormemente fastidiosa; non costa risorse, solo attenzione. Vale anche e soprattutto per Mac OS X italiano.