---
title: "Riassumendo la Wwdc"
date: 2006-08-07
draft: false
tags: ["ping"]
---

Le cose da tenere d'occhio più di altre, nel nuovo Leopard presentato oggi da Steve Jobs al convegno mondiale degli sviluppatori e pronto, a sentire Jobs stesso, per la prossima primavera: Time Machine (il backup rivoluzionato), Core Animation (animazione a livello di sistema), iCal verso la multiutenza (appena accennato da Jobs, ma secondo me importante), le novità sotto Dashboard (Dashcode e il resto), software a 64 bit senza problemi e un nuovo approccio al Built-to-Order, con infinite possibilità di configurazione per le nuove macchine desktop e server.

Apple sta bene e sta lavorando a pieno ritmo. Per noi che amiamo servirci di hardware e software speciale, migliore della media e diverso dalla massa, le notizie sono poche (questo è il convegno per chi programma, non dimentichiamolo) ma le prospettive notevoli e ottime.

Sul lato Microsoft, si accumulano ritardi da anni, le tecnologie promesse vengono tagliate una dopo l'altra e la questione della sicurezza rimane catastrofica.

Chi è contento di stare male stia. <em>Hic manebimus optime</em>.