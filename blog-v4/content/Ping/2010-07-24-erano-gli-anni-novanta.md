---
title: "Erano gli anni novanta"
date: 2010-07-24
draft: false
tags: ["ping"]
---

E per sfuggire alla morsa soffocante (per l'utente) e assassina (per Internet) di Explorer, durante un certo periodo, l'unica alternativa era iCab, <i>browser</i> realizzato da un gruppo di folli programmatori tedeschi.

Che incredibilmente <a href="http://www.icab.de/" target="_blank">ha sfornato una nuova versione del programma</a>. Da non perdere, se non altro in virtù dei vecchi ricordi.

Ed è perfino in versione <i>mobile</i> per iPhone.