---
title: "Vanità delle cose umane"
date: 2008-01-06
draft: false
tags: ["ping"]
---

Durante le vacanze mi sono ritrovato ad avere urgentemente bisogno di spazio libero sul disco rigido.

Avrei dovuto fare un bel backup di foto vecchie o posta vecchia (come poi ho fatto). Ma inizialmente mi sono lasciato guidare dalla presunzione umana di giocare meglio del computer in casa del computer.

Mi sono messo a guardare nelle preferenze, nelle cache, nelle cartelle di supporto applicazioni, alla ricerca di qualcosa da buttare via.

Non che non ce ne fosse e l'ho anche buttato via. Ma lo spazio liberato, in rapporto al tempo impiegato per liberarlo, è risultato ridicolo.

Accanirsi nella caccia all'ultima preferenza, alla cache rimasta in giro, alla cartella di troppo è veramente da ultimo stadio. Molto meglio buttare via file davvero inutili e davvero grandi. Molto più sensato fare pulizia tra le nostre cose, dove si annidano giga di roba che basta mettere su un backup, che lottare contro file di preferenze di otto kilobyte, che forse avevano rilevanza una volta, quando un disco rigido era un centesimo di quelli attuali.

Sotto i dieci megabyte non si aggiorna nemmeno il conto del disco libero come lo mostra il Finder.