---
title: "Avanti tutta"
date: 2006-01-13
draft: false
tags: ["ping"]
---

Aggiornato a <a href="http://docs.info.apple.com/article.html?artnum=302810" target="_blank">Mac OS X 10.4.4</a>. Tutti i sottosistemi in perfetta efficienza. Se avessi una scheda video Geforce, invece della Ati del PowerBook 17”, ci sarebbe perfino un miglioramento delle prestazioni di <a href="http://www.wow-europe.com" target="_blank">World of Warcraft</a>. Ma va bene così. :-)