---
title: "AppleScript e l'istinto della riproduzione"
date: 2008-07-14
draft: false
tags: ["ping"]
---

Mai stato nella situazione di <em>sbobinare</em>, ossia ascoltare un nastro registrato e riassumere il contenuto in un file di testo? I giornalisti lo fanno spesso con le interviste, gli studenti con gli appunti registrati in aula eccetera.

Poter sbobinare con un buon controllo della riproduzione è essenziale. Se devi fare un'acrobazia tutte le volte che devi fermare l'audio, o il video, e un'altra per tornare al programma di scrittura, è finita.

Per questa ragione è consigliabile crearsi una piccola configurazione da sbobinamento. E AppleScript, il prezzemolo della programmazione Mac può aiutare.

Serve un piccolo aiuto esterno: un programma che permetta di assegnare facilmente scorciatoie di tastiera a uno script. Le scelte sono varie, per esempio <a href="http://www.keyboardmaestro.com" target="_blank">Keyboard Maestro</a>, <a href="http://www.red-sweater.com/fastscripts/index.html" target="_blank">FastScripts</a> e financo il gratuito <a href="http://www.shadowlab.org/Software/software.php?sign=Sprk" target="_blank">Spark</a> (quando mi fanno notare che su Mac mancano i programmi, oramai mi limito al sorrisino di circostanza. S&#236;, certo, già, hai proprio ragione, come siamo limitati).

O altro, basta che sia possibile assegnare scorciatoie di tastiera agli script. DI script ne vogliamo almeno tre. Uno che torna indietro nella riproduzione, per riascoltare l'ultimo frammento:

<code>tell document 1 of application "QuickTime Player" to set current time to (current time - 2500)</code>

Poi uno che faccia Play/Pausa:

<code>tell document 1 of application "QuickTime Player"
 if playing is true then
 pause
 else
 start
 end if
end tell</code>

E uno che salti in avanti nella riproduzione, per non perdere tempo su una parte inutile del materiale:

<code>tell document 1 of application "QuickTime Player" to set current time to (current time + 2500)</code>

In questo modo possiamo scrivere nell'editor di testo in primo piano e controllare la riproduzione (in QuickTime Player) anche se questa avviene in <em>background</em>, sullo sfondo.

Di solito commento il codice istruzione per istruzione, ma qui siamo veramente all'essenziale ed è più produttivo arrivarci da soli. Anche il solo essenziale, in AppleScript, è sufficiente per fare cose interessanti. Unica nota, quel <code>if playing is true</code>. È logica booleana, nel gergo dei programmatori: <code>true</code> sta per <em>vero</em> e il senso dell'istruzione è <em>se la riproduzione</em> (il <em>playing</em>) <em>è in funzione</em>, ossia in un certo vera, cioè <em>true</em>. AppleScript interroga il sistema è chiede <em>c'è riproduzione in corso</em>? Se s&#236;, <em>true</em>, e succede quello che è previsto nel programma subito dopo. Se no, <em>false</em>, falso, il programma procede per una strada alternativa, quella che nello script sta dopo <code>else</code>.

Anche stavolta il merito non è mio ma dei maghetti di <a href="http://www.tuaw.com/2008/06/26/simplify-media-transcription-with-hotkeys-and-applescript/" target="_blank">The Unofficial Apple Weblog</a>, non Cory Bohon ma Brett Terpstra.

Buona riproduzione e buon sbobinamento. :-)