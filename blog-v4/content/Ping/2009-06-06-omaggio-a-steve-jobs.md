---
title: "Omaggio a Steve"
date: 2009-06-06
draft: false
tags: ["ping"]
---

Visto che Steve Jobs pare tornare in salute alla guida a tempo pieno di Apple, pare opportuno festeggiare con l'istantanea di una workstation Next, creata durante l'esilio ed esposta al museo Apple di Quiliano.