---
title: "Più umano, più vero"
date: 2005-08-05
draft: false
tags: ["ping"]
---

L'open source imita il commerciale… nei menu

Da tempo esiste anche per Mac OS X una alternativa potente, completa e del tutto economica a Photoshop: <a href="http://gimp-app.sourceforge.net/">Gimp</a>. Ci vuole X11 ma il costo del programma è zero e per un non specialista, che non abbia bisogno assoluto di qualcosa che fa solo Photoshop (non molto in verità), è manna dal cielo.

Scott Moschella ha preso il codice sorgente di Gimp, disponibile pubblicamente, e ha rimontato il giocattolo in modo diverso, così che la disposizione dei menu e delle funzioni somigli il più possibile a quella che ha Photoshop. Lo ha chiamato <a href="http://plasticbugs.com/index.php?p=241">GimpShop</a>.

Così non c'è neanche il trauma del passaggio da un'applicazione all'altra, o è ridotto al minimo. L'open source non è fantastico?

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>