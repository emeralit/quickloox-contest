---
title: "Du' is megl' che three"
date: 2010-01-03
draft: false
tags: ["ping"]
---

Messa in opera anche la nuova Apple Wireless Keyboard arrivata per Natale.

Nessunissimo problema con l'altra, però queste tastiere nuove utilizzano una batteria in meno, cioè due al posto di tre.

Rispetto a qualunque altra caratteristica, il resto dello hardware sembra perfettamente uguale a prima e reagisce nello stesso modo.