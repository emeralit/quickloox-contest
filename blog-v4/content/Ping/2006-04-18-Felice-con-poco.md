---
title: "Felice con poco"
date: 2006-04-18
draft: false
tags: ["ping"]
---

Un editore mi ha chiesto di non mettere il corsivo direttamente nel file di ogni testo passato, ma di aggiungere un codice convenzionale.

Con Script Editor e Tex-Edit Plus ho registrato un paio di operazioni e nel giro di pochi istanti avevo lo script già pronto:

<code>
tell application "Tex-Edit Plus"
	activate
	set selection to "+++CORSIVO+++" &amp; selection &amp; "+++FINE_CORSIVO+++"
end tell
</code>

Con <a href="http://www.tex-edit.com" target="_blank">Tex-Edit Plus</a> è facile associare l'operazione a una scorciatoia di tastiera oppure a un pulsante di una piccola palette. Basta una combinazione di tasti, o un clic, e l'editore è contento.

Si può fare naturalmente molto di più, ma i miei vincoli di tempo e capacità sono notevoli, e anche questo poco per me è un grande aiuto. Se avessi il potente Word, sarei a grattarmi la testa.