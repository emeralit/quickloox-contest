---
title: "Tanti flop fanno un successo"
date: 2002-02-10
draft: false
tags: ["ping"]
---

Nelle università americane i Macintosh vengono considerati macchine assolutamente normali. E anche potenti

Nelle università italiane, come nel resto della nostra scuola, Mac fatica a entrare, vittima di pregiudizi, ignoranza e clientele.
Nelle università americane invece no; la notizia più fresca in materia è che il Macintosh Cluster dell’università di Southern California ha raggiunto una capacità di elaborazione di <link>233 gigaflop</link>http://daugerresearch.com/fractaldemos/USCCluster/USCMacClusterBenchmark.html mettendo in rete un gruppo di 76 Power Mac G4 a doppio processore.
La cifra è un’enormità: nella maggior parte delle nostre attività quotidiane è difficile che capiti di schierare anche un solo gigaflop (miliardo di operazioni del processore con numeri con la virgola).
Un flop è un fiasco; un miliardo di flop sono potenza di elaborazione. Chi lo spiega ai baronetti untuosi delle nostre università?

<link>Lucio Bragagnolo</link>lux@mac.com