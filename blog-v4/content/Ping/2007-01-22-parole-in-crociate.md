---
title: "Parole in crociate"
date: 2007-01-22
draft: false
tags: ["ping"]
---

Casualmente mi sono ritrovato in un grosso centro commerciale, con un grosso Mediaworld.

All’entrata del Mediaworld campeggiava un grosso cartonato a reclamizzare la disponibilità di <a href="http://www.wow-europe.com" target="_blank">The Burning Crusade</a>, l’espansione di World of Warcraft. Subito dietro l’ingresso, un grosso espositore rivaleggiava in dimensioni con l’espositore dell’ultimissimo Dvd del film del momento. Sull’espositore campeggiavano molte decine di copie di The Burning Crusade ed esattamente in quei quattro secondi sono passate due persone a prenderne una copia ciascuno, per poi portarsi alle casse. La confezione e il <em>booklet</em> di The Burning Crusade sono in perfetto italiano.

Il mondo sta cambiando, insomma, ma non è cosa di ieri. Piuttosto, appena uscito da Mediaworld, uno si trovava davanti una bancarella di piantine grasse, da un euro. Molto belline.

Stavo acquistando una piantina quando ho visto il cartello sulla bancarella: <em>depurano l’ambiente dalle radiazioni emesse dai Pc</em>. La piantina l’ho lasciata lì e la prenderò da qualche altra bancarella, priva di cartelli <em>voodoo</em>.

Tra il combattere una crociata a bordo di un drago alato in sorvolo sulle minacce di Hellfire e combatterne una per convincere la gente che una pianta grassa alta due dita non depura niente da nessuna radiazione di nessun Pc, molto meglio la prima, che è divertente e rilassa.