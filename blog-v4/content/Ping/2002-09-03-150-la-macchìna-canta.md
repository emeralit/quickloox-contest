---
title: "150 la macchìna canta"
date: 2002-09-03
draft: false
tags: ["ping"]
---

Si può sbagliare un accento ma non la decisione di passare a Jaguar, alias Mac OS X 10.2

Per ragioni professionali ho dovuto installare Jaguar già prima che fosse disponibile al pubblico e quindi ci vivo da un po’.
L’impatto è formidabile; per la prima volta una nuova versione del sistema operativo fa andare più veloce un Mac, perfino il mio Titanium che ormai è uno dei più vecchi.

Apple dice che in Jaguar ci sono oltre 150 nuove funzioni e ho fatto un esperimento empirico: man mano che vedevo qualcosa di nuovo rispetto a Mac OS X 10.1, lo segnavo in una lista.

Finora sono a 62 ritrovamenti. Tenuto conto del fatto che certi programmi non li ho nemmeno aperti (Mail è molto migliorato, ma il mio default di posta è Mailsmith di Bare Bones da quando è uscito), che ho considerato come ritrovamento singolo ogni insieme di novità relativo a un programma (TextEdit, per esempio, ha più di una novità, ma nel mio conteggio è una singola novità lui stesso) e che tante migliorie sono sotto il cofano (a partire dalla più efficiente ricompilazione di tutto il sistema con gcc 3.1), direi molto empiricamente che Apple mantiene la promessa.

Passa a Jaguar. Mac OS X 10.2 sta a Mac OS X 10.1 come Mac OS 9 sta a Mac OS 7. Ah, stai tranquillo: lo sfondo scrivania di default non è leopardato.

<link>Lucio Bragagnolo</link>lux@mac.com