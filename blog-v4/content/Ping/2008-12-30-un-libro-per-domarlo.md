---
title: "Un libro per domarlo"
date: 2008-12-30
draft: false
tags: ["ping"]
---

È uscito un libro base su AppleScript. Il titolo è <a href="http://www.peachpit.com/store/product.aspx?isbn=0321149319" target="_blank">Apple Training Series: AppleScript 1-2-3</a>, di Sal Soghoian e Bill Cheeseman, per i tipi di Peachpit Press.

Costa 49,99 dollari (<a href="http://www.peachpit.com/store/product.aspx?isbn=0321496183" target="_blank">edizione Pdf</a> a 34,99 dollari) sul sito Peachpit, mentre uscirà nelle librerie americane a gennaio e in quelle italiane chissà (per quanto chi abita vicino a una libreria ben gestita può richiederlo al libraio e farlo arrivare).

Pur nutrendo poche speranze, ho intenzione di vellicare i miei contatti editoriali per indagare la possibilità di realizzarne una buona traduzione.

L'inizio del libro <a href="http://www.apple.com/applescript/firsttutorial/index.html" target="_blank">si può assaggiare sulle pagine AppleScript del sito Apple</a>. C'è un ottimo tutorial pieno di script.