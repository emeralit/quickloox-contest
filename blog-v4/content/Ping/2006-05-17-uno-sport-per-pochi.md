---
title: "Uno sport per pochi"
date: 2006-05-17
draft: false
tags: ["ping"]
---

La <a href="http://pcgen.sf.net" target="_blank">nuova versione di PcGen</a>, 5.10, è totalmente un'altra vita rispetto alla 5.8.1 stabile che si usava fino a dieci giorni fa.

PcGen gestisce le schede personaggio e molti altri aspetti del gioco di ruolo da tavolo e le vecchie edizioni erano sempre state un po' penalizzate da lentezze e problemi che man mano stanno sparendo. La 5.10 fa un vero salto di qualità.

Devo risolvere ancora il problema della visualizzazione degli incantesimi preparati dal mago, ma la cosa prescinde dal fatto tecnico. :)