---
title: "Palatino Love"
date: 2008-03-17
draft: false
tags: ["ping"]
---

Grandioso <a href="http://www.kero.it" target="_blank">Neko</a>:

<em>Letto della tua passione per il carattere Palatino m'è venuto in mente che Dean Allen (il creatore di Textdrive, ora <a href="http://www.joyent.com/connector/web-hosting/" target="_blank">Joyent</a>, nonché <em>designer</em> di libri e appassionato di tipografia) ne ha <a href="http://www.textism.com/textfaces/index.html?id=18" target="_blank">scritto un po'</a> sul suo sito:</em>

<cite>&#8220;Ci sono caratteri chiamati Palatino installati su milioni di computer; inevitabilmente una parodia orribile e debole dell'originale. Purtroppo qualsiasi libro pubblicato tra il 1987 e il 1991 è stato composto in qualche brutto tipo di Palatino; può essere il perché in quegli anni ho guardato cos&#236; tanta televisione&#8221;.</cite>

<em>Credo si riferisca agli innumerevoli <a href="http://www.textism.com/article/328/typography-for-writers" target="_blank">cloni del Palatino</a>, venduti sotto diversi nomi come il più celebre di tutti, Book Antiqua:</em> <cite>&#8220;Spesso si tratta di adattamenti di caratteri consolidati; Times New Roman e Palatino sui maggiori sistemi operativi sono piatte parodie dei progetti di Stanley Morison e Hermann Zapf&#8221;.</cite>

<em>Il Palatino su Mac OS X è <a href="http://cg.scs.carleton.ca/~luc/palatino2.html" target="_blank">diverso dall'originale</a> che Zapf disegnò nel 1948:</em> <cite>&#8220;Le versioni odierne di Palatino per stampanti laser e fotounità, per esempio il</cite> Linotype Palatino <cite>per Pc, Mac eccetera, si riconoscono dagli ascendenti ridotti e dalle grazie simmetriche dei discedenti di</cite> p <cite>e</cite> q. <cite>Oggi sono le</cite> p <cite>e</cite> q <cite>corsive sono senza grazie. Hermann Zapf considera il nuovo</cite> Linotype Palatino (1998) <cite>come la versione definitiva, pubblicata per la prima volta nel 1950 da Stempel AG&#8221;.</cite>

<cite>&#8220;Linotype Palatino si discosta tremendamente dal Palatino originale. Per esempio,</cite> E <cite>ed</cite> F <cite>non avevano grazie nelle linee orizzontali di mezzo, le grazie della</cite> k <cite>erano molto più eleganti,</cite> <cite>come quelle della</cite> y<cite>, e soprattutto l'altezza dei caratteri era minore (quindi con ascendenti più alti) a rendere il carattere più elegante, a somiglianza del Bembo&#8221;.</cite>

<em>Controllando in Libro Font noto comunque che il Palatino di OS X non è stato realizzato da Linotype, bens&#236;</em> &#8220;Copyright &#169; 1991-99, 2006 Apple Computer, Inc. Copyright &#169; 1991-92 Type Solutions, Inc. All rights reserved&#8221;.

<em>C'è una sfracca di roba in più da leggere, probabilmente l'hai già letta e riletta, comunque già che c'ero ho cambiato i caratteri del mio blog da Hoefler Text a Palatino. Ora si legge molto meglio! E per il Web è parecchio più coerente di altri caratteri graziati (a parte il Georgia, ovviamente) perché Palatino (Linotype) è installato di serie anche su Windows. :)</em>

Sulla tipografia (e sul Palatino) c'è da leggere all'infinito. Purtroppo non potremo mai pretendere su uno schermo la stessa eleganza che finiva sulle pergamene. Però ribellarsi alla schiavitù degli Arial, questo s&#236;. Grazie. :)