---
title: "L'elettricità spiegata al profano"
date: 2006-04-18
draft: false
tags: ["ping"]
---

La superficie dell'alimentatore di MacBook Pro sta a quella dell'alimentatore del PowerBook Aluminum come il wattaggio richiesto dal primo sta a quello del secondo.