---
title: "Come si fa il web nel 2010"
date: 2010-08-28
draft: false
tags: ["ping"]
---

Vimeo ha dispiegato un <i>player</i> universale per i suoi filmati, anche quelli presenti nelle pagine di altri siti e trasmessi da Vimeo.

In funzione del <i>browser</i> e del tipo di video, viene erogato automaticamente Flash, oppure Html5, oppure un formato nativo.

Qualunque hardware e qualunque software richiedano un video di Vimeo, <a href="http://www.readwriteweb.com/archives/vimeo_releases_embeddable_html5_video_player.php" target="_blank">vengono accontentati</a>.

Il web si fa cos&#236;. Accesso a tutti, sempre.