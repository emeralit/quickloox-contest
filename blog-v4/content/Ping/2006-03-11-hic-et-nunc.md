---
title: "Hic et nunc"
date: 2006-03-11
draft: false
tags: ["ping"]
---

Sono limitato. Invece di vivere per scoprire quale sarà il prossimo annuncio di Apple, mi godo il Mac che ho davanti.

La mia soddisfazione non viene dal sapere che cosa potrò usare domani, ma da ciò che uso oggi. Qui e ora. Sono malato, vero?