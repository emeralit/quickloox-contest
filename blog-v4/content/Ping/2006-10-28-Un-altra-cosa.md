---
title: "Un'altra cosa"
date: 2006-10-28
draft: false
tags: ["ping"]
---

Continuo a usare Safari per ragioni di continuità, compatibilità integrazione, velocità e familiarità, ma il mio browser alternativo è più che mai Firefox. Adesso che è uscita la versione 2, ho provato a usarlo un po' con Gmail e sembrava di volare. Se il Safari di Leopard non si dimostrerà all'altezza &#8212; pare che farà meraviglie, peraltro &#8212; potrei anche meditare in futuro un cambiamento radicale.