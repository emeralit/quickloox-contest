---
title: "In sostituzione"
date: 2009-07-09
draft: false
tags: ["ping"]
---

Come è noto, Apple ha una pagina dedicata alle <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">novità di Snow Leopard</a> e non l'ha ancora tradotta in italiano. In sostituzione ci penso io, una novità al giorno.

Come pochissimi avranno notato, sulla pagina in questione manca la novità di oggi, che si trova unicamente sulla <a href="http://www.apple.com/macosx/refinements/enhancements-refinements.html" target="_blank">pagina americana</a> (e in Snow Leopard, garantisco, c'è).

<b>Sostituzione di testo</b>

La sostituzione di testo permette di creare scorciatoie a frasi più lunghe, che appaiono automaticamente digitando la scorciatoia. Sono di serie le sostituzioni più tipiche, per esempio digitare <i>(c)</i> e ottenere &#169;, oppure digitare <i>1/2</i> e ottenere &#189;. Si possono personalizzare le sostituzioni e decidere che, per esempio, digitare <i>nap3</i> faccia apparire la scritta <i>non aprire prima del Tremila</i> oppure che <i>csm</i> equivalga a scrivere <i>Contessa Serbelloni Mazzanti Vien Dal Mare</i>.

Io scelgo <i>pc</i>: <i>perché cavolo avere pagine diverse per lo stesso contenuto se nessuno le ha ancora tradotte</i>. Me ne sono accorto per puro caso.