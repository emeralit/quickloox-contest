---
title: "Teniamolo nei bookmark"
date: 2007-11-03
draft: false
tags: ["ping"]
---

Mi scriveva <a href="http://freesmug.org" target="_blank">Carlo</a>:

<cite>Ti segnalo <a href="http://www.cegroup.it/IT/CeGroup-Education-Zona.aspx?Z=EDUCATION&amp;M=275" target="_blank">Teniamoci per Mouse 2007</a>, convegno sulle tecnologie per i docenti di scuole elementari e medie, sponsorizzato anche da Apple, in cui intervengo sull'<em>open source</em></cite>. Il prossimo 29 novembre.

I docenti della Grande Milano in lettura sono sollecitati, perché il tema urge e vale. I non docenti che non hanno ancora scoperto il FreeSmug sono sollecitati, perché il software libero vale, che urga o meno.