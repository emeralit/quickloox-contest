---
title: "Infinite Websites"
date: 2008-05-30
draft: false
tags: ["ping"]
---

È un momento di siti da ricordare e stavolta tocca a <strong>EvTheThinker</strong>, che ha ridisegnato <a href="http://infinite-labs.net/" target="_blank">il proprio sito</a>.

Come sviluppatore Mac, giovane, italiano e intelligente è <em>rara avis</em> e il lavoro degli &#8734;Labs avrà sempre il mio sostegno.

E poi oggi è presente all'<a href="http://www.macworld.it/blogs/ping/?p=1795">evento NeoOffice in @Work</a>. Voglio fare bella figura. :-)