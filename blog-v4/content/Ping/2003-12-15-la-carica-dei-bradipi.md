---
title: "La carica dei bradipi"
date: 2003-12-15
draft: false
tags: ["ping"]
---

Sony: il marchio dei tardony

Elisa Tacconi. Ha appena realizzato la sua prima compilation musicale.
Simona Mapelli. Ha appena sfogliato alla Tv il suo primo album fotografico digitale.
Massimo Bezzi. Ha appena creato il suo primo film su Dvd.

Sono i protagonisti della campagna di affissione murale Sony di questo periodo. Se avessero avuto un Mac, sarebbero alla cinquantottesima compilation musicale, pubblicherebbero il ventiquattresimo album digitale su Internet e sarebbero alla loro decima scatola di Dvd. Sono anni che con un Mac si fanno tutte queste cose, e meglio. Loro se ne sono accorti solo ora, peggio dei bradipi.

Come chiamarli? Ritardatari? Addormentati? Ignoranti? Male informati? Sprovveduti? Sempliciotti?

Tardony.

<link>Lucio Bragagnolo</link>lux@mac.com