---
title: "Non di solo software"
date: 2002-09-06
draft: false
tags: ["ping"]
---

Se Apple smettesse di costruire Mac e si limitasse a vendere Mac OS X?

Ogni tanto un bello spirito ipotizza che Apple dovrebbe smettere di vendere computer e mettersi a vendere Mac OS X per i Pc. Dice, ne venderebbe talmente tanti che guadagnerebbe quanto e più di adesso.

Sì, certo; si metterebbe a fare concorrenza a una Microsoft qualsiasi – una che per le tue tatticucce anticoncorrenza è stata persino riconosciuta colpevole in tribunale – con un sistema operativo fantastico che di punto in bianco convincerebbe decine di milioni di windowsisti a buttare via tutto il loro parco software, acquistare un sistema operativo sconosciuto e scoprire che è senza software, dato che tutti i programmi che esistevano per quel sistema non funzionano più.

Quelli da convincere non sarebbero neanche pochi; per compensare la mancata vendita di un buon Mac Apple dovrebbe vendere almeno una quindicina di copie di Mac OS X.

Apple guadagna vendendo computer, non software; chi invoca Mac OS X per Intel (“così posso usare Mac OS X comprando un computer che costa poco”) pensa di essere furbo. Peccato che Apple finirebbe per diventare una Microsoft qualsiasi, ammesso che riuscisse a sopravvivere.

Sono ipotesi che valgono veramente poco. Come i computer che costano poco.

<link>Lucio Bragagnolo</link>lux@mac.com