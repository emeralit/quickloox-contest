---
title: "Ritagliati e trascinati"
date: 2005-07-10
draft: false
tags: ["ping"]
---

Finalmente in Tiger è possibile

Hai presente i clipping, i ritagli che si ottengono per esempio trascinando del testo con un drag and drop sopra la scrivania?

Con Tiger (e prima invece no) è possibile trascinarli sulle icone appropriate del Dock, con qualche utilità.

Trascini un Url e Safari lo carica, trascini un ritaglio di testo e lui lo cerca su Google, lo stesso ritaglio trascinato in BBEdit apre una finestra del programma con dentro il testo in questione e via dicendo.

Ci avrà pensato un programmatore di buona volontà in un… ritaglio di tempo.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>