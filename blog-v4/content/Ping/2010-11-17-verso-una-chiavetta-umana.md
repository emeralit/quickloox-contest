---
title: "Verso una chiavetta umana"
date: 2010-11-17
draft: false
tags: ["ping"]
---

Torno da un mezzo pomeriggio in riunione presso Vodafone Italia con una notizia simpatica. L’offerta di connessione inizia finalmente a non basarsi più sul tempo o sul peso dei dati scaricati, variabili degnissime e tuttavia, prese come unica discriminante, prive di significato.

Al tempo stesso è stato evitato il ragionamento alternativo che si è sentito spesso, peggiore del male cui pretendeva di porre rimedio: penalizzare o favorire il traffico a seconda del tipo, come assassinare YouTube o il peer-to-peer perché pesano tanto e favorire l’email oppure la chat perché pesano meno. Sono un accanito sostenitore di Internet come luogo fantastico per generare contenuti e parlare con tutti e rispetto, ma non approvo, quelli che vedono la Rete come serbatoio di roba da scaricare e più scarichi e più scarichi gratis più sei furbo. Detto questo, oggi potrei essere intelligente ma domani stupido e qualunque meccanismo di tariffazione basato su quanto mi considera intelligente il provider sarebbe palesemente ingiusto, oltre che destinato a funzionare male.

Sarà invece chi usa la banda a decidere, o meglio finalmente a ragionare, su che uso fare della rete. Il prezzo varierà in funzione del tetto massimo di banda, su abbonamento e ricaricabili, per ora solo su chiavette e iPad ma in seguito anche su cellulari e smartphone. Il minimo è a nove euro mensili non stop, senza limiti di tempo, con tetto a un gigabyte di dati e a 1,8 megabit per secondo di banda. Superando il giga non si pagano extra ma si procede a 64 kilobit per secondo fino alla scadenza del mese, salvo decidere di passare a una tariffa superiore oppure rinnovare la stessa tariffa subito e ripartire con un altro gigabyte per altri trenta giorni a partire da quel momento (il mese solare non interessa).

È una tariffa che interessa persino me su iPad. Spenderei uno-tre euro in più al mese, ma avrei connettività totale e continua.

La tariffa superiore prevede 19 euro al mese con tre gigabyte di dati e tetto di banda a 7,2 megabit per secondo (e chiavetta inclusa). Quella ancora sopra si ferma ai cinque gigabyte e ai 28,8 megabit per secondo. Su questa tariffa, per chi sia disposto a 35 euro al posto di 29, si possono avere due Sim che puntano allo stesso account e che dividono il tetto di cinque gigabyte.
Niente costi aggiuntivi, niente sorprese in bolletta, libertà di cambiare tariffa verso l’alto o verso il basso. Non è ancora il paese delle meraviglie, ma sembra un bel passo in avanti, che inizierà dal 29 novembre.
Di passaggio, il Pc che proiettava la presentazione a un certo punto doveva mostrare due differenti animazioni Flash. La prima, dopo vari tentativi e l’intervento di un’altra persona oltre al relatore, è partita. Per la seconda non c’è stato verso.
