---
title: "Le Mail e una notte (buttata)"
date: 2008-01-13
draft: false
tags: ["ping"]
---

Riparazione dei privilegi? Roba da dilettanti.

Ci sono modi molto più sofisticati per perdere tempo e intanto convincersi di avere il controllo del Mac. Anzi, di avere il Mac <em>in ordine</em>, come se fosse lo scaffale dei libri.

Si può dare un comando SqLite da Terminale per <a href="http://www.friday.com/bbum/2007/03/02/vacuuming-mails-envelope-index-to-make-mail-faster/" target="_blank">ottimizzare qualche database interno di Mail</a>. Un'operazione di fatto inutile per recuperare seriamente spazio su disco e che non è automatizzata proprio perché non è il caso di eseguirla spesso, meno che meno manualmente.

Ma vuoi mettere, l'illusione di avere voce in capitolo nelle segrete del filesystem? Dopotutto, se il comando è presente, ci sarà un motivo, no?

Qualcuno ha anche realizzato <a href="http://www.appleupgrade.co.uk/page1/page1.html" target="_blank">un AppleScript per fare la stessa cosa</a>.

Si può fare anche con Aperture e anche su scala più grande. Non c'è limite a quello che si può fare con il computer per il computer invece che usare il computer per noi e per i nostri scopi.