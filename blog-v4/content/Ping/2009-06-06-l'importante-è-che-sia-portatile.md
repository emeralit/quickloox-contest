---
title: "L'importante è che sia portatile"
date: 2009-06-06
draft: false
tags: ["ping"]
---

I festeggiamenti per il settimo anno del museo Apple continuano (e terminano) con la pizza serale.

Tempo di mettere via i portatili e andare.

Beh, ai tempi del primo Macintosh c'era la sua bella borsa. Dopotutto il computer pesava solo sedici chili.
