---
title: "Parlando… di pizza"
date: 2009-12-04
draft: false
tags: ["ping"]
---

Se qualcuno passa da @Work in Milano stasera, dalle 18 in poi, mi trova sicuro, a chiacchierare di come si vive (benissimo) senza Word e in generale senza Office.

Se poi passa dalla pizzeria Bio Solaire in via Terraggio 20, tre minuti a piedi da <a href="http://www.atworkgroup.net/puntiVendita.php" target="_blank">@Work</a>, dalle 20-20:30 in avanti, sono a concludere la serata in allegria con chi condivide.