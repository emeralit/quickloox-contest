---
title: "Temi di classe"
date: 2009-12-14
draft: false
tags: ["ping"]
---

Per cambiare colore a una giornata grigia vale la pena di provare i temi grafici di Gmail (ultima sezione delle Impostazioni). C'è di tutto e nessuno ci guarda.