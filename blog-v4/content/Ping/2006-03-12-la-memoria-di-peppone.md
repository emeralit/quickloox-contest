---
title: "La memoria di Peppone"
date: 2006-03-12
draft: false
tags: ["ping"]
---

Sabato primo aprile parteciperò come socio all'assemblea annuale del <a href="http://www.giovanninoguareschi.com/23club2.htm" target="_blank">Club dei Ventitr&eacute;</a>. A parte il fatto che si tratta di persone squisite e che nella Bassa si mangia meravigliosamente bene, nella sede del Club esiste un eMac di cui mi occupo occasionalmente. Per un anno intero l'eMac ha prodotto centinaia di scansioni di foto e giornali d'epoca… con 64 megabyte di Ram e Mac OS X.

Ora quei tempi sono passati, ma mi servono da promemoria per quando faccio lo sbruffone e annuncio di avere due gigabyte di Ram nel PowerBook come se fossero una vera neccessità.