---
title: "Mai troppo esposti"
date: 2009-06-27
draft: false
tags: ["ping"]
---

Non c'è molto che riguardi il Dock nella <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">pagina delle novità di Snow Leopard</a> che Apple ancora non traduce, ma le migliorie evolutive sono talvolta interessanti, come questa.

<b>Attivare Exposé dal Dock</b>

Facendo clic e tenendolo premuto sopra un'icona di applicazione nel Dock, tutte le finestre dell'applicazione selezionata si visualizzeranno, rendendo semplice il passaggio tra finestre. Stando dentro Exposé, premere il tabulatore procede alla prossima applicazione e ne mostra le finestre. Le finestre minimizzate appaiono come icone più piccole sotto le altre finestre. E le finestre sono a impulso, cos&#236; si possono trascinare elementi da una finestra a un'altra.

Forse una delle modifiche più comode nell'uso giorno per giorno.