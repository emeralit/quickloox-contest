---
title: "È finita un'era"
date: 2011-03-03
draft: false
tags: ["ping"]
---

Dedicato ai fanatici del processore, della nuova Usb a tutti i costi, del <cite>più è grande meglio è</cite>.

<cite>Nel Dna di Apple c'è la nozione che la tecnologia da sola non è abbastanza. È la tecnologia sposata con le arti liberali, con l'umanità, che produce risultati capaci di fare cantare il cuore. Da nessuna parte ciò è vero come in questi apparecchi post-Pc. E un sacco di persone in questo mercato di tavolette guarda a esse come al prossimo Pc.</cite>

<a href="http://news.cnet.com/8301-17852_3-20038425-71.html" target="_blank">Steve Jobs</a>, indovinare alla presentazione di che cosa.

Fino a che Apple riesce a conservare questa visione e gli altri riescono a muoversi con l'attuale goffaggine, non c'è veramente partita.