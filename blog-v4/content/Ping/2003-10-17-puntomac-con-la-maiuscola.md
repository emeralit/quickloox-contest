---
title: ".mac con la maiuscola"
date: 2003-10-17
draft: false
tags: ["ping"]
---

Non ne parla nessuno e invece si dovrebbe

Sono abbonato a .mac, il servizio di Apple che dà diritto a cento megabyte di disco virtuale, l’indirizzo @mac.com e si interfaccia facilmente con iCal, iSync, Backup e il resto della panoplia di prodotti software di Apple.

Dopo la mossa controversa in cui Apple aveva fatto diventare il servizio a pagamento molti si sono ribellati o semplicemente hanno preferito non abbonarsi. Tutto normale.

È meno normale, però, che nessuno ora dica più una parola sulla crescita di .mac. Quando mi sono collegato per rinnovare l’abbonamento, ho scoperto che avevo diritto, a scelta, a Everquest, The Sims oppure venti dollari di sconto sull’Applestore. Poi ho scaricato gratis iBlog, Marble Blast e, se ne fosse valsa la pena, persino una raccolta di solitari. Ci sono vari sconti su altro software e in più eccellenti lezioni su come usare Mac OS X, iMovie e altro. I nuovi iscritti ricevono in regalo VersionTracker Plus e uno sconto su VersionTracker Pro. E così via.

Senza contare i servizi dati per scontati, che tra l’altro con Mac OS X 10.3 guadagnano ulterioramente funzionalità grazie ai continui progressi di implementazione di WebDav.

Sarà anche <link>.mac</link>http://www.mac.com, ma si merita la maiuscola.

<link>Lucio Bragagnolo</link>lux@mac.com