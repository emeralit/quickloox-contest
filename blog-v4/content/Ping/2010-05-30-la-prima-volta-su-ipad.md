---
title: "La prima volta su iPad"
date: 2010-05-30
draft: false
tags: ["ping"]
---

Solo per provare la fruibilità di Wordpress per iPad e la tastiera virtuale.
Seguiranno impressioni di utilizzo e varie ed eventuali. Per il momento: anziché sincronizzare come se fosse stato un secondo iPhone, lho fatto considerandolo un nuovo apparecchio, secondo me cosa necessaria anche al prezzo di dover reinserire qualche informazione. La compatibilità con le applicazioni iPhone è buona ma non perfetta; si scoprono migliorie sottili a ogni angolo e loggetto conquista in modo inverosimile le persone più inaspettate.
Vedo iPad sotto tre luci diverse: un oggetto in grado di sostituire spesso il portatile on the road in compiti tipici da portatile; un oggetto che svolga meglio funzioni per le quali un portatile non è perfettamente indicato e infine un oggetto che crei opportunità inedite di utilizzo. Mi aspetto buone cose dalla prima, meraviglie dalla seconda e non so proprio dalla terza. Il viaggio però si annuncia entusiasmante. Tra cinque anni il computer for the rest of us sarà fatto in questo modo. Questa è una avanguardia. Per inciso la tastiera non è certo il sistema per scrivere un libro, ma accidenti se si presta per una manciata di righe. Complimenti ai progettisti, non ci avrei messo due lire. Meglio, mi aspettavo una esperienza stile iPhone, mancando clamorosamente il bersaglio.
Per ora è abbastanza. Ne vedrò delle belle e incoraggerò certi parenti luddisti… per loro è finalmente loccasione di un nuovo orizzonte.