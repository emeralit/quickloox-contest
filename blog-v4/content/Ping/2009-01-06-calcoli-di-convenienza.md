---
title: "Calcoli di convenienza"
date: 2009-01-06
draft: false
tags: ["ping"]
---

Mi sono augurato per il 2009 l'estinzione degli imbecilli capaci di sostenere che non conviene sviluppare software per Mac OS X per via della sua ridotta quota di mercato.

So che questo augurio non andrà a buon fine; le capacità procreative delle mamme degli imbecilli sono infinite.

Come promemoria mi tengo questo grafico di Wolfire Software: per loro, supportare anche Mac OS X e Linux ha significato <a href="http://blog.wolfire.com/2008/12/why-you-should-support-mac-os-x-and-linux/" target="_blank">aumentare le vendite del 122 percento</a>.

E vagli a raccontare delle quote di mercato.