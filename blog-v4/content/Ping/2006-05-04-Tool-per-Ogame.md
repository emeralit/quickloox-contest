---
title: "Tool per Ogame"
date: 2006-05-04
draft: false
tags: ["ping"]
---

Rispetto alla richiesta di Simone apparsa in un commento, non ho trovato gran che, ma qualcosa c'è: <a href="http://drago-sim.com/index.php?lang=english" target="_blank">DragoSim</a>, che è un simulatore utilizzabile direttamente via Web, e i tool di <a href="http://www.galaxietool.de/english/" target="_blank">Galaxietool</a> che però richiedono un minimo di conoscenza per poter essere installati.

Inoltre terrei d'occhio <a href="http://meshi-og-stats.de.vu/" target="_blank">meshi-og-stats</a>, che promette presto una versione in inglese. Se Simone sa il tedesco, nessun problema invece. :)