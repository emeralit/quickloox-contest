---
title: "Alan For d(ownload)"
date: 2007-03-21
draft: false
tags: ["ping"]
---

Un regalo da <strong>Alan Dragster</strong>: <a href="http://senduit.com/" target="_blank">senduit</a>, per inviare file di grosse dimensioni.

Il metodo più efficiente resta il trasferimento diretto. Se c'è tempo, però, questo pare elegante. In un servizio web è sempre un buon indizio.