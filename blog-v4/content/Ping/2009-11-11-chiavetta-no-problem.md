---
title: "Chiavetta no problem"
date: 2009-11-11
draft: false
tags: ["ping"]
---

Ho risolto almeno un problema di connessione da Snow Leopard a Internet via chiavetta E169 Huawei con marchio Tim, scaricando il relativo <a href="http://www.huaweidevice.com/resource/mini/200910149695/testmobile1014/index.html?directoryId=3874&amp;treeId=0" target="_blank">driver aggiornato dal sito Huawei</a> e ignorando bellamente il software in dotazione alla chiavetta stessa in fase di installazione e configurazione.

Probabilmente la soluzione si applica anche a chiavette marchiate altrimenti, dato che in gran parte sotto il cofano c'è sempre un motore Huawei.