---
title: "La casa di Steve (e la foto di Jim)"
date: 2006-02-06
draft: false
tags: ["ping"]
---

Prosegue la pubblicazione dei <a href="http://www.allaboutapple.com/speciali/s_francisco_2006_b.htm" target="_blank">ricordi di viaggio</a> in California della delegazione dell'All About Computer Club. C'è davvero un sacco da leggere e, per chi ha perso l'alfabetizzazione elementare, un sacco di figure da guardare!