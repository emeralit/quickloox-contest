---
title: "Rinnovati al cubo"
date: 2006-02-11
draft: false
tags: ["ping"]
---

Una ne fanno, una ne pensano e così procedono alla frequenza di lavoro di un G5 Quad.

Sulle pagine dell'<strong>All About Apple Club</strong> si possono vedere le immagini che mettono <a href="http://www.allaboutapple.com/speciali/cube.htm" target="_blank">a confronto il cubo di NeXt con quello di Apple</a> e anche come è cambiato (in meglio) <a href="http://www.allaboutapple.com/speciali/il_nuovo_volto.htm" target="_blank">l'arredo del museo</a>.

Il tutto magicamente, mentre loro se la spassavano a Cupertino.

Scherzo. Bravi. Veramente.