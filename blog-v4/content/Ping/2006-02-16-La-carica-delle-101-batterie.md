---
title: "La carica delle 101 batterie"
date: 2006-02-16
draft: false
tags: ["ping"]
---

La raccolta dei dati sulla vita delle batterie prosegue a ritmo notevole. Qui sotto pubblico i risultati aggiornati.

Per chi non avesse ancora contribuito: per favore usa solamente <a href="http://www.coconut-flavour.com/coconutbattery/" target="_blank">Coconut Battery</a> su Tiger e <a href="http://www.branox.com/Battorox/Battorox.html" target="_blank">Battorox</a> per Panther e precedenti. Quesat’ultimo non considera i mesi di vita e indica invece la data del computer. Bisogna calcolare a mano, ma non dovrebbe essere un dramma.

Per Coconut Battery: non interessa la <em>charge</em>, quella sopra, ma la <em>capacity</em>, quella sotto, per ragioni intuibili.

Ottimo il suggerimento di mettere su una paginetta con un form e un database dietro… ma qui si fa già fatica ad avere dai tecnici un blog, immagina il resto. :-D

Naturalmente un grosso grazie a tutti!

<table>
	<tr>
		<th></th> <th>Months</th> <th>Loadcycles</th> <th>Percentage</th>
	</tr>
	<tr>
		<td>Lux</td> <td>13</td> <td>123</td> <td>84</td>
	</tr>
	<tr>
		<td>Francos</td> <td>4</td> <td>74</td> <td>99</td>
	</tr>
	<tr>
		<td>Jacopo</td> <td>13</td> <td>181</td> <td>49</td>
	</tr>
	<tr>
		<td>Daniele</td> <td>38</td> <td>1193</td> <td>50</td>
	</tr>
	<tr>
		<td>Giancarlo</td> <td>13</td> <td>118</td> <td>19</td>
	</tr>
	<tr>
		<td>Matteo</td> <td>33</td> <td>192</td> <td>78</td>
	</tr>
	<tr>
		<td>Andrea</td> <td>31</td> <td>285</td> <td>75</td>
	</tr>
	<tr>
		<td>Miki</td> <td>12</td> <td>131</td> <td>98</td>
	</tr>
	<tr>
		<td>Citrullo</td> <td>21</td> <td>177</td> <td>85</td>
	</tr>
	<tr>
		<td>Luca</td> <td>22</td> <td>203</td> <td>90</td>
	</tr>
	<tr>
		<td>Elio</td> <td>24</td> <td>372</td> <td>55</td>
	</tr>
	<tr>
		<td>Vincenzo</td> <td>8</td> <td>95</td> <td>96</td>
	</tr>
	<tr>
		<td>Andrea2</td> <td>48</td> <td>337</td> <td>81</td>
	</tr>
	<tr>
		<td>Caruso_G</td> <td>2</td> <td>75</td> <td>101</td>
	</tr>
	<tr>
		<td>Pierpa</td> <td>15</td> <td>229</td> <td>80</td>
	</tr>
	<tr>
		<td>Mario</td> <td>28</td> <td>643</td> <td>0</td>
	</tr>
	<tr>
		<td>Fabrich</td> <td>28</td> <td>159</td> <td>84</td>
	</tr>
	<tr>
		<td>Armando</td> <td>9</td> <td>33</td> <td>100</td>
	</tr>
	<tr>
		<td>Armando2</td> <td>17</td> <td>37</td> <td>85</td>
	</tr>
	<tr>
		<td>Todi</td> <td>37</td> <td>204</td> <td>77</td>
	</tr>
	<tr>
		<td>Sergio</td> <td>47</td> <td>88</td> <td>18</td>
	</tr>
	<tr>
		<td>Paolo</td> <td>27</td> <td>48</td> <td>59</td>
	</tr>
	<tr>
		<td>AndreaR</td> <td>21</td> <td>143</td> <td>92</td>
	</tr>
	<tr>
		<td>Gimli</td> <td>20</td> <td>434</td> <td>61</td>
	</tr>
	<tr>
		<td>Ellebi</td> <td>38</td> <td>502</td> <td>77</td>
	</tr>
	<tr>
		<td>Sistovmac</td> <td>9</td> <td>152</td> <td>96</td>
	</tr>
	<tr>
		<td>Telemaco</td> <td>31</td> <td>17</td> <td>98</td>
	</tr>
	<tr>
		<td>Fede</td> <td>50</td> <td>36</td> <td>100</td>
	</tr>
	<tr>
		<td>Daniele</td> <td>15</td> <td>704</td> <td>73</td>
	</tr>
	<tr>
		<td>Salvo</td> <td>10</td> <td>117</td> <td>91</td>
	</tr>
	<tr>
		<td>Fearandil</td> <td>33</td> <td>496</td> <td>99</td>
	</tr>
	<tr>
		<td>Antonio</td> <td>21</td> <td>144</td> <td>93</td>
	</tr>
	<tr>
		<td>Andrea f</td> <td>22</td> <td>410</td> <td>85</td>
	</tr>
</table>
