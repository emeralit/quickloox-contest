---
title: "Apple è dieci anni avanti"
date: 2007-12-08
draft: false
tags: ["ping"]
---

L'immagine di Dell sul mercato <a href="http://www.businessweek.com/technology/content/nov2007/tc20071129_057869.htm" target="_blank">non è buona</a>. L'azienda non ha problemi finanziari, ma la sua crescita continua a rallentare e la sua quota di mercato si sgonfia come un budino dimenticato fuori dal frigorifero. Come conseguenza, il valore delle azioni è in forte diminuzione.

Praticamente un ritratto di Apple nel 1997.

Solo che nel 1997 è tornato Steve Jobs e le cose sono cambiate. Nel 2007, in Dell, è tornato Michael Dell, ma ancora non è cambiato niente.

Questo giusto per ricordare che proprio dieci anni fa Michael Dell dichiarava pubblicamente che Apple avrebbe fatto meglio a <a href="http://www.macdailynews.com/index.php/weblog/comments/apple_passes_dell_in_market_value/" target="_blank">chiudere e ridare i soldi</a> agli azionisti.