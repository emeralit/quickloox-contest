---
title: "Il buco nello Zune"
date: 2006-11-12
draft: false
tags: ["ping"]
---

Non l’ho visto, non l’ho provato, non l’ho studiato, non ho letto recensioni a parte l’<a href="http://ptech.wsj.com/ptech.html" target="_blank">articolo</a> di Walt Mossberg sul Wall Street Journal che mi ha ricordato <strong>Carmelo</strong> (grazie). Nell’articolo si spiega che a costruire Zune è stato il team che ha costruito la Xbox

Sono convinto che Zune (nello slang del Quebec, sinonimo di organo genitale) sarà un buco. Ah, venderà un sacco, come la Xbox. Ah, continueranno a farlo, come la Xbox. Ah, Microsoft ci investirà un sacco di soldi, come ha fatto per la Xbox. Eh, resterà marginale, come la Xbox.