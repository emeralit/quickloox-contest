---
title: "Sistemi più che programmi"
date: 2007-09-11
draft: false
tags: ["ping"]
---

Dimentichiamoci per un attimo dei compiti di basso livello del sistema operativo: interfacciamento con i dischi, le stampanti, la Cpu eccetera. Facciamo finta che provveda qualche entità benevola.

Concentriamoci invece sui compiti di alto livello del sistema operativo. Parlare con i programmi, recepire comandi dall'interfaccia grafica eccetera.

Messa in questo modo, ci sono programmi che potrebbero tranquillamente sostituire il sistema operativo.

Il primo che mi è venuto in mente è il Terminale, ma era troppo facile. Serve a quello, non è una novità. Allora pure Script Editor.

Poi, però, mi è venuto in mente BBEdit. Ha un browser del disco, ha i Workbook in cui dare comandi al sistema&#8230; potrei quasi controllare il computer usando solo BBEdit.

Se fossi concentrato unicamente sui media, iTunes sarebbe un altro programma con cui potrei controllare l'intero computer, o quasi.

Ancora: un programma per creare e gestire siti Web, da Dreamweaver in poi, è un altro programma-sistema.

Nel tuo computer c'è qualche programma-sistema? Se s&#236;, quale?