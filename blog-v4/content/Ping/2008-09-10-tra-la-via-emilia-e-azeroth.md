---
title: "Tra la via Emilia e Azeroth"
date: 2008-09-10
draft: false
tags: ["ping"]
---

Sul treno del ritorno armeggio distrattamente con il computer da tasca.

Uno è seduto alla mia destra, l'altro sulla fila di fronte, un sedile a sinistra rispetto alla signora che mi fronteggia e passa in rassegna le suonerie.

Sono entrambi sui trentaqualcosa, tranquilli, lavoro di ufficio, famiglia, rilassati.

Quello alla mia destra è un mago.

L'altro non capisco bene, un paladino o un <em>tank</em>. Stanno progettando un due contro due. Pare che gli avversari saranno un irregolare e un guaritore, o un altro mago.

Passano in rassegna armamenti, vettovaglie, tattiche.

Scendono poco prima di me. Probabilmente non li incontrerò mai più. Eppure può darsi che i nostri personaggi si incroceranno altre cento volte in <a href="http://www.wow-europe.com" target="_blank">World of Warcraft</a>.

Io termino il <em>post</em>, questo, e lo programmo su Ping. La signora davanti a me prosegue la sua <em>jam session</em>.

Internet sta cambiando il mondo. Qualcuno se ne accorgerà un po' più tardi, ecco.