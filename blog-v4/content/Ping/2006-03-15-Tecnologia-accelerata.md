---
title: "Tecnologia accelerata"
date: 2006-03-15
draft: false
tags: ["ping"]
---

Sarà che le vedo di rado, ma in <a href="http://www.macatwork.net" target="_blank">Mac@Work</a> sta per cominciare un evento Epson-Wacom-Microsoft e, se la prima è abbastanza prevedibile e l'ultima è meglio perderla che trovarla, le tavolette grafiche sono invece sempre più sorprendenti ogni volta che ho l'occasione di vedere dal vivo le novità.