---
title: "C’è posta per Mac OS X"
date: 2002-03-26
draft: false
tags: ["ping"]
---

Quando un programma merita, merita. Anche se si paga

Annuncio al colto e all’inclita che tra due giorni abbandonerò, per sempre o quasi, Mac OS Classic. Non te ne frega niente e invece dovrebbe, perché la ragione è la disponibilità di Mailsmith per Mac OS X.
Mailsmith è un programma professionale per la gestione della posta elettronica realizzato da Bare Bones, gli autori di BBEdit: uno dei migliori programmi di tutti i tempi su Mac. E Mailsmith non è da meno. Presente da anni su Mac OS e ormai collaudatissimo, arriva su Mac OS X con alcune funzioni aggiuntive davvero interessanti: in questo spazio ridotto posso citare il fatto che è AppleScript recordable, integra il riferimento a SpamCop per combattere la posta indesiderata, ha clipboard multiple e il trattamento del testo è a livello di quello, magistrale, di <link>BBEdit</link>http://www.barebones.com/products/bbedit.html.
Che cosa ci guadagno a parlarne così bene? Niente. Sono utente di Mailsmith da anni e pagherò il mio bravo upgrade, come tutti.
Sì, <link>Mailsmith si paga ed è giusto così. Prima di tutto lo produce una software house che si guadagna da vivere scrivendo eccellente software esclusivamente per Macintosh. Secondo, è un programma che vale tutti i soldi che costa. Non come altri, che non valgono neanche la svendita e vengono regalati a scopo di monopolio.

<link>Lucio Bragagnolo</link>lux@mac.com