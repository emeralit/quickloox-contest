---
title: "Come lavora Microsoft"
date: 2007-04-19
draft: false
tags: ["ping"]
---

<cite>Mi sto chiedendo se non sia il caso di provare a rendere le estensioni Acpi più specifiche per Windows.</cite>

<cite>È una sfortuna se dobbiamo fare un lavoro, convinciamo i nostri partner a fare lo stesso lavoro e il risultato è che Linux funziona alla grande senza dover fare quel lavoro.</cite>

<cite>Può darsi che non si possa evitare il problema, ma mi dà fastidio.</cite>

<cite>Potremmo forse definire le Api in modo che funzionino bene con Nt e non con gli altri sistemi, anche se sono aperte.</cite>

<cite>Oppure potremmo brevettare qualcosa di correlato.</cite>

Questo è Bill Gates, 1999, in una <a href="http://antitrust.slated.org/www.iowaconsumercase.org/011607/3000/PX03020.pdf" target="_blank">mail interna confidenziale</a> resa pubblica per via di un processo antitrust.

Non so se si capisce. Non va bene che Linux possa funzionare meglio. Magari cambiamo qualcosa in modo che funzioni bene solo Windows. Oppure brevettiamo qualcosa per mettere un ostacolo.

Se Linux funziona meglio di Windows, non si studia per migliorare Windows, ma per azzoppare Linux.

Guarda la tua copia di Office. Stai dando soldi a quest'uomo. E se in questo momento stesse lavorando per azzoppare te?