---
title: "Due storie, un tema"
date: 2009-11-10
draft: false
tags: ["ping"]
---

Due notizie solo apparentemente sconnesse.

Av-Comparative ha effettuato un test della capacità degli antivirus di eliminare il <i>malware</i> (non identificare, eliminare quello che si è identificato). <a href="http://www.net-security.org/malware_news.php?id=1137" target="_blank">Nessuno dei programmi collaudati</a> ha soddisfatto appieno. Per gli interessati c'è anche <a href="http://www.av-comparatives.org/images/stories/test/removal/avc_removal_2009.pdf" target="_blank">il Pdf che dettaglia i risultati</a>.

Seconda storia. Se si ha un iPhone <i>e</i> si è fatto il <i>jailbreak</i> <i>e</i> si è installato <code>ssh</code> <i>e non</i> si è modificata la password, si è a rischio di un <i>worm</i> che non ci vuole certo un genio a scrivere, visto che di fatto un iPhone in questo stato è aperto. Chi volesse può leggersi <a href="http://blog.jeltel.com.au/2009/11/interview-with-ikee-iphone-virus.html" target="_blank">un'intervista con l'autore</a>, contenente anche le istruzioni per uscirne. Il worm dà solo fastidio e non fa danni, per adesso.

Il tema. Siamo in un altro secolo. La sicurezza non è più questione di sistemi, di software, di meccanismi. È questione di cervello, di intelligenza, di attenzione e di ricchezza. Non quella dei soldi. Quella del pensiero. I comportamenti <i>cheap</i>, da straccioni, non hanno a che vedere con il risparmio di denaro, ma con il vuoto mentale. E mettono a rischio più di ogni <i>firewall</i> malconfigurato.

Fermo restando che, parlando di sicurezza, chiunque scelga consapevolmente di avviare Windows ha per lo meno idee confuse. Tra un tuffo in acque pulite e uno in acque inquinate io sceglierei il primo, anche se certamente potrei buttarmi in acqua sporca cosparso lungo tutto il corpo di sostanza protettive.