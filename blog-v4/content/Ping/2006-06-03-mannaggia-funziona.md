---
title: "Mannaggia, funziona!"
date: 2006-06-03
draft: false
tags: ["ping"]
---

Ricevo da <strong>Alex</strong> e pubblico integralmente, ch&eacute; merita.

<cite>Lo so, sono anche &ldquo;capace&rdquo; di &ldquo;fare&rdquo; siti web, ma non ho più n&eacute; il tempo n&eacute; la voglia. Così per presentare il mio progetto ho convertito in 25 minuti da Pages la cartella stampa del mio prossimo lavoro <a href="http://www.videofaker.com/kds/index.html" target="_blank">copiandincollando in iWeb</a>.</cite>

<cite>Non è un capolavoro, ha ancora delle cose da aggiustare, ci sono alcune funzionalità che mi stanno letteralmente sulle p… ma, mannaggia, funziona!</cite>

Credo che per un ingegnere software di Apple potrebbe essere un bel complimento.