---
title: "A favore della ricerca"
date: 2009-06-23
draft: false
tags: ["ping"]
---

Era già successo l'anno scorso. C'era una pagina dedicata alle <a href="http://www.apple.com/macosx/refinements/enhancements-refinements.html" target="_blank">novità del prossimo sistema operativo Apple</a> ma mancava la traduzione italiana.

Anche l'anno scorso avevo presentato una novità per volta dino alla comparsa della traduzione. E cos&#236; oggi.

<b>Risultati di ricerca ordinabili</b>

Organizziamo i risultati di ricerca di Spotlight per nome, data di modifica, data di creazione, dimensioni, tipo, etichetta. Basta aprire il menu Azione, scegliere Organizza per e selezionare il campo di ordinamento.

All'amico <b>Giuliano</b> piacerà un sacco, lo aspettava da una vita.