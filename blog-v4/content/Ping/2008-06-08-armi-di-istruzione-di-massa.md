---
title: "Armi di istruzione di Massa"
date: 2008-06-08
draft: false
tags: ["ping"]
---

Quando leggo delle introduzioni a raffica di computer e tecnologia nelle scuole americane mi viene sempre il magone, pensando alla nostra, di scuola.

Poi scopro che esistono realtà spettacolari come l'Istituto Barsanti di Massa, dove a suon di <em>wireless</em> e portatili Mac sono stati raggiunti <a href="http://web.mac.com/barsantiwifi/" target="_blank">risultati straordinari</a>, non fini a sé stessi e invece positivi per i ragazzi e i docenti.

Consiglio la visita al sito, che è estremamente chiaro nello spiegare i perché e i percome del progetto, con vantaggi, caratteristiche, particolarità. È un'arma formidabile a disposizione di chiunque vorrebbe parlarne nella propria scuola ma non sa da dove cominciare.

Grazie a <strong>Emanuele Donetti</strong>, il docente responsabile, per la segnalazione!