---
title: "Consolare gli afflitti"
date: 2006-02-05
draft: false
tags: ["ping"]
---

Ricordi <strong>Maurizio Lazzaretti</strong> di PcWorld e la sua <a href="http://www.pcw.it/showPage.php?template=attualita&amp;id=1527" target="_blank">intemerata</a> sulla presentazione dei Mac con dentro Intel? Lo si leggeva sinceramente preoccupato di quanti avrebbero avuto bisogno di driver per collegare periferiche, sui Mac vecchi e su quelli nuovi.

Per alleviare le sue preoccupazioni rispetto alle stampanti, gli consigliamo la visita a questa pagina, che riassume la <a href="http://docs.info.apple.com/article.html?artnum=303057" target="_blank">situazione relativa</a> a consegna appena iniziata.

Sicuramente per Windows ci sarà un numero di driver molto più alto, ma lui capirà. Noi fanatici del Mac siamo gente un po' eccentrica e raramente colleghiamo più di una stampante.