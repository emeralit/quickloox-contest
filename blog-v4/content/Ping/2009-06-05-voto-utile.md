---
title: "Voto utile"
date: 2009-06-05
draft: false
tags: ["ping"]
---

Sono alte le probabilità che domani mi trovi a Quiliano per festeggiare il settimo anno del <a href="http://www.allaboutapple.com/blog/index.php?entry=entry090521-224635" target="_blank">museo All About Apple</a>.

È il giorno giusto per una visita e per scoprire persone eccezionali. Nonché capire un sacco del passato della tecnologia.

Lo so che ci sono le Europee, ma si vota anche dopodomani. E il museo è di rilevanza mondiale, quindi ha la priorità.