---
title: "Senza fili ma con radici"
date: 2009-01-31
draft: false
tags: ["ping"]
---

<strong>Ken</strong> mi informa gentilissimamente di quanto accade a <a href="http://soveria.it/soveria_home/" target="_blank">Soveria Mannelli</a>, piccolo gioiello della Calabria già identificato dal Censis come Comune più informatizzato d'Italia.

Lo scorso 19 gennaio Soveria ha inaugurato la copertura totale e gratuita del territorio comunale con la connessione <em>wi-fi</em>. Triste che in Italia vadano segnalate queste iniziative, che dovrebbero essere noiosa consuetudine: ottimo che nello sfacelo nazionale siano segnalabili eccezioni esemplari.

Sarà coincidenza, come sempre, ma nella faccenda c'è lo zampino del <a href="http://www.calmug.org" target="_blank">Calmug</a>, l'attivissimo gruppo di utenza Macintosh della Calabria. Cui appartiene Ken.

Complimenti e insistere. È la rete che deve arrivare dappertutto, non la gente partire in cerca di posti con la rete. Le radici sono importanti.