---
title: "Lo sforzo dell'abitudine"
date: 2006-04-22
draft: false
tags: ["ping"]
---

Faccio sempre fatica ad abbandonare qualcosa di consolidato. Però, positivamente istigato da <a href="http://www.kero.it" target="_blank">Fede</a>, farò un collaudo di <a href="http://www.opencommunity.co.uk/vienna2.html" target="_blank">Vienna</a>, newsreader open source che sembra davvero in grado di competere validamente con <a href="http://ranchero.com/netnewswire/" target="_blank">NetNewsWire Lite</a>, la mia scelta iniziale per i feed Rss e tuttora la prediletta.