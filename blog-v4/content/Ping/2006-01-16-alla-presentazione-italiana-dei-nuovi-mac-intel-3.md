---
title: "Alla-presentazione-italiana-dei-nuovi-mac-intel-3"
date: 2006-01-16
draft: false
tags: [ping]
---

Mi rendo conto solo adesso che la luminosità maggiore dello schermo di MacBook Pro (il 67 percento più di prima, viene detto) non ha solo un vantaggio fine a se stesso. Quando il portatile pilota uno schermo ausiliario, per esempio un Cinema Display, i due schermi avranno praticamente la stessa luminosità.

Per chi deve essere attento al colore dovrebbe essere una buona notizia.
