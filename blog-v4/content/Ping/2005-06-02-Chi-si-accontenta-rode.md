---
title: "Chi si accontenta rode"
date: 2005-06-02
draft: false
tags: ["ping"]
---

Arriva dopo, è più brutto e costa uguale. Il Pc del futuro è tra noi

Come segnala <a href="http://www.zeusnews.it/index.php3?ar=stampa&cod=4168">Zeusnews</a>, i taiwanesi di A-Open (una controllata di Acer) hanno presentato un computer che ricorda molto da vicino (ma anche molto da lontano!) qualcosa che nel mondo Mac esiste già da cinque mesi.

Le forme sono uguali ma, come di consueto, lo stile è più trascurato. Il costo, sempre stando a Zeusnews, è di 500 euro e dentro c'è Windows, invece di un sistema Unix e in gran parte open source come Mac OS X.

Non voglio neanche entrare nel merito della configurazione o delle prestazioni. Dico solo che ci sarà gente che, a prezzo uguale, si compra in ritardo un oggetto più brutto e con un software inferiore, in attesa della calata dei virus.

Evidentemente è gente che si accontenta. Ma un po' rode.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>