---
title: "iMac Core Duo vs. Digital Performer"
date: 2006-01-25
draft: false
tags: ["ping"]
---

Non ne ho la certezza matematica, ma credo proprio che la prova pubblicata da <strong>Alessio</strong> su Mac-community sia veramente il primo test concreto di un iMac Core Duo in Italia alle prese con un compito vero.

Alessio è un professionista della musica e non si è dilettato con Word, ma ha <a href="http://www.mac-community.it/Area%20Supporto/Intel%20Test%20Drive/inteltestdrive.html" target="_blank">installato Digital Performer e un bel mazzetto di plugin</a>, eseguendo tutto sotto Rosetta dal momento che si tratta di software ancora in edizione PowerPc.

I risultati sono da leggere e c'è da meditarci sopra. Positivamente. Bravo anche ad Alessio; non capita spesso di poter vedere questa cura e questa limpidezza nelle recensioni.

P.S.: l'Url potrebbe cambiare. Se il link sopra non funzionasse, vai su <a href="http://www.mac-community.it" target="_blank">Mac-community</a>, poi clicca su Supporto e infine su Intel Test Drive. Lo verificherò appena posso.