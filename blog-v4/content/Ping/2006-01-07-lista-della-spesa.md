---
title: "Lista della spesa"
date: 2006-01-07
draft: false
tags: ["ping"]
---

Il punto della situazione in un momento qualunque di utilizzo

Aperti nel Dock in questo istante: Firefox, Tex-Edit Plus, iChat, Safari, NetNewsWire Lite, Terminale, BBEdit, CyberDuck, GraphicConverter, World of Warcraft, OpenOffice, Mailsmith, SpamSieve, TextEdit, Excalibur, Anteprima, Proteus, iTunes.

Il costo globale di tutti questi programmi manco si avvicina a quello di Word.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>