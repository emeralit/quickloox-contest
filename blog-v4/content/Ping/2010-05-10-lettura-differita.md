---
title: "Lettura differita"
date: 2010-05-10
draft: false
tags: ["ping"]
---

Ennesimo tentativo di trovare un flusso di lavoro in cui trovino posto le innumerevoli cose che sono da leggere, ma in un altro momento: mi sono iscritto a <a href="http://www.instapaper.com/" target="_blank">Instapaper</a>.

Idee alternative?