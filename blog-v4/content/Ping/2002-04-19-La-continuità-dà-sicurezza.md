---
title: "La continuità da sicurezza"
date: 2002-04-19
draft: false
tags: ["ping"]
---

Direbbe la pubblicità: in un mondo che cambia c’è una costante che resta...

Mentre scrivo sto leggendo la notizia di un ennesimo <link>problema di sicurezza</link>http://online.securityfocus.com/archive/1/267561 in Explorer 6.
Sono cose che, paradossalmente, danno sicurezza invece di toglierla. Perché sono puntuali come la primavera, le tasse e il sorgere del sole.
E il bello è che qualcuno insisterà pure a voler usare Explorer. Contento lui, sicuri noi.

<link>Lucio Bragagnolo</link>lux@mac.com