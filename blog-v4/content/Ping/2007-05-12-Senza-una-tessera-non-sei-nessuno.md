---
title: "Senza una tessera non sei nessuno"
date: 2007-05-12
draft: false
tags: ["ping"]
---

L'<a href="http://www.allaboutapple.com" target="_blank">All About Apple Club</a> mi ha nominato Socio Onorario. Sono, appunto, onorato e, seppure immeritatamente, orgoglioso. Per quanto siano loro a fare per me sempre molto più di quanto io potrò mai fare per loro.

Ho autografato una copia di Macintosh Story a Paolo di Leo e il mondo è bizzarro, perché io sono uno scribacchino mentre lui ha realizzato un clone di Apple I e in un mondo normale dovrei chiedere io l'autografo a lui.

Mi scuso per la stringatezza, necessaria. Dettaglierò nei prossimi giorni.