---
title: "Le parole sono trappole"
date: 2010-09-09
draft: false
tags: ["ping"]
---

Se c'era bisogno di una dimostrazione del fatto che una enciclopedia universale compilata dal popolo è un progresso eccezionale, ma con i dovuti distinguo, eccola: Wikipedia pubblica il finale di <a href="http://itunes.apple.com/us/app/show-time-lite-alarm-clock/id374348033?mt=8" target="_blank">Trappola per topi</a>.

<i>Trappola per topi</i> è un famoso romanzo giallo di Agatha Christie, che a Londra rappresentano in forma teatrale dal 1952 e ha superato le 24 mila repliche. Leggere il libro, o assistere alla <i>pièce</i> in teatro, è divertimento se fatto senza impegno, uno straordinario esercizio di logica se fatto con intenzione. Insomma, nel suo genere è un capolavoro, molto del quale &#8211; è un giallo &#8211; dipende esattamente dal fatto che tutti gli indizi cadano al posto giusto nel momento giusto e che chi guardava senza impegno possa emozionarsi e meravigliarsi di fronte alla conclusione, mentre quanti giocavano al detective possano compiacersi di avere trovato la soluzione prima che venisse esplicitata.

Che cosa aggiunge la conoscenza anticipata del finale alla possibilità di apprezzare <i>Trappola per topi</i> e alla cultura generale? Assolutamente niente. Anzi, leva tutto il gusto della sorpresa a chi voleva guardarla senza impegno e della soddisfazione intellettuale a chi invece voleva mettercelo. Due errori con una sola stupidaggine, non male.

Togliere l'enciclopedia dalle mani di un ristretto gruppo di redattori per metterla nelle mani di tutti è un progresso importante e gigantesco. A patto di ricordarsi che le mani di tutti sono ignoranti su come si realizza un'enciclopedia e il buonsenso di cui è dotato il gruppo ristretto di redattori dobbiamo mettercelo noi, altrimenti la consultazione acritica di Wikipedia diventa instupidimento e rovina tutto.

A maggiore riprova la posizione ufficiale di Wikipedia: il finale di <i>Trappola per topi</i> si può trovare ovunque, per cui non c'è problema e basta non leggerlo.

A dimostrazione che uno sciocco trova sempre uno più sciocco di lui.