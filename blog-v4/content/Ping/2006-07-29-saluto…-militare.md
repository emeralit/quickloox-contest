---
title: "Saluto… militare"
date: 2006-07-29
draft: false
tags: ["ping"]
---

Per risalutare <strong>Giulio</strong> ripeto lo scambio di battute che lo ha visto protagonista quando, in bicicletta, è stato fermato dalle forze dell'ordine:

<cite>Apra la borsa.</cite>

<cite>Ecco.</cite>

<cite>Che cos'è questo?</cite>

<cite>Un computer.</cite>

<cite>Ma è bianco.</cite>