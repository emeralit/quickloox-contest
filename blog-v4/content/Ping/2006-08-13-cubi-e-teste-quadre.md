---
title: "Cubi e teste quadre"
date: 2006-08-13
draft: false
tags: ["ping"]
---

Ci sono siti che si devono leggere e altri che fanno solo perdere tempo. Per riempire la seconda colonna è utile la lettura di questo vecchio <a href="http://news.com.com/2061-10802_3-6016810.html?tag=nefd.aof" target="_blank">articolo</a> di Cnet, in cui si descrivono nascita, sviluppo e morte di uno dei gossip più stupidi del 2006: il Google Cube.

Prima presentato come meraviglia tecnologica, poi come aggeggino da infilare in tutte le case (una differenza da niente), costruito unicamente sulla base di fantasie abbastanza malate e alla fine smentito da Google stessa, presumibilmente quando non ne potevano più di leggere scempiaggini scritte da teste quadre.