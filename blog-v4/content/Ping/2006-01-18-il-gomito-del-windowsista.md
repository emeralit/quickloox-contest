---
title: "Il gomito del windowsista"
date: 2006-01-18
draft: false
tags: [ping]
---

Su quasi tutti i portatili non Apple la trackpad non sta al centro, perfetta per destrorsi e mancini, ma spostata a sinistra. Per i mancini probabilmente è un bel vantaggio; per il 90 percento restante è una cosa orrendamente antiergonomica.

Non mi sembra una cosa così difficile da copiare. Perché non la copiano?
