---
title: "Se facesse anche il caffè"
date: 2007-05-19
draft: false
tags: ["ping"]
---

Prima ho letto <a href="http://morrick.wordpress.com/2007/04/03/goodbye-ruby-reader/" target="_blank">Riccardo</a>. Poi me lo ha consigliato <strong>Mario</strong>. Allora ho deciso di provare <a href="http://skim-app.sourceforge.net" target="_blank">Skim</a> come alternativa ad Anteprima per visionare i Pdf.

Mi sono convinto ma, accidenti accidentaccio, Skim non accetta il drag and drop di immagini (i Pdf funzionano). E cos&#236; Anteprima mi serve ancora.

Non ho tempo e però leggo quell'invito sulla pagina del sito a <cite>helping out with the project</cite>. Quanto meno un feedback lo mando.