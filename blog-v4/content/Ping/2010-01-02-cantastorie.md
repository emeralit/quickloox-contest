---
title: "Cantastorie"
date: 2010-01-02
draft: false
tags: ["ping"]
---

In casa c'è una nuova libreria.

Nel riporre tutti i libri mi è tornato in mano il divertente <a href="http://www.macworld.it/blogs/ping/?p=1073" target="_self">Facce da iPod</a> di Alberto Pucci, uscito nel 2006.

A pagina 23 una gemma: la dichiarazione di Andrea Di Mambro, <cite>ideatore e curatore</cite> di <a href="http://www.ipodmania.it/" target="_blank">iPodMania.it</a>. <cite>L'arrivo di Urge darà comunque fastidio all'iTunes Music Store e mi riferisco a tutti quegli utenti che non hanno l'iPod e che, trovandosi il software all'interno di Windows, compreranno automaticamente dallo</cite> store <cite>di Microsoft. Fatta questa premessa, penso però che i clienti Apple rimarranno comunque fedeli a Steve Jobs e ai suoi prodotti.</cite>

La dichiarazione è platealmente espressa con una mano davanti e un'altra dietro, oltre a essere scombiccherata (sarebbe stato interessante proprio sapere che cosa poteva succedere a tutti i clienti Apple che hanno un iPod collegato a un computer Windows e invece&#8230;).

Il punto è un altro. Urge ha fatto <a href="http://www.macworld.it/blogs/ping/?p=1423" target="_self">una fine tristissima</a>, neanche un anno dopo la dichiarazione. E insomma.