w---
title: "Retropensieri"
date: 2008-04-15
draft: false
tags: ["ping"]
---

C'è chi va lontanissimo nel tempo e costruisce addirittura una <a href="http://www.networkworld.com/news/2008/041108-difference-engine.html" target="_blank">replica del Difference Engine 2 di Charles Babbage</a>, un mostro da cinque tonnellate.

C'è chi, ingegnere software di Apple, fa la stessa cosa <a href="http://acarol.woz.org/" target="_blank">con i pezzi del Lego</a>.

C'è infine chi si accontenta e, con la scheda logica di un Apple IIGs, ricava un <a href="http://benheck.com/04-14-2008/apple-iigs-original-hardware-laptop" target="_blank">portatile con tanto di disco flash</a>.

Certe volte è più affascinante l'informatica del tempo dell'infanzia dei computer. :-)