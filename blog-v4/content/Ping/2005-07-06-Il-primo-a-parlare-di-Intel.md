---
title: "Prima di tutti"
date: 2005-07-06
draft: false
tags: ["ping"]
---

A scoprire che Apple passava a processori Intel non è stato il Wall Street Journal

Ricapitolando: Steve Jobs ha confermato che, per una volta, i rumor erano veri il 6 giugno 2005, al discorso di apertura della Worldwide Developers Conference di Apple.

Il Wall Street Journal ha anticipato la notizia il 22 maggio, praticamente due settimane prima, nella sua rubrica Heard on the Street (sentite in strada).

Ma Paul Thurrott, di WindowsItPro, ha battuto tutti sul tempo e non di poco, con un <a href="http://www.windowsitpro.com/Article/ArticleID/46175/46175.html">paragrafo sul suo blog</a> datato addirittura 26 aprile. Oltre un mese prima.

<em>Questa è bizzarra, ma a pranzo abbiamo sentito che Apple non è contenta di come va la produzione di chip di Ibm e passerà a chip Intel quest'anno. Sì, seriamente.</em>

Notato che, dei siti di pettegolezzi, nessuno ci è arrivato? Oramai sono buoni giusto da guardare sotto l'ombrellone, assieme alle foto del matrimonio di Anna Falchi.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>