---
title: "Lezione dal passato recente"
date: 2010-09-18
draft: false
tags: ["ping"]
---

Coro 2007: <i>iPhone bello, ma non ci sono i programmi</i>.

Coro 2010: <i>Apple Tv bella, ma non ci sono i programmi</i>.

<a href="http://www.engadget.com/2010/09/05/entelligence-a-tale-of-two-tvs/?icid=engadget-iphone-url" target="_blank">Che cosa ne pensa Michael Gartenberg di Altimeter Group</a>:

<cite>La televisione ha radicato comportamenti che non cambieranno tanto presto e Apple deve prima istruire i consumatori. Apple presentò iPhone inizialmente senza un negozio di applicazioni per insegnare nuovi modi di usare un telefono e ora sta facendo lo stesso con la nuova Apple Tv. Apple deve entrare nel mercato, consolidare apparecchi e servizi e lentamente portare a evoluzione il comportamento del consumatore in salotto. Grazie a un buon posizionamento nei negozi, a un po' di marketing e ad accordi di distribuzione di contenuti in numero crescente, Apple può lentamente istruire, convincere e fare evolvere a un nuovo utilizzo del televisore. Con una base installata da mostrare agli sviluppatori Apple può lavorare con sicurezza a un</cite> <cite>kit di sviluppo software e portare a regime il rapporto con i programmatori. Occupando solidamente il secondo ingresso del televisore, Apple può aggirare la posizione dominante delle televisioni via cavo senza tentare un durissimo assalto frontale. [&#8230;]</cite>

<cite>Apple ha mostrato la capacità di fare le due cose che nel tempo daranno il successo ad Apple Tv: concludere accordi di distribuzione contenuti che prima parevano impossibili e conquistare gli sviluppatori.</cite>

Pensare, ora, a quello che è successo con iPhone e a quello che potrebbe succedere con Apple Tv da qui a qualche anno. In Italia le cose sono un po' diverse e in arretrato rispetto al Nordamerica, ma non cambia moltissimo: la posizione predominante non è quella della televisione via cavo ma dei digitali terrestri a pagamento, o del satellite e per il resto la situazione è quella. Se cominciano a vendersi un po' di Apple Tv anche nel Belpaese, e nel resto dell'Europa, può darsi che diventi conveniente per Apple anche affrontare la giungla continentale dei diritti di distribuzione del video e l&#236; potrebbe davvero succedere molto, pure interessante.