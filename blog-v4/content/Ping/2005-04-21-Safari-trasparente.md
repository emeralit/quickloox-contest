---
title: "Spesso o trasparente?"
date: 2005-04-21
draft: false
tags: ["ping"]
---

Continuano a inserire funzioni da testare nel menu di debug di Safari

Il menu di debug di Safari contiene spesso funzioni che servono per il collaudo del programma, oppure provvisorie in attesa di diventare definitive. Per esempio, la navigazione a pannelli è stata per un po' nel menu di debug prima di essere ufficializzata.

Per abilitare il menu di debug bisogna scrivere nel Terminale, a Safari chiuso,

<code>defaults write com.apple.safari IncludeDebugMenu 1</code>

(per toglierlo, stessa cosa con lo zero al posto dell'uno)

Nel nuovo Safari 1.3, introdotto dall'aggiornamento a 10.3.9, il menu di debug contiene anche la possibilità di avere una pagina trasparente. Una pagina vuota; se si carica qualcosa, Safari ridiventa opaco.

Però vederlo è interessante e chissà che un domani non diventi una funzione quasi utile. Guardo una pagina e intanto, in trasparenza, vedo se ho già scaricato quel file eccetera.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>