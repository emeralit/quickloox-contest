---
title: "Arrivano i nomi per i widget"
date: 2005-05-06
draft: false
tags: ["ping"]
---

Rivincita dell'italiano o puro divertimento? Forse ambedue

Arrivano le prime risposte alla sfida lanciata l'altroieri: come li chiamiamo, in italiano, quei cosi che grazie a Dashboard di Tiger galleggiano sull'Aqua di Mac OS X sotto il nome di widget?

Giuseppe Saltini propone magheggi. Non male!

L'amico Luca Pedrone propone una intera batteria di alternative: pippolini, aiutilli, servilli, spie, spioncini, oblò, finestrelle… ma il suo preferito è (<cite>al maschile, mi raccomando!</cite>) sveltini.

Chi ha nuove idee è benvenuto! Nel frattempo, ci sono già almeno novanta cosi pronti sul <a href="http://www.apple.com/downloads/macosx/dashboard/">sito Apple</a>.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>