---
title: "Finalmente"
date: 2006-01-09
draft: false
tags: ["ping"]
---

Un nuovo inizio

Finalmente arriva il Macworld Expo… finalmente comincia questo piccolo blog. Coincidenza che non avrebbe potuto essere più appropriata. Ci si legge prossimamente per qualche commento a caldissimo sugli annunci. Chissà che cosa sogna Steve Jobs, in questo momento?

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>