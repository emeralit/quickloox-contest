---
title: "Noia da software"
date: 2005-03-20
draft: false
tags: ["ping"]
---

Prova a contare fino a trecento e scoprire che c'è da continuare

Ho appena terminato un piccolo progetto speciale per Macworld cartaceo. Non dico niente per ora, ma una piccola considerazione la voglio fare.

Prima di dire che per Mac non ci sono programmi, forse uno dovrebbe provare a censirne qualcosa più di trecento, constatando nel mentre che non c'è spazio per citarne quaranta volte tanti altri.

Forse, prima di dire che non ci sono programmi per Mac, uno dovrebbe passare la sua vita a provare un decimo di tutti quelli disponibili.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>