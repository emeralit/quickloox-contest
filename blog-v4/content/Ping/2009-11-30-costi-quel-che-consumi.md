---
title: "Costi quel che consumi"
date: 2009-11-30
draft: false
tags: ["ping"]
---

Gli sprovveduti vanno a comprare il computer guardando il cartellino del prezzo. Ok. Le persone avvedute guardano anche al costo complessivo nel tempo.

Certamente nessuno darebbe troppo peso al consumo energetico, per esempio espresso in questa tabella dove <a href="http://www.sust-it.net/energy_saving.php?id=20" target="_blank">Mac mini occupa i primi quattro posti della classifica di efficienza</a>.

Tre sterline l'anno, per dare uno scarto medio, sono tipo cinque euro l'anno. Chi se ne frega.

Non è il punto però. Un computer che consuma meno è più efficiente, meglio ingegnerizzato, probabilmente meno surriscaldato, con componenti di maggiore qualità, statisticamente più durevoli.

E tutto questo, nel calcolare il costo totale di un computer nel corso della propria vita, dovrebbe avere invece una grande importanza.