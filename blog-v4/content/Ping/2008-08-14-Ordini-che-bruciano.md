---
title: "Ordini che bruciano"
date: 2008-08-14
draft: false
tags: ["ping"]
---

C'è poco da discutere. Accadono, giuro, situazioni in cui per masterizzare una cartella la cosa più veloce da fare è dare un comando da Terminale.

Il comando è

<code>drutil burn</code> <em>percorso</em>

Dove il percorso è per l'appunto quello che porta alla cartella da masterizzare e, in mancanza di meglio, si &#8220;digita&#8221; facilmente trascinando sulla finestra del Terminale la cartella in questione.