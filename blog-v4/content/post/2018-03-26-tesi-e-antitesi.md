---
title: "Tesi e antitesi"
date: 2018-03-26
comments: true
tags: [StatCounter, macOS]
---
Secondo StatCounter il quaranta percento del [traffico di rete Mac](http://gs.statcounter.com/macos-version-market-share/desktop/worldwide) riguarda High Sierra, il venti Sierra (arrotondo), il diciassette El Capitan, l’undici Yosemite, il resto briciole.

Giuro che nel giro di poche ore ho ricevuto due interpretazioni di questi dati da altrettanti amici.

La prima: c’è una vasta fronda di utenti che si oppongono al passaggio alle versioni più recenti del sistema per protesta o per evitare *bug*.

La seconda: Apple pratica obsolescenza programmata e costringe gli utenti ad aggiornare i Mac contro la loro volontà.

Vorrei che, invece di parlare con me, si trovassero a un tavolo per mettersi d’accordo su una sintesi comune. Che racconti in forma coerente  questo mondo singolare dove la gente è forzata ad aggiornare e contemporaneamente non lo fa.
