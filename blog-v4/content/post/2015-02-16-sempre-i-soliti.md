---
title: "Sempre i soliti"
date: 2015-02-17
comments: true
tags: [ canvas, Flipboard, Html5, Css, Html]
---
La rivista *online* e *app* Flipboard ha spiegato sul proprio blog [come ottenere un’interfaccia fluida](http://engineering.flipboard.com/2015/02/mobile-web/) sulla versione *mobile* del proprio servizio: abbandonando lo sviluppo web tradizionale per rifare tutta la pagina in modalità *canvas*.<!--more-->

Per farla brevissima, *canvas* è un attributo di Html5 usando il quale si può aprire in una pagina web una sorte di lavagna su cui disegnare in JavaScript. A Flipboard, invece che strutturare la pagina con il normale Html, l’hanno interamente disegnata su un *canvas*. Il risultato sono scorrimenti dell’interfaccia che arrivano anche a sessanta fotogrammi per secondo, un risultato tecnicamente irraggiungibile con Html e Css.

*Canvas* c’è da un pezzo, ma solo adesso comincia a essere metabolizzato e sfruttato per quello che potrebbe essere uno sviluppo del web in termini estetici e prestazionali che oggi pensiamo inconcepibile, a torto.

Chi lo ha inventato? [I soliti](https://en.wikipedia.org/wiki/Canvas_element). Quelli che pensano solo a vendere telefoni costosi, che hanno rubato l’interfaccia utente a Xerox come peccato originale, che non fanno vera innovazione ma solo marketing scintillante, che fanno computer uguali a tutti gli altri ma li truccano con un po’ di alluminio levigato per gonfiare il prezzo.
