---
title: "Il centro del business"
date: 2022-12-21T04:06:47+01:00
draft: false
toc: false
comments: true
categories: [Internet, Hardware]
tags: [Mac]
---
Festa di Natale organizzata da un rivenditore Apple di dimensioni importanti.

Notizia uno: nonostante sfide e difficoltà non da poco presentate dall’economia e dal clima generale negli ultimi due-tre anni, un bravo rivenditore Apple di dimensioni importanti ha ottenuto grandi risultati, in crescita e sviluppo. È un bel segnale concreto del fatto che un momento difficile non ha da essere per forza anche negativo.

Notizia due: il messaggio passato dal vertice dell’azienda a decine di agenti di vendita, sviluppatori, personale di supporto è che per Apple il centro del business è rappresentato da… Mac.

Pochissimi, fuori dal business reale, farebbero una considerazione analoga. Eppure sembra molto difficile vederla come una costruzione astratta trasmessa per qualche ragione a persone che si guadagnano da vivere con il raggiungimento di obiettivi molto concreti, definiti attraverso un filo diretto con Apple che è molto diretto.

Il fatturato può sbilanciarsi verso altre piattaforme; tuttavia il messaggio strategico è chiaro. Direi anche incoraggiante.