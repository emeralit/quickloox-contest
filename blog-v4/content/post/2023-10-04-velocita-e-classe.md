---
title: "Velocità e classe"
date: 2023-10-04T14:35:15+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [Software, Android, US News, CarPlay, Android Auto, Autoevolution, Porsche]
---
La realtà come la rielaborano alcuni: [Un ennesimo produttore adotterà probabilmente Android intanto che CarPlay perde lentamente terreno](https://www.autoevolution.com/news/one-more-carmaker-likely-to-adopt-android-as-carplay-is-slowly-losing-ground-211828.html).

*Porsche ha confermato i colloqui in corso con Google per integrarne i servizi nei suoi veicoli futuri*. 14 maggio.

La realtà effettiva, né virtuale né aumentata né ibrida né mista né parallela: [Porsche è la prima ad avere le nuove funzioni di CarPlay](https://cars.usnews.com/cars-trucks/features/porsche-is-the-first-to-get-new-apple-carplay-functions). 13 luglio.

Qualcuna che abbandona CarPlay esiste. Tuttavia vuole fare da sola e non adotta Android Auto o come si chiama. È molto presto per capire come andrà a finire e [il dibattito è in corso](https://macintelligence.org/posts/2023-05-18-meno-fortunati/).

Non ricordo chi dicesse che una bugia fa il giro del mondo intanto che una verità finisce di allacciarsi le scarpe. Se si parla di Porsche, la velocità non è tutto; c’è anche la classe e quella si vede poco a poco.