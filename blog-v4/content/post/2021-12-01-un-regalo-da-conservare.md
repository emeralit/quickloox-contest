---
title: "Un regalo da conservare"
date: 2021-12-01T00:38:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Ocarina of Time] 
---
Quante volte ho sentito i profeti di sventura minacciare la scarsa propensione dei dati digitali a sopravvivere. E poi, sventura delle sventure, la mancanza di supporti capaci di leggere i vecchi formati. Poi le locuste, le inondazioni, la pioggia di rane, il gomito che fa contatto con il piede.

Perché un gruppo di appassionati (non un laboratorio di ricerca) nel giro di ventuno mesi (certo non due ore, ma fattibile) [ha decompilato Ocarina of Time](https://arstechnica.com/gaming/2021/11/reverse-engineering-team-completes-its-ocarina-of-time-decompilation/).

Seppure non completamente finito, il lavoro è sufficientemente completo per essere sicuri che giungerà a compimento. Decompilazione significa riavere a disposizione un listato in luogo dell’illeggibile – per un umano – pseudocodice generato dal compilatore a beneficio del processore.

Quindi <em>Ocarina of Time</em> potrà essere ricompilato per altre piattaforme rispetto a quella originale, perfino arricchito o migliorato o ripulito da bug preesistenti, qualsiasi cosa. Tutto è a disposizione di chiunque lo voglia scaricare. Possono diffondersi copie illimitate del gioco, che prima esisteva solo in una Rom effettivamente illeggibile dai computer moderni. E appunto, ora esiste nella sua forma prima della compilazione, ovunque. Tutti gli apparati del mondo possono leggere il codice, se possono collegarsi a Internet.

Non sembra un buon metodo di conservazione di un gioco storico? Certo, qualcuno preferirà stampare le decine di migliaia di righe di codice, per oltre quindicimila funzioni complessive. Perché la carta si conserva più a lungo, certo.

Un gruppo di appassionati ha lavorato duramente per regalare a tutta la comunità un tesoro culturale moderno. Nell’articolo si possono scoprire le altre iniziative in corso per decompilare altri giochi. Questa è conservazione digitale, più furba che mettere una Rom in una teca per proteggerla dalla polvere.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._