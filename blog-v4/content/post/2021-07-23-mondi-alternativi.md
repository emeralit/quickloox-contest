---
title: "Mondi alternativi"
date: 2021-07-23T00:48:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Finder, macOS, Mac OS] 
---
Nel secolo scorso, Mac OS 8.5 ha introdotto le [icone proxy del documento dentro la barra del titolo della finestra](http://mirror.informatimago.com/next/developer.apple.com/documentation/macos8/HumanInterfaceToolbox/WindowManager/ProgWMacOS8.5WindowMgr/WindowMgr.5.html).

Si poteva prendere l’icona proxy e trascinarla in una posizione diversa nel sistema, a patto che il documento fosse salvato. In presenza di modifiche ancora da salvare, l’icona appariva in stato disabilitato e trascinarla era impossibile.

*Design* impeccabile e deliziosamente semplice. Non ti piaceva? Pazienza.

Oggi, Big Sur mostra l’icona proxy solamente se ci passa sopra con il puntatore (un *mouseover*). Secondo diversi commentatori, John Gruber tra gli altri, [è una scelta di *design* meno impeccabile della precedente](https://daringfireball.net/2021/07/document_proxy_icons_macos_11_and_12).

> C’è una relazione uno-a-uno tra l’icone di un documento nel Finder e la finestra aperta dall’applicazione per lavorare su quel documento; mostrare l’icona del documento stesso nella barra del titolo della finestra rafforzava questo concetto.

*Design* ambizioso e sofisticato, più controverso e complesso. Non ti piace? [Modifichi una preferenza nascosta del Finder e ritorni alle icone proxy di prima](https://www.reddit.com/r/MacOS/comments/lxgvlf/getting_back_the_prebig_sur_window_style/). (Si può fare anche a livello di singole applicazioni, o globale).

Meglio allora, quando era tutto più semplice e monolitico, o oggi, più complesso e variamente modificabile? Non ho una risposta univoca. Nel dubbio torno al mio solito principio guida: ciò che ti fa imparare qualcosa costa di più, ma vale di più.

Anche questo principio è discutibile: c’è chi vuole unicamente arrivare al lavoro finito, senza acquisire competenze relative al computer che non riguardano il proprio lavoro. La pensavo anch’io così e piuttosto radicalmente, molti anni fa. Oggi che sono cambiate milioni di cose, nutro forti dubbi che valga ancora. Forse è diventato più importante che ognuno abbia delle possibilità di avvicinare un sistema più complesso ai suoi requisiti personali. Al suo mondo di *computing*.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*