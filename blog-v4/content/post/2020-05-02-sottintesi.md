---
title: "Sottintesi"
date: 2020-05-02
comments: true
tags: [Apple, Google]
---
Molti commenti sulle app di tracciamento contatti e specialmente [sulla nostra](https://www.agendadigitale.eu/cultura-digitale/immuni-come-funziona-lapp-italiana-contro-il-coronavirus/) vertono sulla massa critica di adozione piuttosto alta, che dovrebbe esserci per garantire un funzionamento ottimale.

(Veramente? Se ad Ancona tutti la adottano, ma a Viterbo nessuno, ad Ancona comunque servirà un sacco, anche se la media di utilizzo è minore del necessario stimato).

Il sottinteso è che, prevedibilmente non raggiungendo i numeri desiderati, la app non serva e non vada fatta.

Tra le obiezioni alle lezioni a distanza è che [non raggiungono tutti gli studenti](https://macintelligence.org/posts/2020-04-10-gli-assenti-hanno-torto/).

(Neanche le lezioni in aula lo fanno).

Il sottinteso è che, non essendo ecumeniche, le lezioni a distanza siano sbagliate e da non tenere.

Adesso si comincia a sentire che il *lockdown* è stato una cosa sbagliata perché non totale.

Se il sottinteso fosse che quindi non andava praticato, sarebbe ben triste. Anche perché nelle terapie intensive faticano per fare respirare tutti e c’è stato un momento in cui non ci riuscivano; visto che non c’erano abbastanza ventilatori per tutti, era il caso di toglierlo anche a chi era così fortunato da averlo?

Questo argomento del *non abbastanza* è diventato veramente un sintomo di inutilità del pensiero. Se ci sono risorse di pensiero in una testa, il bene della comunità richiede che vengano usate per capire che cosa fare in più e meglio. Che cosa non va lo si vede tutti e aiuta solo fino a un certo punto. Quello che occorre è [costruire](https://macintelligence.org/posts/2020-04-26-destra-sinistra-basta/), non criticare senza dare alternative migliori.

Nessun sottinteso, qui.