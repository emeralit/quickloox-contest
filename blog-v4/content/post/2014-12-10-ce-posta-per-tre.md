---
title: "C’è posta per tre"
date: 2014-12-10
comments: true
tags: [Gmail, iCloud, Mailsmith, Pop, Imap, GOogle]
---
Le contingenze mi costringono a fare qualche esperimento con la posta.<!--more-->

Il vecchio [Mailsmith](http://mailsmith.org) amministra perfettamente un account Gmail, cosa impossibile con la posta di iCloud. Google permette di avere un account Imap e accedervi con il prototollo Pop per ritirare la posta; con iCloud è impossibile.

Da *browser*, Gmail si aggiorna automaticamente; iCloud no, richiede un clic.

Gmail non ha problemi con *mailing list* che distribuiscono i messaggi in forma di allegati .eml; iCloud consente solo di scaricarli sul computer.

Viene quasi voglia di fare passare la posta iCloud tramite Gmail e continuare a maneggiare la posta con il programma tuttora più pratico e potente tra quelli in giro.

Sicuramente non sarà mai troppo presto per vedere qualche progresso nelle funzioni offerte dalla posta iCloud via *browser*.