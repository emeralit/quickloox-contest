---
title: "Vale tutto"
date: 2015-06-28
comments: true
tags: [OSX, FreeBSD, Linux, Darwin, Windows, XP, Microsoft, Samsung, Lenovo, Superfish]
---
Ci sono numerosi motivi per i quali, se sparisse OS X, adotterei FreeBSD, Linux, Darwin, letteralmente qualsiasi cosa tranne Windows. Per la più parte sono motivazioni sottili, che vanno spiegate e comprese, specifiche, talvolta esoteriche. Ma in fondo basta leggere.<!--more-->

Microsoft, racconta chiunque e ho scelto per nessun motivo [Time](http://time.com/3935855/us-navy-microsoft-windows-xp/), ha smesso di supportare Windows XP ad aprile 2014. Però la Marina statunitense sborsa 9,1 milioni di dollari per continuare a usare Windows XP e farsi supportare.

Morale: vuoi stare su Windows XP anche se è tre generazioni indietro? Niente da fare. A meno che hai soldi da buttare. Se paghi c’è quello che vuoi, non importa quanto assurdo.

Di Superfish, dell’antimalware Microsoft, di Lenovo [abbiamo già parlato](https://macintelligence.org/posts/2015-02-22-gatti-e-volpi/).

Morale: la sicurezza è una finta.

Ultime notizie, i computer Samsung [disabilitano Windows Update](http://arstechnica.com/information-technology/2015/06/samsung-silently-disabling-windows-update-on-some-computers/). Microsoft scrive un comunicato per dire che non va bene, Samsung nega di averlo fatto, insomma la cosa che si capisce maggiormente è la confusione assoluta.

Morale: chi ti vende un computer Windows ci fa sopra quello che gli pare senza che tu lo sappia.

È il Far West? È darwinismo, dove vincono i ricchi e quelli bravi con la *shell*? È *laissez faire* frutto del disprezzo assoluto per chi compra?

Perché la morale complessiva è che vale tutto e sopravvive solo il più adatto. No. La piattaforma non si giudica da quello che riescono a fare i campioni: abbiamo visto calciatori capaci di palleggiare con un’arancia. Il merito non è dell’arancia.

La piattaforma che merita è quella che facilita il compito a chi ne sa meno. Se vale tutto non è libertà, ma legge del più forte. Non fa per me.