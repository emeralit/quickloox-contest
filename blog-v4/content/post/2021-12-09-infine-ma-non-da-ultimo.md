---
title: "Infine ma non da ultimo"
date: 2021-12-09T01:06:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Monterey, Utility Colorsync] 
---
Mac ha graziosamente comunicato la disponibilità di Monterey e così ho acconsentito all’installazione.

Non ci ho fatto gran caso perché mi sono semplicemente spostato su iPad Pro a lavorare; l’installazione ha richiesto meno di un’ora.

Nessun cambiamento significativo nella disponibilità di spazio su disco o nella velocità; le applicazioni di uso quotidiano girano tutte come da copione. Se mai ho avuto un aggiornamento pressoché invisibile come impatto sul funzionamento generale, è questo.

Ho fatto una copia della app di installazione su un disco esterno, come sempre e più di sempre perché a questo giro l’eseguibile contiene sia il codice per M1 che quello per Intel e dunque passa i dodici gigabyte; ne ho liberi una cinquantina e preferisco mantenerli.

macOS era l’ultimo sistema operativo in attesa di aggiornamento annuale e complessivamente tutto sta andando bene. Monterey non porta chissà quali novità epocali, ma sono giorni che mi capitano cose al volo da segnare e *subito* dito o mouse sono corsi nell’angolo in basso a destra di iPad, e ora di Mac, per tirare fuori una nota rapida. Una funzione ausiliaria, piccola, graditissima.

Purtroppo sembra che Utility ColorSync abbia ancora problemi (grossi). Mi sarebbe tornata molto utile ultimamente, solo che l’edizione Big Sur è bacatissima, con dispersioni di memoria clamorose. La primissima prova con Monterey è stata ugualmente deludente, quando un po’ speravo in una soluzione. Il problema, comunque, non è specifico di questa versione di macOS.

In questo periodo, avere un aggiornamento non particolarmente entusiasmante per cambiamenti e novità mi fa comodo; il tempo *business as usual* è più che mai prezioso. Per gli sfizi (tipo SharePlay) c’è tempo e proverò a breve un collaudo.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._