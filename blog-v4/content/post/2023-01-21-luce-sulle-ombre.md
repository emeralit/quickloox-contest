---
title: "Luce sulle ombre"
date: 2023-01-21T01:26:23+01:00
draft: false
toc: false
comments: true
categories: [Software, Hardware]
tags: [Roll20, Safari, iPad, WebGL, Metal, WebGL via Metal, Dynamic Ligthing]
---
Da più di un anno, giocare a [Dungeons & Dragons](https://dnd.wizards.com) con [Roll20](https://roll20.net/welcome) su iPad presenta un problema: se le mappe contengono illuminazione dinamica, iPad non le mostra.

Quando il gioco di ruolo diventa tattico e muoviamo gli avatar dei nostri personaggi sulla mappa del luogo in cui essi si trovano, elementi come rocce, pilastri, muri, porte chiuse nascondono all’avatar la vista di una parte della mappa, quella messa in ombra dall’elemento. Le ombre sono dinamiche, come l’illuminazione, perché quando l’avatar si sposta, cambia in modo corrispondente la fetta di mappa non visibile.

È una funzione che aggiunge realismo e suggestione, naturalmente se ben predisposta dal master. Aggiunge anche *suspence*: il cunicolo giunge a una curva a gomito e, esattamente come accadrebbe dal vero, il personaggio non può sapere che minaccia si nasconda oltre la curva, fino al momento in cui va a vedere.

Si può disattivare l’illuminazione dinamica nelle mappe, ma è un peccato. È pure una mossa contronatura: giochiamo su Roll20 invece che con mappe abbozzate a pennarello proprio per avere più realismo, più qualità grafica, più bellezza nel gioco. Togliere le ombre è togliere significato alla luce e non si fa.

lo staff tecnico di Roll20 non ha brillato per coordinamento e supporto fattivo, ma si è riusciti a capire che il sistema è impegnato a fondo nello sviluppo di strumenti molto migliori degli attuali nella parte *mobile* e questo assorbe anche le energie solitamente impegnate nel risolvere i bug. Non c’è una scadenza annunciata, però.

Per fortuna sui forum di Roll20 è apparso un *workaround* che, nel mio caso, ha funzionato perfettamente: andare nelle impostazioni sperimentali di Safari su iPad, che si trovano dentro le avanzate, che stanno in Safari che sta nelle Impostazioni, e disattivare il pulsante *WebGL via Metal*.

Lo stratagemma chiude efficacemente il problema pur minacciando di aprirne altri: Safari, infatti, non è ufficialmente supportato da Roll20 (sono abituato a lanciare Chrome su iPad apposta per aprire la piattaforma) e potrebbe esserci qualcos’altro che non funziona. Quindi non è la soluzione.

Ma è la riprova che c’è ancora molto da smanettare, nel *computing* di oggi. Perfino su iPad. Non parliamo del supposto compimento del metaverso, se ancora oggi abbiamo da capire come regolarci con le ombre dal punto di vista di avatar bidimensionali usati per giocare.

Sono contento e, in ogni caso, era un passo necessario. Il mio personaggio attuale non è un elfo nero e dunque non deve temere niente che riguardi l’illuminazione.