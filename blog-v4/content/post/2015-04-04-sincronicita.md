---
title: "Sincronicità"
date: 2015-04-05
comments: true
tags: [Script, Macworld, Homebrew, Quicksilver, Pasqua, Cellar, QuickLoox, iPad, Swift, iOS, programmazione, Froese, sincronicità]
---
Per motivi casuali mi ritrovo di tanto in tanto ad aggiungere al *blog* un *post* proveniente dall’archivio di *Script*, il blog che tenevo sotto le insegne di Macworld. Scelto casualmente.<!--more-->

Oggi ho aggiunto, per puro caso, [Un’applicazione alfa](https://macintelligence.org/posts/2013-03-29-unapplicazione-alfa/), scritto proprio in occasione di due Pasque fa. ([Quicksilver](http://qsapp.com/download.php) se la passa ancora alla grandissima).

L’aggiunta stava per essere vuota e vana a causa di una leggerezza: ho pensato che cancellare la *directory* *Cellar* dove stanno i pacchetti installati da [Homebrew](http://brew.sh) avrebbe avuto nessun effetto sul motore di QuickLoox, installato in tutt’altra *directory*.

Mi sbagliavo, per quanto dopo ripetuti esami di *Cellar* non riesca a individuare alcun legame significativo. Fortunatamente Time Machine ha sistemato la situazione con un salto di poche ore nel passato e la pubblicazione è tornata regolare.

Mia figlia, in viaggio per gli otto mesi, è riuscita a fare crashare iPad in modo che il *boot* si bloccasse con la pubblicazione sullo schermo di alcuni messaggi di errore di sistema. Una situazione che non avevo mai visto e non rivedrò più, perché al *boot* successivo tutto è tornato normale. Ero troppo interdetto per scattare una foto.

Dopo avere terminato la stesura di un libriccino su [Swift](https://developer.apple.com/swift/) (che esce a fine mese) mi cimenterò probabilmente nella traduzione di un tomo importante sulla programmazione in iOS (che uscirebbe in estate).

Il Mac sta suonando [Penguin Reference](http://en.wikipedia.org/wiki/Rockoon_(Tangerine_Dream_album)) a ricordarmi che abbiamo perso tra gli altri Edgar Froese e che le cose belle vanno gustate e apprezzate ogni volta che è il loro tempo.

C’è un po’ di [sincronicità](https://lospecchiodelpensiero.wordpress.com/2012/11/05/la-sincronicita/) in questa Pasqua. Buon Passaggio a tutti e ciascuno e grazie per condividere il cammino.