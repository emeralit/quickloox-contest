---
title: "Requisitoria revival"
date: 2013-07-09
comments: true
tags: [Apple, Ebook]
---
Credevo che non mi sarebbe più capitato di leggere questo tipo di cose, per lo meno non a questi livelli. Invece, [il New York Times](http://www.nytimes.com/2013/06/18/technology/apple-executive-defends-pricing-and-contracts-in-antitrust-case.html?_r=0):

>Entrambe le parti hanno mostrato le prove a proprio favore mediante un proiettore. I legali di Apple hanno usato un MacBook per scorrere tra le evidenze documentali, confrontandole su uno schermo suddiviso in più parti e zoomando su paragrafi specifici.<!--more-->

>All’opposto, gli avvocati del Dipartimento della Giustizia potevano mostrare solo una prova per volta. Un video riprodotto come prova è rimasto privo del commento audio necessario.

Vedremo se Apple sia colpevole di [avere tramato con gli editori](http://www.justice.gov/atr/cases/applebooks.html) per stabilire prezzi anticoncorrenziali sugli *ebook*. Una prova è intanto incontrovertibile: funziona. Meglio.