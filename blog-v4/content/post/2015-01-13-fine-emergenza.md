---
title: "Fine emergenza"
date: 2015-01-13
comments: true
tags: [email, Mac]
---
Uno dei Mac era in riparazione e improvvisamente bisognava mandare una email piuttosto urgente. Pareva un problema.<!--more-->

Ho compiuto una rapida ricognizione degli apparecchi presenti in casa, ciascuno capace di consentire composizione e invio della email, al netto del Mac in riparazione.

Sono sette.

Se si fosse guastato il telefono e ci fosse stato da comporre un numero urgente, le alternative sarebbero state tre.

Si fosse rotto il televisore, ce ne sarebbe stato un altro (più due schermi iOS con la app giusta).

Senza la cucina o senza il microonde, avremmo dovuto arrangiarci con il microonde o la cucina.

È l’era digitale perché gli strumenti sono onnipresenti e sempre a disposizione. E in numero pro capite in crescita costante. Tra poco arriveranno gli orologi.