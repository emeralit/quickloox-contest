---
title: "Un altro mondo"
date: 2015-01-12
comments: true
tags: [Masterchef]
---
Ho visto cinque minuti di [Masterchef Italia 4](http://masterchef.sky.it): almeno davanti alle telecamere sono persone con la passione per la cucina, che affrontano sfide di piccola o grande difficoltà per arrivare a vincere.<!--more-->

Sono la punta di una piramide immensa: tutti coloro che, senza avere un lavoro di cucina, cucinano. Probabilmente sono partiti dal nulla o quasi nulla: un uovo sodo, un toast cotto e sottiletta, una pastasciutta, un sofficino surgelato. Risolte quelle difficoltà, hanno provato qualcosa in più, di più sofisticato, più difficile, magari con un piccolo elemento di sfida o con qualcosa da imparare, sia besciamella oppure ragù.

Ora queste persone normalissime, che non hanno un lavoro in cucina ma cucinano e dall’uovo sodo sono magari arrivate a preparare un arrosto o una torta di mele, le trasporti nell’informatica.

Nove su dieci imparano qualcosa più dello stretto necessario solo ed esclusivamente se non ne possono fare a meno. Se fossero ancora in cucina direbbero cose tipo *sono disposto a provare una nuova ricetta, basta che sia uguale a quella che già so, abbia lo stesso sapore e non porti via tempo prepararla*.

Tipo quelli che se gli togli Excel sono dispostissimi a provare altro, basta che sia identico a Excel.

Anche in cucina ci sono quelli che imparano zero; personalmente, tra i fornelli, non ho mai superato la soglia della sopravvivenza.

Però sono circondato da gente che cucina, che sa cucinare, che vuole cucinare, che fa esperimenti, prova cose nuove, si vanta di ciò che è riuscita a fare.

Quanti sono uno su dieci in cucina, sono nove su dieci in informatica. Che continua a restare universo parallelo dalle regole diverse rispetto a tutto il resto della vita.