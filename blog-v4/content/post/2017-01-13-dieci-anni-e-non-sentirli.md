---
title: "Dieci anni e non sentirli"
date: 2017-01-13
comments: true
tags: [iPhone]
---
Vorrei resistere il più possibile alla tentazione di unirmi al coro delle celebrazioni del decennale dell’annuncio di iPhone.<!--more-->

Mi interessa, certamente, ricordare la [presentazione di prodotto migliore di tutti i tempi](https://www.youtube.com/watch?v=t4OEsI0Sc_s). Quella combinazione di efficacia, potenza del messaggio e persino umorismo non è mai stata superata.

(E [lo sforzo dietro le quinte](https://9to5mac.com/2017/01/09/steve-jobs-iphone-presentation-2007-inside-story/) per farla riuscire è stato, retrospettivamente, incredibile).

iPhone ha rivoltato come un calzino il mondo della comunicazione personale e del *computing*, chiaro, ma è così ovvio. Tutto arriva da lì così come nell’occasione precedente arrivò da Macintosh. Non c’è molto da dire, al massimo osservare le persone alla fermata dell’autobus.

Per il resto, amo il *retrocomputing* e mi piacciono i ricordi, ma indulgere troppo mi disturba. Quando Steve Jobs tornò in Apple nel 1996, prese come ufficio una stanza ingombra di vecchi Mac e accessori. Ordinò di portare tutta quella spazzatura, parole sue, all’università di Stanford, dove curano la memoria storica di Apple. Forse questo è un eccesso. Ma anche una lezione. Mai troppo attaccati al passato; significa che il presente non è soddisfacente. E, sempre Jobs, sarebbe allora più il caso di cambiare che rivangare.

Non esce tutti gli anni, un prodotto che cambia il mondo. Macintosh è uscito nel 1984, iPhone nel 2007. Mi piace di più pensare al 2030, dove uscirà qualcosa di altrettanto straordinario e dirompente. O forse no, o forse prima. Delle due fotocamere di iPhone, preferisco quella che guarda avanti.

<iframe width="560" height="315" src="https://www.youtube.com/embed/t4OEsI0Sc_s" frameborder="0" allowfullscreen></iframe>