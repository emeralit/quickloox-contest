---
title: "Tanti per cinque per mille"
date: 2019-05-07
comments: true
tags: [ AAA, AllAboutApple, 5x1000]
---
Faccio il verso a Wikipedia: se ogni lettore di questa pagina desse il cinque per mille a All About Apple, farebbe opera non buona ma straordinaria per un’iniziativa sempre più *insanely great* man mano che passano gli anni.

All About Apple ha fatto la sua parte, con [quindici ragioni](https://www.allaboutapple.com/2017/06/5x1000-ad-all-about-apple-15-buoni-motivi/) per meritarsi il contributo che non fanno una grinza. Per chi li ha conosciuti di persona, poi, veramente non ci sono dubbi.

Almeno quella parte di rapina statale su cui possiamo decidere, decidiamo bene.
