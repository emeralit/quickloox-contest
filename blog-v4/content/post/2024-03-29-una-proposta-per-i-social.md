---
title: "Una proposta per i social"
date: 2024-03-29T00:15:57+01:00
draft: false
toc: false
comments: true
categories: [Internet, Software]
tags: [Swift Playgrounds, Try Ruby, Practical Common Lisp, Duolingo]
---
Mi sono reso conto che ogni tanto dedico qualche minuto ai social anche oltre le questioni di lavoro. Alla fine il tempo cumulato nella giornata potrebbe anche valere una mezz’ora.

A volte serve a staccare tra un lavoro e l’altro, oppure a recuperare la concentrazione dopo una pausa, o ancora se c’è una breve attesa che non vale la pena di colmare con l’impostazione di altro lavoro.

I social hanno una loro utilità. Ci sono amici da seguire, persone autorevoli da leggere, aggiornamenti sulle cose del mondo. Ricordo tuttavia di quando ho compiuto il primo viaggio di una certa durata fuori dall’Italia.

Ho viaggiato dentro gli Stati Uniti per quarantacinque giorni, su rotte – constatato – decisamente poco battute dai turisti italiani. E anche quando mi sono trovato in luoghi molto battuti, da Los Angeles a New York, tutto facevo tranne che cercare un quotidiano italiano.

Sono stato per oltre un mese e mezzo a non leggere quotidiani italiani, cosa che a casa facevo ogni giorno. Quando sono rientrato in Italia, il giorno dopo sono andato in edicola con una forte curiosità: chissà quante cose erano successe, chissà su quanti avvenimenti ero rimasto indietro.

Con sorpresa, vidi che non era vero. Di fatto niente di rilevante era successo; i dibattiti politici, complice anche l’estate, erano sostanzialmente dove li avevo lasciati, per esempio. Di pagina in pagina, di sezione in sezione, tolti i naturali aggiornamenti delle classifiche sportive, mi ero perso poco o nulla.

Ho la sensazione che con i social succeda la stessa cosa, anche se non ci si collega per qualche ora o per qualche giorno. Se l’obiettivo non è il social in sé ma avere notizie.

Che cosa fare al posto dei social? Serve qualcosa che duri qualche minuto, che si possa anche lasciare a metà, che possa dare soddisfazione anche in tempo breve, che abbia un effetto complessivamente positivo, che non sia essenziale ma che, magari, mi porti qualcosa che i social non mi portano.

Avevo pensato di dedicarmi a una lingua straniera con [Duolingo](https://en.duolingo.com), che memorizza tutto e ha lezioni brevi. Temo però che chieda troppo rispetto alla condizione mentale da pausa tra lavori. E a volte una lezione può portare via diversi minuti. Lasciarla a metà non funziona, per l’apprendimento prima ancora che per l’interfaccia.

Ho sempre cercato di sapere qualcosa in più sulla programmazione. Ho la passione di Lisp, solo che richiede impegno; pormi come proposito di leggere [Practical Common Lisp](https://gigamonkeys.com/book/) poco per volta, ad esempio, mi lascerebbe spesso insoddisfatto, perché mi capiterebbe di dover dedicare troppo tempo al tema. Metti che ci sia da debuggare codice che non funziona; metti che un concetto sia particolarmente difficile, da non poter lasciarlo a metà senza averlo capito per intero; e così via.

Ci sono ottime soluzioni sul web che farebbero al caso mio a mani basse, per esempio [Try Ruby](https://try.ruby-lang.org). In tutta franchezza, però, non mi interessano. Se deve sostituire i social, deve essere una cosa che, per carità, per forza richiede impegno, ma per la quale abbia almeno un inizio di interesse. Ruby, come altri linguaggi, sono interessanti, completi, potenti, approcciabili. Non sono la mia tazza di tè (per altri andranno invece benissimo e ci mancherebbe).

Potrei avere la soluzione: [Swift Playgrounds](https://developer.apple.com/swift-playgrounds/). È guidato, si ricorda le cose, è possibile lasciare una lezione o del codice a mezza strada, Swift non è la mia grande passione ma voglio assolutamente conoscerlo meglio, è Apple, posso usarlo con pari interfaccia e senza fatica tanto su Mac quanto su iPad.

Sa molto di proponimento da anno nuovo e siamo in zona primo aprile, ma ci provo lo stesso: da dopo Pasqua voglio lanciare Swift Playgrounds invece di lanciare un social. Vediamo se riesco e che cosa succede.