---
title: "Roba da film"
date: 2013-10-28
comments: true
tags: [iPad]
---
Un regista di nome John Gibson ha realizzato – e vende – un lungometraggio girato quasi tutto su iPad, [Curtis & Dave Are Dead](http://www.curtisanddavearedead.com), con l’aggiunta di un [microfono Røde](http://www.rodemic.com). Solo per alcune sequenze in grandangolo, [afferma The Unofficial Apple Weblog](http://www.tuaw.com/2013/10/26/shooting-a-feature-length-movie-with-an-ipad-curtis-and-dave-are/), è stata utilizzata una videocamera Sony.<!--more-->

Montaggio in Final Cut Pro su un Mac mini e un MacBook Pro. Insomma, serve ancora qualche soldo per darsi al cinema, ma non più come prima. Il bello di iPad è che toglie di mezzo anche tante scuse.