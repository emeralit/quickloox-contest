---
title: "Birra e velocità"
date: 2012-12-15
comments: true
tags: [iMac, Speedmark, Macworld, i7]
---
Ultima puntata del trittico su iMac.

Al momento non ci sono test di velocità delle edizioni base dei nuovi modelli.<!--more-->

Sono stati testati quelli di punta, con il processore i7. Possiamo allora accontentarci per ora del confronto con gli iMac di punta dell’anno scorso.

Il test Speedmark 8 di Macworld.com dà <a href="http://www.macworld.com/article/2013458/measure-mac-performance-with-speedmark-8.html">I seguenti risultati</a>. Di fianco al vincitore ho messo il vantaggio percentuale:

iMac 21,5” 2012: 298 (+53)
iMac 21,5” 2011: 195

iMac 27” 2012: 312 (+22)
iMac 27” 2012: 256

Per i modelli base, aspettiamo i risultati. Per chi punta in alto, un iMac 2012 spinto al massimo dà la birra a un iMac 2011 spinto al massimo e qualcosa vorrà dire.