---
title: "La macina del tempo"
date: 2016-05-12
comments: true
tags: [Time, Machine, LaCie, Rugged, Toshiba]
---
Ho cambiato disco di *backup* Time Machine. [Quasi tre anni fa](https://macintelligence.org/posts/2013-08-12-un-nuovo-backup/) avevo adottato un [Rugged Triple LaCie](http://www.lacie.com/it/products/product.htm?id=10553) che adesso rifiuta di obbedire a qualsiasi comando significativo, da Utility Disco o anche `diskutil` da Terminale. Era costato 169,95 euro ed è durato 1.004 giorni, per una spesa di quasi diciassette centesimi al giorno.

Ho limitato l’accanimento perché un disco di *backup* deve funzionare tranquillo, non a qualunque costo, non tirato per i capelli. Deve essere fidato.

Il successore è un anonimo Toshiba, sempre da un terabyte, sempre autoalimentato, stavolta Usb invece che Firewire. Non posso ancora dire del costo perché trattasi di un regalo di Natale e non ho fatto ricerche sul numero di serie per risalire al modello preciso.

Però, ecco, oggi il costo del *backup* è quello di riuscire a farsi regalare un disco. E se dopo tre anni il disco ti abbandona, pazienza, avanti un altro. Il valore intrinseco dell’oggetto è diventato abbastanza basso da non pensare al tempo che lo erode e lo sbriciola un pezzettino al giorno.