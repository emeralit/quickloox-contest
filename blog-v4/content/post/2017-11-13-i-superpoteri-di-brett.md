---
title: "I superpoteri di Brett"
date: 2017-11-13
comments: true
tags: [Terpstra, Mail, AppleScript]
---
Chi dice che la rete favorisce l’effimero e il superficiale? È questione di volerla usare a fin di bene, come supereroi, la rete.

Un supereroe è Brett Terpstra, che ha pubblicato nel 2012 uno script per ripulire e compattare il database interno di Mail e nell’anno di grazia 2017 [lo aggiorna](http://brettterpstra.com/2017/10/18/updated-mail-vacuuming-script/) per adeguarlo a High Sierra e lasciarlo compatibile in basso fino a Yosemite compreso.

Di più: siccome appunto c’è la rete, recepisce il lavoro di un’altra persona per completare l’opera. Questa è la collaborazione, più ricca e nutriente dei *mi piace* e delle infinite *condivisioni* di maniera, divisioni senza alcun con che presuppone un rapporto qualsivoglia.

La bella notizia, grazie sempre rete, è che i superpoteri di Terpstra sono a nostra disposizione. Si chiamano AppleScript e macOS, un linguaggio di scripting fatto apposta per emancipare le applicazioni e un sistema operativo che prevede espressamente la possibilità di questa emancipazione, per quanto a volte imperfetta e a volte trascurata.

*A nostra disposizione* vuol dire che possiamo provarci anche se non siamo nati su un pianeta alieno o non ci ha morso una tastiera radioattiva.

A riprova: con qualche anno di ritardo su AppleScript, anche Microsoft permette di automatizzare parti di Outlook tramite Visual Basic. Non c’è che [leggere qualche pagina](https://msdn.microsoft.com/en-us/vba/vba-outlook#) per capire che si tratta di funzioni dirette a chiunque, tranne che a una persona normale cresciuta fuori dal gergo dei programmatori.