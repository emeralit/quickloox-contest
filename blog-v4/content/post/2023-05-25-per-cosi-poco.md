---
title: "Per così poco"
date: 2023-05-25T02:14:19+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Hofstadter, Douglas Hofstadter, Fluid Concepts and Creative Analogies, Concetti fluidi e analogie creative, Copycat]
---
§§§

Uno degli scopi centrali dell’architettura di Copycat è permettere a molte pressioni di coesistere, competere e cooperare simultaneamente le une con le altre in modo da guidare il sistema verso certe direzioni. Il modo in cui viene realizzato è convertire le pressioni in stormi di minuscoli agenti software, ciascuno dei quali gode di una qualche piccola probabilità di essere eseguito. Quando questo avviene, l’agente fa sentire nel sistema la propria presenza e quindi quella delle pressioni di cui esso è rappresentante. Nel tempo, le varie pressioni spingono il sistema in direzioni esplorative che dipendono dai livelli di urgenza di alcun agente. In altre parole, le cause perorate in rappresentanza delle rispettive pressioni vengono portate avanti in parallelo, ma a velocità diverse.

§§§

Traduzione del sottoscritto di un brano da [Concetti fluidi e analogie creative](https://macintelligence.org/posts/2023-04-21-stringhe-slacciate/), che spiega la logica di funzionamento di *Copycat*: un programma sperimentale di trent’anni fa che mirava a esplorare la capacità del computer di emulare i meccanismi di pensiero umano facendo lavorare il computer stesso su problemi di trasformazione di stringhe di testo. L’universo in cui si muove Copycat è composto da domande come questa:

Se *ijk* diventa *ijl*, che cosa diventerà *pqr*?

Copycat agiva in un *microdominio*, uno spazio ridottissimo di pochi componenti molto semplici; una condizione che rendeva più agevole concentrarsi sullo sviluppo e sull’analisi del software. Eppure, come testimonia il breve brano qui sopra, arrivare a qualche risultato aveva implicato concepire e implementare un sistema di una certa complessità.

Oggi un sistema che, [dice Stephen Wolfram](https://writings.stephenwolfram.com/2023/02/what-is-chatgpt-doing-and-why-does-it-work/) e non il mio pizzicagnolo di fiducia, *mette semplicemente una parola dietro l’altra*, dovrebbe essere una *intelligenza artificiale* fatta e finita. Percepisco un disequilibrio tra complessità del sistema e sue ambizioni.