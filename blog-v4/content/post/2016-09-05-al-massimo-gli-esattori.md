---
title: "Al Massimo, gli esattori"
date: 2016-09-05
comments: true
tags: [Sideri, tasse, Corriere]
---
Si [era già citato](https://macintelligence.org/posts/2016-02-19-il-corriere-degli-intrusori/) Massimo Sideri del *Corriere della Sera* e non per dirne benissimo. È che frasi fatte di demagogia un tanto al chilo gli vengono bene, oppure ha accumulato un insieme di macro di tastiera cui fa scrivere gli articoli per risparmiare tempo.<!--more-->

Mai sazio di bersagli facili, si dedica alla [questione irlandese](https://macintelligence.org/posts/2016-08-31-un-posso-falso/) delle tasse di Apple.

Ne è uscito un pezzo dove si cita la *profezia di Calvino* (che non era un santone ma uno scrittore, che salì in cattedra in un ateneo americano a consegnare alla storia alcune [lezioni](http://www.apogeonline.com/webzine/2016/03/08/a-passo-di-chiocciola)). Frasi che lasciano il segno:

>[La società irlandese di Apple che la Commissione Europea ha messo sotto accusa] riceve il denaro come pagamento per «i diritti di proprietà» per poi inviarli offshore, nei paradisi fiscali.

Ho letto – sarà la quinta volta – la [comunicazione della Commissione](http://europa.eu/rapid/press-release_IP-16-2923_en.htm) e manca qualsiasi riferimento a *paradisi fiscali offshore*. Neanche l’[infografica](http://ec.europa.eu/competition/publications/infographics/2016_07_en.pdf) ne fa menzione.

>Dunque, l’iPhone è «disegnato in California», assemblato in Cina, venduto ovunque ma tassato da nessuna parte.

Nella [modulistica](http://files.shareholder.com/downloads/AAPL/2510477685x0x901766/5E7D57D1-50E1-497F-B044-4EBA9B7B826A/10-Q_Q3_2016__6.25.2016_FINAL.pdf) che Apple deve depositare ogni trimestre presso l’equivalente americano della Consob si scrive chiaramente che Apple paga tasse per il venticinque-ventisei percento dei profitti. Poco o tanto, certamente è più di zero. *Tassato da nessuna parte* è una frase a effetto per i lettori ancora disposti ad abboccare (la [crisi dell’editoria](https://macintelligence.org/posts/2016-02-18-leica-q-e-la-crisi/) ha tante cause, una delle quali gli articoli vuoti a parte le facilonerie e le frasi fatte).

E sì che nella vicenda non mancano le possibilità di approfondimento. Per esempio si potrebbe scrivere che Apple non paga tasse su una corposa parte dei soldi che incassa all’estero, perché aspetta che gli Stati Uniti concedano condizioni più favorevoli delle attuali per portarli in patria. Significa che quei soldi Apple *non li può usare* fino a che vengono tassati. Mica per niente l’azienda finanzia il riacquisto delle proprie azioni [emettendo obbligazioni](http://www.bloomberg.com/news/articles/2016-07-28/apple-said-to-plan-dollar-bond-offering-to-fund-share-buybacks), cioè indebitandosi. Non che sia un problema, eh? I soldi all’estero vengono forniti come garanzia. Accadesse qualunque peggio e sotto qualunque regime fiscale, la rimanenza di quel denaro appianerebbe qualunque debito. Però è una situazione paradossale e interessante, segnale chiaro che c’è qualche problema anche nelle norme fiscali internazionali.

Detta più chiaramente: mettiamo che l’Irlanda capitoli e chieda ad Apple i tredici miliardi più interessi considerati dalla Commissione europa tasse non pagate. Mettiamo che le norme americane cambino e Apple trasferisca in patria il gruzzolo irlandese rimasto. Il gruzzolo suddetto verrebbe tassato dagli Stati Uniti. E in pratica gli incassi europei di Apple verrebbero tassati *due volte*. Si può discutere su quanto sia tassazione equa; di sicuro tassare due volte lo stesso denaro è iniquo.

Ma Sideri, nel citare Calvino il profeta, è arrivato a rapidità e leggerezza, cioè le prime due lezioni americane. Alla terza, l’esattezza, lo abbiamo perso. Pensava già agli esattori.