---
title: "La festa dei trent’anni"
date: 2021-06-26T00:25:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [CERN, WorldWideWeb, Windows, Windows 11, Tim Berners-Lee, Microsoft, Redmond] 
---
Nel febbraio del 2019, trent’anni dopo i fatti, il CERN [ha ricostruito WorldWideWeb](https://worldwideweb.cern.ch) per poterlo fare usare a tutti all’interno di un normale browser e provare la stessa sensazione di quando, nel 1990, il lavoro iniziato l’anno prima da Tim Berners-Lee si concretizzò in un progetto usabile.

WorldWideWeb, notare il nome, è *il predecessore della maggior parte di ciò che consideriamo o conosciamo oggi come web*.

La pagina offre sezioni di storia, *timeline*, evoluzione dei sistemi, tipografia, codice, praticamente tutto quello che uno potrebbe volere sapere. Il termine *Windows* è assente, da ovunque.

A mia notizia, l’appropriazione indebita della nascita del web da parte di Windows è stata notata [solo da John Gruber](https://daringfireball.net/linked/2021/06/24/microsoft-introduces-windows-11). Le mie osservazioni sono state accolte anche con malcelato fastidio, una cosa tipo *sì, un po’ esagerato, ma è solo un po’ meno vero rispetto al dire che in molti hanno conosciuto il web tramite Windows*.

Un problema è che la seconda considerazione è certamente vera. Ma il grado di verità esistente nella nascita del web su Windows è zero. È una falsità, non una verità un po’ gonfiata.

Un altro problema, assai più grave, è la risposta a questa domanda: *a che serve millantare la nascita del web su Windows all’atto di presentare Windows 11?* Ci sono letteralmente miliardi di persone che sono nate dopo il web. Non hanno idea di come sia nato. Non penso che perfino una di queste si farebbe convincere a comprare Windows in virtù di questo fatto. I più anziani sanno come sono andate le cose e se dovevano adottare Windows lo hanno già fatto; oppure non lo sanno, e ugualmente Windows lo hanno già scelto. La quota di chi deve scegliere il primo sistema operativo dentro il pubblico di Microsoft è irrisoria e, comunque, dubito abbia bisogno di quello per scegliere.

È una affermazione puramente inutile a qualsiasi scopo pratico. Del tutto gratuita. Da gradassi che si prendono quello che vogliono semplicemente alzando la voce; da teste vuote che hanno bisogno di svuotare tutto attorno a loro per mandare avanti il business.

Oppure, *tout se tient*. C’è gente intelligente che usa Windows e tanta altra che lo usa con la consapevolezza di uno zombi. Suppongo che per mantenere un mercato fondato su queste premesse, un pochino si finisca per essere contagiati da un certo modo di pensare. A uno che usa Windows senza sapere perché puoi scrivere che su Windows sono nati gli spaghetti alla carbonara. Si crea sintonia tra menti simili.

O infine, è quel complesso che a Redmond hanno sempre sofferto, di incapacità di lasciare un segno, miliardi di righe di software e sempre per fare soldi su qualcosa creato da altri. Al CERN hanno festeggiato i trent’anni di qualcosa che ha cambiato la storia del mondo. Loro neanche riescono a [liberarsi del codice di Windows NT](https://twitter.com/jonhoneyball/status/1407230936446611459).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">all you need to know about windows 11 in one screenshot <a href="https://t.co/WWespS2VnY">pic.twitter.com/WWespS2VnY</a></p>&mdash; Jon Honeyball (@jonhoneyball) <a href="https://twitter.com/jonhoneyball/status/1407230936446611459?ref_src=twsrc%5Etfw">June 22, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*