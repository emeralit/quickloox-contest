---
title: "Aggiunte concrete"
date: 2015-11-23
comments: true
tags: [watch, AirDrop, Mac, iOS, Handoff]
---
Lavorando su macchine tutto tranne che appena uscite, mi capita di essere escluso da varie tecnologie Apple dell’ultim’ora. Per dire, non ho i requisiti per usare [AirDrop](https://support.apple.com/it-it/HT203106) tra Mac e iOS e neanche per approfittare di [Handoff](https://support.apple.com/it-it/HT204681).<!--more-->

Tuttavia alcune cose arrivano e scrivo per essermi reso conto concretamente di due cose: avevo un appuntamento e Mac ha inviato una notifica per avvisarmi che era ora di uscire, alla luce delle condizioni del traffico. E ho già constatato più volte che raccogliere da Mail nuovi contatti, o aggiornarne di esistenti, era una noia e adesso invece è un clic. Il software dietro a tutto appare finalmente funzionare a dovere: non fa confusione con i nomi, aggiunge i dati giusti nei campi giusti, *funziona*… o così ha fatto con sette/otto contatti.

Un conto è leggerne su una recensione o su una pagina web, di queste funzioni. Un conto è viverle, per esempio come le funzioni che permettono di mandare un disegnino a mano da watch a un altro watch. Sembrano cose banali o puerili, fino a quando nella vita reale aggiungono davvero qualcosa.