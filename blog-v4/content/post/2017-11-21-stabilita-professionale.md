---
title: "Stabilità professionale"
date: 2017-11-21
comments: true
tags: [iPad, Pro, Clip, Studio, iOS, Photoshop, Snell, Procreate, Affinity]
---
Per una volta arriva qualche interrogativo sensato dal tema, che altrimenti trovo surreale, dei professionisti e delle specifiche dei computer.<!--more-->

Jason Snell di *Six Colors* [racconta](https://sixcolors.com/post/2017/11/adobes-ios-failure/) di non essere un professionista della grafica ma di usare Photoshop da sempre, al punto da essere disposto a pagare l’abbonamento a Creative Cloud pur di non dove esplorare altre alternative.

Lui stesso specifica però che lealtà e familiarità, per quanto forti, hanno i loro limiti:

>Se Photoshop abbandonasse Mac, non passerei a Windows. Se Adobe decidesse di chiedere trecento dollari l’anno per Photoshop, probabilmente non li pagherei.

Che succede se lo strumento di elezione del professionista stenta su una piattaforma diversa dalla solita?

Snell constata che su iPad Photoshop, smembrato da Adobe in varie app ancillari, delude su compiti semplici rispetto ad alternative come [Affinity Photo](https://itunes.apple.com/it/app/affinity-photo/id1117941080?mt=8) o [Procreate](https://itunes.apple.com/it/app/procreate/id425073498?mt=8). E allora un iPad Pro con Apple Pencil sarebbe uno strumento non professionale, visto che Photoshop lì sopra non rende, oppure lo è a patto di usare strumenti diversi da Photoshop e così negare a quest’ultimo la qualifica di programma professionale *tout court*?

Le carte si mischiano in modo interessante quando nella discusione entra l’esempio di una fumettista americana di alto livello.

Secondo la quale niente batte un computer fatto e finito assieme a una tavoletta [Cintiq](http://www.wacom.com/en-us/products/pen-displays). Però riconosce l’opportunità di ricorrere a sistemi più leggeri che consentono un lavoro di grande qualità.

La *cartoonist* userebbe tranquillamente un iPad Pro, ma non avrebbe Creative Cloud. Così usa un [Mobile Studio Wacom](https://www.wacom.com/en/products/pen-computers/wacom-mobilestudio-pro-16) in quanto, completa di Creative Cloud, può usare certi pennelli su misura che sono un’esclusiva Adobe.

Tutto questo dura fino a che [si accorge dell’esistenza](https://twitter.com/heyjenbartel/status/929522777903812608) di [Clip Studio per iPad](https://itunes.apple.com/it/app/clip-studio-paint-ex-for-manga/id1262985592?mt=8).

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Well, this changes things. 😱 Clip Studio is making me revisit drawing on my iPad.....!!!! <a href="https://t.co/wM4w2c3AM3">pic.twitter.com/wM4w2c3AM3</a></p>&mdash; Jen Bartel (@heyjenbartel) <a href="https://twitter.com/heyjenbartel/status/929522777903812608?ref_src=twsrc%5Etfw">November 12, 2017</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

Non basta a farla passare armi e bagagli su iPad Pro (10,5”, si badi bene), anche perché trova il software in abbonamento mentre ora è in promozione gratis ancora per un mese. Ma lo raccomanda a chiunque cominci adesso a disegnare con certe ambizioni.

La morale. Photoshop o altro software *professionale* è il mezzo, non il fine. Usi Photoshop perché sei professionista e non il contrario.

Nel contempo, sei professionista perché sai discernere su qualsiasi piattaforma la combinazione di strumenti più evoluta per il tuo lavoro, qualunque essa sia. Se tutto passa attraverso il filtro del tuo programma preferito, stai perdendo dei pezzi per strada. Il professionista non rimane abbarbicato allo stesso strumento per una vita; cava sangue dalle rape con qualunque cosa abbia a disposizione.