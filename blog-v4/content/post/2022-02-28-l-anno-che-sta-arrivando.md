---
title: "L’anno che sta arrivando"
date: 2022-02-27T23:58:20+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Hugo, Coleslaw, Mimmo, Octopress]
---
Di solito scrivo per il blog e poi, se avanza tempo, sviluppo per il blog.

Stasera eccezione, perché sono stati aggiunti al blog i post scritti quando il motore di pubblicazione era [Coleslaw](https://github.com/coleslaw-org/coleslaw); grosso modo, l’anno 2021.

La sessantina di post presenti in questa edizione del blog cresce così oltre quota quattrocento.

Per paradosso, il lavoro più impegnativo è stato compiuto proprio sui post attuali; il formato dell’intestazione utile al motore è infatti stato razionalizzato e semplificato anche per facilitare il rientro dei vecchi post oltre che la stesura di quelli nuovi. Poi è stata questione di qualche espressione regolare.

Ho ancora da ringraziare **Mimmo** (e famiglia…) per tanto lavoro dietro le quinte e una disponibilità per cui non bastano le parole.

Questo è solo l’antipasto, perché ora parte il lavoro di conversione del periodo di [Octopress](http://octopress.org), che abbraccia diversi anni e quindi migliaia di post. Mi aspetto che sia questione di giorni o di poche settimane, salvo stranezze imprevedibili.

Poi toccherà a post sparsi nel passato e meno organizzati. L’obiettivo è riportare tutta la pubblicazione blogghistica di cui dispongo a un formato unificato e sotto un singolo motore.

Se le intestazioni sono collaudate e tecnicamente non ci sono ragioni evidenti per dubitare che tutto funzioni, da un punto di vista di contenuto è possibile che vecchio materiale contenga problemi di sintassi, di link morti, di refusi e quant’altro. È umanamente impossibile verificare tutto in una sola passata e invito chiunque alla pazienza nonché alla segnalazione di qualsiasi cosa non vada. Tutto quanto è noto verrà sistemato e aggiornato.

Un saluto affettuoso a coleslaw, che rimane il mio sogno proibito di motore. Una volta terminato il recupero dei contenuti riprenderò a lavorarci sopra, con l’obiettivo non dichiarato di utilizzarlo nuovamente come motore; ma prima dovrà assolutamente conservare e semmai migliorare tutto il lavoro che oggi è confluito qui su [Hugo](https://gohugo.io), che mi piace e basta – mentre coleslaw porta con sé anche aneliti del cuore – e al quale tuttavia devo molto.

Il lavoro prosegue. Chiedo indulgenza per l’autocelebrazione; d’altronde, questa è davvero una serata speciale. Soprattutto, chi segue il feed RSS del blog dovrà avere pazienza e perdonare una intrusione numericamente importante.