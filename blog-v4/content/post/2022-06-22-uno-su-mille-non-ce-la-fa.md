---
title: "Uno su mille non ce la fa"
date: 2022-06-22T01:35:13+01:00
draft: false
toc: false
comments: true
categories: [Web, Hardware]
tags: [Apple, New York Times, right-to-repair, iPhone]
---
Eccoci qua. Il *New York Times* titola [Ho provato il programma di riparazione fai-da-te di Apple con il mio iPhone. Con esito disastroso](https://www.nytimes.com/2022/05/25/technology/personaltech/apple-repair-program-iphone.html).

Sottotitolo: *gli strumenti e le istruzioni di Apple sono tutt’altro che ideali per molti di noi. Lo so perché, cercando di farne uso, ho rotto il mio iPhone*.

Non voglio neanche leggere l’articolo, perché non è una notizia. È una statistica.

Può darsi che il kit di Apple sia *tutt’altro che ideale* e favorisca qualche manovra sbagliata da parte di un incompetente. Può darsi che sia il peggiore kit possibile. Anche se fosse il migliore, una volta distribuito un numero sufficiente di kit, è matematico che qualcuno non riuscirà a usarlo.

Allora apparirà un articolo su un quotidiano e Apple ne uscirà male, qualunque sia il livello di qualità del kit, qualsiasi siano gli strumenti e le istruzioni, comunque siano realizzati gli iPhone. Anzi, più sono il meglio del meglio, più un’incapacità di usarli farà notizia e scalpore.

Si capisce perché Apple abbia varato il programma di autoriparazione: per calmare gli esagitati del *right-to-repair*. E si capisce che lo abbia fatto per forza, dato che comunicativamente parlando si tratta di uno sparo nel piede annunciato.

Non si poteva fare altro. La cosa bizzarra però è l’impossibilità di instaurare una discussione normale sul right-to-repair, che è tutto tranne un *right*. Se mi danno gli strumenti e non sono capace di usarli, ho il diritto di incolpare il costruttore?

Hai il diritto di riparare il tuo iPhone se prima hai esercitato il tuo diritto di studio e hai imparato come si fa. In altre parole, non hai alcun diritto; semmai è giusto chiedere la possibilità di provarci, che è tutt’altra cosa.