---
title: "Come da programma"
date: 2014-04-30
comments: true
tags: [CodeCombat, JavaScript]
---
Di tutte le strategie che ho visto in rete per arrivare a maneggiare un pizzico di programmazione, [CodeCombat](http://codecombat.com) è certamente la più folle.<!--more-->

L’apprendimento è affidato a una ambientazione di semplice combattimento tra umani e ogre, dove si può impersonare l’una o l’altra categoria e l’obiettivo è distruggere la base nemica. I soldati corrispondono effettivamente a programmi JavaScript che si modificano in tempo reale, attivando metodi di difesa o attacco, pensandone di nuovi, facendo esperimenti. Il tutto mentre si gioca contro una intelligenza artificiale oppure contro altri umani.

La prima impressione è positiva. Nell’essere messi sotto pressione, per quanto ludicamente, c’è un valore (ed è possibile mettere la situazione in pausa per riflettere sul codice; ogni battaglia dura sessanta secondi). La concentrazione aumenta, si sta più attenti, dopo pochi istanti non importa più che sia un giochino. Bisogna vedere se a… gioco lungo si riesce a mantenere un percorso di apprendimento. In tutti i casi, sapere che esiste una risorsa così, gratis per giunta, è un tesoro. Spero che ci siano insegnanti capaci di approfittarne, visto che si può giocare anche in squadra.

E, incredibile a dirsi, c’è la versione in italiano. O meglio la possibilità di contribuire alla traduzione. Fondamentale anche per il docente di lettere.