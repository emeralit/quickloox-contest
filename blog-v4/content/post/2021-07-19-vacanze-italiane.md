---
title: "Vacanze italiane"
date: 2021-07-19T00:21:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Internet] 
---
Capisci quando la stagione balneare è veramente decollata nel momento in cui la banda disponibile sulla connessione telefonica atterra. E comincia a scavare.

Il canarino nella miniera, nel mio caso, sono le situazioni collaborative con audio e video; a inizio giugno era come stare sulla Adsl, a inizio luglio mi toccava uscire sul balcone per avere il video e adesso si fa fatica ad avere l’audio.

Dipende da dove ci si trova e da mille altre faccende, ovviamente. Non si può neanche biasimare la gente perché guarda la partita su iPhone mentre cerchi di svolgere la tua riunione oppure twitta, condivide, selfizza, posta, pubblica, filma, instagramma, tiktokka e facebookka per mostrare il fritto misto o i sempreverdi piedi in fondo alle gambe poggiate sulla sabbia, sulla branda, sulla sdraio. Sono per la *Net neutrality* ed è giusto che sia spazio sulla rete per tutti i comportamenti, anche quelli che non sono produttivi *secondo qualcun altro*. Siamo tutti gli altri di qualcuno.

Dovrebbe però esserci banda per tutti, sempre, comunque. Sembra esagerato? Insostenibile? Antietico? Dico solo che nel mondo, [ogni volta che ha cancellato due posti di lavoro, Internet ne ha creati cinque](https://www.mckinsey.com/~/media/McKinsey/Industries/Technology%20Media%20and%20Telecommunications/High%20Tech/Our%20Insights/The%20great%20transformer/MGI_Impact_of_Internet_on_economic_growth.pdf).

Nelle economie più sviluppate e vitali, Internet ha un ruolo nel prodotto interno lordo ampiamente superiore al cinque percento. È stata più volte verificata la correlazione tra aumento della banda a disposizione pro capite e aumento del prodotto interno lordo in questione.

Se le vacanze italiane facessero trovare ai vacanzieri banda tipo coreana, la nostra economia funzionerebbe meglio e ci sarebbe più benessere per ognuno. Ci sente qualcuno nella stanza dei bottoni, o sono tutti in vacanza anche lì?

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*