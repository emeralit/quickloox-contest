---
title: "Post-pre-inaugurazione"
date: 2015-10-25
comments: true
tags: [AAA, AllAboutApple, Savona, beacon, Bluetooth]
---
Due mesi fa [avevo visitato](https://macintelligence.org/posts/2015-08-25-lavori-in-corso-in-piazza-in-darsena/) la nuova sede in via di allestimento di All About Apple Museum e ho accettato di non divulgare le foto dei lavori. La situazione era questa.<!--more-->

 ![All About Apple a fine agosto](/images/lavori.jpg  "All About Apple a fine agosto") 

I soffitti del tutto da terminare, le luci ancora da montare, tutto abbondantemente da pulire, insomma, la strada era lunga.

Ieri sono stato alla [pre-inaugurazione](http://www.allaboutapple.com/2015/10/ventiquattro-un-numero-che-sa-dimpresa/) per stampa, VIP e istituzioni. Impossibile replicare la stessa inquadratura panoramica: troppo movimento di troppa gente troppo in fretta. L’unico risultato accettabile è questo.

 ![All About Apple a fine ottobre](/images/totale.jpg  "All About Apple a fine ottobre") 

Manca qualche rifinitura e le macchine sono spente, ma l’ambiente è pulito, i soffitti sono completati, le luci sono a posto e ci sono le macchine. Lavoro straordinario, veramente in lotta contro il tempo, arrivando a vincere.

Mi hanno permesso di dire qualche parola e ho cercato di spiegare come All About Apple sia il nucleo di quello che manca alla nuova informatica personale: una robusta iniezione di umanesimo, che nutra le macchine della passione di chi le usa e le spinga in direzioni sempre nuove e costruttive. La passione è stata la costante della prima informatica, che era di pochi; quella nuova è per tutti, solo che senza passione si finisce per seguire la macchina, invece che dominarla.

 ![Accessori vari](/images/developers.jpg  "Accessori vari") 

La soluzione è coltivare storia e tradizione del passato, per fare entrare l’informatica nella nostra cultura oltre che nelle tasche e nelle borsette. L’inizio di tutto ciò è un museo come All About Apple.

Che stia a Savona è solo naturale: i porti sono da millenni crocevia di commerci, comunicazioni, persone. Esattamente quello che oggi sono i computer. E se negli Stati Uniti esiste una [Savona](http://www.villageofsavona.com), nella Savona italiana può benissimo situarsi un museo Apple.

 ![Dall’Apple I](/images/macchine.jpg  "Dall’Apple I") 

L’inaugurazione per il pubblico è fissata al 28 novembre e per quella occasione i lavori saranno stati completati interamente: le macchine saranno in funzione – perché un museo di computer ha un senso solo se i computer sono accesi – e i *beacon* Bluetooth forniranno informazioni istantanee a chi si avvicinerà ai tavoli. Ci sono altre sorprese in programma ma, essendo tali, è giusto che lo restino.

È stata proprio una bella giornata, per giunta con il sole, e l’entusiasmo di staff e pubblico, persino delle autorità, era palpabile. Bello vedere l’elenco dei privati finanziatori che con pochi o tanti euro hanno contribuito a fare rinascere il museo. Ho riconosciuto qualche nome che passa da questo blogghino e mi sono emozionato.

 ![Lo staff intrattiene i visitatori](/images/maglie-blu.jpg  "Lo staff intrattiene i visitatori") 

Bello anche constatare che, dopo quattro anni di interregno alla ricerca di una vera sede come questa, All About Apple ha cambiato pelle e però cuore e cervello sono sempre gli stessi. L’associazione merita più che mai sostegno per il lavoro portato avanti finora e per quello che attende; come minimo, siamo tutti tenuti a presenziare il 28 novembre. Per capire il senso di un museo Apple bisogna trovarcisi.

 ![L’evoluzione del mouse](/images/mouse.jpg  "L’evoluzione del mouse") 
