---
title: "Un testo per l’estate"
date: 2023-06-20T18:21:07+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [50 Years of Text Games]
---
Ho partecipato a suo tempo al prefinanziamento di [50 Years of Text Games](https://if50.textories.com), quando ho accennato alla [vicenda di LambdaMOO](https://macintelligence.org/posts/2021-06-01-i-danni-del-gioco-mancato/) come viene narrata nel libro.

Ora il lavoro è stato completato e *50 Years of Text Games* è completo, scaricabile e anche in stampa a seconda del livello di KickStarter che si era scelto.

Ci sono opzioni di acquisto anche per chi non abbia partecipato all’iniziativa e scopra il libro oggi. Naturalmente deve piacere il genere; cinquant’anni di giochi testuali sono una materia assai specifica.

Se piace il genere, raccomando l’investimento. Come lettura estiva, non si batte.