---
title: "Conto su iPad"
date: 2017-04-13
comments: true
tags: [Viticci, MacStories, Numbers]
---
Federico Viticci manda avanti un sito a tema Apple, *MacStories*, scritto in inglese con abbondanza di collaboratori madrelingua, specie dagli Stati Uniti. Di conseguenza si ritrova a dover maneggiare una contabilità personale mista in dollari ed euro, che va convertita in euro prima di divenire ufficiale.<!--more-->

Viticci vuole che questo lavoro sia il più possibile automatico e soprattutto avvenga su un iPad Pro. E [ha raccontato come utilizzi](https://www.macstories.net/ios/ipad-diaries-numbers-accounting-and-currency-conversions/) un misto di servizi web, automazione iOS e Numbers 3.1, versione ideale per lo scopo.

Condiviso solo parzialmente il suo approccio iPad-only: mi muovo per avere un ambiente dove Mac, iPad e in ambiti limitati iPhone siano il più possibile intercambiabili.

Tuttavia trovo i suoi pezzi eccezionali, per l’ampiezza delle conoscenze e la profondità delle soluzioni: una combinazione molto rara nel web di oggi. Questo articolo è piena conferma.