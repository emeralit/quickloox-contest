---
title: "Il suono della legge"
date: 2018-02-17
comments: true
tags: [HomePod, FastCompany, NTi]
---
Apple sostiene che HomePod diffonde in una stanza sonoro di qualità equivalente ovunque venga piazzato nella stanza; e che la qualità del suono sia uniforme per le persone presenti nella stanza a prescindere da dove si trovino.<!--more-->

FastCompany [ha voluto verificare](https://www.fastcompany.com/40530175/acoustics-tests-show-apples-homepod-audio-claims-are-legit) se sia vero e si è rivolta agli specialisti di [NTi Audio](http://www.nti-audio.com/en/home.aspx).

È vero.

>Come riesce HomePod in questa magia? Mediante alcuni algoritmi molto sofisticati di elaborazione audio. Mentre riproduce la musica, HomePod registra il comportamento delle onde sonore emesse grazie a sei microfoni posizionati nella parte interna dell’apparecchio. Queste informazioni vanno in pasto agli algoritmi, che regolano l’uscita dei diffusori per rendere il suono uniforme in tutta la stanza.<br />
Un altro microfono posto dentro HomePod ricerca interferenze dovute a pareti o altri oggetti di grandi dimensioni. Se l’unità determina di essere posizionata vicino a un muro, un algoritmo regola la modalità di diffusione dei bassi.

L’articolo cita pienamente a proposito la terza legge di Clarke: *ogni tecnologia sufficientemente avanzata è indistinguibile dalla magia*.