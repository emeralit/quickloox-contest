---
title: "Un mondo senza frizione"
date: 2014-02-02
comments: true
tags: [iOS, iPad, iPhone, Pages, Inspire]
---
Sto per chiudere un conto bancario aperto dal secolo scorso: nei dintorni del Natale ho cercato di usarlo per pagare *online* due biglietti del cinema. Sono stato informato che l'uso su Internet era stato disabilitato *per la mia sicurezza* e avrei dovuto adoperarmi per ripristinarlo.<!--more-->

Durante una breve puntata a Lione in Francia, ho acquistato i biglietti per il tram veloce dal centro città alla stazione dei *Train Grand Vitesse*, Tgv. L’ho fatto dalla camera d'albergo, con iPad. Ovviamente con un’altra carta di credito. Nessun biglietto fisico: sul tram un addetto ha verificato la carta di credito ed è bastato.

La *app* del Tgv mi ha comunicato il binario del treno. Non ho dovuto neanche guardare i tabelloni. Neanche a dirlo, iPad mi ha permesso di coprire le piccole comunicazioni di famiglia e di lavoro; ho anche prodotto dentro Inspire Pro una firma, poi inserita in un noioso ma urgente modulo burocratico già importato dentro Pages. Da non raccontare in giro, specie se funziona.

In questi giorni ho usato iPad e iPhone ad ampio raggio per tutta una serie di compiti, alcuni dei quali si sarebbero potuti fare comunque anche nel mondo di una volta, ma con molta più difficoltà. E altri che sarebbero risultati impossibili.

Nessuno di questi era un lusso da ricchi o un privilegio di pochi. Il digitale, se condotto con criterio, elimina frizioni e semplifica la vita. Il cambio automatico per la quotidianità.