---
title: "L’anello debole"
date: 2016-03-10
comments: true
tags: [KeRanger, Transmission, XProtect, ransomware]
---
Alla fine la vicenda [Transmission](https://www.transmissionbt.com) si è risolta molto meglio di come è partita.<!--more-->

Per gli ignari, il sito di distribuzione del programma (che tra l’altro è molto ben fatto e raccomando) è stato violato e la versione 2.90 di Transmission per Mac conteneva un *ransomware*, primo caso funzionante conosciuto su OS X (ne era apparso uno, incompleto e non funzionante, un anno fa) di nome [KeRanger](http://researchcenter.paloaltonetworks.com/2016/03/new-os-x-ransomware-keranger-infected-transmission-bittorrent-client-installer/).

(Ransomware significa software che cifra non richiesto i file del sistema, con una chiave nota solo ai pirati che lo hanno scritto, e poi chiede il riscatto per decifrarli).

È andata molto bene per vari motivi:

- Gli unici a correre rischi sono stati coloro che hanno scaricato Transmission versione 2.90 dopo le 20 ora italiana del 4 marzo e prima delle quattro del mattino del 6 marzo.
- Si tratta di [circa 6.500 copie](http://www.forbes.com/sites/thomasbrewster/2016/03/07/keranger-ransomware-hits-apple-mac/#1d5efe5a6fdf). Una ogni diecimila Mac.
- Al 6 marzo Apple aveva già preso le contromisure necessarie e così gli sviluppatori di Transmission. Oggi OS X rifiuterebbe di installare una copia contagiata.

Si presume – non è certo – che nessuno, tra quelli che hanno scaricato, sia caduto vittima del ransomware, che aveva tre giorni di incubazione. Le contromisure dovrebbero avere funzionato prima, per tutti.

Anche nel peggiore dei casi, sembra che [falle crittografiche nella sua struttura](http://www.computerworld.com/article/3042184/security/mac-ransomware-kerangers-flaws-could-let-users-recover-files.html) potrebbero permettere alle vittime di recuperare i propri dati.

Chi ritenga di avere corso o correre dei rischi a causa di un Transmission 2.90 avvelenato, risolve installando la versione 2.92 o superiore.

Il punto è un altro. Questo attacco ha colpito indipendentemente dal livello di protezione e consapevolezza delle persone. Perché i pirati sono riusciti ad alterare la firma digitale di Transmission: il software si presentava ai controlli con tutte le carte in regola.

Amo scrivere che la migliore protezione è tenere il cervello accesso, solo che in questo caso sarebbe servito esattamente a niente.

Invece bisogna guardare con altri occhi a Mac App Store. Da lì possono arrivare solo app sicure a meno che i pirati compromettano i server di Apple; molto più difficile che quelli di Transmission.

Mac App Store non ha avuto finora lo stesso successo di App Store su iOS e molti lo avversano apertamente. Preferiscono la *libertà*, come la chiamano, di scaricare quello che vogliono da dove vogliono.

Compreso Transmission 2.90, tra il 4 e il 6 marzo. Se fosse stato distribuito tramite Mac App Store, questo *post* non esisterebbe e neanche le vicende di cui parla. L’anello debole della catena, stavolta, non era un ipotetico utente ignaro, ma il meccanismo di distribuzione del programma.

Per carità di patria taccio su chi ha alzato il polverone per lasciare intendere che Mac sia vulnerabile a queste cose tale e quale a Windows. A oggi, su settanta milioni di Mac, gli affetti da ransomware potrebbero essere una manciata, o anche zero con un po’ di fortuna.