---
title: "A ognuno il suo linguaggio"
date: 2018-04-02
comments: true
tags: [Swift]
---
Ogni tanto capita di leggere uno stratega della rete che vorrebbe, tipo, iTunes su Linux o iWork su Android, e adduce ragioni a suo dire assai profonde che invece si riducono alla stessa: *voglio il valore senza doverlo pagare*.<!--more-->

È proprio l’ottica a essere distorta. Linux inteso come un succedaneo del Finder è un controsenso; molto meglio il Finder. Android è fatto per giustificare accrocchi di bassa lega a basso costo per gente a basse pretese. Cui bastano e avanzano le Google App. Che neanche sono poi così male.

Piuttosto, se una cosa deve girare su Linux, è il linguaggio di programmazione. E vedere il dato di fatto di [Swift 4.1 disponibile su Ubuntu](https://swift.org/download/#snapshots) come la cosa più normale del mondo, oltre a essere un inedito per Apple, è anche un segno assai positivo.