---
title: "Tempi di apertura"
date: 2014-11-03
comments: true
tags: [Yosemite, Gnu-Darwin, PureDarwin, opensource, OSX]
---
Bisogna essere contenti della [pubblicazione delle parti open source che compongono OS X 10.10 Yosemite](http://www.opensource.apple.com), effettuata con sollecitudine a pochissimo tempo dall’uscita del sistema.<!--more-->

Un po’ meno del grosso ritardo che ha iOS, ancora fermo alla versione 6 per il lato *open*.

Ancora meno per lo stato polveroso in cui si trova il progetto [PureDarwin](http://www.puredarwin.org/) (realizzazione di una immagine disco Iso installabile su qualsiasi computer), fermo a qualche anno fa. E lo stesso vale per [Gnu-Darwin](http://gnu-darwin.sourceforge.net), fermo credo da anni (il sito è completamente indecifrabile senza dedicargli uno studio più accurato di quello che merita).

Rimango del parere che una buona apertura sul fronte *open source* aiuterebbe notevolmente la causa di OS X e in fin dei conti la soddisfazione dell’utilizzatore finale. Proprio perché Apple chiude il buco principale di tutti i progetti *open source*, ovvero una interfaccia utente e un design di eccellenza. Sarebbe davvero una marcia in più.