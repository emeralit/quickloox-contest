---
title: "Piegati ma non spezzati"
date: 2019-02-20
comments: true
tags: [Samsung, Galaxy, Fold, Courier, Microsoft, Office, Windows, Android, iPhone]
---
A fine aprile esce un computer da tasca Samsung dal prezzo annunciato di [millenovecentoottanta dollari](https://www.theverge.com/2019/2/20/18231249/samsung-galaxy-fold-folding-phone-features-screen-photos-size-announcement).

Ricordo i decerebrati che, di fronte a iPhone X oltre i mille dollari, scrivevano [fatevi un viaggio](https://macintelligence.org/posts/2017-09-24-fatevi-un-viaggio/). Tutta gente per cui Android faceva le stesse cose, o meglio, a meno. Sono curioso di vedere che cosa scriveranno ora.

E parliamo dei geni di Microsoft, pronti a concepire il progetto Courier e poi a [seppellirlo](https://www.cnet.com/news/the-inside-story-of-how-microsoft-killed-its-courier-tablet/) una volta rivelatisi incapaci di farne un prodotto reale, intanto che iPad si prendeva tutto il mercato.

Non contenti, si trastullano con [brevetti di uno-due anni fa](http://fortune.com/2017/01/17/microsoft-phone-tablet-patent-fold/) che il 26 aprile verranno superati a sinistra, con un vero prodotto in vendita, da Samsung (mica da Apple; da Samsung).

Il vero motivo per cui sarebbe ora di farla finita con il virus Windows e il malware Office è che entrano in azienda troppi soldi, così che una manciata di privilegiati può concepire grandi idee inutilizzabili dalle persone reali, a meno che ci pensi qualcun altro.