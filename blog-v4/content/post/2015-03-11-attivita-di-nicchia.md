---
title: "Attività di nicchia"
date: 2015-03-12
comments: true
tags: [Parkinson, Stanford, ResearchKit, iOS, Lisp, Lisping]
---
Il valore del [milione e mezzo di *app* su App Store](http://www.pocketgamer.biz/metrics/app-store/) non sta nel numero impressionante: altrimenti avrebbero avuto ragione, nell’età della pietra, quelli che accusavano Mac di avere pochi programmi.<!--more-->

Sta nel servire innumerevoli nicchie con software molto migliore di quello che c’è altrove. Un word processor, un foglio di calcolo, il browser, la posta elettronica, ce l’hanno tutti. Se poi sono quelli di Google, sono uguali dappertutto.

Invece, se sono interessato al linguaggio Lisp, su iPad posso fare funzionare [Lisping](https://itunes.apple.com/it/app/lisping/id512138518?l=en&mt=8) e ricevere un servizio che un androidiano se lo sogna.

Quanti siamo interessati a Lisp in Italia? Cento? Avere Lisping, in sé, sposta niente in termini di vendite. Ma cento nicchie da cento persone fanno diecimila iPad potenziali.

Compro un iPad per avere Lisping? Ma no. Se però ho dieci nicchie che mi interessano e dieci soluzioni di qualità, probabilmente sono più vicino all’acquisto. Si fa più presto di quanto sembri.

Anche la nicchia è un argomento di vendita e per questo sarebbe infantile pensare che Apple abbia varato [ResearchKit](https://www.apple.com/researchkit/), la piattaforma *open source* di ricerca scientifica per iOS, solo in base a qualche ideale di bontà e amore per il prossimo. È società per azioni, il suo scopo finale è creare valore per gli azionisti.

Però poi escono notizie come uno studio di medicina cardiovascolare condotto a Stanford dove, grazie a ResearchKit, in un amen si sono raccolti [oltre diecimila partecipanti volontari](http://appleinsider.com/articles/15/03/11/over-10k-participants-sign-up-for-stanford-medical-trial-after-researchkit-debut).

<blockquote class="twitter-tweet" lang="en"><p>Over 10K participants sign up for Stanford medical trial after ResearchKit debut: <a href="http://t.co/9pv6SQdWJK">http://t.co/9pv6SQdWJK</a></p>&mdash; Shane (@ShaneCole_) <a href="https://twitter.com/ShaneCole_/status/575686320317665280">March 11, 2015</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Altra notizia, in sei ore sono stati reclutati [7.406 volontari](https://twitter.com/wilbanks/status/575125345977810945) per uno studio sul Parkinson. Il test più vasto condotto finora ne comprendeva 1.700.

<blockquote class="twitter-tweet" lang="en"><p>After six hours we have 7406 people enrolled in our Parkinson&#39;s study. Largest one ever before was 1700 people. <a href="https://twitter.com/hashtag/ResearchKit?src=hash">#ResearchKit</a></p>&mdash; John Wilbanks (@wilbanks) <a href="https://twitter.com/wilbanks/status/575125345977810945">March 10, 2015</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Che si vendano più o meno iPhone o iPad grazie a ResearchKit conta certamente per i contabili di Apple, eppure passa clamorosamente in secondo piano. Contano di più nicchie importanti servite, ora, molto meglio di prima.