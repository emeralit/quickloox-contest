---
title: "La folle calma"
date: 2013-07-24
comments: true
tags: [Apple]
---
Apple ha ottenuto [risultati finanziari](http://www.apple.com/it/pr/library/2013/07/23Apple-Reports-Third-Quarter-Results.html) perfettamente in linea con le proprie previsioni: poca crescita complessiva e diminuzione dei profitti.<!--more-->

Niente di entusiasmante insomma. Però i dati hanno superato di poco quello che si aspettavano gli analisti e così le azioni in Borsa sono salite.

Avrebbero dovuto salire perché, per dire, nel trimestre dello scorso anno era appena uscito un ipad nuovo e stavolta no, e lo stesso vale per esempio per i Mac; oppure perché iTunes Store gira a tutto vapore, oppure – e soprattutto – perché Apple non è agitata né in mezzo a una crisi: lavora ai nuovi prodotti e ha già mostrato di prendersi spazio e tempo di manovra quando servono.

Il cielo non voglia che battano le previsioni al prossimo giro, pena un crollo del titolo.