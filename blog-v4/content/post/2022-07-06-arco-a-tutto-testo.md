---
title: "Arco a tutto testo"
date: 2022-07-06T00:48:34+01:00
draft: false
toc: false
comments: true
categories: [Web, Internet]
tags: [Kickstarter, 50 Years of Text Games From Oregon Trail to A.I. Dungeon, 50 Years of Text Games]
---
Mentre scrivo mancano quarantatré ore alla conclusione del lancio su Kickstarter di [50 Years of Text Games: From Oregon Trail to A.I. Dungeon](https://www.kickstarter.com/projects/aaronareed/50-years-of-text-games). Sottotitolo: *Un libro definitivo sul primo mezzo secolo di fiction interattiva*.

Non è una chiamata alla mobilitazione per salvare un progetto: l’autore chiedeva quasi ventottomila euro e ad adesso ne ha ottenuti più di quattrocentotremila. È uno di quei Kickstarter che chiamo *animati* perché la cifra raccolta aumenta mentre guardo la pagina.

Nessuno ha bisogno di sostenere il progetto, ma qualcuno potrebbe avere voglia di possedere il libro, che verrà pronto attorno a inizio del prossimo anno. Cinquanta anni di giochi testuali interattivi, un gioco per ogni anno, dissezionato da cima a fondo. Un arco narrativo di cinquant’anni è l’arco della vita attiva di una persona e trovo suggestiva la sovrapposizione possibile delle due narrazioni.

Attenzione a chi ordina la copia fisica, perché le spese di spedizione sono calcolate a parte e sono alte rispetto al costo del libro. La copia digitale ovviamente non soffre di questo problema.

In un momento nel quale [è uscito un gioco testuale interattivo persino per watch](https://macintelligence.org/posts/2022-05-13-una-locazione-per-volta/), ripercorrere la storia del genere seguendo una ricerca approfondita è un suggello al valore di tutti quei valorosi che hanno progettato, programmato, testato e anche dato testate per risolvere tanti giochi splendidi, da [Colossal Cave](https://macintelligence.org/posts/2019-12-02-breve-storia-della-stupidità-computazionale/) in avanti.