---
title: "Fatti, non foste per essere ignorati"
date: 2016-10-31
comments: true
tags: [Surface, Studio, Book, MacBook, Pro, Nvidia, Gizmodo, Apple, Microsoft, Intel, Skylake]
---
Ho perso il conto delle reazioni, da Facebook in su, alle presentazioni dei nuovi MacBook Pro e del Surface Studio di Microsoft.<!--more-->

Le posso comunque riassumere così: Microsoft ha pensato ai professionisti e Apple invece li ha delusi.

Poi leggo Gizmodo e il suo articolo: [Perché Apple e Microsoft usano processori così vecchi nei loro nuovi computer?](http://gizmodo.com/why-are-apple-and-microsoft-using-such-old-processors-i-1788302547)

I MacBook Pro hanno processori con architettura Intel Skylake, vecchia di un anno. Ma anche Surface Studio e Surface Book.

>Ma Studio e Surface Book stanno anche usando schede video molto più vecchie, della serie Nvidia 900, rispetto alla serie 1000 fatta di chip molto più veloci e meno affamati di energia.

Avessi letto un professionista, uno solo, che nel lamentarsi dei MacBook Pro e nell’entusiasmarsi per Surface Studio avesse fatto notare la cosa.

Ma a quanto pare si tratta di persone incapaci di giudicare i propri strumenti di lavoro oltre la superficie e gli annunci da vetrina, ignorando certa concretezza che invece dovrebbe influire più di tutto nelle loro valutazioni.

P.S.: ho cancellato il mio [bando personale](https://macintelligence.org/posts/2016-06-13-sulla-buona-strada/) di Gizmodo dalle mie letture abituali, visto che il farabutto senza scrupoli che lo ha fondato è finalmente fuori dai giochi e lo ha dovuto vendere.