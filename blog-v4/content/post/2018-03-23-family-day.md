---
title: "Family Day"
date: 2018-03-23
comments: true
tags: [Pampers, Twitter]
---
Entri nel sito [Pampers](https://www.pampers.it/) e decidi di autenticarti con l’account Twitter.

 ![Autenticazione via Twitter fallita sul sito Pampers](/images/pampers.png  "Autenticazione via Twitter fallita sul sito Pampers") 

Il messaggio di errore spiega che sono rimasti alla vecchia versione delle interfacce di collegamento con Twitter e devono (ancora) aggiornare.

Come faccio a spiegare a Pampers che sono lì per la seconda figlia?