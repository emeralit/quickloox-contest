---
title: "Carta conta"
date: 2016-10-05
comments: true
tags: [Pathfinder, Runelords, Waterdeep, D&D, Dungeons, Dragons, iOS]
---
Avevo già [accennato](https://macintelligence.org/posts/2014-02-24-varie-da-viaggio/) all’esistenza di *Lords of Waterdeep*, trovandolo straordinario per giocabilità ed efficacia della [versione iOS](https://itunes.apple.com/it/app/d-d-lords-of-waterdeep/id648019675?mt=8), che costa oltretutto 6,99 euro contro i 49,99 dollari della [scatola](http://dnd.wizards.com/products/tabletop-games/board-games/lords-waterdeep) per giocare al tavolo.

*Waterdeep* è un classico gioco da tavolo che potrebbe avere qualsiasi ambientazione o anche non averla. Ha una ambientazione *fantasy* perché è stato concepito dai creatori del gioco di ruolo [Dungeons & Dragons](http://dnd.wizards.com). E vengo al punto: i giochi di ruolo sono una faccenda maledettamente difficile da progettare. Servono bilanciamento, varietà, completezza, espandibilità. Quando i creatori di un gioco di ruolo si cimentano in un gioco da tavolo, sembrano particolarmente bravi nel mantenere queste caratteristiche. Forse perché il gioco da tavolo è vincolato a confini ben precisi e questo, che manda in crisi il progettista improvvisato, per loro – che devono rendere possibile la creazione di universi di fantasia – è quasi una mano santa.

Ora mi hanno fatto scoprire [Pathfinder Adventures](https://itunes.apple.com/it/app/pathfinder-adventures/id966333863?mt=8), che discende da [Rise of the Runelords](http://paizo.com/pathfinder/adventurePath/riseOfTheRunelords), originalmente una campagna (come un serial televisivo, ogni puntata uno scenario) di un gioco di ruolo concorrente di D&D, Pathfinder.

Il gioco di ruolo è stato trasformato in gioco di carte – ci sono carte per personaggi, mostri, oggetti magici, eventi, situazioni, e regole per giocare queste carte in modo da raggiungere un certo obiettivo finale – e il gioco di carte è stato trasformato in *app free-to-play* per iOS.

Ecco: ti risucchia e tendi a giocare senza prestare troppe attenzioni al lungo *tutorial*. E finisce che d’improvviso il tuo giocare superficiale non è più sufficiente a sconfiggere i cattivi e raggiungere l’obiettivo.

I creatori di un gioco di ruolo non solo sono diabolicamente abili a ottenere bilanciamento, equilibrio e varietà. Sanno anche farlo con architetture di gioco complesse.

In altre parole, *Pathfinder Adventures* è tutt’altro che un *casual game*. Bisogna avere capito bene le regole, studiare bene le carte, stare attenti all’equipaggiamento dei personaggi, concepire una strategia, applicarla con flessibilità e concentrazione.

Se piace il gioco di ruolo, se piace l’ambientazione *fantasy* e se ci si trova a dedicare qualche momento della giornata al gioco di qualità, *Pathfinder Adventures* è una bella sfida. La differenza con *Lords of Waterdeep* è che quest’ultimo è un *boardgame* classico: ogni partita è storia a sé. *Rise of the Runelords* su *Pathfinder Adventures* è un lungo cammino, da provare sicuramente con curiosità e poi intraprendere consci che vedere il gran finale costerà tempo e impegno.

<blockquote class="twitter-tweet" data-lang="en"><p lang="und" dir="ltr"></p>&mdash; Lucio Bragagnolo (@loox) <a href="https://twitter.com/loox/status/783448180692283392">October 4, 2016</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>