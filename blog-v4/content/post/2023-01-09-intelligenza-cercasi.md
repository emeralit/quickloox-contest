---
title: "Intelligenza cercasi"
date: 2023-01-09T00:38:32+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [ChatGPT, Marcus, Gary Marcus, Hofstadter, Douglas Hofstadter, Fluid Concepts and Creative Analogies]
---
Nell’[articolo commentato ieri](https://macintelligence.org/posts/2023-01-08-caccia-allerrore/) a proposito della struttura e dei limiti di meccanismi come ChatGPT, l’autore propone una definizione minima (scritta da altri) di intelligenza: di che cosa dovrebbe essere consapevole un sistema con il diritto di definirsi intelligenza artificiale.

La definizione segue qui sotto. Ciascuno può fare i dovuti confronti rispetto a ChatGPT, un bambino di prima elementare, uno scimpanzé, un foglio elettronico eccetera.

§§§

Ci sono cose, che hanno proprietà.

Secondo le proprietà che possiedono, esistono tipi diversi di cose. Alcune, come i numeri o le opinioni, sono immateriali. Altre, come gufi, frigoriferi e laghi sono fisiche e dunque dispongono di proprietà come dimensione o posizione.

Alcune delle proprietà delle cose sono semplicemente qualità, come essere vivo oppure tenersi tutti i giovedì. Altre proprietà riguardano le relazioni tra cose e altre cose, come essere nato in una certa città o essere la somma di due numeri. Alcune proprietà sono funzionano in modo relativo, come l’essere pesante oppure lieto di qualcosa.

I punti nel tempo sono cose non fisiche ordinate in fila indiana, quindi aventi la proprietà di trovarsi prima o dopo altri punti nel tempo. Quando parliamo di cose che esistono o delle proprietà che hanno, questo avviene sempre relativamente a un punto nel tempo. Una cosa può non esistere in un punto, esistere in un punto successivo e cessare di farlo in un punto ancora più in là. Similmente, una cosa può possedere una proprietà in un dato istante e esserne sprovvista in un altro.

Il verificarsi degli eventi è una cosa non fisica che ha la responsabilità dei cambiamenti. Ciò che esiste e le proprietà che possiede si considerano stabili nel tempo, a meno che nel tempo si verifichi un evento che provoca cambiamenti. Alcuni eventi si verificano a causa di agenti. Altri, come risultato di eventi precedenti. Altri ancora accadono spontaneamente.

Alcuni eventi si verificano solo in un singolo punto nel tempo e risultano in un cambiamento immediato. Altri eventi, che chiamiamo processi, hanno luogo lungo un certo numero di punti nel tempo e producono un cambiamento graduale. Nello stesso punto del tempo possono avere luogo più eventi. Un processo chiamato passaggio del tempo si verifica in continuazione e cambia gradualmente tutte le cose fisiche.

§§§

Qualcosa in più di *scrivi una poesia in stile steampunk*, insomma.

Douglas Hofstadter ha pubblicato quasi trent’anni fa un libro molto meno noto di quello che conoscono tutti, [Fluid Concepts and Creative Analogies](https://s3.amazonaws.com/arena-attachments/669097/a6e33859f5f6677f20615f14fdbf52fa.pdf). Il link porta a una versione PDF del libro (che penso e spero legale, ma possiedo orgogliosamente una copia cartacea, quindi non temo di violare il *fair use*, senza contare che si trova almeno in un altro post). Chi avesse tempo e voglia potrebbe guardare agli esperimenti compiuti nel tentativo di accendere una minuscola scintilla di intelligenza nel software. Riderà di fronte alle dimensioni minuscole degli esperimenti; potrebbe provare a proporle a ChatGPT, giusto per valutare se sia più intelligente o più artificiale, alla prova con cose del secolo scorso eseguite su computer che, rispetto ai cloud di oggi, sono da operetta.