---
title: "Un altro manifesto"
date: 2018-01-26
comments: true
tags: [popup, pubblicità, blank]
---
Un amico mi ha suggerito di mettere su queste pagine link con `target="_blank"` in modo che si aprano in un’altra finestra, per questioni di ottimizzazione del posizionamento sui motori di ricerca e della raccolta di traffico.<!--more-->

La mia risposta è stata, di getto, su Facebook Messenger e quindi sulla piattaforma peggiore possibile per scrivere, la seguente.

*No, no, sono rispettoso del mio lettore, del suo tasto Back, della sua capacità di aprire un link in una nuova finestra se ritiene, del  suo screen real estate, della sua eventuale voglia di navigare tranquillo, della sua possibilità di ritornare quando vuole, della sua libertà di dimenticarmi senza essere obbligato a chiudere una finestra che non gli interessa più.*

Tutto sommato non cambierei una virgola. Certo questo blog non ha lo scopo di raccogliere denaro. Se e quando lo avesse, tuttavia, non cercherebbe di farlo gonfiando il traffico. O pensando di gonfiarlo, perché la “mia” finestra non lo è: è quella di chi legge, che per fortuna può fare quello che vuole. Nessuno mi leggerà un minuto di più se uso trucchi per tenere aperta la mia pagina nel suo browser.

O meglio, le statistiche diranno che è accaduto e sarà ancora peggio: sarò convinto che qualcuno legga. Non ha senso.

Se e mai qui ci sarà della pubblicità sarà elegante, pertinente, discreta e informativa. Basta volerlo fare.

In generale, tutti i mali del web derivano esattamente dalla volontà di spremere il limone pensando a tutto tranne alle persone che stanno davanti allo schermo.

(Per completezza, un primo [manifesto](https://macintelligence.org/posts/2017-12-22-un-manifesto/) c’era già e parla di tutt’altro).

Sarebbe meglio il contrario e niente migliorerà se non lo facciamo notare, nelle scelte personali e nei feedback. Il web algoritmico fine a se stesso ha un sacco di buone ragioni, ma non servire le persone.