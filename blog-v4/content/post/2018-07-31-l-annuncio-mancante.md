---
title: "L’annuncio mancante"
date: 2018-07-31
comments: true
tags: [Mac, Wwdc, Apple, pay, Paypal, iPhone]
---
Apple ha appena messo agli archivi [il miglior trimestre primaverile di sempre](https://www.apple.com/newsroom/2018/07/apple-reports-third-quarter-results/) e naturalmente è spacciata perché, anno su anno, le vendite di Mac sono diminuite in unità del tredici percento.

Il trimestre che finisce in giugno è tradizionalmente quello dove si concentrano gli acquisti delle scuole, che vanno su Mac solo in minima parte. Più di questo, l’anno scorso durante Wwdc vennero annunciati nuovi MacBook Pro; quest’anno, nessuno.

Insomma, il dato sembra largamente spiegabile senza tirare in ballo le perdite di qualità e i problemi con le tastiere.

Ci sono altri punti di interesse nella relazione trimestrale (per esempio: iPhone X è il modello più venduto; pay fa più transazioni *mobile* di PayPal) e invito i più pazienti a dare un’occhiata al [transcript della anch’essa tradizionale audioconferenza](https://seekingalpha.com/article/4192790-apple-aapl-q3-2018-results-earnings-call-transcript?part=single) tenuta da Apple con gli analisti.