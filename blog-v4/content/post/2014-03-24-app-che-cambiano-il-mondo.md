---
title: "App che cambiano il mondo"
date: 2014-03-25
comments: true
tags: [FireChat, AirDrop, Multipeer]
---
Mentre scrivo, *Macity* mette in *pole position* sulla sua pagina l’importantissimo avvento di [Punizione Pazzo](https://itunes.apple.com/it/app/crazy-freekick/id816202468?l=en&mt=8), *il gioco di calcio e logica che spopola gratis in App Store*.<!--more-->

Niente contro il calcio e ampie simpatie per la logica. Preferisco comunque segnalare [FireChat](https://itunes.apple.com/it/app/firechat/id719829352?l=en&mt=8), *app* di messaggistica che ha qualcosa di assolutamente particolare e con un milione di concorrenti dentro App Store non è cosa facile da scrivere: diffonde connessione Internet via *peer-to-peer*.

Esempio facile facile: sala riunioni sotterranea e non c’è campo, né una base Wi-Fi. Ipotizzando che ci siano abbastanza iPhone a usare FireChat, i telefoni si agganciano tra loro e l’iPhone più vicino all’esterno si aggancia con un iPhone fuori dalla sala, dove c’è campo e un *hotspot*, oppure c’è Wi-Fi. Tutti gli iPhone, aiutandosi l’uno con l’altro, a questo punto riescono ad arrivare su Internet. Anche quelli nel punto più isolato della sala riunioni.

Che FireChat prenda piede o meno è un problema relativo: Apple ha inserito in iOS 7 una tecnologia detta [Multipeer Connectivity Framework](https://developer.apple.com/library/ios/documentation/MultipeerConnectivity/Reference/MultipeerConnectivityFramework/Introduction/Introduction.html), che tutte le *app* possono utilizzare e consente esattamente quanto descritto. FireChat è solo il primo esempio.

AirDrop funziona secondo lo stesso principio, solo che consente unicamente lo scambio di file e non il prolungamento di una connessione Internet. È la stessa situazione in cui si trova Android (neanche a dirlo, una *app* innovativa appare su iOS).

Se a Macity la trovano notizia degna di nota, possono copiare e incollare da [Cult of Mac](http://www.cultofmac.com/271225/appreciated-ios-7-feature-will-change-world/). Anche se capisco quanto Punizione Pazzo sia fondamentale.