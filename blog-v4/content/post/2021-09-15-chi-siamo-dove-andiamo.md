---
title: "Chi siamo, dove andiamo"
date: 2021-09-15T00:13:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, Steve Jobs, iPad, iPad mini, iPhone 13, iPhone, Apple Watch, Apple TV+] 
---
Sembra ieri e invece era il 2007 quando [Steve Jobs annunciò il cambio della ragione sociale](https://www.macworld.com/article/183057/applename.html), da Apple Computer a Apple.

La cosa fece gran rumore. Avrebbe potuto farne molto di più, se non fosse stato che Jobs si esibì in uno dei suoi più formidabili campi di distorsione della realtà:

>Mac, iPod, TV e iPhone. Solo uno di questi è un computer. Quindi cambiamo il nome.

Se avesse detto la verità, avrebbe suscitato un clamore superiore, ma forse Apple ne avrebbe risentito in modo anche ragguardevole. Perché *erano tutti computer*.

Jobs non poteva ammetterlo, perché tantissimi non avrebbero mai comprato un computer da tasca al posto di un telefono, mentre invece si sarebbero messi in fila per un telefono dotato di nuove possibilità.

E sapeva benissimo che ammetterlo sarebbe stato inutile, per un motivo: tempo qualche anno, *a nessuno sarebbe più interessato alcunché* di che cosa fossero quegli aggeggi in vendita con la mela sopra.

Eccoci al 2021. Apple distribuisce [un aggiornamento di sicurezza straordinario](https://threatpost.com/apple-emergency-fix-nso-zero-click-zero-day/169416/), per chiudere la porta a un attacco piuttosto grave a cui è vulnerabile l’intera linea di prodotti (visto, che sono computer?).

Quando si parlava di computer, in un caso come questo fioccavano gli articoli divulgativi, le interviste, i disassemblaggi del malware da spiegare all’uomo della strada, cronistorie sulla sicurezza informatica, polemiche.

Oggi è rumore di fondo. Chi ci tiene aggiorna al volo, chi non ne sa aggiornerà quando glielo diranno i device, non succederà niente di drammatico.

Intanto Apple manda in onda uno [show in diretta planetaria](https://www.apple.com/apple-events/september-2021/) dedicato ai nuovi annunci di prodotto.

Invece di un amministratore delegato su un palco, abbiamo un maestro di cerimonia che dipana un filo rosso tra musicisti, illustratori, medici di pronto soccorso, rider, studenti, sportivi, uomini e donne della strada, cantanti, arrampicatori, ginecologi, trainer, videomaker, attori, allenatori, surfisti, impiegati e chissà quanti ne dimentico. L’umanità rappresentata mentre viene liberata di volta in volta grazie ad watch, iPhone, iPad (classico e mini), naturalmente iPhone 13 normale e Pro, tv+.

La tecnologia c’è e di eccellenza, ma sta immersa tra una serie TV e un trekking sulle colline, meditazioni in palestra e montaggi video, scogliere e quartieri urbani, maggioranze e minoranze, un caleidoscopio creato con dispendio immenso di mezzi e capacità.

Ci siamo arrivati. A nessuno interessa più la tecnologia in quanto tale, perché Jobs ha vinto, anzi, stravinto e il computer, da oggetto con cui interfacciarsi, è diventato oggetto della nostra vita, pervasivo come i rubinetti dell’acqua o le sedie o i pigiami.

L’amplificatore di intelligenza originale oggi svolge la stessa funzione per la salute, la forma fisica, la preparazione scolastica, le imcombenze d’ufficio, gli hobby e le aspirazioni, i rapporti umani e quelli da stampare per il capo, senza vincoli di distanza, difficoltà, peculiarità.

Il conglomerato Apple è un’impresa nell’impresa, impegnato nella sfida di trasformare in valore qualunque aspetto della vita quotidiana. La metafora della scrivania si è espansa all’universo conosciuto, almeno quello abitato.

Il computer, anche se nessuno più lo riconosce, è diventato l’ausilio prezioso che avrebbe già voluto essere dagli anni ottanta, che ci aiuta a definire chi siamo, che cosa vogliamo, dove andiamo, e anche a tradurre tutto questo in realtà.

La questione su quanto duri la batteria di watch o se iPad mini sia meglio con USB-C o Lightning, che una volta avrebbe appassionato intere mailing list, è risolta dall’esistenza stessa dell’oggetto, che si autolegittima.

Fu detto *cambiare il mondo una persona alla volta*. È questo. Psichedelico, rutilante, sopra le righe, sempre al limite della spacconata o del kitsch, sempre evitati con un cambio di scena che trasforma ogni sequenza in una celebrazione olistica con reminiscenze hippy della vita, delle persone, del talento di ciascuno.

Celebrazione interessata, non c’è dubbio, non per questo meno vera. Apple manda in scena il più grande spettacolo del mondo, dedicato allo spettacolo del mondo.

Chi siamo e dove andiamo dipende da noi, ma anche da tutti noi, e anche da loro, i cui droni hanno tributato un omaggio vitale e quasi selvaggio alla California, dove non è cominciato tutto, ma si è girato un angolo che ha cambiato la storia dell’umanità.

Non sappiamo di preciso quando arrivano gli watch Serie 7, ma viene voglia di svegliarsi domani e mettersi in gioco su qualcosa di immensamente sfidante, armati di tecnologia sottile, leggera, fidata, a volte imprevedibile e incostante, sempre in sviluppo, come vorremmo potesse sempre essere l’oggi.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*