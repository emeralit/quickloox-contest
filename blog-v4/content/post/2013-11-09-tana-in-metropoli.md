---
title: "Tana in metropoli"
date: 2013-11-09
comments: true
tags: [Mac]
---
Windows al servizio dei passeggeri in un pomeriggio milanese. Un Mac almeno mostrerebbe l’icona di una cartella con un punto interrogativo sopra.

 ![Pannello Windows guasto in metró](/images/metro.jpg  "Pannello Windows guasto in metró") 