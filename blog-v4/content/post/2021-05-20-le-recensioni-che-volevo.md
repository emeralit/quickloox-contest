---
title: "Le recensioni che volevo"
date: 2021-05-20T00:34:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [M1, iPad Pro, Federico Viticci, MacStories, John Gruber, Daring Fireball, iMac] 
---
Chiaro che se vuoi farti l’idea di come sia iPad Pro M1, vai da [MacStories con Federico Viticci](https://www.macstories.net/stories/ipad-pro-2021-review/); ovvio che se hai una curiosità riguardo agli iMac M1, consulti [John Gruber su Daring Fireball](https://daringfireball.net/2021/05/the_24_inch_m1_imac); è una cosa scontata.

Ma stavolta – specie Viticci – si sono superati. La recensione di iPad Pro 2021 è un capolavoro, no, nemmeno; è la recensione di iPad Pro come avrei sempre voluto leggerne una. Neanche il [megathread su Reddit](https://www.reddit.com/r/apple/comments/nfa5w4/megathread_apples_m1_imac_reviews_first/) regge il confronto. Un esempio di come si fa l’informazione seria su Internet. Lui di recensioni straordinarie ne ha scritte più di una. Questa è un traguardo.

Me la sono letta mettendo da parte qualunque altra priorità e pure con gran gusto. Non è solo sapere che lo schermo di iPad Pro da solo vale l’acquisto, ma è un esempio di come fare giornalismo tecnico indipendente ad altissimo livello. L’ultima volta che ho visto una cosa del genere era su *Byte* di carta, nella [Chaos Manor](https://www.jerrypournelle.com) del compianto Jerry Pournelle.

Una delle poche occasioni in cui si parte per aggiornarsi e si finisce per imparare.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               