---
title: "Un dubbio si fa strada"
date: 2018-09-02
comments: true
tags: [Google]
---
Godo di un breve passaggio su una lussuosa Lexus munita di ampio schermo a lato cruscotto, con regolamentare mappa del percorso. Il conducente la ignora, nel preferire il suo Samsung Qualcosa appoggiato in orizzontale allo schermo stesso, su cui consulta la mappa di Google.

*Perché questi costruttori di automobili non licenziano il loro spazio schermo a Google o a chi per lei? Tanto oramai devono montare un sistema operativo, tanto vale farlo fare a chi lo sa fare!* osserva il conducente.

Rispondo *non vogliono cedere un territorio che si preannuncia strategico. Un domani chi fornisse il sistema operativo a un’auto potrebbe condizionarne lo sviluppo e l’ingenerizzazione. Poi, che succederebbe se chi fornisse il sistema operativo facesse lo stesso con il sistema di guida autonoma? Il costruttore diventerebbe un semplice produttore di hardware, quasi o del tutto indifferenziato rispetto alla concorrenza, e perderebbe profitti potenziali.*

*È successo in informatica con Wintel*, proseguo. *I PC erano visti come essenzialmente intercambiabili e competevano quasi solo sul prezzo. In più il sistema operativo per tutti poteva essere solo limitatamente ottimizzato; chi salirebbe su una macchina a guida autonoma dotata di un sistema operativo creato non per quella macchina, ma per tutte?*

Concludo e mi rendo conto della mia ignoranza di fondo: in verità non so perché Lexus non commissioni a Google una versione su misura di Android. Parlo per supposizioni. Qualcuno lo sa?

Penso anche a quanto siamo stati fortunati ad avere miliardi di computer in giro per il mondo equipaggiati con un sistema operativo approssimato un tanto al chilo, per girare ovunque e valorizzare nessuno, senza che accadesse alcunché di veramente grave: una centrale nucleare che impazzisce, un acquedotto che si ferma, una portaerei che lancia un missile intercontinentale per errore. Del sistema operativo, intendo.

Oggi però che ci si accinge a mettere in mano infinite decisioni salvavita da prendere in millesimi di secondo a hardware lanciato a cento chilometri l’ora, non so se mi andrebbe bene un sistema operativo generico a bordo.
