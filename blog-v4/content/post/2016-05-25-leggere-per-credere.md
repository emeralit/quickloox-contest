---
title: "Leggere per credere"
date: 2016-05-25
comments: true
tags: [Kerning, design, Nasa, ProPublica, tipografia]
---
La Nasa ha un elenco di consigli di *design* tipografico per i testi di cui dispongono i piloti in cabina. Una spiegazione semplice ed efficace del perché scrivere a tutte maiuscole inficia la leggibilità di un testo. Quanto serve poco a fare la differenza in un cartello stradale e salvare delle vite.

Questo e altro in un bell’[articolo di ProPublica](https://www.propublica.org/article/how-typography-can-save-your-life). I rudimenti della tipografia andrebbero insegnati a scuola.

Chi può, a proposito, non manchi la visita a [Kerning](http://2016.kerning.it). Manca pochissimo.