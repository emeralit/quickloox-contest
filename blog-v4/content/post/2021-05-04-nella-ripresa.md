---
title: "Nella ripresa"
date: 2021-05-04T00:43:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [OBS Studio, OBS Camera, Fëarandil] 
---
Il primo tempo della mia partita con il software per usare iPhone come webcam di Mac è durato poco.

Istigato da **Fëarandil** che su queste cose ne sa a montagne, ho abbandonato l’approccio *poco maledetto e subito* e ho cestinato EpocCam, [adottato pochi giorni fa](https://macintelligence.org/posts/Da-telefono-a-webcam.html).

Nel secondo tempo schiero invece uno squadrone, nelle sembianze di [OBS Studio](https://obsproject.com) su Mac e [OBS Camera](https://obs.camera) per iPhone.

Il tutto mi costa nove euro in più (ho chiesto il rimborso degli 8,99 ero di EpocCam e corrisposto 17,99 euro per OBS Camera) ma in cambio, invece che uno hack per avere la webcam virtuale su Mac, ho uno hack con intorno un sistema pazzesco per mettere a punto conferenze, presentazioni, trasmissioni dove il limite è dato solo dall’esperienza.

OBS Studio (che peraltro Fëarandil mi aveva già segnalato tempo addietro) è open source, multipiattaforma, con una quantità siderale di plugin per fare veramente la qualunque, fino alla manipolazione 3D delle finestre inserite nelle *scene* che sono l’unità di lavoro principale.

Devo ancora impratichirmi di brutto perché le possibilità a disposizione sono appunto infinite e non ho titoli per spiegare a puntino che cosa farci; rozzamente, inizio a dire che con OBS Studio si assemblano, appunto, scene contenenti quello che decidiamo di metterci: feed video, pagine web, schermate da applicazioni, audio, di tutto. Dopo di che si può andare in streaming con quella che è di fatto la regia di uno show virtuale, con la possibilità di registrare e fare una milionata di altre cose.

Sempre il losco figuro già menzionato mi ha accennato a [BlackHole](https://github.com/ExistentialAudio/BlackHole), un driver per audio virtuale, e [OBS Ninja](https://obs.ninja) con cui si potrebbe benissimo mettere in piedi da soli un talk show fatto e finito, che aggiungono ulteriore sostanza al tutto.

Non penso che arriverò mai oltre il dieci percento di quello che OBS Studio può fare; arrivarci sarebbe un super salto di qualità. Per esempio, mi scervellavo su come inserire in modo elegante [Web Captioner](https://webcaptioner.com) in certe presentazioni dove serve accessibilità. OBS Studio è una risposta precisa e micidiale nella sua efficacia.

Tutto software libero, poi, trasparente, aggiornato, documentato. Abbiamo anche problemi, chi lo nega, ma questa è un’epoca con aspetti prodigiosi.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*