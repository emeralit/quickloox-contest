---
title: "A spade sguainate"
date: 2021-11-10T01:42:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [The Manual of Mathematical Magic, Backblaze] 
---
Le cronache mostrano impietose un livello di analfabetismo numerico cui sta dietro solo l’ignoranza dell’italiano.

Impegnarsi verso l’esterno è difficile. Gli analfabeti più esposti e più probabilmente visibili nelle nostre bolle relazionali hanno in realtà problemi psicologici e insegnare loro qualcosa non si può; al massimo sono (molto cautamente) accompagnabili per mano in un percorso di recupero difficile, lungo e a fasi alterne.

Dove non dobbiamo sbagliare è su noi stessi. È facile cedere a certe sirene se non abbiamo una tranquilla e solida nozione di che cosa significano numeri e dati. Il calcolo delle probabilità è uno dei bastioni più critici, dove si consumano le battaglie più frustranti e sanguinose.

Facciamoci dei favori personali. Il primo: teniamo d’occhio BackBlaze che ogni trimestre scodella le [statistiche di affidabilità del suo ingente parco di dischi rigidi](https://www.backblaze.com/blog/backblaze-drive-stats-for-q3-2021/). Leggiamolo e comprendiamolo. Rendiamoci conto che il disco davanti a noi è una goccia nel mare e dal comportamento di una goccia è in verità difficile prevedere come saranno le onde.

Il secondo: scarichiamo e leggiamo [The Manual of Mathematical Magic](http://www.mathematicalmagic.com), che insegna in modo semplice e giocoso un sacco di probabilità di base, assai più utili nella vita di quanto possiamo immaginare.

Le visualizzazioni di dati sui giornali e dai media fanno mediamente schifo. Attorno a esse si scatenano risse verbali inaudite mentre, intanto, nessuno si ferma a comprendere come nove volte su nove siano grafici incompleti, imprecisi, sbagliati, confusi e brutti.

Impariamo a difenderci dai cattivi grafici. Leggiamo ma soprattutto meditiamo, è l’attenzione l’antidoto migliore agli abusi visivi dei dati. Leggiamo articoli come [i 27 peggiori grafici di tutti i tempi](https://www.businessinsider.com/the-27-worst-charts-of-all-time-2013-6).

Non pretendo che si prendano lezioni di [linguaggio R](https://www.r-project.org), ma almeno fare attenzione a quello che produciamo dal nostro foglio di calcolo.

Scrisse Chesterton che si sarebbero sguainate spade per mostrare che due più due fa quattro. Si trattava di una metafora naturalmente, nessun sangue verrà versato attorno a un grafico stupido. Il momento, tuttavia, è arrivato. Sguainiamo attenzione, esperienza, familiarità con l’argomento.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*