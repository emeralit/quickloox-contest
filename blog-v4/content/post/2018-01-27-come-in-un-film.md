---
title: "Come in un film"
date: 2018-01-27
comments: true
tags: [Gimpirea, Fcp, Hollywood]
---
>Un sabato mattina a Los Angeles una troupe di dieci studenti della Hollywood High School, guidati dalla regista diciassettenne Celine Gimpirea, sta trasformando un angolo del cimitero Calvary in un set cinematografico. In [The Box](https://youtu.be/7WX-0D09sVM), un ragazzo entra in uno scatolone e si ritrova trasportato in un posto diverso. Sul prato ben curato, di un verde che sembra impossibile, tra le file di lapidi di granito nero, si trovano file di contenitori neri con dentro stazioni di lavoro per tecnici dell’immagine, iPad e MacBook Pro: gli strumenti che porteranno alla vita la vicenda.

È l’inizio di un articolo della Newsroom di Apple, [Filmmaker losangelini emergenti creano cortometraggi con budget ridotti e grandi idee](https://www.apple.com/newsroom/2018/01/emerging-la-filmmakers-create-short-films-with-small-budgets-and-big-ideas/).<!--more-->

Chiaramente è una partita giocata in casa. Apple ha fornito i materiali e l’estensore del pezzo ha gioco facile nel magnificare i risultati. Eppure bisogna guardare le foto, i filmati, possibilmente seguire il racconto, per capire se e quanto computer, portatili e tavolette con la Mela sopra siano adeguati per reggere il peso di una produzione cinematografica.

E non inganni il fatto anagrafico. Quando a girare corti sono liceali di Hollywood, le aspettative – loro, della scuola, di chi fornisce i materiali – sono ben diverse da quelle di un liceo, che so, di [French Lick, Indiana](https://en.wikipedia.org/wiki/French_Lick,_Indiana).