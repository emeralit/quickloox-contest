---
title: "La discrezione è tutto"
date: 2015-08-26
comments: true
tags: [watch, iPhone, Siri]
---
Sto adottando watch con gradualità: per dire, in casa lo tolgo. Così facendo, per esempio, vanifico il lavoro storico dell’apparato di rilevazione attività che ti ricorda di alzarti per un minuto se sei seduto da un’ora. Se mi dessi alla *cyclette* in questa situazione, *lui* non registrerebbe l’attività compiuta. watch si aspetta di essere – e dà il meglio se viene – indossato per tutta la giornata, sonno escluso quando anche per lui è ora di ricarica. Per ora evito e va bene così.<!--more-->

Nel tempo trascorso all’esterno invece mi rendo conto che, se a volte le notifiche in arrivo da iPhone risultano fastidiose o eccessive, su un watch non personalizzato sono discrete, non invasive, leggere, facilissime da trattare se contano o ignorare se no.

Ho provato a chiamare un cellulare in macchina, svegliando l’assistente vocale con il comando *Ehi Siri* e chiedendo la chiamata. In auto (rumorosa) si riesce a parlare usando l’orologio come vivavoce e senza nessuna distrazione visiva. Contemporaneamente, se arriva una chiamata anche in una situazione di folla, si riesce a gestire senza che tutti ti ascoltino e riuscendo ad ascoltare l’interlocutore. È stato svolto un lavoro direi minuzioso per calibrare a perfezione il volume dell’altoparlante esterno.

Non ho ancora fatto un giro delle *app* installate, però ho guardato l’interfaccia che è proprio gradevole. Lo schermino diventa usabile e mai il dito è andato fuori posto (fermo restando che le *app* si possono anche lanciare a voce).

Problemi, per ora, nessuno. Il mio quadrante preferito, quello astronomico che utilizzo con l’indicazione della posizione relativa dei pianeti, si riavvia mostrando invece la modalità mappamondo (quella iniziale) se spengo e riaccendo watch. Sopravvivo.