---
title: "Volutamente nebuloso"
date: 2014-11-13
comments: true
tags: [design, Apple]
---
Per una volta non entrerò nello specifico. Sarebbe noioso. Faccio solo alcune richieste generiche ai progettisti di Apple (e anche altri, perché no).<!--more-->

Per favore, non lavorate a modi per fare su nuovi apparecchi quello che si è sempre fatto. Lavorate perché nascano modi nuovi di fare cose mai pensate prima.

Per favore, non lavorate ad apparecchi con una funzione. Lavorate ad apparecchi per i quali sia possibile inventare funzioni sorprendenti, che cinque minuti dopo ti fanno dire che certo era ovvio, a pensarci prima. E nessuno ci aveva pensato, prima.

Per favore, nel dubbio, togliete invece che aggiungere.

Grazie.