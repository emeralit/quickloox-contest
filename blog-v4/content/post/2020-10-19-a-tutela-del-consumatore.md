---
title: "A tutela del consumatore"
date: 2020-10-19
comments: true
tags: [Immuni, iOS, iPhone, Mappe, Edge]
---
Siamo stati in gita nei boschi in cerca di castagne. Per arrivarci mi sono fatto guidare da Mappe.

Nella serata ho ripensato a un amico che ha installato [Immuni](https://www.immuni.italia.it) e da quel momento ha visto crollare l’autonomia di iPhone.

Gli ho proposto di verificare se il problema sia veramente Immuni oppure qualche processo impazzito, qualche app con problemi momentanei eccetera. Mi ha risposto che no, siccome l’aumento dei consumi coincide con l’installazione di Immuni, per forza si tratta di quella.

Ho guardato la sezione Batteria delle Impostazioni per guardare che cosa fosse successo a livello di consumi nella giornata di ieri, piuttosto atipica per la mia routine.

 ![Consumo batteria di iPhone X](/images/consumo-batteria.png  "Consumo batteria di iPhone X") 

Come premessa, siamo partiti verso le dieci con batteria al cento percento e siamo tornati a casa alle diciotto con batteria al 62 percento. Mappe ha funzionato per sessanta/settanta minuti. Per il resto l’unità è stata usata pochissimo, di domenica, in giro nei boschi con le figlie, dove in certi punti era un lusso avere una connessione Edge.

Come si vede, Mappe ha fatto la parte del leone. In basso si vede perfino il consumo per i momenti in cui iPhone non trovava la rete e ha alzato i consumi nel tentativo di captare un segnale.

Immuni non è presente nell’elenco, che invece mostra le Notifiche di Esposizione. I consumi dell’architettura Apple/Google su cui si basa Immuni sono percepibili ma assolutamente nella norma per una app che di tanto in tanto si sveglia e opera. Si tenga conto che passare una domenica nei boschi, al giorno d’oggi, ti fa incontrare una marea di gente, per quanto in condizioni sicure.

Non saprei che dire al mio amico, se non magari di provare a reinstallare Immuni. O di verificare gli effettivi consumi delle app. Anche lui però dovrebbe saper dire qualcosa a me; può darsi benissimo che ci sia un bug oscuro che si manifesta solo in determinate condizioni hardware e software. L’applicazione non può essere inaffidabile come lui sostiene, però. Altrimenti, ci vorrebbe un bug miracoloso capace di farla funzionare impeccabilmente sulla mia macchina. Ben più elaborato dell’altro.