---
title: "Naturale e artificiale"
date: 2023-03-28T12:53:03+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [A2, Campanile, Achille Campanile, Filippo Strozzi, Strozzi, Roberto Marin, Marin, podcast, Acqua minerale]
---
Achille Campanile scrisse uno sketch per mettere alla berlina l’ostilità della società-bene del suo tempo verso i figli nati da relazioni giudicate illegittime. Sì intitola [Acqua minerale](http://www.campanile.it/testi/acua_m.htm) e [gioca lungamente](https://youtu.be/eTRgDfjeZbo) sull’equivoco linguistico dei significati di *naturale*.

Con [Filippo e Roberto](https://macintelligence.org/posts/2023-03-08-detto-tutto/) è stato assolutamente naturale ritrovarsi in podcast una volta di più, naturale nel senso più bello della parola. Volendo giocare sulle ambiguità linguistiche, potremmo contrapporre *naturale* a *artificiale*, visto che la puntata era dedicata alle sedicenti intelligenze in questo momento nella loro primavera (tranquilli che arriverà [l’inverno](https://www.techtarget.com/searchenterpriseai/definition/AI-winter)).

Loro sono *podcaster* nati (bisognerebbe avere un neologismo apposta, che so, podmaster) ed è un piacere tenere i loro ritmi e alimentare la conversazione che viene intavolata.

Sono riuscito a evitare il pippone che avevo sulla punta della lingua – prima o poi arriverà lo stesso, eh – e mi sono anche goduto il microfono comprato non dico apposta ma quasi.

E la soddisfazione di avere coperto tanta discussione su quello che i sistemi generativi rappresentano per l’ecosistema Apple, dove il *machine learning* ha per ora mantenuto il proprio nome invece di spacciarsi per intelligenza. Si poteva anche andare avanti per ore perché il tema è sconfinato, ma le cose importanti sono passate. Probabilmente siamo riusciti a passare qualche dritta anche ai più preparati tra chi ha seguito la diretta YouTube.

Rispetto all’intelligenza, leggo e porterò contributi. Rispetto al podcast e al piacere di comporlo ospite di Filippo e Roberto, mi piace che sia così naturale ritrovarcisi e contribuire. Sono proprio bravi e un livello così è fuori portata di qualsiasi meccanismo artificiale, per quanto in voga.

<iframe width="560" height="315" src="https://www.youtube.com/embed/eTRgDfjeZbo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>