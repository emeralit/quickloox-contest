---
title: "Complessità contro durata"
date: 2013-11-11
comments: true
tags: [iPad, Mac]
---
Grazie a [Mario](https://multifinder.wordpress.com) per la *citazione del giorno*, come mi ha scritto, presa dal [blog di Fraser Speirs](http://speirs.org/blog/2013/11/8/desktop-class.html):

>Siamo andati oltre il dibattito sulla possibilità di usare iPad per la creazione di contenuto. Questa discussione si è chiusa e mese dopo mese chi la prosegue per negare [che iPad possa essere usato per creare contenuto] parla più di sé che di iPad.<!--more-->

Il momento più forte del pezzo è il confronto tra il nuovo iPad Air che si è preso e il vecchio MacBook Air del 2010 che utilizzava. Il divario di prestazioni continua a ridursi:

>Il mio iPad Air è più veloce di quel MacBook Air e ha la stessa capacità. Ha il doppio di batteria di quel portatile e pesa la metà. Costa due terzi del prezzo e integra connessione cellulare.

La mia [presentazione di venerdì scorso](https://macintelligence.org/posts/2013-11-09-il-futuro-e-nella-magia/) è stata preparata metà su iPad e metà su Mac. Sul posto ho portato tutto, per avere massima ridondanza. Però ho usato solo iPad e iPhone come telecomando. Il Mac è rimasto chiuso nello zaino.

E ho un iPad di terza generazione. Immagino come mi sentirei con un iPad Air, se non erro quindici volte più veloce.

Sempre Speirs presenta in [un altro intervento](http://speirs.org/blog/2013/3/4/beyond-consumption-vs-creation.html) un bel grafico sulla ripartizione dei compiti tra telefoni, tavolette e calcolatori. Non è questione di creare o meno, ma di complessità contro durata.