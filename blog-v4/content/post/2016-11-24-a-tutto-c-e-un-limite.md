---
title: "A tutto c’è un limite"
date: 2016-11-24
comments: true
tags: [MacBook, Pro, Ram, Ddr3, Ddr4, Schiller, Lenovo, Thinkpad, p50, T460, Dell, Xps, 15]
---
Forse si può dire una parola, se non definitiva, approfondita sul limite di Ram dei nuovi MacBook Pro a sedici gigabyte, grazie a un notevole [articolo su MacDaddy](https://macdaddy.io/macbook-pro-limited-16gb-ram/).

Il succo del pezzo è che, fatti alla mano, la durata della batteria sarebbe stata penalizzata in maniera percettibile dall’inserimento di trentadue gigabyte di Ram, visti i limiti delle architetture Intel utilizzate. Secondariamente, solo come ordine delle conclusioni, la scheda logica avrebbe dovuto essere riprogettata, molto probabilmente forzando spessore e peso a valori superiori a quelli attuali.

Terzo, Apple avrebbe anche potuto creare una batteria più ingombrante per usare trentadue gigabyte di Ram e garantire la stessa autonomia; ma molto probabilmente avrebbe oltrepassato le specifiche di una normativa dell’aviazione civile americana. Ipotizzo che avrebbe potuto significare il divieto di imbarco in aereo o l’obbligo di imbarco nella stiva.

Avvertenza per quelli che portano sempre un esempio dal mondo Windows per controbattere una affermazione propria del mondo Mac: evitare di menzionare Xps 15 Dell, Thinkpad p50 di Lenovo e sempre di Lenovo Thinkpad T460, e così risparmiarsi una brutta figura.

L’autore dell’articolo pare avere chiaro il concetto di esigenze personali e riferisce di un’attività da lui svolta di recente, mondo reale, professionisti, che ha richiesto scrittura su disco per la bellezza di oltre venti terabyte. Il compito sarebbe stato molto più veloce con trentadue gigabyte di Ram a bordo. La sua posizione è diversa da _tanto a me non serve quindi non serve a nessuno_.

Conclusione:

>Apple non ha mai scelto grandi compromessi ingegneristici e passare adesso alla Ram Ddr4 giusto per supportare trentadue gigabyte sarebbe relativamente assurdo alla luce degli svantaggi descritti in questo articolo. Sarebbe stato più probabile che, per supportare la Ram Ddr4 a basso consumo, avessero sostituito il processore Intel con un processore Arm progettato da loro.