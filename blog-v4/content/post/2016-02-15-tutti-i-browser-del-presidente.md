---
title: "Tutti i browser del Presidente"
date: 2016-02-15
comments: true
tags: [NetMarketShare, Cio, Explorer, Microsoft]
---
Oltre un anno fa ho indicato brevemente i motivi dell’assoluta [inaffidabilità di NetMarketShare](https://macintelligence.org/posts/2015-01-03-fuori-trend/) nel definire le quote di traffico web per sistema operativo, *browser* eccetera.<!--more-->

Non sono (più) solo. La testata *Cio* (tecnica, rivolta a un pubblico di *Chief Information Officer*, non robaccia nazionalpopolare cliccaclicca) constata che [C’è qualcosa di sospetto in questi numeri di diffusione di Windows 10](http://www.cio.com/article/3032570/windows/there-s-something-fishy-about-those-windows-10-market-share-numbers.html).

>Non mi fido dei loro numeri […] Non è solo che sono partner Microsoft; è che insistono a sostenere che Internet Explorer batta a mani basse tutti gli altri browser.

L’autore dell’articolo mi ha insegnato l’esistenza della pagina [analytics.usa.gov](https://analytics.usa.gov), con dati che considera più attendibili.

Certamente sono dati locali, riferiti agli Stati Uniti, non rappresentativi della realtà mondiale. Al tempo stesso, la pagina fa mezzo miliardo abbondante di visite al mese e qualcosa vale. Se non altro, ci si può aspettare che i Paesi industrializzati tendano grossolanamente ad avere dati simili a quelli. Inoltre sono dati puri e non pesati, interpretati, elaborati con criteri discutibili.

Ignoravo anche che NetMarketShare è partner Microsoft. Il misuratore del traffico ha interessi privati condivisi con uno dei misurati. Questo è il chiodo finale sulla bara.

I dati che arrivano sulla scrivania di Obama – e sulle nostre, previo un clic – sono rappresentativi solo degli USA. Eppure scommetto un euro contro un bottone che i dati italiani sono più simili a questi che alle cifre di NetMarketShare.

Se adesso il governo italiano varasse una pagina simile (o mi facesse sapere che c’è), la cosa si farebbe davvero interessante.