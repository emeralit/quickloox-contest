---
title: "Gli assenti hanno torto"
date: 2020-04-10
comments: true
tags: [Face, ID, Touch]
---
A seguito del [dibattito sulla didattica a distanza](http://www.macintelligence.org/blog/2020/04/08/sentenzia-da-casa/), recepisco tutti i distinguo che vengono fatti; la faccenda coinvolge milioni di persone tra studenti e docenti, è per forza complessa e priva di soluzioni semplici e ovvie.

Una obiezione però non la capisco e andrebbe tolta dal tavolo: lo hanno detto anche i sassi, [un terzo degli studenti non ha un computer](https://www.repubblica.it/tecnologia/2020/04/06/news/istat_un_terzo_delle_famiglie_non_ha_pc_o_tablet_in_casa_sud_penalizzato-253279893/).

Sì, e allora? Diamoglielo. Non è una scusa per seppellire la didattica digitale, è una colpevole omissione della scuola e di chi le sta dietro.

Chiunque si aggrappi al dato per argomentare che le lezioni a distanza non possono raggiungere tutti, ignora o finge di ignorare che in Italia [un terzo delle persone ha solo la licenza media e quasi due su cinque non arrivano al diploma](https://www.truenumbers.it/titolo-di-studio-dati-2018/).

Tradotto: *neanche le lezioni in aula raggiungono tutti*. Milioni di persone rimangono fuori dalle lezioni in aula. Eppure nessuno sostiene l’inopportunità di tenerle.

Due torti non fanno una ragione. Piuttosto di disquisire su quelli che restano fuori, pensiamo a portarli dentro, che l’aula sia fisica o virtuale. Dopo quarant’anni di computer dal prezzo accessibile alle famiglie, l’unica didattica che è concepibile pensare unisce analogico e digitale come facce della stessa moneta.

In questo senso bisogna riflettere sul fatto che le famiglie che a scuola i figli li mandano, [spendono centinaia di euro in libri](https://www.studenti.it/caro-libri-per-risparmiare-c-e-chi-li-compra-online.html) (per non parlare del resto del materiale). Ho la netta impressione che se spendessero meno in libri di carta, avanzerebbero i soldi per avere una macchina con cui fare didattica digitale decente, o ci andrebbero vicini.

Come? Tante famiglie per risparmiare si buttano sul libro usato? Certo, l’ho fatto anch’io, che male c’è? Si può fare anche sul tablet usato, sul computer usato. Non serve un computer top di gamma per studiare o per fare videoconferenza. Un po’ di leva in questo senso, un [Libraccio](https://www.libraccio.it) orientato allo hardware stile [Trendevice](https://www.trendevice.com) ed ecco che ci siamo quasi.

Ah, ma la banda, il *digital divide*. Esiste, eh, ma è di un altro tipo: l’idea folle che studiare a distanza sia fare videoconferenza con il docente. Che un’ora di lezione in aula, portata in rete, sia un’ora di video con lezione frontale.

Proviamo con un ragionamento semplice: si scrive con il word processor come sul quaderno? No. Si usano il pallottoliere o il regolo calcolatore allo stesso modo dello *spreadsheet*? No. Si gioca al tavolo come si gioca online? No! *Nessuna attività digitale imita il suo corrispondente analogico*. La lezione digitale è tutt’altra cosa, perché ha punti di forza e debolezze diverse, *et pour cause*. Una buona lezione digitale può funzionare molto bene anche dove la banda scarseggia. In attesa che il nostro staterello colmi anche questo gap (non tratterrei il fiato).

Certo, uno dice, ma tablet usati, banda quella che c’è, sembra un’armata Brancaleone; come si fa con i formati, gli standard, le compatibilità? Come si fa a dare a tutti lo stesso programma per scrivere?

*Si lavora sul formato, non sull’applicazione*. Si lavora sul risultato, non sulla procedura. Si usa il testo puro: qualsiasi macchina lo può utilizzare. Appena serve formattazione, si va su HTML. Appena si sale di complessità, si arriva a XML. Nel frattempo lo studente impara veramente a trattare dati digitali. E continua a lavorare sul testo, compatibile con tutto. Per il resto, non ti devo insegnare come si usa un foglio di calcolo. Ti devo insegnare che cosa ci si fa. Compito a casa: individua la funzione che ti serve, sul programma che hai scelto, per ottenere il risultato chiesto dall’insegnante. Usi Excel, Numbers, [LibreOffice](https://www.libreoffice.org), [Calca](http://calca.io), [Ethercalc](https://ethercalc.net), [Matrex](http://matrex.sourceforge.net), [Wolfram Alpha](https://www.wolframalpha.com), [Sage](http://www.sagemath.org), l’interprete Python della riga di comando? Arrangiati. Conta il *risultato*. L’insegnante passa una mattina con i ragazzi a capire qual è il software che si può usare con l’equipaggiamento a disposizione. Problema risolto. Sei un insegnante e non hai un accidente di idea dei software che si possono usare? Chiamami, costo poco e ti risolvo il problema a livello di istituto. Faccio per dire; chiama chiunque, ci sono decine di migliaia di persone in Italia in grado di formare gli insegnanti su questo e sono quasi tutti migliori di me. Ne hai uno anche dietro casa. Probabilmente un tuo collega sa già tutto. O tua figlia. Ci sarebbe Internet, se proprio. Quel signore in giacca e cravatta che entra in istituto e lusinga il preside perché adotti la soluzione-della-multinazionale-apposta-per-la-scuola, ecco, quella è la strada facile. Infatti è quella sbagliata.

Pdf lo capiscono tutti. Il testo lo capiscono tutti. Html lo capiscono tutti. L’email la capiscono tutti. L’insegnante che conosce [Pandoc](https://pandoc.org) non avrà mai più un problema di compatibilità nella sua vita. Gli insegnanti che conoscono (tre funzioni in croce di) Office e non conoscono Pandoc, loro sono un problema di compatibilità: con la salute didattica dei loro studenti.

C’è gente che già si misura con il problema invece di nascondersi dietro alla videocamera e uccidere qualsiasi passione per lo studio con ore di slide e voce monotona. [Venerandi](http://www.quintadicopertina.com/fabriziovenerandi/?p=1531), per esempio. Tutt’altro che un invasato del digitale, ha ben presente potenzialità e limiti, ha una posizione equilibrata e ragionevole. Certo, nella scuola italiana una posizione equilibrata e ragionevole sullo studio in digitale e a distanza è sufficiente a ribaltare decenni di *sièsemprefattocosì*. Ma prima o poi bisogna prenderne atto; ci sono in ballo milioni di ragazzi che aspettano da noi la conoscenza, invece delle nozioni.

C’è anche mia figlia, che comincia a settembre. Se le dovrai insegnare informatica e mi stai leggendo, prima ti manifesti meglio è. Facciamo in modo che nella scuola di domani (inteso domani mattina) ci sia il digitale e non ci siano assenti, né per mancanza di attrezzature, né per mancanza di competenze. Grazie.