---
title: "Il senno di allora"
date: 2014-05-08
comments: true
tags: [Sony, Apple, Jobs, Blu-ray, Mac, Vaio]
---
Quanti improperi, quanto sarcasmo, quante critiche nel 2008 quando Steve Jobs [dichiarò](http://www.tuaw.com/2014/05/07/steve-jobs-wise-decision-to-avoid-blu-ray/) Blu-ray una *bag of hurt*, una valigia di dispiaceri, e che Apple al momento non aveva intenzione di adottarlo nei Mac:<!--more-->

>Blu-ray è una valigia di dispiaceri. Non intendo dal punto di vista dei consumatori. È grande per guardare i film, ma avere le licenze è molto complesso. Aspettiamo che le cose si sistemino e che Blu-ray decolli prima di gravare i nostri clienti con il costo delle licenze.

Nel 2010 Jobs [rispose](http://www.macrumors.com/2010/06/30/steve-jobs-suggests-blu-ray-not-coming-to-mac-anytime-soon/) per posta a un tale Siva, dispiaciuto di vedere una nuova edizione di Mac mini senza lettore Blu-ray:

>Blu-ray somiglia sempre più a uno di quei formati di fascia alta candidati a succedere ai Cd e che, come i Cd, saranno superati dai formati scaricabili via Internet.

Sony aveva previsto per l’anno fiscale 2013 profitti per l’equivalente di circa 782 milioni di dollari e recentemente ha ridotto la sua previsione a 254 milioni.

Le ragioni sono due. La prima è la sua uscita dal *business* dei personal computer Vaio, [venduto](http://www.theverge.com/2014/2/6/5385212/sony-sells-off-vaio-pc-division) a un fondo di investimento giapponese.

La seconda, come da [documentazione ufficiale Sony](http://www.sony.net/SonyInfo/IR/financial/fr/13revision_sony.pdf), è questa:

>Principalmente a causa della della domanda di supporti fisici, che si contrae più rapidamente del previsto, soprattutto in Europa, la futura profittabilità della produzione di dischi è stata rivista [al ribasso].

Riassumo i periodi successivi, che in originale hanno una sintassi complicata: il *business* dei dischi ottici ha perso più valore del previsto e Sony non riuscirà a recuperare pienamente le spese sostenute per esso, con ripercussioni sul bilancio.

Scommetto che Jobs, quando parlava delle licenze, voleva parlare di qualcosa d’altro ma si trattenne perché era affare di terzi. E sapeva o intuiva molto bene come sarebbe andata a finire.

Certo, con il senno di poi facile dirlo. È che lui ragionò con il senno di allora.