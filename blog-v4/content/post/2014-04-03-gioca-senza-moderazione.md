---
title: "Gioca senza moderazione"
date: 2014-04-03
comments: true
tags: [iWork, AppleScript, Pages, Keynote, Numbers]
---
Per chi ricorda la [scommessa simbolica](https://macintelligence.org/posts/2014-01-27-una-scommessa-ancora-a-mezzo/) fatta su iWork e AppleScript, posso sentirmi vicino alla vittoria. L’aggiornamento di ieri a Keynote, Numbers e Pages (grande soddisfazione nel vederlo per iOS, Mac e iCloud contemporaneamente; non sono nove programmi diversi, ma tre edizioni per programma) apporta decisi miglioramenti in ambito AppleScript a tutte e tre i programmi.

Cosa ancora più importante, il dizionario appare in larga misura unificato. Se si considerano le funzioni base, è possibile scrivere un solo AppleScript che funziona allo stesso modo sull’una o l’altra applicazione semplicemente cambiando il nome della stessa. Segno che lo sviluppo è stato condotto in modo saggio e buon viatico per aggiunte successive.

C’è già disponibile da copiare e capire roba semplice oppure tosta. Al capitolo Pages, uno [script già pronto](http://iworkautomation.com/pages/examples-mail-merge.html) realizza una funzione di *mailmerge* (creazione automatica di lettere personalizzate) partendo da un gruppo di indirizzi in Contatti e da un documento tipo pronto da scaricare e personalizzare. Lo script, basta guardarlo, non è stato realizzato in cinque minuti. È bello pronto e a disposizione di tutti.

Ancora meglio, il sito semiclandestino sull’automazione per OS X ora ha anche una pagina [AppleScript and iWork](http://iworkautomation.com). Invito chi ha fegato e vuole aggiungere anche le ridotte al motore iWork a guardare, provare, soprattutto – così si apprende davvero – giocarci senza moderazione alcuna.