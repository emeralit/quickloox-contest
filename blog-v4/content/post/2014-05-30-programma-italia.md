---
title: "Programma Italia"
date: 2014-05-31
comments: true
tags: [appmadeinitaly, Scarciello, Google+, app]
---
Grazie a [Roberto](https://plus.google.com/+RobertoScarciello/posts) per avermi invitato a conoscere [Made in Italy Apps](https://plus.google.com/communities/107227415039668372788), comunità Google+ dedicata alle *app* nate nel nostro Paese grazie al lavoro di autori che, ricorda Roberto stesso, hanno niente da invidiare a tanti colleghi di oltreconfine.

Mi permetto di estendere l’invito. L’iniziativa è appena partita; scommetto che prenderà piede ben presto. Da ricordare lo *hashtag* #appmadeinitaly.