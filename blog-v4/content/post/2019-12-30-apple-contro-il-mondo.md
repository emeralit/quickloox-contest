---
title: "Apple contro il mondo"
date: 2019-12-30
comments: true
tags: [Apple, Amazon, watch, ebook, Wiesel, News, Rss, Vox]
---
L’azienda più cenerentola, odiata e derisa dagli omologati è diventata in vent’anni il centro della tecnologia digitale e si capisce come i giudizi, se formulati da gente con una vita di informatica sulle spalle, possano non essere del tutto ragionati.

C’è però un limite a tutto. Apple fa cose cattive, cose discutibili (che meritano una discussione) e cose buone. Come tutti. Quando fa cose buone in modo speciale, prende le distanze e vola via, da sola. In altre situazioni fa parte del nostro mondo.

Esempio di cosa cattiva: [Apple News ha smesso di supportare i feed Rss](https://mjtsai.com/blog/2019/12/26/apple-news-no-longer-supports-rss/). È un controsenso, un ossimoro, notizie senza la possibilità di un feed Rss. Rss è una cosa buona.

Esempio di cosa discutibile: un medico [porta Apple in tribunale](https://www.bloomberg.com/news/articles/2019-12-27/apple-sued-by-new-york-doctor-over-watch-s-heart-technology-k4oqtleg) perché watch userebbe senza autorizzazione e senza pagare diritti un suo brevetto sulla metodologia per cogliere segni di fibrillazione atriale nel battito cardiaco.

Detta così lascia scettici, vedremo che cosa decide il tribunale. Rimane il fatto che watch abbia già tolto dai pasticci qualche decina di cardiopatici che non lo sapevano, o che non se ne curavano, o che rischiavano. Se il dottor Joseph Wiesel detiene davvero un brevetto utile a salvare vite in talune circostanze, qualche passo concreto in più per farlo usare – oltre a chiedere soldi a terzi – ci starebbe.

Esempio di cosa buona nonostante tutto: secondo *Vox* [il mancato decollo del mercato degli ebook è colpa di Apple](https://www.vox.com/culture/2019/12/23/20991659/ebook-amazon-kindle-ereader-department-of-justice-publishing-lawsuit-apple-ipad).

L’articolo ce la mette tutta per sostenere una tesi impossibile, tira in ballo i *millennial*, rispolvera la causa persa da Apple che la accusava di avere cospirato assieme ai grandi editori per tenere alti i prezzi degli ebook. Resta il fatto che Amazon detiene praticamente un monopolio e che il pubblico decide quando spendere e quando no, così che, se il prezzo è magari troppo alto, guarda tu, non compra.

Apple ha creato un modello di distribuzione della musica che ha aiutato non poco il mercato. Ha portato lo stesso modello nel software, con App Store, e ha erogato miliardi di guadagni agli sviluppatori, miliardi che altrimenti non sarebbero esistiti o circa. Ha portato lo stesso modello nei libri, dove però penso venda tra il dieci e il venti percento del totale. Cioè è ininfluente nel dettare le dinamiche globali, causa o non causa.

Sembra che il decollo del mercato degli ebook dovesse essere un obbligo di legge; la verità è che Amazon pratica politiche micidiali nei confronti di tutti, dagli autori al pubblico, e che nel novantanove percento dei casi gli ebook sono un prodotto di qualità *mediocre*.

Apple ha fatto solo bene a portare nel mercato un modello di distribuzione che, bene applicato, porta libertà di mercato e perfino troppa, se uno guarda ai prezzi della musica e delle app.

Auspico un 2020 dove si possa parlare di Apple in maniera più serena e meno surreale. Farà la fine dei tipici propositi da anno nuovo, ma provare almeno a dirlo costa pochissimo.