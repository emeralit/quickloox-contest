---
title: "Sbagliare veloci"
date: 2024-03-10T00:40:41+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet, People]
tags: [Dr. Drang, Drang, Macintosh Pascal]
---
In questi giorni mi ritrovo in un aspetto del [post di Dr. Drang sul suo ricordo personale in occasione del quarantesimo anniversario di Mac](https://leancrew.com/all-this/2024/01/my-mac-40th-anniversary-draft/).

In particolare, la sua scelta del software preferito di quei tempi. Che è infine ClarisCAD, con argomentazione inoppugnabile dal suo punto di vista, ma che per un momento avrebbe potuto essere Macintosh Pascal.

(Il mio software preferito di quei tempi? Molta indecisione, tra Daleks, Dark Castle, un database scritto in linguaggio macchina veloce l’impossibile, chiamato OverVue, FreeHand… tuttavia probabilmente la cosa che più mi ha conquistato e impressionato sono i sei ingombranti volumi di Inside Macintosh, al tempo pieni di rivelazioni e misteri oscuri da fare impallidire la biblioteca di Hogwarts).

Lo slogan di Macintosh Pascal, grandioso, era *Make mistakes faster*, sbaglia più in fretta. Ecco: quarant’anni dopo sono qui ad approfittare della possibilità di sbagliare come non succedeva da tempo. Poi Pascal non mi è mai piaciuto, però Macintosh Pascal resterà nella storia. 