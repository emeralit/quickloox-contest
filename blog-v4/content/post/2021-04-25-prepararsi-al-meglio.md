---
title: "Prepararsi al meglio"
date: 2021-04-25T00:43:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Philip Elmer-DeWitt, Martin Peers, The Information, Apple] 
---
A giorni Apple comunicherà i risultati trimestrali. Philip Elmer-DeWitt ha pubblicato [un brano della newsletter The Briefing](https://www.ped30.com/2021/04/24/apple-doomed-martin-peers/), inviata periodicamente agli abbonati di The Information:

>È facile trascurare la realtà dell’andamento finanziario di Apple in mezzo allo sbavare dei fanboy su iPhone viola e iMac colorati, ma l’azienda più valutata del mondo ha performato in modo mediocre come crescita negli ultimi anni.

Il commento di Elmer-DeWitt:

>Gli odiatori devono per forza odiare, ma l’autore del pezzo deve veramente contorcersi come un pretzel per sostenere che dopo i guadagni dell’ottanta per cento nel 2020 e i risultati record del primo trimestre 2021, gli investitori dovrebbero “restare sintonizzati” in attesa di qualche genere di disappunto in arrivo mercoledì.

È stupendo che centinaia di milioni di acquirenti di iPhone, iMac, iPad e tutto il resto siano *fanboy che sbavano*, in un contesto di analisi finanziaria che dovrebbe coinvolgere la quintessenza della competenza.

Sarà un buon trimestre.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*