---
title: "Versi compilabili, in italiano"
date: 2015-11-03
comments: true
tags: [Luca, xCoding.it, Swift]
---
Ricevo e pubblico da **Luca**, cui sono grato per la segnalazione:

**a seguito del tuo post** [Versi compilabili](https://macintelligence.org/posts/2015-10-30-versi-compilabili/), volevo segnalarti un sito tutto italiano: xCoding.it.<!--more-->

Il sito, nato poco più di un anno fa, ha ora diversi iscritti e offre un blog, dei tutorial, un forum in cui si parla di programmazione e dei corsi tra i quali anche uno gratuito di programmazione in Swift, con esercizi da svolgere al termine di ogni lezione.

Non da ultimo, il fondatore di xCoding.it è un ragazzo di 22 anni che ora ha diversi collaboratori e insieme **mandano avanti questo bellissimo progetto**.

Nel panorama generale italiano situazioni come queste hanno valore incommensurabile e posso solo augurare il massimo successo dell’iniziativa. La programmazione è il nuovo inglese. Non dobbiamo diventare programmatori, ma capire e farci capire quelle volte che occorre. Da ripetere prima e dopo i pasti.