---
title: "La banda del buco"
date: 2017-07-21
comments: true
tags: [watch]
---
Il ciclo delle notizie, come lo chiamano in Usa, è davvero in sofferenza e l’estate aguzza gli ingegni di chi deve riempire di pubblicità a basso valore pagine web di valore ancora inferiore.

Ne è portavoce *The Verge*, con un articolo surreale su [come dovrebbe diventare lo *smartwatch* per sopravvivere](https://www.theverge.com/circuitbreaker/2017/7/16/15974000/smartwatch-smart-watch-bands-buckle-kickstarter-hybrid-mechanical-timepieces).

La tesi è infatti che il concetto non funzioni e che la sua evoluzione debba essere l‘integrazione della tecnologia dentro il cinturino, che possa essere applicato a orologi convenzionali o ibridi, un po’ analogici un po’ digitali.

Per dire che il concetto non funziona sembrerebbe prestino, pensando per esempio che iPhone, dopo tre anni dal lancio, funzionava ma non era certo affermato come oggi.

In più su dipinge l’utilizzatore di computer da polso come unicamente interessato a notifiche e tracciamento dei dati della salute.

Anche ammesso che sia vero, averne! Facile dismettere le notifiche come un lusso superfluo prima di fermarsi a constatare che sono *comunicazione*. Quando ricevo una notifica su watch, talvolta è il segnale di una conversazione. Capita che interi scambi di messaggi della mia giornata lavorativa passino attraverso watch e poter fare a meno dello schermo più grande, in molti frangenti, è qualità della vita.

Non c'è altro, oltre a questo? Pazienza, ma basta e avanza! È comunque del’altro c’è, come chiunque abbia usato watch in viaggio, in cucina per impostare un timer, nello sport per avere una colonna sonora sa benissimo. Un settore nato da tre anni, che ha già superato gli svizzeri, è difficile da liquidare sulla base del fatto che intanto si è venduto oltre un miliardo di orologi convenzionali. Agli occhi di chi produce un watch si tratta solo della conferma dell’esistenza di un mercato sconfinato.

Inevitabilmente, poi, si corre con il pensiero a quando iPhone era acerbo e si vendevano miliardi di cellulari convenzionali. Sono trascorsi neanche dieci anni, non cinquanta o cento, e interi mercati e modi di vivere sono stati rivoltati.

Comunque è gente che va capita. Il sito è lì, se non inventi tu inventa un altro, tanto vale parlare in qualche modo di un prodotto anche in assenza di qualunque fatto vero.
