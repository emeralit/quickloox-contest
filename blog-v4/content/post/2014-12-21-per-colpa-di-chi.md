---
title: "Per colpa di chi"
date: 2014-12-21
comments: true
tags: [Bbc, Apple, stagno, Indonesia]
---
La Bbc ha pubblicato un servizio su certe miniere di stagno in Indonesia pericolose e malsane, dove lavorano bambini in condizioni inconcepibili e da cui si estrae metallo per innumerevoli prodotti tra cui elettrodomestici, e computer da tasca.<!--more-->

Naturalmente il servizio è uscito puntando il dito verso Apple. La quale si è dichiarata in una email [profondamente offesa](http://www.bbc.com/news/technology-30548468), aggiungendo che poche altre aziende lavorano con la stessa intensità per migliorare le condizioni di lavoro lungo tutta la catena delle forniture. Nello specifico:

>Apple ha due scelte: assicurarsi che tutti i propri fornitori comprino stagno da fonderie non indonesiane, probabilmente la cosa più facile per schermarsi dalle critiche. Ma sarebbe la strada più pigra e codarda, perché farebbe niente per migliorare la situazione dei lavoratori indonesiani o l’ambiente, dal momento che Apple consuma una minima frazione dello stagno ivi prodotto. Abbiamo scelto la seconda strada, restare coinvolti e provare ad agevolare una soluzione collettiva.

I salvatori del mondo che invocano i boicottaggi o si inventano prodotti *ecologici* o *etici* potrebbero, per cominciare, consultare la pagina [Responsabilità dei fornitori](https://www.apple.com/it/supplier-responsibility/) e poi confrontarla con quelle di altre aziende. Se esistono.

E poi porsi qualche domanda sulla catena delle responsabilità. Rispetto ai bambini indonesiani che lavorano in miniera, che cosa fanno i genitori o i tutori, la polizia indonesiana, il governo indonesiano, le aziende indonesiane.

Qualcuno crede che boicottando quello stagno i bambini si metteranno a correre per i prati a catturare farfalle. Sono gli utopisti, a cui frega niente dei bambini e interessa solo la propria Idea. I più pericolosi.