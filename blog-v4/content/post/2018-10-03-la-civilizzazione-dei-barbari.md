---
title: "La civilizzazione dei barbari"
date: 2018-10-03
comments: true
tags: [iPhone, Xs, Surface]
---
Mi scuso per l’autocitazione. È che ieri, quando [scrivevo](http://www.macintelligence.org/blog/2018/10/02/nuovo-hardware-nuovo-software/):

>C’è gente che va in crisi di astinenza se la privi dei gigahertz, dei gigabyte, dei nomi di tecnologie e funzioni scelti apposta per farli sembrare importanti e per fare sembrare importante, soprattutto, chi li pronuncia.

Non immaginavo che oggi mi sarei ritrovato a leggere un [articolo di questo tenore](https://www.engadget.com/2018/10/02/surface-pro-6/):

>È tempo di un rinfresco e Surface Pro 6 arriva con Cpu Intel di ottava generazione unite a sedici gigabyte di Ram e un Ssd […]. Il display 12” da 2.736 x 1.824 è migliorato, a 267 punti per pollice e […] il miglior contrasto mai raggiunto. C’è anche una fotocamera dorsale da otto megapixel assieme a quella frontale da cinque megapixel e 1080p.

Sono ancora lì, a contare che cos’è più lungo, più grosso, più grande, più di questo e più di quello. A parte le grandezze specifiche, poteva essere il 1998. E tutto questo spiegamento per che cosa? Office. Appunto, il 1998.

I barbari sono indietro di vent’anni, ma prima o poi arriveranno anche loro alla civilizzazione. Il timore è che per recuperare i venti anni ce ne mettano venticinque.