---
title: "Conflitto di interessi"
date: 2017-11-07
comments: true
tags: [iPhone, X, MacRumors, SquareTrade]
---
*MacRumors* titola [SquareTrade afferma che iPhone X è “l’iPhone più fragile di sempre” dopo test di caduta che portano alla rottura del vetro](https://www.macrumors.com/2017/11/06/iphone-x-breakability-drop-tests/).

Parafraso in base al mestiere di SquareTrade: *Venditore di garanzie per apparecchi elettronici afferma che il modello di cui parlano tutti ha forte bisogno di una garanzia*.

Sapendo che i preordini di iPhone si sono aperti col botto e la lista di attesa [è lunga alcune settimane](https://www.forbes.com/sites/anthonykarcz/2017/10/27/apple-iphone-x-pre-orders-sell-out-in-minutes-wont-be-back-in-stock-till-december/#1f9375b6341a), parafraso ancora.

*Venditore di garanzie per apparecchi elettronici afferma che un modello che sarà probabilmente molto venduto ha forte bisogno di una garanzia*.

A me sembra una affermazione lievissimamente interessata.