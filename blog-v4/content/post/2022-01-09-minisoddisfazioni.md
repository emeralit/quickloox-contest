---
title: "Minisoddisfazioni"
date: 2022-01-09T00:01:22+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Due cose che mi sono dimenticato parlando del [Mac mini M1 preso per il 2022](https://macintelligence.org/posts/2022-01-07-anno-nuovo-mac-nuovo/).

La macchina regge senza alcun problema la risoluzione massima del monitor (3.840 x 2.160). La specifica sarebbe teoricamente uguale a quella del vecchio mini, solo che quest’ultimo lavorava al limite; a volte cliccavo la barra spazio per svegliare il computer, in stop con monitor in standby, e lo schermo non si riaccendeva. C’era forse anche qualche problema di software, perché talvolta le ventole si imbizzarrivano e dietro le quinte succedeva certamente qualcosa. Senza che tornasse l’immagine; dovevo tornare in un altro momento oppure provare a chiudere qualcosa a caso via controllo remoto con iPad.

Alla fine l’unica soluzione era stata abbassare la risoluzione di uno scalino, a 2.560 x 1.440. Non ci si lamenta di 3,7 megapixel, eh. Andava benissimo. Ora però lavoro su una scrivania da 8,2 megapixel, che vuol dire tanta produttività in più. Il monitor è lo stesso, la frequenza è la stessa.

Seconda cosa: poco dopo essere tornato all’operatività, Mac mini ha chiesto l’installazione di Rosetta. Lo prevedevo, dato che certi software certamente sono disponibili solo compilati Intel, almeno per ora.

Il punto è che non me ne accorgo. Se è molto facile sapere quali app richiedono lo strato di traslitterazione da Arm a x86 (nelle Informazioni), nell’uso quotidiano non si percepisce alcun salto tra l’ambiente nativo e quello ereditato. App come [OBS](http://obsproject.com) sono dichiaratamente Intel ma vanno ben più veloci di prima; app universali come [Pixelmator Pro](https://www.pixelmator.com/pro/) funzionano con tanta leggerezza in più; in questo caso, specialmente, gli interventi della funzione di machine learning sono rapidi e snelli. Prima, ogni tanto, ci si faceva un caffè ad aspettarli.

Cose piccole; se avessi avuto un Mac mini Intel i5 o magari i7, non credo che avrei cambiato e forse il divario non si avvertirebbe così vasto. Da i3, non c’è confronto. E il guscio del computer, dopo avere aperto la qualunque tra app e pagine web, si mantiene praticamente freddo, o minimamente caldo, non saprei definirlo con precisione. Questa è certamente la vera qualità della vita con M1.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*
