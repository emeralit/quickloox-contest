---
title: "La (data)base del computing"
date: 2022-09-21T01:58:05+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Rome FileMaker Week, FileMaker, Giulio Villani, Claris, FMGuru]
---
Chi domina il testo domina il computing. A riprova: con un Terminale acceso e la capacità di usarlo, niente è precluso. Gli zero e gli uno sono fuori dalla portata umana; la rappresentazione di ordine immediatamente superiore – il testo – ha potenziale infinito. È questa la prima base del computing.

Se però si tratta di costruire qualcosa – creare una struttura, organizzare dati, dare vita a insiemi ordinati di informazione – chi domina i database domina il computing. Dietro il Finder c’è un database, dietro Foto c’è un database, dentro le app ci sono database, Spotlight è un database.

L’uso diretto dei database è stato quindi irrinunciabile, per molti anni. Nella cassetta degli attrezzi doveva esserci un database.

Nel tempo il database è scivolato dietro le quinte. Come il Terminale, guarda caso. Rappresentazioni di ordine ancora superiore delle informazioni hanno preso il sopravvento, nascondendo l’esperienza diretta del database.

L’intuito mi suggerisce che questo scenario stia cambiando, anche con l’evoluzione del concetto di database come app. E niente racconta questa evoluzione più delle trasformazioni di FileMaker, da database *for the rest of us* a supermotore per applicazioni di livello come minimo aziendale a… qualcosa di diverso.

Se si chiede a un passante che cosa sia oggi FileMaker, quasi certamente non saprà rispondere. Lo stesso passante dieci o venti anni fa avrebbe parlato di campi, record, tabelle, interrogazioni, insomma di un database. Un database classico. Oggi, in cima al sito di [Claris](https://www.claris.com), produttrice di FileMaker, si legge:

>Personalizza applicazioni. Automatizza workflow. Sii padrone della tua innovazione con Claris FileMaker e Claris Connect.

Il database c’è sempre, solo che il suo trattamento oggi può portare un professionista o un azienda molto più lontano, soprattutto molto più avanti, di prima.

Sembrerà un paradosso: la verità è che nel discorso comune un prodotto come FileMaker viene a galla molto meno di una volta. Proprio per questo, FileMaker è oggi rilevante come forse non è mai stato. Nel computing, le opportunità sono dove la folla non guarda.

Ecco perché è rilevante, rilevantissima, la [FileMaker Week di Roma dal 4 al 9 ottobre](https://www.mettilealialtuosviluppofilemaker.com). È una settimana intera per scoprire FileMaker, lato tecnico, e vivere una esperienza notevole di (ri)scoperta della capitale, lato umano.

A organizzare tutto è la community FileMaker più importante in Italia, [FMGuru](https://www.fmguru.it), una garanzia assoluta lato tecnico e lato umano. Parlo per esperienza, essendo stato [ospite per un giorno di raduno qualche anno fa](https://macintelligence.org/posts/2017-01-16-guru-per-un-giorno/) oltre che conoscendo da tempo [Giulio Villani](https://www.fmguru.it/author/giulio-villani/), anima di FMGuru.

Il programma che hanno messo in piedi per Rome FileMaker Week è impressionante; c’è da uscirne trasformati, o promossi a un livello superiore di padronanza di FileMaker. Con intrattenimento di gran livello una volta terminati i lavori.

Perfezionamento? Contatto con la comunità? Scoperta di FileMaker? Scoperta di un percorso professionale possibile? Se ti iscrivi a un corso di Excel, non vai da nessuna parte. Se ti iscrivi a una Rome FileMaker Week, c’è caso che getti le basi per una carriera di lavoro in una nicchia di mercato dove la domanda di competenza è probabilmente assai maggiore dell’offerta.

Per me la logistica di quei giorni è complicata e non so se mi sarà possibile presenziare. Ma avrei un elenco di gente, se fossi un bravo venditore, da tampinare con ogni mezzo, perché sarebbe per loro un’esperienza preziosa e non uso a caso questa parola.

Non sono un bravo venditore, ma sono capace di empatia verso le persone e le organizzazioni meritevoli. Auspico alla Rome FileMaker Week un gran successo e a tante persone di non buttare via un’occasione veramente extra-ordinaria.