---
title: "Fuga per la sconfitta"
date: 2020-06-18
comments: true
tags: [Brogue, Yendor]
---
Purtroppo non sono andato lontano dopo avere preso [l'Amuleto di Yendor](https://sites.google.com/site/broguegame/). Però sono arrivato a prenderlo e non era mai successo prima.

Non avevo capito che il dover riportare l'Amuleto alla superficie costringe a ripensare totalmente tattica e stile di gioco rispetto a come si arriva a prenderlo.

Tanto di cappello allo sviluppatore. Più gioco a Brogue e più rimango quasi sconcertato dalla sottigliezza complessiva del gioco.