---
title: "Tutti al mare"
date: 2014-07-28
comments: true
tags: [Phil, Venus, Jobs, Venezia, Brindisi, Tirreno, Adriatico, Macitynet, iMac, Leonardo]
---
Lascio la parola a [@Tecnophil](https://twitter.com/tecnophil). Da grassetto a grassetto.

**Come** avrai sicuramente saputo, il *Venus*, *yacht* della famiglia Jobs, ha fatto scalo in alcuni porti italiani.<!--more-->

Come nei migliori film di Fantozzi si favoleggiava sull'esistenza e sull’aspetto fisico del megadirettore galattico, qui si scrivono baggianate talmente assurde che non si sa se ridere o piangere…

>dotato di oltre 27 iMac nella plancia di comando
[Leonardo](http://hi-tech.leonardo.it/venus-lo-yacht-steve-jobs-in-italia/)

>A bordo c'è una plancia di comando composta da sette iMac da 27 pollici.
[Quotidiano di Puglia](http://quotidianodipuglia.it/brindisi/lungomare_di_brindisi_yacht_steve_jobs/notizie/817630.shtml#fg-slider-auto-75157)

Oppure, da [Macitynet](http://www.macitynet.it/venus-yacht-voluto-jobs-in-viaggio-in-italia/)…

>Successivamente Venus ha disceso la costa del Tirreno ed è stato visto attraccare prima a Venezia, in Riva degli Schiavoni nei giorni della festa del Redentore, e poi in riva Sette Martiri, a poca strada a piedi da Piazza San Marco. Infine è arrivata a Brindisi dove si trova, come ci segnalano alcuni lettori, nella giornata di oggi.

Chiaramente Venezia e Brindisi si trovano in **Adriatico!**

Tutti al mare. A svuotarlo con il secchiello, se può aiutare qualcuno a stare lontano dalla tastiera.