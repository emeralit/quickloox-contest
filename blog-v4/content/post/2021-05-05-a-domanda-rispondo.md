---
title: "A Domanda rispondo"
date: 2021-05-05T00:33:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Facebook, iOS 14.5, iOS 14.5.1, Flavio] 
---
Ho aperto la app di Facebook. È apparsa la schermata introduttiva alla Domanda. Facebook mi ha spiegato che permettere alla app di tracciare la mia navigazione su Internet *esterna a Facebook*

* mi permette di avere pubblicità personalizzata;
* aiuta Facebook a mantenere gratuito il suo servizio;
* aiuta le piccole attività che vivono di pubblicità.

Il momento che aspettavo da mesi, di cui si parlava ultimamente con **Flavio** perché, pur avendo aggiornato iOS, le app non ponevano la Domanda.

Mi sono bloccato. Senza motivo; qualunque risposta dia alla Domanda, posso sempre tornare indietro e cambiare la mia impostazione. Non bastava. Dovevo pensarci bene. Questa situazione è senza precedenti e non riguarda la pubblicità, Facebook, o l’ecosistema su Internet. Forse non riguarda neppure me.

**Pubblicità personalizzata**. Ecco, credo di essere un po’ fuori dalla media. Non ricordo quando ho comprato l’ultimo paio di scarpe. Al mattino indosso una t-shirt qualunque, magari vecchia vent’anni. Non ricordo di avere mai posseduto un’auto con meno di centomila chilometri addosso. Compro Mac nuovi di zecca, iPad Pro carrozzati; poi però mi durano dieci anni.

Non è snobismo; al più, trasandatezza.

Un paragrafo fa ho già diffuso più profilazione di quanta sia riuscita a farmene Facebook. Il quale Facebook, funziona così. Per lavoro ho di recente chiacchierato con una giornalista-sommelier-consulente in comunicazione per aziende vinicole. La sorella di una mia ex collega di lavoro dirige il marketing di un importante venditore online di vino.

Nella mia timeline di Facebook compaiono pubblicità di vino. Che non bevo.

Rispetto la pubblicità e capisco il valore della pubblicità personalizzata. Ma questa non è personalizzazione; è un pasticcio maldestro. A questo livello, preferisco la pubblicità generica.

**Servizio gratuito**. Facebook a pagamento. Certo. Avanti il prossimo.

**Piccole attività**. Solidale, molto solidale. Sono un libero professionista; a mio modo, sono una piccola attività. Potrei guadagnare di più intercettando la posta elettronica di clienti potenziali? Seguendo manager di nascosto per capire se potrei offrire loro consulenze? Approfittando dell’accesso a reti aziendali in cerca di segreti da rivendere?

Probabilmente sì. Solo che, molto prima di essere illegale, è ingiusto. È scorretto. È sbagliato. È disonesto. (È anche stupido, eh). Mai.

La piccola attività può convincermi con una pubblicità creativa, un sito onesto, una newsletter ben scritta. Sono vulnerabile alla buona comunicazione.

La piccola attività vuole seguirmi dove e quando non la riguarda? Magari a mia insaputa? Come sopra. È sbagliato e poi anche tutto il resto. È sempre stato così? Pazienza. Il momento di portare una normalità in questo ambito non arriverà mai troppo tardi. Se la piccola attività si sostiene solo grazie al tracciamento inappropriato delle persone, mi dispiace e rimango solidale, ma c’è un problema da correggere. La mia piccola attività è onesta e preferisco giocare ad armi pari.

Tutto questo ancora non bastava. Perché non disinteressarsene? In fondo, che cosa mi può fare il tracciamento di Facebook? Quando mi collego fuori da iOS mi traccia ugualmente. Di enti che mi tracciano, a parte la protezione offerta da Safari, ce ne sono decine. Alla fine la Domanda somiglia a una goccia nel mare. Perché prendere posizione, senza guadagnarci niente?

Perché ho visto nascere Internet. Di recente citavo [John Perry Barlow e la sua dichiarazione di indipendenza del ciberspazio](https://macintelligence.org/posts/Aziende-planetarie-al-posto-di-stati-nazionali.html). Per un attimo abbiamo visto barriere che si sbriciolavano, una nuova coscienza, la *netiquette*. Ho passato nottate su server Unix privi di interfaccia, con una connessione Internet rubata, grazie a un modem veloce come una pianta grassa, a chiacchierare senza scopo con sconosciuti di tutto il pianeta. Era l’alba di una nuova epoca, paragonabile a quando migliaia di navigatori hanno iniziato a bordeggiare nel Mediterraneo alla ricerca di chissà che e, con il commercio, con il dialogo, è fiorita la civiltà.

Il commercio. Niente in contrario che si possa vivere con Internet, anzi. La pubblicità rispettosa ha tutti i suoi perché. Ho comprato una *ring light* proprio grazie a una inserzione su Facebook.

È utopico e impossibile pretendere di avere tre miliardi di persone connesse e un ambiente medio da élite di intellettuali. Se Facebook riesce a raccogliere due miliardi di utenze attive, è un merito. Se coltiva una crescita a base di polarizzazione e valorizzazione di chi è inascoltabile, va deprecato. Ma stare su Facebook non è obbligatorio e nemmeno siamo obbligati a seguire gente che non si può ascoltare.

Il punto è esattamente l’opposto: *essere obbligati a essere seguiti*, senza che neanche lo sappiamo.

La Domanda, semplicemente, ripristina un frammento perduto di quelle aspirazioni che avevamo. Una nuova epoca, con più dialogo, più possibilità, più conoscenza reciproca. Internet come motore di un salto in avanti dell’umanità.

Ecco perché ho risposto *Ask app not to track*, chiedi alla app di non tracciarmi. È una cosa enorme, per i nostri tempi. Eppure è il minimo. Siamo stati abituati malissimo, Facebook davanti a tutti (parlo di Facebook, ma sono tanti). La Domanda è il primo passo, piccolo anche se immenso, per riportare su Internet il rispetto per ognuno. E sono mostruosamente orgoglioso questa notte di avere un *device* che pone la Domanda.

È perfino possibile dire *sì, tracciami, non c’è problema*. Per la prima volta da lustri, ritorniamo ad avere una parte di controllo dell’esperienza. Non è importante rispondere sì o no; è importante rispondere.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*