---
title: "Il tempo della ragione"
date: 2023-06-18T01:45:43+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Lisp, Common Lisp, Reddit]
---
Oggi Reddit è a rischio implosione per [avere messo a pagamento le sue interfacce di programmazione applicativa](https://techcrunch.com/2023/04/18/reddit-will-begin-charging-for-access-to-its-api/), e che pagamento; ci sono [servizi che dovrebbero pagare decine di milioni](https://apolloapp.io) e quasi certamente chiuderanno a breve appunto per [l’impossibilità di farlo](https://www.theverge.com/2023/5/31/23743993/reddit-apollo-client-api-cost).

Sembra tutto abbastanza irragionevole.

Ma c’è stato un tempo più gentile e lungimirante, in cui si scriveva per costruire più che per monetizzare a ogni costo, e la rete costituiva un luogo di aggregazione per interessei comuni piuttosto che un mare dove fare pesca a strascico a caccia di denaro.

È il tempo in cui veniva scritta la prima versione di Reddit. Non a caso, trattandosi di un tempo più ragionevole e logico, [è stata realizzata in Common Lisp](https://github.com/reddit-archive/reddit1.0) e a leggere i sorgenti sembra di essere seduti in spiaggia al tramonto, cullati dalla risacca.