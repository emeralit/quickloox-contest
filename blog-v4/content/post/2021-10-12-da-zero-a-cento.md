---
title: "Da zero a cento"
date: 2021-10-12T00:16:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, Steve Jobs, Macworld Italia, Edoardo Volpi Kellermann] 
---
Proprio l’altro giorno, su stimolo di [Edoardo](https://tolkieniana.net/wp/rivendell/musica/edoardo-volpi-kellermann/), inserivo qui [un vecchio articolo](https://macintelligence.org/posts/Azioni-e-reazioni.html) scritto molti anni fa per *Macworld Italia*, con riferimenti all’epoca in cui Apple chiudeva l’anno fiscale 1997 con un miliardo di dollari in perdite su sette miliardi di ricavi.

Stava tornando Steve Jobs e le cose cambiarono piega nel giro di pochi mesi.

Oggi leggo che Apple potrebbe [chiudere l’anno fiscale 2021 con un attivo di cento miliardi di dollari](https://www.barrons.com/articles/oil-big-tech-saudi-aramco-apple-microsoft-amazon-alphabet-51633713073).

Di fronte a una notizia come questa possono aprirsi numerosi fronti di discussione, di cui uno solo mi interessa: comunque vada, abbiamo vissuto un momento storico irripetibile.

Ieri pomeriggio mi occupavo di un libro che spiega come guadagnare un milione di follower sui social media nel giro di un mese. Fa impressione, sì. Convincere un miliardo e mezzo di persone della bontà del tuo prodotto, che si trova fuori dalla corrente e va scelto perché nessuno te lo fa trovare scontato sul tavolo, è davvero tutta un’altra impresa.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*