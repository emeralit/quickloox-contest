---
title: "Uno su mille ce la fa"
date: 2013-07-21
comments: true
tags: [Apple]
---
C’è ancora speranza di trovare vita intelligente su Internet. *Macgasm* ha deciso di [farla finita con gli Apple Rumors](http://www.macgasm.net/2013/07/17/apple-rumors-were-so-over-it/):

>Non riesco più a sopportare di scrivere sempre lo stesso articolo anno dopo anno. Si può tranquillamente raccogliere il gossip di un anno su iPhone e riscriverlo uguale l’anno dopo, cambiando iPhone 5 con iPhone 6. Non sto neanche scherzando, succede un sacco di volte.<!--more-->

Intanto, in Italia, *Macity* titola [Time Capsule difettosa, secondo Which? è un problema comune](http://www.macitynet.it/time-capsule-difettosa-secondo-which-e-un-problema-comune/).

*Which* è una rivista stile *Altroconsumo* che si è già visto appartenere a [mosche cocchiere](https://macintelligence.org/posts/2013-07-04-mosche-cocchiere) incompetenti e superficiali, anche se per *Macity* è una *spettata rivista*.

A *Which?* avrebbero intervistato 1.926 persone chiedendo loro quali dischi esterni hanno il maggiore tasso di guasti. I [risultati](http://blogs.which.co.uk/technology/apple/apple-external-hard-drives-highest-fault-rate/) della ricerca – che naturalmente *Macity* si guarda bene dal linkare – allineano nella classifica Buffalo, Toshiba, Seagate, Western Digital, Sony, Samsung e Apple.

Ci sarebbe un piccolo problema: Apple non produce dischi rigidi.

Ce ne sarebbe un altro: *Which?* ha intervistato 1.926 persone, non 1.926 proprietari di Time Capsule. Tra questi ci sono, appunto, proprietari di dischi di sette marche diverse e il possesso di dischi Apple riguarda solo una percentuale non rivelata di questi 1.926 maghi. Se le Time Capsule in gioco siano cento, mille o ventisette, non si ha modo di saperlo.

*Macity* commenta che, non essendoci altre informazioni disponibili,

>è impossibile speculare oltre il semplice dato numerico.

Che non c’è! Esistono solo percentuali, non si sa relative a che insieme. Oltretutto è facilissimo che le quantità di dischi per marca cambino e che, magari, lo studio confronti cinquanta dischi Buffalo con cinquecento dischi Sony. Una cretinata priva di ogni senso statistico e scientifico.

*Macity* ci mette del suo con delirio tra il poetico e il lisergico:

>L’utente del dispositivo di Apple, per la peculiarità di Time Machine, sfrutta molto più intensivamente la meccanica del disco esterno per back up costanti e ripetuti, rispetto a chi lo usa un HD per collocare su di esso file di grandi dimensioni o per archiviare documenti. A questo punto i cicli di lettura e scrittura della Time Machine, superiori a quelli cui viene sottoposto un HD esterno tradizionale, spiegherebbero in parte il maggior numero di guasti.

Strafalcioni compresi. Roba che non sta né in cielo né in terra. La conclusione del pezzo è favolosa:

>Apple ha riposte all’indagine di Which? dicendo essere a conoscenza del problema ma solo per alcuni esemplari, venduti tra febbraio e giugno del 2008

Neanche il punto finale. *Il problema* poi non si sa quale sia, a meno che tutte le Time Capsule conteggiate da *Which?* si siano guastate per la stessa e identica ragione. Roba forte.

*Macgasm* ce l’ha fatta. *Macity* sta ancora negli altri novecentonovantanove.