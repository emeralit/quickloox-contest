---
title: "L’accento sull’età"
date: 2022-02-03T01:38:01+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Uno dice che è una faticaccia dover pensare a Sanremo per motivi di famiglia, ma non considera che, nel mio caso, significa dovermi occupare di Raiplay.

Glisserò con scioltezza sui crash, sul blocco occasionale della rotazione dello schermo di iPhone, dell’icona di AirPlay che sparisce, della scarsa sensibilità dei pulsanti, di tutte le scelte di navigazione interfaccia che testimoniano problematiche certamente situate oltre il mero fatto tecnico.

Ma, ecco, siccome la tecnologia non si arresta mai, quest’anno potevo fare qualcosa più del solito: caricare la app su tv per usarla direttamente sul televisore e lasciare liberi da impegni, almeno loro, gli apparecchi esterni.

L’ho fatto e dopo l’installazione mi è comparsa la schermata di presentazione della app. Una app che, mentre scrivo, si trova alla versione 5.3.1; la versione 5.x, dice App Store, era già in lavorazione l’anno scorso. Raiplay è in giro come minimo da oltre un lustro, anni e anni di sviluppo, da parte di un’organizzazione come la Rai che sarebbe pure servizio pubblico oltre a godere di risorse, eufemismo, abbondanti.

E non riescono ancora a mostrare una schermata di presentazione con le accentate corrette.
