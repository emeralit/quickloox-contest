---
title: "L’era spaziale che sarà"
date: 2021-08-31T01:56:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Frederic Filloux, Monday Note, Sputnik, Apollo, Gagarin, Armstrong, Arthur Clarke] 
---
Ho qualche amico nostalgico dei tempi eroici dell’informatica, di quando ogni giorno c’era un progresso e si viveva con l’entusiasmo di tutto quello che poteva stare per succedere, anche senza sapere che cosa sarebbe successo. Era un Paese delle meraviglie tecnologico, da abitare con entusiasmo e curiosità. *Mentre oggi*, è sempre la conclusione, *non è più così*. Soprattutto, *tutto è già stato inventato*.

Ho sempre combattuto questa visione delle cose, che prelude alla senilità (del pensiero, il corpo è un’altra cosa) e difatti esistono articoli come quelli di Frederic Filloux di *Monday Note* dedicate ai piani della cosiddetta Big Tech per [espandersi nello spazio](https://mondaynote.com/big-tech-the-new-space-invaders-2f27d47f1d08).

Filloux, per capirci, scrive su *Monday Note* in alternanza con Jean-Louis Gassée e non è esattamente uno che passi per caso.

Nel quarto articolo della serie *Big Tech, i nuovi Space Invaders* si parla di un settore tecnologico del tutto nuovo, anche se i servizi che si vogliono offrire sono sempre quelli e i grandi nomi dell’industria sono già noti a chiunque. Ma si parla in embrione della nuova era spaziale che si avvia a cominciare. La prima, Gagarin, Sputnik, Apollo, Armstrong, è stata l’equivalente degli anni cinquanta dell’informatica, con le schede perforate e le valvole termoioniche. La prossima, SpaceX e tutto il resto, equivarrà all’informatica dei primissimi anni settanta.

Vale a dire che inizieremo a vederne delle belle e anche la tecnologia digitale attraverserà passaggi piuttosto interessanti, per chi saprà vedere oltre la superficie dei banali contratti di cloud o di noleggio satelliti.

Le previsioni di Arthur Clarke e altri personaggi della fantascienza stanno per iniziare ad avversarsi, solo con qualche anno di ritardo e in forma più articolata. L’epoca delle meraviglie sta per tornare, solo un po’ di pazienza e – sempre – di curiosità.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*