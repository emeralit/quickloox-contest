---
title: "Imprese impossibili"
date: 2019-11-15
comments: true
tags: [Jamf, IBM, SAP, Catalyst, Mac, iPad]
---
Alla ricerca di piccole realtà che decidono clamorosamente di adottare macchine Apple in ufficio nonostante, ovvio, non siano adatte all’uso aziendale, all’esigenza professionale, alle necessità del business. Organizzazioni inconsapevoli dal male che si arrecano.

Dopo [IBM](https://macintelligence.org/posts/2019-11-13-pronipoti-e-antenati/), ecco [SAP](https://www.sap.com/italy/index.html). Una mosca bianca, estranea alle logiche del mercato e della concorrenza. [Centomila dipendenti, quattrocentomila clienti, venticinque miliardi di fatturato](https://www.sap.com/corporate/en/company.html). Gente evidentemente avulsa dalla realtà del lavoro quotidiano.

Solo così si spiega la follia di dispiegare, nel corso del solo 2018, [diciassettemila Mac, ottantatremila apparecchi iOS e centosettanta Apple TV](https://www.applemust.com/sap-to-port-ipad-apps-to-mac-using-catalyst-and-more/) (un indizio: non sono usate per guardare la televisione).

Moriranno, sono pazzi. Nonostante [l’andamento delle azioni](https://www.sap.com/corporate/en/investors/stock.html) sui cinque anni sia assolutamente lusinghiero.

Chiaro che giocare con nomi che non conosce nessuno è troppo facile. Chiedo pazienza, notoriamente non sono obiettivo. Troverò prima o poi un esempio veramente convincente.
