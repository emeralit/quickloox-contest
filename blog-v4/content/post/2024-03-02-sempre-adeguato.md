---
title: "Sempre adeguato"
date: 2024-03-02T01:46:58+01:00
draft: false
toc: false
comments: true
categories: [Gardware]
tags: [iPhone]
---
In questo periodo il mio ciclo di sonno e veglia può definirsi, ecco, variegato, con variazioni importante degli orari di sveglia, delle ore di sonno e quant’altro.

È con un misto di soddisfazione, fiducia e serendipità che vedo iPhone cambiare il ritmo di carica notturno della batteria in funzione di come cambia la mia agenda. Se a un certo punto diceva di ottimizzare la carica per le 6:30, poi è passato alle 10:30 in un periodo di lavoro molto notturno e, per coincidenza ma perfetta, ha appena adeguato l’orario di fine carica all’ora precisa della sveglia di domani.

È un caso, non poteva saperlo in anticipo e non aveva elementi per prevederlo. Ma è la ciliegina sulla torta di un comportamento ben più solido e regolare che ti fa sentire tranquillo; ci pensa lui.

Ben diverso da watch che ti segnala di alzarti in piedi anche se sono le cinque del mattino e sei appena andato a dormire. Pur di chiudere il cerchio turchese delle dodici alzate si fa tutto, ma dovrebbe farsi un’idea di come sono passate le ore precedenti e magari darsi una regolata.