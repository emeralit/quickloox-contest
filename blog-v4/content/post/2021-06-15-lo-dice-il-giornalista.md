---
title: "Lo dice il giornalista"
date: 2021-06-15T00:07:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Giorgio Dell’Arti, Anteprima, Word, Mailchimp] 
---
Il livello della stampa quotidiana italiana, specialmente nelle versioni telematiche, non è esaltante e da molto tempo ho smesso di perdere tempo con quei siti. In compenso pago molto volentieri Giorgio Dell’Arti per la sua [Anteprima](https://anteprima.news/), newsletter quotidiana nei giorni feriali che spreme le cose sensate e interessanti dalle edizioni cartacee e le mette insieme in una rassegna perfetta per cominciare la giornata.

Dell’Arti è un giornalista vecchia scuola con un talento mostruoso per l’archiviazione e il ritrovamento delle notizie di archivio. Non è un informatico ma neppure uno sprovveduto: vista la sua passione per gli archivi, qualche computer deve maneggiarlo; *Anteprima* viene erogata via [Mailchimp](https://mailchimp.com), una piattaforma professionale, facile da approcciare quanto impegnativa per farci un prodotto editoriale in abbonamento pagato; organizza stanze su [Clubhouse](https://www.joinclubhouse.com). È consapevole. In più Anteprima è lunghissima, tutti i giorni, e tra copiaincolla e scrittura originale richiede uno sforzo significativo di composizione di testo. Lui scrive davvero e tanto.

Ieri Anteprima si concludeva così:

>Word è il peggiore programma di computer sulla piazza.

È null’altro che una opinione e può essere una opinione sbagliata. Quello che mi colpisce è che sia l’opinione di un giornalista che vive scrivendo migliaia di parole al giorno, tutti i giorni, da molti anni. Non ho idea del perché formuli il giudizio; però, se un programma nato per scrivere fallisce nell’intercettare i bisogni di uno che vive da sempre di scrittura, qualche domanda bisognerebbe porsela.

Ho sempre in cucina la mia enciclopedia del perché non usare Word, che devo ancora finire e prima o poi ci arriverò. Sono sicuro che Dell’Arti intende zero di quello che sto scrivendo io e ha altre ragioni. Ulteriori. Bisognerebbe porsi anche più di qualche domanda.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*