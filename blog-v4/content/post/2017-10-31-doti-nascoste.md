---
title: "Doti nascoste"
date: 2017-10-31
comments: true
tags: [Mac, Safari, McElhearn]
---
Un dicembre fa Kirk McElhearn scriveva su *Macworld* [Per Safari è tempo di mettersi a dieta con la memoria](https://www.macworld.com/article/3148256/browsers/it-s-time-for-safari-to-go-on-a-memory-diet.html):

>Una delle cose utili da considerare per migliorare le prestazioni complessive di Mac sarebbe guardare Safari e trovare modi per limitare il suo utilizzo, spesso eccessivo, della Ram.

Passato quasi un anno, [ritorna sull’argomento](https://www.kirkville.com/apples-safari-web-browser-now-uses-much-less-memory/) dal suo blog *Kirkville*:

>Dall’arrivo di High Sierra, ho notato che l’utilizzo di memoria da parte di Safari è diminuito di un bel po’.

Se ne parli sembra un dettaglio da niente: sai, Safari consuma molta meno memoria. L’altro sbuffa, scrolla le spalle, sì vabbè, ma non ti cambia la vita.

Invece, specie se ci lavori, cambia. Solo che questi particolari sono poco strombazzati e ci vuole discrezione, soprattutto nella testa, per apprezzarli.

Buona vigilia di Ognissanti a tutti. Farò un giro su [Spooky Runes](https://itunes.apple.com/it/app/spooky-runes/id793200812?l=en&mt=12).