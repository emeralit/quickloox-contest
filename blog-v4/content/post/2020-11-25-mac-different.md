---
title: "Mac Different"
date: 2020-11-25
comments: true
tags: [Mac, M1, Gruber, Silicon, Apple, Silicon, Windows, Arm, Daring, Fireball]
---
Un altro segnale che qualcosa è cambiato ed è cambiato molto? John Gruber ha scritto su *Daring Fireball* un [commento ai nuovi Mac con processore M1](https://daringfireball.net/2020/11/the_m1_macs) che gli ho chiesto eccezionalmente di poter tradurre per intero, da quanto tocca e ispira.

Non ho con lui un contatto personale diretto e immagino che il mio *tweet* si sia perso in mezzo a una tonnellata di altri. Peccato, lo avrei fatto volentieri nonostante la fatica – è roba bella lunga – in quanto non si leggeva una cosa del genere, non solo di Gruber, da molti e molti anni.

Con Apple Silicon è tornata una emozione, è tornato l’entusiasmo. Ancora una volta i pioli tondi nei fori quadrati hanno scosso lo *status quo* e difatti la conclusione del suo pezzo è questa, che molti hanno rimproverato ad Apple di avere dimenticato:

>Invero, Think Different.

*Different*, spiega Gruber all’inizio della lettura, è condizione necessaria perché si arrivi a *better*, migliore. La sua prima esperienza con le prime macchine uscite è stata così migliore che si è concesso una riga extra alla fine delle note a pié di pagina:

>Steve Jobs avrebbe fottutamente amato questi Mac M1.

Il senso generale della recensione è che i nuovi Mac mantengono tutte le promesse fatte da Apple e anche di più. Ci sono anche i difetti, non è certo un omaggio acritico. Dove Apple era attesa al varco, comunque, i risultati sono straordinari. Non posso tradurlo, ma posso certo citare qualche frase, a partire… dall’inizio:

>Migliore implica necessariamente diverso. È uno dei miei assiomi preferiti. Ma va dritto al cuore di una certa dissonanza cognitiva inerente all’umanità. Vogliamo che tutto diventi migliore. Eppure facciamo resistenza contro il cambiamento.

>I nuovi Mac […] sono quella specie di migliore che piega il pensiero. Per riconoscere quanto sono buoni – e sono qui per dire che sono clamorosamente buoni – devi riconoscere che certi presupposti su come andrebbero progettati i computer, su che cosa rende migliore un computer, su che cosa serve a un buon computer, sono sbagliati.

>Alcune persone negheranno per anni quello che Apple ha saputo raggiungere. Così va il mondo.

Dopo i dibattiti che ho letto in questi giorni sui social media, dell’ultima frase si potrebbe fare l’epigrafe per un monumento equestre a Gruber, tanto è epica.

Non mancano i riferimenti tecnici. La batteria dura quanto dichiarato. un MacBook Pro 13” M1 non scalda, se non un po’ sopra la Touch Bar quando viene sollecitato. Altrimenti, neanche si intiepidisce. Il sistema di raffreddamento (guai a chiamarlo ventola, è come chiamare volante un sistema di trasmissione) non entra mai in funzione o, meglio, se lo fa, non lo si sente.

>Nessun portatile Intel con prestazioni vagamente paragonabili può uguagliare quel silenzio. Se per te il rumore è un fattore importante, la partita è già chiusa.

>[La ragione per cui i Mac M1 sono più efficienti con meno Ram dei Mac Intel] è la combinazione di software e hardware progettati insieme. È il motivo per cui gli iPhone danno la birra anche alle ammiraglie Android, pur con molta meno Ram a disposizione.

Raccomando l’articolo. C’è tutto. Non c’è da preoccuparsi di Rosetta 2, l’emulazione Intel va perfino meglio dell’originale. Sedici gigabyte di Ram saranno sufficienti per la stragrande maggioranza delle persone senza che il sistema rallenti. La videocamera di bordo è migliorata, ma resta di qualità non eccelsa. L’opzione di lanciare app da iOS e iPadOS è interessante magari in potenza, ma ora è assai acerba. La chiusa:

>Si usava dire che nessuno è mai stato licenziato per comprare Ibm. Similmente, nessuno ha mai vinto una scommessa contro Intel e l’architettura x86. Intel ha sempre vinto. Fino a ora, quando è chiaro che ha perso. Le due precedenti transizioni di architettura sono state effettuate da posizioni disperate: Mac saltava fuori da navi che affondavano. Questa transizione è stata chiaramente effettuata da una posizione di forza schiacciante. […]

Nessuno cita più l’impossibilità di essere licenziati per avere comprato Ibm. Presto, nessuno penserà più che qualunque scommessa contro Intel e x86 sia persa.

Chi ripete cose già dette e già sentite, in questa situazione, sarà un perdente. *Think different, invero*.