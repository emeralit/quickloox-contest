---
title: "Un’esperienza spaziale"
date: 2024-01-31T00:15:55+01:00
draft: false
toc: false
comments: true
categories: [Internet, Software]
tags: [Vision Pro, Siracusa, John Siracusa]
---
John Siracusa ha scritto una [lectio magistralis](http://hypercritical.co/2024/01/30/spatial-computing) a proposito dello *spatial computing* che corrisponde secondo Apple all’esperienza dell’utilizzatore di [Vision Pro](https://www.apple.com/apple-vision-pro/), in arrivo tra due giorni.

Sono parziale nel giudizio perché l’illustrazione di apertura è la schermata *Info su* del Finder 1.1g, anno millenovecentoottantaquattro. Per me si potrebbe anche chiudere qui.

Scherzi a parte, il suo punto di vista è sommamente interessante, e stranamente lo è anche se non ha ancora messo le mani sull’apparecchio: si vede la differenza di statura con chi vide iPad presentato in streaming e [annunciò che temeva potesse ribaltarsi a causa del retro bombato](https://macintelligence.org/posts/2019-01-26-lo-spessore-dellanniversario/).

Sarcasmo a parte, la sua tesi è che Vision Pro sia in realtà il meno *spaziale* dei computer che lo hanno preceduto, segnatamente Macintosh e iPhone. In quanto entrambi hanno sfruttato i nostri millenni di evoluzione per risolvere i problemi di interfaccia attraverso comportamenti basati sulla manipolazione diretta degli oggetti, prima con il mouse (indiretta, ma senza precedenti nel mercato di massa) e poi davvero direttamente con il *touch*.

Vision Pro non ha niente del genere, almeno per ora; uno dei gesti base della sua interfaccia consiste nel toccare pollice e indice e così agire sull’oggetto selezionato nello spazio virtuale, con il quale non c’è alcun collegamento percepibile.

Sarà sufficiente? Il resto dell’esperienza compensa questa situazione? Manca ancora un salto tecnologico prima di chiudere il cerchio? Tastiera e puntatore pilotato fisicamente rimarranno essenziali anche se il resto si è dematerializzato?

Sono tutte domande aperte. Lui è felice di vedere che l’evoluzione dello *spatial computing* continua, non da dopodomani ma dal 1984. Lettura raccomandatissima.