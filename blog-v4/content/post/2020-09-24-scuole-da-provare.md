---
title: "Scuole da provare"
date: 2020-09-24
comments: true
tags: [Bezos, Amazon]
---
*Che ci azzecca*, avrebbe detto qualcuno, Amazon con la scuola materna? Eppure Jeff Bezos [ha annunciato l’apertura](https://www.instagram.com/p/CFcMd_RnQ_I/) di una serie di asili destinati a bambini svantaggiati tra i tre e i cinque anni, i cui genitori pagheranno come retta zero.

La foto suggerisce un ambiente piacevole e curato; il testo non va oltre il ringraziamento al team che ha realizzato il primo progetto, a [Des Moines](https://www.desmoineswa.gov), nello stato di Washington.

Ci sarà chi la vede come una inaccettabile intrusione delle multinazionali nel mondo dell’istruzione. Io la vedo come una scuola materna in più a disposizione, destinata a bambini che altrimenti non la avrebbero.

Un commento di Bezos è particolarmente divisivo: *il bambino è il cliente*. La si può leggere in due modi, uno estremamente negativo e uno persino rivoluzionario: una scuola che mette il bambino al centro delle sue attenzioni. Invece dei precari, invece degli scioperi, invece dei concorsi, invece dei bandi, invece dei trasferimenti a casa, invece dei certificati medici compiacenti.

D’altronde, come si fa a lasciare gli insegnanti con uno schermo solo?

In realtà questo è il secondo argomento. Tra virus, digitale e lezioni remote, per lavorare gli insegnanti oggi hanno bisogno di buoni schermi. Un piccolo gruppo di lavoro nei dintorni di Seattle ha avuto un’idea e ha fondato [Two Screens for Teachers](https://www.twoscreensforteachers.org). Lavoreranno molto meglio, è stato il pensiero, se possono scegliere come disporre ragazzi in remoto, lezione, risorse eccetera *tra due schermi* anziché uno solo, magari quello di un portatile.

Hanno dunque messo a punto un sito di incontri: tra insegnanti che si iscrivono per chiedere un monitor extra a donatori che offrono schermi fisici oppure, appunto, donazioni di denaro per arrivare all’acquisto.

Hanno come obiettivo arrivare a duecentocinquantamila monitor distribuiti entro Natale. Qualche utilità sembrerebbe esserci. Se quella di prima era una ingerenza inaccettabile di multinazionale, questa è una iniziativa no-profit. Come è stata messa in piedi da cinque americani, potrebbe essere messa in piedi uguale uguale uguale da tre italiani.

Quale sarebbe il problema insito nel replicare queste esperienze in Italia? Nel Paese le cui scuole possono mancare di carta igienica, non ci sono abbastanza aule, qualunque pensiero critico termina nell’accusare il governo di tagli alla scuola, effettuati in verità da tutte le amministrazioni centrali da quarant’anni a questa parte, e nella richiesta sistematica di più personale come se fosse una soluzione non si sa a che cosa. Aiutare centomila maestri, o anche solo mille, a tenere lezioni migliori grazie a un secondo schermo è sconsigliabile? Disdicevole? Diseducativo? Troppe comodità tutte insieme?