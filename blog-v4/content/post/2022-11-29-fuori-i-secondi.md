---
title: "Fuori i secondi"
date: 2022-11-29T16:53:00+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [BBEdit, Ventura, macOS, Shortcuts, Comandi rapidi, Clippings]
---
Per inserire data e ora nelle intestazioni dei file del blogghino mentre scrivo da iPad uso [un Comando Rapido](https://macintelligence.org/posts/2022-08-11-datemi-una-data/) che legge e copia in memoria le corrispondenti informazioni provenienti da macOS.

Su Mac, il mio BBEdit ha un Clipping contenente le variabili che il programma usa per identificare le stesse informazioni e associato a una combinazione di tastiera. Aziono la combinazione da tastiera e mi ritrovo data e ora a posto.

Da quando è arrivato Ventura, però, il comando non raccoglie più i secondi. Funziona come prima, solo che invece di inviare 16:43:28 invia 16:43.

[Fuori i secondi](https://www.youtube.com/watch?v=JH4VEkI5km4), BBEdit.

Guardo nel manuale e scopro che usavo la variabile interna *#time#*, probabilmente trovata in qualche esempio. Ecco, con Ventura si crea il problema.

Una scorsa alla sezione Placeholders and Include Files del manuale (guardare il manuale di BBEdit è impegnativo ma riconcilia con il mondo) e arrivo a usare *#LOCALTIME %T#*. Problema risolto, BBEdit sa tutto e a tutto provvede, l’utente è soddisfatto.

Questo per soddisfare l’ampio pubblico che notoriamente scrive tutti i giorni pezzi di codice per visualizzare un orario in BBEdit. Ritrovare i secondi sembra poco e invece l’universo ritrova una direzione concordata, a dispetto di Ventura.

<iframe width="560" height="315" src="https://www.youtube.com/embed/JH4VEkI5km4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>