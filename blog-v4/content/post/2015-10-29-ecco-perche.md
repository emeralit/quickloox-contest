---
title: "Ecco perché"
date: 2015-10-29
comments: true
tags: [Android, Authority, iPhone, 6s]
---
Ho lanciato su El Capitan una copia di InDesign CS5, a dire poco vecchiotta. Il sistema mi ha fatto installare un pezzo vecchiotto di Java e ho continuato a lavorare.<!--more-->

[Vienna](http://www.vienna-rss.org) ha iniziato a chiudersi da solo dopo il lancio. Console ha mostrato che c‘erano problemi con il database. In rete ho trovato [istruzioni voodoo](http://forums.cocoaforge.com/viewtopic.php?t=26313) per ricostruire il database con comandi Sqlite3. Intrigante, solo che avevo fretta: inquadrato il file del database, ripristinata una sua copia da Time Machine. E ho continuato a lavorare.

Lo sapevo nella teoria, ma me ne sono reso conto ora anche nella pratica: Mail mostra nei messaggi un pulsante per aggiungere o aggiornare una voce ai contatti. Basta un clic e funziona. Mi sono sempre ostinato a tenere aggiornati i contatti e questa funzioncina, per un secondo, mi cambia la vita ogni volta che la uso. Dopo di che continuo a lavorare.

Ecco perché uso Mac: occuparsi degli imprevisti può anche essere semplice. E poi continuo a lavorare.