---
title: "Le lunghe nottate"
date: 2017-03-07
comments: true
tags: [Matias]
---
Arriverà solo a giugno, la [tastiera wireless retroilluminata di Matias](http://www.matias.ca/aluminum/backlit/).

È una tastiera *full size*, forse un po’ anacronistica per i tempi odierni, però probabilmente è solo questione di recuperare le antiche abitudini.

Si apparenta via Bluetooth a un massimo di quattro apparecchi.

Ha due batterie ricaricabili, una per la tastiera (un anno) e una per la retroilluminazione (da una a due settimane). Geniale.

Gli manca una Touch Bar, ma non è che si possa pretendere.

*9to5Mac* ha provato il modello già esistente, uguale tranne che per la retroilluminazione. Ne [dice bene](https://9to5mac.com/2017/03/02/matias-wireless-aluminum-keyboard-review-mac-ipad-video/).

Quella retroilluminazione sembra un’amica ideale per certe lunghe notti d’estate che sono lunghe lo stesso, a dispetto dell’ora legale e della posizione del Sole nell’emisfero boreale.

Adesso devo solo spostarmi il compleanno di un semestre.