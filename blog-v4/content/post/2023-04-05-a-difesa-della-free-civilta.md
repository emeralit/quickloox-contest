---
title: "A difesa della free-civiltà"
date: 2023-04-05T09:55:43+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [FreeCiv]
---
Ci eravamo [lasciati in sospeso un anno e mezzo fa](https://macintelligence.org/posts/2021-11-14-non-cè-tre-senza-beta/) sulla questione della possibile compatibilità Mac della versione 3.0 di FreeCiv e se ne può annunciare la conclusione positiva: [dal 20 marzo è ripresa la produzione della versione Mac](http://www.freeciv.org).

Agli ostinati e agli ingegnosi non è mai mancata veramente la possibilità di giocare, grazie anche alla versione solo web del programma; chiaro che una app da far partire con un normale doppio clic è tutt’altro, se parliamo di esperienza utente.

Ho perso l’abitudine di giochicchiare con FreeCiv ultimamente, per somma di priorità altre, ed è proprio grazie all’averla nativa se faccio un pensierino a ricominciare. In ogni caso FreeCiv resta un simbolo: di software di qualità senza essere software costoso, delle capacità della comunità degli sviluppatori liberi, di giochi ben fatti senza essere per forza quelli imperanti, persino della ribellione insita nel chiudere tutto e perdere, per modo di dire, un quarto d’ora a tessere strategie in un mondo immaginario privo di ritorni e di scadenze.

Il gioco ha un valore sociale e culturale, oltre che di crescita del sé, molto sottovalutato e che si sta un po’ perdendo, per via del fatto che deve essere un gioco anche di qualità; massimo rispetto per chi si siede in autobus e accende Candy Crush Saga, solo che non è gioco; è passatempo. La differenza l’ho imparata durante il servizio di leva e non è il caso di raccontare come; basti dire che esiste ed è fondamentale.

Forza Freeciv e un giorno magari si organizza pure un torneo. Non sai giocare? Chi se ne frega, all’inizio capisci niente, poi man mano impari. Appunto, è un gioco e non c’è migliore piattaforma di un gioco per imparare senza rischi.