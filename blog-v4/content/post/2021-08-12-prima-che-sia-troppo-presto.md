---
title: "Prima che sia troppo presto"
date: 2021-08-12T00:15:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Claudio, Chiara, Word, Html, Lidia, Office] 
---
Scrivo in risposta a **Claudio**, molto gentile nel dirmi che a suo parere insegnare l’Html ai bambini è troppo e troppo presto.

Può certamente darsi che sia troppo presto. Mi figuro studenti che arrivano alla terza media capaci di strimpellare Html come farebbero con uno strumento se avessero trovato un buon insegnante di musica, non dei webmaster. In terza elementare l’obiettivo è chiaramente molto più limitato.

Ma posso dire che in prima elementare ci sono tanti esercizi per leggere e scrivere correttamente in italiano. Si imparano le maiuscole e le minuscole, le accentate, le pronunce, i tempi verbali. Si passa dallo stampatello maiuscolo allo stampatello minuscolo, intanto che si prepara il terreno al corsivo.

In prima elementare, quindi, si gettano le basi della tipografia. Non ci vedo niente di strano nel gettare le basi della marcatura del testo in terza.

Non per scrivere per forza testo marcato – l’osservazione che la maggior parte di loro non metterà mai un tag in tutta la sua vita è corretta – ma per sapere come funziona. Esattamente come a me, certo più avanti, venne spiegato come calcolare le radici quadrate manualmente. Non ho mai calcolato una radice quadrata a mano fuori dal quaderno di matematica, ma vorrei erigere un monumento a chi mi ha mostrato come fare e come funzione una radice quadrata.

A scuola non si insegnano gli strumenti, si insegna la materia. Me ne sono reso conto quando grazie ai buoni uffici di **Chiara** sono stato a parlare di nuove tecnologie nel suo liceo artistico.

Sono arrivato con un discorso preparato all’uopo, quando i docenti mi hanno mostrato con orgoglio che gli studenti di quinta lavoravano al nascente sito dell’istituto.

Gli studenti di quinta, liceo artistico, scrivevano il testo di una pagina in Word. Poi salvavano in Html, dentro una cartella del computer che raccoglieva il lavoro.

Buttai all’aria il programma del mio intervento. Chiesi a tutti di chiudere Word e di aprire, erano PC, il Blocco Note. Gli feci scrivere a mano una piccola pagina Html, con Title, Head, Body, il minimo assoluto. Gli chiesi di salvare, di aprirla con il browser. Mostrai l’effetto che i tag avevano sulla resa grafica nel browser e l’effetto che *non* avevano (ovviamente ignoravano che esistesse un tag Head e a che servisse).

Poi aprimmo nel Blocco Note uno dei loro file Word salvati in Html. Era pieno di cose incomprensibili a loro e anche a me. Chiesi retoricamente perché avessero scelto di usare il tag chisiricordaquale. Mi dissero che non erano stati loro a scegliere, lo aveva fatto Word. *Il sito della scuola vorreste costruirlo voi o lasciare che ci pensi Word al posto vostro, senza sapere che cosa fa?*

Mi si dirà sì, ma stai parlando di maturandi, tutt’altra età, tutt’altro contesto.

Intanto era il 1998. Come si intuisce, di Html nella scuola si cominciava appena a parlare. Erano i maturandi perché non poteva essere prima di così.

Secondo, avanti veloce, nel 2023 inizieranno a insegnare Office a mia figlia in terza elementare.

In linea di principio, Lidia potrebbe ritrovarsi nella stessa identica situazione di quei maturandi sventurati del 1998.

Tra parentesi: spero che nessuno dei poveracci abbia deciso di scegliere una carriera di web designer, partendo da Word come strumento professionale. O è disoccupato, o ha cambiato lavoro, o ha dovuto imparare la sera quello che avrebbero dovuto insegnargli a scuola la mattina.

Ho la folle speranza che per mia figlia possa essere diverso e che, venticinque anni dopo, possa sapere che cosa sta facendo, invece di farlo e basta.

Office a scuola è sempre e comunque troppo presto. Se lo sia anche insegnare i rudimenti dell’ipertesto non so, può darsi. Ma sarebbe un errore più benefico di quello di sedersi su Word e chiamarlo insegnamento.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               