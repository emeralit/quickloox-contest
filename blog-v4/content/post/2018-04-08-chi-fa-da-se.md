---
title: "Chi fa da sé"
date: 2018-04-08
comments: true
tags: [microLed, Apple, iPhone, watch, Ive]
---
Sillogismo: Apple preferisce controllare ogni anello della catena di produzione; Apple apre una fabbrica di microLed; ergo, Apple vuole produrre da sé i microLed che alla lunga sostituiranno gli Oled sugli schermi di iPhone, watch e magari altro.<!--more-->

Sbagliato e [lo spiega bene Ben Lovejoy sulle pagine di 9to5Mac](https://9to5mac.com/2018/03/19/apple-microled-production-reasons/).

Apple vuole avere il primato tecnologico; gli Oled di iPhone X sono superiori a quelli su altri apparecchi, tant’è vero che li produce Samsung e fatica a tenere dietro alle specifiche.

Poi vuole avere la segretezza e poi, punto chiave, Jonathan Ive ha detto più volte che un buon *design* si ottiene anche facilmente; è più difficile avere un *design* facilmente producibile su scala industriale, magari in milioni di esemplari.

Apple ha in realtà aperto un avamposto di frontiera, dove toccare con mano le nuove tecnologie, riuscire a dominarle meglio dei concorrenti e sapere bene che cosa chiede (e può chiedere) ai fornitori. Ci saranno sempre fornitori esterni per gli schermi di iPhone, o almeno per una generazione.

Quei fornitori, però, dovranno produrre componenti con specifiche che nessun altro o quasi. Ecco dove sta la differenza e perché non è vero che i componenti sono tutti uguali.