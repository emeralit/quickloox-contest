---
title: "Eravamo tre amici alla Touch Bar"
date: 2022-05-31T00:21:34+01:00
draft: false
toc: false
comments: true
categories: [Internet, Web]
tags: [A2, Avvocati e Mac, Filippo, Strozzi, Filippo Strozzi, Roberto, Marin, Roberto Marin, Markdown, LaTeX, Avvocati e Mac, MArCh]
---
Spiacente, per stasera niente contenuti sapidi, caustici, ironici o investigativi. Sono stanco morto, di quella stanchezza rinfrescante e lenitiva che arriva dopo lo sport e dopo altre cose belle della vita. Come partecipare a un podcast.

Abbiamo cominciato a chiacchierare un quarto d’ora prima dell’inizio e, un quarto d’ora dopo l’orario, c’erano i soliti amiconi a fare le battute su YouTube visto che non ci decidevamo a iniziare. Avevamo già cominciato e quanto non è rientrato nella registrazione avrebbe potuto tranquillamente.

**Filippo** e **Roberto** sono stati padroni di casa eccellenti e, parlo per me sperando di poter coinvolgere loro, ci siamo divertiti. Nel frattempo abbiamo parlato di una milionata di cose attorno a Mac, iPad, Apple, Apple Pencil, la scuola italiana, l’open source e via discorrendo.

È partito qualche aneddoto, sicuro, ma eravamo vivi; niente di paragonabile a certi podcast rococò dove la parola d’ordine è *tutto, basta che sia di un tempo lontano*.

Chi non ha ascoltato in [diretta su YouTube](https://www.youtube.com/watch?v=eMKVhCrSTRk) potrà sentire la chiacchierata editata, ripulita, riordinata, mi dicono a inizio luglio. Naturalmente mi farò parte diligente nel segnalarlo.

[A2](https://www.avvocati-e-mac.it/a2-pod), l’architetto e l’avvocato, li conosco da un po’ per resa testimonianza; ho sempre letto con avidità e invidia [Avvocati e Mac](https://www.avvocati-e-mac.it), dove si impara a cavarsela con [LaTeX](https://www.latex-project.org) e [Markdown](https://www.markdownguide.org) in uno studio legale e, se ci si riesce lì, e non stai usando LaTeX, vuol dire che sei su una cattiva strada o se non altro sprechi un’occasione; ho sempre tenuto [MArCh](https://marchdotnet.wordpress.com) nella lista dei blog buoni come quelli capaci di approfondire una professione (segnatamente, l’architettura) e intanto progredire nella conoscenza dell’ecosistema Apple, niente fine a se stesso, zero utilitarismo o aridità, calore umano e gentilezza a mille.

Ho appena finito di passare una serata davvero energizzante con loro e sarò ben lieto di farne altre se e quando avranno voglia e tempo. Il titolo potrebbe essere quello di questo post, se va bene un gioco di parole e se qualcuno [ricorda ancora la Touch Bar](https://macintelligence.org/posts/2016-11-05-mamma-guarda-due-mani/), naturalmente.

Persone preziose, da ringraziare per addormentarsi con la sensazione di una giornata compiuta.