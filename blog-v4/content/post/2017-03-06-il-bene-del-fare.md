---
title: "Il bene del fare"
date: 2017-03-06
comments: true
tags: [Notability, iPad, Twitter, font, Hoefler]
---
Facile di questi tempi guardare a Internet per i lati negativi: gli imbecilli scatenati, le post-verità, gli attacchi d’odio.

La rete potrebbe anche servire ad altro ed effettivamente è piena di quello che ci si riversa. Significa che l’unica vera strategia verso gli imbecilli è ignorarli e, invece, immettere nel traffico cose buone. Fare del bene. Anche solo fare una buona domanda, come questa:

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Hey <a href="https://twitter.com/HoeflerCo">@HoeflerCo</a>! I’ve started on this typeface and would love some pointers. <a href="https://t.co/8rSe9PIXZs">pic.twitter.com/8rSe9PIXZs</a></p>&mdash; Louie Mantia (@mantia) <a href="https://twitter.com/mantia/status/835337375559516161">February 25, 2017</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

>Ehi, signori di Hoefler, ho cominciato a lavorare su questa famiglia di caratteri e mi piacerebbe qualche osservazione.

(Tipo *Scusate, lì in Ferrari c’è qualcuno che mi dà un consiglio sul motore che ho iniziato a progettare?*).

Facile di questi tempi guardare a iPad perché le vendite sono in flessione, perché non dà l’accesso al filesystem, perché la tastiera non consente ancora il pieno governo della macchina e iOS è tuttora immaturo a questo riguardo eccetera.

Con un iPad si può fare moltissimo e l’unica vera strategia è guardare a raggiungere nuovi obiettivi, invece che ostinarci a voler applicare su iPad i nostri automatismi sviluppati con Mac.

La risposta al *tweet* è arrivata da Hoefler con una serie di osservazioni appuntate a penna sul font in lavorazione.

Appuntate con Apple Pencil, tramite [Notability](https://itunes.apple.com/it/app/notability/id360593530?mt=8).

La domanda, la risposta, la velocità della risposta, l’efficacia dei consigli, l’eleganza del risultato: tutto questo, in questa situazione, profuma di bravura e saper fare. Saper fare bene.

Porto a casa due belle lezioni, una su iPad e una su come guardare alla rete.