---
title: "Le trasmissioni riprenderanno"
date: 2014-01-30
comments: true
tags: [Mac]
---
A causa di una congiunzione astrale tra un viaggio, una riparazione Mac e un paio di installazioni che non ho esattamente avuto il tempo di fare, è possibile che non appaiano nuovi post o che la pubblicazione risulti erratica da qui a martedì.<!--more-->

Non dipende interamente da me e quindi posso solo auspicare che tutto vada per il meglio.

Non c’è alcun problema e appena terminati viaggi, riparazioni e installazioni tornerà tutto alla normalità. Nel frattempo mi scuso per l’inadempienza. :-)