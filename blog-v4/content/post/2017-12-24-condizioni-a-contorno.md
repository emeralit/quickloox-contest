---
title: "Condizioni a contorno"
date: 2017-12-24
comments: true
tags: [Wesnoth, iOS, Natale]
---
La situazione equivoca di cui [parlavo un anno e mezzo fa](https://macintelligence.org/posts/2016-08-29-non-aiutero-battle-for-wesnoth-for-ios/) relativa a Battle for Wesnoth si è chiarita.

Ora su App Store sta una versione unica della app, aggiornatissima, in mano a chi si occupa dello sviluppo generale del gioco e non a carneade che abusano della facoltà di mettere mano su codice *open source*.

[Battle for Wesnoth per iOS](https://itunes.apple.com/it/app/battle-for-wesnoth-hd/id575852062?l=en&mt=8) costa 4,49 euro che vanno certamente a contribuire alle spese di mantenimento del progetto o a compensare sviluppatori volontari. Niente ambiguità, niente equivoci. Le condizioni a contorno sono proprio cambiate.

La butto lì. Se alla vigilia di Natale manca ancora il temibile pensierino per qualcuno provvisto di iPad, è un regalo intelligente e senza impegno che vale sei o sette volte il suo prezzo.

Intanto auspico per chiunque legga questi bit di poter passare questo tempo pensando a regali, amici, amori, parenti, affezioni in serenità e gioia. Negli ultimi mesi mi è capitato per ragioni di vita e lavoro di arrivare a fine giornata senza un *post*; in quei momenti vado a letto con la sensazione dell'incompletezza. Il vero appagamento è essere in compagnia e Natale possa essere per ognuno l'epitome dell'appagamento.

Auspici positivi e a risentirci ogni giorno, chi vuole.
