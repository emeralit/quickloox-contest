---
title: "Domanda e pretese"
date: 2017-02-04
comments: true
tags: [iPhone]
---
Nel trimestre natalizio iPhone ha superato se stesso con oltre settantotto milioni di unità vendute, oltre le previsioni di chiunque a cominciare dagli analisti, nove scontrini al secondo, un volume (fisico) di affari pari a quello di [seicentotrenta carri armati](http://www.zdnet.com/article/how-many-iphones-did-apple-sell-every-second-during-the-last-quarter/). È tornato a essere la marca di computer da tasca più venduta al mondo, davanti a Samsung. Non se ne erano mai venduti così tanti, a dispetto di chi descrive il mercato come saturo.

iPhone criticatissimo per avere osato [rinunciare al jack audio](http://www.theverge.com/a/apple-iphone-7-review-vs-iphone-7-plus) e per [costare troppo](https://www.bloomberg.com/news/articles/2017-01-26/apple-s-iphone-sales-set-to-rebound-but-for-how-long), in media [quattro dollari in più](http://images.apple.com/pr/pdf/q1fy17datasum.pdf) che l’[anno scorso](http://images.apple.com/pr/pdf/q1fy16datasum.pdf). Ma a quanto pare il pubblico ha pensato diversamente. Abbiamo un nuovo insegnamento da scrivere sulle tavole, la legge di Evans: *le critiche dei tecnologi sono generalmente un buon controindicatore della domanda dei consumatori*.

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Highest-ever iPhone sales. Reminder: technologist complaints about Apple products are generally a good counter-indicator for consumer demand</p>&mdash; Benedict Evans (@BenedictEvans) <a href="https://twitter.com/BenedictEvans/status/826552171617357824">January 31, 2017</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Il bello è che l’assenza di jack audio, nei piani, doveva spingere gli [AirPods](http://www.apple.com/it/airpods/). I quali, un ritardo dopo l’altro, sono stati indisponibili per la gran parte dell’autunno, mandando in fumo le buone intenzioni. Il record magari poteva essere persino più alto.

Da ricordare sempre: la domanda apre il portafogli, le pretese sono sovente gratuite.
