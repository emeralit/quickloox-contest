---
title: "Un altro modo di vedere le cose"
date: 2019-11-21
comments: true
tags: [Scientific, American, Xdr, Hdr, Apple]
---
Citazione dal critico culturale tedesco [Walter Benjamin](https://www.theartstory.org/influencer/benjamin-walter/), risalente al 1915:

>Il modo in cui è organizzata la percezione sensoriale umana e il mezzo nel quale viene raggiunta sono determinati non solo dalla natura ma dalle circostanze storiche.

Ancora:

>In un mondo di produzione di massa di immagini, è la tecnologia a dettare le nostre aspettative visive.

Lo ricorda il *podcaster* tecnologico [Wade Roush](http://www.waderoush.com) a proposito di [Pro Display Xdr](https://www.apple.com/it/pro-display-xdr/). Un pezzo abbastanza anodino, ma di respiro abbastanza ampio da spiegare come Xdr stia per *Extreme Dynamic Range*, una condizione di visualizzazione che vuole andare oltre una tecnologia evoluta e recente come Hdr, [High Dynamic Range](https://www.eizo.it/nozioni-pratiche/know-how-sul-tema-monitor/informazioni-sullhdr-di-che-cosa-si-tratta/), già diffusa su qualunque computer da tasca degno del nome.

I Led dietro lo schermo di Pro Display Xdr proiettano luce blu, non bianca, spiega Roush. La luce blu può essere emessa da un singolo chip e quindi controllata con più precisione; nel caso del monitor Apple, racconta uno degli ingegneri che ci ha lavorato, la luce blu attraversa un substrato di correzione cromatica e poi subisce una *trasformazione quantistica* che genera tutti i colori dello spettro ottenibile.

Qualcuno è in grado di programmare app o un sistema operativo per Pro Display Xdr usando Pro Display Xdr come piattaforma di elaborazione? Ne dubito. Eppure, *è un computer*:

>Un nuovo controller di temporizzazione modula non solo i pixel Lcd ma le fonti di luce che gli stanno dietro, analizzando il contenuto e per esempio spegnendo i Led in tutte le aree che dovrebbero essere nere. “L’algoritmo contenuto nel controller applica una orchestrazione armoniosa del tutto”, spiega Vincent Gu [ingegnere Apple]. “Stiamo eseguendo molta computazione e intensa”.

Già, [qualcosa che computa è un computer](http://www.macintelligence.org/blog/2019/11/04/parole-in-liberta/).

L’articolo termina con una ultima citazione di Benjamin:

>L’ingrandimento di una istantanea non si limita a rendere più preciso ciò che era già visibile, per quanto poco chiaro: rivela formazioni strutturali del soggetto interamente nuove.

Ecco, un altro modo di vedere Pro Display Xdr, diverso dalle battute sullo stand da 999 dollari e forse più producente.

Apparso sulle pagine di quella famigerata rivista di tifosi ciechi e acritici, [Scientific American](https://www.scientificamerican.com/magazine/sa/2019/09-01/).

 ![Apple’s Amazing New Screen, Scientific American settembre 2019](/images/sciam.jpg  "Apple’s Amazing New Screen, Scientific American settembre 2019") 