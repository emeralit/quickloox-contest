---
title: "Una gita fuori aeroporta"
date: 2020-02-12
comments: true
tags: [Alitalia, Londra, Learning, Technologies]
---
L’adiacenza del centro congressi [ExCel](https://www.excel.london) all’aeroporto [London City](https://www.londoncityairport.com/) mi permette di partecipare a [Learning Technologies](https://www.learningtechnologies.co.uk) con un viaggio in giornata.

Ci fosse qualcuno in zona, un caffè salterebbe fuori graditissimo.

Sono stato a Londra quasi sempre da turista più che per business e questa occasione sarà interessante per vedere i Docks ristrutturati. City airport, metropolitana leggera, infrastrutture varie, [app di ordinanza](https://apps.apple.com/it/app/learning-technologies-2020/id1496587989?l=en) per la manifestazione;  Brexit o meno, questa parte del mondo è Europa evoluta e si muove rapida.

Mi trovo su una corriera che, alle 4:47, aveva L’orologio impostato sulle 8:29, probabilmente per confortare il passeggero. Ma in fondo sto anch’io raggiungendo un city airport e, a questo ritmo, le mie figlie ci arriveranno in metropolitana.
