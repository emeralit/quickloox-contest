---
title: "Una cosa per volta"
date: 2015-08-27
comments: true
tags: [iOS, iPhone, iPad, Mac, Gmail, iCloud]
---
Prendo atto che, in una situazione di connessione vergognosa, sono riuscito a usare Gmail in versione web (e in versione base, solo Html senza magie varie), mentre per la posta iCloud non c’è stato verso.<!--more-->

Da questo punto di vista, a portare la salvezza sono iPhone e iPad. Escludere tutti i servizi che consumano rete su Mac è un’impresa impegnativa, senza contare che una volta tornati nella civiltà tocca rifare tutto a rovescio; su un apparecchio iOS, che fa una cosa per volta e poco più, le cose sono già a posto o quasi e si può leggere la posta anche se i dati sono un rivoletto più secco di certi rigagnoli a mare, definiti con sprezzo del ridicolo *rii* o *torrenti* nella toponomastica locale.