---
title: "Tredici per tredici"
date: 2014-01-29
comments: true
tags: [iPhone]
---
Se qualcuno dovesse ancora essere convinto che viviamo in un mondo straordinario, dovrebbe guardare [una pagina pubblicitaria del 1991](http://www.trendingbuffalo.com/life/uncle-steves-buffalo/everything-from-1991-radio-shack-ad-now/) firmata dalla catena americana di elettronica di consumo Radio Shack.<!--more-->

L’autore dell’articolo che la riproduce, Steve Cichon, ha sommato i prezzi degli articoli presentati: 3.054,82 dollari del 1991, oggi – dice – grossolanamente equivalenti a 5.100 dollari.

I tredici oggetti elencati, tredici anni dopo, sono brillantemente sostituiti da un iPhone e relative *app*, a una frazione del prezzo.

Avanzano un *radar detector* e un altoparlante a tre vie, ma per il resto non c’è male.