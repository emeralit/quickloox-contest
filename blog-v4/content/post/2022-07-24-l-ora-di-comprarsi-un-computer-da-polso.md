---
title: "L’ora di comprarsi un computer da polso"
date: 2022-07-24T01:22:30+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Apple Watch]
---
Si è tirato davvero tardi nella spiaggia sotto la collina e abbiamo risalito il sentiero di ritorno con molto buio.

Ho illuminato i punti critici del sentiero con watch. All’inizio ci ho provato per scherzo; poi funzionava.

Durante la serata, abbiamo giocato a individuare qualche costellazione, con l’aiuto di [Night Sky](https://apps.apple.com/it/app/night-sky/id475772902?platform=iphone) (livello gratuito).

A un certo punto, su watch mi è arrivata la notifica di un passaggio della Stazione spaziale internazionale.

L’ho accettata e il computer da polso mi ha mostrato in realtà aumentata il tracciato dell’orbita, contro il cielo stellato.

Alle medie, quando qualcuno chiedeva *che ore sono*, lo si prendeva in giro rispondendo *ora di comprarsi un orologio*.

Non capisco veramente per quale ragione si dovrebbe avere un orologio, nel 2022. Sul polso voglio un computer e pure potente.