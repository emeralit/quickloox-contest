---
title: "Una locazione per volta"
date: 2022-05-13T00:01:38+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [MacStories, Automation April, Comandi Rapidi, Shortcuts, Viticci, Federico Viticci, Smiling Isle]
---
La farò cortissima: *MacStories* [ha infine pubblicato i vincitori del suo concorso Automation April](https://www.macstories.net/stories/introducing-the-2022-automation-april-shortcuts-contest-winners/), di cui [abbiamo parlato in precedenza](https://macintelligence.org/posts/2022-04-18-la-settimana-automatizzata/), ed è tutto da leggere, vuoi perché qualcosa può veramente tornare utile, vuoi perché il fatto artistico gioca un ruolo fondamentale: sono arrivate centinaia di proposte e ciascuna si è misurata con i vincoli della piattaforma, per spingere oltre il limite di quello che è possibile fare con i Comandi Rapidi. La cosa più bella che [ha dichiarato Federico Viticci](https://twitter.com/viticci/status/1524770005623181318) è questa:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Don&#39;t miss the honorable mentions at the end: there&#39;s a text adventure game you can play in Shortcuts (yes) and a location check-in app that&#39;s actually a shortcut 🤯<br><br>I have no idea how people can come up with these shortcuts, which is exactly the point.<a href="https://t.co/3sUAox3NGd">https://t.co/3sUAox3NGd</a> <a href="https://t.co/p9mpAsQ5wN">pic.twitter.com/p9mpAsQ5wN</a></p>&mdash; Federico Viticci (@viticci) <a href="https://twitter.com/viticci/status/1524770005623181318?ref_src=twsrc%5Etfw">May 12, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

>Non ho idea di come le persone possano arrivare a presentare Comandi Rapidi come questi, il che è esattamente il punto.

La mia preferenza va al Comando descritto qui sopra, che neanche ha vinto:

>C’è una avventura testuale giocabile con i Comandi Rapidi (sì).

Non so se scaricarla per giocarla, per smontare il giocattolo e imparare una milionata di cose su come maneggiare il testo nei Comandi Rapidi, o l’una e l’altra cosa.

È comunque entusiasmante vedere fermento, creatività, idee, originalità attorno a una piattaforma che i soliti noti vorrebbero chiusa, senz’anima, azzoppata da Apple per motivi biecamente commerciali e invece, ancora una volta, catalizza ingegno e genialità.

Non l’ho fatta cortissima come promesso; è che più ne scrivo più mi piace. Adesso però è ora di guardare [Smiling Isle](https://www.icloud.com/shortcuts/711095b186ab448bbf49392746d7890c), avventura costruita con i Comandi Rapidi, distillando una locazione per volta da un unico lungo testo scritto in una sintassi apposita per…