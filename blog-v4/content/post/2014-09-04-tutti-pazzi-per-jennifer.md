---
title: "Tutti pazzi per Jennifer"
date: 2014-09-04
comments: true
tags: [iCloud, iBrute, phishing, AnonIB, Wired, Elcomsoft]
---
Qualche nota appena più approfondita sull’*affaire* delle (più o meno) celebrità messe a nudo in pubblico attraverso la violazione dei loro account, sia esso iCloud o altro.<!--more-->

Non è una cosa nata una settimana fa. Esiste una comunità oscura di gente che si ingegna per recuperare foto golose e fare scambio con altri “appassionati”. Esiste da anni e i loro sforzi sono continui e costanti. La comunità alla ribalta in questo momento è [AnonIB](http://anon-ib.com) (attenzione: è un *link* **per adulti**) e  ne esistono svariate.

iCloud è finito nel tritacarne principalmente per il fatto di consentire il backup dei dati di un iPhone o un iPad. Il backup è una misura di salvaguardia dei dati, ma diventa un *boomerang* se si perde il controllo dell’apparecchio backuppato. Se l’attricetta incappa in un *phishing*, se ha un cameriere furbetto che approfitta di un momento di distrazione o di una dimenticanza, se ha un fidanzato di pochi scrupoli, facile che i suoi dati di iCloud finiscano in possesso di non autorizzati e questi scarichino da iCloud nientemeno che un backup del telefono, quindi non solo foto ma anche dati. In modo tale che l’attricetta neanche se ne accorge. Così il gioco può andare avanti per mesi.

Se ho in mano l’iPhone di un personaggio che ha concesso miliardi di interviste, dichiarazioni, articoli, racconti sulla sua vita privata e sulle sue passioni, e ho cinque minuti liberi, provo a contattare il servizio iForgot. Che, per “ricordare” la password, mi proporrà le domande sulla sicurezza. Come si chiama il mio animale da compagnia preferito, dove sono andato a scuola, qual è stato il mio primo viaggio eccetera eccetera. Se sto cercando di indovinare la password di uno che passa per strada, di lui non so niente. Dell’attricetta, a questo livello so un sacco di cose. O almeno posso provare. Se non funziona fa niente, riprovo un’altra volta.

Wired [fa riferimento](http://www.wired.com/2014/09/eppb-icloud/) a Elcomsoft Phone Password Breaker, attrezzo software che dovrebbe essere usato solo per informatica forense e invece pare essere molto apprezzato per questo genere di operazioni.

iCloud c’entra, sì, ma solo come puro contenitore dei dati. Il problema sta nella perdita della password, non in una falla che consenta di entrare senza.

Parlando di *password*, Apple ha avuto una responsabilità forse ben maggiore di quanto trattato finora. Pochi giorni fa a un convegno di *hacker* è stato mostrato uno strumentino scritto in Python chiamato [iBrute](https://github.com/hackappcom/ibrute), basato su una dimenticanza relativamente grave: in certe circostanze, era possibile effettuare un numero potenzialmente infinito di tentativi di indovinare una password iCloud. Nessuno ha tempo infinito: però, tentare un attacco appoggiato a un dizionario di password tipiche poteva portare a violare account protetti da password molto deboli o sciocche. Indovinare che livello di sofisticazione di password può avere tipicamente un’attricetta molto esperta di *red carpet* ma pochissimo di *privacy*…

Anche in questo caso, è questione di iCloud fino a un certo punto: la debolezza nel sistema c’era – è stata chiusa di recente – ma una *password* di qualità non viene trovata da un attacco semplice. Una scarsa, invece, può capitolare. Per inciso, non ci sono prove che iBrute abbia effettivamente contribuito alla faccenda. È stato presentato ufficialmente pochi giorni prima dello scandalo; non c’era il tempo materiale di usarlo per accumulare il materiale che si è riversato in rete. A meno che fosse noto da tempo nei soli bassifondi, ma di ciò non vi è prova.

La sensazione è che Apple sia protesa da tempo verso una maggiore sicurezza dei propri sistemi e nei prossimi giorni vedremo probabilmente un maggiore utilizzo del TouchID attualmente presenta nel solo iPhone 5s. Servirebbero inoltre una autenticazione a due fattori più completa di quella usata (sull’esempio di quella di Google, che è ottima) e magari, [si è proposto](http://9to5mac.com/2014/09/03/opinion-after-the-celebrity-hacks-the-vulnerability-that-still-exists-and-what-needs-to-be-done/), inserire una modalità privata di scatto fotografico, che non entra a fare parte di Streaming Foto.

Il che rappresenta solo un palliativo e naturalmente, nel momento in cui una foto sta in un solo apparecchio, aumenta il rischio che quella foto vada perduta; senza contare il fatto che violare il telefono mette comunque a rischio.

Nel frattempo, di nuovo: i dati su Internet sono tanto più protetti quanto più forte è la *password*; gli unici dati sicuri, sicuri, sempre, sono i dati cifrati.