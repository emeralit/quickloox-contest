---
title: "Ich Bin Ein Savonese"
date: 2023-02-16T02:19:58+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Savona, All About Apple]
---
Il Comune di Savona ha pubblicato un [questionario](https://www.allaboutapple.com/2023/02/ciao-savonese-come-ti-senti-oggi/) riservato a nessuno, ovvero aperto a chiunque, dedicato alla conoscenza dei luoghi museali della città.

Effettivamente non ho ancora visitato la Pinacoteca e certamente lo farò.

Sì, ho visitato il museo All About Apple. Sì, più di una volta. Intendo tornarci. È un luogo straordinario perché animato da persone straordinarie.

Chiunque abbia visitato il museo All About Apple dovrebbe compilare il questionario, che impegna per pochi minuti e contiene domande facili. Il passaggio più complicato è quando viene chiesto il numero di musei a Savona. Non ne avrei la minima idea a essere sincero, solo che la domanda seguente li elenca tutti.

Sono certo che il Comune di Savona ricaverà una impressione positiva dell’importanza di All About Apple per la città e della sua rilevanza culturale, turistica, umana.