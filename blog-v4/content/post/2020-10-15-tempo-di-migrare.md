---
title: "Tempo di migrare"
date: 2020-10-15
comments: true
tags: [Mac]
---
Per quanto tenuto alla più assoluta riservatezza, una cosa la posso dire: la totalità dell’azienda passerà a Mac. Il numero di postazioni di lavoro interessate ha quattro cifre.

Se qualcuno vuole illustrarmi le terribili conseguenze che avrà questa decisione, sono tutto orecchi, perché proprio non le scorgo.