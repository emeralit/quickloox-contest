---
title: "La fine di Avadon"
date: 2016-09-17
comments: true
tags: [Spiderweb, Avadon, Warborn]
---
[Avadon 3: The Warborn](http://www.spiderwebsoftware.com/avadon3/) è finalmente definitivo e, in quanto [beta tester](https://macintelligence.org/posts/2016-06-12-anime-candidate/), posso dire che – se qualcuno vuole regalarsi magari per Natale un gioco di ruolo a turni che eccelle per trama e intrico più che per grafica ed effetti speciali – sono venti dollari che ne valgono molti di più.<!--more-->

Da tester posso dire che il gioco funziona bene: tutte le parti che ho giocato sono ineccepibili nel funzionamento e le ho giocate non tutte, ma la gran parte.

Va detto che giocarlo tutto è impossibile, o meglio servono più campagne. *Avadon 3: The Warborn* ha una trama veramente estesa, con un sacco di diramazioni secondarie e quella principale che spinge continuamente il personaggio giocante – e il giocatore – a dubitare della sua strategia e delle sue prese di posizione. Nonostante i nemici da sconfiggere sul campo, il vero cuore del gioco è una lotta di potere tra il vecchio che ancora ruggisce e il nuovo che avanza. A ogni angolo di strada bisogna schierarsi e prendere posizione. Il risultato finale cambia a seconda di quello che si è fatto o non fatto durante il gioco e non parlerò del finale per non rovinare la sorpresa. Dico solo che arrivare in fondo vale la pena, comunque vada a finire (io ho provato due finali, ma ce ne sono molti di più).

Ho apprezzato il ritmo costante del gioco, la capacità di non creare vicoli ciechi ma di tenere alta l’attenzione per trovare la soluzione al problema del momento, la varietà infinita di configurazione dei personaggi, dei loro poteri, degli oggetti che si possono trovare e delle possibilità che offrono. È un vero gioco di ruolo vecchia maniera. Al suo interno c’è una quantità di conoscenza enorme sul mondo e sugli eventi passati, che leggere tutta è davvero un impegno. I dialoghi sono spesso decisivi per capire che cosa fare e sono molto articolati, mai da sottovalutare. A chi piace immedesimarsi, calarsi nella realtà fittizia del mondo di fantasia, questo gioco piacerà.

A mani basse, è il migliore *Avadon* della serie e pare che stia andando bene, con vendite che l’autore definisce molto soddisfacenti. Se lo merita tutto.