---
title: "Al servizio di sua contabilità"
date: 2015-08-24
comments: true
tags: [Fabio, Microsoft, Expo, Flavio]
---
**Fabio** mi segnala l’eccellente lavoro svolto dai sistemi Microsoft in zona Expo.<!--more-->

 ![Microsoft eccelle a Expo](/images/aree-climatiche.jpg  "Microsoft eccelle a Expo") 

Come già [in altra occasione analoga](/blog/2015/08/03/aspettali-al-varco/), sarebbe interessante sapere quanto ha incassato l’azienda per la prestazione e quanto costa ai contribuenti ogni minuto di messaggi di errore.

 ![Ingrandimento del messaggio di errore](/images/consolle-exe.jpg  "Ingrandimento del messaggio di errore") 

Ha l’aria di essere un’attività redditizia: realizzare una schermata come queste dovrebbe costare davvero poco. Fatico a figurarmi il cliente, ecco.