---
title: "La luce del passato"
date: 2013-10-09
comments: true
tags: [iPhone]
---
Abbinamento tra un sito di [straordinarie foto scattate nelle Terre Alte scozzesi con un iPhone 5S](http://proof.nationalgeographic.com/2013/10/07/capturing-the-aura-of-the-scottish-highlands-with-the-iphone-5s/) e l’eccezionale racconto del *New York Times* sul [giorno in cui Steve Jobs annunciò iPhone](http://www.nytimes.com/2013/10/06/magazine/and-then-steve-said-let-there-be-an-iphone.html) a Macworld Expo di San Francisco, il 9 gennaio 2007.<!--more-->

Il laboratorio di dieci metri quadrati allestito dietro il palco. Gli iPhone programmati per mostrare sempre cinque tacche a video qualsiasi cosa succedesse. *Gli* iPhone, perché il software era provvisorio e nessuna delle unità funzionava abbastanza a lungo senza avere un problema. Per cui Jobs aveva più unità a disposizione e, quando una faceva i capricci, ne prendeva un’altra.

La torre cellulare portatile installata da At&T per avere una ricezione  forte. Le basi *wireless* truccate per operare su frequenze radio usate in Giappone e non negli Stati Uniti, in modo che nessuno in sala le trovasse. Tre diverse versioni preliminari di iPhone progettate tra il 2005 e il 2006. Sei diversi prototipi dell’iPhone finale, ognuna leggermente diversa dalle altre per hardware e software. Il primissimo prototipo di schermo sensibile al tocco multiplo realizzato… su un Mac. E un prototipo realizzato veramente come aveva scherzato Jobs sul palco, con i *chip* radio inseriti in un iPod che telefonava usando la rotella. E avanti così.

>Sei una superstar nel tuo ruolo attuale. Ho un altro progetto che vorrei tu prendessi in considerazione. Non posso dirti che cosa riguarda. Posso solo dirti che devi rinunciare a serate e fine settimana e lavorerai più duro di quanto tu abbia mai fatto nella vita.

E poi uno dovrebbe pensare ad Android perché costa meno. Voglio qualcosa che è stata realizzata da gente che ha dato il massimo, non che ha fatto economia. Finisce che nel giro di qualche anno arrivi a scattare grandi foto dalla Scozia, piene di luce.