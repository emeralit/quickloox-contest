---
title: "Un posso falso"
date: 2016-08-31
comments: true
tags: [Apple, EU, Irlanda, tasse]
---
Riassumo: un quarto di secolo fa Apple ha ottenuto condizioni fiscali favorevoli dall’Irlanda perché impiantasse lì stabilimenti e uffici. Apple ha pagato per questo tasse con un’aliquota molto vantaggiosa, per di più su una parte molto piccola dei profitti, per via di un giro contabile astuto, seppure legale, che abbatte massicciamente l’imponibile. Il sistema è talmente vantaggioso che tutti gli incassi ottenuti in Europa vengono registrati in Irlanda, lasciando briciole di tasse ai vari Stati in cui opera l’azienda.<!--more-->

Apple è speciale per molti versi, ma non per questo. Praticamente tutte le multinazionali che operano in Europa ha raggiunto accordi simili e si comporta alla stessa maniera. Ingiusta, ma legale.

La questione è più spinosa di quanto sembri, perché certo Apple paga una percentuale di tasse infima e ingiusta sui suoi profitti europei; ma paga le tasse previste dagli accordi e dalle leggi irlandesi, italiane eccetera. Per capirci: Apple (e tutte le altre) paga pochissime tasse in Europa *rispettando le leggi europee*, come ha scritto Tim Cook in una [lettera aperta](http://www.apple.com/it/customer-letter/). Cioè: le leggi europee fanno schifo.

Pochissimi soldi rispetto al totale dicevamo, ma sono tasse pagate e sono quelle dovute. Apple paga le tasse in Europa. Solo che poi si guarda bene dal mandare negli Stati Uniti i soldi rimasti. Perché verrebbero tassati. *Lo stesso profitto sarebbe tassato due volte*. Cioè: le regole di tassazione sulle multinazionali fanno schifo.

In un mondo ideale, i profitti delle multinazionali sarebbero tassati una volta, in modo equo. Oppure mille volte, una per nazione, ma in modo complessivamente equo. Nel mondo reale sono tassati in modo doppiamente iniquo: per le persone normali che faticano ad arrivare a fine mese per via di un fisco fuori controllo, al contrario delle multinazionali, e per le multinazionali stesse. Che rispettano la legge nel modo più vantaggioso per loro; il problema è che, se lo facessero eticamente, verrebbero punite.

La Commissione Europea ha deciso di risolvere zero di questi problemi. Nel frattempo, comunque, ha [ordinato all’Irlanda](http://europa.eu/rapid/press-release_IP-16-2923_en.htm) di recuperare da Apple tredici miliardi di euro (più interessi) di tasse relative al periodo 2003-2014.

Irlanda che, avendo promesso vantaggi fiscali in cambio di investimenti, ora si rimangerebbe i vantaggi fiscali tenendosi gli investimenti. Il sistema migliore per farsi dimenticare per il futuro da qualsiasi azienda in cerca di luoghi per crescere. Il tono è quello della battuta, ma i vertici della politica turca hanno fatto sapere che loro sono a disposizione:

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Apple should move to Turkey. Happy to provide more generous tax incentives. Won&#39;t have to deal with EU bureaucracy <a href="https://t.co/9ceOnauGi0">https://t.co/9ceOnauGi0</a></p>&mdash; Mehmet Simsek (@memetsimsek) <a href="https://twitter.com/memetsimsek/status/770590795099467776">August 30, 2016</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

>Apple dovrebbe spostarsi in Turchia. [Saremmo] felici di fornire vantaggi fiscali più generosi. Non dovrà avere a che fare con la burocrazia dell’Europa.

Europa che decide di cambiare retroattivamente le regole fiscali. Se sono sbagliate vanno cambiate, ma quello che è stato è stato, o l’ingiustizia è profonda. Perché il contrario non funziona: non posso andare domani all’Agenzia delle Entrate e spiegare che nel 2003 ho pagato più tasse di quante oggi ho deciso di averne dovute pagare. Le tasse retroattive sono l’iniquità più grande in uno Stato di diritto. O un continente, non cambia.

La Commissione Europea ritiene di poter fare bullismo fiscale sulle multinazionali (più di quaranta sotto indagine negli ultimi due anni) senza che tutti ne paghiamo le conseguenze. Questo, in luogo di una vera riforma fiscale e di lavorare per favorire la crescita (quindi riscuotere più tasse), è il modo peggiore. È un posso falso.