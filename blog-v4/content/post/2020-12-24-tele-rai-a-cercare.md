---
title: "Tele Rai a cercare"
date: 2020-12-24
comments: true
tags: [Rai, Safari, Raiplay, Vpn]
---
Un amico che mi suggerisce di guardare l’[America’s Cup](https://www.americascup.com), spettacolo straordinario.

Dal sito capisco che in Italia si può guardare da Rai e Sky.

Entro su Raiplay dove mi chiedono la registrazione. Scelgo l’Apple Sign-On.

Neanche a dirlo, quando devo accedere, Raiplay non si ricorda la password impostata automaticamente.

Faccio Recupera password. Inserisco i dati e mi ritrovo con la schermata seguente.

 ![Recupero password su Raiplay](/images/rai.png  "Recupero password su Raiplay") 

La schermata di recupero password ha problemi a recuperare la password.

La cosa più bella è che tutto il resto di Safari funziona come un orologio svizzero, mentre questa singola finestra, solo lei, *è impossibile da chiudere*.

Come portatore sano di Vpn vado a vedermi l’America’s Cup all’estero e pazienza. Colpa anche un po’ mia, perché rivolgersi alla Rai con pretese di ricevere un servizio fa molto 2020. Sarebbe stato meglio tenere le distanze dagli assembramenti, specie se di incapaci.

Per fortuna da domani cambia tutto. Si rinasce, religiosi o atei non importa. Comunque è bello pensarlo.

Sarà anche diverso dal solito. Il senso della ripartenza sarà ancora più acuto. La distanza sanerà conflitti e avvicinerà animi. Anche il più acceso detrattore di Internet sarà in videochiamata con un parente lontano ed è giusto così.

In quest’anno bizzarro ho potuto godere di molte fortune e privilegi, legati al restare in salute e poter svolgere egregiamente il mio lavoro da un angolo di casa, senza altre preoccupazioni. Non è andata così per tutti e non so neanche quanto durerà per me. <em>Ma nessuno lo sa</em>, si diceva alla fine di <em>Blade Runner</em>, almeno prima che cominciasse la sarabanda dei rimontaggi e delle edizioni speciali.

In ogni caso, nessun privilegio è più sentito di avere uno spazio personale dove scrivere e una banda di persone straordinarie disposte a leggere, oggi, queste righe. Buon Natale qualunque cosa ci unisca, ci separi, ci faccia incrociare come la tecnologia e le arti liberali.

Un grosso grazie.