---
title: "Così andrà il mondo"
date: 2020-11-12
comments: true
tags: [M1, MacBook, Pro, Air, Mac, mini, Arm, Pro, iMac, Intel]
---
Abbiamo avuto la possibilità di vedere come saranno costruiti i computer tra cinque anni.

Solo che questi saranno disponibili [nel giro di una settimana](https://www.apple.com/it/apple-events/november-2020/).

Se dovessi comprare un Mac mini con M1 carrozzato esattamente come quello che ho preso nel 2019, spenderei tre quarti della cifra. Solo che andrebbe veloce il triplo o giù di lì, consumando pure meno.

MacBook Pro 13” dichiara venti ore di autonomia. MacBook Air diventa il nuovo portatile per la persona media, che deve fare cose normalissime. Poteva farle anche prima, con il MacBook Air Intel; ma adesso le farà molto più velocemente e la batteria dichiarata dura il sessantasette percento in più di ore dichiarate.

Gli indomabili della dissonanza cognitiva, per cui se è un Mac vuol dire che qualcosa deve essere sbagliato per forza, si sono mobilitati subito a dire che cose che vanno veloci uguali si trovano; cose che consumano uguali si trovano. Il loro problema è che cose veloci uguali E con consumi uguali, insieme, non ce le ha nessuno e passerà pure del tempo.

E sono le prime macchine, la prima iterazione del chip. Arm è un’architettura che parte centrata sulla riduzione dei consumi prima che sulle prestazioni e il lavoro che attende Apple è faticare per riuscire a spremere dai processori anche le prestazioni richieste per i futuri Mac Pro, ma anche per gli iMac, i MacBook Pro 16”. È fatica. Ma ci sono anche margini di crescita. Mentre Intel annaspa e Amd l’ha certo superata, ma ha margini di crescita normali. La piattaforma Apple è partita adesso, quella Intel/Amd è al suo culmine.

Nel 2005 avevamo visto la transizione di una Apple un po’ esasperata, con la strada obbligata davanti. Nel 2020 è una decisione autonoma che sembra avere dietro fondamenta molto solide.

Non saranno tutte rose e fiori naturalmente, come in ogni discontinuità di questo genere. Però, che partenza. M1 è persino scandaloso da quanto è pulito e curato. La sua estetica, prima dell’altroieri, atteneva alla fantascienza.

Quelli di iFixit e del *right to repair* scopriranno che la RAM fa parte del package del processore, per cambiarla bisogna lavorare a livello di nanometri. Come minimo entrano in sciopero della fame.