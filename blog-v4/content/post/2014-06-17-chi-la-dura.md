---
title: "Chi la dura la scrive"
date: 2014-06-18
comments: true
tags: [Samsung, Ssd]
---
Spettacolare esperimento di *The Tech Report*, che ha preso sei dischi Ssd (a stato solido) e [li ha sottoposti a un test di sopravvivenza](http://techreport.com/review/26523/the-ssd-endurance-experiment-casualties-on-the-way-to-a-petabyte) consistente nel continuare a scrivere dati sulla loro superficie (la scrittura è il punto debole di questa tecnologia) fino a quando soccombono.<!--more-->

La cattiva notizia è che i modelli della serie 840 Samsung, diffusissimi per il prezzo competitivo (ne ho fatto montare con piena soddisfazione uno sul MacBook Pro in funzione sotto queste dita), sono i primi ad andarsene.

La buona è che questo accade verso i trecento terabyte, una quantità probabilmente più che sufficiente ad arrivare alla sostituzione del disco per semplice obsolescenza, con un cambio di computer oppure per avere più spazio e più velocità, più che per via dei guasti.

Sarebbe interessante avere dati simili anche sui dischi rigidi meccanici.