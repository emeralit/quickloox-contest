---
title: "Poveri consumisti"
date: 2023-12-04T19:59:19+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Internet]
tags: [iPad, Hockney, David Hockney, Bigger Christmas Trees, Battersea Power Station, Londra, Tim Cook, Cook]
---
[David Hockney](https://www.hockney.com/home), uno dei maggiori pittori viventi, si è dilettato a preparare un’opera intitolata *Bigger Christmas Trees* che in questi giorni viene proiettata a Londra sopra l’iconica centrale elettrica di Battersea, [già nota agli ascoltatori di rock progressivo](https://faroutmagazine.co.uk/pink-floyd-giant-pig-loose-over-london/) tra le altre cose.

[Se ne è accorto persino Tim Cook](https://twitter.com/tim_cook/status/1731761610195361973), altrimenti impegnato a guidare Apple, per la ragione che Hockney – da anni – ha inserito tra i suoi strumenti di lavoro anche iPad e proprio con quest’ultimo ha realizzato la sua ennesima opera.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">A huge thank you to the incomparable David Hockney for helping us get into the spirit of the season! Your new artwork, Bigger Christmas Trees, created on iPad looks incredible on London’s Battersea Power Station. Happy holidays everyone! <a href="https://t.co/6EwFgP9Lik">pic.twitter.com/6EwFgP9Lik</a></p>&mdash; Tim Cook (@tim_cook) <a href="https://twitter.com/tim_cook/status/1731761610195361973?ref_src=twsrc%5Etfw">December 4, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

iPad, quell’apparecchio che non è un vero e proprio computer, buono per consumare media più che crearne, limitato in tante cose, che bello per carità ma per fare lavoro serio non basta.

Almeno per gli artisti, almeno a un artista, a qualcosa serve. E ai consumisti va il boccone di traverso.