---
title: "Felici e spendenti"
date: 2018-02-09
comments: true
tags: [Dediu, Apple]
---
Horace Dediu ha svolto un gran servizio per tutti nel [riportare su Asymco](http://www.asymco.com/2018/02/05/apple-remarks-to-investors-in-fq1-2018-earnings-conference-call-categorized-and-annotated/) le dichiarazioni di Tim Cook e Luca Maestri a seguito degli ultimi risultati finanziari, con qualcosa in più: in rosso le frasi che chiariscono la strategia di Apple e in blu il suo commento personale.

Il bigino che ne nasce è eccezionale per andare diritti al punto nel capire che cosa fa Apple e come lo fa.

Primi elementi, la base degli apparecchi attivi e l’indice di soddisfazione. Questi numeri sono probabilmente la prima priorità assoluta dell’azienda, indipendentemente da quanti soldi fa iPhone o se l’azienda pensa più a questo che a quello.

Gli indici di soddisfazione sono altissimi, sempre ai vertici delle classifiche. Eppure chi usa Apple spende: nelle stime di Dediu, usa Apple la metà di chi usa Android. La spesa in app, però, è complessivamente doppia.

Ecco, chi dice che con Android si fanno le stesse cose. Palesemente, no. Si spende meno per fare meno. Con altri indici di soddisfazione.