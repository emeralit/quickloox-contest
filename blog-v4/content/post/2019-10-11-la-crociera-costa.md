---
title: "La crociera costa"
date: 2019-10-11
comments: true
tags: [64, 32, bit, Mojave, Catalina]
---
Sono vagamente perplesso.

A mia conoscenza, è da aprile 2018 che macOS avverte ogni volta che si apre una app a 32 bit: *non è ottimizzata. Non funzionerà con le prossime versioni di macOS*.

 ![Al lancio di LibreOffice a 32 bit su Mojave](/images/libreoffice-32-bit.png  "Al lancio di LibreOffice a 32 bit su Mojave") 

A giugno 2018, durante WWDC, è stata mostrata [una slide](https://9to5mac.Com/2018/06/05/macos-mojave-32-bit-support/) che ha fatto il giro del mondo (Mac) e recita, certamente in inglese ma insomma,

>Mojave è l’ultima versione di macOS a supportare le app a 32 bit.

La transizione a 64 bit è iniziata, vero, da poco tempo: [2006](https://en.wikipedia.org/wiki/MacOS).

Sono otto anni che il sistema operativo funziona solo su processori a 64 bit.

Da febbraio 2015 gli sviluppatori su iOS sono tenuti a presentare app a 64 bit e iOS 11 ha eliminato la compatibilità con i 32 bit. Siamo a iOS 13.

Dal [conteggio delle visioni di YouTube alle sessioni di Chess.com](/blog/2017/06/14/l-unica-cosa-e-sorridere/), il mondo è al lavoro per superare i 32 bit, perché non sono più adeguati, semplice.

Poi arriva uno che evidentemente è stato in crociera ai Caraibi, suppongo per due o tre anni, beato lui, a sorprendersi perché ohibò, Catalina non supporta i 32 bit.

No, perché lui con la app a trentadue bit *ci lavora*.

E come si fa normalmente con gli strumenti di lavoro, si è ben guardato dal tenersi informato – diverso e più semplice del tenersi aggiornato – sulla loro evoluzione e sulla loro conformità ai suoi bisogni. O sono io che vivo in un mondo diverso?

Vivere con la mente in crociera è divertente, ma costa e prima o poi c’è da pagare. Che poi, se proprio, è il prezzo di una copia di Parallels. Spendi di più se porti la famiglia al multisala.