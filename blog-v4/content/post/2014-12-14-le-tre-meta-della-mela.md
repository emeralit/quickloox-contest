---
title: "Le tre metà della mela"
date: 2014-12-14
comments: true
tags: [Honan, iCloud]
---
Il meccanismo di autenticazione a due fattori di iCloud consente che possa verificarsi [l’impossibilità di accedere al proprio account nel caso si perda la *recovery key*](http://thenextweb.com/apple/2014/12/08/lost-apple-id-learnt-hard-way-careful-two-factor-authentication/).<!--more-->

È un bel problema. Uno dice, il sistema dovrebbe essere più flessibile per evitare questi incidenti.

Vero; ma se fosse stato inflessibile in questa misura, il giornalista Mat Honan avrebbe ancora integro il proprio account, che invece [è stato violato](http://www.wired.com/2012/08/apple-amazon-mat-honan-hacking/all/) con conseguenze spettacolari.

Non si può avere la migliore flessibilità e la migliore sicurezza insieme; contemporaneamente non si può ideare un sistema di autenticazione e sicurezza che governi quasi un miliardo di *account* e sia esente da casi particolari e problemi eccezionali, nel senso dell’eccezione.

Avere tutto questo insieme è una mela fatta di tre metà.