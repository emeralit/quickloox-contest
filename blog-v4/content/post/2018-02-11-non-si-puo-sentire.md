---
title: "Non si può sentire"
date: 2018-02-11
comments: true
tags: [HomePod, Dalrymple, Reddit]
---
Torno sull’argomento HomePod e sul discorso del [lavoro che non si vede](https://macintelligence.org/posts/2018-02-07-lavoro-che-non-si-vede/), perché scopro che Jim Dalrymple ha pubblicato un [resoconto della sua visita nei laboratori audio di Apple](http://www.loopinsight.com/2018/02/06/inside-apples-homepod-audio-lab/) e, per esempio, nella camera anecoica a -2 dBA, sotto la soglia uditiva dell’orecchio umano, in modo da poter lavorare sui rumori più nascosti e sottili.<!--more-->

I laboratori sono molto occupati con HomePod e con iPhone ovviamente, ma viene detta una cosa importante:

>Il laboratorio Noise & Vibration fu creato anni fa per lavorare sui rumori indesiderati di Mac.

Tutto è partito molto prima dell’ovvio, sempre per lavorare su cose che non si vedono, come quelle che non si sentono. O meglio, che non vede chi guarda solo il prezzo.