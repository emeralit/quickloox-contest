---
title: "Un manifesto"
date: 2017-12-22
comments: true
tags: [BBEdit, regex, AppleScript]
---
**Ora come ora** sono un BBEditiano puro (tra l’altro la versione 12 appena uscita ha aggiunto l’unica cosa che un po’ si poteva invidiare ad altri editor di testo). Scrivo prevalentemente in Markdown o anche in Html diretto, dipende (anni di personalizzazione fanno sì che io abbia una combinazione di tastiera per ogni tag importante e la velocità di scrittura in Html sia praticamente quella del testo normale).<!--more-->

Tengo di scorta Pages e LibreOffice per quando il cliente-azienda mi invia l’inevitabile file Word e me la cavo benissimo; per il resto, distico, tutto testo. :-)

Le espressioni regolari sono utilissime a livello di progetti: capitoli di un libro, un faldone virtuale di documenti o anche testi lunghi e complicati internamente, e persino piccole cose che cambiano la vita.

Ultimissimo esempio: per Apogeonline mando via email piccole interviste agli autori dei libri pubblicati dall’editore. Per comodità numero le domande. Prima lo facevo a mano. Adesso mi ricordo che BBEdit ha la sua brava funzione di numerazione delle righe e così le righe le numera lui. Solo che le domande si leggono molto meglio se sono separate da una riga vuota. Invece che inserirle io a mano, chiedo a BBEdit di cercare

`(\n)( ?\d?\d)`

e di sostituirlo con

`\1\1\2`

così pensa a tutto lui.

Il prossimo step sarà inserire in un piccolo AppleScript sia il comando di numerazione delle righe sia l’espressione regolare che inserisce le righe vuote, in modo da dimezzare il numero di operazioni manuali necessarie. Piccole cose, ma il tempo non basta mai e **meno riesco a finire dentro lavori manuali privi di valore intrinseco, meglio è**.

§§§

Quanto sopra l’ho scritto per posta elettronica a un amico, discutendo di testo ed editor di testo. E però lo sosterrei tranquillamente in pubblico, parola per parola.

Un manifesto della ricerca dell’efficienza possibile e doverosa.
