---
title: "È bello ciò che funziona"
date: 2023-05-30T02:57:45+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Google Analytics]
---
Da un po’ non dovevo prendere in mano [Google Analytics](https://analytics.google.com/) e avevo da recuperare un po’ di memoria operativa. Così ho lanciato Safari su Mac e non mi sono posto subito il problema di non vedere subito i menu che interessavano; dopo tanto tempo, chissà quante cose ha cambiato Google.

Certamente il design. Nuovo, filante, elegante, chiaro. Tutto più e meglio di prima (l’aspetto dell’Analytics cui sono abituato me lo ricordavo).

Il design ha iniziato a perdere priorità man mano che passavano i minuti e non riuscivo a fare quello che dovevo. La funzione non si trovava dove me la ricordavo, ma c’era e ci ero arrivato. L’espressione regolare nel campo di ricerca, però, non dava risultati.

Regex semplice e collaudata, niente di speciale. Ma niente, come averla. Forse che Google abbia cambiato sintassi…? Metto e tolgo il *pipe*, aggiungo e rimuovo le parentesi, accorcio il path, poi lo allungo; provo una snervante serie di varianti e ottengo nulla.

Poi, all’improvviso come una mucca al pascolo fa il ruttino, l’ispirazione: *proviamo a lanciare Chrome*.

Il design ipermoderno su Safari, scopro, è dovuto a qualche fattore forse tecnico, forse di marketing, non so; di fatto, su Safari Analytics funziona male. Su Chrome l’aspetto è quello noioso e vecchio di sempre e tutto funziona battere ciglio.

La morale: non è bello ciò che è bello, ma è bello ciò che funziona.