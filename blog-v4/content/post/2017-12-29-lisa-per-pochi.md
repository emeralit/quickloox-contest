---
title: "Lisa per pochi"
date: 2017-12-29
comments: true
tags: [Lisa]
---
All’inizio degli anni ottanta un Lisa era roba per pochissimi. Servivano diecimila dollari e una visione davvero particolare, per cogliere l’innovazione di funzioni come memoria protetta, multitasking, un sistema operativo a finestre sovrapponibili, salvataggio automatico dei file e tante altre capacità che il resto del mondo avrebbe conosciuto molto tempo dopo.<!--more-->

Emerge da una mailing list di appassionati che nel 2018 gran parte del codice sorgente di Lisa [verrà pubblicata con licenza open source](https://groups.google.com/forum/m/#!msg/lisalist/aIo6cNu54xM/_Ck_CsmSBgAJ).

Rimarrà una cosa per pochi. Il sorgente andrà ricompilato, probabilmente adeguato a nuove risoluzioni di schermo, ovviamente mancano i driver per qualsiasi apparecchiatura moderna e così via.

Tutto ciò non esclude, anzi, che vedremo prima o poi un Lisa OS o qualcosa di vicino a esso reso scaricabile e installabile, per quanto con funzioni limitate. È un esercizio formidabile; vedere Lisa in funzione su un Mac del 2018 sarebbe veramente un’occasione storica. Anche perché la benevolenza di Apple verso progetti come questo non va data per scontata.

Rimarrà una cosa per pochi, sì. La notizia tuttavia è che sarà cosa per qualcuno, trentacinque anni dopo. È un bel viatico per il 2018.