---
title: "Lamenti rumorosi"
date: 2022-04-29T02:42:39+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Mac, Apple, Cook, Tim Cook]
---
Sotto pandemia e guerra, Apple ha registrato [il miglior trimestre invernale di sempre](https://www.apple.com/newsroom/2022/04/apple-reports-second-quarter-results/).

Uno dei dettagli di interesse è l’andamento di Mac. Dice Tim Cook che [i migliori sette trimestri per vendite di Mac nella storia di Apple sono… gli ultimi sette trimestri](https://twitter.com/macjournals/status/1519777723174031360). Quello appena concluso è inferiore solo a quello natalizio ed è appena normale che vada così.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">CNBC quoting Cook: “The last seven Mac quarters have now been the top seven quarters ever in the history of the Mac. … we also introduced the M1 Ultra which is the most powerful chip for a personal computer.”</p>&mdash; MacJournals.com (@macjournals) <a href="https://twitter.com/macjournals/status/1519777723174031360?ref_src=twsrc%5Etfw">April 28, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Rifletto allora su chi rappresentino realmente i lamentosi cronici, per i quali l’Ssd e la Ram saldati sulla scheda logica sono delle truffe, la macchina è chiusa, si trovano Pc che fanno lo stesso a meno prezzo, la qualità diminuisce e così via. Non parliamo dei misteriosi professionisti in fuga perenne.

Certamente su ciascun punto si può aprire un dibattito; al tempo stesso, però, come fanno macchine sbagliate a vendere sempre di più? Le ipotesi di un continuo e crescente afflusso di sprovveduti sono sempre meno credibili e, lato hardware, non si contano i modelli prodotti da questa e quella, che all’aspetto sono tali e quali a un MacBook Air. Le occasioni di comprare qualcosa di diverso abbondano e non è neanche un periodo di eccellenza nel software, per Apple.

L’unica conclusione plausibile è che i lamenti facciano più rumore del silenzio dei soddisfatti. Dove _soddisfatto_ è diverso dal dire _tutto va bene_. E i lamenti sono espressione di banali ubbie personali legate a mille questioni pienamente legittime, di nessun impatto serio sulla collettività.