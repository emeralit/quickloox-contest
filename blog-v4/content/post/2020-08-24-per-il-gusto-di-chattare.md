---
title: "Per il gusto di chattare"
date: 2020-08-24
comments: true
tags: [Irc, Drang, Slack, Telegram, Apple, Relay, Chat]
---
Nel guardare alla giornata di oggi dal punto di vista degli avvenimenti presso Apple e dintorni, posso solo sposare appieno la [considerazione di Dr. Drang](https://leancrew.com/all-this/2020/08/epic-relief/):

>Uno dei maggiori vantaggi di non essere, né avere aspirazione a essere, un blogger Apple professionista è che non si sente l’obbligo di scrivere di qualsiasi vicenda salti fuori.

Voglio scrivere ogni giorno, parlo spesso di Apple e tutto sommato mi piacerebbe provare a essere un blogger Apple professionista (con un reddito basato su questa attività). Ma se lo fossi, comunque, non vorrei parlare di tutto quello che succede.

Così posso affermare sereno che la cosa più importante di oggi è – o sarebbe secondo il mio archivio – l’anniversario di Irc, [Internet Relay Chat](https://en.wikipedia.org/wiki/Internet_Relay_Chat), nato nel 1988.

A mio parere Irc rimane tuttora il modo più svelto ed efficace di chattare in gruppo via Internet con più gusto, per quanto Wikipedia descriva una comunità in declino, poche centinaia di migliaia di utenti attivi in una Internet da quattro miliardi di persone.

Prediligo i programmi che in qualche modo si ispirano a Irc, come Slack o Telegram, e accarezzo la voglia di frequentarlo maggiormente. Anche in informatica, le cose migliori e più importanti sono state inventate prestissimo e il resto è stata tutta evoluzione.