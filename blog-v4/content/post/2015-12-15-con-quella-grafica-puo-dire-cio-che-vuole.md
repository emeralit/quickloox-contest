---
title: "Con quella grafica può dire ciò che vuole"
date: 2015-12-15
comments: true
tags: [Apple, Microsoft, Surface, iPad, innumeracy]
---
Mi ha dato l’imbeccata **Reostato** con un commento a [questo articolo](https://macintelligence.org/posts/2015-12-14-di-ventotto-ce-n-e-una/):

>A proposito di Windows: ma è vera ’sta roba?? Un mio amico mi ha inviato [questo link](http://microsoft-news.com/microsoft-triumphs-apple-online-tablet-sales/), a me non sembra nemmeno possibile che Microsoft col Surface venda in alcuni segmenti più di iPad.<!--more-->

Il *link* porta a un articolo appena appena compiacente nei confronti di Microsoft (chi se la prende con i media a suo giudizio pro-Apple dovrebbe anche farsi un giro a vedere che cosa avviene con la concorrenza) nel quale si cita un lavoro di ricerca per cui, nel mese di ottobre, Microsoft ha iniziato a vendere più tablet di Apple.

Potrebbe essere, ma qui si entra a gamba tesa nella [innumeracy](https://en.wikipedia.org/wiki/Innumeracy_(book)), l’incapacità di comprendere a fondo i numeri e di converso la possibilità di rappresentare assolutamente quello che si vuole, manipolando i numeri nel modo più conveniente.

Ho fatto un giro sul [post originale](https://www.1010data.com/company/blog/detail/microsoft-surpasses-apple-in-online-tablet-share-for-the-first-time-in-2015) da cui nasce l’articolo linkato.

I dati arrivano da *milioni di acquirenti americani che permettono il tracciamento anonimo del loro comportamento online a scopi di ricerca di mercato* di più non sappiamo. I dati raccolti subiscono una elaborazione che secondo gli autori della ricerca dovrebbe rendere il campione simile al comportamento globale di Internet, di cui non è nota l’efficacia.

Si parla dunque di sola vendita online, solo negli Stati Uniti, solo nel mese di ottobre, rilevata su un campione di entità sconosciuta. È abbastanza per considerare la faccenda quanto meno soggetta a un pizzico di scetticismo puramente scientifico? Guarda il caso a volte, ottobre è esattamente il mese in cui Microsoft ha annunciato Surface Pro 4. Indizio: se si guarda il primo mese di vendite di un nuovo modello di iPhone, per dire, le vendite sono *sempre* superiori a quelle del resto dell’anno, fatta eccezione magari per il Natale.

C’è dell’altro, più significativo. La ricerca parla delle *dimensioni mensili del mercato online dei tablet*: a ottobre (ricordiamo, solo negli Stati Uniti) sarebbero state di 226 milioni di dollari, contro 166 milioni di dollari a giugno.

Non c’è un dato preciso per luglio agosto e settembre, ma a occhio il dato potrebbe variare mese per mese tra i centosessanta e i duecento milioni di dollari. Teniamo per buoni i duecento milioni, che sono il dato più lusinghiero. Vogliamo una stima di duecento milioni di dollari per ogni mese tra luglio e settembre perché non abbiamo ancora i dati di ottobre sulla vendita di iPad; ma abbiamo [quelli sul trimestre estivo](http://images.apple.com/pr/pdf/q4fy15datasum.pdf).

Apple ha venduto nel trimestre 4,276 miliardi di dollari di iPad, cioè in media 1,425 miliardi di dollari al mese. In tutto il mondo. Quanti di questi iPad sono stati venduti negli Stati Uniti? Una grossolana proporzione con il venduto di tutta Apple rispetto al venduto nelle Americhe farebbe stimare 1,8 miliardi. Facciamo un miliardo e mezzo per i soli Stati Uniti. Cinquecento milioni al mese.

Quanti di questi iPad americani sono stati venduti online? La grafica della ricerca è poco precisa ma lascia pensare che in settembre iPad abbia rappresentato circa il 33 percento del venduto tablet online negli Stati Uniti. (Si noti che manca qualsiasi quantificazione fuori dalle percentuali). Se l’intero mercato valeva duecento milioni di dollari, iPad ne occupava 66 milioni.

È tutto molto stimato e approssimato, però i margini per contestare le conclusioni sono pochini, perché c’è un divario enorme tra il presunto venduto online (66 milioni) contro il venduto complessivo di cinquecento milioni: il primo rappresenta il 13,2 percento del secondo.

Qualunque cosa sia successa tra settembre e ottobre, riguarda poco più di un iPad su dieci. E a questo punto sarebbe assai più interessante sapere che succede nell’87 percento dei casi invece che in una nicchia.

Reostato: il tuo amico ha grande voglia di credere a tutto quello che legge. Digli che quella cosa riguarda il dieci percento del mercato e trascura il novanta percento. Lui comunque crederà a quello che vorrà credere e dirà che *quelli di Apple* sono fanatici.

Quelli di Apple sanno che con i numeri e una grafica sufficientemente approssimata, si può dire quello che si vuole, indipendentemente dalla realtà dei fatti.