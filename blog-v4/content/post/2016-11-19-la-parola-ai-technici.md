---
title: "La parola ai technici"
date: 2016-11-19
comments: true
tags: [MacBook, Pro, Ram, Ssd, Thunderbolt, Usb-C]
---
*Ars Technica* ha messo insieme una straordinaria recensione dei nuovi [MacBook Pro](http://arstechnica.com/apple/2016/11/the-2016-13-and-15-inch-touch-bar-macbook-pros-reviewed/).

Direi che una attenta lettura è necessaria per chiunque voglia parlare dell’argomento con cognizione di causa. Il livello è altissimo, come dettaglio tecnico e come vista di insieme. Se gli sforzi editoriali su Internet si muovessero in questa direzione invece che tendere alla spazzatura a basso costo, il mondo sarebbe migliore e non solo quello Mac.

Segnalo in particolare che nella recensione si spiega perché Apple ha usato le schede grafiche che ha scelto, questione che è normalmente un po’ complicata da dirimere. Premesso – lo dice *Ars* – che mai Apple ha inserito schede grafiche di altissime prestazioni nei suoi portatili e non lo ha fatto questa volta come non lo ha fatto neanche nelle precedenti, c’è un problema di DisplayPort che deve ancora arrivare alla sua prossima versione e non può garantire una certa banda passante.

Così i progettisti hanno dovuto sovrapporre parzialmente (semplifico) la banda di due canali per riuscire a fare sì che le macchine riuscissero a pilotare due schermi esterni da 5K in simultanea. Altrimenti le prestazioni video sarebbero state più alte, ma il sistema avrebbe pilotato un solo monitor esterno.

La progettazione è spesso fatta di compromessi e questo, prestazioni contro superficie, l’ho trovato significativo.

Una curiosità? Le periferiche con le migliori prestazioni vanno connesse preferibilmente alle porte sul lato sinistro. Anche qui c’è un perché.

Nella recensione c’è moltissimo altro e leggerla è un impegno. Ma chiunque voglia esprimere un parere sui nuovi MacBook Pro senza averne in mano uno, prima deve passare l’esame di *Ars Technica*.