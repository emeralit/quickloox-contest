---
title: "It’s how it works"
date: 2021-07-30T00:30:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Stefano, iPad, iPadOS, macOS] 
---
Ho il piacere di ospitare nuovamente, dopo diverso tempo, **Stefano**:

§§§

Per ragioni di lavoro ho dovuto abbandonare il mio vecchio iPad Pro da 12”9 di terza generazione per passare al nuovo modello carrozzato con M1. Il vecchio terminale aveva poco disco oramai.

In quell’occasione ho deciso il salto verso la tastiera retroilluminata (prima avevo la tastiera stile folder) che avevo trascurato durante la sua presentazione principalmente per l’aumento di peso, concentrandomi sulla portabilità della vecchia soluzione e non dando troppa importanza al “mouse” per iPad.

E come sempre accade Apple mi ha smentito.

Spesso mi capita di utilizzare anche il MacBook Air di mia moglie e il mio disagio (permettimi il termine) è forte passando da iPadOS a macOS: non trovo le *gesture* a video, mi ritrovo a mettere le ditate sullo schermo sperando che accada qualcosa eccetera. Davvero mi sembra un salto nel passato quando devo utilizzare il MacBook.

Da quando sto giocando con l’interfaccia di iPadOS e il suo mouse sembra che Apple abbia trovato l’uovo di Colombo: un mix di classiche gesture con le dita, molto immediate, con la praticità del puntamento del mouse.

L’utilizzo di iPad è diventato più veloce ovunque lo appoggi, vista la rigidezza della tastiera. E quello che prima pensavi fosse un limite (il peso) all’improvviso svanisce.

In più unisci la potenza sotto il cofano dell’M1: un prodotto devastante, non economico, ma con una durata di utilizzo spalmata almeno sui cinque anni a dir poco.

E con la Apple Pencil di seconda generazione hai un kit all’avanguardia di puntamento e lavoro ineguagliabile.

§§§

[*Design is how it works*](https://oraajan.medium.com/the-design-is-not-just-what-it-looks-like-and-feels-like-the-design-is-how-it-works-steve-jobs-9b79674126bb).

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*