---
title: "Un solo annuncio"
date: 2015-06-02
comments: true
tags: [Apple, Google, Photos, Wwdc]
---
Dopo avere assistito alla nascita di [Google Photos](https://medium.com/backchannel/bradley-horowitz-says-that-google-photos-is-gmail-for-your-images-and-that-google-plus-is-not-dead-54be1d641526) e avere dato un’occhiata a come funziona, premesso che non mi piace troppo lasciare troppi dati in mano a Google, beh, l’unico annuncio che mi interesserebbe veramente sentire da Apple alla prossima [Wwdc](https://developer.apple.com/wwdc/) è *stoccaggio illimitato e gratuito delle foto su iCloud*.<!--more-->

Sembra un controsenso, considerato quanto costa impegnarsi a contenere dati illimitati per un tempo indefinito. E però, se lo fa Google, è possibile. E Google può essere antipatica o inquietante, ma quando si va sul cloud ha sempre qualcosa da insegnare ad Apple.