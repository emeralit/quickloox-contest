---
title: "Il software che verrà"
date: 2022-04-06T00:40:56+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Riccardo Mori, Mori, macOS, Apple, Universal Control, Douglas Engelbart, Pixelmator Pro, Go, AlphaZero, VisiCalc, OpenDoc, machine learning]
---
Come al solito, i pezzi di **Riccardo** possono essere condivisi o suscitare voglia di controbattere. Fanno anche qualcosa di meglio però: spingono a pensare.

Come questo, che si chiede [come possa Apple essere una forza di ispirazione nel software](https://morrick.me/archives/9542). La tesi di fondo è che Apple stia lavorando bene nello hardware, portando novità importanti e trascinandosi dietro la concorrenza in un miglioramento complessivo dell’offerta (è tutto molto più sfumato di così, ma devo parlare dell’articolo e anche dormire qualche ora). Mentre nel software, in essenza, si fa quello che si faceva vent’anni fa, solo più comodamente e velocemente.

Si può discutere sull’affermazione. Vent’anni fa avevamo la possibilità di trasformare in testo le scritte dentro un’immagine? Sì, però era una cosa da superspecialisti e la semplicità con cui lo fa iPhone neanche era immaginabile. Avevamo, sia pure in embrione, Universal Control, in cui ti siedi e tutti i tuoi device possono diventare estensioni del tuo Mac? Proprio no.

Il punto però è un altro, anche più interessante. Che Riccardo abbia ragione o meno, dove dovremmo puntare per trovare ispirazione nel progresso del software? A che tipo di innovazioni potremmo voler mirare? L’articolo di Riccardo ha il grande valore di farci uscire dalla _comfort zone_ e porci queste domande.

Anche se non vedo più di tanta responsabilità da parte di Apple, è vero che i progressi nel software, specialmente quello applicativo, non sono stati esaltanti. In parte è dovuto alla nostra stupida mania umana di restare ancorati a seimila anni di storia e non riuscire a pensare digitale: continuiamo a produrre software che scimmiotta le attività analogiche invece di ribaltare il tavolo e fare davvero qualcosa di impossibile con carta e penna.

Inoltre è indubbio che le logiche del mercato, dopo che Macintosh esiste da trentotto anni, non favoriscano più di tanto direzioni di ricerca basate su grandi scommesse. Non c’è più spazio per dei novelli [Douglas Engelbart](https://dougengelbart.org) che dentro il loro ufficio inventano un paradigma distruttivo per il vecchio e ispiratore per il nuovo.

Per esempio: i progressi generali del computing ci hanno portato a poter praticare seriamente il _machine learning_, l’apprendimento meccanico dei computer. [Pixelmator Pro](https://www.pixelmator.com/pro/) lo usa in vari momenti dell’elaborazione grafica, per esempio. Fin qui siamo nel territorio fotografato da Riccardo: niente che non ci fosse negli anni novanta, circolare, non c’è niente da vedere. Solo più veloce.

Ma proprio perché è più veloce, perché non abbiamo sistemi operativi e applicazioni che si configurano nel tempo e presentano un’interfaccia sempre più adattata alle mie abitudini? Se dopo tre anni c’è un oggetto di interfaccia che non ho mai usato, perché resta nello stesso posto in cui si trovava il primo giorno, invece di lasciare spazio a un altro oggetto che uso quotidianamente? Il machine learning non è intelligenza artificiale; ma è esattamente questo, imparare nel tempo. Siamo riusciti a creare [giocatori virtuali di Go che surclassano i grandi maestri umani](https://www.deepmind.com/blog/alphazero-shedding-new-light-on-chess-shogi-and-go), ma – se non lo specifico io – Mac mi apre una finestra del Finder come vuole lui.

Questa è una direzione plausibile, per creare software in grado di ispirare? È un salto di qualità desiderabile? O è solo _wishful thinking_, pensare fine a se stesso, la novità per la novità?

Un’altra direzione di sviluppo è quella che non fa conto tanto sulle capacità di elaborazione, ma su nuovi modi di elaborare dati. Penso per esempio alle possibilità di [Obsidian](https://obsidian.md) nella gestione di basi di testo scritto in [Markdown](https://www.markdownguide.org/basic-syntax/). Solo un esempio; se la mente fosse libera di vagare, le proposte sarebbero ben più ardite. Creazione di contenuti, per dire; a tutt’oggi, scrivere in digitale è mettere un carattere dietro l’altro. Mettere insieme foto, video, animazioni, testo, è un’attività di una rigidità mostruosa. Se volessi fare sporgere un elemento tridimensionale da una pagina di testo, tanti auguri.

La pandemia ha portato al boom della videoconferenza, attività che era colpevolmente negletta. Ma perché, dopo due anni, non posso farmi vedere in mezzo ai miei dati, manipolarli e disporli liberamente sullo schermo, forse persino lasciarli manipolare ai colleghi? C’è la condivisione schermo, certo, ma è lo scimmiottamento dell’attività analogica. Con il digitale potremmo organizzare sistemi solari aziendali e non parlo degli sfondi virtuali, ma di veri sistemi di equilibrio basati sulla gravitazione simbolica delle persone e dei documenti gli uni attorno agli altri.

Dobbiamo entrare nella famigerata produttività da ufficio? Qui Riccardo ha ragione. Dopo quarantasei anni da [VisiCalc](https://history-computer.com/visicalc-of-dan-bricklin-and-bob-frankston-guide/)… siamo a VisiCalc, solo più grosso.

Il problema vero è che le innovazioni sono condannate a determinare l’obsolescenza di quello che c’era prima. Quello che c’era prima, oggi, viene usato da miliardi di persone, da decenni. Perché possa diventare obsoleto occorrono tempi e pazienze che probabilmente neanche le multinazionali potrebbero permettersi, o non sarebbero multinazionali. Sarebbe meraviglioso se Apple riprendesse in mano il progetto [OpenDoc](https://wiki.c2.com/?OpenDoc); sarebbe meraviglioso se le persone dessero realmente una _chance_ al progetto OpenDoc, che naufragò dopo le promesse di un concorrente intoccabile, dotato di niente di lontanamente paragonabile come tecnologia; in compenso godeva di una immensa base installata presso persone omologate e obbedienti, incapaci di concepire una qualsiasi alternativa, e di un battaglione di giornalisti pronti a distruggere qualsiasi minaccia allo _status quo_.

Perché Apple, guardando bene, dovrebbe investire denaro in una qualunque innovazione software che miliardi di persone neanche prenderanno in considerazione, perché mentalmente inabili ad accettare l’idea che possa esistere più di un formato per il testo?

Riccardo ha ragione in certi punti e in altri invece si ferma troppo presto nell’analisi. Il suo però è stato un bello sforzo di cominciare a tracciare un panorama della situazione, sul quale riflettere e magari iniziare a discutere. L’unica speranza che abbiamo di vedere software capace di ispirarci è che ne esista una domanda. Adoperiamoci per suscitarla.