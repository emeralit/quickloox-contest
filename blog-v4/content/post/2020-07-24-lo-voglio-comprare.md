---
title: "Lo voglio comprare"
date: 2020-07-24
comments: true
tags: [BBEdit]
---
Il dibattito pro e contro le app in abbonamento su App Store per me si riduce  a una rilevazione molto facile: pago qualsiasi cifra ragionevole per programmi che mi risolvono la giornata, di lavoro, ludica, didattica e via dicendo.

Se questo criterio è soddisfatto, la questione è chiusa. Su Mac compro regolarmente ogni e qualsiasi aggiornamento principale di [BBEdit](http://www.barebones.com/products/bbedit/). Se prendo la cifra pagata e la divido per il numero di mesi trascorsi dalla versione precedente, ottengo un canone di abbonamento ideale che ho semplicemente pagato in soluzione unica, come mi capitava di fare con Sky o [Media Temple](https://mediatemple.net).

Su iPad ci sono ottimi editor di testo, dalla qualità molto migliorata negli anni. Nulla però si avvicina alla qualità e alla versatilità di BBEdit. E allora pago più che volentieri un prezzo equo, ma l’abbonamento no.

Il software che non è un compagno di vita, lo voglio comprare. È una modalità di partnership più adeguata.