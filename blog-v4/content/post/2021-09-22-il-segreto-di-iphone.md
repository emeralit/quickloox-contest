---
title: "Il segreto di iPhone"
date: 2021-09-22T00:26:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [iPhome 13, iPhome, Asymco, Horace Dediu] 
---
>Da poco è stato venduto l’iPhone numero due miliardi.

La tocca piano, Horace Dediu di Asymco. Il suo pezzo ha un titolo che la tocca pianissimo: l’[iPhone più importante di sempre](http://www.asymco.com/2021/09/21/the-most-important-iphone-ever/).

Il numero tredici (non il tredicesimo)? Come è possibile?

Dediu traccia un panorama ad amplissimo respiro, un vero e proprio stato della nazione iPhone. Gli utenti effettivi sono oltre un miliardo, circa un quarto dei tre miliardi e ottocento milioni di computer da tasca presenti sul pianeta.

La varietà di configurazioni aumenta e così gli incentivi dei fornitori di telefonia. In più, iPhone domina il mercato dell’usato. E arriviamo a iPhone 13, che sarebbe appunto il più importante di sempre.

>Crea la percezione di che cosa dovrebbe essere un telefono e imposta una traiettoria di domanda che prima non esisteva. È facile pensare che un telefono più vecchio che va ancora bene sia sufficiente. Che siamo soddisfatti dalle app e dalla messaggistica attuale. Video a posto, foto a posto. Senza bisogno di modalità notturne, grandangoli e macro.

Questo è vero, continua Dediu, se il livello top delle prestazioni resta fermo; a un certo punto arrivano da sotto prodotti *good enough*, buoni a sufficienza, che tolgono il mercato da sotto i piedi. Invece,

>Ciò che rende speciale iPhone e forse Apple è che sembra offrire cose che nessuno ha chiesto e poi tutti vogliono, evitando nel contempo di magnificare una dimensione di prestazioni che qualcuno domanda ma in effetti nessuno usa.

Ecco perché iPhone sembra un aggiornamento neanche tanto significativo (il *Wall Street Journal* per esempio dice che è [tutta questione di batteria e fotocamera](https://www.wsj.com/articles/iphone-13-pro-max-mini-review-11632223198), con poco altro) e invece è il più importante:

>Bisogna ridefinire la nozione di prestazione; competere su basi nuove, azzerare le aspettative. Per iPhone, trovare nuove dimensioni di prestazioni e quindi nuova domanda è effettivamente una soluzione al dilemma dell’innovatore.

Per evitare che dal basso salga qualcosa di *good enough*, si cambia continuamente la definizione di *alto*. Sarà questo il segreto? Di sicuro, iPhone non ha mai goduto salute migliore.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*