---
title: "Risorse per asceti"
date: 2018-03-13
comments: true
tags: [briand06, slack-term, Slack, Terminale]
---
Motivi per [attivare i propri canali Slack nel Terminale](https://github.com/erroneousboat/slack-term), con un grande grazie a [Sabino](https://melabit.wordpress.com) per l’indicazione, e spegnere l’applicazione ufficiale (o almeno farci un pensiero).<!--more-->

Per imparare. Si tratta di saper rendere eseguibile un file e impartire pochi semplici comandi di Terminale. Sono capacità di base che vanno tenute esercitate.

Per risparmiare risorse. [Slack](https://slack.com) funziona via *browser* o via *app*. Il *browser* mette tipicamente alle corde qualsiasi macchina moderna se solo si aprono abbastanza pagine e poi è fatto per la consultazione; l’interazione risulta sempre poco amichevole. La *app* è realizzata con [Electron](https://electronjs.org), architettura di base con pregi e difetti. Un difetto è la scarsa efficienza. Una *app* Electron potrebbe impegnare il computer più di quanto farebbe la stessa *app* scritta con un diverso sistema.

Perché è essenziale. L’interfccia di macOS è la migliore al mondo e, per dire, ho per sfondo scrivania un affascinante panorama rubato a [World of Warcraft](https://worldofwarcraft.com/en-gb/). Il colore, l’animazione, lo stile sono fondamentali per una eccellente esperienza di utilizzo. Quando si tratta di comunicare, tuttavia, in genere l’interfaccia più spartana è quella più producente, perché libera dagli orpelli anche il pensiero. Eliminare il superfluo dalle interfacce è una forma raccomandata di ascetismo digitale.

Avvertenza: scaricare l’eseguibile con Safari *potrebbe* generare un errore e [mostrare un file con una incongrua estensione .dms](https://forums.macrumors.com/threads/safari-erroneously-adding-dms-extension-to-downloads.2080108/). Nel mio caso è bastato scaricare con Chrome per risolvere.

Seconda avvertenza: per ottenere il *legacy token* di autorizzazione da parte di Slack all’uso del sistema, bisogna essere collegati allo spazio interessato via web. Con la *app*, il sito Slack non si accorge del collegamento. richiamo le impostazioni dell’account, che non si modificano dentro la *app* e aprono una pagina apposta. Si abbia sottomano la password di accesso allo spazio Slack. Dopo avere chiesto il token e fornito la password, la pagina si presenta come se fosse di nuovo necessario chiedere il token. Si clicca con fiducia di nuovo sul pulsante e il token arriva.

Prima di spegnere definitivamente la *app* ufficiale c’è tanta strada da fare. Bisogna creare una configurazione per ogni diverso spazio di lavoro (ho iniziato un AppleScript che uno dopo l’altro li apre tutti), non c’è amministrazione dei file, non ci sono evidentemente le *app* dell’ecosistema, tutto è molto all’inizio.

La comunicazione funziona, però. L’ottanta percento di Slack. Mica poco.