---
title: "La tastiera altrove"
date: 2013-12-24
comments: true
tags: [1Keyboard, Mac, iPhone, iPad]
---
Apparentemente, [1Keyboard](https://itunes.apple.com/it/app/1keyboard/id766939888?mt=12) sembra essere l’ultima moda quanto a programmi per scrivere su iPad e iPhone usando la tastiera di Mac. E comandare da Mac anche una Apple TV, eventualmente.<!--more-->

Potrebbe essere utile per il video-DJ improvvisato la sera di Natale, finita la frutta secca. Oppure anche una delle sere prossime.

Chi passa la festa in coppia si sfidi a [Dead Man’s Draw](https://itunes.apple.com/it/app/dead-mans-draw/id584916423?l=en&mt=8). Diabolicamente divertente e per niente banale.

Fino alle otto persone, invece, [Spaceteam](https://itunes.apple.com/it/app/spaceteam/id570510529?l=en&mt=8). Dura poco, perché dopo tre o quattro partite si è sfiniti, afoni, rintronati. E immensamente divertiti.

Tutto gratis, così non ci sono da sacrificare i regali.