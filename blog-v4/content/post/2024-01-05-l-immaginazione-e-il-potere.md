---
title: "L’immaginazione e il potere"
date: 2024-01-05T02:02:31+01:00
draft: false
toc: false
comments: true
categories: [Internet, Web]
tags: [Wongery, wiki, rpg]
---
Alla vigilia di una giornata di gioco di ruolo con gli amici di sempre, posso solo compiacermi che sia uscito allo scoperto [The Wongery](http://www.wongery.com/): una wiki contenente informazioni immaginarie su mondi di fantasia.

Gli autori hanno iniziato a lavorarci quindici anni fa e hanno reso accessibile a tutti il sito undici anni, undici ore e undici minuti prima dello scorso Capodanno. Ma per tutto questo tempo non hanno mantenuto il silenzio sull’iniziativa.

The Wongery ospita una sezione, Central Wongery, popolata da materiale degli autori; chi lo voglia, ha a disposizione la Public Wongery, dove vengono accettati contenuti da tutti gli iscritti alla wiki. Chiunque può farlo, a costo nullo.

Materiale immaginario, dichiarato tale, a scopo ludico, creato e organizzato da persone entusiaste: è una splendida immagine della Internet giusta e del trionfo dell’umano. Una sedicente intelligenza artificiale non riuscirebbe neanche a concepire il titolo.

L’immaginazione l’abbiamo sempre avuta e la rete ci ha dato il grande potere di poterla esprimerle; a patto di giocare onestamente e con rispetto. Sotto a chi tocca.