---
title: "In memoria della comunicazione"
date: 2022-07-16T01:31:07+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Macerata, Lidia]
---
Abbiamo visitato con successo un bel museo di storia naturale. Non è stato facile, però.

Un sito Internet, una targa sul portone della sede, un gonfalone appeso nelle vicinanze; ciascuno riportava orari di visita diversi.

Era obbligatorio prenotare e farlo *telefonicamente*. Email, SMS, l’incubo ubiquo WhatsApp? Zero, salvo la possibilità di richiedere informazioni con la prima (che abbiamo usato invano, senza risposta). Tutto questo di venerdì, giorno che in Italia suona ancora vagamente lavorativo.

Alla prima telefonata, dopo undicimila squilli ha risposto *il commissariato*. Giuro.

Alla seconda, una persona gentilissima con cui abbiamo concordato le modalità della visita. Mi ha anche fornito la quarta versione degli orari di visita, differente dalle tre precedenti.

Cominciavo a confondermi e confesso che ero anche poco preparato; ricordavo l’indirizzo del museo, ma non conoscevo il nome del palazzo relativo. La persona ha cominciato a nominare il nome di un palazzo e ho dato per scontato che si riferisse a quello del museo.

Invece no, era un altro; per visitare il nostro museo, dovevamo vederci prima con il custode in un palazzo diverso. Non era scritto da alcuna parte.

Eravamo già davanti al museo e sono riuscito a convincere le autorità a spostare il custode da noi, invece del contrario.

Il custode è arrivato con un mazzo di chiavi, che non aprivano la porta di ingresso del museo.

Siccome ci trovavamo in un corridoio con quattro porte, ho suggerito che forse le chiavi in dotazione aprissero una delle altre tre. Questo ha sbloccato la situazione e finalmente siamo riusciti a entrare. Bellissima visita, dicevo, meritevole della temperanza necessaria ad arrivarci.

Il museo ospita una collezione paleontologica nei sotterranei del palazzo, fisicamente distinti dai locali che contengono la collezione naturalistica e mineralogica, con un accesso diverso.

C’è bisogno di dire che il mazzo di chiavi in dotazione al custode non conteneva quella giusta?

Eravamo comunque soddisfattissimi (Lidia ama gli animali molto più dei loro scheletri) e abbiamo ringraziato il custode per la buona volontà. Lui si è scusato molto e proprio non era il responsabile della situazione, si capiva.

Non siamo qui a parlare dell’organizzazione o presunta tale di un ente pubblico, ma dell’importanza della comunicazione. Tutte le falle del meccanismo che ho descritto risalgono a un’unica causa: la carenza e lo scoordinamento, anzi, la morte della comunicazione.

Conosco – molto in generale – due modi di approcciare il problema della comunicazione nell’organizzazione. Il primo è top-down, dove il vertice dirama i contenuti e le regole della loro diffusione; il secondo è quello applicativo, in cui il software organizza autonomamente la distribuzione della comunicazione a partire da template studiati appositamente. Anche il più impreparato degli impiegati compila il template giusto e il software trasforma quella cosa in informazione per il sito, per i volantini, per il gonfalone, per tutto.

Poi esiste anche l’autoorganizzazione bottom-up, basata su linee guida e sulla assunzione personale di responsabilità in base ai progetti e agli obiettivi dei progetti.

Abbiamo certamente esempi acclarati di anarchia e inerzia, in cui letteralmente ogni azione è una monade e in qualche modo tutto va avanti indipendentemente da se stesso, all’incirca come in giardino l’acqua che gocciola dalla canna mal chiusa si espande in una pozza sempre più larga. Ma non è un approccio pratico, semmai la conseguenza della mancanza di un approccio.

Per fare funzionare un museo piccolo ma prezioso, in un contesto di pochi dipendenti pubblici che agiscono nella sostanziale assenza di supervisione e con mansioni sovrapposte tra più enti e più progetti, che approccio sceglieresti?