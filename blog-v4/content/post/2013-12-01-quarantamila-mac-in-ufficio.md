---
title: "Quarantamila Mac in ufficio"
date: 2013-12-01
comments: true
tags: [Google, Mac, Apple]
---
Nelle aziende vecchio stile si trova ancora qualche dirigente che vent’anni fa è entrato in ibernazione e, quando arrivano i primi caldi, scongelandosi borbotta che il Mac non è un computer professionale, non è *business*, non è standard e via così.<!--more-->

Naturalmente, parlando di ditte che usano Mac, una certa Apple naviga attorno ai settantamila dipendenti, vale cinquecento miliardi di dollari sul mercato azionario, ne fattura quest’anno tipo centocinquanta e insomma pare che con i Mac possa anche riuscire a combinare qualcosa.

L’osservazione viene presa come di parte, però; Apple riesce a operare professionalmente con i Mac poiché li vende. Quindi adopererà sicuramente qualche trucco per fare sembrare cose che non sono.

Parliamo allora di un’altra aziendina: Google. La quale utilizza oltre quarantamila Mac, corrispondenti a quasi il 100 percento dei dipendenti; in Google, se qualcuno vuole usare Windows, *deve presentare motivazioni specifiche e dimostrare che ne ha veramente bisogno*.

L’uso dei Mac in Google è talmente pervasivo che un gruppo di sette ingegneri [ha sviluppato una serie di applicazioni interne](http://www.theregister.co.uk/2013/11/27/google_mac_support/) per l’ammininistrazione di tutti questi computer, per bypassare gli strumenti software di Apple come OS X Server e Apple Remote Desktop, ritenuti non all’altezza delle esigenze.

L’intera storia è stata recentemente raccontata dai protagonisti durante il convegno Lisa ’13 ed è consultabile in forma di una bizzarra ma efficace [videopresentazione](https://www.usenix.org/conference/lisa13/managing-macs-google-scale). I primi dieci minuti fanno pensare che niente funzioni, ma pazientando c’è tutto.

La prossima volta che Mac viene fatto passare come uno strumento inadatto all’azienda, si può dire *Google fattura cinquanta miliardi l’anno usando quarantamila Mac. Noi…?*