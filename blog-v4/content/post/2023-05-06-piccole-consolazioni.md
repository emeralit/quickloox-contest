---
title: "Piccole consolazioni"
date: 2023-05-06T03:32:47+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Internet]
tags: [Macintosh, MLB, Major League Baseball]
---
1986: una settimanaccia. Ma abbiamo un computer con un pulsante per inviare un interrupt hardware o entrare nel monitor di sistema.

2023: una settimanaccia. Ma su TV+ c’è il Friday Night Baseball.