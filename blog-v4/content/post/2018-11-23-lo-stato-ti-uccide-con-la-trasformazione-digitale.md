---
title: "Lo stato ti uccide con la trasformazione digitale"
date: 2018-11-23
comments: true
tags: [Q8, fatturazione]
---
(Anni fa girava attorno al Castello Sforzesco di Milano un innocuo svalvolato il cui mantra era *La Chiesa ti uccide con l’onda*. Il titolo è un piccolo omaggio).

Ho scritto un paio di pezzi su [Thrive](https://thrive.dxc.technology/it/?noredirect=it_IT), di cui [uno già pubblicato](https://thrive.dxc.technology/it/2018/10/10/per-salvare-il-mondo-non-serve-nemmeno-avere-un-ufficio/), che ruotano attorno al tema della trasformazione digitale. È la nuova parola d’ordine, il Grande Obiettivo delle aziende. Peccato che nessuno abbia veramente capito che significa. Al momento serve come scusa per vendere hardware e software o per tagliare i costi interni in modo cieco. Altrimenti, per fare danni a gente che non c’entra. Nessuno dimostra questa situazione meglio dell’azienda più inetta e noncurante che esista: la macchina statale.

Varie categorie professionali devono compilare le schede carburante, documenti pensati nel pre-Cambriano con la scusa di permettere detrazioni sui consumi, con il motivo vero di consentire i maneggi più ribaldi e con il risultato effettivo di gravare inutilmente sui tempi e sulle disponibilità delle persone che lavorano.

Che cosa fa lo Stato allora? Introduce l'obbligo della registrazione elettronica, naturalmente con le migliori intenzioni. E compagnie come [Q8](https://www.q8.it) varano lodevolmente siti completi di funzioni per passare dagli scontrini alle fatture digitali.

Fai il pieno quindi e, arrivato a casa, ti disponi a registrare lo scontrino. Mai più foglietti con i timbri, eccoci nel XXI secolo.

*La data dello scontrino deve essere inferiore a quella del giorno corrente*.

Traduzione: puoi registrare scontrini solo il giorno dopo che hai fatto il pieno. Ah, il brivido futurista dei sistemi telematici veloci.

Ma aspetta: sono le 22. Si lavora fino a tardi, vuoi vedere che dopo mezzanotte cambia la data sul server e si aggira l’ostacolo?

Il server lavora con un fuso orario suo e cambia data solo all’una, ma lo fa. Perfetto, è il momento della digitalizzazione.

*Nessuna chiusura cassa è stata ancora effettuata.*

Affascinanti questi messaggi di errore, esplicativi, pensati apposta al servizio dell’utente finale, eppure esposti al mistero dell’azione di forze sconosciute. Pensandoci, probabilmente è il benzinaio che deve effettuare una chiusura di cassa manuale, magari giornaliera, dopo la quale il server sarà pronto ad accettare informazioni. E il benzinaio, per motivi propri, la eseguirà la mattina dopo.

Che c’entra lo stato, si dirà? C’entra, c’entra, proprio perché inetto e noncurante. Una rete digitale funziona – e la trasformazione digitale si dispiega – se è stata ripensata da zero per evitare qualunque forma di attrito. Altrimenti la sua efficacia è solo marginalmente migliore della rete vecchia e l’investimento è stato abbastanza inutile.

Lo stato butta lì obiettivi e obblighi senza curarsi della rete che li deve elaborare. Nel caso specifico, nessuno ha alleggerito il carico amministrativo dei benzinai (e non parliamo della programmazione del sito). Il risultato finale è una forma diversa di inefficienza e frustrazione per l’utente finale.

Se Q8 avesse colto l’occasione per ripensare a modi più astuti di gestire la cassa dei distributori. Se lo stato avesse colto l’occasione di sollevare l’utente finale da un obbligo privo di senso in un mondo telematico. Invece è stato commesso l’errore capitale di appoggiare il digitale sopra una rete analogica e conservarne le procedure. Soldi buttati e tempo dilapidato. Tutto è cambiato, nulla è cambiato. Moriremo inefficienti.

Un ragazzino delle medie appena guidato da un buon insegnante mette a punto un programmino che unisce targa, quantità e importo in una stringa da cui si produce uno *hash* che ne garantisce univocità e genuinità. Da qui in avanti lascio il lavoro a quelli che lo dovrebbero saper pensare, prima ancora di farlo o di chiederlo.