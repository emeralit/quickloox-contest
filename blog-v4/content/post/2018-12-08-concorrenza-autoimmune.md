---
title: "Concorrenza autoimmune"
date: 2018-12-08
comments: true
tags: [Edge, Safari, WebKit, Chrome, Blink, Chromium, Google, Microsoft, Explorer]
---
Dopo avere tentato di condannarci tutti al monopolio di [Explorer](http://macintelligence.org/blog/categories/explorer/), Microsoft ci ha riprovato con una nuova ricetta, Edge, che però è arrivata fuori tempo massimo. Gli sviluppatori del web si preoccupano che le cose funzionino su Chrome, magari su Firefox, e del resto poco si curano. Safari è progenitore di Chrome, costruito su una base di codice parzialmente sovrapposta, e il più delle volte funziona.

Il browser Microsoft viene ignorato o trascurato, una variante di *andarono per suonare*.

La cosa curiosa è che da più parti si levano lamenti perché il mondo dei browser perde un’alternativa e quindi c’è meno concorrenza. Perfino [da parte di Firefox](https://blog.mozilla.org/blog/2018/12/06/goodbye-edge/), che rappresenta una concorrenza inesistente al tempo di Explorer. Quando potevi essere escluso da un sito anche importante, anche quello della tua banca per dire, se non ti conformavi.

Sarebbe stato gradevole sentire qualche vice a favore della concorrenza durante quei tempi bui.

La conseguenza è peggiore di quello che si può immaginare. Siccome Microsoft non è più in grado di fare concorrenza a Chrome (come fa Firefox, retto da una fondazione senza scopo di lucro), intende [contribuire alla sua base di codice](https://blogs.windows.com/windowsexperience/2018/12/06/microsoft-edge-making-the-web-better-through-more-open-source-collaboration/) open source.

Vale a dire che cercherà di condizionare lo sviluppo di Chrome dall’interno invece che proporre qualcosa di diverso e più valido.

Un po’ come quelle malattie dove il sistema immunitario attacca l’organismo che invece dovrebbe proteggere.