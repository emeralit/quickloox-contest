---
title: "Realtà virtuale"
date: 2013-10-11
comments: true
tags: [Mac, OSX, Mavericks]
---
Qualcuno ricorderà che il mio Mac si mantiene rigorosamente *Microsoft-free*, ma la passione per il gioco di ruolo mi obbliga a usare un sito che richiede il maledetto *plugin* Silverlight, proprio di Microsoft, per Safari.<!--more-->

Per salvare capra e cavoli installo Silverlight dentro OS X… virtualizzato in [VirtualBox](https://www.virtualbox.org). Così il morbo rimane confinato in una scatola inviolabile e il computer resta libero, un po’ come quei biologi che maneggiano patogeni potentissimi in camere di isolamento.

Per farla breve, solitamente VirtualBox salta per aria quando OS X inizia a cambiare sotto il cofano, oppure diventa difficile installare OS X in forma virtualizzata.

Ora posso invece confermare che per quanto riguarda Mavericks, VirtualBox funziona perfettamente; e virtualizzare una copia di Mavericks richiede un pizzico di destrezza, esattamente la stessa che serviva per Mountain Lion: seguire le istruzioni presenti presso [OS X on OS X](http://ntk.me/2012/09/07/os-x-on-os-x/) per creare un disco di avvio contenente una certa estensione che serve a fare proseguire il processo di installazione oltre un punto critico.

Ricordo che la licenza delle nuove versioni di OS X permette di virtualizzare fino a due copie del sistema operativo legittimamente posseduto. E che la virtualizzazione è una realtà estremamente comoda per avere a disposizione una copia di sistema operativo dove, malissimo che vada, butti via un file e riparti senza problemi.