---
title: "E le locuste"
date: 2020-04-07
comments: true
tags: [iPad, Android, Police]
---
Certo che se *Android Police* titola [Fatti un favore e comprati un iPad durante il lockdown](https://www.androidpolice.com/2020/04/04/do-yourself-a-favor-and-buy-an-ipad-during-lockdown/), significa che non è la pandemia, ma lo svolgersi delle profezie dell’Apocalisse. La fine del mondo è vicina e questi sono i segni.
