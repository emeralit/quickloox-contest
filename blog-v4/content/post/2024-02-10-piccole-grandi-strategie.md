---
title: "Piccole grandi strategie"
date: 2024-02-10T00:22:43+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Wesnoth, Battle for Wesnoth, Freeciv, Freeciv Go]
---
Non era mai successo che dovessimo convertire all’ultimo momento una serata di *boardgame* in presenza in un ritrovo online.

Nel farlo ho scoperto che, se [Battle for Wesnoth](https://apps.apple.com/it/app/battle-for-wesnoth/id575852062) su iPad è da tempo fermo alla versione 1.14.13 (gioco su quella), la sua [controparte desktop](https://www.wesnoth.org) ha ricevuto un aggiornamento non banale, per quanto di manutenzione. Mi sono anche reso conto che da tempo la versione iOS è diventata gratuita; la raccolta fondi tramite la distribuzione del software è demandata a [Steam](https://store.steampowered.com/app/599390) e [itch.io](https://wesnoth.itch.io/battle-for-wesnoth). Chi vuole lasciare un contributo monetario a [uno dei più bei giochi di strategia e tattica](https://macintelligence.org/search/?query=wesnoth), con una buona community e sviluppo costante anche se lento, sa dove andare.

All’opposto, ho sistematicamente praticato [Freeciv](http://www.freeciv.org) da desktop pensando che su iPad ci volesse la [versione per browser](https://www.fciv.net). Invece esiste [Freeciv Go](https://apps.apple.com/it/app/freeciv-go/id1514658494?l=it), sempre gratis come tutto il resto. Grande aggiunta a uno [strategico gestionale eccellente](https://macintelligence.org/tags/freeciv/), curato, longevo. Anche in questo caso, se investiamo un euro l’anno nel software libero, questo è un candidato valido.

Parlando di software libero, ho sentito recentemente e di nuovo che non sarebbe possibile pubblicare software open source su App Store. Wesnoth è tutto [GPL e Creative Commons](https://wiki.wesnoth.org/Wesnoth:Copyrights); FreeCiv è [GPL](https://freeciv.fandom.com/wiki/License). Di sicuro non sono giochi che muovono l’opinione pubblica o che condizionano la strategia di una multinazionale; contemporaneamente, sono progetti più che sufficientemente complessi per ricevere una revisione consapevole da parte degli editor di App Store.

(Né gratis né open, però in pochi – praticamente una élite – giochiamo regolarmente a [Battle for Polytopia](https://polytopia.io) ed è sempre gradita nuova compagnia, purché con *sense of humor* e *fair play*).