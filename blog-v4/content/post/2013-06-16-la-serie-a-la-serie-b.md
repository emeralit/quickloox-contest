---
title: "La serie A, la serie B"
date: 2013-06-16
comments: true
tags: [Apple, Jobs, iBooks]
---
Presente quell’adagio per cui Steve Jobs non c’è più e quindi Apple ha esaurito la spinta creativa e non sa più innovare? Quella cosa ripetuta ovunque che ha fatto dire a Phil Schiller sul palco del Wwdc *can’t innovate anymore my ass*?<!--more-->

Eddy Cue, responsabile media di Apple, sta testimoniando a un processo e gli atti <a href="http://www.tuaw.com/2013/06/14/steve-jobs-didnt-want-an-ibookstore-until-eddy-cue-convinced-hi/">raccontano</a>, riepiloga *The Unofficial Apple Weblog*, che fu lui a spingere Jobs verso l’apertura ai libri elettronici su iPad:

>[Steve] non era interessato. […] Gli dissi perché pensavo che iPad sarebbe stato un grande apparecchio per i libri elettronici… e dopo qualche discussione tornò e mi disse che avevo ragione, e da lì iniziò a concepire idee su che cosa fare e come sarebbe stato perfino meglio se oltre a un lettore fosse stato anche un negozio.

Forse Cue si fa bello in tribunale a spese del suo vecchio capo impossibilitato a difendersi? Può darsi, certo non aveva paura a esporsi. A gennaio 2011, molti mesi prima della scomparsa di Jobs, <a href="http://news.cnet.com/8301-13579_3-57486733-37/heres-apples-e-mail-thread-about-a-7-inch-ipad/">aveva scritto</a> agli altri dirigenti di Apple per convincerli dell’opportunità di creare un iPad da sette pollici.

Proprio Jobs <a href="http://www.businessweek.com/smallbiz/news/coladvice/book/bk981106.htm">diceva</a>, parlando di persone da assumere:

>Molto meglio cercare la crema della crema. È quello che abbiamo fatto. Poi puoi costruire una squadra che attira giocatori di serie A+. Un piccolo gruppo di giocatori A+ può far mangiare la polvere a un grande gruppo di giocatori di serie B e C. È quello che abbiamo tentato di fare.

Per inciso, Apple <a href="http://paidcontent.org/2013/06/12/apple-we-have-20-percent-of-the-u-s-ebook-market/">dichiara in tribunale</a> di detenere il 20 percento del mercato dei libri digitali. Partendo da zero e onestamente occupandosi prioritariamente di un sacco di altre cose prima di questa, con un concorrente come Amazon per tacere del resto, non sembra un brutto risultato.