---
title: "Ogni promessa è debito formativo"
date: 2022-02-12T00:54:12+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Come [anticipato](https://macintelligence.org/posts/2022-02-08-e-tutto-un-prepararsi/), ecco una piccola novità per questo blogghino: si è tornati all’esperienza estetica e funzionale maggiormente rappresentativa di QuickLoox.

Sotto il cofano non c’è [Octopress](http://octopress.org), come quando ero partito; il motore è rimasto quello di [Hugo](https://gohugo.io), adottato a fine 2021 per ragioni di urgenza tecnica, che mi piace fino a un certo punto ma ha il merito innegabile di avere funzionato al primo colpo, vantare una comunità a supporto, presentare documentazione decente, essere velocissimo e ragionevolmente completo.

Ringrazio **Mimmo** che ha svolto un lavoro straordinario e prezioso, con gentilezza e disponibilità per le quali non ho parole. Si è offerto di dare una mano e, con semplicità e precisione, lo ha fatto: ha risolto varie cose che non avrei saputo fare, ha fatto più velocemente e meglio quello che avrei saputo fare, ha reso tutto maneggevole e tranquillo.

Il punto di partenza è un uovo di Colombo, di quelli ovvi… dopo, quando qualcuno altro ci ha pensato: parallelamente al motore che pubblica, si è lavorato su una copia del motore, condivisa attraverso [GitLab](https://gitlab.com/), sulla quale applicarsi serenamente facendo tutti gli esperimenti del mondo senza patema alcuno di fermare la pubblicazione o fare danni su quello che c’è già.

In rete esiste un framework per trasferire il tema principale di [Octopress su Hugo](https://github.com/parsiya/hugo-octopress), che ha fornito le basi di lavoro. Si è limato qua e là e siamo a un punto di qualità e soddisfazione che reputo sufficiente per adottarlo pubblicamente.

Va da sé che la responsabilità di qualsiasi cosa non funzioni è inesorabilmente mia.

Il lavoro non finisce qui. Intanto, nonostante le attenzioni messe, è possibile che ci sia qualcosa che non funziona; i commenti funzionano, basta dirlo che lo si mette all’ordine del giorno.

Può darsi che manchi qualcosa; uguale, i commenti sono lì ad aspettare.

Se qualcosa andasse modificato per il meglio, ancora una volta, il mezzo per segnare un parere, una proposta, un’idea è sempre quello.

Intanto, preparo l’importazione dei vecchi post. L’obiettivo è ovviamente avere online tutto il pregresso disponibile. Ci sono *header* da uniformare, codici interni agli articoli da convertire, ma niente di problematico e si farà. Una volta uniformato tutto il *corpus* dei post, sarà ancora più semplice affrontare un eventuale cambiamento futuro. È per via di questo se ancora i riferimenti a categorie e tag mancano, per dire; lo stesso si può dire dell’archivio. Sono cose note
e si farà in modo di arrivarci.

Sono contento e grato, a chi ha lavorato, a chi segue con pazienza e partecipazione, a chi segue senza dirlo, a chi ha seguito in passato e magari si è perso, a chi si arrabbia, a tutti. Se può sembrare che la qualità dell’esperienza del blog non fosse considerata importante, ecco, ci sono tutte le ragioni. Semplicemente la vita ha le sue priorità e io i miei difetti. Ma è mancata l’applicazione pratica, non la considerazione per la sua priorità.

Basta lagna, si va avanti. Oltre agli obiettivi dichiarati ho sempre il sogno di bloggare in Lisp, ma – se tornerà – tornerà solo a parità di funzioni e di opzioni, come minimo. Una cosa che ho imparato è che sbagliare si può, sempre; mai, invece, regredire.

Ho imparato poi tante altre cose, alcune tecniche e altre meno. Imparare è uno degli obiettivi personali principali di queste pagine. Collaborare è un sistema formidabile per portare a galla il proprio debito formativo, che è la prima condizione per imparare.

Finalmente, comunque, qualche promessa significativa la si è mantenuta. 😅
