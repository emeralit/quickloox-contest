---
title: "Lo scontrino sui prezzi"
date: 2016-11-06
comments: true
tags: [MacBook, Pro, Ssd, i7]
---
Dissento su vari aspetti dello [scontento di Sabino verso i nuovi MacBook Pro](https://melabit.wordpress.com/2016/11/03/tutto-qui-lhardware/), ma trovo le sue argomentazioni degne di discussione e di rispetto.

Mi sembrano invece gonfiate e in buona parte pretestuose le polemiche sugli aumenti di prezzo. In quanto prendono lo spunto dai prezzi, beh, di quindici giorni fa. Perché il metro dovrebbe essere questo? Ibm stima una durata media di quattro anni per i suoi [novantamila Mac](http://www.apogeonline.com/webzine/2016/10/27/ibm-contro-labitudine). Perché non si fanno i calcoli sul prezzo di tre o quattro anni fa, per dire? Ritengo che chi cambia Mac ogni volta che esce, appena esce, un nuovo modello rappresenti una percentuale minima del totale della comunità.

Non ho voglia di cercarmi i prezzi di quattro anni fa; per amore della provocazione, posso parlare del mio [MacBook Pro di *otto* anni fa](https://support.apple.com/kb/SP503?viewlocale=it_IT&locale=en_US) (il prossimo febbraio).

In quel lontano 2009 ho comprato il top di gamma portatile, pompato al massimo delle specifiche, spendendo circa tremila euro. Per arrivare alla configurazione attuale ho acquistato separatamente quattro gigabyte di Ram pagati più o meno ottocento euro ai tempi, e più di recente un disco Ssd pagato circa cinquecento euro. Totale, 4.300 euro.

Se confronto le specifiche, scopro che oggi, alla cifra essenzialmente identica di 4.139 euro, [porterei a casa quanto segue](http://www.apple.com/it/shop/buy-mac/macbook-pro?product=MLH42T/A&step=config#):

- processore i7 quad-core contro un Core 2 Duo dual-core, con un miglioramento (a spanne) superiore al 100 percento nelle prestazioni;
- scheda video da quattro gigabyte di memoria, da cui mi aspetto ragionevolmente oltre il 100 percento di prestazioni in più sulla mia scheda attuale da 512 megabyte, quattro volte meno;
- schermo Retina, con grossolanamente il 100 percento dei pixel in più;
- sedici gigabyte di Ram invece di otto gigabyte, un miglioramento del 100 percento;
- Ssd da un terabyte al posto di quello da 512 gigabyte, cioè il 100 percento in più di capacità;
- *trackpad* con superficie doppia (+100 percento).

Posso tralasciare le [prestazioni dei nuovi dischi Ssd](http://actualapple.com/the-new-15-inch-macbook-pros-got-the-fastest-in-the-industry-ssd/) che sono i più veloci sul mercato, lo [schermo *wide color*](http://appleinsider.com/articles/16/10/28/first-on-imac-ipad-pro-iphone-7-apples-vibrant-wide-color-tech-now-comes-to-macbook-pro) prezioso per la grafica e la fotografia, Touch Bar, iWork (che al tempo si pagava mentre nelle macchine attuali è incluso) e il risparmio in peso e spessore per affermare senza timore di smentite che *otto anni dopo posso avere il doppio di tutto alla stessa cifra di otto anni fa*. Inflazione e tassazione esclusa.

Lo scontro sui prezzi, da questo angolo di visuale, mi pare appunto piccolo piccolo. Uno scontrino.
