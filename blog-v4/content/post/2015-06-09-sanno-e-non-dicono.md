---
title: "Sanno e non dicono"
date: 2015-06-10
comments: true
tags: [iPhone, One, M9, Htc, Android, Motherboard]
---
Dice che il tale aggeggio Android ha una ottica super e fotografa a millemila megasuperpixel e costa meno.<!--more-->

Tuttavia, scrive *Motherboard*, è solo metà della faccenda. Il software svolge l’altra metà ed è importante. C’è un’azienda che cura allo spasimo l’integrazione tra hardware e software, indovina quale? E invece una fila di altre che sbattono lo stesso software senza troppi complimenti sui modelli più disparati e come va va. Effettivamente costano meno.

Difatti l’articolo di *Motherboard* si intitola [Perché le fotocamere sui telefoni Android continuano a fare schifo](http://motherboard.vice.com/read/why-android-camera-phones-still-suck).

Ci sono eccezioni, per carità. Che confermano la regola. L’articolo abbonda in immagini e spiegazioni che dovrebbero togliere ogni dubbio a una mente lucida.

D’altronde Htc presenta il nuovo One M9 in oro a ventiquattro carati e ne pubblica una bellissima immagine. [Scattata con un iPhone](http://venturebeat.com/2015/06/05/htc-introduces-24-karat-gold-one-m9-using-a-photo-taken-with-an-iphone/).

<blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr">.<a href="https://twitter.com/htc">@htc</a> <a href="https://twitter.com/ChampionsLeague">@ChampionsLeague</a> Taken with an iPhone...🐸☕️ <a href="http://t.co/kHcFdE6jIu">pic.twitter.com/kHcFdE6jIu</a></p>&mdash; o storey (@olliestorey) <a href="https://twitter.com/olliestorey/status/606806465744146433">June 5, 2015</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Loro lo sanno. E non lo dicono.

(Tanto che in seguito hanno cancellato il loro *tweet* originale, conservata per i posteri nell’articolo linkato).