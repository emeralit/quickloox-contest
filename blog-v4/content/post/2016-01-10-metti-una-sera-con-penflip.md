---
title: "Metti una sera con Penflip"
date: 2016-01-10
comments: true
tags: [Penflip, Frix01, Git, BBEdit, BitBucket, Markdown]
---
Su suggerimento di **frix01** ho provato a scrivere uno dei pezzi scorsi con [Penflip](http://www.penflip.com/), un sistema di scrittura collaborativa e controllo delle versioni che fa uso di [Git](https://git-scm.com) per l’uno e l’altro scopo.<!--more-->

I lati belli sono l’interfaccia, semplice e gradevole, e la leggerezza di tutto l’ambiente. Diversamente da molte applicazioni dentro-il-browser, tutto è sempre scattante e immediato. Ho scritto uno dei *post* per il blog e tutto è andato molto fluidamente e tranquillamente. Non c’è fatica di scrivere in Penflip come può accadere a volte dentro Google Docs, non perché sia lui (a me piace), ma per fare un esempio conosciuto da molti.

Si può migliorare in un paio di aspetti che forse sono troppo semplici: la scelta dei font (due è un numero essenziale, però sarebbe stata gradito il numero perfetto, con l’aggiunta di un font monospaziato alle opzioni con grazie e senza grazie) e l’intera faccenda del controllo versione.

La quale funziona in modo impeccabile e semplice, intendiamoci; ma non per merito di Penflip, che si limita a dare le istruzioni necessarie. Niente di diverso da quello che ho in piedi con [BBEdit](http://www.barebones.com/products/bbedit/) e [BitBucket](https://bitbucket.org) e soprattutto niente di diverso da quello che toccherebbe fare a chiunque si avvicinasse alla materia, indipendentemente dall’uso di Penflip. Va bene così e in pochi minuti si parte, però da un servizio via web mi sarei aspettato un po’ più di integrazione.

C’è una sola cosa che non capisco e non mi piace: la necessità di salvare (!) manualmente i documenti. Trovo la scelta anacronistica e francamente me ne stavo pure dimenticando. Non so quando mi sia accaduto l’ultima volta di salvare un documento senza che ci pensasse il sistema, da solo, qualunque fosse il sistema.

Di innovativo c’è l’idea di rendere pubblici i documenti realizzati da chi usa la piattaforma gratuitamente. In molte situazioni la confidenzialità è inutile e Penflip può diventare così anche una risorsa da consultare per trovare informazioni interessanti.

Lo consiglio a chi voglia accostarsi alla scrittura collaborativa, o al controllo di versione, o ad ambedue le materie. Con Penflip si arriva davvero al prodotto finito senza perdersi in tecnicismi e le operazioni necessarie per prendere contatto con Git sarebbero le stesse in qualsiasi altro ambito, quindi meglio affrontarle qui dove c’è anche una interfaccia comoda per scrivere.

Ci sarebbe da propagandare anche l’utilizzo, qui assolutamente consigliato e supportato, di [Markdown](https://daringfireball.net/projects/markdown/), ma credo sia meglio scriverne in un *post* dedicato.

Non è abbastanza per convincermi a cambiare il mio flusso di lavoro, proprio perché le basi di Git le ho già affrontate; tuttavia l’esperienza di utilizzo è rinfrescante e fa capire come le *web app* possano essere oggi produttive in modo piacevole.