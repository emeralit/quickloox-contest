---
title: "Il lupo travestito"
date: 2014-03-22
comments: true
tags: [Microsoft, OneNote, Windows, Word, Excel, PowerPoint, Office, Linux, Bing, censura, Cina, Graham]
---
Mi dice che Microsoft è cambiata, più aperta, più rispettosa degli standard, meno monopolista. Il tutto perché su Mac App Store è apparso [OneNote](https://itunes.apple.com/it/app/microsoft-onenote/id784801555?l=en&mt=12) ed è pure gratis, perché non provarlo?<!--more-->

Perché aspetto di capire se il lupo ha messo i panni dell’agnello perché vuole o perché deve. Se si contano le unità iOS, Apple ha venduto nel 2013 [quasi lo stesso numero](https://macintelligence.org/posts/2014-01-15-parita/) di computer venduti con sopra Windows. Se si contano gli apparecchi personali capaci di collegarsi a Internet, Windows è passato in pochi anni [dal dominio assoluto alla minoranza](https://macintelligence.org/posts/2013-07-25-il-partner-inaspettato/).

La presa di Microsoft alla gola del mercato sta svanendo. Il mondo, finalmente, sta cambiando.

Nonostante questo, i desktop [continuano a essere Windows](http://www.netmarketshare.com) per il novanta percento. Internet Explorer continua a essere oltre il cinquanta percento del traffico Internet. Negli uffici [la gente continua ad accendere Word, Excel, PowerPoint](http://www.technologyguide.com/feature/cloud-based-office-suites/) nonostante tutto. L’azienda riesce a sbagliare un sacco di cose e accumulare ugualmente [camionate di miliardi](http://www.microsoft.com/investor/EarningsAndFinancials/Earnings/PressReleaseAndWebcast/FY14/Q2/default.aspx), seduta sugli allori di Windows e Office. Apple fa miliardi solo se fa giuste tutte le cose importanti; Microsoft fa miliardi comunque vada.

Perché vuole o perché deve? Installare Linux su un PC venduto con Windows [non è sempre semplicissimo](http://www.eightforums.com/installation-setup/19739-before-you-dual-boot-truth-about-ms-oem-s-linux.html) ed è facile vedere lo zampino di Microsoft, l’azienda che ha intrappolato un tizio colpevole di avere diffuso in anticipo Windows 8. Lo ha trovato [frugando nella sua posta Hotmail](http://www.zdnet.com/how-microsoft-tracked-down-a-spy-who-leaked-its-secrets-7000027545/). Microsoft legge la posta delle persone sui suoi servizi.

Bing, il motore di ricerca Microsoft, [applica più censura in Cina dei motori di ricerca governativi cinesi](https://docs.google.com/file/d/0B8ztBERe_FUwdkVPbWxkMHJ2ZG8/edit). Nokia, dopo la [cura Microsoft](https://macintelligence.org/posts/2013-09-03-mission-accomplished/), è una morta che cammina.

Si potrebbe andare avanti a lungo. Prima di fidarmi nuovamente di Microsoft, aspetterò che sia il venti percento. Del desktop, dei *browser*, del software per l’ufficio e così via. Voglio la concorrenza, non un monopolista in ginocchio che cerca di rialzarsi e dispone di denaro infinito senza fare niente o quasi.

Altrimenti rimango ad aspettare che si concretizzi la [visione di Paul Graham](http://www.paulgraham.com/microsoft.html). E OneNote neanche se mi pagano.