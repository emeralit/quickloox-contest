---
title: "Realtà distanziata"
date: 2021-06-18T13:53:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [The Motley Fool, Travis Holm, Apple, AR, Lidar, Asymco, iPhone, iPad] 
---
Ogni tanto fa bene alzare lo sguardo e puntare verso l’orizzonte. Articoli come [Apple sta già costruendo un futuro di realtà aumentata](https://www.fool.com/investing/2021/06/17/apple-building-augmented-reality-future-ar-vr/), su *The Motley Fool*, fanno vagare la mente sul lungo periodo, che è una cosa buona.

>In questi ultimi trimestri è diventato chiaro come Apple stia costruendo davanti ai nostri occhi le fondamenta della propria strategia di realtà aumentata. I Lidar presenti negli iPhone e iPad di oggi aumentano la precisione e la fedeltà della realtà aumentata sugli apparecchi e Apple sta già creando un ecosistema di app e strumenti per gli sviluppatori.

A leggerne così sembra una cosa grossa, molto grossa, che impatterà significativamente. Sono molto scettico sulla seconda parte e non credo che ci sarà tutto questo impatto. Concordo maggiormente sul fatto che sia una cosa grossa, solamente non destinata a spostare equilibri.

Devo tuttavia stare all’erta: se Asymco è d’accordo su una proiezione a lunga scadenza, quella proiezione merita doppia considerazione. E Asymco ha detto [Yup](https://twitter.com/asymco/status/1405535664188264449).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">“What&#39;s become clear in the last few quarters is that Apple is building the foundation of its AR strategy right before our eyes.” Yup. <a href="https://t.co/5ypqz7tp91">https://t.co/5ypqz7tp91</a></p>&mdash; Horace Dediu (@asymco) <a href="https://twitter.com/asymco/status/1405535664188264449?ref_src=twsrc%5Etfw">June 17, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*