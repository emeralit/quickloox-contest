---
title: "Grandi amatori"
date: 2013-06-29
comments: true
tags: [iPad, iPhone]
---
Research In Motion, la società canadese nota nel mondo per Blackberry, è una realtà interessante nonostante fortune recenti, diciamo, alterne: nel [primo trimestre 2013](http://www.marketwire.com/press-release/blackberry-reports-first-quarter-fiscal-2014-results-nasdaq-bbry-1806946.htm) ha venduto 6,8 milioni di telefoni intelligenti, non esattamente i 37,4 milioni di iPhone, ma una buona cifra (due BlackBerry ogni undici iPhone).<!--more-->

Il loro computer a tavoletta Playbook ha invece venduto solo centomila esemplari. È un dato misero, rispetto ai [19,5 milioni di iPad](http://www.apple.com/it/pr/library/2013/04/23Apple-Reports-Second-Quarter-Results.html) (un Playbook ogni centovantacinque iPad). L’hardware è vecchio di due anni, come se Apple avesse ancora iPad 2 come modello di punta. L’azienda [aveva promesso](http://arstechnica.com/gadgets/2013/01/blackberry-playbook-tablet-will-get-blackberry-10-update/) a gennaio un aggiornamento al nuovo sistema operativo BlackBerry 10, che invece [non arriverà](http://arstechnica.com/gadgets/2013/06/blackberry-backtracks-wont-release-bb10-playbook-update-after-all/).

Playbook era stato annunciato nel 2011 con una [campagna pubblicitaria](http://mashable.com/2011/05/06/playbook-amateur-hour/) che annunciava *Amateur Hour Is Over*, l’ora del dilettante è terminata. Nel 2011 esisteva significativamente solo una tavoletta e quindi non è difficile capire chi fosse tacciato di amatorialità e dilettantismo.

Si direbbe, due anni dopo, che i grandi amatori ricevano più soddisfazione dei professionisti, specie sedicenti.