---
title: "Non-recensioni: macOS Mojave"
date: 2018-11-26
comments: true
tags: [Mojave, macOS, ElCapitan, High, Sierra]
---
Ero rimasto indietro di oltre un giro, lavorando negli ultimi anni con El Capitan, alias macOS 10.11. Ho avuto durante l’estate un brevissimo flirt con High Sierra e ora, con [l’arrivo di Mac mini](http://www.macintelligence.org/blog/2018/11/25/non-recensioni-mac-mini-2018/), mi sono ritrovato macOS [Mojave](https://www.apple.com/it/macos/mojave/), 10.14.

Confesso che, durante il breve periodo di High Sierra, sapevo di avere davanti una nuova versione di sistema operativo ma non avrei saputo indicare a bruciapelo che cosa ci fosse di veramente diverso e con Mojave la sensazione è identica, anche se ho dato un’occhiata alla Modalità scura, il *Dark Mode*, e certo non avrei potuto farlo con altre versioni.

È gradevole ma non fa per me, almeno fino a quando arriverà un aggiornamento di BBEdit che ne fa uso. Credo che per molti sia una novità piacevole.

Se dovessi inventare io lo slogan per Mojave, sarebbe *te ne accorgi solo quando ti serve*. Chiami FaceTime e ti accorgi che c’è spazio per trentadue, apri un Pdf e capisci di poterlo manipolare senza doverlo aprire, guardi le caratteristiche del sistema e ti accorgi di poter intervenire su cache, file inutili, Cestino e quant’altro da una unica, benvenuta, meravigliosa finestra. Gli strumenti di modifica disponibili in Visualizzazione rapida presagiscono un futuro sereno dove apri una app solo se serve davvero, il resto lo fa il sistema. E poi dicono il Finder deve migliorare.

Non è più tempo dei rilasci di sistema attesi metà di un lustro per aspettarsi di vedere la vita cambiare, quattordici versioni dopo la prima. Il nuovo obiettivo è migliorarla, la vita, e l’impressione è che tutto il nuovo che sta fuori dal cofano sia stato aggiunto con in testa obiettivi precisi, non solo per fare numero come è successo a volte in passato.

In particolare, l’amministrazione dello spazio su disco è evidentemente rivolta a tutte quelle persone che hanno trovato conveniente ricorrere a un MacBook Air o hanno risparmiato sugli Ssd, per dover far quadrare i conti in un quarto dello spazio che avevano prima.

Qui l’integrazione con iCloud diventa significativa. Lo so, me lo ha detto chiunque, è uno sporco trucco di Apple per fare pagare iCloud invece che regalarlo e io stesso, contestualmente all’aggiornamento, ho attivato l’abbonamento ai cinquanta gigabyte, novantanove centesimi al mese. Ho avuto pochissimi problemi nel sopravvivere con i cinque gigabyte gratuiti; poi però mi è capitato di dover coordinare una sessione di lavoro di due giorni in Polonia, con registrazioni audio, presentazioni, appunti vocali e manuali. L’unico modo di portare a casa un risultato professionale era ampliare lo spazio iCloud e lo prendo come il risultato di un esperimento: se sei un professionista, probabilmente è meglio spendere quei novantanove centesimi al mese.

Posso dire poco altro perché il Finder lo guardo pochissimo. La scrivania dinamica di Mojave è carina. Non ho organizzato i documenti in pile sulla Scrivania perché non organizzo mentalmente in quel modo. Apprezzo molto la continuità di intenti tra Mac e iOS, specie l’aspetto che si possano richiedere prestazioni via Mac. App Store poteva solo migliorare e lo ha fatto.

Ho provato, questo sì, la nuova vista Galleria e non per curiosità: mi tornava comoda. Aggiunta gradita, quindi, non gratuita. I metadati in piena vista non mi sono ancora tornati utili, però so che è solo questione di tempo.

Ritengo che il ruolo di Mojave e in generale di macOS nel momento storico attuale sia proprio questo: migliorare la gestione quotidiana, facilitare i rapporti tra Mac e iOS, integrare il cloud con la scrivania   e naturalmente togliere di mezzo quanti più bug possibile. Al sistema operativo non si chiede più di essere protagonista bensì collaboratore e assistente fidato, del padrone umano e di altri sistemi operativi.

Mi sta bene.
