---
title: "La surrealtà supera la fantasia"
date: 2020-08-21
comments: true
tags: [Fortnite, iPhone, iPad, iOS, Epic]
---
Quando davo la disponibilità al [noleggio di iPad per giocarci a Fortnite](https://macintelligence.org/blog/2020/08/18/ipad-noleggiasi/), scherzavo.

Ora leggo di un’alluvione di [iPhone equipaggiati con Fornite in vendita su eBay](https://www.macrumors.com/2020/08/19/iphones-fortnite-ebay/) appositamente per stare dietro al gioco di Epic.

Penso che molta di questa attività sia fittizia, però sempre maggiore di zero e la trovo surreale.

Diciamo che continuo a scherzare, ma intanto alzo la tariffa di noleggio.