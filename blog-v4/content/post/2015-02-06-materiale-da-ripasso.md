---
title: "Materiale da ripasso"
date: 2015-02-07
comments: true
tags: [Blodget, BusinessInsider, iPhone, Apple, Windows, Android, Google, Facebook, iOS, Samsung, Htc]
---
Henry Blodget titolava nel 2011 da *Business Insider* [Android sta distruggendo tutti, specialmente Rim; iPhone cadavere galleggiante](http://www.businessinsider.com/android-iphone-market-share-2011-4).<!--more-->

Non sono tanto i dati sulle quote di mercato, che vanno e vengono. Sono i ragionamenti, che si sentono sempre uguali nonostante il mondo sia diverso a livello quasi doloroso da prima, da tanto le cose sono cambiate.

>I mercati delle piattaforme tecnologici tendono a standardizzarsi attorno a un singolo soggetto dominante (come Windows nei PC, Facebook nei social, Google nella ricerca). Più dominante diviene la piattaforma, più acquista valore e più è difficile da divellere. Entra in gioco l’effetto rete e gli sviluppatori che producono software per la piattaforma le dedicano sempre più energie. Le ricompense del lavoro su altre piattaforme intanto diminuiscono e gradualmente i programmatori smettono di curarle.

Peccato che il mercato dei computer da tasca non funzioni così. Non si è affatto standardizzato (negli Stati Uniti iPhone ha quasi pareggiato le vendite di Android e Apple ha più o meno pareggiato a livello globale le vendite di Samsung); gli sviluppatori scrivono prima di iOS e poi, se avanza tempo, per Android. iOS ripaga gli sviluppatori molto più di quanto faccia Android, nonostante la sua superiorità numerica.

>Apple sta combattendo una guerra molto simile a quella che ha combattuto – e perso – negli anni novanta. Cerca di costruire prodotto più integrato e di mantenere il controllo sull’ecosistema, ciò che gli permette di costruire prodotti “migliori” ma complica la competizione contro una piattaforma software standard per molti produttori di hardware (Windows vent’anni fa, Android oggi).

Peccato che il mercato dei computer da tasca non funzioni così. I produttori di hardware Android sono in realtà pochi, mentre i produttori di PC erano una pletora; soprattutto, i produttori di PC *guadagnavano*. I produttori di hardware Android, a eccezione di Samsung e Htc se tira il vento giusto, *perdono denaro*.

Intanto Apple ha venduto 74 milioni di iPhone nel trimestre di Natale: record assoluto, mai successo prima, una crescita in unità spaventosamente alta per questi livelli di diffusione.

Il mercato dei computer da tasca non funziona così. Il punto non è tanto *Business Insider* di quattro anni fa, quanto la gente che continua a ripetere le stesse fesserie come se fossero vere. Sono false; ripassare, memorizzare, correggerli.