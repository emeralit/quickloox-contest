---
title: "Giochi e pregiudizi"
date: 2023-11-15T22:15:10+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Blender, Metal]
---
Con tutti i progetti software chiusi e proprietari che lamentano la difficoltà e i costi come scuse per non fornire un supporto Mac di qualità professionale, ogni tanto compare un progetto open source che lo fa e basta.

E così Blender, [non esattamente l’ultimo arrivato alla modellazione 3D](https://macintelligence.org/posts/2023-04-08-un-esempio-a-tre-dimensioni/), [prepara la prossima versione della app con il supporto per Metal](https://code.blender.org/2023/01/introducing-the-blender-metal-viewport/), le API di visualizzazione preparate da Apple per i giochi.

Sarà davvero così difficile, così costoso, così sbagliato? Io penso maggiormente al pregiudizio. All’ignoranza. Alla chiusura. All’ostilità preconcetta.