---
title: "I nuovi mostri"
date: 2022-04-03T02:23:07+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Bucha, Chesterton, Gilbert Keith Chesterton]
---
Riprenderò a parlare di Apple, Internet e tecnologia, domani. Oggi mi sono informato.

Da oggi, più ancora che nei giorni scorsi, chi si pone da quella parte si (s)qualifica da solo.

Chi osi giustificare, ma anche solo fingere una neutralità vomitevole, si pone fuori non solo dalla storia, ma anche dal consesso umano.

Come già in un’altra situazione precedente (e perlopiù sono le stesse, incredibili, insostenibili persone), la misura è abbondantemente colma.

È chiaro che vediamo comportamenti da isolare e reprimere a livello mondiale,  nonché giustificazioni e connivenze incompatibili con la civiltà sui media italiani.

Personalmente non so censurare, non so essere violento oltre che contro le zanzare, non so odiare, non so sostenere scientemente falsità. Forse sono inadatto alla vita nel Ventunesimo secolo.

Alle vittime, agli eroi, agli innocenti dedico [The Ballad of the White Horse](https://www.gutenberg.org/ebooks/1719), di Gilbert Keith Chesterton. Anche ai colpevoli, ai vili, agli sciacalli, ai nuovi mostri di questo pianeta, perché possano pagare un giusto prezzo per la loro bassezza e possano sentire la sconfitta contrargli lo stomaco anche quando sentano o credano o millantino di avere vinto.

Link simbolico, perché inaccessibile dall’Italia (serve una VPN. Mando il testo in qualsiasi formato esistente a chi lo chieda). Ciò che meriterebbe libertà di diffusione viene soffocato, contrariamente ad altro per cui mai sosterrò la censura, ma che lavorerò per contrastare. Ogni parola indegna va battuta con qualcosa di migliore. Ogni persona indegna va affrontata dialetticamente con la forza dei fatti, che funzioni o meno. È un dovere morale da oggi, se non lo fosse stato anche prima.

Non so che cosa vedrò domani o il mese prossimo. I mostri, tuttavia, perderanno. Come certi alberi imponenti dalla corteccia spessa e impenetrabile, marci dentro e privi effettivamente di vita.