---
title: "Insolito solitario"
date: 2016-02-01
comments: true
tags: [Churchill, Rumsfeld, Clash, Clans, Solitaire]
---
Non è il mio genere e diffido dei *battage* pubblicitari.

Eppure, motivato illogicamente dal successo di un attacco quasi magistrale su [Clash of Clans](http://supercell.com/en/games/clashofclans/), ho scaricato e provato [Churchill Solitaire](http://churchillsolitaire.com).<!--more-->

Un’altra cosa da cui sto lontano sono i giochi collaudati che giocano sull’ambientazione, la tombola di Star Trek, il rubamazzo di Robin Hood eccetera.

Dietro Churchill Solitaire c’è Donald Rumsfeld, che è stato un politico ed è stato un politico discusso, due buone ragioni ulteriori per pensare ad altro.

Ho provato un solitario Easy e ho vinto, grazie a tre aiuti e a una concentrazione di livello che mai avrei supposto necessario.

Sono quasi tentato di acquistare la versione Premium, che pure mai giocherò perché la mia passione per i solitari è, al più, flebile.

Assolutamente da provare. Per una volta, qualcuno è riuscito a dare gusto e interesse a un gioco stravecchio, e anche a non disturbare con un’ambientazione che è finalmente un elemento in più da godere e non una scusa per nascondere magagne. Complimenti.

(Il mio clan è sempre [alla ricerca di nuove reclute](https://macintelligence.org/posts/2015-09-30-clan-in-espansione/).