---
title: "Il gioco del se fosse"
date: 2018-04-10
comments: true
tags: [DeLong, iPad]
---
Dave DeLong ha realizzato un post con [dodici iterazioni del concetto *Se iPad fosse fatto per i ragazzi*](https://davedelong.com/blog/2018/04/06/if-ipads-were-meant-for-kids/), con l’intento di dimostrare che non lo è, nonostante la pubblicistica di Apple.<!--more-->

Una delle asserzioni è azzeccata. iPad manca di una funzione di controllo genitoriale come invece ha Mac e le Restrizioni, per quanto facciano molto, non fanno proprio tutto.

Il resto mi sembra pretestuoso. DeLong desidera funzioni come l’installazione remota di app e altrimenti un controllo remoto pressoché completo sull’apparecchio, per evitare che i figli giochino invece di fare i compiti oppure stiano svegli invece di dormire.

Le soluzioni esistente di Mobile Device Management prevedono tutto quello che lui chiede, ma se ne lamenta perché sono faticose da installare e amministrare. Non è vero; certo aggiungono complessità di gestione, ma se vuoi installare app su un iPad remoto e controllarlo sotto ogni aspetto, mi sembra un prezzo inevitabile.

Secondo, i suoi figli sono più grandi dei miei e certo pongono un problema più complicato da affrontare, ma ingenuamente ritengo che la sera l’iPad dovrebbe spegnersi perché c’è una regola in famiglia, non perché c’è una funzione su iPad.

Gli ultimi due capoversi valgono più o meno per tutto il pezzo. Vuole funzioni che sostituiscano la necessità – e certo, la fatica – di educare. Se la prende con il problema dei figli che impostano un codice di sblocco e poi se lo dimenticano, per esempio, e desidera una funzione apposta.

Ho anche l’impressione che non conosca fino in fondo le possibilità delle Restrizioni.

iPad non è fatto per ragazzi. È fatto per *tutti*. Ci sono modi buoni di usarlo e altri meno buoni. Ma vale anche per lo stare a tavola. Sono cose che si insegnano, senza marchingegni che tengano i gomiti appoggiati al tavolo.