---
title: "Risate da paura"
date: 2015-08-08
comments: true
tags: [Angband, RoomScan]
---
Volevo segnalare l’arrivo di [Angband 4.0.1](http://rephial.org) in pieno clima estivo e disimpegnato e invece mi è successa una cosa.<!--more-->

Mi sono trovato a produrre la pianta di un appartamento con [RoomScan Pro](https://itunes.apple.com/it/app/roomscan-pro-app-that-draws/id673673795?l=en&mt=8). Recensione in quarantotto parole: funziona bene se si rimane attenti e concentrati, altrimenti è facilissimo perdere tempo. Il prezzo di 4,99 euro è sopportabile e l’acquisto in-app da 0,99 euro di cinquanta crediti di esportazione delle piante in formati diversi dall’immagine è furbetto. Tuttavia dovrebbe preoccupare solo un professionista del ramo.<!--more-->

Professionisti del ramo. Ne era presente uno, con aggeggio Android di ordinanza, bello grosso, suoneria bella squillante (il gioco di parole è intenzionale). Mi ha chiesto se fosse una *nuova app*. Più o meno: la versione 2.0 è del luglio 2013 e ora gira la 4.17. Gliel’ho mostrata e ho proceduto con la mappa. Appariva molto divertito. Un mio accompagnatore, che non sapeva dell’esistenza di RoomScan Pro, ridacchiava. In effetti l’utilizzo pubblico della *app* può generare imbarazzo: sei in un posto e cominci ad appoggiare iPhone su tutte le pareti ad aspettare ogni volta un *beep*. Devi essere per forza uno strano.

Per esperienze passate, ho benissimo presente gli uffici dove lavora il professionista. Preparano numerosi documenti. A mano, con la carta carbone per i duplicati, sempre su carta anche quando è inutile, e se devono modificare uno di quei documenti riscrivono a mano tutte le parti che non sono prestampate. Su ogni scrivania campeggia un computer, evidentemente ritenuto inutile per lo scopo.

RoomScan Pro approssima la misura delle pareti al decimo di metro e consiglia l’uso assieme a un misuratore laser per avere valori di massima precisione. Su questo è alla pari con il professionista, che per buona misura – il gioco di parole è intenzionale – si porta sempre in auto il metro pieghevole da muratore.

In diversi ridono e mi prendono per strano anche quando gli parlo di Angband (che in versione 4.0.1 è ancora più coerente e stabile e aggiunge piacevolezze come una varietà assai più ampia di stanze). Vedo un’analogia: è il riso di chi sente di stare perdendosi qualcosa e non riesce a capire.