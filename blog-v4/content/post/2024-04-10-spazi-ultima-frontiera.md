---
title: "Spazi, ultima frontiera"
date: 2024-04-10T01:20:41+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Niléane, MacStories, Quitter, Arment, Marco Arment, Siracusa, John Siracusa]
---
Il tema parrebbe banalotto e invece questo [articolo di Niléane su MacStories](https://www.macstories.net/stories/single-space-challenge-trying-to-manage-my-macos-windows-all-in-one-virtual-desktop/) mi ha coinvolto dal primo paragrafo, soprattutto dopo avere letto che John Siracusa ha una opinione forte sul modo di organizzare spazi di scrivania e finestre su Mac.

Niléane era abituata a tenere le proprie app in tre spazi: nello spazio uno le app di comunicazione, nello spazio due il browser, nello spazio tre le varie ed eventuali. Di volta in volta portava una app nello spazio due quando per ragioni di convenienza era opportuno affiancare qualcosa a Safari.

Da lì, seguendo la linea di Siracusa, si è organizzata per avere tutto su uno spazio singolo e restare con un flusso di lavoro efficiente.

Come risultato, ha finito per scoprire Stage manager su Mac e anche una bella utility di Marco Arment di cui non avevo nozione: [Quitter](https://marco.org/apps#quitter), che termina le app predestinate dopo un tempo prefissato di inutilizzo. Applicata specialmente alle app di comunicazione, che restano attive in background anche se vengono chiuse sullo schermo, Quitter è una bella mano a tenere la scrivania ragionevolmente sgombra dalle cose inutili.

Al termine del proprio test, Niléane non sa se restare sul metodo-Siracusa oppure tornare ai suoi tre spazi. In sincerità, non mi ha convinto abbastanza da rinunciare al mio sistema attuale (uno spazio privato mio e poi uno per ciascun cliente, dove una app può trovarsi attiva in più spazi con finestre diverse). Però la lettura è stata interessante e mi ha anche suscitato qualche dubbio rispetto alle mie abitudini. In situazioni come queste si tende a pensare di applicare il miglior metodo possibile unicamente per il fato di praticarlo ogni giorno, esserci abituati e non considerare le alternative. Provare a riguardare l’organizzazione degli spazi scrivania, magari alla luce di questo articolo, è raccomandabile.