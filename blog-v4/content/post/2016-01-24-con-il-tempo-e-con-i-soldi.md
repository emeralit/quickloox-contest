---
title: "Con il tempo e con i soldi"
date: 2016-01-24
comments: true
tags: [Mit, IoT, Udacity, Google, TensorFlow]
---
Varrebbe la pena di iscriversi al [corso del Massachusetts Institute of Technology (Mit) sulla Internet of Things](http://web.mit.edu/professional/digital-programs/courses/IoT/index.html).<!--more-->

Con il tempo e basta si potrebbe studiare il *machine learning* applicato a TensorFlow, il sistema di Google, grazie a [questo corso su Udacity](https://www.udacity.com/course/deep-learning--ud730).

Tanta gente ha problemi di lavoro, di formazione continua, di aggiornamento, di scarsa resa del *business*. Queste sono solo due delle nuove frontiere che hanno niente di fantascientifico e già oggi sono concrete, presenti, applicabili. Chi si fa una posizione oggi in ambiti come questi, entro tre anni dorme tra quattro guanciali e può dare una bella svolta alla propria vita professionale anche in termini di reddito.

Ciò detto, rimango semplicemente affascinato da quanto oggi la rete può fare per chiunque in termini di apprendimento. Il corso del Mit, accidenti, è del Mit, uno degli atenei più prestigiosi al mondo. Si può frequentare anche stando a ventimila chilometri di distanza da Boston. E costa 495 dollari. Certo non sono noccioline; al tempo stesso, quel corso potrebbe valere venti volte la cifra e non parliamo della resa professionale.