---
title: "Primo soccorso"
date: 2022-12-03T17:47:57+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [iPhone 14, SOS, Alaska, MacRumors, iOS, satellite]
---
Ecco che il servizio di messaggio di emergenza via satellite adottato da Apple ha portato al [soccorso di un uomo rimasto a piedi in Alaska](https://www.macrumors.com/2022/12/01/iphone-14-satellite-sos-in-action/). Per giunta a una latitudine dove la funzione [rischia di non trovare satelliti disponibili](https://support.apple.com/en-us/HT213426).

Non c’è prova che il viaggiatore in difficoltà tra Noorvik e Kotzebue non se la sarebbe cavata anche senza iPhone 14. Certamente ha rischiato meno e ha ricevuto aiuto prima. Sembra abbastanza da chiudere il caso ed essere contenti di questo sviluppo di iOS.

Se sentissimo meno *alla fine è solo un telefono*, ne guadagneremo anche in qualità del dibattito. Un progresso non previsto forse dai progettisti, ma sempre benvenuto.