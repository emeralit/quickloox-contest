---
title: "La grande corsa all’innovazione"
date: 2021-08-03T00:40:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Google, Apple, Google Tensor, Arm, Qualcomm, Pixel 6, Pixel 6 Pro] 
---
Leggo che [quest’anno Google abbandonerà Qualcomm e costruirà i propri processori per computer da tasca](https://www.cnbc.com/2021/08/02/pixel-6-will-have-processor-designed-by-google.html).

Ho la sensazione che venga già fatto, da un’altra società, da qualche anno, con discreti risultati, ma non riesco a focalizzarne il nome, magari poi mi viene in mente.

Può anche darsi che proprio i risultati ottenuti da questa società con i suoi processori fatti in casa sia la ragione del cambio di rotta di Google. Che dice anche molto sulla competitività dei processori usati attualmente, forse non proprio ottimale.

Il che parla chiaro anche sulle prestazioni di quello che viene venduto oggi con questi processori.

Da cui si deriva chi stia facendo effettivamente innovazione invece di andare al traino. Se solo mi venisse in mente il nome…

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*