---
title: "Tanto rumore nel nulla"
date: 2020-11-06
comments: true
tags: [iPad, Mlb, Surface]
---
La stagione del baseball americana si è giocata in stadi vuoti, per le ragioni che sappiamo. Tuttavia, per i team in campo e per il pubblico a casa, gli stadi sono stati sonorizzati con rumori di folla.

Viene fuori, grazie a un articolo di *Sports Illustrated*, che l’orchestrazione dei rumori [viene eseguita da un iPad](https://www.si.com/mlb/2020/09/05/baseball-fake-crowd-nosie) in dotazione a ciascuna squadra.

Sembra una sciocchezza, prima di leggere. Invece si scopre che i rumori in gioco sono più di mille, sono sovrapponibili (per un numero di combinazioni possibili altissimo) e ci sono tante sfumature da considerare. Qualche squadra ha applicato personalizzazioni e il tutto è impossibile da attuare senza una supervisione umane, proprio per via delle sfumature.

>Gli stadi cercano di imitare i rumori di un pubblico vero, che è una cosa un po’ differente dall’imitazione di un pubblico ideale.

Da notare che il baseball è un mondo piuttosto conservatore, dove solo di recente hanno iniziato a [compilare i calendari con il computer](https://www.macintelligence.org/blog/2013/11/25/che-combinazioni/), [adottare referti elettronici](https://macintelligence.org/blog/2013/07/18/si-metta-a-referto/), [decidere le tattiche di gioco](https://macintelligence.org/blog/2016/10/19/tavoletta-rasa/) e anche [barare](https://macintelligence.org/blog/2017/09/08/furto-a-orologeria/) (con watch).

Ma appena serve innovazione, corrono e più velocemente che per arrivare in prima base.