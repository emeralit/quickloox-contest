---
title: "E che tasca"
date: 2015-09-27
comments: true
tags: [Gruber, iPhone6s, MacBook, Android, core]
---
Quando scrivo o parlo di *computer da tasca* la reazione migliore che ottengo è essere guardato strano. Ma non lo sai? Hanno un nome, sono *smàrfon*.<!--more-->

Meglio che qualche anno fa, quando non erano computer da tasca, ma *telefonini*, come se il fatto di telefonare fosse più di una comodità tra tante.

John Gruber ha pubblicato su Daring Fireball una monumentale, epica [recensione di iPhone 6s e 6s Plus](http://daringfireball.net/2015/09/the_iphones_6s).

C’è dentro tutto quanto può essere anche remotamente importante sapere prima di un acquisto, o anche solo per curiosità. Se c’è un problema con l’inglese, rispondo a domande specifiche nei commenti, senza problema.

C’è dentro anche il fatto che Gruber ha fatto anche girare anche qualche test di velocità base per i processori. Siccome iPhone ha un processore a doppio *core*, nucleo di elaborazione, non fa scintille se paragonato a certi modelli Android a quattro o perfino otto *core*.

Tuttavia la gran parte dell’uso di uno *smàrfon* è sostenuta da un *core* singolo, anche per questioni di architetture software. E, a *core* singolo, iPhone 6s straccia chiunque. Gruber scrive che è avanti anni sul resto del mercato.

Non sono in grado di confermare o smentire. Ero più interessato a un altro dato: sempre nei test *single core* [iPhone 6s](http://www.apple.com/it/iphone/) batte nettamente l’ultimo [MacBook](http://www.apple.com/it/macbook/).

Una della due: o MacBook non è un computer, o iPhone è un computer da tasca. Certo, viste le dimensioni, una tasca ragguardevole.