---
title: "I ritardi e gli auguri"
date: 2023-04-09T02:18:16+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Pasqua]
---
È un periodo intenso e sono un po’ indietro con il blog. Tanti post aperti e pochi chiusi, per mancanza di tempo o impellenza di questa o quell’altra distrazione.

Però da qualche parte devo mettergli, gli auguri di Pasqua, per le grandi persone più brave di me al punto di trovare il tempo di seguire questa pagina.

Allora li metto subito gli auguri e poi continuerò a recuperare fino a riempire la cronologia dei post sulle date mancanti. Nessuna ingegneria del contenuto: i post esistono già, vanno solo completati.

Uova, colombe, famiglia, [Hearthstone](https://hearthstone.blizzard.com/en-gb) o che altro sia, che sia lieto, sereno e soleggiato, al limite dentro, se il meteo non si concilia.

Buona festa a tutti e sempre grazie.