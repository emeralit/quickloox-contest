---
title: "Ricerche e sviluppi"
date: 2023-06-05T02:14:19+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [Hofstadter, Douglas Hofstadter, Searle, stanza cinese, Chinese room, intelligenza artificiale, Ai, Concetti fluidi e analogie creative, Fluid concepts and creative analogies, mailmasterc]
---
Devo l’ispirazione a **Mailmasterc**, il quale a seguito di un mio commento sulle differenze di vedute tra Douglas Hofstadter e John Searle a proposito della stanza cinese ha chiesto approfondimenti.

La [stanza cinese](https://plato.stanford.edu/entries/chinese-room/) è un argomento portato da Searle contro l’idea di una intelligenza artificiale forte, capace di pensare come gli umani. Nella stanza sta un signore equipaggiato con un manuale. L’unico contatto che ha con il mondo esterno consiste in bigliettini contenenti frasi in cinese. Il signore ignora il cinese e consulta il manuale, che contiene le istruzioni sugli ideogrammi giusti da usare in risposta a quelli che riceve.

Il manuale non è un dizionario e non traduce i segni; indica che altri segni usare per rispondere.

Il signore riceve il bigliettino, consulta il manuale, disegna la risposta su un altro bigliettino e lo consegna all’esterno. Chi sta fuori ha l’impressione di parlare con qualcuno che conosca il cinese, mentre invece dentro si trova solo uno che maneggia perfettamente segni senza alcuna idea del loro significato. La morale è che il computer si comporta come quel signore e se la cava bene con i segni, ma ignora che cosa significhino. Né potrà mai saperlo.

(Le pretese intelligenze generative oggi sulla cresta dell’onda sono precisamente nella situazione del signore di Searle; maneggiano simboli per loro provi di significato ma danno l’idea apparente di comprendere una conversazione o un ordine).

Ero convinto che Hofstadter avesse scritto sulla stanza cinese in [Gödel, Escher, Bach](https://macintelligence.org/posts/2020-10-22-un-libro-in-un-articolo/), mentre parrebbe di no e questo mi mette nei guai, dato che mi tocca fare ricerche su tre o quattro libri. In compenso, saltano fuori frammenti interessanti che non sono ancora la critica diretta dell’argomento di Searle, però aiutano a inquadrare il dibattito.

Qui sotto ripubblico parte di una nota presente in [Concetti fluidi e analogie creative](https://macintelligence.org/posts/2023-03-23-schemi-e-analogie/). La traduzione è mia e potrebbe divergere da quella dell’edizione italiana.

>…la paura, instillata da filosofi biosciovinisti come John Searle, che possano esserci due modi fondamentalmente differenti di passare il test di Turing: non solo da parte di un sistema dotato di vera comprensione e di significati, che possiede l’intenzionalità, ma anche da parte di un sistema i cui simboli sono del tutto vuoti e privi di contenuto; in altre parole, sistemi-zombi, il cui comportamento apparente sarebbe indistinguibile da quello di sistemi dotati di simboli radicati. Questa è la paura primaria suscitata in chi si lascia convincere dall’argomento, ben formulato quanto autocontraddittorio, della stanza cinese.

Arriveranno altri frammenti e, spero, anche il commento dedicato di Hofstadter all’argomento di Searle.
