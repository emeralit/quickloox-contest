---
title: "Lo stage posticipato"
date: 2022-06-26T01:21:11+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [Stage Manager, iPad Pro, Riccardo Mori, Mori, Riccardo, iPadOS, iPadOS 16]
---
Si sta facendo chiacchiera interessante a proposito della [polemica su Stage Manager](https://macintelligence.org/posts/2022-06-17-la-sicumera-degli-stagisti/) e sarà ugualmente interessante vedere che decisioni finali prenderà Apple in proposito: ricordiamo infatti che in questo momento [non c’è neanche la beta pubblica](https://beta.apple.com/sp/betaprogram) di iPadOS 16 e che la maggior parte dei commentatori parla a titolo del tutto teorico. Si è visto nel recente passato che le beta possono portare cambiamenti anche consistenti al software (come l’anno scorso per il campo URL di Safari su iOS) e a oggi si discute di che cosa ha detto Apple, non di quello che farà.

Proprio perché c’è un fattore tempo in mezzo, proporrei di eliminare un elemento della discussione che a mio parere porta solo problemi: l’idea perversa che, siccome c’è una funzione importante in arrivo per l’autunno, sia vitale averla.

Mentre sul software tendo a essere *early adopter* (salvo per avere rinunciato alle beta; senza macchine di riserva da destinare allo scopo, avere beta del sistema operativo su Mac, iPad, iPhone, watch e tv contemporaneamente, quando c’è lavoro da sbrigare, è un po’ troppo), sullo hardware acquisto quando è imprescindibile farlo: il mio iPhone è un iPhone X (che mi hanno regalato, per giunta). Il mio iPad Pro è del 2019. Mac e watch sono stati acquistati quest’anno, però il primo era imprescindibile e il secondo è arrivato dopo sei anni dall’acquisto precedente.

Questo vuole dire che per me è completamente normale, a un certo punto della vita delle macchine, vedere uscire novità software che non userò fino a un acquisto successivo.

In altre parole, l’uscita di Stage Manager aggiunge lo zero assoluto alle mie decisioni di acquisto. Ero soddisfattissimo del mio iPad Pro il giorno prima di Wwdc e lo sono rimasto il giorno dopo. Se e quando comprerò un nuovo iPad Pro, la presenza o meno di Stage Manager influenzerà zero la mia azione (mentre, per dire, la situazione della figlia che comincia ad avere bisogno di un computer per la scuola è infinitamente più rilevante).

Quello che esce domani non cambia la bontà di quello che sto usando oggi, altrimenti devo guardarmi allo specchio e prenotare un elettroencefalogramma; se sto usando la macchina sbagliata, mica è Craig Federighi che me lo deve dire nel *keynote*.

Ecco, smettiamo di parlare di Stage Manager come se per forza tutti lo dovessero adottare a ottobre, pena un ipotetico declassamento a utente di serie B. **Riccardo** ha puntualizzato in un suo [post molto critico](http://morrick.me/archives/9582) che non tutti cambiano iPad ogni uno-due anni (è una minoranza a farlo, aggiungo io). Discutiamo di Stage Manager, se sia buono o cattivo, se debba esistere solo per M1 o anche per processori inferiori, e con quali limitazioni, ci mancherebbe altro. Rendiamoci conto che c’erano persone contente del loro iPad Pro prima, che continueranno a esserlo anche dopo, senza Stage Manager. La frustrazione del mancato aggiornamento è artificiale e artificiosa, serve come argomento rafforzativo della propria tesi, che la rende antipatica invece che più forte: *la gente non può permettersi di cambiare iPad ogni anno, quindi Stage Manager…*

Quindi niente; è una funzione che vedremo e giudicheremo quando ci sarà, per come funziona. Giudicarla in base a quanti comprano iPad e quando lo fanno, è meschino. A parte questo, neanche c’entra qualcosa.