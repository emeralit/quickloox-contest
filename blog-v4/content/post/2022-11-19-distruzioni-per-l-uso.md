---
title: "Distruzioni per l’uso"
date: 2022-11-19T13:35:24+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Obsidian, Twitter, LaTeX, Markdown]
---
Guardo sconcertato le corse, i tutorial, i consigli, i dibattiti, le discussioni su come dove e perché scaricare i propri dati da Twitter nel timore del possibile grande crollo del servizio.

Il cinico standard commenterebbe *per farci cosa?* ma non sono cinico e detesterei essere standard. Sforzandomi di essere cinico, almeno quello, commento *per amministrarli come?*.

Non ho avuto nel tempo una vera strategia di conservazione dati e più che altro li ho accumulati. Se devo rifletterci sopra alla luce dell’attualità, la domanda cruciale è realmente la modalità di organizzazione.

Colgo una sorta di *memento mori* nella smania di scaricare tutto; Twitter magari muore, ma conservo i miei dati, per sempre, per sempre, *per sempre*…

Forse il problema sono io, che su Twitter faccio pochissimo e, quel pochissimo, irrilevante. Chi invece ci vive sopra si ritroverà un qualche blob di qualche gigabyte. Come i pesci scampati all’acquario di Nemo che galleggiano nell’oceano liberi, si fa per dire, nei loro sacchetti di plastica, *e adesso?*

Nei miei dischi vecchi e nuovi galleggiano numerosi di questi blob. Posta elettronica. Trattavo decine di messaggi al giorno, c’è dentro qualsiasi cosa. Compresi header, citazioni chilometriche, allegati, conversazioni costellate di messaggi con dentro *Ok* e intorno cinque mega di contorno superfluo. Quale filtro toglierà di torno il veramente inutile? Che *machine learning* ricostruirà i thread significativi togliendo croste, infiltrazioni, muffa? Ho provato qualche volta. Bisognerebbe destinargli mesi, forse anni di lavoro.

Con un *dump* da Twitter ho la spiacevole sensazione che possa essere la stessa cosa. Ti riprendi quello che è tuo, per carità; poi? Chi la toglie la struttura del database, chi leva l’inutile, specie se sono gigabyte di dati?

Più passa il tempo più mi convinco che la struttura migliore è quella minore: meno struttura possibile. In questo blogghino girano file di testo, con una nomenclatura semplice, una intestazione minima e null’altro. Se Internet fondesse e un archeologo ritrovasse la mia cartella blog su Mac, potrebbe leggere le stesse informazioni che oggi vede con il browser, senza altri fastidi.

Non è stata una scelta strategica eppure, dovessi compierla oggi, la ripeterei. Invidio quanti riescono a strutturare sui loro computer un flusso di testo integrale e con strumenti come [LaTeX](https://www.latex-project.org), [Markdown](https://daringfireball.net/projects/markdown/syntax), [Obsidian](https://obsidian.md) si allontanano sempre più dalla complessità dei contenuti come la vogliono le app, non sempre in nome della facilità,di utilizzo. Persone sagge che scrivono presentazioni con il testo, disegnano diagrammi con il testo, fanno conti con il testo, rappresentano sistemi complessi con il testo. Questi sono i dati che verranno davvero recuperati, riletti, riscoperti. Altro che gli archivi di file Excel.

Slogan: *destruttura i tuoi dati per il tuo futuro. E per il tuo passato*.