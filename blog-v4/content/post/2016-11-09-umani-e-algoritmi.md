---
title: "Umani e algoritmi"
date: 2016-11-09
comments: true
tags: [Pell, NextDraft, Jobs, machine, learning]
---
Sostengo con fermezza l’utilità della tecnologia e la necessità, anzi, la grande opportunità di progredire. Al tempo stesso però mi ritrovo vicino al ricordo del pensiero di Steve Jobs sotto [quel cartello che segna l’intersezione tra la tecnologia e le arti liberali](https://m.youtube.com/watch?v=KlI1MR-qNt8); la prima è da sostenere anche perché può dare nuova linfa e stimolo alle seconde.

Purtroppo umani troppo aridi, avidi e ignoranti spingono la tecnologia e precisamente la branca del *machine learning* verso la sostituzione del creare umano con gli algoritmi. Questa parte del progresso mi piace meno e neanche mi sembra tanto progresso.

Per questo mi sono avvicinato con curiosità a NextDraft su [consiglio di John Gruber](http://daringfireball.net/feeds/sponsors/2016/11/nextdraft). NextDraft è una newsletter quotidiana compilata da tale Dave Pell, che legge settantacinque fonti al giorno e produce una *compilation* di commenti sui dieci fatti più salienti della giornata, in tutti i settori.

Soprattutto, Pell lo fa con passione, gusto, un pizzico di umorismo, la capacità di andare oltre il banale, la varietà espressiva. L’ultima cosa che desidero è una newsletter in più; ma Pell è veramente sopra la media e oltretutto uno può scegliere se riceverla in posta *oppure* dentro [la sua *app*](https://appsto.re/it/IXOVG.i). Ho scelto la seconda: ho informazione in più senza avere una mail in più.

Nel [suo sito](http://nextdraft.com/), Pell rivendica la propria umanità: *l’algoritmo sono io*, scrive. Le sue fonti e i suoi link non sono frutto di uno script che perlustra la rete.

Basta leggere una tornata di NextDraft per capire che nessuna macchina oggi potrebbe mai neanche sperare di avvicinarsi allo stile e al valore di quello che scrive Pell.

Non escludo che tra vent’anni ci sarà una versione di [Watson](http://www.ibm.com/watson/) che tenterà di riprodurre lo stile di NextDraft, leggendo settantacinquemila fonti. Mi ostino a credere che il risultato sarà freddo e impersonale come il [resoconto mensile](https://www.quillengage.com/) del traffico di questo blog, scritto da una macchina. Va benissimo che l’analisi dei dati sia affidata alle macchine, che è il loro pane. Il commento dei fatti deve restare un bastione di umanità.

<iframe width="560" height="315" src="https://www.youtube.com/embed/KlI1MR-qNt8" frameborder="0" allowfullscreen></iframe>
