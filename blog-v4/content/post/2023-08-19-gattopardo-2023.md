---
title: "Gattopardo 2023"
date: 2023-08-19T01:22:29+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Intelligenza artificiale, ai, Microsoft Travel, Bing]
---
Nuove frontiere dei contenuti online: uno stagista sotto sostanze psicotrope ha scritto per la sezione Microsoft Travel di MSN.com una guida ai luoghi turistici da visitare in Canada e vi [ha aggiunto, venisse appetito, la Food Bank](https://archive.is/FGP9q): la sede dell’organizzazione canadese che aiuta i bisognosi incapacitati a mettere un piatto caldo a tavola.

Mi sono sbagliato: sopra ho scritto *stagista* mentre la dizione giusta è *intelligemza artificiale*.

Quella che cambierà tutto e porterà la prossima rivoluzione, tanto è vero che Microsoft ha fatto i salti mortali per integrarla alla velocità della luce in Bing. Così che possa consigliare ai visitatori del Canada il pranzo alla mensa dei poveri.

Lo sforzo inizia a dare i suoi frutti e finalmente, dopo anni passati a raccogliere le briciole delle ricerche su Internet, oggi Bing può orgogliosamente vantarsi di [raccogliere le briciole delle ricerche su Internet](https://archive.is/6Wq1C).

Comincio a pensare che il grande cambiamento promosso dai grandi modelli linguistici sarà gattopardesco. E che cambierà nulla, o poco, nonostante lo scespiriano grande rumore.

*Gli aggiornamenti in agosto del blog potrebbero essere irregolari o infrequenti.*