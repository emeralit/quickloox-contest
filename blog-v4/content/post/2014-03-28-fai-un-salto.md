---
title: "Fai un salto, fanne un altro"
date: 2014-03-28
comments: true
tags: [Microsoft, Asus, Office, iPad, Gates]
---
Quasi un anno fa Microsoft [spiegava in pubblicità](https://www.youtube.com/watch?v=zgu9uo2UpPg) come un accrocchio Asus fosse meglio di iPad, (anche) perché sopra ci funzionava Office.

<iframe width="560" height="315" src="https://www.youtube.com/embed/zgu9uo2UpPg" frameborder="0" allowfullscreen></iframe>

È lo stesso periodo in cui Bill Gates [argomentava](http://www.businessinsider.com/bill-gates-on-the-ipad-2013-5) che gli utilizzatori di iPad erano frustrati.

>Non possono dattiloscrivere, non possono creare documenti, non hanno Office.

Adesso, non c’è neanche bisogno di linkare, Microsoft ha pubblicato Office per iPad. Premio *salto logico dell’anno* per il 2014, già assegnato a marzo.