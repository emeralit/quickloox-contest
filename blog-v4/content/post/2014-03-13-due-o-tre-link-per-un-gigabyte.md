---
title: "Due o tre link per un gigabyte"
date: 2014-03-13
comments: true
tags: [Koding, Melabit, kOoLiNuS]
---
[Koding](https://koding.com/), un ambiente condiviso di sviluppo codice *online* accessibile via *browser*, contiene la possibilità di fare funzionare una macchina virtuale Linux Debian con tre gigabyte di spazio. Sembra poco, ma un programmatore professionista, amatore o ausiliario (persone che svolgono un’altra professione ma si giovano di masticare programmazione) in tre gigabyte può combinare molto.<!--more-->

(Una macchina virtuale è un computer che sta da qualche parte nell’universo invece che a casa nostra e non è neanche un computer fisico, ma un microcosmo software che insieme ad altri mille abita dentro un megaserver e si comporta come un computer fisico).

Il servizio – la cosa è già [apparsa nei commenti](https://moot.it/quickloox#!/blog/blog/2014/03/11) – sta regalando fino a esaurimento spazio un ampliamento a quattro gigabyte, con possibilità di salire fino a venti gigabyte, uno per volta, per quanti reclutano nuovi iscritti.

Siccome siamo ancora tutti a uno stadio precedente di civilizzazione, dire *macchina virtuale* non scatena gli entusiasmi selvaggi di quando si dice *spazio disco gratis*, tuttavia su Koding c’è ancora spazio di offerta da sfruttare.

Consiglio: iscriversi. Digiuni di programmazione? Fa assolutamente niente: passare un pomeriggio piovoso, una sera che gli amici hanno tirato il bidone, un sabato con il coniuge in viaggio, un momento che il cane lo porta a spasso il vicino e capire che cos’è, che bisogna allargare gli orizzonti e tenere la mente allenata. Se poi viene da esplorare anche il funzionamento vero del sistema, provare a muovere qualche passo in direzione di digitare delle istruzioni, fantastico. Non indispensabile.

Iscrivendosi, farlo regalando un gigabyte in più (con un clic sul link che segue e conseguente iscrizione) a chi ne farà ottimo uso. Per esempio, l’[autore di Melabit](https://koding.com/R/melabit), che ne parla in un [articolo](http://melabit.wordpress.com/2014/03/07/una-macchina-virtuale-gratis/) apposta.

Se si desidera una alternativa, regalarlo a [kOoLiNuS](https://koding.com/R/koolinus), che ha seminascosto il link nel proprio [blog](http://www.koolinus.net/blog/). Anche in questo caso il gigabyte è ben dato, anzi benissimo.

Sarei ipocrita se facessi finta di non gradire qualche gigabyte in più, ma il mio uso sarà mediocre. Metto pertanto in piccolissimo il link che mi riguarda, per agevolare il clic sui primi due[.](https://koding.com/R/loox)