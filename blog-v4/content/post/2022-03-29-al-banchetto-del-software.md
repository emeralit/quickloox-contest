---
title: "Al banchetto del software"
date: 2022-03-29T00:37:04+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [Coda, Oscar, Amazon, Netflix, Apple, Hardware Upgrade, Tom’s Hardware, Marc Andreessen, Andreessen, Eix, Comandi rapidi, Shortcuts, LaTeX, cloud, machine learning, Cwi, Chief Digital Officer, Cdo, Chief Information Officer, Cio, Chief Technology Officer, Cto]
---
Gli Oscar del cinema non contano più come una volta: hanno perso un tot di pubblico e di credibilità. Qualcosa però lo rappresentano ancora e [il primo produttore di opere trasmesse in streaming a vincere un Oscar per il miglior film è Apple](https://www.theverge.com/2022/3/28/22999481/apple-tv-plus-oscars-best-picture-coda-netflix-power-of-the-dog-dont-look-up).

La cosa da notare, più della vittoria, è che Apple [si applica in questo campo da un paio di anni](https://www.apple.com/it/newsroom/2022/02/apple-lands-first-best-picture-oscar-nomination-for-coda-and-more/), con un budget [inferiore a quello di Amazon e soprattutto di Netflix](https://observer.com/2021/09/state-of-the-streamer-apple-tv-is-charting-a-unique-path-through-the-streaming-wars/), che parrebbe investire il doppio. Un modo diverso di fare le cose e interpretare la qualità, evidentemente.

In ogni caso, il mondo è cambiato. [Una testata che si chiama Hardware Upgrade pubblica notizie sui premi Oscar](https://www.hwupgrade.it/news/web/oscar-apple-batte-netflix-e-i-servizi-in-streaming-dominano-sulle-major_105922.html). E non solo, il cinema è solo uno dei tanti argomenti: su [Tom’s Hardware](https://www.tomshw.it) si leggono news di giardinaggio, aerospazio, gioco di ruolo, libri e fumetti, automobili…

Il software, più che [mangiarsi il mondo](https://a16z.com/2011/08/20/why-software-is-eating-the-world/), ne sta rilevando l’infrastruttura. Non è un bene e neanche un male, ma una trasformazione irreversibile, immensa, che impatta su qualunque settore. Il fenomeno è talmente rilevante che si può leggere come [il software si mangi il software che si mangia il mondo](https://eiexchange.com/content/software-is-eating-the-software-thats-eating-the-world) e verrebbe da pensare a una parodia, mentre invece il contributo è serissimo.

Vado a parare sempre nel solito posto: è impensabile, anno 2022, escludere il software dalla propria dieta di apprendimento. Sia programmazione, siano reti, sia cloud, sia machine learning, sia scripting, sia [LaTeX](https://www.latex-project.org), sia Comandi rapidi, un’esperienza e una capacità nel software diventeranno essenziali anche per il notaio o il pizzaiolo.

Chi scrolla le spalle potrebbe leggere [Chief Digital Officer: lo stratega e l’evangelista dell’innovazione digitale](https://www.cwi.it/cio/chief-digital-officer-lo-stratega-e-levangelista-dellinnovazione-digitale_42145115) su *Cwi*. Una volta esisteva l’IT Manager e sembrava troppo: oggi le aziende che innovano parlano di Chief Information Officer, Chief Technology Officer e Chief Digital Officer, tre figure diverse e complementari.

Va capito e metabolizzato. Dopotutto, questo blogghino sta ricevendo una solida cura di software e oggi offre funzioni che a Capodanno erano impensabili. Ed è un *labor of love*. Una multinazionale, o anche una piccola-media azienda, o financo il professionista, come possono permettersi di ignorare il software? Non possono (più).