---
title: "Al lavoro nell’oscurità"
date: 2024-01-03T01:29:24+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [iPhone, parco di Monza, Trame di luce, fotografia notturna]
---
Dall’ultima volta che mi sono trovato a scattare fotografie in notturna con iPhone, sono passati parecchi aggiornamenti sotto i ponticelli (la capisce chi si ricorda delle *breadboard*).

La prima foto l’ho bucata completamente, sorpreso dalle novità dell’interfaccia. Non sapevo nulla del timer visualizzato per avvisarmi della lunghezza del tempo di esposizione e del cursore a croce da tenere sovrapposto al suo alter ego a conferma della stabilità dell’inquadratura.

Poi ho capito e mi sono trovato bene. Il sistema spiega esattamente quanto manca alla fine dell’esposizione, se l’obiettivo si sta muovendo rispetto all’inizio e, terminata l’acquisizione delle informazioni, con una animazione del pulsante di scatto informa della fase computazionale interna prima di restituire lo scatto definitivo.

Da dilettante assoluto della fotografia ma vagamente esperito nelle interfacce utente, posso solo dichiararmi soddisfatto e alla maniera Apple, con un pizzico di sorpresa e meraviglia. Il cursore a croce da allineare, soprattutto, ora che l’ho visto fatico a capire come non sia sempre esistito, dovunque, comunque.

![Un panorama da Trame di luce, al parco di Monza, ripreso al buio con iPhone 12](/images/trame.jpeg "All’evento Trame di luce dentro il parco di Monza, iPhone 12, iOS 17.1.2, nessuna postproduzione dopo lo scatto.")