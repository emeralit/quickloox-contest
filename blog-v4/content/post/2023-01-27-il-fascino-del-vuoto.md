---
title: "Il fascino del vuoto"
date: 2023-01-27T00:28:05+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Myst, Cyan]
---
[Myst](https://cyan.com/games/myst/) fu effettivamente incredibile per la sua epoca, una specie di meteorite piombato in mezzo ai dinosauri. Veramente *insane*, [pazzesco](https://twitter.com/randemtweets/status/1618422089031254019).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">It’s insane to me, too! <a href="https://t.co/YWx5lkv17p">https://t.co/YWx5lkv17p</a></p>&mdash; rand miller (@randemtweets) <a href="https://twitter.com/randemtweets/status/1618422089031254019?ref_src=twsrc%5Etfw">January 26, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Il suo fascino veniva dalla presentazione di un mondo vuoto e inerte che tuttavia pulsava del ricordo di chi lo aveva abitato come da trama (svelata man mano che si procedeva nell’esplorazione).

I giochi di oggi sono perlopiù abitatissimi e pieni di movimento, ma assai più vuoti a causa della mancanza di immaginazione e dell’impossibilità commerciale di realizzare qualcosa fuori scala per il momento presente almeno quanto lo fu Myst così tanti anni fa.

Un vero appassionato potrebbe anche tornare sul luogo del delitto. Cyan ha ridisegnato Myst con l’aiuto dei nuovi motori di grafica tridimensionale e anche per la realtà virtuale.

Non vedo applicazioni particolarmente irresistibili oggi per la realtà virtuale, almeno nulla che non si sia già visto. Se mi capitasse sottomano un Oculus, comunque, una visita *dentro* Myst la farei volentieri. Alla ricerca di quel vuoto pulsante che ha riempito diverse notti di quei mesi.