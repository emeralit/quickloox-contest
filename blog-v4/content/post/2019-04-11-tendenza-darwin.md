---
title: "Tendenza Darwin"
date: 2019-04-11
comments: true
tags: [OneDrive]
---
Posso dire di avere vissuto molte esperienze nella vita, alcune discutibili. A questa parte dell’elenco sono in grado di aggiungere l’avere ricevuto accesso a un file residente sul disco OneDrive di una organizzazione.

*Accesso* significa che ho ricevuto un link, stile Dropbox. Differentemente da Dropbox, il link mi ha fatto inserire una password di un qualunque account Microsoft sensato (nel mio caso, Skype). Successivamente ho dovuto confermare l’operazione tramite un codice. Questo è bastato per farmi passare alla fase successiva: un altro login. Dopo averlo confermato, mi è apparsa la conferma, nella finestra presente a sinistra in questa immagine. *Puoi tornare a quello che stavi facendo*.

 ![La via Microsoft](/images/live-account.png  "La via Microsoft") 

A destra si vede quello che stavo facendo. Il candidato spieghi attraverso quale elemento dell’interfaccia potrei *tornare* o, meglio, ad avere il dannato file.

La risposta è nessuno. Neanche il tasto Indietro del browser. Ho ricliccato il link ricevuto inizialmente e, stavolta sì, ho potuto accedere al file.

Ma niente lo lasciava neanche suggerire.

Il consueto approccio darwiniano di Microsoft. Sistemi potentissimi, sofisticatissimi, pensati per utenti di infinite capacità. Se sei uno normale, ti devi arrangiare e se non ci arrivi sono fatti tuoi; ci penseranno i *power user* a dirti che è colpa tua.

Ovvero, l’antitesi della storia dell’informatica personale degli ultimi trentacinque anni. E pazienza se sembra esagerato, a me sembra di andarci piano.