---
title: "Sicuramente una svista"
date: 2015-10-16
comments: true
tags: [Magic, Trackpad, Mouse, Keyboard, iMac, Bondi]
---
Quelli che pensano solo agli iCosi e intendono lasciare morire Mac hanno presentato un nuovo iMac che [rispetto al primo](http://www.apple.com/imac/then-and-now/), quello color Bondi Blue, presentato nel 1998, vanta quattordici milioni di pixel in più, grafica sessantaduemila volte più veloce, oltre trecento volte la potenza di elaborazione, mille volte più Ram e un disco di capienza settecento volte superiore.<!--more-->

Nel contempo, non contenti di lasciare Mac alla sua agonia, hanno allestito [nuove versioni](http://www.apple.com/it/magic-accessories/) di Magic Mouse, Magic Keyboard e Magic Trackpad.

Vero, non mi hanno dato la retroilluminazione sulla tastiera. Me ne sono sempre fatto una ragione, peraltro.

Sbadati.