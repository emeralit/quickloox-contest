---
title: "Il mistero del nuovo sensore"
date: 2019-11-20
comments: true
tags: [MacBook, Pro]
---
Fa sensazione venire a sapere che i nuovi MacBook Pro 16” dispongono di un nuovo sensore, per la [misurazione dell’angolo di apertura dello schermo](https://www.macrumors.com/2019/11/19/16-inch-macbook-pro-lid-angle-sensor/), del quale fuori da Apple nessuno è certo di conoscere la ragion d’essere, come mostrano bene i commenti all’articolo.

Fa sensazione perché è una sorpresa piccola, anche minima, ma è una sorpresa. La vulgata parla della mancanza di innovazione, di macchine tutte uguali, quando invece c’è un lavoro sotterraneo semiinvisibile di cui, se va bene, abbiamo consapevolezza solo alla fine. Anche quelli che promettono di sapere tutto, anticipare tutto, spifferare ogni cosa prima della data, in realtà non sanno, o sanno poco.

Sarà FaceID? Una funzione di controllo per verificare che il portatile sia davvero chiuso? Un test per il supporto tecnico quando si tratta di capire se una macchina è stata trattata nel modo giusto oppure abusata? Per ora resta un mistero e anche un mistero interessante; non so quanto altri portatili in giro per il mercato montino un sensore di angolazione dello schermo.
