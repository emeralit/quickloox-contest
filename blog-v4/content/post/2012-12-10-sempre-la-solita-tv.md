---
title: "Sempre la solita TV"
date: 2012-12-10
comments: true
tags: [Apple, TV, Script]
---
L’analista di Piper Jaffray Gene Munster ha annunciato che Apple potrebbe <a href="http://www.businessinsider.com/gene-munster-apple-could-announce-new-tv-this-december-2012-5">annunciare un proprio televisore</a> da vendere durante il 2013, mossa corroborata (?) da <a href="http://www.businessinsider.com/apple-tv-reportedly-launching-next-year-2012-11">affermazioni del prestigioso (per dire) China Times</a>.

Poi ha cambiato idea e ha dichiarato che l’annuncio <a href="http://www.businessinsider.com/no-apple-tv-until-november-2013-says-analyst-gene-munster-2012-11?op=1">avverrà a novembre 2013</a>.

Nel 2009 aveva predetto che Apple <a href="http://tech.fortune.cnn.com/2009/08/20/munster-an-apple-television-set-by-2011/">sarebbe entrata nel mercato durante il 2011</a>.

Digitimes spiegava più avanti che Apple avrebbe iniziato ad assemblare i componenti nel primo trimestre 2012 per <a href="http://venturebeat.com/2011/12/27/apple-preparing-32-37-inch-itvs-for-a-summer-launch/">lanciare il prodotto durante l’estate</a>.

Sappiamo con sicurezza tre cose: Gene Munster ha antenati Maya e, quando Apple magari lancerà un televisore, ci sarà la gara a scrivere io l’avevo detto. Macity li copierà tutti, refuso dopo refuso, senza perdere un colpo a vuoto.

Molto più divertente e istruttivo seguire le <a href="http://www.mondaynote.com/2012/12/09/5175/">analisi di Jean-Louis Gassée</a> corredate da foto di un suo pranzo al ristorante <a href="http://lyfekitchen.com">Lyfe</a> di Palo Alto, dove sono ben visibili <a href="http://www.mondaynote.com/wp-content/uploads/2012/12/Lyfe-Menu.png">cinque pannelli luminosi</a> ad annunciare i menu. Pilotati da <a href="http://www.mondaynote.com/wp-content/uploads/2012/12/Lyfe-Mac-MInis-Edited.png">altrettanti Mac mini</a>.