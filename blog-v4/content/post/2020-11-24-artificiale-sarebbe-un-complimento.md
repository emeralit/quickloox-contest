---
title: "Artificiale sarebbe un complimento"
date: 2020-11-24
comments: true
tags: [iPhone]
---
Su Twitter infuria, tra mille *thread*, quello dedicato ai chirurghi che fotografano momenti o esiti delle proprie operazioni e [iPhone che interpretano gli scatti come cibo](https://twitter.com/MarkHoofnagle/status/1329812783458881537). Si trovano centinaia di contributi.

Conosco la materia per avere partecipato come giornalista a un paio di congressi di videochirurghi e sono abbondantemente vaccinato contro l’effetto *splatter*. Qualcuno potrebbe trovarsi imbarazzato o disgustato nei confronti di immagini non consuete e, nel caso, lo prego o la prego di fidarsi e proseguire oltre.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">I love it when the Iphone AI tries to interpret the photos on a surgeon’s phone. <a href="https://t.co/3GkqdO6OrN">pic.twitter.com/3GkqdO6OrN</a></p>&mdash; Mark Hoofnagle (@MarkHoofnagle) <a href="https://twitter.com/MarkHoofnagle/status/1329812783458881537?ref_src=twsrc%5Etfw">November 20, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Si noterà che il chirurgo accenna alla *intelligenza artificiale di iPhone* e questo era il punto. C’è *machine learning*, apprendimento meccanizzato, un progresso scientifico importante e agli inizi.

Per il resto, di intelligente non c’è nulla e almeno *artificiale* potrebbe essere un complimento, perché indicherebbe quantomeno un lavoro da parte di qualcuno; invece sono elucubrazioni di una rete neurale, che produce confusione e ambiguità. Peggio ancora, la rete è chiusa in se stessa, senza possibilità di crescere o scegliere una strada diversa da quella che le è stata indicata.

Ci fosse intelligenza, artificiale oppure no, distinguerebbe una infezione al piede da un bollito. O comunque si chiederebbe se non ci sia qualcosa di strano.

iPhone migliorerà e un giorno individuerà perfettamente le foto scattate dal chirurgo in sala operatoria, capace di distinguerle da quelle scattate al ristorante. In tutte le case ci sarà un’unità di machine learning a ottimizzare la spesa, i consumi energetici, i bucati eccetera.

Ricordiamoci, ora come allora, di considerare la macchina come un ausilio, amplificatore, potenziatore, invece che come un sostituto. I computer sono splendidi nel percorrere a velocità mostruosa i percorsi conosciuti. È all’uomo che compete la capacità di fare un singolo, sorprendente, decisivo passo laterale inaspettato e creare qualcosa di completamente nuovo.

Cosa che i computer (lo dice un sostenitore dell’intelligenza artificiale forte) neanche tra un secolo.