---
title: "Come passa il tempo quando ci si diverte"
date: 2022-01-16T02:12:11+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Dropbox [ha pubblicato la versione beta della sua app per Apple Silicon](https://www.macrumors.com/2022/01/13/dropbox-apple-silicon-support-beta-users/). In fondo la nuova architettura hardware di Apple è stata annunciata solo a giugno 2020… basterà un anno e mezzo per ricompilare del codice con un target diverso e mettere a posto i problemi, posto che ce ne siano?

E tutto questo dopo che la prima risposta di Dropbox era *ci vogliono più richieste*, al punto che è dovuto [intervenire l’amministratore delegato](https://www.macrumors.com/2021/10/28/dropbox-no-plans-to-support-apple-silicon/) per dire che sì, Dropbox per Apple Silicon sarebbe arrivato.

(Non è ancora arrivato; c’è la beta. Diciotto mesi).

Per me speravano che Apple cambiasse idea e tre mesi dopo dicesse *no, ok, troppo sbattimento, torniamo a Intel*.

Per essere chiari: vari motivi mi rendono conveniente usare tuttora Dropbox, di cui sono un utente non pagante. In quanto tale, se mi venisse risposto di accontentarmi della transcodifica di Rosetta, non batterei ciglio.

Se fossi utente pagante, però, avrei iniziato da un pezzo a chiedere una versione nativa per il mio sistema e a farmi anche qualche domanda su dove vanno i miei soldi.

Sarà così difficile? O hanno cose più divertenti da fare?

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._
