---
title: "La patente del Mac"
date: 2023-05-04T00:21:55+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Maggi, Sabino, briand06. MelaBit, Terminale, du]
---
Conseguire l’autorevolezza del Vero Esperto di Mac richiede obbligatoriamente il passaggio dell’esame di controllo dello spazio disco su Mac tramite il Terminale, alla fine del [corso tenuto dall’acclamato professor MelaBit](https://melabit.wordpress.com/2023/05/03/come-controllare-lo-spazio-occupato-sul-mac-con-il-terminale/).

Argomenti principali del corso: `du` (che sembra un vocalizzo neanderthaliano mentre invece fa altre cose, interessanti e sconosciute ai più). Mi raccomando la preparazione sugli *switch* perché il docente su questo è molto esigente.