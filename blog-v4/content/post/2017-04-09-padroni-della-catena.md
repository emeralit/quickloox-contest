---
title: "Padroni della catena"
date: 2017-04-09
comments: true
tags: [iPhone, Imagination, Gpu]
---
I so-tutto-io che leggono la lista dei componenti e dichiarano che un Android è meglio di un iPhone perché monta più Ram a bordo e ignorano che [iPhone offre prestazioni migliori con specifiche inferiori](https://macintelligence.org/posts/2016-09-14-concorrenza-asimmetrica/).<!--more-->

Stanno per ricevere un altro duro colpo perché Apple è in procinto di [abbandonare un fornitore di unità di elaborazione grafica](http://bgr.com/2017/04/05/apple-vs-imagination-iphone-gpu/) (Gpu) per progettare internamente, come fa già con i processori per iOS.

Vuol dire che le Gpu dei futuri iPhone saranno più integrate e personalizzare all’architettura interna della macchina; costeranno meno e perfomeranno meglio dei componenti standard provenienti da fornitori esterni. Se serve una dimostrazione in atto, da lungo tempo i processori serie A che equipaggiano iPhone e iPad dimostrano costantemente la bontà dell’approccio.

Apple può ovviamente [commettere errori](https://macintelligence.org/posts/2017-04-07-mac-pro-e-iphone-4/) ma continua a programmare bene per il medio e lungo termine. Dai tempi di Macintosh, 1984, a Cupertino sanno che avere il controllo pieno della catena di produzione garantisce un vantaggio competitivo e lentamente continuano a progredire in questa direzione.

È un lavoro di anni; leggere una tabella di specifiche, completamente vuota alla voce *integrazione*, costa pochi secondi. E il valore delle due azioni è proporzionale alla loro durata.