---
title: "Test e taste"
date: 2013-09-29
comments: true
tags: [iPhone, iOS]
---
Onore al merito dei colleghi di Macworld Italia, che hanno prodotto un utilissimo [riepilogo](http://macworld.it/benchmark-iphone-5s-batte-i-migliori-android_844189/) di tutti i test di velocità e autonomia condotti su iPhone 5S e concorrenti.<!--more-->

Nel frattempo io ho testato assolutamente niente, a parte iOS 7 su iPhone e iPad, [da poco installato](https://macintelligence.org/posts/2013-09-28-recita-a-soggetto/). Dopo un riavvio (per forza di cose), la fame mostruosa di batteria su iPhone sembra rientrata. Mappe, in particolare, ha ripreso il comportamento abituale dopo che aveva combinato cose piuttosto strane ieri.

Le mie *app* essenziali sembrano funzionare tutte, anche alcune trascurate dal programmatore e così prive di aggiornamento *ad hoc*. [Goodreader for iPad](https://itunes.apple.com/it/app/goodreader-for-ipad/id363448914?l=en&mt=8) mostra a volte un problema di visualizzazione di pagina, che rientra se cambio pagina e riprovo. Sopportabile e certo arriverà un aggiornamento.

Su iOS 7 in generale sarò disponibile a spendere parole vere tra qualche tempo, dopo averlo rodato e avere guardato in tutti gli angoli. Per ora, mi piacciono alcune cose dell’interfaccia (lo schema di colori, la prevalenza del bianco) e altre invece no (pulsanti troppo stilizzati, pulsanti contornati, certe sbavature di design). Il sistema è ricco di dettagli nuovi e cose che si scoprono.

L’autoaggiornamento delle *app* è un uovo di Colombo. Mai più senza.

Ma confesso: impazzisco per l’effetto parallasse. Ogni tanto prendo ancora in mano l’aggeggio e lo giro di qua e di là per vedere lo sfasamento prospettivo tra icone e sfondo. Ho anche personalizzato lo sfondo, cosa che non avevo mai fatto prima. Ma adesso vale la pena.

Non è certo un test, semmai questione di *taste*, gusto. Anche quando vedo cose che il sistema ha da farsi perdonare, a un certo punto torno a rivedere l’effetto parallasse e tutto si aggiusta.

 ![sfondo](/images/sfondo.png  "sfondo") 
