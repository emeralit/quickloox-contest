---
title: "Quello che non si vede"
date: 2014-05-28
comments: true
tags: [Gruber, Vesper]
---
John Gruber e compagni [hanno presentato](http://vesperapp.co/blog/vesper-2-0-and-vesper-sync/) la versione 2.0 della loro *app* [Vesper](https://itunes.apple.com/it/app/vesper/id655895325?l=en&mt=8) per raccogliere su iPhone annotazioni testuali e fotografiche.<!--more-->

Una delle novità di sostanza è Vesper Sync, un sistema di sincronizzazione via cloud delle annotazioni.

Nel [*feed*](http://daringfireball.net/feeds/main) Rss Gruber scrive una cosa bellissima, da memorizzare nell’imminenza dei prossimi annunci in programma a [Wwdc](https://developer.apple.com/wwdc/):

>È la cosa più bizzarra, impiegare otto mesi di sviluppo intensivo, progettazione e collaudo per costruire qualcosa che (speriamo) si limita a funzionare in modo quasi invisibile. Sono realmente orgoglioso di questa versione.

Difficile, sempre, che il valore sia quello di superficie, di cui tutti parlano, evidente e strillato.