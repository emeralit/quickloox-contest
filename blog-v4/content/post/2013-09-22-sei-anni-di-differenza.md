---
title: "Sei anni di differenza"
date: 2013-09-22
comments: true
tags: [iPhone]
---
Devo la ricostruzione a [MG Siegler](http://parislemon.com/post/61777379694/what-a-difference-six-years-makes).<!--more-->

Steve Ballmer, a breve non più amministratore delegato di Microsoft, [nel 2007](http://www.youtube.com/watch?v=eywi0h_Y5_U):

>Al momento vendiamo milioni e milioni e milioni di telefoni ogni anno. Apple sta vendendo zero telefoni ogni anno.

<iframe width="420" height="315" src="//www.youtube.com/embed/eywi0h_Y5_U" frameborder="0" allowfullscreen></iframe>

Sempre Ballmer [pochi mesi dopo](http://usatoday30.usatoday.com/money/companies/management/2007-04-29-ballmer-ceo-forum-usat_N.htm):

>È una domanda abbastanza buffa. Scambierei il 96% del mercato con il 4% del mercato? [risata]. Voglio avere prodotti accattivanti per tutti. Adesso avremo una possibilità di ripetere la storia con i telefoni e i lettori musicali. Non c’è alcuna chance che iPhone arrivi a una qualsiasi quota di mercato significativa. Nessuna chance.

Ballmer [l’altroieri](http://mobile.theverge.com/2013/9/19/4750086/ballmer-almost-no-mobile-share-microsoft-opportunity):

>Apparecchi mobili. Abbiamo quota di mercato quasi zero.

Il campo di gioco si è capovolto in sei anni, una rapidità incredibile per un cambiamento così profondo. Qualcuno però è pagato milioni di dollari per capirlo e provvedere con il potere di manovrare decine di migliaia di lavoratori qualificati. Un esercito.
