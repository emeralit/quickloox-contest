---
title: "Disobbedire al cubo"
date: 2015-09-09
comments: true
tags: [Minecraft, Minetest, Microsoft, iOS]
---
Sono sempre stato grande fan di [Minecraft](https://minecraft.net), che però è stato [comprato da Microsoft](https://macintelligence.org/posts/2014-09-16-i-denti-del-lupo/) e lentamente, inesorabilmente, da grande invenzione ludica diventa supporto alla vendita di qualche altro prodotto della multinazionale. E oramai da moltissimi anni brigo per essere libero da software Microsoft sui miei computer.<!--more-->

Mi pare che una buona alternativa possa essere [Minetest](http://www.minetest.net), con un vantaggio fondamentale – *open source* – e per ora ancora un problema: la mancanza di una buona versione per iOS. I miei cubi, comunque, ora li dispongo su Minetest.

Da promuovere e fare conoscere, perché dall’altra parte hanno megafoni molto grossi e legioni di brave persone subito pronte a obbedire. E il germe dell’idea di Minecraft, *pardon*, Minetest,  la libertà assoluta di disporre del mondo virtuale in cui giocare, cioè predisposizione alla disobbedienza (quella creativa).