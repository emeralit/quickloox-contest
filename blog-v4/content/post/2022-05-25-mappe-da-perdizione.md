---
title: "Mappe da perdizione"
date: 2022-05-25T03:38:55+01:00
draft: false
toc: false
comments: true
categories: [Software, Web]
tags: [Mappe, Maps, xkcd, Randall Munroe, Munroe]
---
Le reazioni del geniale Randall Munroe, riportate in [xkcd](https://xkcd.com/2617):

*Ti guardi attorno un giorno e capisci che quanto consideravi una costante immutabile dell’universo è cambiato.*

*Le fondamenta della nostra realtà ci scivolano sotto i piedi.*

*Viviamo in una casa costruita sulla sabbia.*

L’evento scatenante:

*Il giorno in cui ho scoperto che ora le Mappe di Apple sono piuttosto buone.*

Di sicuro il modo più divertente per dirlo che abbia mai visto.