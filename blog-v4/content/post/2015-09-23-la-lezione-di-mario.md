---
title: "La lezione di Mario"
date: 2015-09-23
comments: true
tags: [Mario, Miyamoto, Nintendo]
---
A volte i giapponesi si esprimono in modo che per la nostra cultura sembra un po’ ridicolo e infantile ed è un peccato, rispetto a questo [filmato di Shigeru Miyamoto](https://www.youtube.com/watch?t=499&v=zRGRJRUWafY) che illustra il mondo 1-1 di Mario, forse uno dei primi livelli più famosi nella storia dei videogiochi, di cui Miyamoto è stato il progettista per Nintendo.<!--more-->

I sottotitoli in inglese sono chiari e si può cogliere, attraverso il candore di Miyamoto, quanto lavoro e quanto impegno venga versato dai progettisti di un gioco. Anche solo il primo livello deve spiegare al giocatore che cosa aspettarsi, dargli indizi comportamentali e naturalmente ricompense che lo mettano nella condizione di voler proseguire il gioco.

Poco più di otto minuti che, visti con attenzione, insegnano molto per tutte le volte che dobbiamo interagire con qualcuno.

<iframe width="560" height="315" src="https://www.youtube.com/embed/zRGRJRUWafY?rel=0" frameborder="0" allowfullscreen></iframe>
