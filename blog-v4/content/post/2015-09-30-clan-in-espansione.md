---
title: "Clan in espansione"
date: 2015-09-30
comments: true
tags: [CoC, Hearthstone]
---
Il vecchio giro di amici e sodali sta continuando ad applicarsi su [Clash of Clans](http://supercell.com/en/games/clashofclans/) e ha già vinto con largo margine la terza guerra tra clan, aperto una *mailing list* apposta per le comunicazioni che non stanno nella *chat* del gioco, iniziato a discutere sulle strategie di gruppo.<!--more-->

Un successo. È un gioco che mi diverte e coinvolge meno di [Hearthstone](http://eu.battle.net/hearthstone/it/?-), tuttavia messo momentaneamente da parte: un gioco buono in compagnia è sempre molto meglio di un gioco ottimo da soli.

Il gruppo storico è relativamente limitato per le dimensioni ottimali di un clan e accettiamo adesioni. Requisiti (che rispetto e non ho deciso): si parla italiano, è solo un gioco, senso dell’umorismo, disponibilità a ricevere qualche mail oltre alla *chat* interna.

Il mio identificatore univoco su Clash of Clans è #CVJ9JRLQ (lvcivs), quello del clan (Golden Eagle) #YV2UVG99. Dire che si arriva da QuickLoox.

Non sono io a decidere definitivamente sull’accoglienza, ma perorerò di cuore la causa di chi merita. Soprattutto se mi dà qualche dritta in attacco, dove ho molto da imparare.