---
title: "Maestri della musica"
date: 2022-10-04T01:43:23+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Apple, Apple Music, iTunes]
---
Di milestone che significano solo una cifra tonda in più ne abbiamo passate tante. Eppure, se Apple annuncia il superamento dei [cento milioni di brani su Apple Music](https://www.apple.com/newsroom/2022/10/celebrating-100-million-songs/), è difficile affettare indifferenza.

Ad aggregare più brani di qualsiasi altro servizio è una società che è nata per costruire computer.

Vengono in mente iPod, iTunes, il rippaggio dei CD, le polemiche sul DRM culminate con la [lettera aperta di Steve Jobs](https://macintelligence.org/posts/2014-12-06-la-solita-musica/) a dire *non vediamo l’ora di poterlo togliere* e oggi, infatti, non c’è più.

Apple si è presa un settore diverso dal proprio, lo ha computerizzato e ne è diventata protagonista. La computerizzazione che rimescola il vecchio andamento delle cose è una schema contante, chiedere ai fabbricanti di telefoni e di orologi.

iPod è partito con la promessa di *mille canzoni in tasca*. Oggi è un po’ diverso e a me la logica dello streaming piace poco. Cento milioni di canzoni sono comunque centomila iPod tradizionali, non proprio in tasca ma a portata di dito.

Complimenti.