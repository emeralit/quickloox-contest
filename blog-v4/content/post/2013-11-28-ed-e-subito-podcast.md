---
title: "Ed è subito podcast"
date: 2013-11-28
comments: true
tags: [Speirs, GarageBand, Auria, iPad, podcast]
---
Rivelatore, Fraser Speirs nel proprio blog a raccontare come abbia risolto l’esigenza di registrare il proprio *podcast* anche in viaggio: [facendo tutto su iPad](http://speirs.org/blog/2013/11/19/portable-podcasting.html) per viaggiare leggero il più possibile.<!--more-->

I perché non usare GarageBand ma [Auria LE](https://itunes.apple.com/it/app/auria-le/id585683569?l=en&mt=8), e poi Dropbox, il microfono giusto, la velocità: a suo dire, l’esportazione di audio in Mp3 è quattro volte più veloce su iPad Air che su un iPad di terza generazione.

>Quando ho detto a qualche amico che “podcastavo” su iOS, presumeva che io registrassi su iOS e poi effettuassi più tardi la postproduzione su Mac. Non è così! In effetti la produzione avviene su iOS dall’inizio alla fine.

Quasi una legge fisica: quando qualcuno presume che su iPad non si possa svolgere un certo lavoro, ha torto o finirà per avere torto entro un tempo ragionevole.