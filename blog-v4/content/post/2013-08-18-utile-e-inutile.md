---
title: "Utile e inutile"
date: 2013-08-18
comments: true
tags: [iPad, iOS, Internet]
---
Ringrazio **Stefano** per avermi segnalato due *link* che replico più che volentieri.

Uno si intitola [Every Second on the Internet](http://onesecond.designly.com) ed è una visuale particolare sul mondo in cui viviamo.<!--more-->

L’altro è [Editorial](http://omz-software.com/editorial/), un programma per produrre testo su iPad che ha l’aria di essere veramente un passo avanti a tutto il resto, con ottima compatibilità [Markdown](http://daringfireball.net/projects/markdown/) – oramai scontata – e soprattutto con caratteristiche di automazione notevoli, compreso un interprete [Python](http://python.org) incorporato per chi si senta di voler fare veramente sul serio.

Acquisirò Editorial a settembre. La soluzione migliore per documentarsi da subito è la monumentale [recensione](http://www.macstories.net/stories/editorial-for-ipad-review/) di Federico Viticci su *MacStories*. Ha dovuto perfino togliere dei contenuti perché la pagina era talmente piena da mandare in *crash* Safari e Chrome su qualche iPad. Una sera non di agosto magari gli rubo due o tre paragrafi per metterli in italiano.

Utile e inutile sono concetti relativi. L’unico assoluto cui tengo almeno un po’ è l’interessante.