---
title: "Come giocarsi il jack"
date: 2018-03-18
comments: true
tags: [Apple, Samsung, Gruber, Daring, Fireball, jack, iPhone, Bluetooth, Usb-C]
---
Bel pezzo di John Gruber che risponde alla domanda [Perché Samsung non ha ancora levato il *jack* audio dai suoi computer tascabili?](https://daringfireball.net/2018/03/samsung_jack_off_redux).<!--more-->

C’è una risposta tecnica che riassumo: le tecnologie non Apple che permettono soluzioni *wireless*, al momento sono meno performanti oppure molto più costose (rileggere, rileggere: molto più costose).

Poi c’è una risposta filosofica: Samsung fa quello che vuole il pubblico. Apple fa quello che il pubblico potrebbe volere.

Una strategia di gioco, quest’ultima, che continuo a trovare più affascinante e originale della prima.