---
title: "La grande congiura"
date: 2013-10-31
comments: true
tags: [iPhone]
---
Il mondo si è messo ad andare veramente alla rovescia. Sul *New York Times*, dico, il *New York Times* appare l’articolo [Spezzare la trappola di Apple](http://www.nytimes.com/2013/11/03/magazine/why-apple-wants-to-bust-your-iphone.html) in cui si accusa Apple di pianificare l’obsolescenza pianificata dei propri prodotti.<!--more-->

>Circa nel momento della presentazione di iPhone 5S e 5C, ho notato che il mio vecchio e triste iPhone 4 stava diventando molto più lento. Anche la batteria iniziava a esaurirsi molto più velocemente.

A riportare le cose alla realtà [è stato Gizmodo](http://gizmodo.com/no-apples-not-trying-to-bust-your-phone-1453999009), normalmente un sito di millantatori senza scrupoli.

>Nei tre anni e mezzo [di vita di iPhone 4], il telefono ha attraversato innumerevoli aggiornamenti del firmware, scattato una quantità indicibile di foto, è stato caricato impropriamente (succede a tutti) ed è caduto (idem). In poche parole, è stato usato. Più i componenti vengono usati, più si degradano.

Non pensavo che mi sarei trovato a citare *Gizmodo*, ma è così. La tecnologia diventa obsoleta. Le batterie non durano per sempre. Non è solo un problema di Apple, né è solo colpa di Apple. L’unica cosa che frega il tuo telefono è la regolare marcia del progresso.