---
title: "Grandi aspirazioni"
date: 2018-11-22
comments: true
tags: [blog]
---
Quando ho iniziato a bloggare ho capito che non avrei voluto mai smettere e oramai sono quasi vent’anni.

Questa è la seconda ripresa dopo due pause importanti e sono lieto che si sia sempre trattato di cause di forza maggiore: combinati disposti di guasti hardware, pressing di casa e lavoro, mia incompetenza: se riesco a fare solo in un’ora qualcosa che a una persona normale richiede dieci minuti, tendo a non farlo. Questo blog dovrebbe essere ospitato online più che in locale e dovrei rinnovare il parco macchine.

La seconda attività è già in corso. Ho anche capovolto il modo di pensare; stavolta il nuovo Mac è desktop e anonimo nella configurazione, perché ho presunto fosse adeguatissimo per il lavoro e perché ho la sensazione che nel giro di tre/cinque anni varrà la pena cambiare.

Là prima presunzione sembra fondata. Scriverò forse una recensione anche se sono indeciso, per come la macchina si distingue in base alla sue assenza. Abituato al portatile, trovo straniante operare senza quasi vederla fisicamente, a parte il silenzio assoluto e il calore ininfluente. La seconda, vedremo.

Spostare online la casa del blog è dovuto da tempo e appunto qui è questione di pressing e incompetenze. Gli amici mi hanno spiegato come impostare rapidamente cose da pubblicarsi su GitLab o altre piattaforme, anche via iPad. È che vorrei restare padrone della piattaforma. Forse ci riuscirò, forse rinuncerò a favore della praticità.

Sempre gli amici hanno offerto macchine sostitutive nel periodo in cui ho lavorato al cento percento su iPad e sono stati vicini nei modi più disparati. Le due esperienze – lavorare interamente su iPad e il supporto degli amici – sono state speciali e voglio rinraziare tutti, nonché acquisire un iPad Pro appena possibile. Lavorare con un iPad 2012 è possibile e efficace, alla faccia dei parolai, solo che il lato software mostra la corda.

Il tempo resta poco e la pressione (non quella arteriosa) alta. Però continuo a nutrire grandi aspirazioni. Mi sembra doveroso e no, aspiro ma non ho fumato alcunché. Vedo dove voglio arrivare, lá in cima alla salita.
