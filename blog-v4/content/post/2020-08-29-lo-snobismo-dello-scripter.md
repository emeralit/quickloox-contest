---
title: "Lo snobismo dello scripter"
date: 2020-08-29
comments: true
tags: [ScriptingOSX]
---
iOS 14 [renderà più difficile il tracciamento della navigazione degli utenti](https://www.facebook.com/audiencenetwork/news-and-insights/preparing-audience-network-for-ios14/) effettuato a loro insaputa e così Facebook ha deciso rappresaglia: sulle orme di [Epic](https://macintelligence.org/blog/categories/fortnite/) che ha fatto escludere Fortnite da App Store, [ha aggiornato una sua app in modo che dovesse essere respinta](https://www.reuters.com/article/us-facebook-apple-exclusive/exclusive-facebook-says-apple-rejected-its-attempt-to-tell-users-about-app-store-fees-idUSKBN25O042?il=0) sempre secondo le regole dello Store.

Il complesso dei commenti letti in queste settimane tende ad aggregarsi attorno a una posizione di questo tipo: Apple ha ragione nel merito (e [incassa neanche il quattro percento](https://macintelligence.org/blog/2020/06/21/il-triangolo-di-app-store/) dei soldi che girano nell’ecosistema di App Store), ma con il suo comportamento sta recando danno agli utenti e questo è rischioso.

Se le norme di App Store creano problemi agli sviluppatori vanno magari riviste, ma qualunque modifica dovrebbe prioritariamente favorire l’utilizzatore.

Ciò detto, mi dissocio da me stesso e proclamo lo sciopero delle polemiche su App Store.

Di conseguenza, ogni volta che la notizia più interessante della giornata riguarderà le scaramucce attorno ad App Store, commenterò invece qualche articolo a tema scripting. Per esempio [quelli di ScriptingOSX](https://scriptingosx.com/article-series/) (che dovrebbe aggiornare il nome, ma pazienza).

Vivrei molto volentieri senza Facebook (faccio del mio meglio in proposito, ma il lavoro mi frena) e vivo tranquillamente senza Fortnite. Che ne parli chi vuole, io preferisco leggere di scripting e guardarli dall’alto in basso (non ne sono capace, ma miro a impressionare, reggimi il gioco).