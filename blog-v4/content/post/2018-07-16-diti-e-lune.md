---
title: "Diti e lune"
date: 2018-07-16
comments: true
tags: [MacBook, Pro, Ssd, Boston, Martinez, Geekbench]
---
Salta fuori che i nuovi MacBook Pro hanno [il disco a stato solido più performante di sempre](https://www.laptopmag.com/articles/2018-macbook-pro-benchmarks), con prestazioni in scrittura che valgono *dieci volte la media del mercato* o *dei volte quelle del concorrente più vicino*.

Una cosa fuori dal mondo. È giocare in un altro campionato.

E chi se ne frega. È un record, è un segno di eccellenza ingegneristica e progettuale. Spariscono sempre in momenti come questo quelli che Apple trascura i Mac. Chissà come fanno, visto che un traguardo del genere, senza pazzesca integrazione hardware/software, è irraggiungibile. Useranno Ssd provenienti da [Hogwarts](https://www.pottermore.com/explore-the-story/hogwarts).

Ma è guardare il dito. Se sei in debito tecnico, puoi avere il disco più veloce dell’universo e il debito continua a restare.

La Luna è J.D. Martinez dei Red Sox di Boston. Un battitore che, prima, faticava a trovare il rendimento. Allora ha iniziato ad [acquisire con iPad i video delle sue battute](https://www.boston.com/sports/boston-red-sox/2018/07/14/j-d-martinez-and-his-bp-ipad-no-longer-a-laughing-matter) in partita e in allenamento, per individuare e correggere i propri difetti.

I compagni di squadra ridacchiavano. Ora hanno smesso: Martinez ha firmato un contratto di 110 milioni di dollari per cinque anni.

Una capacità, lavoro per migliorarla, strumenti per farlo se usati con cervello. Il senso dell’informatica è tutto qui.