---
title: "Il verso all’Europa"
date: 2014-03-26
comments: true
tags: [Lightning, Europa, Usb]
---
Riflessione di **Stefano** con *post scriptum* finale raccomandato per la diffusione presso tutti i consessi, di casa, scuola, ufficio eccetera.<!--more-->

>Da tempo trovo il cavo Lightning di alimentazione una di quelle invenzioni geniali che passano come al solito inosservate, per un semplicissimo motivo: non ti deve più importare il verso di collegamento al telefono o all’iPad.

>Facendo questo gesto dalle tre alle quattro volte al giorno la cosa si fa sentire. Viceversa non so per quale legge dei grandi numeri, ogni volta sbaglio il verso della Usb. C’è chi pensa a certe cose e chi no. Adoro Apple per questo.

>P.S.: avvisare l’Unione Europea quando vorranno fare l’importantissimo cavo di alimentazione universale…