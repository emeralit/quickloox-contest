---
title: "Corsi e ricorsi"
date: 2013-07-22
comments: true
tags: [Lisa]
---
Nel 1983, Apple pose fine all’epoca di Lisa [facendone seppellire](http://www.binarydinosaurs.co.uk/museum/apple/lisa/index.php) 2.700 modelli invenduti in una discarica situata a Logan, nello Utah.<!--more-->

Una buona idea per Microsoft, che siede su una quantità di Surface RT invenduti che potrebbe andare [dai cinque ai sei milioni](http://arstechnica.com/business/2013/07/microsoft-takes-900-million-hit-for-unsold-surface-rts-in-4q13-earnings/). Per capirci, molto approssimativamente, un mese di iPad. Solo che i numeri di Surface RT sono leggermente inferiori, come minimo di [qualche decina di volte](https://macintelligence.org/posts/2013-07-16-e-i-tredici-nani/).

Chissà se tra trent’anni esisterà almeno un sito visitabile residente su un Surface RT ancora funzionante. [Lisa ne ha uno](http://www.lisa2.com).