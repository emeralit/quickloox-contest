---
title: "Non per soldi"
date: 2023-09-07T18:14:17+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [AI, intelligenza artificiale, MacRumors, The Information]
---
C'è una testata chiamata *The Information* che, come altre, eroga i propri articoli a pagamento e secondo me possiamo farne a meno, se prendiamo per buona la [ripresa del pezzo da parte di MacRumors](https://www.macrumors.com/2023/09/06/apple-conversational-ai-spending/).

Apple spenderebbe somme ingenti per l’addestramento dei propri modelli linguistici interni. Ma pensa. E avrebbe un chatbot attivo in via sperimentale, cui alcuni dipendenti possono accedere. Sconvolgente, vero?

Apple avrebbe anche team dedicati alla generazione di video e immagini tramite AI (chi ci avrebbe pensato?) e lavora per portare la tecnologia nell’automazione di compiti complessi da dare in pasto a Siri, là dove ora bisogna fare a mano con i Comandi rapidi. Per una società capitalizzata verso i tre trilioni di dollari sembra il minimo sindacale, più che uno scoop.

Ma dove si coglie l’inutilità dell’articolo e anche dell’articolista, posto che sia umano, è dove si legge che ormai il chatbot a base di Llm è diventato un *must-have*, qualcosa che non si può fare a meno di avere.

E difatti, scorrendo i numeri e i conti di Apple, sembra proprio che tutta questa urgenza manchi, o sia poco visibile; il chatbot con la mela sopra manca e nemmeno è percepita un’urgenza qualsivoglia in tal senso. La società macina vendite e trovo difficile immaginare la scena all’Apple Store dove il commesso propone un Mac e il visitatore replica *ma il chatbot ce l’ha? Se non ce l’ha non lo compro!*

C’è un punto importante da qualche parte? Sì, non nell’articolo però. Ce lo ricordiamo noi.

Apple spende da molti anni cifre ingenti nel machine learning e in tutte le tecnologie attualmente *trendy*. Prima ancora di arrivare a Siri, i punti dell’ecosistema dove ricadono i frutti degli investimenti sono numerosi e spesso evidenti.

Il punto è che *Apple non investe per fare soldi*. Non direttamente, almeno. Il progresso in un Llm interno domani si traduce in mappe migliori, un servizio più brillante a chi usa messaggi, il riconoscimento più sollecito nei messaggi di posta di materiale da riportare dentro i Contatti o nel Calendario eccetera eccetera. Niente di tutto questo, tuttavia, viene venduto come prodotto a sé. Non si fa l’abbonamento ad *Applechat*.

Intanto, senza clamori e conversioni sulla via di Damasco, l’ecosistema software si completa, arricchisce, rafforza. Un apparecchio Apple si fa apprezzare nel suo insieme, più che per correre dietro a tecnologie tutto tranne che nuove.

*The Information* avrebbe fatto un pezzo migliore se avesse spiegato come e perché Apple fa cose diverse dagli altri. Invece ha cercato di farci credere che debba agganciarsi al trenino e questa cosa ha smesso di essere divertente dai remoti tempi dei *netbook*.