---
title: "Datemi una tee"
date: 2018-09-04
comments: true
tags: [Huffpost, Huffington, t-shirt, cotone]
---
Da leggere nelle scuole questo [articolo di Huffpost](https://www.huffingtonpost.com/entry/white-t-shirt-cost_us_5b801dc9e4b0729515127306) che analizza se e perché abbia un senso comprare t-shirt più costose della media.

La risposta è che non è garantito, ma in genere ci sono numerose ragioni a favore.

Una t-shirt si fa in cotone, che può differire in lunghezza del filo, aggiunta di elastene, qualità del filo stesso eccetera.

Speriamo che qualcuno si ricordi di quando trancia giudizi sui Mac e sul relativo costo. Tutti i computer sono uguali, tutti i componenti pure.

Beh, non ci sono due modelli di t-shirt uguali. I computer sono appena, appena più complessi.
