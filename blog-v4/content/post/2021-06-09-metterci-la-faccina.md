---
title: "Metterci la faccina"
date: 2021-06-09T00:58:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Memoji, Wwdc, Tim Cook, Medium, emoji, iMessage] 
---
Nella [finzione scenica di Wwdc](https://macintelligence.org/posts/Con-la-maiuscola.html), Tim Cook ha parlato davanti a una platea popolata di Memoji, a rappresentare gli sviluppatori idealmente presenti.

C’è chi l’ha presa per una baracconata o una [profanazione](https://twitter.com/denischamp/status/1402201045737029632) di un luogo che alla fine dei conti è intitolato a Steve Jobs; una caduta di stile, un abbassamento del livello.

<blockquote class="twitter-tweet"><p lang="it" dir="ltr">... lo trovo puerile. Non fanno ridere , L’audience dello Steve Jobs theatre pieno di Memoji come un cartoon under 6 fanno pensare a pochezza di fondo . E non è così ... perché poi anche ieri hanno poi offerto materiale interessante ... ma lo presentano in maniera sciocca. IMHO</p>&mdash; Denis Rizzi (@denischamp) <a href="https://twitter.com/denischamp/status/1402201045737029632?ref_src=twsrc%5Etfw">June 8, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Ogni opinione è degna di rispetto: tutti sicuri però che la più profittevole azienda di tecnologia digitale al mondo, che mette insieme due miliardi di fatturato ogni tre giorni, decida a cuor leggero di esporsi a figuracce? Abbiamo la certezza che dove fino a ieri a dettare la line era il design, oggi si ceda al cattivo gusto semplicemente per essere alla moda o per fare i [supergiovani](https://www.youtube.com/watch?v=L7pD3IHtCcw)?

Nei miei messaggi esagero con gli emoji, mentre i Memoji li riservo a pochissime occasioni e persone. Non sono quindi la persona più adatta a pronunciarsi. Tuttavia, un [articolo di Angela Lashbrook su Medium](https://onezero.medium.com/memoji-are-apples-greatest-invention-since-the-iphone-3ab8feb2575b) porta in merito una serie di informazioni interessanti.

Ci sono studi che mostrano come l’aspetto degli avatar influenzi il comportamento online delle persone e come le persone stesse si identifichino più volentieri in un avatar idealizzato, che le rende più gradevoli, di uno fotografico.

Chi vede un avatar di aspetto simpatico o piacevole tende a riporre più fiducia nell’interlocutore che lo adotta. I Memoji costituiscono un surrogato, certo limitato, del linguaggio del corpo e dell’espressività che portiamo nel mondo fisico. *Limitato* vale comunque più di *nullo*.

In altre parole, il come le persone si rappresentano in forma grafica e il come considerano le altre persone tramite le loro rappresentazioni è un dettaglio; un dettaglio importante in una faccenda delicata come la comunicazione interpersonale. Comunicando con il suo pubblico di Memoji, Tim Cook certamente dava risalto alle novità annunciate per iMessage; contemporaneamente trasmetteva tutta una serie di messaggi al pubblico non sviluppatore interessato all’apertura del convegno mondiale degli sviluppatori. Sì, il *keynote* è nominalmente per gli sviluppatori e di fatto parla al più grande pubblico degli appassionati. La vera comunicazione rivolta agli sviluppatori è il [Platforms State of the Union](https://developer.apple.com/videos/play/wwdc2021/102/), la prima cosa da guardare dopo il keynote.

Che si possa parlare del keynote Wwdc in termini semplici non implica che la sua preparazione sia semplicistica. Apple ci mette la faccia e pure le faccine sempre a ragion veduta, magari sbagliando, per carità, però con un’idea precisa.

<iframe width="560" height="315" src="https://www.youtube.com/embed/L7pD3IHtCcw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               