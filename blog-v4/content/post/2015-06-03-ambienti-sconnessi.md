---
title: "Ambienti sconnessi"
date: 2015-06-03
comments: true
tags: [iPad, Mac, Dropbox, OSX, iOS, PowerPoint]
---
I commenti relativi al [post di due giorni fa](https://macintelligence.org/posts/2015-06-01-due-indizi-fanno-una-presentazione/) hanno preso due strade: una, molto stimolante e che vorrei poter approfondire, su come insegnare al meglio l’arte della presentazione nelle scuole e trasformare il tedio di PowerPoint in occasione didattica; l'altra, più sterile, sui difetti reali e percepiti di Keynote e iOS.

Mi permetto, per la seconda, di constatare che questo *post* è scritto su iPad e finisce istantaneamente su Mac grazie a Dropbox. Nel 2015, spostare file tra OS X e iOS non può  essere giudicato un problema.

Ma il punto vero è un altro. Dalle critiche traspare la conoscenza di un ambiente scolastico nel quale gli insegnanti ragionano per chiavette, dove scaricare dei materiali da Internet è impensabile, in cui esiste sempre un solo modo di lavorare con i mezzi digitali ed è vecchio.

Un ambiente doppiamente sconnesso, nel senso della mancanza di connessione e della decrepitezza delle sue strutture, materiali e filosofiche.

Un ambiente – questa è la cosa agghiacciante – che qualcuno trova normale e a cui trova naturale conformarsi.