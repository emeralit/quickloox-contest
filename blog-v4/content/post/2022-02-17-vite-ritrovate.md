---
title: "Vite ritrovate"
date: 2022-02-17T01:41:22+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [AirTag, watch]
---
Ricapitoliamo.

L’Attorney General di New York (diciamo il procuratore capo, per capirci) [invia ai cittadini una raccomandazione di sicurezza](https://ag.ny.gov/press-release/2022/consumer-alert-attorney-general-james-warns-new-yorkers-tracking-threat-malicious): attenzione agli [AirTag](https://www.apple.com/it/airtag/), che potrebbero essere usati a scopo di *stalking*.

Un problema, dunque.

Ora passiamo al notiziario: [Uomo arrestato per presunto stalking nei confronti di una donna, dopo avere posizionato un AirTag sull’auto di lei](https://www.wpxi.com/news/local/man-arrested-allegedly-stalking-woman-placing-airtag-tracking-device-her-car/FM5XIFQUYJFUPBRZMYKRWGQ62U/).

Leggiamo.

> Un uomo è stato arrestato ad Apollo, Pennsylvania, dopo che una donna ha dichiarato alla polizia di avere ricevuto la notifica della presenza nelle sue vicinanze di un AirTag che non era di sua proprietà.

L’uomo aveva ricevuto un ordine di *protection from abuse* (un ordine restrittivo) nei riguardi della donna e, per la legge, non avrebbe potuto avvicinarsi alla donna oltre una certa distanza.

Questo non gli ha impedito di posizionare l’AirTag sull’auto della donna. L’ordine restrittivo non ha risolto il problema.

La donna ha ricevuto la notifica da AirTag e ha fatto arrestare l’uomo. AirTag ha funzionato meglio dell’ordine restrittivo.

AirTag, più che il problema, sembra essere la soluzione.

Vedo un parallelo con tutte le situazioni ormai numerose nelle quali watch [ha salvato vite umane](https://www.cnet.com/tech/mobile/apple-watch-lifesaving-health-features-read-5-peoples-stories/) grazie alla sua capacità di [riconoscere una caduta potenzialmente grave](https://indianexpress.com/article/trending/trending-globally/apple-watch-saves-mans-life-7755449/) o una situazione di [alterazione pericolosa della frequenza cardiaca](https://www.techtimes.com/articles/268438/20211124/apple-watch-saves-life…-again-woman-says-apple-watch-saved-her-life-after-detecting-heart-rate-condition.htm).

Anche lo stalking mette a rischio vite e, dove non arriva a questo, rende la vita impossibile a tante donne che meriterebbero maggiore protezione di quella offerta da un ordine restrittivo.

Se AirTag genera problemi di stalking che, a differenza degli altri, si possono risolvere nel modo giusto prima che sia troppo tardi e permettono alle donne perseguitate di ritrovare il controllo della propria vita, forse è da raccomandare l’uso consapevole, più che ammonire sulla sicurezza.
