---
title: "La terza invenzione"
date: 2024-01-08T15:40:58+01:00
draft: false
toc: false
comments: true
categories: [Internet, Web, Software]
tags: [Copernicani, budget.g0v.it, Xanadu, Www, Berners-Lee, Tim Berners-Lee, Irc, Internet Relay Chat]
---
Prima di dire la propria sul bilancio dello Stato, sui tagli, sugli stanziamenti, qualcuno ha fatto un giro su [budget.g0v.it](https://budget.g0v.it/) a documentarsi grazie alla gradevolissima interfaccia fornita dai [Copernicani](https://copernicani.it)? questa è una bella abitudine del 2024, parlare preparati e farlo nel modo più efficace. I dati del bilancio sono disponibili ovunque, però nessun altro che io conosca li presenta in modo più suggestivo e comprensibile alla prima occhiata e prima di scendere nei dettagli.

Sono queste le iniziative che tengono viva la fiamma di Internet: comunicazione per tutti, chiara, facile, pulita. Sulla rete poggia uno strato che è il web, per il quale dobbiamo ringraziare [Tim Berners-Lee](https://www.w3.org/People/Berners-Lee/) e che permette una vista semplice di dati complessi come la rende budget.g0v.it.

Non è scontato. Ci aveva provato Ted Nelson con il progetto Xanadu, a poggiare sulla rete uno strato di semplificazione e facilitazione della comunicazione, senza esito. (Nelson [ci sta riprovando](https://xanadu.com) e gli auguro le migliori fortune).

I social hanno poggiato uno strato di interazione sullo strato di comunicazione poggiato sulla rete, ma a scopo di profitto. Difatti hanno fatto grandi numeri, ma rispetto ad altri criteri non va benissimo e i pachidermi da miliardi di utenti appaiono sul viale del (lentissimo) tramonto.

Con tutti gli autori di fantascienza in giro, qualcuno ci spiega come si potrebbe superare il web per fare ancora meglio? Al terzo tentativo, dopo Xanadu e il World Wide Web, potrebbe iniziare la vera Età dell’Oro della rete. Non per i soldi, per la comunicazione tra le persone.

Quando ci penso trovo ispirazione in [Internet Relay Chat](https://www.ionos.com/digitalguide/server/know-how/irc/), il *framework* di chat testuale che ha fatto la storia di Internet e continua a farla. (Ho scoperto che Google tiene attiva una rete di server Irc nel caso dovesse ricorrere a comunicazioni di emergenza). A differenza di [Usenet](https://nordvpn.com/blog/what-is-usenet/) che è clinicamente morto, Irc c’è ancora e suscita ancora traffico. Più di questo, è stato pensato con criteri che oggi appaiono spaventosamente attuali per governare il dialogo tra persone su temi diversi.

Questo è solo uno dei mattoni, chiaro. Qualunque tipo di messaggistica deve tenere presenti immagini, audio e video, per non parlare delle realtà immersive che potrebbero arrivare a breve. C’è la questione del copyright, quella delle transazioni e tanto altro. Un compito da fare tremare i polsi, eppure qualcosa emergerà in sostituzione di quello che c’è oggi. Visionari, inventori, startup, avanti; c’è posto.