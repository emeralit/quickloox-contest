---
title: "Non c’è tre senza cloud"
date: 2022-06-06T18:11:07+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Wwdc, Apple, Runestone, Riccardo]
---
Tutti sanno già tutto dei presunti annunci di [Wwdc](https://developer.apple.com/wwdc22/) (che si dimostreranno invariabilmente diversi dalle sparate appena si lascia la sfera dell’ovvio) ma lasciamo i frustrati a frustrarsi; io mi collego per quanto possibile (per la famiglia è orario critico) e sarò sullo [spazio Slack](https://goedel.slack.com) collegato a questo blog. Chi avesse voglia, mandi un indirizzo email a commento di questo post per ricevere un invito *no strings attached*.

A livello di contenuti e annunci, ognuno ha i suoi desideri; ho trovato interessanti i [pensieri di Riccardo](http://morrick.me/archives/9567).

Per quanto mi riguarda, posso solo dire che, complici [Runestone](https://apps.apple.com/us/app/runestone-text-editor/id1548193893) e iCloud Drive, ho iniziato a scrivere questo post su iPad, ho aggiunto due righe con iPhone e sto per completarlo su Mac. Questo aspetto dell’ecosistema per me ha un valore inestimabile e spero proprio di vedere novità in questo senso.

A tra poco per quanto riesco, con chi c’è!