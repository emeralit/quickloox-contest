---
title: "Discorsi complessi"
date: 2023-12-18T14:28:15+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [macOS, Apple, Researxh, intelligenza artificiale, ai, ia, Multimodal Reference Resolution System, Marrs, Machine Learning Research at Apple]
---
Mentre gli acchiappaclic si consumano sul dramma di Apple che rimane indietro sull’intelligenza artificiale mentre tutti gli altri avanzano a grandi passi, i ricercatori di Apple pubblicano lo studio su un [sistema per la risoluzione del contesto in un dialogo](https://arxiv.org/pdf/2311.01650.pdf).

Qualcuno avrà notato che i chatbot sono bravissimi a presentare testo con attinenza a una richiesta, ma possono perdere la bussola se intendevamo una cosa e lui ne ha capita un’altra oppure se da un primo responso vogliamo in qualche modo variare l’oggetto della conversazione.

Ci sono almeno sei modi per farlo, all’interno del dialogo oppure presentando nuovi elementi all’esterno dello stesso, come si evince dalla lettura del *paper*, né lungo né noioso. Si evince pure che l’argomento è stato già trattato da altri ricercatori, ma mai con questa ampiezza di copertura.

È un trattato scientifico e nulla garantisce che vedremo Siri o qualunque altro sistema applicare sistemi di risoluzione del contesto che spingano un passo in avanti i servizi offerti da un chatbot. Certo è che una visita alla pagina [Machine Learning Research at Apple](https://machinelearning.apple.com) è rinfrescante: non tutti a Cupertino sono concentrati sul colore da dare agli iPhone o sulla RAM da mettere in tv.