---
title: "Perseverare diabolicum"
date: 2015-04-04
comments: true
tags: [Surface3, Microsoft, iPadAir2]
---
I test di velocità pura non contano più niente da tempo, se mai avessero contato qualcosa prima. Qualcuno però ancora ci crede.<!--more-->

A beneficio di questa fetta di popolazione sono stati effettuati test preliminari sul nuovo [Surface 3](http://www.microsoftstore.com/store/mseea/it_IT/pdp/Surface-3/productID.314948400) di Microsoft, per l’appunto la terza generazione dell’apparecchio. [Ne parla Computerworld](http://www.computerworld.com/article/2905860/benchmark-scores-show-performance-gap-between-surface-3-and-surface-pro-3-ipad-air-2.html):

>I punteggi mostrano anche che iPad Air di Apple – con lo stesso prezzo base di Surface 3 a 499 dollari – è tra il 36 percento e il 93 percento più veloce.

[iPad Air 2](https://www.apple.com/it/ipad-air-2/) viaggia verso l’avvicendamento con il prossimo modello; Surface 3 deve ancora uscire.

Ah, in Italia il prezzo base di iPad Air 2 è [499 euro](http://store.apple.com/it/buy-ipad/ipad-air-2). Surface 3 parte da 609,90 euro. Uno è a 16 gigabyte, l’altro a 64, ma poco conta: iPad Air Wi-Fi a 64 gigabyte costa 599 euro e Surface 3 non ha connessione cellulare.

Ed è il terzo tentativo.