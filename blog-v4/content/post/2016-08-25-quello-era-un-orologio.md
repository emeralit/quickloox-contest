---
title: "Quello era un orologio"
date: 2016-08-25
comments: true
tags: [Watch, Apple, System]
---
L’Apple Watch [regalato a chi aggiornava a System 7.5 entro la fine di luglio 1995](https://512pixels.net/2016/08/mac-os-watch/). Altro che gli aggeggi attuali.