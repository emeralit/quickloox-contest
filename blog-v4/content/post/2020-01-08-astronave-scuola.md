---
title: "Astronave scuola"
date: 2020-01-08
comments: true
tags: [Miur]
---
Data astrale oggi, faccio il mio ingresso nell’[astronave del Ministero dell’Istruzione dell’Università e della Ricerca](https://iam.pubblica.istruzione.it/) per iscrivere la primogenita alla scuola primaria. L’operazione è riuscita perfettamente e magari forse il termine di fine gennaio, per un anno scolastico che inizia a settembre, è un attimo maniacale. D’altronde non vorrei criticare l’istituzione se per una volta le cose vengono fatte in anticipo.

Ho saltato la procedura di registrazione perché ho un’utenza Spid e quindi posso entrare direttamente nel sistema. Molto buono, molto veloce; i miei dati anagrafici vengono inseriti direttamente nella domanda. Parlando di amministrazione pubblica, sono cose da un altro pianeta. Poi l’esperienza utente prosegue e si fa più interessante.

I dati in arrivo da Spid contengono tra l’altro sesso e cellulare. Nei campi della domanda però il sesso non figura; sarà per evitare sospetti di discriminazione. Buono che il numero di cellulare sia nel formato completo di prefisso internazionale, +393334567890.

È il momento di inserire i dati dell’altro genitore, quello che ha effettuato l’accesso. Il campo sesso stavolta è presente: non interessa quello del primo genitore, ma quello del secondo sì. O viene registrato ma non visualizzato, per motivi esoterici. Boh.

Diligentemente inserisco il cellulare dell’altro genitore, come richiesto, nello stesso formato con prefisso internazionale che ho appena visto nella sezione che mi riguarda. Errore rosso; ci sono dati sbagliati. Più precisamente, il prefisso internazionale non va inserito. Cerco di capire perché memorizzare numeri di telefono in due formati diversi, ma insomma, il Ministero avrà pure le sue ragioni. Ed è il momento di compilare la sezione relativa alla primogenita. Siamo anche un po’ emozionati, non avevamo mai iscritto qualcuno alla scuola primaria.

La prima cosa che viene chiesta è il codice fiscale; nessun problema. Il cognome, il nome… il sesso. Che è chiaramente indicato nel codice fiscale. La data di nascita. Che è chiaramente indicata nel codice fiscale. Il luogo di nascita. Chiaramente indicato nel codice fiscale. È il nostro primo compito a casa? stanno guardando se siamo attenti alla lezione?

E ora la scuola. Bisogna avere il *codice meccanografico* ma siamo preparati. Ci sono quattro opzioni di frequentazione a disposizione, tempo più o meno parziale, tempo più o meno pieno. L’esercizio consiste nell’indicare un valore di priorità per ciascuna, da 1 a 4. Eseguo diligentemente. Errore rosso: le possibilità sono quattro ma posso sceglierne al massimo tre. Se lo avessi saputo lo avrei fatto subito. Avrei probabilmente adottato un sistema più funzionale, invece che indurre in errore qualchecentomila utenti su una pagina che comunque ha un costo di mantenimento, però, ok. Dopotutto, errore qui, stranezza là, ma procediamo a una velocità inusitata per la burocrazia che sono abituato a conoscere.

Si possono indicare fino a tre scuole, nel caso si voglia provare a fare domanda per un istituto che non è quello assegnato automaticamente per residenza. Proviamo e dunque inserisco il codice della seconda scuola. Appaiono sempre le quattro opzioni di frequentazione. Mi sono evoluto; ricordo che devo sceglierne al massimo tre. Solo che due di esse non sono attivabili; inutile sceglierle, non saranno attuate. Inutile mostrarle, penso tra me e me, inutile presentare opzioni inutili.

Siamo in fondo. Mancano solo alcune domande cui rispondere SI o NO. Guardo il campo testo, che probabilmente potrebbe contenere oltre un migliaio di caratteri. Riguardo le istruzioni. SI o NO. D’accordo e scrivo tutto in maiuscolo esattamente come da legenda: sai mai che scrivo *sì* invece di SI e mando in crisi il centro meccanografico?

Domanda inoltrata, mail di conferma inviata, tutto a posto. Neanche male, per lo stato italiano. Certo, vorrei incontrare quelli che hanno pensato alla *user experience*. Gente che ha vinto un concorso. Guardarli, studiare i loro movimenti, decifrare i loro complessi processi mentali capaci di dare origine a pagine che comunque, non c’è verso, almeno un piccolo incomodo lo devono sempre erogare. Forse la mancanza di logica è un requisito di legge.

Scendo dall’astronave scuola e mi dirigo verso un meritato riposo. Mi rendo conto tuttavia che, ancora una volta, nonostante tanti sforzi positivi di trovare un linguaggio comune, si tratta di un’astronave aliena.