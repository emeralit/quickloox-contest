---
title: "I signori Bonaventura"
date: 2016-07-27
comments: true
tags: [iPhone, Cook, Bonaventura]
---
Intorno a tutte le chiacchiere che si fanno sui risultati finanziari di tre mesi in tre mesi, se si allarga un momento la prospettiva si viene a sapere che è stato venduto il [miliardesimo iPhone](http://www.apple.com/newsroom/2016/07/apple-celebrates-one-billion-iphones.html).

Pensarci per un momento. Sul pianeta Terra vivono sette miliardi di persone. Certo molti iPhone non sono più in servizio; certo, molti hanno o hanno avuto più di un iPhone. Cambia poco.

Tim Cook ha commentato il traguardo con un commento abbastanza di prammatica, tranne quando ricorda che Apple non è al lavoro *to make the most*, per vendere più che si può costi quel che costi, ma *to make the best* e, semmai, raggiungere vendite importanti attraverso l’eccellenza tecnologia e di *design*.

Non è che in Apple siano [signori Bonventura](http://www.cartonionline.com/personaggi/signor_bonaventura.htm) e si ritrovino il milione (che negli anni diventò miliardo) grazie alla fortuna sfacciata che li accompagna quando camminano per strada. Un miliardo di iPhone significa pianificazione, intuizione, logistica, invenzione, una montagna di cose che mettere insieme e fare funzionare in concerto risulta assai più difficile del tagliare alluminio con precisione al milionesimo di metro.

Va notato, però, che si possono vendere tanti, tanti iPhone pensando prima di tutto a qualcosa che la vendita non è. Altri, molto più ferrati in materia, il miliardo se lo sognano.

*[L’accostamento potrà sembrare ardito. Tuttavia anche [Cuore di Mela](http://cuoredimela.accomazzi.it) sta guadagnandosi la fattibilità pensando in prima battuta a qualcosa che non è il denaro fine a se stesso, ma bensì l’idea di [cambiare il libro](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/) e farne un oggetto più adeguato al XXI secolo. Ambiziosa, l’idea, sì. Speriamo di [essere appoggiati](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac) nell’impresa, con il passaparola a finanziariamente, per poterci provare.]*