---
title: "Toccarla piano"
date: 2017-09-01
comments: true
tags: [Matias]
---
È settembre e dunque si può cominciare a parlare di Natale.

È uscita la [Wired Aluminum Keyboard](http://www.matias.ca/aluminum/mac/) di Matias per Mac, cablata Usb, con un tasto volume a sessantaquattro gradazioni invece delle consuete sedici, tastierino numerico e tutta la qualità Matias (che [perdura da tempo](https://macintelligence.org/posts/2017-01-07-aspettando-una-retro-illuminazione/)).

Esiste con *layout* italiano.

Costa cinquantanove dollari.

Praticamente, la tempesta perfetta.