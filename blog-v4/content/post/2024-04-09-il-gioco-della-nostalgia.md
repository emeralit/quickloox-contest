---
title: "Il gioco della nostalgia"
date: 2024-04-09T01:51:57+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Swift Playgrounds, HyperCard]
---
Dice che *una volta ti veniva l’idea per un gioco e con HyperCard era fatto. Oggi come mimino devi imparare un linguaggio*.

Sì e anche no. [HyperCard](https://hypercard.org) è stato un capolavoro di semplicità e intelligenza, con un approccio a quel tempo veramente rivoluzionario. Si prototipava, è vero, alla grande.

Poi però HyperTalk era immediato, su alcune cose. Non proprio su tutte. E poi parliamo di un contesto dove non c’era Internet, il colore era un optional di lusso, i processori avevano prestazioni che è meglio non confrontare con l’oggi. HyperCard era fantastico per un mondo monoutente, monocromatico e non troppo veloce.

[Myst](https://cyan.com/games/myst/) ha sconvolto il mondo dei giochi ed è stato creato su HyperCard. Ma Cyan Software ha truccato il motore della macchina come nessun altro era in grado di fare in quel periodo. Sì, era HyperCard, come ci sono le auto *commerciali* che fanno la Parigi-Dakar o qualche rally. Sono commerciali prima di entrare in officina.

Consiglierei al nostalgico un po’ di stage su [Swift Playgrounds](https://developer.apple.com/swift-playgrounds/). La possibilità di apprendimento del programma è veramente al di là del bene e del male. Il primo corso di lezioni può essere seguito quasi da un analfabeta, che potrebbe pure imparare a leggere. Un bambino di sei anni, se ha voglia ed è interessato, il primo corso se lo mangia. E non arriva poco.

Un po’ alla volta, con un po’ di auto dal sito sviluppatori, ci si rende conto che creare un giochino semplice si fa con iPad e Swift Playgrounds e quasi niente altro.

La nostalgia è canaglia perché ci chiude gli occhi sul domani. Non quello lontano, quello tra un’ora, il tempo di scaricare una app, predisporsi a imparare, fare qualche esperimento. Python a confronto sembra un master postuniversitario (ed è incasinato, ma semplice e amichevole).