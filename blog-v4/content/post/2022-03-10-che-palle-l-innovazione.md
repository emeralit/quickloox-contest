---
title: "Che palle l’innovazione"
date: 2022-03-10T23:31:43+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Marco, tv, Tim Cook, Friday Night, MLB, TV+, Australia, Brasile, Canada, Corea del Sud, Giappone, Italia, Messico, Portorico, Regno Unito, Stati Uniti, Jason Snell, Six Colors]
---
Tim Cook [ha detto il vero](https://www.apple.com/apple-events/march-2022/): il baseball su TV+ sarà visibile in tutte le nazioni dove si può avere la app.

Il problema, per me e per **Marco** come minimo, è che dalla frase manca il _quando_: all’inizio [lo vedranno solo nove nazioni](https://arstechnica.com/gadgets/2022/03/friday-night-baseball-on-deck-for-apple-tv-subscribers/#p3) (Australia, Brasile, Canada, Corea del Sud, Giappone, Messico, Portorico, Regno Unito, Stati Uniti).

La speranza, visto che è stato dichiarato un allargamento successivo del servizio, è che avvenga presto e coinvolga anche l’Italia nel secondo _inning_ di diffusione delle partite.

Se fosse solamente una fissa di appassionati del baseball, non se ne occuperebbe Jason Snell come [ha fatto su Six Colors](https://sixcolors.com/post/2022/03/apples-big-baseball-deal-detailed/). Lui ha una padronanza anche tecnica e non ha bisogno di trovare argomenti da chiacchiera per mantenere l’attenzione del pubblico. Scrive così:

> La verità è che questo è il futuro a lungo termine per gli sport teletrasmessi. 

Sullo schermo volano palle, dietro si fa innovazione; anche se strategica, anche se sui servizi, anche se mediale, anche se non si inventa un nuovo processore per un miliardo di apparecchi. Apple non lo fa mai proprio come gli altri, anche dove sarebbe tanto facile e scontato. Anche dove si può inseguire il profitto immediato e chi vivrà vedrà.