---
title: "Widget, gadget"
date: 2015-08-18
comments: true
tags: [Dashboard, Konfabulator, Gruber, OneClick, DesktopX, gadget, widget, Windows, Apple, scripting, JavaScript, WebCore, Html, Css, Cocoa]
---
Ancora un secondo sulla [questione dei widget](https://macintelligence.org/posts/2015-08-17-widget-e-non/), dato che ho trovato un vecchio [*post* di John Gruber](http://daringfireball.net/2004/06/dashboard_vs_konfabulator) che riempie varie mie lacune in argomento. È piuttosto lungo, comunque consigliato. Riporto alcuni brani chiave.<!--more-->

>Konfabulator sarebbe originale perché i suoi *widget* si basano su un linguaggio di scripting? Esistono frotte di runtime di scripting che permettono di creare piccole-applicazioni-con-finestra. Per esempio, su Mac OS, avevamo OneClick.

>Konfabulator contiene il proprio motore di runtime JavaScript. I suoi schemi di interfaccia utente sono specificati in un formato Xml personalizzato. […] Dashboard, d’altra parte, si basa su WebCore, il motore di impaginazione e scripting dietro Safari, che è quello incorporato in OS X. Per quanto riguarda l’interfaccia, i gadget di Dashboard sono specificati in Html e Css.

L’idea è sempre quella; la realizzazione è molto diversa.

>I gadget Dashboard sono uteriormente estendibili usando Cocoa. Non è necessario farlo, ma esiste l’opzione di farlo se con JavaScript da solo non ci si arriva.

L’idea che *widget* e applicazioni si distinguano in base al linguaggio usato per scriverle ancora una volta non regge.

>L’idea di un runtime scriptabile che mostri piccoli applet nelle loro finestre non è di Konfabulator. (Si veda DesktopX, un prodotto simile per Windows nato nel 1999).

Vero è che Apple li chiamò inizialmente *gadget*, per poi rubare il nome ai *widget* di Konfabulator. Sembra un po’ poco, però, per istruire un processo.
