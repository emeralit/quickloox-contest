---
title: "With a little help"
date: 2022-01-29T01:02:48+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Farò finta di non accorgermi della deriva egotistica dell’[ultimo post](https://macintelligence.org/posts/2022-01-28-a-volte-ritornano/) e di questo, mostrando [un tweet qualsiasi](https://twitter.com/macjournals/status/1486822278277046274):

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Tim Cook to CNBC on China: “We grew 21% in China, which we’re very proud of. It was an all time revenue record for us in the country … We had a very strong customer response to the iPhone 13 family and in particular, we set a record number of upgraders in China…” (1/2)</p>&mdash; MacJournals.com (@macjournals) <a href="https://twitter.com/macjournals/status/1486822278277046274?ref_src=twsrc%5Etfw">January 27, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

che ha il solo scopo di mostrare un tweet; non avevo ancora imparato a farlo correttamente.

Chi aveva manifestato interesse per averle, troverà le date a fianco dei titoli dei post presentati nella pagina home.

Aggiorno il *to do* in ordine di priorità decrescente:

- ritorno dei vecchi post;
- valutazione di un nuovo tema;
- modifica del feed RSS in modo che mostri il post completo e non solo la prima parte;
- altre migliorie assortite.

Sempre grazie a **Mimmo** che si è messo con ordine e disciplina a trovare cose nella documentazione e a fare prove.
