---
title: "Una notte al Museo"
date: 2021-07-08T01:04:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [All About Apple] 
---
Apprendo adesso che stasera [il museo All About Apple sarà straordinariamente aperto dalle 21 alle 23](https://www.allaboutapple.com/2021/07/open-night-pfizer-all-about-apple-apre-in-notturna-giovedi-8-luglio/), con ingresso ridotto a cinque euro e nessun bisogno di prenotazione (anche se gli ingressi saranno necessariamente regolati).

Sono nelle Marche in attesa che arrivi il pezzo per riparare la macchina e piuttosto bloccato. Chi ha una macchina che funziona, la usi. Queste occasioni al Museo diventano rapidamente serata indimenticabili.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*