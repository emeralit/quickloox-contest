---
title: "Gli asparagi e l’immortalità dei dati"
date: 2017-02-19
comments: true
tags: [Saffo, Shangri-La]
---
Discussa l’impossibile immortalità, ma sarebbe meglio dire immutabilità, dei [supporti](https://macintelligence.org/posts/2017-02-17-gli-asparagi-e-l-immortalita-dei-supporti/) e dei [formati](http://macintelligence.org), rimane la questione al cuore del problema.

*Come posso conservare i miei dati per sempre?*

Già detto che *per sempre* è una illusione (l’universo in cui ci troviamo finirà, o ricomincerà, o tutt’e due le cose, ma sempre con un *reset* totale), rimane la più umana aspirazione di *più a lungo possibile*.

Il problema è che siamo umani e quindi riusciamo solo a praticare soluzioni umane: imperfette, fallibili, precarie.

*Ma perché gli ingegneri non si impegnano a trovare una soluzione definitiva?*

Anche in questo caso l’informatica è diventata [Shangri-La](https://it.wikipedia.org/wiki/Shangri-La), la terra dei sogni. Ogni giorno ci si sveglia dovendosi guadagnare il pane, prendiamo raffreddori, mettiamo peso e perdiamo peso, andiamo dal dentista, andiamo in palestra. Ogni due giorni il nostro corpo è composto da cellule totalmente diverse da quelle di due giorni prima: eppure ci mettiamo davanti a un computer, premiamo-clicchiamo-tocchiamo-diciamo *salva* e deve essere immutabile, per sempre. Nel frattempo cambia il clima e molte miglia più in là in un freddo lago scozzese si distende una misteriosa creatura.

Ancora una volta, una verità spiacevole: la conservazione dei dati dipende da *te*, non dall’hardware, non dal software. Anche perché, se avessero risolto il problema della conservazione eterna dei dati nel 1976 con l’Apple I, oggi avremmo *audiocassette* immortali.

Eh, una volta non era così, certo. La cara vecchia carta, la pellicola e c’è sempre un genio che tira in ballo le incisioni rupestri. Senza avere la minima idea di quante siano scomparse per sempre e quante, benissimo conservate, mai verranno lette da alcuno, sepolte sotto una montagna o un deposito alluvionale. Dei versi di [Saffo](http://www.treccani.it/enciclopedia/saffo/) rimangono pochi frammenti; il problema non è su che cosa scrivesse, ma che le copie di riserva non sono bastate. La pergamena sfida i secoli, solo che è costosissima e ingombrante; la tua libreria di casa, per ospitare pergamena al posto della [carta acida](http://printwiki.org/Acid_Paper) e deperibile che usa oggi, andrebbe triplicata nel volume e decuplicata nei costi. Se serve un preventivo, sono disponibile.

Di tante statue greche e romane, quelle che abbiamo ritrovato, esistono solo pezzi o monconi. Esistono meritorie fondazioni che restaurano i film su pellicola, perché le vecchie pizze si leggono ancora b-e-n-i-s-s-i-m-o, solo che i colori se ne vanno, l’audio si degrada, le inquadrature si fanno sempre più slavate. 

Pensi che invece la tua raccolta di diapositive sia pristina e intatta, con gli stessi colori e la stessa precisione di venti-trenta-cinquanta anni fa quando è stata scattata la foto? Non starei così tranquillo. I vinili si consumano a ogni ascolto e comunque, anche se letti con il laser, vengono toccati. Tutti hanno presente che succede al marmo di Carrara quando, sui monumenti, ogni passante lascia un lieve tocco del polpastrello. L’Ultima Cena di Leonardo a Milano ha visite contingentate perché persino il respiro dei visitatori va tenuto sotto controllo e in quel caso la scelta dei materiali pittorici non è stata felicissima dal punto di vista della conservazione.

Le nostre cose sono caduche. I nostri dati pure.

Per conservare al meglio i nostri pavimenti ce ne prendiamo cura quotidianamente o quasi. Non si capisce perché i nostri dati siano fatti per essere abbandonati su un disco e, per questo fatto, resteranno intonsi per l’eternità. Non è sul digitale che prenderemo la rivincita contro la nostra condizione umana.

Domani potrebbe cambiare. Sono allo studio cristalli olografici che si autoriparano e contengono, se non per sempre molto ma molto a lungo, in volumi risibili una quantità immensa di informazioni. Analoga speranza suscitano le memorie organiche, essenzialmente Dna riconvertito a uso archiviazione.

Niente di questo è disponibile a prezzo accettabile e in numeri di massa. Per il momento l’unica soluzione valida per conservare validamente i dati è provvedere regolarmente a ridondare, con più copie, e diversificare nei metodi di memorizzazione. E una cassetta di sicurezza termoregolata, imbottita, sottovuoto, isolata…? Nah. I dati, qualunque sia la tecnologia oggi, piano piano si degradano. Si chiama [bit rot](https://macintelligence.org/posts/2014-06-16-mal-comune/), putrefazione dei bit.

Se infine qualcuno fosse veramente testardo e determinato a trovare un sistema di memorizzazione che ci penso adesso e poi non ci penso più, si ricordi che deve essere [schermato dai raggi cosmici](https://www.lanl.gov/science/NSS/issue1_2012/story4full.shtml). Tante buone cose.