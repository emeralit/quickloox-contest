---
title: "Quel che non riluce"
date: 2015-01-29
comments: true
tags: [Mac, iPad, Natale, S&P500, iPad, iPhone, Cina, iOS, Mac, AppleTV, tv, GooglePlay, AppStore, Thompson]
---
Facile liquidare i [risultati del trimestre natalizio di Apple](https://www.apple.com/it/pr/library/2015/01/27Apple-Reports-Record-First-Quarter-Results.html) con i boati e i fuochi d’artificio.<!--more-->

Sono finiti in cassa 74,6 miliardi di dollari contro 57,6 di un anno fa. 74,5 milioni di iPhone venduti, una cifra enorme rispetto ai 51 milioni del Natale 2013. A causa delle fluttuazioni del dollaro, Apple ha perso per giunta il cinque percento del fatturato potenziale. Ovvero, [per dirla con Ben Thompson](http://stratechery.com/2015/bad-assumptions/),

>Apple ha perso più denaro per le fluttuazioni del dollaro dei profitti che Google accumula in un trimestre.

Normalmente le azioni di Apple affondano dopo ogni trimestre positivo, perché gli analisti trovano un pretesto qualsiasi per esprimere pessimismo. Stavolta era troppo grossa: le azioni sono schizzate in alto tanto che l’indice Standard & Poor’s 500 sarebbe stato a -0,5 percento senza Apple e invece, con Apple, ha fatto +1,3 percento.

<blockquote class="twitter-tweet" lang="en"><p><a href="https://twitter.com/search?q=%24AAPL&amp;src=ctag">$AAPL</a> to the rescue. S&amp;P 500 Q4 earnings growth expands to 1.3% after <a href="https://twitter.com/search?q=%24APPL&amp;src=ctag">$APPL</a> report. Ex. <a href="https://twitter.com/search?q=%24APPL&amp;src=ctag">$APPL</a> it&#39;s -0.5%, per <a href="https://twitter.com/FactSet">@FactSet</a> (h/t <a href="https://twitter.com/LauricellaTom">@LauricellaTom</a>)</p>&mdash; Kristen Scholer (@KristenScholer) <a href="https://twitter.com/KristenScholer/status/560477011576389632">January 28, 2015</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

A parte un ente governativo americano di mutui reduce da una operazione di contabilità selvaggia, [nessuna altra azienda nella storia](http://en.wikipedia.org/wiki/List_of_largest_corporate_profits_and_losses#Largest_Corporate_Quarterly_Earnings_of_All_Time) ha totalizzato altrettanti profitti in un trimestre. Delle prime venticinque *performance*, diciotto sono di aziende petrolifere, sei di Apple.

Centosettantotto miliardi di dollari in cassa, trentaquattromila iPhone venduti ogni ora, 8,3 milioni di dollari in tasca ogni sessanta minuti, un miliardo di apparecchi iOS venduti dal 2007 eccetera eccetera.

Tutta grancassa, tutta roba che riluce. L’oro è altro, anche se sette dollari su dieci vengono guadagnati da Apple grazie alla vendita di iPhone.

L’oro è per esempio Apple TV che senza nessun *battage*, senza alcuna promozione, senza la minima enfasi, a marketing zero ha totalizzato venticinque milioni di unità vendute.

Sono gli oltre cinque milioni di Mac venduti, che per un pelo non è record assoluto. La vendita di Mac è cresciuta anno su anno per trentaquattro trimestri degli ultimi trentacinque, mentre il resto del mercato stenta.

Nessun’altra azienda potrebbe guardare alla flessione di iPad, 4,6 milioni di unità in meno rispetto allo scorso anno, con tranquillità e ottimismo come fa Apple. Perché sanno benissimo che gli iPad hanno un ciclo di ricambio più lungo, sono una scelta meditata mentre molti iPhone sono acquistati di impulso e soprattutto ogni iPad in meno non è una tavoletta dei concorrenti; è un MacBook Air in più o un iPhone 6 Plus in più. Ogni cinque acquisti effettuati *online* con una tavoletta, quattro arrivano da iPad.

Delle transazioni commerciali *contactless* effettuate tramite i tre più grandi circuiti di carta i credito negli Stati Uniti, Apple Pay copre due dollari ogni tre. I siti di *rumors* si avvitano su come sarà il prossimo iPhone e intanto, sottocoperta, Apple sta entrando da *leader* nel meccanismo dei pagamenti via *smartphone*.

Sono questi i meccanismi da tenere sotto osservazione, perché raccontano molto più dei grandi numeri e dei grandi titoli.

Sento già quello che segnala il *bug* su Yosemite non riparato o la cattiva esperienza in Apple Store con l’iPhone fuori garanzia bisognoso di riparazione. Non è che Apple fa soldi a palate e quindi va tutto bene; la qualità del software può (e deve) migliorare, App Store – in crescita mostruosa – deve migliorare le funzioni di ricerca e di presentazione, il *business* della musica *online* è in declino, bla bla bla. Apple non è il paradiso dell’informatica, ma il migliore dei mondi possibili. E chi si racconta che *faccio le stesse cose spendendo meno* ne fa di meno, oppure non ha chiaro *come* le fa.

Poche note per finire.

Quando si chiacchiera dei massimi sistemi di Apple, bisogna ricordarsi come funziona la cultura orientale, perché la Cina sta diventando una zona di mercato formidabile per i prodotti con la mela sopra. Esce un iPhone con lo schermo più grande di quanto possa coprire il movimento del pollice? Invece che ridere di Steve Jobs che raccontava come lo schermo di iPhone avesse le dimensioni che consentono l’operatività con una sola mano, pensa come un cinese: i numeri dicono chiaramente che il successo di iPhone è planetario, ma trainato dall’Estremo Oriente. A ovest lo schermo grande è un *plus*, a est è un *must*. Se si trascura questo, si sbagliano i giudizi.

Su Google Play i download sono il 60 percento più che su App Store. Su App Store [il fatturato è il 70 percento più che su Google Play](http://recode.net/2015/01/28/google-play-downloads-are-growing-faster-but-apple-is-still-winning-the-money-race/). È – in generale – App Store che dà il pane a chi scrive le *app* e di conseguenza favorisce la nascita di *app* sempre più utili, innovative, migliori. App Store fa il progresso, Google Play è il mondo dei potrei-ma-non-voglio. Fosse per loro, saremmo fermi al 2006.

Criticare i prodotti e le strategie di Apple ha senso in molte situazioni e spesso ci sta tutto. Purtroppo bisogna affrontare la realtà: gli errori marginali di Apple vengono oscurati dall’efficacia della strategia centrale. Può non piacere che MacBook Air abbia poche porte o che, come leggo ultimamente su alcune *mailing list*, sia stato abbandonato il mercato sedicente professionale. Di sicuro, con questi numeri, pare che Apple faccia incetta di dilettanti. E comunque, con questi numeri, non cambiano certo strategia.

Meglio leggersi [l’articolo di Ben Thompson già citato](http://stratechery.com/2015/bad-assumptions/), al momento la migliore analisi della situazione:

>Non è vero che i mercati siano monolitici.
>Non è vero che i consumatori pensino solo alla velocità e al prezzo.
>Non è vero che le dichiarazioni di Apple sull’intenzione di *realizzare i migliori prodotti* siano solo fumo di marketing.

Se c’è una cosa positiva in tutto il luccichio dei risultati di vendita di questo Natale, è che costringono a ripulire il cervello dalla bruma dei ragionamenti troppo semplici e troppo abituali.