---
title: "L’ora alfa per le beta"
date: 2021-07-01T01:50:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [macOS, iPadOS, iOS, watchOS, iPhone, iPad, Mac, watch] 
---
Breve sondaggio: qualcuno ha intenzione di installare le beta pubbliche dei nuovi sistemi operativi per iPhone, iPad, Mac, watch…?

Apple mi ha colto un po’ di sorpresa perché sono ancora a lavorare in località amene con qualche vincolo al download di ingenti masse di dati. Ma appena torno sotto l’ombrello di una connessione stabile, la tentazione è forte.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*