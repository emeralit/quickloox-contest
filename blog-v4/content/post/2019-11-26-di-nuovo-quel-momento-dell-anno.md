---
title: "Di nuovo quel momento dell’anno"
date: 2019-11-26
comments: true
tags: [Apple, Xmas, Natale]
---
Ci sono in agenda albero e presepe, la letterina al Babbo reclama priorità, manca un mese eppure ci sentiamo in ritardo. Qualcuno [ha già fatto i compiti](https://www.youtube.com/watch?v=LDeRyyDrS40), in compenso.

<iframe width="560" height="315" src="https://www.youtube.com/embed/LDeRyyDrS40" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Brett Terpstra [continua ad approfondire Fish](https://brettterpstra.com/2019/11/25/all-my-fish-functions/) e *Ars Technica* [parla di MacBook Pro 16”](https://macdailynews.com/2019/11/25/ars-technica-reviews-apples-16-inch-macbook-pro-a-notable-step-up-for-performance-minded-users/) come di *un notevole salto di livello per gli utenti attenti alle prestazioni*.

Nel 536 una eruzione vulcanica [tenne per diciotto mesi l’Europa al buio e al freddo](https://www.sciencemag.org/news/2018/11/why-536-was-worst-year-be-alive), [Basecamp](https://basecamp.com) ha un piano gratuito per essere provato (merita) e un sacco di altre cose.

Domani.