---
title: "Con iOS si vola"
date: 2014-12-19
comments: true
tags: [X-Plane, Sande, Cessna, iPhone, iOS8]
---
Nell’imminenza delle feste viene più naturale segnalare software di qualche interesse. L’illusione è di avere più tempo, avere più soldi e anche essere più buoni. Se non altro è software davvero interessante.<!--more-->

Come [X-Plane 10 Mobile Flight Simulator](https://itunes.apple.com/it/app/x-plane-10-mobile-flight-simulator/id566661426?l=en&mt=8). Visto che hanno già svolto il lavoro di parlarne, lo rubo a [The Unofficial Apple Weblog](http://www.tuaw.com/2014/12/17/x-plane-10-is-out-for-ios-and-it-is-terrific/):

>X-Plane è sufficientemente fedele da essere stato certificato dalla Federal Aviation Administration per alcune fasi dell’addestramento al pilotaggio e il nostro Steve Sande ha persino menzionato piloti di linea ingannare il tempo in attesa di salire a bordo, “giocando” con X-Plane 9 sui loro iPhone.

>La versione gratuita permette di volare su un Cessna 172 […] La grafica è di alto livello. Si può guardare liberamente fuori dall’abitacolo trascinando il dito. Un tocco abilita la visione dall’esterno, con più angolazioni. […] Si possono simulare guasti e condizioni meteo difficili. Le città sono rese mediante edifici 3D che brillano nella notte. È possibile decidere in che orario si vola. […] Via Game Center si può partecipare ad avventure in compagnia di altri piloti. Una scuola di volo incorporata insegna i fondamentali del volo.

>Esito a definire X-Plane un gioco, perché è una grande simulazione di volo. È una app universale, che vuole iOS 8 ed è ottimizzata per le linee 5 e 6 di iPhone. Altamente raccomandato.

E una cosa così è *gratis*. Arriva davvero il Natale.