---
title: "Certificazione definitiva"
date: 2016-07-18
comments: true
tags: [Poste]
---
Il mio fabbisogno di posta elettronica certificata, da gennaio 2016 a oggi, ammonta a due messaggi inviati e zero ricevuti. Ecco perché la mia posta elettronica certificata è la più economica e spartana che ho trovato. Mi interessa che le mail partano e arrivino, tutto il resto è secondario. Due cose, però, non ho capito.<!--more-->

La prima è che per richiedere una casella di posta elettronica a Poste.it bisogna inviare un fax. Che è costosissimo come sistema, rispetto a una buona autenticazione via web. Perché un servizio ai minimi del prezzo e dell’offerta si permette di sprecare denaro con una procedura inefficiente e costosa?

Se devi chiedere un reset della password, a integrazione di quanto sopra, devi mandare un fax. Non è antiquato, è antieconomico.

La seconda ragione è che telefoni al numero verde per avere un chiarimento e ti risponde una scortese operatrice che dà il peggio di sé e alla fine – secondo lei, perché il problema è rimasto irrisolto – attacca senza neanche salutare. A mio parere è molto più costoso mantenere un servizio di assistenza telefonica irritante e inutile che uno cortese e utile, che almeno incoraggia ulteriori iscrizioni al servizio.

Cosa che sono qui per dissuadere dal fare. Una posta elettronica certificata di Poste.it è vicina al peggio che si possa trovare sul mercato in materia. Un conto è fare come me e assolvere a un obbligo senza alcuna altra pretesa. In qualunque altra configurazione è da evitare, specialmente se non piacciono i fax. Esperienza certificata e difatti neanche lo linko, sai mai che uno cliccasse per errore.