---
title: "Ritorno all’analogico"
date: 2021-08-01T00:46:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [analogico, digitale, F24, Cuore di Mela] 
---
Abbiamo interrotto un soggiorno di quasi due mesi al mare all’incirca perché il trenta luglio terminava un leasing.

Avevamo tre giorni per restituire il bene oppure pagare il riscatto ed entrarne in possesso. Soprattutto, la comunicazione relativa sarebbe arrivata per posta cartacea, al mio indirizzo canonico. Bisognava essere a casa.

Nel rientrare, siamo stati allietati da altre comunicazioni.

La tassa comunale sui rifiuti: dovuta, modesta, scadenza tranquillissima. Unico neo: sette fogli di carta per farti pagare un F24 semplificato composto da due righe.

Niente comunque batte una casa editrice che di questo periodo, ogni anno, invia il riepilogo dei diritti d’autore per l’anno prima.

Da diversi anni non scrivo libri e so già che non sono maturati diritti su quelli vecchi che da tempo hanno smesso di vendere.

La busta della casa editrice, per dirmelo, contiene *ventinove* fogli.

Non nego che vedere su carta la lettera di una amante del passato, uno zio d’America, un fan club di [Cuore di Mela](https://books.apple.com/it/book/cuore-di-mela/id1168482926) farebbe sorpresa, piacere, gusto, interesse, mistero.

Ventinove fogli per una comunicazione amministrativa che starebbe in una tabellina non mi danno alcun brivido.

Semmai, mi fanno chiedere quanti costi anacronistici debba infliggersi nel 2021 una casa editrice.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               