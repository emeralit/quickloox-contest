---
title: "Passare parola"
date: 2024-01-26T00:28:03+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet, Security]
tags: [NIST, National Institute of Standards and Technology]
---
Prego sentitamente chi avesse contatti con organizzazioni e siti italiani, di qualsiasi ordine e grado, di diffondere le [linee guida aggiornate dell’Istituto americano degli standard e della tecnologia (Nist) relativamente alle identità digitali](https://pages.nist.gov/800-63-3/sp800-63b.html).

Le linee guida suddette raccomandano come comportarsi ad aziende, enti governativi, organizzazioni, professionisti, chiunque. Il Nist è una autorità, non un gruppetto di ricercatori di una università sperduta.

Il documento è molto lungo e difficile da leggere, però se si potesse dare una certa enfasi al paragrafo 5.1.1.2, Memorized Secrets Verifiers, ne sarei grato.

Più del resto, il seguente capoverso:

>Verifiers SHOULD NOT impose other composition rules (e.g., requiring mixtures of different character types or prohibiting consecutively repeated characters) for memorized secrets. Verifiers SHOULD NOT require memorized secrets to be changed arbitrarily (e.g., periodically).

Lo traduco per comodità:

I verificatori NON DOVREBBERO imporre altre regole di composizione (per esempio richiedere mescolanze di tipi diversi di carattere o proibire la ripetizione consecutiva di caratteri) per i segreti memorizzati. I verificatori NON DOVREBBERO richiedere di cambiare i segreti memorizzati per motivi arbitrari (per esempio periodicamente).

I *segreti memorizzati*, nell’insopportabile burocratese del Nist, sono le *password*. I *verificatori* sono il system administrator di una azienda, i gestori di un sito, chiunque predisponga un accesso via password a qualcosa e debba controllare la validità delle password che vengono inserite.

Il Nist pubblica una serie di regole cui dovrebbero sottostare le password e anche quelle cui *non* dovrebbero sottostare, perché superate dalla tecnologia, o inutili ai fini della protezione o altro.

Queste linee guida sono il frutto di indagini e riscontri sul mondo della sicurezza, presa d’atto di studi e ricerche, consultazione con specialisti. Non c’è il capriccio, manca il *lo so io che cosa bisogna fare*.

Rendo infine in italiano semplice e spero chiaro.

Imposizioni tipo *una maiuscola, un numero, un carattere speciale* NON DOVREBBERO essere adottate (uso anch’io il maiuscolo esortativo del NIST). In teoria dovrebbe bastare una [vignetta di Xkcd](https://xkcd.com/936/) per capire, ma no.

Obblighi tipo *evitare caratteri consecutivi uguali* NON ANDREBBERO imposti.

Il cambio periodico della password ogni X tempo NON DOVREBBE essere richiesto.

Se tutto ciò mi eviterà la solenne scocciatura di ruotare una password in più di quelle che già toccano senza motivo, o di perdere tempo a soddisfare regole cervellotiche che tolgono sicurezza invece che aggiungerne, sarà stato tempo ben speso. Ringrazio in anticipo.