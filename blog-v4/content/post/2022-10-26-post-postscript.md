---
title: "Post-PostScript"
date: 2022-10-26T03:09:00+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [PostScript, Anteprima, macOS, Ventura]
---
Vero che da anni non mi passava sotto le mani un file PostScript. Vero che me ne è passato sotto le mani uno giusto la scorsa settimana.

Vero è che Anteprima in effetti [non ha bisogno del supporto dei file PostScript](https://support.apple.com/en-us/HT213250). Vero pure che non mi dispiaceva averlo, anche a fare niente: ho sempre considerato Anteprima una scatola delle sorprese da cui di tanto in tanto usciva qualcosa di inaspettato e piacevole, alla stregua di TextEdit e altre amenità di macOS.

Vero, come spiega la pagina del supporto, che per visionare un file PostScript ci sono millanta alternative anche gratis.

PostScript (che rimane essenziale per la stampa e che è comunque riconosciuto dalle stampanti installate nel sistema) è stato un bel pezzo di storia. Se solo non fosse stato così lento, avrebbe conquistato il mondo. Oggi si ritira da Anteprima.