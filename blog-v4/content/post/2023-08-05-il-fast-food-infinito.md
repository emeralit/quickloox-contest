---
title: "Il fast food infinito"
date: 2023-08-05T02:34:52+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Midjourney, Intelligenza artificiale, Taco Bell, tie-dye]
---
Può capitare nel mese che le figlie chiedano in via eccezionale di mangiare fast food. Le accontento volentieri, perché è una mia debolezza. Addentare un hamburger e seguire con una Coca-cola è uno dei piaceri della vita. L’Italia, al netto di tanti lati positivi, ne presenta anche di negativi e uno di questi è l’assenza di Taco Bell, la catena di fast food messicano le cui sedi sono un fattore di valutazione se progettiamo una visita a una città europea.

Al netto delle considerazioni sulla buona alimentazione e sulla salute, parlando unicamente di piacere, amo il fast food se tra una consumazione e l’altra c’è un giusto lasso di tempo. Se non passa come minimo una settimana, non ho alcun desiderio di fast food. È un piacere se preso nella giusta dose, con i giusti tempi. Altrimenti scade subito.

Conosco gente che ha scoperto [Midjourney](https://www.midjourney.com/) (o equivalente). Probabilmente ha avuto problemi con il disegno, specie a scuola, oppure si è portata dietro un complesso di inadeguatezza di qualche genere. Midjourney è per loro una rivincita dell’ego; possono finalmente disegnare senza saper disegnare. Peggio degli amici che premevano per invitarti a casa loro per la fantozziana visione delle diapositive di viaggio, inondano social, chat, canali di ogni tipo con le loro, come se lo fossero, creazioni: Giulio Cesare in costume da bagno a Ostia, l’elegante decorato come una [tie-dye](https://thetiedyehippie.com), il grattacielo capovolto con il cappotto di astrakan, Mel Gibson con i capelli di Bruce Willis e il naso di Gwyneth Paltrow e avanti così senza soluzione di continuità, con i prompt più svalvolati che riescono a trovare, con una ragione validissima per farlo: perché possono.

Il fast food è goloso ma di scarsa qualità. Negli Stati Uniti una visita nel quartiere appropriato mostra impietosamente i danni di una alimentazione a base esclusiva di fast food: corpi deformati, menti ottenebrate.

Midjourney è il fast food dell’immagine. Costa poco, puoi soddisfare i tuoi capricci più infantili, superare inibizioni e repressioni, sentirti finalmente come gli altri anzi meglio e puoi masticare immagini all’infinito, con infiniti prompt, alla ricerca sempre più spinta di un assurdo iconografico che nessuno altro abbia ancora raggiunto, senza sosta, senza ritegno e senza discrezione.

Nessuno dei consumatori di Midjourney diventerà grande obeso o svilupperà complicazioni da disequilibri nutritivi. Nel corpo. Ma osservo con orrore il degradarsi della loro attività cognitiva. E ora la serie dei personaggi celebri vestiti da marinaretto, con Maria Stuarda, Giovanni Papini e Sandro Mazzola, nel nome del progresso e dell’innovazione. Controcorrente, chiaro.

*Gli aggiornamenti in agosto del blog potrebbero essere irregolari o infrequenti.*