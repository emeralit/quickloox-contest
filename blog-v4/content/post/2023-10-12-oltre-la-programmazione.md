---
title: "Oltre la programmazione"
date: 2023-10-12T18:48:43+01:00
draft: false
toc: false
comments: true
categories: [Software, Hardware, Education]
tags: [SwiftIO, SwiftIO Playground, Swift]
---
[SwiftIO Playground](https://www.crowdsupply.com/madmachine-limited/swiftio-playground) ha passato la fase di finanziamento e quindi verrà prodotto.

La *piattaforma di esercizio definitiva per la programmazione hardware in Swift*, a vederla, pare una rappresentazione fisica del linguaggio [Swift](https://developer.apple.com/swift/): elegante, strutturata, ordinata, potente.

Swift nasce in Apple (dove viene sempre più utilizzato per costruire sistemi operativi e app) ma è open source, ragion per cui è utilizzabile ovunque da chiunque.

A centosedici dollari, un kit del genere farebbe la felicità di qualsiasi laboratorio didattico di informatica e la fortuna di chi vi studiasse.

Non si ha idea di quanto valga un progetto capace di legare con efficacia hardware e software e mostrare a chi impara come applicare gli effetti del codice nel mondo reale.

Ci sono insegnanti con un po’ di fegato, nei dintorni?