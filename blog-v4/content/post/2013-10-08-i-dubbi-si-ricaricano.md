---
title: "I dubbi si ricaricano"
date: 2013-10-08
comments: true
tags: [Apple]
---
Scoperto che anche nella Apple Wireless Keyboard II c’erano ricaricabili, come [dentro Magic Trackpad](https://macintelligence.org/posts/2013-07-27-ritorno-alle-origini/).<!--more-->

La loro durata è stata di 148 giorni, contro i cambi precedenti – di usa e getta – durati rispettivamente 338, 351 e 541 giorni.

Il sospetto che le ricaricabili durino molto meno si rafforza, anche se devo ancora compiere almeno un giro con pile usa e getta soggette alle nuove regole di amministrazione (non spengo mai gli apparecchi quando chiudo Bluetooth su Mac e lascio che siano loro ad attivare le loro funzioni interne di risparmio energetico).

Nel frattempo mi chiedo se, dopo essere passati da tre batterie a due con il passaggio da Wireless Keyboard a [Wireless Keyboard II](http://store.apple.com/it/product/MC184T/B/tastiera-apple-wireless-keyboard?fnode=56), vedremo un modello III che chiede una batteria sola.