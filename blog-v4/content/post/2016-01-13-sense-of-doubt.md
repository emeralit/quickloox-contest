---
title: "Sense of Doubt"
date: 2016-01-13
comments: true
tags: [Bowie, BowieNet, Verbasizer, Hadley, Iss, Space, Oddity, Major, Tom]
---
Ora che è passato il momento della commemorazione obbligatoria voglio ricordare David Bowie come persona attenta alla tecnologia.<!--more-->

Aprì un’attività di [provider Internet nel 1998](http://www.theguardian.com/technology/2016/jan/11/david-bowie-bowienet-isp-internet), BowieNet, dichiarando

>Se tornassi ad avere diciannove anni, bypasserei la musica e andrei dritto su Internet.

Collaborò a un software per la composizione automatica di testi di canzoni, [Verbasizer](http://motherboard.vice.com/en_ca/read/the-verbasizer-was-david-bowies-1995-lyric-writing-mac-app), e [marginalmente a un gioco di avventura](http://arstechnica.com/the-multiverse/2016/01/david-bowie-legendary-artist-dies-at-69/).

È fin troppo risaputo che il suo primo grande successo [è stato suonato alla chitarra](https://www.youtube.com/watch?v=KaOC9danxNo) da un astronauta sulla Stazione Spaziale Internazionale e molti lo hanno ricordato di impulso proprio attraverso quella canzone.

Io sceglierei [Sense of Doubt](https://www.youtube.com/watch?v=f43DiHUH-6A).

<iframe width="420" height="315" src="https://www.youtube.com/embed/f43DiHUH-6A" frameborder="0" allowfullscreen></iframe>