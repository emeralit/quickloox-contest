---
title: "Realtà incrudelita"
date: 2022-01-06T01:02:02+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Si ricomincia a dibattere di chiusure, aperture, didattica a distanza, baluardi da difendere, simboli, funzioni sociali, figure professionali a rischio, come se in questi due anni la scuola fosse cambiata zero di fronte alla sfida del virus.

Notizia shock: in questi due anni, è cambiata zero. Così come detrattori e tifosi, una compagnia di giro mortificante.

Su questo si innesta naturalmente la crociata contro il digitale. La scuola delle mie figlie, per altri versi eccellente, *ha una riflessione in corso* ma intanto non permette di sostituire i libri di scuola con un tablet contenente tutti i libri di scuola.

Non si sta parlando di lezioni fantasiose o tecnologie aliene; dei libri di testo digitali.

Ma tant’è, la piccola società di Ron Avitzur, l’autore della Calcolatrice Grafica (torneranno anche i vecchi post), ha una pagina non indicata sulla home, dove si parla del lavoro che sta compiendo [sui libri di testo con la realtà aumentata](http://pacifict.com/AR/).

imposti una funzione su un apparecchio e la vedi davanti a te, appoggiata sul banco, sul libro, per terra. Il poligono, la curva, la superficie sono manipolabili a gesti. Se una parte della curve non è chiara le si può girare intorno, capovolgerla, ingrandirla.

Insomma, la rovina della nostra gioventù, che potrebbe imparare matematica e geometria con mezzi sconosciuti alle generazioni precedenti e invece potrà consolarsi dell’odore della carta.

La realtà aumentata, chiedo scusa per il truismo, è una realtà. La realtà in quanto tale è una realtà incrudelita: mentre noi ci balocchiamo con il campionario dei trucchi per fare rimanere la scuola nel novecento, il duemila è già oltre un quinto del percorso.

Rimarremo indietro, ancora più indietro, e quelli che propugnano l’arretratezza per approfittarne saranno a godersi le loro pensioni immeritate.

Pensarci, che fra un po’ si vota, che i nostri figli prima o poi saranno diventati genitori a loro volta. Almeno cerchiamo di costruire per i nipoti.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*
