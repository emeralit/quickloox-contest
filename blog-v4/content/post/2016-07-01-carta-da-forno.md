---
title: "Carta da forno"
date: 2016-07-01
comments: true
tags: [RocketBook, Moleskine, Stefano, OwnCloud, SpiderOak, iPad, Pro, Apple, Pencil, microonde]
---
Una persona che lavora, come **Stefano** diceva pochi giorni ha [ho abbandonato la mia Moleskine](https://macintelligence.org/posts/2016-06-26-pro-come-una-matita/), una volta provato [iPad Pro](http://www.apple.com/it/ipad-pro/) con [Apple Pencil](http://www.apple.com/it/apple-pencil/).

Forse Stefano ignora l’esistenza di *gadget* come [Rocketbook Wave](http://getrocketbook.com). Un quadernetto ad anelli con le pagine puntinate invece che quadrettate (wow! Progresso! Startup!). La carta è normalissima ma bisogna usare per forza la penna che forniscono loro, per ragioni che si spiegheranno tra poco.

Una volta presi gli appunti, *si fotografano le pagine* con lo smartphone. Giusto per ridurre la dipendenza dall’informatica. La *app* ancillare manda le pagine fotografate su un cloud a pseudoscelta (solo quelli che hanno pensato loro, per esempio niente [SpiderOak](https://spideroak.com) o [OwnCloud](https://owncloud.org)). Sulle pagine del quadernetto c’è un codice QR che le tiene numerate automaticamente e piccole icone che, nella app, indirizzano la pagina verso sette diverse raccolte tematiche.

In altre parole, se prendo un notes di carta, fotografo con iPhone le pagine e le mando a mano in sette cartelle diverse che mi sono preparato su Dropbox o iCloud Drive, ho replicato il novanta percento delle funzioni di Rocketbook Wave. Che chiede ventinove dollari per il dieci percento aggiuntivo.

In cui si trova la funzione fondamentale. Diversamente dal mio notes trovato nel vecchio cassetto, le pagine di Rocketbook Wave sono riciclabili. La penna speciale contiene un inchiostro speciale che sbianca con il calore. In questo modo è possibile riprendere appunti sulle pagine già usate, dopo *avere passato il quadernetto nel forno a microonde*. E il metodo funziona anche cinque o dieci volte.

Un iPad Pro con Apple Pencil costa tipo venticinque volte un Rocketbook Wave, ma io lo sceglierei venticinque volte su venticinque. E poi mi regalerei un Wave per parlarne alla pizza con gli amici, vantarmi dell’ultimo *gadget* del momento, fare finta di essere un *early adopter* sempre sulla cresta dell’onda tecnologica, fare ancora più fesso un potenziale cliente già fesso di suo.

Se pretendo che sia un oggetto veramente utile per il lavoro, sono un cretino integrale.

E questo probabilmente è il problema di Stefano: lui lavora veramente e non ha tempo per le cretinate.

*[Una riga per farsi beffe di Rocketbook non ci sta, nel libro tradizionale su macOS. In un [prodotto nuovo e diverso](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/) magari sì. Previa [Approvazione concreta](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac), sotto forma di passaparola e adesioni concrete, da parte dei lettori.]*