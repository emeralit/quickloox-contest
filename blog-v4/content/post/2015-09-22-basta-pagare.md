---
title: "Basta pagare"
date: 2015-09-22
comments: true
tags: [Nfl, Surface, Microsoft, iPad]
---
Fa tenerezza la *naïveté*, il candore che ha portato il *marketing* Microsoft a pagare quattrocento milioni di dollari la [National Football League](http://www.nfl.com) americana per promuovere [Surface](http://www.microsoft.com/surface/en-us). Microsoft ha fornito tavolette alle squadre, in cambio di inquadrature televisive ravvicinate in occasione dei *timeout*. Quello che si chiama *product placement* al cinema, pubblicità occulta.<!--more-->

Il granello di sabbia in questo perfetto meccanismo è che il contratto data al 2013 e ancora oggi [presentatori, allenatori e giocatori li chiamano](http://hothardware.com/news/nfl-fumbles-microsofts-400-million-surface-ad-deal-again-still-gets-called-an-ipad) *iPad*, *copie di iPad*, *tavolette stile iPad* e così via. Ascoltati da milioni di spettatori in diretta TV.

In Microsoft pensano che basti pagare e si possa comprare ogni cosa. Se un prodotto ha redefinito un mercato, invece, comprare le menti e le abitudini è più difficile.

Ci hanno pensato e hanno partorito una soluzione:

>Abbiamo addestrato alcuni dei protagonisti e continueremo a farlo, per essere sicuri che i nostri partner siano bene equipaggiati per parlare di Surface quando la videocamera riprende i giocatori che lo usano nel corso delle partite.

Giustamente. Li hanno comprati, adesso li addestrano. Alla fine probabilmente saranno capaci di apparire convinti. Come se i Surface li avessero scelti. Non so quanto funzionerà, con personaggi bravi nello sport e meno nella recitazione.

Io direi: basta pagare. Producano piuttosto qualcosa che gli utilizzatori, dentro e fuori la Nfl, siano soddisfatti di scegliere e adoperare. Come accade, tu guarda, con iPad.