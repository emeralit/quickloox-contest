---
title: "Dissonanze cognitive"
date: 2020-09-02
comments: true
tags: [ iPhone, App, Store]
---
iPhone costa troppo, è esagerato, è un lusso inutile replicabile alla metà del costo.

iPhone 11 è [il computer da tasca più venduto nel primo semestre del 2020](https://macdailynews.com/2020/09/01/apples-iphone-11-is-the-worlds-best-selling-smartphone-this-year-by-far/).

Secondo, terzo e quarto in classifica, messi insieme, neanche lo sfiorano. Per superarlo bisogna aggiungere anche il quinto, che peraltro è iPhone SE.

I dati del 2019 erano lusinghieri ma molto meno. Questa è superiorità schiacciante.

Oppure.

L’insostenibile Apple Tax del trenta percento, gli sviluppatori infuriati, il margine eccessivo.

Tutto discutibile, eh. Intanto App Store [ha creato in America trecentomila posti di lavoro da aprile 2019](https://macdailynews.com/2020/09/02/apples-app-store-created-300000-jobs-since-april-2019/).

Potevano essere di più se Apple si prendesse il dieci percento che il trenta? Possibile, se ci fosse una controprova. Quale altro store ha fatto meglio, su un lato o sull’altro? Personalmente, dopo la pandemia, sono stupito che ci sia un saldo positivo, prima ancora di vedere se è grande o piccolo.

C’è troppa gente che non vuole credere alla realtà delle cose e, per conciliare la realtà in questione con le proprie convinzioni, si fabbrica storie *ad hoc*.

Dissonanze cognitive. La prima fu quella della volpe, che vedeva l’uva troppo in alto e la dichiarò acerba.
