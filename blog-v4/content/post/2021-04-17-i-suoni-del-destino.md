---
title: "Come suona il destino"
date: 2021-04-17T01:28:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Mac, 512 Pixels] 
---
Abbiamo sentito tutti infinite volte il suono di un Mac che si accende e pochissime o neanche, per fortuna, quello di un Mac che annuncia un problema serio.

*512 Pixels* ne ha raccolti [una serie](https://512pixels.net/2021/04/mac-chimes-of-death/) e ritengo che nessuno sul pianeta sia riuscito a sentirli tutti nelle circostanze previste (addetti alle riparazioni, programmatori ficcanaso, retroappassionati con emulatore: non vale).

Che esistesse anche il suono di un’auto in collisione, per esempio, mai lo avrei detto.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*