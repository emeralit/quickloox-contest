---
title: "Un Paese, due sistemi"
date: 2020-08-14
comments: true
tags: [Venerandi, didattica, Google, Earth, GarageBand, Immagini]
---
Fabrizio Venerandi è stato come al solito definitivo sullo studio dell’università di Milano Bicocca riguardante le opinioni delle mamme lavoratrici del nord-ovest sul fatto che i figli siano stati sottoposti a lezioni online di emergenza invece di venire forse più utilmente abbandonati a se stessi fino a fine lockdown.

Se ne è [parlato ieri]([https://macintelligence.org](https://macintelligence.org/blog/2020/08/13/desertica-a-distanza/): le mamme non sono state contente perché hanno faticato a lavorare. Come spiega Venerandi, quello che hanno imparato o non imparato i ragazzi, quello che hanno imparato o non imparato i docenti, [sembra avere meno rilevanza](http://www.quintadicopertina.com/fabriziovenerandi/?p=1653).

A me fa uscire dai gangheri questa idea della scuola intesa primariamente come parcheggio dei figli per i genitori che lavorano. A scuola si istruiscono, si educano e si formano i ragazzi. Intanto, certo, i genitori lavorano, ma  è un effetto collaterale. In caso di emergenza è ai ragazzi che devo guardare. Anche perché, se devo scegliere un parcheggio, voglio un _camp_ di basket, un trekking degli scout, la piscina, lezioni di musica, un fablab. C’è una ragione se, invece che fare sport e stare all’aria aperta per ore, i ragazzi si sacrificano su italiano o matematica: non è il lavoro dei genitori, per agevolare il quale andrebbe benissimo anche la scuola circense.

Ma il punto non era questo. Oggi la primogenita ha tirato fuori un peluche che ricordava un pinguino particolarmente colorato. Mi ha chiesto _che pinguino è questo?_

Non so nulla di pinguini, ma so che esiste un browser di nome Puffin, con una icona che mi ricordava qualcosa, e ho detto _magari è un puffin. Vediamo su Internet_.

iPad, Google Immagini, _puffin_. Foto.

_Ma papà, quello è il pulcinella di mare!_

iPad, Google Immagini, _pulcinella di mare_. Foto.

Mia figlia sa cose sui pinguini che io neppure immagino e deve ancora andarci, a scuola. Io di cose sui pinguini ne ho appena imparata una.

Lidia tocca una delle immagini e si apre un articolo, sui puffin che stanno tornando a nidificare in Scozia.

_Ma papà, quanto è lontana la Scozia?_

iPad, Google Earth, geolocalizzazione. _Figlia, tocca la lente e scrivi_ Scotland.

_Fallo tu_. (Non ama l’inglese e ha paura di sbagliare).

_Facciamo una lettera per uno_. S-c-o-t-l-a-n-d, Invio e volo virtuale dal cancello di casa alle coste scozzesi. La Scozia è lontana ma non lontanissima, un giorno andremo a vedere i puffin. Ti ricordi che il papà è stato a Londra? Ecco, qui è Londra (volo virtuale). La Scozia è un po’ più lontana di Londra.

Poi si sono fatte altre cose ma, prima di uscire, con lo stesso iPad, abbiamo imparato a suonare le prime note di _Tanti auguri a te…_ dal momento che manca poco al suo compleanno. Su GarageBand, sullo stesso iPad, per tentativi perché qui, all’apice delle nostre capacità, si strimpella. Poi le note le abbiamo sentite al pianoforte, alla chitarra elettrica, alle maracas.

Venerandi:

> la didattica digitale dovrebbe essere un normale strumento quotidiano per chi vada a scuola nel 2020.

Perché, scrive giustamente, la _didattica a distanza_ non esiste, mentre esiste la _didattica digitale_.

> Che accanto alla scuola analogica, sia necessario creare delle unità trasversali di studenti che accedono a contenuti, videogiochi, esercizi, fonti, videolezioni, strumenti on-line, preparati dai docenti della scuola, calibrati sul singolo studente, che non sostituiscono la lezione in presenza e nemmeno la affiancano, ma costituiscono un percorso parallelo di apprendimento che lo studente possa seguire quando e come vuole.

Ho proposto a Venerandi di scrivere un libro a quattro mani sulla didattica digitale. Nel frattempo prego perché Lidia trovi una maestra brava nel catturare l’attenzione e capace di fare quello che deve sulla lavagna, con i gessetti, con il libro di testo. _Ma anche con il digitale_.

Perché il nome inglese del pulcinella di mare lo scopro con il digitale, e come sono fatte le coste della Scozia, e come suono gli auguri di compleanno con lo strumento che mi viene in mente. Questo è il 2020 e questa (anche, non solo) deve essere la scuola.

Si preferisce che mia figlia vada in aula? Per carità, così posso lavorare. Basta che accanto all’analogico trovi il digitale e viceversa.