---
title: "Retromomenti"
date: 2013-12-13
comments: true
tags: [Amiga, Mac, Plus]
---
Uno software: Google ha mostrato come un *browser* di oggi possa tranquillamente [emulare un Amiga 500](http://pnacl-amiga-emulator.appspot.com).<!--more-->

Serve [Chrome](https://www.google.com/intl/it/chrome/browser/) per questioni di architettura, non di prestazioni. Nel 1985 era un computer potentissimo perché aveva il *multitasking* incorporato nell’hardware, così come la sintesi vocale; in molti studi televisivi si rischia ancora oggi di trovare un Amiga 500 in uso per creare sovrimpressioni, perché trent’anni fa lo faceva all’avanguardia a un prezzo imbattibile. Adesso sta in una finestrella tra le cento sul mio schermo.

Se Amiga naufragò fu a causa di Commodore, tutta concentrata sull’hardware per dimenticarsi del software, di rara approssimazione se paragonato a Mac o anche agli Atari dell’epoca. Altro discorso, comunque.

E uno hardware: che cosa si deve provare a portare su Internet un Mac Plus del 1986? La faccenda è meno facile di quello che si possa pensare e [l’elenco degli ostacoli superati](http://www.dailydot.com/opinion/mac-plus-introduce-modern-web/) è degno di attenzione.

Il fascino, ventisette anni dopo, rimane.

<iframe width="560" height="315" src="//www.youtube.com/embed/5UBRUyofiiU" frameborder="0" allowfullscreen></iframe>