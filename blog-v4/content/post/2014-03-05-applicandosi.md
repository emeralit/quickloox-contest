---
title: "Applicandosi"
date: 2014-03-05
comments: true
tags: [TextEdit, Anteprima, Rtf, BBEdit]
---
Sempre alle prese con il salvataggio dello stato di lettura di Anteprima, sul mio Mac ancora più erratico di [quello che credevo](https://macintelligence.org/posts/2014-02-28-due-contro-uno/), ho però ricevuto una soddisfazione compilando un noioso modulo che mi hanno spedito in formato Pdf, di quelli con tre pagine di dati sempre uguali da ripetere nell’intento di complicare la vita. Inserito il testo nei campi della prima pagina e copiatolo, Anteprima lo ha incollato al posto giusto nelle pagine successive.<!--more-->

Stimo da sempre molto TextEdit ma ho dovuto constatare un problema autentico: cancellare o inserire righe (che non siano l’ultima) di una tabella dentro un documento in formato Rtf. Non c’è un comando e a quanto sembra non ci sono soluzioni. A volte posizionare il cursore sulla riga sottostante e dare Opzione-Delete funziona; più spesso, tuttavia, la struttura della tabella va a carte quarantotto.

Ho provato ad aprire il documento con BBEdit per portarne alla luce il codice. Passato il primo istante di smarrimento, le cose sono più semplici di quello che sembrano. Questo è il codice Rtf di una riga di una tabella di un documento qualunque, composta da tre caselle nelle quali ho inserito il nome di altrettante unità di misura:

`\itap1\trowd \taflags1 \trgaph108\trleft-108 \trbrdrl\brdrnil \trbrdrr\brdrnil`
`\clvertalc \clshdrawnil \clheight20 \clbrdrt\brdrs\brdrw20\brdrcf2 \clbrdrl\brdrs\brdrw20\brdrcf2 \clbrdrb\brdrs\brdrw20\brdrcf2 \clbrdrr\brdrs\brdrw20\brdrcf2 \clpadl100 \clpadr100 \gaph\cellx2880`
`\clvertalc \clshdrawnil \clheight20 \clbrdrt\brdrs\brdrw20\brdrcf2 \clbrdrl\brdrs\brdrw20\brdrcf2 \clbrdrb\brdrs\brdrw20\brdrcf2 \clbrdrr\brdrs\brdrw20\brdrcf2 \clpadl100 \clpadr100 \gaph\cellx5760`
`\clvertalc \clshdrawnil \clheight20 \clbrdrt\brdrs\brdrw20\brdrcf2 \clbrdrl\brdrs\brdrw20\brdrcf2 \clbrdrb\brdrs\brdrw20\brdrcf2 \clbrdrr\brdrs\brdrw20\brdrcf2 \clpadl100 \clpadr100 \gaph\cellx8640`
`\pard\intbl\itap1\tx566\tx1133\tx1700\tx2267\tx2834\tx3401\tx3968\tx4535\tx5102\tx5669\tx6236\tx6803\pardirnatural`
`\cf0 sievert\cell `
`\pard\intbl\itap1\tx566\tx1133\tx1700\tx2267\tx2834\tx3401\tx3968\tx4535\tx5102\tx5669\tx6236\tx6803\pardirnatural`
`\cf0 ohm\cell `
`\pard\intbl\itap1\tx566\tx1133\tx1700\tx2267\tx2834\tx3401\tx3968\tx4535\tx5102\tx5669\tx6236\tx6803\pardirnatural`
`\cf0 coulomb\cell \row`

Anche senza [studiare](http://msdn.microsoft.com/en-us/library/office/aa140284%28v=office.10%29.aspx), qualcosa si capisce. Si intuiscono le abbreviazioni di *color* e *border*. Il testo delle caselle è sempre preceduto da `\cf0` più uno spazio e così via. Cancellare da BBEdit o altro editor di testo puro l’intero blocco mostrato sopra cancella efficacemente la riga. Inserire una nuova riga è appena più complesso, perché si tratta di copiare e incollare un blocco esistente nella posizione desiderata.

Sarebbe bello riuscire ad automatizzare queste operazioni e magari creare un servizio di OS X che supplisce alla mancanza dei comandi su TextEdit. Più bello ancora: scaricare il [codice del programma](https://developer.apple.com/library/Mac/samplecode/TextEdit/Introduction/Intro.html) ed emendarlo in XCode. Manca il tempo per applicarsi, certo non gli spunti.
