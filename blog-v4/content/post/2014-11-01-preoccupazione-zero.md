---
title: "Preoccupazione zero"
date: 2014-11-01
comments: true
tags: [Mac, iPad, iPhone, Keynote, AppleTv, iCloud, Dropbox, GoogleDrive]
---
Reduce da una giornata di trasferta a Roma, dove tutto – il cosmo a volte procede per vie misteriose – ha funzionato esattamente come nelle aspettative.<!--more-->

Gli ultimi ritocchi applicati sul Frecciarossa alla presentazione Keynote si sono propagati a iPhone e Mac non appena entrati nel Wi-Fi della sala riunioni.

Il sistema di amministrazione di contenuti è *responsive* dalla nascita, per cui funziona su qualsiasi apparecchio. Ho potuto scambiare gli apparecchi ogni volta che faceva comodo e il videoproiettore li ha sempre riconosciuti all’istante, neanche ci fosse stato un banco regia.

Perfino la batteria di Mac è stata all’altezza della situazione e l’unico cavo necessario è stato quello per il videoproiettore, aspettando il giorno in cui tutti avranno una Apple TV. Il computer si è arreso verso le quattro del pomeriggio, quando la parte ufficiale era terminata da tempo. È stato naturale commutare al lavoro su iPad, che ovviamente non ha battuto ciglio, ma si sapeva.

Non ho batterie diverse da quelle che hanno tutti; semplicemente ho meccaniche di lavoro che consentono di arrivare bene a fine giornata senza troppi problemi. Non è genialità, è fortuna; nondimeno, è una preoccupazione che sparisce. Altra preoccupazione che se ne va è la sincronizzazione del lavoro tra apparecchi. Questo aspetto di iCloud (Dropbox, Google Drive) ha il profumo della magia.

L’informatica personale inizia finalmente a mantenere una promessa fatta decenni fa e rimasta incompiuta: lasciarci concentrare sul lavoro senza preoccupazioni supplementari.

O forse ho goduto dei giusti trigoni e di congiunzioni astrali favorevoli. Mentre scrivo, funziona persino il Wi-Fi del Frecciarossa. Roba da preoccuparsi della stabilità dell’asse terrestre.