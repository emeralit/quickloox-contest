---
title: "L’intelligenza è discreta"
date: 2023-06-15T23:51:05+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [WWDC, ai, intelligenza artificiale, machine learning]
---
Se ti accorgi di avere troppo spesso per la bocca il lemma *intelligenza artificiale* usato per programmi specializzati (in modo incredibile e utilissimo) nell’assemblare testo una parola dietro l’altra, senza avere la minima nozione del contenuto, suggerisco una terapia.

Riguardare il [*keynote* di presentazione di WWDC](https://developer.apple.com/videos/play/wwdc2023/101/)

Contare quante volte viene detto *artificial intelligence* o *ai*.

Contare quante volte viene detto *machine learning*.

Tirare le conclusioni, consapevoli che quanto viene chiamato intelligenza artificiale oggi dai più è solo una applicazione particolarmente raffinata e complessa di una azione precedente di machine learning.

*La carità si compiace della verità, non si vanta, non si gonfia*, dice il Nuovo testamento.

Umilmente parafraso: l’intelligenza è discreta. L’intelligenza artificiale ancora la aspettiamo ma, quando arriverà, dovrà essere elegante e leggera.