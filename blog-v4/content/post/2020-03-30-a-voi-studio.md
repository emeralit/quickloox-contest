---
title: "A voi studio"
date: 2020-03-30
comments: true
tags: [Roger, NBC, iPhone, iPad]
---
Una banale verità di cui faremmo meglio ad accorgerci in questi giorni di videoconferenza obbligata. Al Roker, meteorologo statunitense, [ha mostrato il proprio *setup*](https://twitter.com/alroker/status/1243585277219811329) nel momento in cui causa coronavirus trasmette dal giardino di casa.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Yes. Made it to Fri-yay. Here’s my backyard setup for <a href="https://twitter.com/TODAYshow?ref_src=twsrc%5Etfw">@todayshow</a> and @3rdhourhourToday iPhone is Live U, other iPhone is return. iPad is prompter. And a LED light panel and an iRig/Sennheiser mic combo <a href="https://t.co/DomHW57KTf">pic.twitter.com/DomHW57KTf</a></p>&mdash; Al Roker (@alroker) <a href="https://twitter.com/alroker/status/1243585277219811329?ref_src=twsrc%5Etfw">March 27, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Due iPhone, un iPad, illuminazione Led, postazione microfonica.

Colpisce da una parte la sobrietà dell’apparato; riflettore Led a parte, sono requisiti che migliaia di famiglie normalissime potrebbero soddisfare senza preavviso.

Colpisce dall’altra la cura richiesta a chi voglia stabilire un contatto video realmente efficace. Dedicato a chi pensa di fare *smart working* perché gli si è accesa la videocamera in salotto.

Certo non è necessario che chiunque si attrezzi uno studio di produzione in casa. D’altro canto, qualcuno inizierà prima o poi a fare attenzione al modo di presentarsi delle persone in video, e filtrare via quello molto prima che stare a guardare la cravatta che è roba del tempo prima delle videoconferenze obbligatorie per la sopravvivenza.
