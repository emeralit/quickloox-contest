---
title: "Dischi rotti"
date: 2019-06-07
comments: true
tags: [Apple, WWDC, Swift, Mac, Catalina, Catalyst, iPad]
---
Continuo a leggere reazioni al [*keynote* di inizio WWDC](https://developer.apple.com/wwdc19/), comprese quelle al mio [piccolo riassunto](https://macintelligence.org/posts/2019-06-06-sogni-e-realta/), meravigliato di come anche persone di intelligenza straordinaria prendano posizione in modo automatico quando si tratta di annunci Apple. C’è un lato positivo: quando tutti hanno una propria posizione su un tema, automatica o manuale, vuol dire che il tema tocca tutti. I temi che toccano tutti non sono moltissimi e significa che Apple ha un *mind share*, anche in questo caso, superiore alla concorrenza.

Altri lati invece non me li spiego e soprattutto uno. Provo a dettagliare.

Leggo di timori sulla chiusura di [macOS Catalina](https://www.apple.com/macos/catalina-preview/) e che macOS arrivi a diventare un sistema chiuso, disposto a ospitare le sole app provenienti da Mac App Store. È paradossale che ci si pensi proprio quando si invitano gli sviluppatori iPad a portare le loro app su Mac grazie a [Project Catalyst](https://developer.apple.com/ipad-apps-for-mac/); prima conseguenza della WWDC e di Catalina è che il numero si sviluppatori presenti su Mac *aumenterà*. Si potrebbe obiettare che iPad è un sistema chiuso che però non se la cava così male in termini di libertà di movimento e varietà di app disponibili, ma la vera questione è un’altra: sono gli stessi timori che leggevo nel 2012, anno di partenza di Gatekeeper. Sono passati sette anni e tuttora si installa su Mac quel che si vuole. Nutrire timori *in progress* senza un orizzonte temporale è intellettualmente disonesto: in quanto tempo dobbiamo temere che diventi un sistema chiuso? Un anno, cinque, venticinque? Comincia a fare la differenza.

E il prezzo? Stavolta è il prezzo di [Mac Pro](https://www.apple.com/mac-pro/), ancora di più quello dello stand di Pro Display XR. Troppo alto, esagerato, lo faccio fare al mio falegname e coi soldi risparmiati vado in vacanza, *Apple tax*, con il modello X marca Y faccio le stesse cose e costa meno, addio Apple, i professionisti se ne vanno bla bla bla.

È confortante per il clima economico vedere quante persone pubblichino pareri qualificati su apparati che costano molte migliaia di euro per essere goduti, e per il clima informativo che riescano a farlo, evidentemente documentatissimi, mesi prima che i suddetti apparati siano effettivamente in vendita. Il mio criterio è un altro; tendo a fare riferimento a letture autorevoli. Per esempio [Ars Technica](https://arstechnica.com/gadgets/2019/06/our-first-look-photos-of-the-apples-new-mac-pro-and-the-pro-display-xdr/):

>Prima di tutto, Xdr non è affatto uno schermo *consumer*. In realtà compete nella fascia bassa con schermi di qualità usati per esempio dai graphic designer, che costano tra duemila e seimila dollari. Ma compete anche direttamente con schermi di riferimento professionali che costano trentamila, quarantamila o anche cinquantamila dollari.

>E la scelta dello stand a prezzo separato ha senso, nonostante il prezzo. Alcuni utenti vorranno usare lo schermo attaccato a una parete, altri dispongono già di una soluzione, mentre altri ancora vorranno lo stand. […] Pro Display Xdr è così specialistico che nemmeno è un prodotto tipico per una nostra recensione. Ma siamo entusiasti all’idea di passarci sopra più tempo quando sarà effettivamente disponibile.

Anche qui il punto è un altro: è dal 1984, con il primo Mac a duemilaquattrocentonovantanove dollari, che il prezzo è troppo alto e si fanno le stesse cose con un Pc a prezzo inferiore. Gente, è ora di farsene una ragione. È una critica che ha compiuto trentacinque anni e quindi, mi permetto, forse ha qualche punto debole, o Apple si sarebbe estinta come Commodore, Compaq, Atari, IBM (lato computer) eccetera.

Capita anche di leggere che il sottoscritto non sia obiettivo (qualcuno ha scritto *fanboy* e sono ringiovanito di dieci anni, gente che evidentemente è uscita dal congelatore apposta per vedersi il *keynote* e parla da un’altra epoca). Quelli che lo scrivono sono invariabilmente persone con una patente di obiettività autoconferita non si sa bene su quali basi. Non sono obiettivo perché penso in modo diverso da loro e Apple sbaglia perché non fa la macchina che serve a loro al prezzo che va bene per loro. L’informatica gli ruota intorno che neanche la Terra sul proprio asse.

Il *pattern* rimane sempre il solito: posso prendere qualche articolo di un quarto di secolo fa e leggere le stesse identiche critiche.

Per statistica, une delle persone richiamate nei paragrafi precedenti finisce per tirare in ballo la (mancanza di) innovazione. Quando c’era Steve Jobs era un’altra cosa e non è più come una volta, si stava meglio quando si stava peggio. Dischi rotti.

Amici: se nessun altro innova, fatelo voi. Siete capaci di muovere una critica che non suoni uguale a quelle del 1999? Innovare le idee dovrebbe essere più semplice che lo hardware. Su, qualcosa di nuovo, grazie.
