---
title: "Era meglio il peggio"
date: 2022-08-28T01:45:41+01:00
draft: false
toc: false
comments: true
categories: [Internet, Software]
tags: [iPhone, Sir Apfelot, Soncini, Guia Soncini, Linkiesta, Csam, Apple, Google, Android]
---
Prima dell’argomento principe di oggi, un aggiornamento sul premio candore 2022 che verrà assegnato senza opposizione alcuna a *Sir Apfelot*, con la sua [predizione del prossimo evento Apple](https://www.sir-apfelot.de/en/apple-september-keynote-was-koennen-wir-kommenden-monat-erwarten-44034/):

>C’è una possibilità che la data cadrà il 13 settembre.

Stabilito che [Far Out avrà luogo il 7 settembre](https://edition.cnn.com/2022/08/24/tech/apple-event-iphones/index.html), credo che potrei anche spendere del denaro per quanto sono curioso di conoscere quale fosse quella possibilità. Nel 2022 è ancora possibile scrivere cose a caso nel riferire eventi reali e di questo si può solo essere sollevati, un po’ come vedere in certi hotel americani raduni di centinaia di persone vestite da orsacchiotto.

Ma veniamo al tema, [messo agli atti dall’ineguagliabile Guia Soncini di Linkiesta](https://www.linkiesta.it/2022/08/google-facebook-privacy-polizia/): una *agghiacciante storia di una indagine per pedofilia in cui la pedofilia era il meno agghiacciante degli elementi*. Scrive Soncini:

>Il poverocristo al centro dell’articolo aveva, durante la pandemia, fatto delle foto al pisello del figlio, gonfio e arrossato, in modo che il pediatra potesse vederle. Le aveva mandate alla moglie, la quale aveva poi provveduto a caricarle sulla piattaforma utilizzata dal medico per le cure in remoto.

Qui, scrive lei, *arriva la parte che sembra un messaggio promozionale di Apple*:

>La moglie aveva un iPhone, quindi non le era successo niente. Il marito aveva un Android, che gira su Google.

Il punto sarebbe che *Google collabora con gli investigatori per smantellare giri di pedofili controllando le foto di bambini che i suoi utenti inviano e ricevono (Apple no, era intenzionata a farlo ma, dice il NYT, gruppi per la difesa della privacy hanno obiettato)*.

È più complicato di così, ma come sintesi per una testata generalista ci siamo.

Qualcuno ricorderà che Apple aveva proposto un proprio sistema per salvare la capra della privacy dell’utente e i cavoli della lotta alla pedofilia online, per poi rinviarlo *ad kalendas* a seguito di polemiche enormi di cui [scrivevo esattamente un anno fa](https://macintelligence.org/posts/2021-08-28-privacy-e-realtà-un-attacco-omologato-e-superficiale/).

Se il poverocristo al centro dell’articolo avesse usato un iPhone e se Apple fosse andata avanti con la messa a punto del suo sistema di individuazione di contenuti potenzialmente pedofili, *non gli sarebbe accaduto alcunché*. Se proprio vogliamo architettare la peggiore di tutte le combinazioni possibili, al termine di congiunzioni astrali inosabili, gli sarebbe arrivata una comunicazione confidenziale da Apple.

Si noti che il problema qui non è la giustizia: la polizia, compiuta l’indagine, ha capito la situazione. Google no; ha chiuso l’account del malcapitato, che ha perso foto, posta, contatti eccetera. Siccome usava un piano telefonico di Google, ha perso anche il numero di telefono. La polizia ha avuto inizialmente problemi a contattarlo perché non rispondeva alle chiamate.

Quando Apple ha proposto il proprio sistema, tutti a chiedersi che fine avrebbe fatto la privacy delle foto memorizzate nei cloud delle aziende e che implicazioni ci sarebbero state per i dissenzienti negli Stati autoritari o altro, mai implicate o effettivamente realizzabili con l’implementazione di Apple. Tutti a protestare. Tutti a ribellarsi.

La situazione oggi è questa: fotografi tuo figlio per mostrare un problema al pediatra e vieni indagato per pedofilia. Con Android, certo, ma vogliamo dire *per fortuna* o *affari loro*? Avendo due figlie in età da pediatra posso solo solidarizzare, anche con chi ha sbagliato marca di computer da tasca.

I custodi della libertà e della privacy sono tutti zitti, visto che un padre in difficoltà non rende in visibilità come la *lotta alla multinazionale capitalista*. Il sistema di Apple, per quanto perfettibile e criticabile all’infinito, a) non avrebbe mai portato a una situazione come questa e b) era molto, molto meglio di questo peggio.

Complimenti vivissimi.