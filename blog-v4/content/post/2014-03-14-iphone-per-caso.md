---
title: "iPhone per caso"
date: 2014-03-14
comments: true
tags: [iPhone, FaceTime]
---
Un soldato americano in Afghanistan si salva da un attentato suicida perché una biglia destinata probabilmente a colpire un’arteria viene [parata dal suo iPhone](http://www.ksl.com/?sid=29019328&nid=148&title=iphone-helps-save-utah-soldiers-life&fm=home_page&s_cid=featured-1).<!--more-->

(Apple si impunta sulla burocrazia ma alla fine il buonsenso prevale).

Mamma dell’Arizona si infortuna e perde troppo sangue per riuscire a chiamare il numero di emergenza. Viene [salvata dal figlio di due anni](http://edition.cnn.com/video/standard.html?/video/bestoftv/2014/03/09/pkg-facetime-toddler-saves-mom.kgun) che pulisce iPhone dal sangue e chiama un’amica di famiglia usando FaceTime.

Forse si acquista un iPhone per caso. Forse è predestinazione.