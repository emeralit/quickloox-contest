---
title: "Tavoletta rasa"
date: 2016-10-19
comments: true
tags: [Surface, Nfl, Belichick, Patriots, Microsoft]
---
Ci eravamo lasciati con Microsoft che [ha comprato l’utilizzo di tavolette Surface](https://macintelligence.org/posts/2016-01-27-minuti-di-sospensione/) da parte delle squadre Nfl, il campionato professionistico di football americano.<!--more-->

Tra giocatori che li chiamano iPad invece che Surface, malfunzionamenti, interruzioni di servizio e cachinni vari, abbiamo che il *coach* dei New England Patriots, Bill Belichick, [si è lievemente stufato](https://www.youtube.com/watch?v=wyhjUFcXgo0).

(Si noti che a YouTube è stato chiesto di non permettere l’incorporazione in siti esterni del video appena linkato. Che stranezza.)

Durante la [conferenza stampa del dopopartita](http://www.patriots.com/news/2016/10/18/bill-belichick-conference-call-transcript-1018), Belichick ha detto cose interessanti. La domanda era *È stato riportato che domenica ci sono stati problemi con la tecnologia ausiliaria, come le cuffie e le tavolette che usate. Questo influenza il numero delle chiamate di azioni che riuscite a effettuare? E come influisce sulle modifiche che potenzialmente potreste apportare nel corso di una partita?*

(Presa graziosamente alla lontana, vero?)

Stralci della risposta di Belichick:

>Come avete probabilmente notato, con le tavolette ho chiuso. Gli ho dato tutto il tempo che potevo dargli. Sono semplicemente troppo inaffidabili per me. Userò fotografie, come fanno molti altri allenatori, perché semplicemente non c’è abbastanza coerenza nel funzionamento delle tavolette.

>È tutto equipaggiamento della lega e non nostro. Intendo dire, lo usiamo, ma non è come disporne lungo la settimana e risolvere i problemi. L’equipaggiamento ci viene dato il giorno della partita, anzi, alcune ore prima, e lo collaudiamo e a volte funziona, a volte non funziona. Di solito al momento della partita è funzionante, ma non direi sempre.

>Per quanto mi riguarda, è una decisione personale, con le tavolette ho chiuso. Ho cercato di lavorarci ma semplicemente per me non funziona, in quanto non c’è coerenza nella mia esperienza.

Per mandare una email, figuriamoci, nessun problema. In un ambiente critico e sensibile alla reattività come la panchina di una squadra di football americano, ecco che qualcosa non va.

C’è un’ultima cosa da notare: le notizie sull’[adozione di iPad Pro nella Major League Baseball americana](http://www.wsj.com/articles/baseballs-latest-recruit-is-an-ipad-1459310403) sono praticamente inesistenti da marzo. Come se lì le tavolette, come dire, funzionassero.

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Here&#39;s Belichick&#39;s full answer as to why he hates the tablets. Lasted five minutes and 25 seconds. <a href="https://t.co/wcSqebtQRu">pic.twitter.com/wcSqebtQRu</a></p>&mdash; Zack Cox (@ZackCoxNESN) <a href="https://twitter.com/ZackCoxNESN/status/788411998006603776">October 18, 2016</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>