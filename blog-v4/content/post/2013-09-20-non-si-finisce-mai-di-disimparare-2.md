---
title: "Non si finisce mai di disimparare / 2"
date: 2013-09-20
comments: true
tags: [Scuola, Education]
---
Ringrazio **Piergiovanni** per il permesso di pubblicare la sua lettera. E per la sopportazione che lui e tanti altri insegnanti devono tuttora mettere a piene mani per riuscire a fare il loro lavoro.<!--more-->

>Caro Lux, al Liceo Scientifico Alberti di Cagliari, la mia scuola, sono arrivate le Lim. Sono arrivate anche nel liceo di mia moglie. Le nostre sono le Panaboard, modello UB-P800 credo. Quelle della scuola di mia moglie sono le Oliboard modello 78T.
Avevo avuto modo di “studiare” le Lim diversi anni fa. Il Liceo Linguistico in cui lavoravo, nel piccolo paese di San Gavino, le aveva installate già 5 anni fa. Avevo dunque già “masticato” le potenzialità dello strumento. Tu lo sai, io lavoro da anni con le slide, preparo da me il materiale, costruisco i learning object. Faccio così da almeno 15 anni, anche senza Lim. Una Lim per me è come un videoproiettore più potente. Da subito ho capito come sfruttarlo (io non vedo cosa compare sullo schermo): prendo un rimovibile, scarico la presentazione, faccio up e down tra le slide, apro video e web links. Non è che ci voglia poi una laurea al Politecnico, no? Sbagliato.

>A scuola hanno organizzato un incontro di 90 minuti tenuto da un formatore che sembrava un rappresentante. Ci hanno divisi in due gruppi. Un'ora è andata via mostrando il setup della Lim e del proiettore. Il pc ha Windows 8 e ha fatto i capricci, come si conviene a un software di Redmond. Te la faccio breve, per non annoiarti: io non so chi scelga questi formatori, ti dico solo che a me è capitato un formatore poco formato. Il giovanotto – peraltro preparato –  si è dilettato per un’ora a mostrare come si settava lo schermo, con il mouse che si impallava, il proiettore che andava in standby, Windows che perdeva il collegamento, lo screen saver che irrompeva e bloccava tutto. Alla fine il 70% dei colleghi ha capito che l’innovazione era solo una rottura di palle e basta. A quel punto lui che fa? Dice: quando avete problemi chiamate il tecnico! (signor Giuseppe, credo abbia già pronta una domanda di ore eccedenti che al Preside verrà un infarto).

>Nella mezz’oretta seguente il formatore ha fatto vedere il software Elite. Ora, far vedere e spiegare sono due cose diverse. Già c’erano molte resistenze culturali verso le Lim: dopo quella dimostrazione la frase più ricorrente era “facciamo rimettere le altre lavagne” (il partito era diviso tra lavagne bianche e lavagne nere, come nella Firenze medievale) seguita da “ma è un casino”. I colleghi sono anche preoccupati per il registro digitale.

>Il formatore è stato un inetto. Se ci vedessi bene sarei stato capace di fare una lezione migliore. Li ha spaventati e non ha fatto capire che in realtà la Lim è un aiuto e non una complicazione. Durante la riunione dei docenti di Filosofia e Storia le Lim sono state bollate come strumenti infernali. I colleghi non le useranno. E perderanno una grande possibilità.

>Ho chiesto che si organizzi un corso di aggiornamento obbligatorio sulla scuola digitale. Dobbiamo lavorare molto, bisogna cambiare mentalità. Per farti un esempio: lo scorso anno sono partiti i nuovi ordinamenti ma sono rari i manuali che si sono adeguati e nei dipartimenti ti dicono di fare l’intero programma. Come pensi che possiamo cambiare rotta se i timonieri non ci fanno avvicinare al timone?

Solo come appunto, personale: la scuola è sempre di tutti nelle discussioni demagogiche. Quando devono girare gli equipaggiamenti, e i soldi, spesso è di pochissimi. Magari fossero i migliori.