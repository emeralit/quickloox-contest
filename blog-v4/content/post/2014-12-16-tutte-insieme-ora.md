---
title: "Tutte insieme ora"
date: 2014-12-16
comments: true
tags: [Automator, Workflow, OSX, iOS]
---
Sarebbe bello se su iOS si stabilissero meccanismi capaci di concatenare più *app* per costruire flussi di lavoro complessi a partire da mattoni di costruzione semplici, come accade su OS X grazie ad [Automator](http://www.macosxautomation.com/automator/).

Ora è uscito [Workflow: Powerful Automation Made Simple](https://itunes.apple.com/it/app/workflow-powerful-automation/id915249334?l=en&mt=8) che rappresenta una pietra angolare decisamente interessante. Anche solo un paio di anni fa, pensare a qualcosa del genere su iOS era fantascienza.

Costa solo 2,99 euro in edizione lancio e non va perso.