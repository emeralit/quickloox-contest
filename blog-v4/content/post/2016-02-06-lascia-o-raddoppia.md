---
title: "Lascia o raddoppia"
date: 2016-02-06
comments: true
tags: [Apple, azioni]
---
Si sa che gli analisti del settore tecnologico non capiscono Apple e gli investitori in Borsa non capiscono niente, neppure gli analisti. Così succede che regolarmente Apple annunci risultati record e il titolo in risposta cali, o presenti prodotti eccellenti e succeda la stessa cosa. L’andamento azionario compare spesso nelle chiacchiere da bar e naturalmente costituisce uno degli elementi per insinuare il dubbio, o esprimere la certezza, che l’azienda sia condannata.<!--more-->

Ora scopro una cosa interessante. Il titolo Apple [raddoppia il proprio valore mediamente ogni ventidue-ventitré mesi](http://fortune.com/2016/02/04/apple-share-price-double/). Compresi i cali, comprese le discese post-annunci, tutto compreso.

Chiaro che si tratta di un rilievo storico privo di collegamento con l’andamento futuro del titolo: non c’è alcuna garanzia che questo valore temporale resti, si allunghi, si accorci, niente.

Io però la mia azione Apple me la tengo stretta. Punto al raddoppio.