---
title: "Dalla ba alla zeta"
date: 2019-06-09
comments: true
tags: [macOS, Catalina, bash, zsh, GPL, Apple, Terminale, Mac]
---
Un esempio dei cambiamenti nel prossimo macOS controversi per zero motivi? L’adozione di [zsh come shell preimpostata nel Terminale](https://support.apple.com/en-ca/HT208050) al posto di bash.

La modifica è praticamente di nessun conto: vale solo da [Catalina](https://www.apple.com/macos/catalina-preview/ ) in poi (e chi arriva da un sistema esistente manterrà bash); commutare la scelta su bash o altro è questione di un comando, come è sempre stato; gli script di shell scritti con la buona abitudine dello *shebang* (la sigla `#!` che precede l’indicazione del linguaggio da usare) continuano a funzionare; la compatibilità di [zsh](https://www.zsh.org/) con [bash](https://www.gnu.org/software/bash/) è relativamente buona e con [sh](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/sh.html), in uso spesso negli automatismi presenti di serie in macOS, molto buona, grazie a una modalità di emulazione. Rumore per nulla o quasi nulla.

Chi manifesta preoccupazione forse ignora che la sua shell bash su Mac, così importante, [è aggiornata a dieci anni fa](https://thenextweb.com/dd/2019/06/04/why-does-macos-catalina-use-zsh-instead-of-bash-licensing/). Chi lavora con una versione di avanguardia di bash ha già provveduto di persona e dunque, volendo, provvederà ugualmente di persona a conservare bash come preimpostazione. Tenendo presente che parliamo di Terminale, la procedura – descritta nella nota Apple linkata a inizio pagina – è banale.

C’è sempre il problema di dove Apple vada a parare con questi cambiamenti; la ragione [è politica più che tecnica](https://www.theverge.com/2019/6/4/18651872/apple-macos-catalina-zsh-bash-shell-replacement-features) e riguarda la licenza di utilizzo di bash versione aggiornata, da cui Apple vuole stare lontana perché inficia certi aspetti della sua piattaforma, come l’obbligo di usare software firmato digitalmente.

Quindi Apple manovra nell’oscurità per impedirci di fare quello che vogliamo sulle nostre macchine, pagate per giunta a caro prezzo? Si va verso un Mac chiuso che soffoca l’innovazione e ci imprigiona nel giardino dorato?

Non lo si può escludere ma, [come ho già scritto](https://macintelligence.org/posts/2019-06-07-dischi-rotti/), anche le tempistiche contano. In sostanza Apple evita software con licenza [Gpl terza versione](https://it.m.wikipedia.org/wiki/GNU_General_Public_License). Ma ha cominciato a sganciarsi dal suddetto software [oltre dieci anni fa](http://meta.ath0.com/2012/02/05/apples-great-gpl-purge/).

La grande maggioranza degli ansiosi sull’apertura di Mac manco lo sapeva. A tutti, anche a chi lo sapeva, chiedo quanto in dieci o quindici anni sia cambiata la loro libertà di installare il software che vogliono. La risposta è che oggi tocca fare un clic dentro una Preferenza di sistema. Non mi sembra un regime così oppressivo, magari tenendo anche conto del fatto che prima Mac non aveva processore Intel e la libertà di cui sopra era ben più circoscritta di oggi, nei numeri.

Nel 2030 Mac sarà un sistema chiuso? Sì, no, forse. A me sembra una domanda oziosa, visto che nel 2007 lavoravo senza iPhone, senza iPad e senza Apple Watch, giusto per fare notare che le cose in un lasso di tempo così lungo cambiano un tantino rispetto alle nostre infallibili previsioni. Andasse così male, passerò a [FreeBSD](http://freebsd.org) e ritroverò pure bash, o zsh.
