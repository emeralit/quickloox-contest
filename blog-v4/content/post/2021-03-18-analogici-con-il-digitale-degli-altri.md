---
title: "Analogici con il digitale degli altri"
date: 2021-03-18T01:45:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Dad, Covid, scuola, coronavirus] 
---
Si è costituita pochi giorni fa una [Rete Nazionale Scuola in Presenza](https://www.facebook.com/groups/retescuolainpresenza/about) la quale, come dice il nome, si batte per la riapertura delle scuole… errore: l’iniziativa, per come è scritto nella pagina del gruppo Facebook, *serve a raccogliere e diffondere le iniziative contro la DAD a livello nazionale*.

C’è una sottile differenza e significativa, nell’ambito di una tendenza che durante quest’anno è stata segnalata più volte: il vero centro di interesse non è affatto la riapertura della scuola, ma la lotta contro l’uso del digitale, di cui chiaramente le lezioni remote sono la manifestazione più evidente. Nonché l’unico modo di fare scuola che per mesi abbiamo avuto a disposizione e senza il quale ci sarebbe stato il vuoto assoluto.

Sarebbe facile fare ironia sul fatto che la rete contro la didattica a distanza si nutre di Facebook e web; invece la questione è più seria perché si tratta di persone disposte alla manipolazione e alla distorsione dei fatti pur di prevalere.

La Rete si compone di più gruppi in giro per l’Italia, compreso *A scuola!* che gravita su Milano e lancia [una manifestazione per domenica 21 marzo in piazza Duomo](https://www.a-scuola.it/iniziative/rete-nazionale-scuola-in-presenza-milano-21-marzo-h-16-00/), dove esibire bandiere bianche *perché la scuola non ha colore*, maschere integrali bianche *a testimoniare l’alienazione dei nostri figli*, cappelli a cono d’asino *per denunciare la dispersione scolastica*, campanelle da suonare, zaini *da disporre a scacchiera*, disegni dei ragazzi da esporre sugli zaini e… *ragazzi seduti vicino al loro zaino*, elencati nella lista al pari delle bandiere e delle campanelle.

Che tuo figlio sia uno strumento di protesta da esibire in piazza è discutibile e l’idea di affollare una piazza in una giornata in cui moriranno tipo quattrocento persone per via di una pandemia molto contagiosa mi sembra poco qualificante; tuttavia il confronto delle idee è sacro e guai a chi volesse censurare un’idea, per quanto reprensibile.

Però non puoi barare al gioco. False informazioni, pseudoverità, mezze bugie non sono ammesse. E qui cascano alcuni asini, anche senza cappello a cono.

Leggo la pagina e, nonostante un link interno non funzionante (forse l’Html non è materia degna di apprendimento nel 2021?), vedo che si appropriano dell’[articolo 34 della Costituzione](https://www.senato.it/1025?sezione=121&articolo_numero_articolo=34#:~:text=La%20scuola%20è%20aperta%20a,gradi%20più%20alti%20degli%20studi). Avrebbero ragione loro perché, citano, *la scuola è aperta a tutti*. Ecco, il senso di quella frase è un altro. Non so, di notte le scuole sono chiuse, o la domenica. Non ci si riferisce all’apertura *fisica*.

Rigettano *l’uso prolungato e indiscriminato della Didattica a Distanza*. Anch’io, e chi non lo farebbe? Ma posso sapere che cosa vuole dire *prolungato e indiscriminato*? La scuola di mia figlia quest’anno ha fatto i salti mortali per aprire in sicurezza e per restare aperta. Un venerdì pomeriggio sono uscite da scuola tre classi su diciannove; le altre sedici erano in quarantena per contagi di bambini, genitori, parenti. Nessuno voleva chiudere. Ora hanno chiuso, mia figlia fa lezione in remoto e le insegnanti stanno facendo un lavoro spettacolare. Chiaro che in altri posti andrà magari meno bene, per mille ragioni, o ancora meglio, per mille altre. Dove sta però il prolungamento, dove sta l’indiscriminazione? Vedo della farneticazione, piuttosto.

Dicono che la *tutela della salute psicofisica [è] gravemente minacciata in bambini e adolescenti*. Vero. E poi:

>La prolungata mancanza di socialità e di una sana relazionalità didattica sta determinando tra i giovanissimi un allarmante aumento dei casi di tentato suicidio e di autolesionismo, mentre la scarsa attività fisica e il dilatarsi del tempo trascorso davanti a tablet e PC inducono l’aumento dei casi di pubertà precoce.

Nel primo caso si fa riferimento a un [articolo di Huffington Post](https://www.huffingtonpost.it/entry/i-giovanissimi-si-tagliano-e-tentano-il-suicidio-mai-cosi-tanti-ricoveri-prima-della-pandemia_it_6006f714c5b697df1a09146e) su cui ci sarebbe da scrivere un post dedicato. Il tema è delicato anche se riguarda numeri piccoli rispetto alla popolazione scolastica, ma anche un solo ragazzo in difficoltà merita rispetto. Solo un commento a margine: le scuole sono definite *gli spazi in cui si possono infrangere le regole sotto lo stretto controllo dell’adulto*. Insomma.

La manipolazione più grave è quella del secondo caso. *La scarsa attività fisica e il dilatarsi del tempo trascorso davanti a tablet e PC inducono l’aumento dei casi di pubertà precoce*.

Sarebbe una questione esplosiva. Disgraziatamente, lo [studio scientifico](https://ijponline.biomedcentral.com/articles/10.1186/s13052-021-01015-6) cui si fa riferimento esplora un aumento dei casi di pubertà precoce *a causa del lockdown*; non a causa dei tablet o della mancanza di attività fisica. Certamente, durante le chiusure succede anche questo; ma sono semmai concause e non i primi responsabili, come si lascia intendere.

E poi che cosa c’entra questo con la scuola in presenza? Mia figlia, nell’orario in presenza, fa attività motoria *un’ora alla settimana*. Questa è esattamente scarsa attività fisica; la mancanza di attività fisica degna di questo nome è un danno grave che la scuola in presenza infligge ai bambini. I quali, per la maggior parte del tempo, passano la scuola in presenza seduti.

Ah, lo studio copre il periodo da marzo a settembre 2020; in altre parole, per metà dell’arco di tempo coperto *le scuole erano chiuse*. Tutte, in presenza e pure online. Alla fine dello studio si legge che, per stabilire veramente una correlazione tra *fattori patogeni specifici* e pubertà precoce, occorrono altri studi e campioni più numerosi. Ovvero, si fa dare per scontata allo studio una semplice ipotesi di lavoro.

Questi sono i passaggi più gravi. Ci sarebbe da scrivere per ore. Questo, per esempio:

>Le Istituzioni si devono adoperare per mettere in atto rapidamente tutte le misure necessarie allo svolgimento delle lezioni in sicurezza e in presenza per ogni ordine e grado di istruzione.

[Risate amare] È un anno che siamo in pandemia. Rapidamente? Forse il problema non è proprio la Dad, ma chi prende decisioni inefficaci e sconsiderate. Se scendono in piazza per mandare a casa il ministro della Salute, mi metto in prima fila. Se invece manifestano contro la Dad, come facciamo a salvare il salvabile, senza lezioni online e con governanti incapaci di mettere le scuole in sicurezza?

Tutti vogliamo che riaprano le scuole. Io non voglio che riaprano per tornare indietro di cinquant’anni. La scuola deve offrire il meglio dell’analogico e il meglio del digitale, armonizzati e bilanciati. Perché esiste una didattica sola, a volte in presenza e altre volte a distanza. Tutti i compiti che doveva sbrigare mia figlia a casa dopo otto ore di scuola in aula mi sembravano un’idiozia ed erano didattica a distanza bella e buona, anche se interamente analogica.

Chi vuole fare l’analogico con il digitale degli altri si faccia le sue di scuole, già ce ne sono di bellissime, la Costituzione non batte ciglio. Non si ha l’idea di quanto e bene si possa imparare online con buoni docenti e buoni metodi. La socialità, certo. È una foglia di fico. Si mettano in sicurezza le palestre per poter fare tutto quello sport, necessario, che la scuola schifa, e che sviluppa la socialità assai meglio delle lezioni frontali *d’antan*.

Visto infine che questa è gente che gioca sporco, un paragrafo insinuante me lo concedo. Non voglio neanche approfondire, ma sono convinto che dietro al sito di *A scuola!* ci sia una persona che di mestiere fa la *creative strategist* di una agenzia molto digitale e molto creativa.

Tutti volontari, eh. Ma per fare guerra al digitale nella scuola, qualcuno mette soldi per pagare competenze.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*