---
title: "Nessun dorma"
date: 2022-09-14T11:36:47+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [MacStories, Viticci, Federico Viticci, Apple TV+, Mac OS X, John Siracusa, Siracusa, Dungeons & Dragons, D&D, Roll20, baseball]
---
La ragione per fare tardi stanotte è che Federico Viticci ha pubblicato la sua [recensione di iOS 16](https://www.macstories.net/stories/ios-16-the-macstories-review/), che è obbligatoria praticamente quanto quelle di Mac OS X scritte [ai tempi di John Siracusa](https://hypercritical.co/2015/04/15/os-x-reviewed).

Domani notte ci sarà da preparare la sessione di [Dungeons & Dragons](https://dnd.wizards.com) su [Roll20](https://roll20.net).

Venerdì notte, dopo avere giocato a D&D, imperdibile il baseball su [TV+](https://www.apple.com/apple-tv-plus/).

Di giorno si vive la vita, di notte si vivono i sogni.