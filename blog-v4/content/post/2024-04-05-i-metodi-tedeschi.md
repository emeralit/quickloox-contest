---
title: "I metodi tedeschi"
date: 2024-04-05T15:35:58+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [LibreOffice, Open Source, Schleswig-Holstein, Brazil, Gilliam, Terry Gilliam]
---
Non riesco a convincere un’azienda di trenta persone a evitarsi lo spreco di Office.

In compenso lo Stato federale tedesco di Schleswig-Holstein [adotta Linux, LibreOffice e il software open source su trentamila computer dell’ammininistrazione locale](https://blog.documentfoundation.org/blog/2024/04/04/german-state-moving-30000-pcs-to-libreoffice/).

La decisione è avvenuta a seguito di un programma pilota condotto con successo e impatta su tre milioni di cittadini tedeschi, non esattamente un affare di nicchia.

I governanti parlano di *primo passo verso la sovranità digitale* e hanno rilasciato questa dichiarazione sulle motivazioni:

>Non abbiamo influenza sui processi operativi delle soluzioni proprietarie e sulla loro amministrazione dei dati, compreso in possibile flusso di informazioni verso Stati esteri. In quanto Stato, abbiamo verso i nostri cittadini e le nostre aziende una grande responsabilità di garantire che i loro dati sono conservati al sicuro, noi abbiamo sempre il controllo delle soluzioni IT che usiamo e possiamo muoverci in modo indipendente.

È vero che un’azienda privata può scegliere di guastarsi la vita come vuole. Sono fresco reduce da un ambiente che, quanto a equipaggiamento, ricordava [Brazil](https://www.mymovies.it/film/1985/brazil/) di Terry Gilliam. Ancora una volta, non è tanto la scelta in sé, ma il clima creato in cui la scelta riesce ad apparire giustificata a chi la compie. Chi usa quei prodotti si cala in un circolo vizioso che può portarlo a pensare e comportarsi in quel modo.

Ripeto, azienda privata fa quello che vuole. Ma azienda pubblica, che spende denaro dei contribuenti, dovrebbe farlo in modo trasparente e responsabile. L’unico modo di fare questo è *public money, public software*. In tutti gli altri casi ci sono dietro magagne.

Ora, capire come copiare i tedeschi. Detto da un italiano è un bel rosicare. eh.