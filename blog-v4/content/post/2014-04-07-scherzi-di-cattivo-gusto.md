---
title: "Scherzi di cattivo gusto"
date: 2014-04-07
comments: true
tags: [Windows, XP, Olanda, UK]
---
Teoricamente, domani termina il supporto di Windows XP da parte di Microsoft. Teoricamente; leggo notizia di un [accordo da cinque milioni e mezzo di sterline](http://www.computerweekly.com/news/2240217389/Government-signs-55m-Microsoft-deal-to-extend-Windows-XP-support) stipulato dal governo britannico con Microsoft, e di una [analoga iniziativa](http://www.dutchnews.nl/news/archives/2014/04/dutch_government_to_pay_micros.php) *multimilionaria in euro* da parte del governo olandese, per prolungare il supporto oltre la data stabilita.<!--more-->

Il sistema è già stato pagato e strapagato. Viene annunciata la cessazione del supporto – credo per la terza o quarta volta – ed è una finta, che serve solo a chiedere altri soldi, a far pagare un prodotto obsoleto multipli complessivi del prezzo originale.

Dall’altra parte si hanno incompetenti che lavorano con un sistema vecchio e stravecchio, più incompetenti perché pagano per conservarlo in vita invece che pagare per aggiornarlo, ancora più incompetenti perché in tredici anni non hanno avuto la capacità di programmare una transizione.

È una situazione che fa male a tutti. I *web designer* devono sprecare risorse per stare dietro a *browser* fuori da tutti gli standard e impermeabili a qualunque progresso; la sicurezza di tutti questi sistemi antichi è molto a rischio rispetto a qualsiasi sistema moderno e sono solo due esempi. Come se continuassimo a costruire tetti con dentro l’amianto per non fare lo sforzo di passare a soluzioni più sane e sicure.

Carnevale è finito da un bel pezzo, il primo di aprile è passato da quasi una settimana e gli scherzi continuano. Questo però è meno simpatico degli altri, poiché tutto il denaro di cui si parla è versato dai contribuenti e viene speso non per volontà degli stessi, bensì per incapacità di chi li rappresenta e astuzia da parte di chi vende. Parola grossa, *vende*; direi *propina*.