---
title: "Le mani nello spork"
date: 2015-09-02
comments: true
tags: [fork, spork, NeXTBsd, FreeBsd, OSX, Apple]
---
Neanche quest’anno ci sarà tempo per occuparsi di tutte le cose interessanti. Scopro or ora che si lavora a uno *spork* di [FreeBsd](http://freebsd.org) chiamato [NeXTBsd](http://nextbsd.org).<!--more-->

FreeBsd è un sistema operativo libero da cui Apple ha preso molto nel fare nascere OS X. NeXTBsd aspira a prendere il meglio di FreeBsd e unirlo alle parti *open source* di OS X, a scopo di modernizzazione ed efficienza.

Gli sviluppatori lo chiamano *spork* perché non lo intendono come un *fork*, un ramo di FreeBsd che diventa indipendente e si sviluppa diversamente da quello ufficiale aumentando sempre più il livello di separazione. Piuttosto, sembra di capire, si vuole seguire la linea prendendosi la libertà di intervenire quando pare opportuno.

È una bella notizia. Il lavoro su OS X che ha portato risultati aperti a tutti diventa foriero di sviluppi che non sono necessariamente quelli di Apple e contemporaneamente le parti *open source* del sistema operativo Mac possono solo diventare più robuste e sicure.

Per ora c’è poco o nulla. Promettono una installazione funzionante per fine mese e non vedo l’ora di poterci [mettere le mani](https://github.com/nextbsd/nextbsd).