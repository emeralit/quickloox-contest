---
title: "Umanamente impossibile"
date: 2017-05-28
comments: true
tags: [ Kern, Apple, Music, News, Politico, Go]
---
Già [spiegato](https://macintelligence.org/posts/2015-06-27-il-tocco-musicale/) che Apple ha scelto un approccio umano alla propria offerta musicale. Gli altri vanno per algoritmi: capiscono che cosa ti piace e vanno avanti a offrire qualcosa che ti piace o ti dovrebbe piacere perché ricorda quello che ti piace. Non scoprirai mai più musica inaspettata, sorprendente, che ti apre un mondo. L’algoritmo è per pensionati (mentali, non di anagrafe).<!--more-->

L’approccio umano, invece, forse è meno perfetto, forse può spiacerti una volta, ma un’altra potrebbe farti una sorpresa. Diventa un dialogo.

Non mi dispiace apprendere che Apple [ha assunto Lauren Kern](https://www.appleworld.today/blog/2017/5/25/lauren-kern-named-editor-in-chief-of-apple-news), figura redazionale di spicco del *magazine* [New York](http://nymag.com), per occuparsi di [Apple News](https://www.apple.com/news/).

Tra un flusso redazionale di notizie e uno di musica ci sono più similitudini che apparenze. Sono sicuro che su Apple News non compariranno *fake news* come quelle [tanto esecrate oggi](https://macintelligence.org/posts/2017-05-02-pre-verita/), esistenti da sempre e oggi alla ribalta proprio grazie all’algoritmo.

Algoritmo che fa cose potentissime, senza il quale non potremmo permetterci il tenore di civiltà che abbiamo, capace di spingere il possibile ai suoi limiti estremi, per esempio [sconfiggere il più forte giocatore di Go al mondo](https://www.theverge.com/2017/5/25/15689462/alphago-ke-jie-game-2-result-google-deepmind-china).

L’impossibile, però, può inseguirlo solo il tocco umano.
