---
title: "Chiudiamo una parentesi"
date: 2016-01-11
comments: true
tags: [Penflip, Frix01, Git, BBEdit, BitBucket, Markdown]
---
Accennavo ieri [all’idea di un post su Markdown](https://macintelligence.org/posts/2016-01-10-metti-una-sera-con-penflip/) ed eccolo qui.<!--more-->

Sono un *fan* consolidato di [Markdown](https://daringfireball.net/projects/markdown/) e lo uso ovunque posso: è uno splendido uovo di Colombo per realizzare testo con accatiemmellizzazione e formattazione leggera che restino comunque leggibili se visti in forma originale, non resa graficamente da un *browser*.

Imparare le basi del sistema è veramente, veramente banale e i programmi che permettono di scrivere produttivamente in Markdown oramai sono una pletora, tanto che rimando chiunque a una comune ricerca su Google o su App Store.

Un numero esagerato di programmatori ha lavorato ad ambienti che permettono di scrivere in forma semplice, con pochi o nessun menu, con poca o nessuna grafica a contorno, pochi o nessun comando, poca o nessuna distrazione, tutto essenziale, tutto minimalista, e Markdown è un formato che si adatta più che bene a questo stato mentale.

Non vedo o non conosco nessuno che sia al passo successivo e lavori ad ambienti che permettono di scrivere in forma furba. Mi spiego: un *link* in Markdown comprende tra parentesi quadre il testo che apparirà linkato e tra parentesi tonde l’indirizzo del link. Un sovraccarico di quattro caratteri rispetto al testo nudo, un’inezia. Scrivere a mano un link in Markdown è una passeggiata, farlo in Html è un male di testa.

Solo che non basta più. Io uso [BBEdit](http://www.barebones.com/products/bbedit/) per la sua personalizzabilità infinita. Ci vuole davvero poco per creare un comando che prepari automaticamente le quadre e le tonde in cui scrivere, o che applichi il formato del link a testo appositamente evidenziato.

Con lo stesso poco si fa esattamente la stessa cosa per l’Html. Ovvero, scrivere un link in Markdown oppure in Html a me costa *esattamente lo stesso sforzo*, perché il lavoro lo fa il programma. Anche se il link in Html è roba assai più complicata.

Tuttavia sono un professionista e mi tocca, anche non mi interessasse, inseguire la massima efficienza nello scrivere. Così affronto – mi interessa pure – la minima fatica di personalizzare BBEdit per poter avere questi risparmi di tempo, che possono sembrare poca cosa e invece, per un professionista, a fine giornata si fanno sentire.

Manca, o non l’ho ancora scoperto, un programma che sia veramente astuto e che i link e tutto il resto li metta, Markdown o Html, da solo o quasi. Allora sì che Markdown potrebbe diventare di uso comune e anche Html conoscerebbe una diffusione e una confidenza più vicina a quella che si merita.

Gli autocompletamenti, i menu grafici, le *palette* di comandi che vedo in giro mi paiono un buon inizio, ma niente più di questo.

Nel frattempo consiglio a chiunque Markdown, dove volendo si può fare tutto anche a mano con livelli sopportabilissimi di sovraccarico rispetto al testo puro. E consiglio pure un buon editor di testo molto personalizzabile, perché alla lunga perfino Markdown diventa noioso da inserire a mano.

Al momento, però, è la strada maestra per scrivere in modo rapido, leggero, ricco e comunque leggibile.