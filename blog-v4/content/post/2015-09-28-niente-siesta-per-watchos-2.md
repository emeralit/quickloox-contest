---
title: "Niente siesta per WatchOS 2"
date: 2015-09-28
comments: true
tags: [WatchOS, iPhone, watch]
---
L’aggiornamento a WatchOS 2 è apparentemente semplice a spiegarsi. La mia esperienza è stata tuttavia più complessa.<!--more-->

Si passa dalla app Apple Watch per iPhone e i requisiti sono orologio nella stessa Wi-Fi del telefono, orologio alimentato e orologio già carico almeno per metà.

Ho proceduto confidente che iPhone avrebbe amministrato con saggezza eventuali pause nel procedimento (l’aggiornamento nel mio caso pesava 515 megabyte), ma così non è stato. Avviato l‘aggiornamento me ne sono disinteressato, fino a scoprire due giorni dopo che era bloccato su iPhone in una fase di verifica.

Ho provato a spegnere e riaccendere watch, che poi funzionando altrimenti non rispondeva All‘ordine. Ho allora effettuato un riavvio forzato, di lui e di iPhone, dopo avere levato a quest‘ultimo l’autoblocco dopo cinque minuti.

Tutto è andato bene in tempo più che ragionevole.

Morale: l’aggiornamento di WatchOS 2 non è ancora trasparente e autonomo come quelli di App Store. Consiglio supervisione dell’operazione in un momento nel quale certamente non verrà interrotta da altre faccende o da un’uscita con watch al polso. E non lasciare che iPhone possa andare in stop.