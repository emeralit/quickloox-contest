---
title: "Manca il sorriso"
date: 2013-09-09
comments: true
tags: [iTunes, iPod, Windows]
---
Quando installo iTunes su Mac, mi scoccia passare dall’approvazione del contratto di licenza. Neanche lo guardi, è irrilevante per la vita quotidiana di una persona normale. Due clic di tempo perso.<!--more-->

Non lo dirò mai più. Ho installato iTunes e un iPod shuffle – un iPod shuffle, quattro centimetri quadrati – su un computer con Windows 7.

Il contratto di licenza è solo l’inizio. Appare la lista dei formati audio da decidere se assegnare a iTunes o a Windows Media Player. E poi l’antivirus: consenti l’esecuzione di questo? Consenti l’installazione di quello? Consenti la configurazione di quell’altro? Ogni volta consenti e ti pare di essere in quei film di cappa e spada dove il protagonista si sposa anche se non c’entra niente, in sacrificio per salvare l’onore della dama.

Tutto avviene con una lentezza caraibica e il calore del Circolo polare artico.

Alla fine iTunes parte e ti sembra un miracolo. Colleghi fisicamente lo shuffle – quattro centimetri quadrati – e contempli l’avviso che sta installandosi il driver.

Ti chiedi quanta gente passi una parte consistente della vita a installare driver, sposata contro natura a un antivirus isterico, e capisci perché sia gente con una vita informatica triste.

Regala un sorriso a chi usa Windows.