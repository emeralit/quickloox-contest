---
title: "Guidare l’innovazione"
date: 2014-01-11
comments: true
tags: [Mac]
---
*9to5 Mac* pubblica [due foto di Chromebook Toshiba](http://9to5mac.com/2014/01/07/toshiba-leading-innovation/). Ovvero, portatili che non hanno un sistema operativo classico ma un sottoinsieme svelto e snello concepito non da Toshiba, ma da Google.<!--more-->

Guardando le foto ci si può fare un’idea della fonte dell’ispirazione per il telaio.

Il motto aziendale di Toshiba è *leading innovation*. E io sono senza parole.
