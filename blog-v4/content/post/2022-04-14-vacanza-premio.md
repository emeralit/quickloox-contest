---
title: "Vacanza premio"
date: 2022-04-14T00:34:29+01:00
draft: false
toc: false
comments: true
categories: [Internet, software]
tags: [QuickLoox, blog, Unix, Julia Evans, Evans, Terminale]
---
Da oggi sono in viaggio con la famiglia e nei prossimi giorni l’aggiornamento del blog potrebbe essere sporadico o nullo. A parte il clima vacanziero, devo collaudare il setup per il collegamento a distanza con Mac e qualcosa potrebbe non andare secondo le aspettative.

In ogni caso la produzione dei post continuerà; nel peggiore dei casi, gli aggiornamenti resteranno fermi fino a martedì ma sarà rispettata la consegna di un post al giorno, o verrà colmato al più presto un eventuale ritardo.

Mi consolo pensando che tutti i casi possibili sono previsti, non c’è alcun impedimento tecnico o personale e le cose importanti funzionano; inoltre, a gennaio queste pagine erano una pallida ombra di quello che sono ora in termini di navigazione, organizzazione, base dati. Al più sarà una piccola vacanza, temporanea per giunta, dopo un buonissimo primo trimestre 2022.

Uova per tutti, sorprese per chi le ama e colombe a volontà, che servono.

Un esempio di sorpresa? Un elenco di [nuovi (o quasi) comandi per il Terminale](https://jvns.ca/blog/2022/04/12/a-list-of-new-ish--command-line-tools/), radunati da Julia Evans. Unix è tutt’altro che scritto nella pietra.