---
title: "Ausilio alla lettura"
date: 2015-09-11
comments: true
tags: [Blodget, iPhone5, Galaxy, Samsung, Apple]
---
Scrivo questo *post* ben prima che inizi l’evento dove Apple certamente avrà presentato il prossimo iPhone.<!--more-->

Qualunque cosa scrivano i commentatori sui siti so-tutto, si ripensi alle parole di [Henri Blodget su Business Insider](http://www.businessinsider.com/if-the-iphone-5-really-looks-like-this-apple-may-be-screwed-2012-7?IR=T), 30 luglio 2012, riguardanti iPhone 5:

>Se iPhone 5 ha davvero l’aspetto che mostrano le foto pubblicate anticipatamente su web, Apple potrebbe essere fregata. Perché iPhone 5 somiglierebbe moltissimo a iPhone 4S. Che era identico a un iPhone 4, un telefono oramai vecchio di due anni, mentre intanto Samsung e altri costruttori fanno uscire modelli che a vederli resti a bocca aperta. […] Il Galaxy sembra un telefono di nuova generazione. iPhone, a confronto, sembra piccolo e vecchio. […] Speriamo quindi che quelle immagini non siano davvero di iPhone 5.

Lo erano, invece. E le cose però sono andate in maniera *leggermente* diversa dalla fregatura.