---
title: "Gli piace predire facile"
date: 2023-06-12T01:26:03+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Vision Pro, Steven Sinofsky, Sinofsky]
---
La ricaduta dell’annuncio di Vision Pro consiste anche in mucchi di recensioni che si pronunciano, con competenza discutibile, sul futuro della piattaforma.

A tutti costoro dedico un [Sinofsky d’annata](https://twitter.com/stevesi/status/1666173292053696512), sei giorni fa, invecchiato benissimo.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Why are people so quick to proclaim failure for new products? It seems a dumb thing to ask. I mean knowledgable people look at a new product and think it doesn&#39;t cut it and will fail. Much more going on. Innovation is nearly impossible to deliver. Harder to predict/analyze. 1/ <a href="https://t.co/Y8l6YwtOhR">pic.twitter.com/Y8l6YwtOhR</a></p>&mdash; Steven Sinofsky (@stevesi) <a href="https://twitter.com/stevesi/status/1666173292053696512?ref_src=twsrc%5Etfw">June 6, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

>L’innovazione è quasi impossibile da realizzare. Più dur ancora predire/analizzare.

A me piace soprattutto uno dei punti successivi del thread:

>Predire il fallimento è una forma di credito sociale, un modo di porsi sopra l’azienda. In effetti è una appropriazione di potere. È anche una forma di imbroglio. Una truffa.

Tutto è spiegato. Il thread è lungo ma assolutamente informativo.