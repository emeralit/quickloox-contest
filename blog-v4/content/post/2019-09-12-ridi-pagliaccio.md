---
title: "Ridi, pagliaccio"
date: 2019-09-12
comments: true
tags: [iPhone, Popular, Mechanics]
---
Legge di [Canio](https://it.wikipedia.org/wiki/Vesti_la_giubba):

*Quando in rete si ridicolizza un annuncio Apple, il successo del prodotto è certo.*

Dimostrazione a cura di [Popular Mechanics](https://www.popularmechanics.com/technology/gadgets/a28984993/iphone-11-pro-camera/), dove si mostrano un po’ di *tweet* parodistici sulla disposizione delle tre fotocamere sul dorso di iPhone Pro e poi si spiega che cosa, con l’aiuto del software, possono fare. La conclusione:

>Insomma, l’aspetto della tripla fotocamera è buffo? Sicuro. Ma sembra poter essere la migliore fotocamera da telefono mai apparsa; e non ci sarà da ridere quando scatterai i primi ritratti in stile professionale con questo mostro.

Come il personaggio dei *Pagliacci* di Leoncavallo, spesso chi fa ridere rosica dentro. A volte per ragioni importanti, a volte per invidia brutta, a volte per ricomporre la dissonanza cognitiva: c’è da trovare un modo per continuare a dirsi che, nonostante si sia spesa la metà, i risultati sono comunque interi. Impegnativo.

[Ridi del duol che t'avvelena il cor!](https://www.rockol.it/testi/1917815/luciano-pavarotti-pagliacci-vesti-la-giubba-leoncavallo).