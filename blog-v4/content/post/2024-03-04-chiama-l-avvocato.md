---
title: "Chiama l’avvocato"
date: 2024-03-04T01:42:15+01:00
draft: false
toc: false
comments: true
categories: [Software, People]
tags: [Filippo Strozzi, Strozzi, Avvocati e Mac, Ollama, ai, intelligenza artificiale, LLM]
---
Filippo è stato così gentile da anticiparmi l’URL (attivo dalle nove) del post su *Avvocati e Mac* che raccoglie i frutti di un weekend di hacking durante il quale io sono riuscito a firmare un PDF su iPad con Apple Pencil, mentre lui [si è installato un modello linguistico (LLM) completo di chatbot su Mac mini M1 mediante software open source](https://avvocati-e-mac.it/blog/2024/3/3/lintegrazione-dellintelligenza-artificiale-in-uno-studio-legale-usando-un-macmini-m1-vantaggi-e-sfide), per arrivare a costruire un flusso di lavoro per svolgere lavoro effettivo.

Dal curioso al potenziale emulatore, raccomando assolutamente la lettura, perché Filippo ha documentato su Telegram tutte le fasi della procedura e so quanta roba c’è nell’articolo. Se interfacciarsi con il solito chatbot nel cloud è banale, portare un LLM in locale in modo utilizzabile, con ovvi vincoli di memoria e processore, è un bel traguardo.

Sempre con l’obiettivo di attivare un flusso di lavoro professionalmente significativo, sono state affrontate varie sfide, dall’uso dell’italiano a quello dei Comandi rapidi, alla connessione in remoto da iPad fino al riconoscimento delle immagini.

Non è tutto qui e non voglio dire come è andata, per non rovinare la sorpresa. Il racconto sarà affascinante e ci sarà un sacco da imparare.

Una volta pareva che Mac fosse diventato un sistema chiuso privo di interesse per chi ama smanettare e provare cose nuove. Invece il giusto mix di tempo, determinazione e voglia di divertirsi possono portare molto lontano. La laurea, va da sé, è opzionale.