---
title: "Chi non sa"
date: 2024-01-14T14:57:20+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [ChatGPT, intelligenza artificiale, AI, Khan Academy, Gates, Bill Gates, New York Times, NYT]
---
*Chi sa fa, chi non sa insegna*, si scherza. Se è la premessa, nessuna sorpresa che si parli molto degli assistenti generativi come strumenti da usare nell’istruzione.

All’amministratore delegato di Khan Academy è piaciuto molto un bot costruito sopra ChatGPT che funge da insegnante personalizzato per ciascuno studente, [scrive il New York Times](https://www.nytimes.com/2024/01/11/technology/ai-chatbots-khan-education-tutoring.html). L’articolo ricorda che i tentativi precedenti di affermare l’insegnamento robotizzato sono iniziati già sessant’anni fa e nessuno ha avuto un gran successo. Sessanta anni fa, o trenta, non esisteva però [Khan Academy](https://www.khanacademy.org): una organizzazione no profit dietro un sito di lezioni e tutorial su qualunque materia, gratis. Hai un punto debole? Una lacuna? Vuoi rifare il liceo per capire la trigonometria? Sulla Academy trovi materiale pronto, collaudato, di qualità. È una risorsa impensabile nel mondo prima di Internet, incomprensibile per i nonni, rivoluzionaria (e infatti se ne parla solitamente poco). In linea di principio, allocato il tempo e presente la motivazione, la Academy consente un apprendimento efficace a qualunque livello, università compresa, senza spendere soldi che non vadano in un computer e una connessione alla rete.

In questa ottica, un buon bot capace di sostenere un dialogo e proporre i contenuti di una lezione in modo più filante e costruttivo può solo fare del bene. È uno sviluppo perfettamente logico che si sposa molto bene con le capacità dei chatbot, una volta messi al servizio dei contenuti che servono. Ne posso pensare solo bene.

Altri hanno idee più ampie su cui nutro dubbi. A novembre, Bill Gates ha scritto una delle sue Notes con il titolo [L’intelligenza artificiale sta per cambiare completamente il modo in cui usi il computer](https://www.gatesnotes.com/AI-agents?). A essere cattivi, sembra che il titolo sia stato concepito proprio dall’intelligenza artificiale. Ma andiamo avanti.

Gates crede negli *agenti* software, entità che comprendono il linguaggio naturale e possono eseguire le attività più disparate grazie alla loro conoscenza di chi usa il computer. Se ne parla da trent’anni, come ammette lui stesso, però i sistemi attuali potrebbero avere finalmente aperto la porta al loro avvento.

Non è una passeggiata. Gates scrive tranquillamente che *nessuno si è ancora fatto un’idea di come sarà la struttura dati di un agente*:

>per creare agenti personali, ci serve un nuovo tipo di database in grado di acquisire tutte le sfumature dei propri interessi e delle proprie relazioni e richiamare prontamente queste informazioni nel rispetto della privacy.

Un altro problema aperto è quanti agenti interagiranno con l’utente e, se saranno più di uno, chi si occuperà della prioritizzazione della loro attività oppure della loro eventuale collaborazione. *Vaste programme*, diceva Charles De Gaulle dei progetti utopistici.

Facciamo finta tuttavia di averli, gli agenti, e vediamo come potrebbero funzionare:

>Poche famiglie possono permettersi un tutor che lavori faccia a faccia con uno studente in aggiunta alla sua attività in classe. Se gli agenti potessero cogliere ciò che rende efficace un tutor, aprirebbero a chiunque la voglia la possibilità di aumentare la propria istruzione. Se un agente tutor sa che a un ragazzo piacciono Minecraft e Taylor Swift, userà Minecraft per insegnarli a calcolare il volume e l’area delle forme, e i testi delle canzoni di Swift per insegnare storytelling e schemi di rima. L’esperienza sarà molto più ricca – per esempio, con grafica e sonoro – e personalizzazione di quella dei tutor automatici di oggi.

In superficie suona tutto molto bene, non fosse che, se l’idea di attaccare l’intelligenza generativa al sito di apprendimento valorizza i punti di forza dell’intelligenza generativa stessa, la descrizione dell’agente pare martellare su tutti i suoi punti deboli. Se a me piace Taylor Swift ma – pura ipotesi – i suoi versi rimano da schifo, l’agente seguirà i desiderata dello studente o sceglierà un cantante dalle rime perfette? E se invece servisse Gabriele D’Annunzio, dai contenuti scialbi ma dalla struttura formale più che perfetta?

Sui miei computer sono sparse decinaia e decinaia di app provate, magari a lungo, e dismesse, che nessuno si è ancora dato la pena di eliminare, oppure per cui potrei avere un improvviso risveglio di interesse tra cinque anni, chi può dirlo? E sarà proprio Minecraft il programma ideale per calcolare i volumi? Come verrà scelto? Se il programma migliore, ancora una volta, dovessi ancora scoprirlo, ci penserebbe l’agente? O si limiterebbe a compiacermi?

Il problema principale degli assistenti generativi è la totale assenza di creatività, invenzione, improvvisazione, capacità di rovesciare il tavolo e provare qualcosa di completamente nuovo. Non parliamo dell’intuizione: ascolti Taylor Swift ma impazziresti per Garth Brooks, se solo tu sapessi che esiste. Questo è prerogativa umana, presente nei grandi insegnanti, e totalmente assente dall’intelligenza artificiale di ora.

Capisco che Bill Gates ami l’idea di un esercito di gente formata su Minecraft di Microsoft e su Office di Microsoft, che impari quello che piace a Microsoft. Questo è il mestiere praticato dall’imprenditore e dalla società in tutti questi anni. I veri agenti non sono dentro il computer, ma alla tastiera: hanno imparato perfettamente quello che vuole il computer, e lo mettono in pratica.

L’istruzione però è un’altra cosa. Khan Academy, oggi, manca di inventiva, improvvisazione e tutte quelle cose di prima. ChatGPT non fa neanche finta di portarle, tuttavia migliora l’esistente. E tutto si riferisce a uno strumento il cui uso è libera scelta dell’utente.

Pensare ai bambini con davanti il bot che li indottrina con la melassa del consenso diffuso su Internet e si limita a confermare le loro abitudini e preferenze (dopo averli profilati fino alla punta dei calzini, peraltro), senza un colpo di testa, una ribellione, un anticonformismo, una sparigliata, il suggerimento che secondo me dovresti studiare filosofia teoretica e non chimica industriale, ecco, l’apprendimento standardizzato rivolto alla formazione standardizzata di persone standardizzate. è desolante, a partire dal fatto che per insegnargli a creare un sito userebbe WordPress.

Chi non sa insegna e va bene, purché sappia almeno insegnare e i sistemi attuali mancano totalmente delle basi per essere insegnanti almeno tollerabili.