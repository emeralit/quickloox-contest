---
title: "Costi quel che costi"
date: 2018-09-16
comments: true
tags: [iPhone, XS, Max, MacBook]
---
Interessante esperimento: [confrontare il nuovo iPhone XS Max con MacBok Pro 13”](https://twitter.com/somospostpc/status/1041018921279741952) mediante il metodo anni novanta, le rispettive liste della spesa.

<blockquote class="twitter-tweet" data-lang="it"><p lang="en" dir="ltr">I did a thing to convince myself the XS MAX 512 GB wasn&#39;t so expensive <a href="https://t.co/ne3hiR2NR9">pic.twitter.com/ne3hiR2NR9</a></p>&mdash; Alex B 📈 (@somospostpc) <a href="https://twitter.com/somospostpc/status/1041018921279741952?ref_src=twsrc%5Etfw">15 settembre 2018</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

Viene fuori che il prezzo di iPhone non è così così campato per aria.

Interessante il fatto che i so-tutto rimasti fermi agli anni novanta, che confrontano tutto con la lista della spesa, si guardino bene dall’effettuare il confronto senza il quale non riescono a vivere: se ne escono con battute tipo *piuttosto fatevi un viaggio* oppure *una PlayStation costa meno*.

È che salta fuori che, se le cose costano quello che costano, a volte c’è un motivo. Gente cresciuta passando la vita a cercare di nasconderlo, che altro può fare?