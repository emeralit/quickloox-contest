---
title: "Il tempo per gli hobby"
date: 2013-07-08
comments: true
tags: [Apple]
---
Apple è al lavoro sul televisore, vero? Dopotutto l’analista Gene Munster [ci ha costruito sopra](https://macintelligence.org/posts/2012-12-10-sempre-la-solita-tv/) la sua carriera degli ultimi anni e la prevede per la seconda metà del 2013 [già dall’anno scorso](http://www.businessinsider.com/gene-munster-on-apple-tv-2012-11).<!--more-->

Magari ha pure ragione e forse meglio se l’avesse, così almeno dovrebbe farsi venire un’altra idea. Certamente aspettarsi che Apple entri in un mercato perché quel mercato esiste e Apple non c’è ancora entrata non è una idea luminosissima: c’è ancora gente in fila ad attendere il *netbook*.

E poi, o cambi veramente le cose, o sprechi soldi e lavoro. A fine settembre Microsoft <a href="http://www.webtv.com/msntv2/ClosureFAQ.asp">chiude Msn TV</a>, una volta nota come WebTV. Era partita nel 1996, con una spesa di acquisto di un mezzo miliardo di dollari.

Diciassette anni buttati partendo da un’idea semplicissima e ovvia, mettere insieme la Rete e lo Schermo. Peccato che la gente abbia il cervello acceso sulla prima e spento davanti al secondo, così che l’ibrido non ha ancora dimostrato di funzionare.

Se arriverà una Apple TV diversa dall’attuale Apple TV, sarà anche diversa da tutto quello che conosciamo almeno quando iPad è stato diverso dai *netbook*. Dovrà anche essere dannatamente limitata, in modo che coloro-che-sanno possano predirne in massa e con grande sarcasmo la fine prematura.

Dopo avere satireggiato in lungo e in largo la definizione dell’attuale Apple Tv come *hobby* da parte di Steve Jobs, naturalmente. Intanto gli altri chiudono.