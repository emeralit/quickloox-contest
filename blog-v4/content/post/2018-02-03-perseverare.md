---
title: "Perseverare"
date: 2018-02-03
comments: true
tags: [Apple, iPhone, Tgcom24]
---
La rilevanza data allo studio di Consumer Intelligence Research Partner per cui [iPhone 8 ha venduto più di iPhone X](http://appleinsider.com/articles/18/01/22/iphone-8-outsold-iphone-x-in-apples-holiday-quarter-consumer-survey-finds) nel trimestre di Natale.<!--more-->

La sicumera con la quale l’autorevolissimo Tgcom24 titola [iPhone X, vendite al di sotto delle aspettative](http://www.tgcom24.mediaset.it/tgtech/iphone-x-vendite-al-di-sotto-delle-aspettative-sara-ritirato-entro-autunno_3118830-201802a.shtml) senza avere capito alcunché della notizia che riporta e producendo circa un errore fattuale per paragrafo.

Tim Cook, [a commento](https://sixcolors.com/post/2018/02/this-is-tim-transcript-of-apples-q1-2018-earnings-call/) dei risultati effettivi:

>iPhone X […] è stato il nostro telefono più venduto in ogni settimana di commercializzazione.

Luca Maestri, *Chief Financial Officer* Apple:

>Durante il trimestre abbiamo venduto 77,3 milioni di iPhone, il numero più alto di sempre per un trimestre di tredici settimane. La media di vendite per settimana è in crescita del sei percento rispetto al trimestre di Natale dello scorso anno.

Tgcom24, questo, mica lo scrive. Piuttosto rilancia i [deludenti numeri sulle vendite](http://www.tgcom24.mediaset.it/tgtech/nuova-grana-per-l-iphone-x-non-permette-di-rispondere_3121598-201802a.shtml).

Diabolico.