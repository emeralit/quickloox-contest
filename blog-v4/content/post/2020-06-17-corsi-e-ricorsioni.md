---
title: "Corsi e ricorsioni"
date: 2020-06-17
comments: true
tags: [Fractal, Tree, Generative, Explorer, iPad, Lidia]
---
A parte le possibilità infinite di navigare arbitrariamente dentro gli oramai consunti insiemi di Mandelbrot e Julia, ho scoperto [Generative Tree Fractal Explorer](https://apps.apple.com/it/app/generative-tree-fractal-explorer/id1111819857?l=it) che è gratis su App Store.

Primogenita, che disegna appassionatamente, ha notato una vecchia t-shirt del papà a tema frattale e ha chiesto informazioni. Esplorare Mandelbrot richiede troppa pazienza e comprensione della materia per essere veramente apprezzato; questa soluzione certamente non è la migliore possibile in senso assoluto, ma mi ha sorpreso molto positivamente.

La app affronta la generazione frattale ricorsivamente e mette a disposizione una interfaccia scandalosamente intrigante per quanto è semplice e funzionale. Il curioso vorrà lasciare i parametri generativi come sono e smanettare liberamente. Il risultato orbita tra le classiche felci generate a macchina e lo spirografo, in modi insospettabilmente vari e liberi.

Primogenita ci si è persa dentro per un'ora, contentissima, e io pure. Per una prossima sera: modifichiamo anche i parametri.