---
title: "Nodi alle dita"
date: 2017-01-23
comments: true
tags: [Codemotion, Knotmania, iOS]
---
Spezzo volentieri una lancia a favore di [Knotmania](http://www.2think.it/knotmania/), gioco per iOS che ho scoperto durante la [visita all’ancora recente Codemotion di Milano](https://macintelligence.org/posts/2016-11-28-i-due-giorni-del-codice/).

Il gioco visualizza l’attività di creature enigmatiche e paciose, le String, il cui passatempo quotidiano è intrecciarsi.

A noi tocca districare dolcemente e con perizia i loro inviluppi.

Le String sono pacifiche e non si oppongono, solo che – lasciate a se stesse – si intrecciano. L’operazione deve dunque avvenire senza troppe pause, o tocca rifare.

Ci sono diverse modalità di gioco, compresa una stile Zen nella quale mancano limiti di tempo ed è possibile perdersi con la mente nell’interagire amabilmente con le String al netto di qualsiasi pressione.

Il prezzo è onestissimo per la qualità del gioco e rappresenta un compenso del tutto equo per una *software house* piccola, indipendente, capace e largamente italiana.

Ogni tanto fermo le dita e sto semplicemente a guardare le String che si divertono; anche soltanto questo vale la pena.
