---
title: "Una versione per attirarli"
date: 2019-08-27
comments: true
tags: [Angband, Brogue]
---
Ho appreso con soddisfazione delle modifiche apportate ad [Angband](http://rephial.org) in versione 4.2.0. Se, tra tanti pregi, uno dei difetti del gioco stava in una certa meccanicità dell’interazione, ora molto è cambiato.

Molte cose possono accadere che vanno ben oltre il semplice incontro dei mostri e relativo combattimento. I ragni tessono tele, i ranger posano bersagli fantoccio per sviare i mostri, i maghi possono assorbire energia magica da oggetti magici, i ladri possono derubare i mostri e c’è molto altro ancora nella direzione di una maggiore imprevedibilità.

Compaiono inoltre nuovi mostri, ma soprattuto nuove classi da giocare e persino due nuovi regni magici, natura e ombra. La rigiocabilità, già alta, è ancora superiore. Chi conosceva a memoria classi e incantesimi può trovare vere novità da scoprire.

Lo sviluppo è stato condotto nell’ascolto della comunità degli appassionati e si è registrato un consenso generale sull’idea che le migliorie andassero per quanto possibile verso una maggiore e migliore tolkienizzazione del gioco. Un bell’indizio che la crescita è sana.

Per portare qualcuno a scoprire i *roguelike*, Angband è più che mai una scelta di qualità. Per veterani che magari avevano perduto interesse, è l’occasione di imbarcarsi nuovamente nell’esplorazione sotterranea.

Se solo lo mettessero anche su iOS.
