---
title: "Pornografia e abuso"
date: 2014-06-05
comments: true
tags: [Android, iOS, Pornhub, Gizmodo, Safari]
---
Dopo anni e anni, permane il mistero degli apparecchi Android che vengono acquistati in grandi numeri ma poi non figurano nelle statistiche di traffico. O milioni di aggeggi fatti per andare su Internet vengono acquistati per non andare su Internet, o sono troppo scarsi perché ci si vada su Internet con soddisfazione, o chissà.<!--more-->

Spesso saltano fuori spiegazioni sociologiche, di benessere economico degli acquirenti e altro.

Beh, Pornhub ha compiuto – su richiesta di Gizmodo, che quando c’è di mezzo immondizia non si fa mai pregare – una [indagine](http://www.pornhub.com/insights/pornhub-web-browser-comparison/) sui visitatori del sito, che contiene quantità irragionevoli di filmati pornografici visibili gratis.

Il porno è contenuto discutibile ma fruito universalmente, da ricchi e da poveri. Il porno gratis, poi, non ne parliamo.

E anche qui la stessa storia. Safari su iOS è vastamente più usato di Android, che pure è diffuso in quantità molto maggiori. Forse i seguaci di iPad e iPhone sono più sporcaccioni? Per niente; le percentuali sono le stesse che si vedono per il commercio elettronico o il comune traffico web.

Quale che sia il motivo, la massa equipaggiata con Android non va su Internet, o ci va molto poco. I soldi non c’entrano, se neanche il porno gratis li muove. E direi che le spiegazioni alternative a un cattivo *design* di hardware e software, a questo punto, sono davvero terminate.