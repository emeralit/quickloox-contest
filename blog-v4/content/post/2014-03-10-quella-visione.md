---
title: "Quella visione"
date: 2014-03-10
comments: true
tags: [Jobs, iPad, FaceTime]
---
Per quanto lo hardware sia inesorabilmente datato e le interazioni con lo schermo avvengano tramite lo stilo di quei tempi, impossibile non vedere oltre: nel video [Moment of Discovery](http://www.youtube.com/watch?v=JufNR1hQ9h0), datato 1995, si intuisce iPad, si anticipa FaceTime, soprattutto è chiara ed evidente la consapevolezza che la rete e la tecnologia facile e a portata di ogni individuo siano fattori di progresso da incoraggiare e perseguire.<!--more-->

Del tutto a margine: Steve Jobs era altrove da dieci anni. Mancavano allora il coordinamento, l’organizzazione, la *leadership*. Non le idee.

<iframe width="420" height="315" src="//www.youtube.com/embed/JufNR1hQ9h0" frameborder="0" allowfullscreen></iframe>