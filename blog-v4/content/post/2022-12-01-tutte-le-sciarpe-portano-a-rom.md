---
title: "Tutte le sciarpe portano a Rom"
date: 2022-12-01T00:03:05+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Macintosh, IIsi, Macintosh IIsi, fbz, ImageMagick]
---
Un amico tiene giustamente come trofeo un maglione lavorato a mano che riprende l’interfaccia grafica del Finder e non c’è niente da dire; ma adesso è arrivata la [sciarpa fatta con una Rom di Macintosh IIsi](https://hackers.town/@fbz/109394159995704341) e non ce n’è veramente più per nessuno.

Sono stati scomodati GitHub, Archive.org e qualche tipo di macchina per cucire domestica (hackerata) oppure industriale, nonché ImageMagick per un passaggio grafico essenziale.

Sarà tutto l’insieme a tenere caldo, o solo i bit accesi? E se non tiene caldo abbastanza, svolgerla e riavvolgerla funzionerà?