---
title: "A brutto Muse"
date: 2022-04-04T01:32:20+01:00
draft: false
toc: false
comments: true
categories: [Web, Software]
tags: [Muse]
---
Vogliamo portare le figlie al [Muse di Trento](https://www.muse.it/it/Pagine/default.aspx) approfittando delle vacanze pasquali.

Le prenotazioni si aprono con dieci giorni di anticipo sulla data scelta e non prima.

Così, a mezzanotte in punto, mi sono presentato per prenotare i biglietti su giovedì 14. Appena l’orologio ha segnato 00:00 ho ricaricato la pagina. Ci avrà messo due o tre secondi.

La vendita per giovedì 14 è chiusa su tutto il giorno, in tutte le fasce orarie.

In quella data il museo è aperto, dunque dovrebbe vendere normalmente i biglietti.

C’è qualche problema nel sistema? Non parrebbe: in tutte le altre date possibili, c’è disponibilità di posti.

Sono stato battuto in velocità da decine e decine di concorrenti? Non si può escludere. Però significherebbe vera ressa. Per il prossimo weekend ci sono ancora posti, non vedo assalti festivi. E poi sono arrivato sulla pagina in cinque secondi, non in cinque minuti.

La spiegazione che mi sono dato – e verificherò tra qualche ora – è che il sistema mostri le prenotazioni all’ora giusta del giorno giusto e però ci voglia il clic di un solerte dipendente per aprire effettivamente le vendite.

Nel qual caso non ci sarebbe nulla di male, a parte chiedersi perché questo comportamento non venga semplicemente programmato; ma allora si potrebbero informare gli sprovveduti che si affidano a una pagina web.

Per ora la mia interazione con il Museo delle scienze è sembrata quella con il Museo dell’improvvisazione gestionale. Spero di potermi ricredere.