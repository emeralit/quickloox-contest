---
title: "Guarda quel bit"
date: 2017-07-27
comments: true
tags: [Mame, Apple, Venerandi, Hamurabi, Ars, Technica]
---
Delle tante iniziative di retrocomputing, una mi appassiona particolarmente ed è [Mame](http://mamedev.org/), l’emulazione degli arcade game apparsi dagli anni settanta fino a oggi, responsabili della prima esperienza digitale di tanti ragazzi di allora e pure di numerose passioni per la tecnologia germogliate proprio in quel momento.<!--more-->

È difficile rimanere genuinamente stupefatti da un articolo, di questi tempi, ma a me è capitato leggendo su *Ars Technica* che alcuni retroappassionati recuperano vecchi giochi a rischio di estinzione aprendo i chip protetti delle macchine di una volta e [leggendo uno per uno i byte del codice anticopia](https://arstechnica.com/gaming/2017/07/mame-devs-are-cracking-open-arcade-chips-to-get-around-drm/) grazie a ingrandimenti dei circuiti.

Ci sono gruppi di persone che lavorano a vista su gruppi di bit e questo è veramente fuori dall’ordinario.

Aggiungo che lavori di conservazione di vecchi giochi e programmi [sono fiorenti](http://www.apl2bits.net/2011/08/22/the-art-of-the-crack/) anche per Apple ][ e che dobbiamo ringraziare tutti quanti si cimentano in sfide di questo genere.

Nonché quanti la buttano sul culturale come [Fabrizio Venerandi con il suo Hamurabi](https://macintelligence.org/posts/2017-07-22-il-codice-il-codex-il-coding/). Sono i più rari, che servono maggiormente in questi tempi oscuri.