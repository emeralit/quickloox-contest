---
title: "La posizione del problema"
date: 2016-05-15
comments: true
tags: [Apple, iTunes, iMore]
---
Apple ha [confermato a iMore](http://www.imore.com/apple-confirms-reports-potential-bug-itunes-safeguard-patch-expected-next-week) che un numero molto ridotto di utilizzatori di iTunes e servizi come Apple Music si è ritrovato con la propria musica cancellata dal disco locale.<!--more-->

Il problema esiste ufficialmente ma non si è ancora riusciti a riprodurlo, ossia a determinare un difetto di programmazione oppure una sequenza precisa di operazioni che porti alla sicura cancellazione di una raccolta di musica.

Uscirà un aggiornamento di iTunes provvisto di *salvaguardie aggiuntive*. Ma a che serve, se finora il problema non è stato riprodotto? Il software non può contenere la soluzione di un *bug* che ancora non è stato identificato.

Viene il sospetto che la posizione del problema sia situata più tra la sedia e la tastiera, più che dentro il codice. E c’è una differenza tra il dovere di fare il software più semplice e comprensibile che si può, e il diritto di operare distratti con la pretesa che il sistema ci protegga al mille per mille dalla distrazione.