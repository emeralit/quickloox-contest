---
title: "L’importante è partecipare"
date: 2021-06-30T12:37:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Polympics, Polytopia] 
---
Cominciano a brevissimo i Giochi. Diverse discipline, atleti singoli e a squadre uniti sotto vessilli nazionali, competizione che porta a un vincitore e a una classifica ma affratella e riunisce.

Parlo naturalmente dei [Polympics](https://polytopia.fun), le olimpiadi di Polytopia.

Lancio l’appello a chi giocasse abbastanza forte da poter degnamente rappresentare l’Italia. Sono un giocatore discreto ma non memorizzo, faccio esperimento, prendo strade a volte solo divertenti e non produttive, per cui non riesco a perseguire indefessamente l’obiettivo _citius_, _altius_, _fortius_.

Soprattutto, non riesco a tradurre efficacemente in italiano. Polympiadi? Polytopiadi? Olimpolypiadi? PolyGiochi? Polypiadi…?

(Il nostro gruppo di gioco, intanto, accetta sempre nuove candidature. Chi arriva ultimo lancia la prossima partita…).

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*