---
title: "Come dissipare i risparmi"
date: 2021-11-02T00:44:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Pixel, Ars Technica] 
---
*Ars Technica* riferisce dei [video sullo smontaggio dei Pixel 6 e Pixel Pro 6 di Google](https://arstechnica.com/gadgets/2021/11/pixel-6-teardown-shows-lots-of-heat-dissipation-questionable-mmwave-layout/#p3).

>Apri uno o l’altro telefono e trovi una tonnellata di dissipazione di calore. Il retro dello schermo è ricoperto di rame, per farne un grosso dissipatore. Sotto il display, l’interno è ricoperto da una pellicola appiccicosa di grafite per la dissipazione del calore, il che fa apparire il telefono clamorosamente più disordinato rispetto alla cura e all’attenzione di iPhone Pro. Sotto, c’è ulteriore dissipazione: una placca di alluminio collegata ai chip fondamentali da nastro termico.

Per confronto, si può guardare lo [smontaggio di iPhone 13 Pro da parte di iFixit](https://www.ifixit.com/Teardown/iPhone+13+Pro+Teardown/144928).

>Si può vedere una doppia dose di batterie a forma di L, che reclamano ogni millimetro di spazio disponibile.

In pratica, con un iPhone 13 si compra energia. Con un Pixel 6, calore. Buono per l’inverno, comunque.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*