---
title: "Convegnisticamente"
date: 2015-03-30
comments: true
tags: [IfBookThen, iPhone, iPad]
---
Venerdì scorso ho passato la giornata a [IfBookThen](http://www.ifbookthen.com/ibt-2015/) 2015. Dalle nove alle quattro a twittare, fotografare (cinquantatré immagini), scrivere, posta e quant’altro.<!--more-->

Poi mi sono spostato verso casa di amici dove abbiamo passato la serata con [Dungeons & Dragons](http://dnd.wizards.com). Lanci di dadi, consultazioni di Pdf, mappe interattive, chat eccetera.

Sono arrivato a casa alle due di notte, dopo essere uscito alle sette del mattino, che iPhone aveva ancora un alito di batteria e iPad stava benone.

*Just for the record*.