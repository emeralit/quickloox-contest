---
title: "Scrittura creativa"
date: 2015-02-12
comments: true
tags: [Wordpress, Mail, Ctrl-T, tastiera, Textastic, Safari, Editorial, Python, emacs, Mac, iPad]
---
In giro viaggiando leggero, con iPad e una [Wireless Keyboard](https://www.apple.com/it/keyboard/) Apple a fare le veci di Mac. Si lavora bene e tranquilli, a parte qualche bizzarria derivante dall’uso della tastiera esterna associato a quello di Wordpress.<!--more-->

Scoprire quali scorciatoie di tastiera sono attive su Mac e anche su un iPad con tastiera esterna è un gioco interessante e foriero di produttività. Per esempio, spedire una mail via tastiera da Mail su iPad è uguale a farlo su Mac; creare un messaggio di risposta invece no e va fatto a schermo. Purtroppo sembra che le scorciatoie di [emacs](http://www.gnu.org/software/emacs/) siano state tutte lasciate fuori e questo dispiace un po’ perché sono comode (se non esistesse *Control-T* per scambiare di posizione due caratteri bisognerebbe inventarlo e difatti, quando mi tocca usare un sistema dove la scorciatoia manca, cerco subito un sistema di ricrearla). 

Con Wordpress le cose si fanno davvero bizzarre perché le personalità da fare convivere sono tre: tastiera, Wordpress e programma dentro cui si accede al sistema (normalmente sono due, tastiera e programma).

Così ti rendi conto che se editi un documento Wordpress in Safari, da tastiera non si riesce a selezionare un blocco di testo. La cosa è fastidiosa per l’editing ovviamente, ma anche per assegnare tag a testo selezionato, visto che selezionare non si può.

Allora provi con la *app* [Wordpress](https://appsto.re/it/i9Mau.i) e la selezione funziona perfettamente. Però, arrivato in fondo allo schermo, il cursore scende e affonda nella parte non visibile della pagina, sulla quale il testo non scorre da solo; va fatto via *touch*. Niente di che, ma irrita. E poi rimane l’annoso problema dei campi personalizzati di Wordpress, che nessuna *app* riesce a governare da sola.

Il tutto testimonia di quanto sia giovane la piattaforma e il software relativo (Mac ha trentuno anni, iPad cinque). La soluzione è adattarsi oppure padroneggiare maggiormente la parte tecnica. Probabilmente il modo migliore per scrivere in libertà su iPad articoli destinati a Wordpress è usare [Editorial](https://appsto.re/it/UqWkO.i) e appoggiarsi alla sua parte programmabile in [Python](https://www.python.org) per interagire direttamente con il codice di Wordpress e bypassare l’interfaccia.

Oppure scrivere separatamente da Wordpress, con un editor come [Textastic](https://appsto.re/it/KWo3w.i) che con la tastiera esterna rimane un gradino sotto [BBEdit](http://barebones.com/products/bbedit/) su Mac, ma funziona molto bene. E poi copiare e incollare dentro Wordpress.

Il che però è proprio poco elegante. Se si mette creatività nello scrivere – o almeno ci si prova – vale la pena di mettere qualche creatività anche nel pubblicare.

P.S.: usando iOS in inglese, quando si scrive nel campo indirizzo di Safari con la tastiera esterna, la codifica della tastiera diventa americana, anche se la tastiera è italiana e le impostazioni di lingua sono italiane. Migliorabile.