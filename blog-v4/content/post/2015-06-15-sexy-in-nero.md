---
title: "Sexy in nero"
date: 2015-06-15
comments: true
tags: [emacs, vim, Pages, BBEdit, Control-T, OSX, Terminale]
---
In un giro di chiacchiere digitali mi è capitato di spiegare al volo le tre ragioni principali per cui (a volte) uso [emacs](http://www.gnu.org/software/emacs/) e non [vim](http://www.vim.org) come superelaboratore di testo e addentellati via Terminale.<!--more-->

* C’è dentro un [Lisp](http://www.paulgraham.com/lisp.html).
* Preferisco l’interfaccia centrata sulle combinazioni di tasti rispetto a quella che commuta la modalità di lavoro.
* OS X adotta le combinazioni di tastiera di emacs, per cui lo stesso comando su emacs può funzionare, poniamo, su Pages o BBEdit. Esempio banalissimo, il mitico Control-T per scambiare la posizione di due caratteri adiacenti.

Per chi volesse approfondire, ho scoperto che esiste la pagina [Emacs is sexy!](http://emacs.sexy), ottima per partire in fretta con poche idee molto chiare e tutte le risorse essenziali alla scoperta di emacs.

Il rischio, come sempre, è di perdere qualcosa in apparenza e ritrovarsi molto più efficienti di prima con meno ingombro, più velocità e potenza inimmaginabile.

Non c’è neanche bisogno di stare nel Terminale, dato che esistono programmi che lo incapsulano in una app classica da Mac (abbondantemente linkati nella pagina).

In nero Terminale, comunque, emacs è sexy.