---
title: "Rivali terminali"
date: 2016-09-21
comments: true
tags: [emacs, vim]
---
Guarda caso, vim annuncia una [nuova splendente versione](https://groups.google.com/forum/#!topic/vim_announce/EKTuhjF3ET0) frutto di anni di sviluppo con funzioni che per un editor di testo da Terminale sarebbe apparse un tempo mirabolanti. E emacs annuncia una [nuova splendente versione](https://fossbytes.com/emacs-25-1-text-editor-released/) frutto di anni di sviluppo dalla quale è persino possibile guardare YouTube e navigare il web, nonché usare le virgolette tipografiche mentre si scrive (non c’è niente da ridere e invito a configurarsi da soli un emacs non ancora impostato per usare Unicode; voglio vedere quanto tempo occorre prima di riuscire a digitare un’accentata).

Chi è arrivato prima, magari di un’ora? Importa poco. Sono programmi prodigiosi, vecchi di decenni e tuttora impagabili per chi sa veramente come mettere le mani sotto il cofano.

È anche una delle rivalità più vecchie in assoluto dell’informatica, se non la più vecchia. E me li vedo, a compilare il “loro” editor preferito da una parte e tenere d’occhio i progressi del rivale dall’altra, nel tentativo di arrivare prima e non lasciare neanche una funzione alla concorrenza.

Si sappia che io sto con emacs. Fa perfino i [fogli di calcolo](https://macintelligence.org/posts/2015-06-30-una-parentesi-sperimentale/).