---
title: "Un font, mille app"
date: 2020-01-24
comments: true
tags: [JetBrains, Mono, Viticci, MacStories, Menlo]
---
Da molto tempo scrivo con un font monospaziato, più neutro rispetto al contenuto e più ordinato quando mi trovo ad applicare tag o simboli, Html o Markdown per fare gli esempi più banali.

JetBrains, che produce ambienti di lavoro per gli sviluppatori software, ha prodotto [JetBrains Mono](https://www.jetbrains.com/lp/mono/), un font fatto apposta per chi scrive codice (ma anche per me: leggibilità immediata del testo e relax per gli occhi servono anche se non si programma), open source, liberamente scaricabile e usabile su desktop e web.

Poche cose sono più personali di un font e mi trovo *molto* bene con Menlo. Proverò JetBrains Mono e vedrò se mi piace sulla lunga sistanza. Ma assolutamente, che interessi o meno il font, la visita al sito bisogna farla. Presentazione semplicemente spettacolare e, per una risorsa gratuita, tanto di cappello.

Altrettanto gratuita è l’[edizione 2019 delle *must-have app*](https://www.macstories.net/stories/my-must-have-apps-2019-edition/), le app *mai più senza*, secondo Federico Viticci di *MacStories*. Totalmente da leggere e scommetto un euro contro un bottone che chiunque troverà almeno un titolo prima sconosciuto, ma ci voleva poco, e inaspettato, che invece significa di più.

Per questo mi fermo qui, non c’è nulla da riassumere perché è una storia che va colta per intero. Complimenti a Viticci e un bel fine settimana per chi ama sperimentare nuove configurazioni per il proprio *computing* personale.