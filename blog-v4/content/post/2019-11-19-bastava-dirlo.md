---
title: "Bastava dirlo"
date: 2019-11-19
comments: true
tags: [Signal, versus, Noise, Mac]
---
Le tastiere dei portatili Mac hanno esasperato *Signal v. Noise* al punto da [provare hardware e software alternativi dopo vent’anni di Mac](https://signalvnoise.com/back-to-windows-after-twenty-years/). Il risultato:

>Ciò che questo esperimento mi ha insegnato, peraltro, è quanto mi piaccia in realtà macOS. La soddisfazione che mi dà la sua resa tipografica. L’aspetto piacevole del mio codice in TextMate 2. Quanto sia facile vivere l’esistenza di uno sviluppatore *nix e comunque usare un computer dove ogni cosa (beh, eccetto quella fottuta tastiera!) nella maggior parte dei casi funziona e basta.

Le esperienze possono essere diverse, i gusti sono gusti. Ma quello che rende eccezionale il post è la chiusura, con l’invito ad Apple perché *metta a posto quelle fottute tastiere*.

Apple [lo ha fatto](https://www.apple.com/it/macbook-pro-16/) davvero. Quest’uomo è da seguire.