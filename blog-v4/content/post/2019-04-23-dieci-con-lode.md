---
title: "Dieci con lode"
date: 2019-04-23
comments: true
tags: [MacStories, Viticci]
---
Adesso che la festa ufficiale é terminata, voglio rendere omaggio al decennale di MacStories.

Si parla molto della qualità dell’informazione in rete; la verità è che bisognerebbe parlare meno e agire di più, per portare il *browser* dove si possono trovare qualità e autorevolezza e lasciare a se stessi i siti spazzatura e quelli di pubblicità con qualche contenuto negli spazi occasionalmente vuoti.

MacStories è un posto straordinario per leggere buona informazione sul mondo Apple e la [retrospettiva di questi dieci anni](https://www.macstories.net/stories/10-years-of-macstories/) lo spiega bene, anche attraverso gli impegni presi con sé e con il lettore:

- riportiamo le notizie tempestivamente ma senza affannarci.
- cerchiamo di aggiungere contesto e analisi.
- non pubblichiamo dicerie e voci di corridoio.
- scriviamo solo di app collaudate di persona.
- mai, neanche in futuro, recensioni a pagamento.
- non scriviamo titoli sensazionalistici in cerca di clic.
- se Apple ci fornisce prodotti da recensire, scriviamo comunque di quello che non ci piace, perché l’onestà è più importante dell’accesso.
- prendiamo tutto il tempo che serve per scrivere i nostri articoli.
- riconosciamo sempre le nostre fonti e siamo prodighi di link.
- lavoriamo per avere il sito più veloce possibile. Usiamo un solo servizio di analisi e non pubblichiamo inserzioni o media con codici di tracciamento sospetti.
- scriviamo il blog Apple che ci piacerebbe leggere.

Ho risssunto, ma non tagliato. MacStories è stato e continua a essere questo; in più promette di restarlo e lo fa in forma assolutamente credibile.

Ognuno di noi dovrebbe farsi l’esame di coscienza e, sapendo quanta poca attenzione possiamo dedicare agli aggiornamenti, metterla su siti che davvero ci danno indietro valore autentico.

Buon compleanno MacStories e complimenti di cuore.
