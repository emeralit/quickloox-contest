---
title: "Un business da miliardi"
date: 2019-02-27
comments: true
tags: [Asymco, Dediu, iPhone]
---
La lettura più sorprendente della settimana, anche se un po’ in ritardo, per me sarà questo [articolo di Asymco](http://www.asymco.com/2019/01/07/apples-unit-economics/) sulla struttura del conto economico interna ad Apple.

Va letto tutto; il principio base è che ogni apparecchio in vendita vada visto come una attività commerciale a sé, con il suo costo e i suoi profitti.

In questa luce, il conto delle unità vendute perde importanza e le metriche portano alla luce situazioni interessanti, come il fatto che il business complessivo sia stato fondamentalmente costante nell’ultimo triennio.

La cosa più interessante di tutte è l’idea che sia *l’azienda stessa* a valutarsi e misurarsi in questo modo. Non quanti iPhone si vendono, ma quanto valore genera ciascun iPhone (proprio non è la stessa cosa), nel grande network da un miliardo e quattrocento milioni di apparecchi attivi costruito finora.