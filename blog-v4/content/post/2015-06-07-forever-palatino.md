---
title: "Forever Palatino"
date: 2015-06-08
comments: true
tags: [Zapf, Palatino, Inconsolata, Consolas, BBEdit, Menlo, Terminale, Texstatic, Monaco, Futura]
---
Non ricordo bene che anno era, ma decisi che il “mio” font *serif*, con le grazie, era [Palatino](http://it.wikipedia.org/wiki/Palatino_Linotype).<!--more-->

(Non ho un “mio” font *sans serif*, anche se amo molto [Futura](http://it.wikipedia.org/wiki/Futura_(carattere)), e meno che mai un monospaziato: uso [Consolas for BBEdit](http://en.wikipedia.org/wiki/Consolas) in [BBEdit](http://www.barebones.com/products/bbedit/), [Menlo](http://en.wikipedia.org/wiki/Menlo_(typeface)) nel Terminale, [Inconsolata](http://www.levien.com/type/myfonts/inconsolata.html) su [Textastic per iPad](https://itunes.apple.com/it/app/textastic-code-editor-for/id383577124?l=en&mt=8) e qua è là applico un buon vecchio [Monaco](http://en.wikipedia.org/wiki/Monaco_(typeface))).

È ancora Palatino e continuerà a esserlo anche se il suo autore è stato chiamato a progettare font per qualcuno [più in alto di noi](http://qz.com/421855/hermann-zapf-the-font-designer-behind-palatino-and-zapf-dingbats-has-died-at-96/).

<p style="font-family:Palatino Linotype">Auf wiedersehen, Hermann.</p>