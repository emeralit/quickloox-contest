---
title: "Quei piccoli particolari"
date: 2022-12-13T02:03:12+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [emacs, BBEdit]
---
Anche se BBEdit non arriva a queste [vette](https://twitter.com/nixcraft/status/1602348715830022144) di genio assoluto, ha pur sempre i worksheet!

Quei piccoli particolari che fanno la differenza.

<blockquote class="twitter-tweet"><p lang="zxx" dir="ltr"><a href="https://t.co/PmnBrTRt3Q">pic.twitter.com/PmnBrTRt3Q</a></p>&mdash; nixCraft 🐧 (@nixcraft) <a href="https://twitter.com/nixcraft/status/1602348715830022144?ref_src=twsrc%5Etfw">December 12, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>