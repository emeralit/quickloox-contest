---
title: "A piena pagina"
date: 2022-11-27T18:21:00+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [OSX Daily, webkit2png]
---
Oggi svolgiamo attività di servizio a favore di chi, come il sottoscritto, si è tolto da un impiccio grazie al comando Unix `webkit2png` che permette di registrare la schermata di una pagina web; *tutta* la pagina web, anche se è molto più lunga della porzione che lo schermo è in grado di mostrare.

*OSX Daily* giusto oggi ricorda di [installare webkit2png via Homebrew](https://osxdaily.com/2022/11/27/take-full-webpage-screenshots-on-mac-via-command-line-with-webkit2png/) (non avevo idea di come si trovasse su Mac) e poi si scrive

`webkit2png -F URL`

dove *URL* è l’indirizzo della pagina. Tutta la pagina.