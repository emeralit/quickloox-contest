---
title: "Quando il pregiudizio chiama"
date: 2016-07-11
comments: true
tags: [Rescuecom]
---
Ci sono i pregiudizi, tipo che i Mac non hanno più la qualità di una volta, oppure che un PC fa tutto quello che può fare un Mac e costa meno.<!--more-->

Poi ci sono i dati. Nella sua ultima [raccolta](http://www.rescuecom.com/blog/index.php/rescuecom/understanding-the-top-tech-problems-and-how-to-fix-them-rescuecoms-2016-first-half-computer-repair-report/) Rescuecom, organizzazione specializzata nella riparazione dei computer precisa chiamata telefonica dell’avente problemi, riferisce che le richieste di soccorso riguardanti Mac sono appena lo 0,7 percento del totale.

Certo, è probabile che moltissime ricerche di aiuto riguardanti Mac passino da un Apple Store prima di arrivare a una Rescuecom. Stiamo comunque parlando di un dato dieci volte inferiore a quello della quota di mercato.

E se fosse vero che i Mac si guastano come i PC, ma risolvono via Apple Store, ecco trovato qualcosa che i PC non possono fare: approfittare di un supporto dedicato.

Se invece ci fosse una superiorità di efficienza come pare da questo dato, beh, certo un PC può fare tutto quello che può fare un Mac. Non quando è guasto, però, e intanto il Mac funziona.

*[La maggiore efficienza di Mac e dintorni non rende inutile l’esistenza di un progetto più-che-editoriale come [quello di Akko](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/). Che anzi, [merita fiducia](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac) in quanto permette di passare al livello successivo: sfruttare meglio la macchina e acquisire un reale vantaggio competitivo.]*