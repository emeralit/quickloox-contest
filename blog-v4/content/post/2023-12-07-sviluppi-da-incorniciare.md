---
title: "Sviluppi da incorniciare"
date: 2023-12-07T03:59:37+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [MLX, ai, intelligenza artificiale, Apple Silicon, MIT]
---
La Apple indietro sull’intelligenza artificiale, chiamiamola così per sopraggiunta stanchezza, invece di promettere l’universo o far pagare venti euro al mese per tutto il *machine learning* presente nei suoi sistemi operativi, sviluppa [MLX](https://github.com/ml-explore/mlx) senza dire niente a nessuno o quasi.

MLX è un framework (architettura, struttura, cornice, impalcatura, base di supporto) per installare e addestrare modelli di *machine learning* su macchine Apple Silicon ed è stato scritto dalle stesse persone che lavorano in Apple sul *machine learning* per avere a disposizione uno strumento sufficientemente potente e abbastanza semplice da utilizzare senza troppe complicazioni.

Il tutto con [licenza MIT](https://github.com/ml-explore/mlx/blob/main/LICENSE), sicuramente compromissoria dal punto di vista dell’utilizzo da parte di aziende orientate al profitto, ma ancora *open source*. Cioè a disposizione di tutti senza impegni e senza obblighi.

Pur lavorando nell’ombra e facendosi sbertucciare da mezzo mondo mediale, qualcosa lo mandano avanti pure loro.