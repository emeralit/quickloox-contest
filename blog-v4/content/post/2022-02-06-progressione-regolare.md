---
title: "Progressione regolare"
date: 2022-02-06T00:49:42+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Una bellissima [lezione di Dr. Drang](https://leancrew.com/all-this/2022/02/wordle-and-grep/) per imparare la base delle espressioni regolari, a partire da uno spunto semplicissimo originato da un [gioco di parole piuttosto in voga](https://macintelligence.org/posts/2022-01-12-un-bel-wordle-dura-poco-ma-apre-un-mondo/).

Un elenco di parole con identico numero di lettere, una sequenza iniziale particolare, una domanda: dopo il primo tentativo, c’era una sola soluzione oppure anche altre?

Drang ci arriva una lettera per volta e, nel farlo, costruisce una espressione regolare da principianti, seppure pregnante. Da principianti non per qualità, ma per il contenuto: le cose basilari che si apprendono nel raggruppare caratteri e cercarli in un testo. Difficile spiegarlo con più immediatezza.

Piccolo spoiler: la soluzione non era unica, al contrario della leggerezza e della chiarezza cristallina dell’esposizione di Drang.

La forza dell’[Internet di una volta](https://macintelligence.org/posts/2022-01-17-l-internet-di-una-volta/).
