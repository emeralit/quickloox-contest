---
title: "Se manca Time Machine"
date: 2013-08-05
comments: true
tags: [OSX, Mac]
---
Compiute tutte le prove che mi sono venute in mente, direi che il disco rigido portatile FireWire 800 My Passport Western Digital da 500 gigabyte che rappresentava Time Machine è defunto.<!--more-->

Purtroppo non ho notizie certe sulla data del suo acquisto, che però potrebbe essere attorno a cinque anni fa.

Provato a cambiare cavo, spianare il disco tre o quattro volte, dimenticarlo e reimpostarlo come disco Time Machine, verificarlo e riverificarlo, rifare tutto da capo dopo avere anche azzerato la Ram parametrica di Mac, niente: Time Machine parte e si blocca irreversibilmente dopo qualche gigabyte.

Ho anche sospettato un problema software – dopotutto sono sulla Developer Preview 4 di Mavericks – solo che, su un altro disco rigido, un esperimento di backup iniziale con Time Machine è andato perfettamente. Quindi il software fa egregiamente il proprio lavoro.

Probabilmente cercherò ancora un disco portatile – meno ingombro, meno cavi – sempre con interfaccia FireWire 800, così le Usb restano libero per altro, e sono curioso di vedere se riesco a trovarlo agevolmente fuori da Internet (in questo periodo di viaggi farsi spedire pacchi è a rischio mancata coincidenza con il corriere).

La cosa veramente da notare è come un pasticcio su Time Machine riesca a compromettere il funzionamento di Mac. Ci ho messo un po’ a trovare il colpevole, perché succedeva di tutto, da iTunes che appariva nel Dock senza mostrare la sua finestra a BBEdit che si rifiutava di salvare a InDesign bloccato fino al Terminale (!) inaccessibile. Il Terminale è *refugium peccatorum*, l’ultima cosa che ragionevolmente può arrendersi prima che ci sia solo da riavviare. Ho cercato per qualche giorno la causa dentro Mac prima di rendermi conto che si trovava fuori, quando un giorno di lavoro a Time Machine intenzionalmente e sperimentalmente staccato è andato liscio senza il minimo problema.

Alla fine ho trovato un messaggio criptico in Console che annunciava un errore di *input/output* su un disco che corrispondeva a una unità esterna. Suscitata l’interfaccia di Time Machine, ho finalmente constatato che tutto era bloccato.

Magari l’esperienza può servire ad altri, sai mai.