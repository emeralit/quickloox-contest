---
title: "Sento lo sgretolarsi"
date: 2013-12-14
comments: true
tags: [Monaco, LibreOffice, app, Google, Mac]
---
Poco tempo fa si parlava di Google e dei suoi [quarantamila Mac](https://macintelligence.org/posts/2013-12-01-quarantamila-mac-in-ufficio/) con cui magicamente riesce a operare come un’azienda multimiliardaria.

Continuano intanto a succedere cose. La città di Monaco di Baviera ha impiegato dieci anni per [eliminare Windows dalla propria infrastruttura](http://www.cio.co.uk/news/change-management/munich-open-source-completed-successfully/), con un risparmio di oltre dieci milioni di euro. Fatica che vale lo sforzo.

App Store per iOS ha superato il milione di *app* attive.

<blockquote class="twitter-tweet" lang="en"><p>[BREAKING] The <a href="https://twitter.com/AppStore">@appstore</a> just reached 1 MILLION live apps (473k optimized for iPad)! (from 1 million approved in nov/2012)</p>&mdash; Appsfire (@appsfire) <a href="https://twitter.com/appsfire/statuses/409352589055369216">December 7, 2013</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Ci sarà magari anche qualcosa che serve ai professionisti, in un milione di programmi. Una cifra che Windows neanche si può sognare.

Nel primo mese pieno di presenza, Mavericks [ha preso la maggioranza assoluta](http://thenextweb.com/apple/2013/12/02/os-x-mavericks-hits-2-42-market-share-passing-predecessors-just-one-full-month-availability/#!pRX80) tra gli utilizzatori di Mac.

Quelli che ripetono stancamente le formule di sempre, sappiano che non è più come ai loro tempi. Le vecchie mura stanno venendo giù.