---
title: "Prova d’amore"
date: 2020-09-22
comments: true
tags: [Jobs, Powersharing, Gates]
---
Retrocomputing, Ok. Storia di Apple, Ok. Anni ruggenti, Ok.

Ma è nostalgia del vigore psicofisico del tempo che fu o vero amore per un’epoca irripetibile e sì, visto il seguito, autenticamente rivoluzionaria?

Il *litmus test* è costituito da [questo audiobook](https://www.amazon.com/Powersharing-People-Computers-Complete-Digital/dp/1733832602/ref=sr_1_1?dchild=1&keywords=powersharing+series+complete+digital+edition&qid=1600735366&sr=8-1): centotrentaquattro presentazioni per duecento ore di audio e sedici ore di video dei grandi dell’informatica degli anni ottanta, a partire da Steve Jobs ovviamente. Ci sono anche personaggi chiave per quanto meno alla ribalta, come Alan Kay, e personaggi protagonisti anche se ne avremmo fatto volentieri a meno, come Bill Gates.

Solo cinquantanove dollari e novantacinque, solo Amazon.com. Su Amazon.it si trova unicamente la [guida al contenuto](https://www.amazon.it/Powersharing-Computers-Resource-Complete-Digital/dp/1733832610/ref=sr_1_1?__mk_it_IT=ÅMÅŽÕÑ&dchild=1&keywords=the+powersharing+series&qid=1600735318&sr=8-1), dodici euro.

Personalmente, interesse distaccato. Ne accennerò a babbo Natale.