---
title: "Il verdetto invertito"
date: 2016-04-12
comments: true
tags: [Re/Code, watch, wank, Parker]
---
Tranciante, *Re/Code*: [Un anno dopo, il verdetto su Apple Watch: metà degli intervistati pensa sia una ciofeca](http://recode.net/2016/04/11/apple-watch-fluent-survey/).<!--more-->

Il sondaggio è stato condotto da Fluent, una agenzia di comunicazione per il marketing che per comodità e definizione immaginiamo competente e capace, qualsiasi cosa dica. Così semplifichiamo.

Fluent titola [Apple Watch, un anno dopo: qual è il verdetto?](http://blog.fluentco.com/apple-watch-1-year-later-whats-the-verdict) Sembra proprio che *Re/Code* abbia preso delle iniziative.

Il *post* parte da un anno fa, alla vigilia del lancio di watch, quando Fluent condusse un’indagine ([L’orologio sarà il primo flop di Apple?](http://blog.fluentco.com/will-the-watch-be-apples-first-flop/), bilanciatissima dalla partenza) da cui risultava che l’aggeggio si trovava nelle intenzioni di acquisto di circa il cinque percento degli americani adulti, approssimativamente 12,5 milioni di persone. Un dato che spinse l’autore del *post* a cambiare posizione e nutrire ottimismo verso l’affermazione dell’oggetto.

Oggi hanno verificato quell’ipotesi. L’otto percento del campione dichiara di possedere un watch.

Dall’indagine emerge che solo l’11 percento degli acquirenti ha comprato per moda e che gli altri utilizzano con buone percentuali notifiche, posta, chat, funzioni di fitness e benessere, ascolto di musica, insomma tutto quello che fa di watch un computer da polso e non un semplice orologio. Persone che lo usano pienamente per quanto può offrire. Tre su cinque lo usano con Apple Pay, cioè pagano avvicinando watch al sensore.

Il 77 percento dei possessori di watch è assolutamente entusiasta; la metà esatta degli intervistati ritiene che la maggioranza degli americani avrà un computer da polso entro dieci anni da oggi; la stessa cosa pensano tre quarti dei proprietari di watch.

Il 47 percento degli intervistati ritiene che watch sia un prodotto di successo. Esatto, questo scrive Fluent, assieme a tutto quanto sopra. Ma è sufficiente a *Re/Code* per capovolgere l’affermazione – se uno si è occupato di sondaggistica per dodici secondi, sa che manca qualunque appiglio per farlo – e inventarsi il proprio verdetto.

Il guaio è che le cifre pubblicate da Fluent consentono di esercitare piccola aritmetica da terza elementare. Il 92 percento degli interpellati non ha watch.

Quindi il titolo di *Re/Code* si potrebbe tranquillamente riscrivere come *Più di metà degli americani senza Apple Watch pensa che sia un insuccesso*. E allora vabbeh, in uno studio ancora precedente la volpe concluse che l’uva era acerba.

P.S.: Mi spiace, [Massimo](https://www.facebook.com/mmoruzzi/posts/10208947044064277): stavolta il *wank* lo fa l’altrimenti fantastico [George Parker](http://adscam.typepad.com/my_weblog/2016/04/its-official-the-apple-watch-is-a-wank.html). :-)