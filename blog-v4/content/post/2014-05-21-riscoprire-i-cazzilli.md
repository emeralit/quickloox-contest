---
title: "Riscoprire i cazzilli"
date: 2014-05-22
comments: true
tags: [Rubik, CubeTwister, widget, Dashboard, Techradio, DynamicRubikFree]
---
Giorni fa si è celebrato il quarantesimo anniversario del [cubo di Rubik](http://eu.rubiks.com) e ha fatto la sua parte [anche Google](http://www.google.com/doodles/rubiks-cube).<!--more-->

Nel parlarne in chiave Apple per un prossimo episodio di [Techradio](http://www.techradio.it) ho scaricato [CubeTwister per Mac](http://www.randelshofer.ch/cubetwister/), 7,3 megabyte di scaricamento. Su iOS ho trovato [DynamicRubikFree](https://itunes.apple.com/it/app/dynamicrubikfree/id593281517?l=en&mt=8), gratis, 18,1 megabyte.

Poi c’è [Rubik Unbound](http://www.svenstoll.de/projekte/rubik.html), un *widget* (*cazzillo*) di Dashboard a soli 145 chilobyte. Tre ordini di grandezza sotto, senza un divario paragonabile nelle prestazioni.

Forse è il momento di riscoprire Dashboard su Mac, sempre messo in un angolo dalle recenti versioni del sistema. Penso a tanti Mac veterani che non hanno le risorse di quelli in vendita oggi e potrebbero giovarsi moltissimo di un uso accorto di Dashboard.

Che certo non può fare tutto e non raggiunge livelli di sofisticazione di un’applicazione professionale. Eppure i *widget* ancora oggi scaricabili [sono quasi quattromila](http://www.apple.com/downloads/dashboard/) e di materiale utile se ne trova senza faticare.

Non c’è da farsi illusioni: al momento il futuro di Mac va in tutt’altra direzione. Sul presente di tanti Mac provenienti dal passato, però, rifletterei.