---
title: "Da strabuzzare le orbite"
date: 2022-09-03T18:11:55+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [AirDrop, Singapore Airlines]
---
Negli anni novanta ci esaltavamo – anche con qualche incoscienza – con una rete Wi-Fi tirata tra due auto affiancate in autostrada (e in movimento).

Ma le nostre bravate impallidiscono rispetto ai piloti di linea che [hanno fatto AirDrop tra due aerei in volo](https://twitter.com/ilove_aviation/status/1565329923715260417).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">747-8 Captain airdrops pics with Singapore Airlines 777 pilots midair <br><br>📸 Loub747 <a href="https://t.co/o1nCOA5XVP">pic.twitter.com/o1nCOA5XVP</a></p>&mdash; Airplane Pictures ✈️ (@ilove_aviation) <a href="https://twitter.com/ilove_aviation/status/1565329923715260417?ref_src=twsrc%5Etfw">September 1, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Qualcuno avvisi [Samantha Cristoforetti](https://twitter.com/AstroSamantha) che ci sono ancora record da battere. Bisogna solo incrociare due orbite.