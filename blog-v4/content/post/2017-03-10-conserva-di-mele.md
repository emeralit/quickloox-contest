---
title: "Conserva di mele"
date: 2017-03-10
comments: true
tags: [iCloud, Aws, Amazon, Google]
---
Risposta che devo a **avariatedeventuali** dopo essere stato esortato a chiedere ad Apple [che servizio di cloud utilizzi](https://macintelligence.org/posts/2017-02-20-gli-asparagi-e-l-immortalita-del-cloud/) (e di conseguenza dedurne l’affidabilità).

Non l‘ho chiesto ad Apple ma ho trovato chi ha fatto la cosa giusta, ossia [ha analizzato il traffico di rete](https://cloudierthanthou.wordpress.com/2016/02/29/apples-icloud-is-a-multi-cloud-beast/) per sapere dove vanno a finire i dati per iCloud.

Viene fuori che Apple fa uso di più servizi, in modo non del tutto spiegato; tuttavia sembra chiaro che l’immagazzinamento vero e proprio dei dati avvenga su spazio comprato da Amazon e quindi, come da specifiche di quest’ultima, la durata nel tempo sia stimabile a nove decimali (99,999999999 percento) con possibilità di accesso a due decimali (99,99 percento). Gli Amazon Web Services, da questo punto di vista, offrono il meglio che esista sul pianeta.

Rimangono ancora cose da chiarire (che cosa venga esattamente salvato sul cloud di Google, e se e quanto venga usato di cloud Microsoft), ma in tema di solidità del servizio, i dati che finiscono su iCloud si conservino al massimo delle possibilità umane attuali.
