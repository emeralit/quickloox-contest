---
title: "Non tutto il male"
date: 2021-03-31T00:51:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Wwdc, Apple, Mac, Arm, iPadOS macOS, watch, tv] 
---
La prossima edizione di Wwdc [sarà ancora interamente online](https://www.apple.com/newsroom/2021/03/apples-worldwide-developers-conference-is-back-in-its-all-online-format/) oltre che auspicabilmente piena di spunti interessanti. Le nuove versioni dei sistemi operativi, onestamente, sono eventi meno significativi che non un tempo, quando le versioni principali erano meno frequenti e i sistemi stessi meno maturi. macOS esiste da vent’anni, iPadOS da undici; tutti ci auguriamo migliorie, *bug fix* e sorprese grandi e piccole, ma difficilmente qualsiasi nuova possibilità del software a bordo di iPhone o Mac scuoterà il mondo.

La transizione dei processori Mac a Arm attende nuove macchine e nuovi *system-on-chip*. Sarebbe una delusione per tutti se venissero tradite le aspettative e quindi si può legittimamente sospettare che Apple avrà fatto di tutto e di più per non tradirle.

Poi c’è watch, poi c’è tv, poi c’è il fatto che tutti i *memoji* sulla grafica dell’annuncio hanno gli occhiali e questo scatena le fantasie di chi aspetta *one more thing* in tema di realtà aumentata, insomma, c’è qualcosa di stimolante veramente per ognuno, fino alla Swift Student Challenge per i giovanissimi.

La notizia principale, non me ne vogliano i commentatori dei siti acchiappaclic, è però oggi il format online. Tutti si spera di tornare presto a una normalità che non potrà essere esattamente come prima e che, però, potrebbe essere ragionevole, mentre ora non lo è.

Wwdc era però un evento esclusivo, accessibile a poche migliaia di sorteggiati disposti a scodellare dall’Italia tre o quattromila euro di iscrizione, aereo, vitto e alloggio, autonoleggio, varie ed eventuali.

Questa Wwdc rimane un evento esclusivo per i contenuti (Apple ha anche rimesso mano di recente alla app [Developer](https://apps.apple.com/it/app/apple-developer/id640199958?l=it), non per coincidenza); tuttavia, come l’anno scorso, è aperta veramente a *chiunque* disponga di connessione decente e tempo da investire.

Milioni di partecipanti invece di migliaia. Nei dolori e nei problemi a volte inenarrabili che il virus continua a lasciare in giro, corresponsabili governanti inetti e cittadini sconsiderati, questo effetto collaterale è una piccolissima eppure concreta buona notizia.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*