---
title: "Momenti di transizione"
date: 2020-11-09
comments: true
tags: [Intel, Apple, Silicon, Arm, Transmeta, Motorola, Ibm, Tag, PowerPC, mini]
---
Quindici anni fa scrissi che Mac non avrebbe adottato processori Intel, il giorno prima che lo facesse. Il mio ragionamento era corretto, solo che mi mancavano informazioni tecniche essenziali di cui ero colpevolmente all’oscuro.

Il giorno prima che Mac adotti di fatto processori Arm mi guardo bene dal negarlo; sia perché anche stavolta il ragionamento è corretto, sia perché – dopo un annuncio ufficiale a giugno – nulla resta da indovinare, al massimo quali saranno le prime macchine ad adottare la nuova architettura.

Questo è un culmine di un [lungo percorso](https://macintelligence.org/blog/2017/06/19/padroni-del-silicio/) iniziato da Apple proprio per non ritrovarsi nella situazione di vedere le proprie fortune determinate da altri, come stava per succedere con i processori PowerPC che il consorzio formato con Ibm e Motorola aveva difficoltà a fornire con specifiche e prestazioni che fossero all’altezza degli omologhi processori Intel. Adesso le parti si sono capovolte e a faticare nel tenere il passo è proprio Intel.

Apple sta per assumere il controllo di un componente fondamentale del proprio hardware e, se resterà indietro, lo farà sotto la propria responsabilità invece che di altri.

Attendo curioso gli annunci e per il momento mi tengo stretto il mio Mac mini Intel, che ho zero ragioni per voler cambiare. Per tanti potrebbe arrivare qualche succosa tentazione natalizia.

Visto che avevo sbagliato clamorosamente la previsione della vigilia nel 2005, è giusto cercare di sbagliarne un’altra oggi: azzardo che verranno presentati i Mac Apple Silicon e… *one more thing*, qualcosa d’altro. Che potrebbero essere gli Apple Tag di cui si vocifera da tempo.

Se sbaglierò, pazienza. So da un bel po’ di cavarmela meglio a capire che a predire. Buona transizione a tutti.