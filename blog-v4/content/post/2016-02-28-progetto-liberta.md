---
title: "Progetto libertà"
date: 2016-02-28
comments: true
tags: [LibreItalia, LibreOffice, ProjectLibre]
---
Ha assolutamente niente a che vedere con il gran lavoro degli amici di [LibreItalia](http://www.libreitalia.it) Su [LibreOffice](http://www.libreoffice.org), eppure mi suona interessante.<!--more-->

Perché [ProjectLibre](http://www.projectlibre.org) si propone, parole loro, di *sostituire Microsoft Project sulla scrivania* ed è software *open source*.

In più hanno anche un progetto ProjectLibre Cloud, per fare, sempre parole loro, *a MS Project quello che Google Documenti ha fatto a Word*.

Il primo è gratis, il secondo a pagamento, entrambi su una buonissima strada. Meno Microsoft significa meno massificazione, meno conformismo, meno stagnazione. Project è una piaga minore, ma non per questo meno purulenta. Meno Microsoft, più libertà.