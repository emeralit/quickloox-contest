---
title: "Quante storie questi editori"
date: 2015-02-21
comments: true
tags: [Venerandi, Tombolini, Cunietti, Login, coworking, cloud, editoria, inthecloud]
---
Giovedì ho passato il pomeriggio a Login, *coworking* milanese, dove si sono alternati vari protagonisti dell’editoria e del *cloud computing* a investigare e raccontare alleanze possibili tra un settore tradizionale e autocontenuto e una disciplina informatica dirompente che costringe all’apertura e alla collaborazione.<!--more-->

Gente come Fabrizio Venerandi, Antonio Tombolini, Mariano Cunietti meriterebbero una settimana più che un pomeriggio di attenzione. Tuttavia ce li siamo fatti bastare e la giornata è stata assai interessante.

Per una volta non tento la sintesi ma inserisco la cronaca via Twitter di tutta la manifestazione, realizzata via Storify. Vediamo se e come funziona.

<div class="storify"><iframe src="//storify.com/loox/editoria-in-the-cloud-a-login/embed?border=false" width="100%" height="750" frameborder="no" allowtransparency="true"></iframe><script src="//storify.com/loox/editoria-in-the-cloud-a-login.js?border=false"></script><noscript>[<a href="//storify.com/loox/editoria-in-the-cloud-a-login" target="_blank">View the story "Editoria in the Cloud a Login" on Storify</a>]</noscript></div>