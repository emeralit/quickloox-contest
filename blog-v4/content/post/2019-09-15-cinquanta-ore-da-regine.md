---
title: "Cinquanta ore da regine"
date: 2019-09-15
comments: true
tags: [Spiderweb, Queen's, Wish, Conqueror, Vogel, GamesBeat]
---
[Queen’s Wish: The Conqueror](https://www.spiderwebsoftware.com/queenswish/index.html) è finalmente uscito dopo mesi di attesa e lo si può solo consigliare.

Tutti i giochi di Spiderweb Software sono vecchio stile nella grafica e nei media, inutile avere casse Dolby Surround o mouse da competizione; quanto ai requisiti di sistema, gira anche su quel vecchio Mac lasciato in eredità dal nonno, che accendiamo solo per vedere se si accende ancora.

La trama, invece; la profondità dell’albero delle scelte possibili; i dialoghi, gli intrighi, i colpi di scena portano sempre qualcosa di inaspettato e si finisce per rimanere agganciati.

Jeff Vogel, lo sviluppatore dietro Spiderweb, reclamizza cinquanta ore di gioco per arrivare in fondo, a un prezzo di venti dollari, che spalanca le porte di un mondo ancora più esteso dei precedenti, tanto geograficamente quanto in magia, equipaggiamenti, ricchezza di gioco.

Vogel ha concesso a *GamesBeat* una [bella intervista](https://venturebeat.com/2019/06/04/queens-wish-the-conqueror-interview-jeff-vogel-wants-you-to-be-a-fantasy-ceo/) a giugno nella quale racconta delle novità del gioco – che contiene aspetti inediti rispetto alla tipica produzione Spiderweb – e di come ha lavorato per arrivarci. Leggerla è farsi un favore, comprare Queen’s Wish è il gusto di mettersi al servizio della regina (forse…) per cinquanta ore che veramente pochi altri riescono a valorizzare nello stesso modo.

E se non interessano i giochi di ruolo a turni, sicuramente c’è un amico cui invece sì, in attesa di un bel regalo a prezzo ridicolo. Facciamolo contento.

(Su iOS bisogna aspettare ancora un pochino, ma certamente si fa in tempo per Natale. Mancasse un’idea).
