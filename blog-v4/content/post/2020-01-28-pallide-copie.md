---
title: "Pallide copie"
date: 2020-01-28
comments: true
tags: [Kennedy, Nist, ArsTechnica, Einstein]
---
[L’equazione più famosa di Albert Einstein](https://www.space.com/36273-theory-special-relativity.html) mostra come una piccola massa sia equivalente a una quantità immensa di energia.

Ci vorrebbe una equazione analoga che mostri la relazione tra una piccola massa e i dati che si possono ricavare dalla sua digitalizzazione. Nel caso dei [proiettili che hanno ucciso John Kennedy](https://www.nist.gov/news-events/news/2019/12/kennedy-assassination-bullets-preserved-digital-form) il lavoro per arrivare a una digitalizzazione il più precisa possibile ha prodotto secondo *ArsTechnica* la bellezza di [trecentosessanta gigabyte di dati](https://arstechnica.com/science/2019/12/nist-digitized-the-bullets-that-killed-jfk/), che presto diventeranno pubblici nei [National Archives](https://www.archives.gov/research/jfk) statunitensi.

I dati permetteranno di rilasciare stampe 3D di quello che è rimasto dei proiettili a storici, studenti e ricercatori e nel contempo preservare gli originali nelle condizioni più costanti di umidità e temperatura.

Qual è la risoluzione adeguata per un progetto di digitalizzazione? E per un progetto come questo? Per i proiettili di Kennedy il lavoro di scansione ha portato a una risoluzione orizzontale di quattro micrometri, *un decimo dello spessore di un capello*, e una verticale di 0,5 micrometri, un ottantesimo.

Se qualcosa di tutto questo può darci da pensare è forse che la digitalizzazione come la intendiamo nel linguaggio comune, infilare un foglio nello scanner a trecento punti e trovare il file un po’ ingombrante, non è che la pallida copia del reale, una approssimazione la cui entità ci pone all’inizio della storia, anzi, della preistoria del digitale.

La seconda cosa è che ogni digitalizzazione è un campionamento; il reale che si trova tra un punto e l’altro è perso per sempre. Rendere questa perdita sopportabile, o superarci tecnologicamente per superare il problema, è un’altra grande sfida per questo secolo.