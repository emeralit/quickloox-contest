---
title: "Il rovescio alla riparazione"
date: 2023-09-22T18:13:36+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Pixel Watch, The Verge, Victoria Song]
---
Non fai in tempo a parlare di [Pixel Tablet messi in vendita giusto per smaltire componenti](https://macintelligence.org/posts/2023-09-17-a-chi-serve/) che *The Verge* riporta [l’assenza di opzioni di riparazione in caso di danni allo schermo di un Pixel Watch](https://www.theverge.com/23874281/google-pixel-watch-cracks-no-repairs-warranty).

Emerge dal pezzo che la garanzia rilasciata da Google (o forse Alphabet, non ho approfondito) copre pressoché nulla. Non ci sono estensioni di garanzia, non ci sono centri di supporto. Le possibilità a disposizione sono, testuale dal pezzo, trovare un altro Watch su eBay (!) oppure imbarcarsi nella procedura di sostituzione descritta dai consueti eroi di iFixit.

Con l’avvertenza che lo schermo di Pixel Watch ha un design a cupola circolare, soggetto a creparsi o rompersi con relativa facilità.

Non viene in mente che l’abbaiare sulla scarsa riparabilità degli apparecchi Apple possa essere una fotografia del migliore dei mondi possibili, almeno attualmente. Resta che il diritto alla riparazione interpretato come *arrangiati e spera* sembra un po’ estremo come concetto.