---
title: "Ci vuole orecchio"
date: 2022-08-19T02:28:50+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Mobile Fidelity, MoFi, SoundStsge, Jannacci, Enzo Jannacci, Ci vuole orecchio]
---
Semplicemente delizioso lo scandalo Mobile Fidelity (MoFi): casa produttrice di edizioni discografiche di altissima qualità in vinile, che ha sempre vantato un processo produttivo interamente analogico, [da vent’anni applica un passaggio in digitale](https://www.soundstageglobal.com/index.php/blogging-on-audio/278-matt-bonaccio/1021-the-needle-and-the-damage-done-mobile-fidelitys-dsd-scandal). Mai dicniarato. (Da domani, comunque, comunicato stampa, massima trasparenza).

La spiegazione sta nel classico adagio *follow the money*: nel dichiarare una produzione cento percento analogica si potevano giustificare edizioni limitate da vendere a prezzi molto alti, con il motivo di preservare la qualità del master.

Se invece c’è un passaggio in digitale, una edizione limitata per queste ragioni diventa una scusa per fare più soldi. Eppure questa è la parte più noiosa e banale della storia.

Perché sussiste, deliziosamente, un piccolo esercito di clienti di MoFi furioso per la rivelazione, che adesso fa causa alla società, stronca edizioni prima tenute in gran conto, minaccia boicottaggi e via dicendo.

Audiofili autodichiarati di alto rango, in prima linea a sostenere la superiorità audio del vinile e dell’analogico, che *in vent’anni non si sono accorti del trattamento digitale di quello che ascoltavano*.

Gli si dedica commossi uno [Jannacci di annata](https://www.youtube.com/watch?v=8bLTWrsuA1k).

<iframe width="560" height="315" src="https://www.youtube.com/embed/8bLTWrsuA1k" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>