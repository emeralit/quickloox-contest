---
title: "Avere o essere"
date: 2020-12-18
comments: true
tags: [Unreal, Strassburger, iPhone, 3D, Yoda, attca]
---
Stavo intitolando il post *Un cortone animato*, con il pretesto di questo [minifilm da sette minuti](https://www.youtube.com/watch?v=nyq3mfY9jD4) che mi ha segnalato [@attca](https://twitter.com/attca) (mille grazie).

La cosa grossa (per quello, *cortone*) è che si tratta di [computergrafica realizzata da una sola persona, con un iPhone X e materiali di repertorio](https://www.redsharknews.com/the-future-of-animation-production-this-is) reperibili con spesa modica sullo store del motore grafico Unreal.

L’intera produzione è costata pochi soldi e quindici giorni di lavoro all’autore, che gli ha dedicato alcuni fine settimana.

Immagino che si sarebbe potuto fare lo stesso con apparecchi diversi da un iPhone.

È più interessante presa dal lato opposto: se *sei* una persona di un certo tipo, allora è più facile che lavori con un iPhone. La storia dei pioli tondi nei fori quadrati.

Sulla qualità artistica del copione non ho titoli; invece raccomando i titoli di coda, dove si vede l’autore prestare corpo e voce alla videocamera per animare i personaggi in computergrafica. Cose che si possono fare, oggi, in cameretta quando ieri serviva uno studio di produzione.

Anni fa scrivevo che la stampa 3D non avrebbe sfondato presso il grande pubblico perché mancavano l’equivalente di Macintosh, qualcosa che facilitasse al massimo l’approccio, e l’App Store, qualcosa che semplificasse al massimo il reperimento di oggetti 3D pronti da usare.

Qui si vede come siamo vicini a un *tipping point*, un punto di svolta, in un campo adiacente: vuoi creare un film animato? Puoi trovare un sacco di roba già pronta ed esiste software che ti ci fa lavorare in modo semplice.

Nei prossimi anni potremmo vedere cambiamenti grossi a livello di tecnologie 3D, a due e tre dimensioni, dopo tanti anni di pionierismo.

I primi ad arrivarci e a creare cose che ancora fatichiamo a immaginare non saranno persone che hanno un iPhone, bensì persone che sono tipi da possederne uno. Fa gran differenza e mette un pizzico di orgoglio, nonché di pepe dietro la schiena; l’ostacolo alle nostre ambizioni, spesso, è solo un nostro pregiudizio su noi stessi. Liberiamoci e mettiamo al lavoro quell’iPhone, che non vede l’ora.

<iframe width="560" height="315" src="https://www.youtube.com/embed/nyq3mfY9jD4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
