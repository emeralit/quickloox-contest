---
title: "Il codice, il codex, il coding"
date: 2017-07-22
comments: true
tags: [Venerandi, Hamurabi, Apple, Basic, JavaScript, Swift, coding]
---
Che bella l’operazione di Fabrizio Venerandi: prendere un vecchio gioco digitato pazientemente per il Basic di Apple ][ e riadattarlo in JavaScript a una [qualsiasi finestra del browser](http://www.venerandi.com/HAM/index.html).<!--more-->

Non sono capace di esprimere il portato culturale, storico, intellettuale e passionale di quello che ha fatto in forma più sintetica di quella usata da lui, ergo gli passo la parola:

>La cosa affascinante di questa cover, è quella del tramandare in qualche modo una idea di codice nata e conservata per cinquant'anni. Pensare al codice non come a qualcosa di strutturale e meccanico, ma come a un codex, un impianto di intelligenza pari a un sonetto, una poesia.

Era già un codice, di altro tipo, quello di [Hammurabi](https://en.wikipedia.org/wiki/Hammurabi).

Ecco. Bene la [nostalgia per HyperCard](https://macintelligence.org/posts/2017-07-16-la-carta-della-nostalgia/), così come quella per Apple Basic. Poi, però, se ha significato qualcosa, accetti una sfida analoga a quella di trent’anni fa e magari, nei ritagli, lo rifai in Swift, in Html, in Python. Rendi attuale quella storia che hai vissuto e che merita di essere trasmessa in forma diversa da quella del sarcofago e del *come eravamo*, tanto romantico quando noioso.

Non sto neanche a parlare della scuola, dove un esercizio del genere è logica, pensiero computazionale, aritmetica, programmazione e persino storia contemporanea (perché a questi bambini e ragazzi qualcuno dovrà pure dirlo, come è nato tutto ciò).

Già di notte ho da fare di mio, e ora mi tocca pure finire Hamurabi.