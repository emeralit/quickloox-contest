---
title: "Una lettura propedeutica"
date: 2021-05-02T23:07:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Gianni Catalfamo, CorpoNazione] 
---
Non ho ancora finito il mio contributo all’esegesi delle *CorpoNazioni*, come le ha definite genialmente Gianni Catalfamo sul proprio blog, per cui mi limito a indicare come lettura preventiva [il suo post](https://sonofgeektalk.wordpress.com/2021/04/30/la-nascita-della-corponazione/), al quale, come da sua richiesta (aperta, non personale), aggiungerò un commento con le mie opinioni.

Commento che, comunque, apparirà prima qui.

Ci saranno sovrapposizioni e lacune. Il tema è più grosso di quanto sembra e organizzarlo in modo immediatamente comprensibile genera fatica. Si abbia pazienza, a essere americani si avrebbe un agente pronto a coprirci di denaro. Qui abbiamo da sbrigare gli affari correnti.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*