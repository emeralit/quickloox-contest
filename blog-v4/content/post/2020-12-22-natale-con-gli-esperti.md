---
title: "Natale con gli esperti"
date: 2020-12-22
comments: true
tags: [BBEdit, Expert, Preferences]
---
A me risulta che sia una cosa nuova o almeno recente: la pagina con [tutte le Expert Preferences di BBEdit](https://www.barebones.com/support/bbedit/ExpertPreferences.html).

Sono parametri di configurazione assenti dall’interfaccia grafica, che sagomano il comportamento di BBEdit in situazioni specifiche secondo il volere dell’utilizzatore.

Esempio qualunque: in certi linguaggi di programmazione, per esempio Swift, è uso nominare le variabili secondo lo schema cosiddetto Camel Case: si comincia con una parola in minuscolo e si aggiungono parole con l’iniziale maiuscola, senza spazi, trattini, *underscore* o altro.

Così il nome di una variabile potrebbe essere *pagineMancantiAllaFine*, per dire. Oppure *valoreAtteso*, *numeroCaratteriParagrafo*.

Ecco: BBEdit, nello spostare il cursore, ha comandi che si fermano su ciascun passaggio da minuscola a maiuscola, per aiutare il programmatore nell’editing.

Oppure… no. Una Expert Preference permette di negare questa impostazione.

Consultare l’elenco è un piacere per occhi *nerd* e, per me, notevolmente utile. Come se in Bare Bones mi avessero fatto un regalo per Natale.