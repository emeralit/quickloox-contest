---
title: "Nuove recensioni"
date: 2021-12-19T00:29:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Ars Technica, M1, MacBook Pro, ThinkPad, Xps 15, Dell, Lenovo, X1] 
---
Qualcuno argomenta che M1 sia un progresso tecnologico, certo, ma null’altro che una sorta di nuova versione di processore di punta, i cui miglioramenti in prestazioni e consumi saranno presto assorbiti dal mercato con l’uscita di chip concorrenti capaci di pareggiare ogni divario, se non superarlo.

Può darsi: nel frattempo M1 è già entrato nella storia se non altro per la sua capacità di cambiare lo stile di scrittura delle recensioni.

Il titolo dedicato da _Ars Technica_ a ThinkPad X1 Extreme Gen 4, anche solo un paio di anni fa, non sarebbe mai apparso: [un laptop potente con problemi di riscaldamento](https://arstechnica.com/gadgets/2021/12/review-lenovos-thinkpad-x1-extreme-gen-4-is-a-powerful-laptop-with-heat-problems/). Sottotitolo: _le prestazioni migliorano apprezzabilmente se sei disposto a lasciarlo scaldare_.

(Sottotesto: senza scaldare, rallenta e non è potente come strombazza il titolo).

Le nuove recensioni, come quelle vecchie, mettono a confronto il protagonista con modelli della concorrenza. Questo è un portatile di punta e viene confrontato con cose di Dell, di Lenovo e anche con MacBook Pro 16” M1. Veniamo a sapere per esempio che può raggiungere temperature operative di venti gradi superiori a quelle di un Xps 15 Dell.

Il nuovo stile è che, mentre è facile sapere che MacBook Pro è più pesante, la sua temperatura operativa _non viene riportata assieme alle altre_. Dimenticanza, forse.

Al momento di misurare le prestazioni, il dato di MacBook Pro compare solo in alcuni test. Potrebbe essere solo questione tecnica; certi test magari non sono ancora tarati correttamente su Arm. Certo, strano che non sia riportata la misura della temperatura, dove le diverse architetture non pongono problemi di misurazione.

Doppiamente strano, perché neanche l’autonomia della batteria di MacBook Pro viene riportata.

Sarà comunque un caso che i dati mancanti corrispondano ai punti di forza di M1. Sicuramente.

Sicuramente è una formula che potrà suscitare nuovo interesse nei lettori: chissà da quali computer arriveranno i dati della prossima tabella.

È ancora presto per valutare l’impatto di M1 sull’informatica personale. Sull’editoria informatica, soprattutto sulle recensioni dei portatili, ha già lasciato il segno.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per Qui
ckLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._