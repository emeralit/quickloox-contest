---
title: "Istinto e ragione"
date: 2013-06-13
comments: true
tags: [Octopress]
---
Giorni fa l’editore di iCreate ha abbandonato per questioni di licenza il dominio icreatemagazine.com, che ospitava Script. La cosa mi ha sorpreso nel mezzo di un turbine di impegni, lavori, spostamenti e viaggi che mi ha impedito di occuparmene per un po’.<!--more-->

Al momento di occuparmene, mi sono sentito stufo di dipendere da qualcuno per lo *hosting*. Preferisco essere responsabile diretto se qualcosa non va.

Al tempo stesso, potevo accendere un blog Wordpress, TextPattern, <a href="http://drupal.com">Drupal</a> o altro con un singolo clic, come sa chi si trova ad avere uno spazio di *hosting* su [TextDrive](http://textdrive.com).

La ragione mi avrebbe fatto risparmiare tempo e fatica. L’istinto mi ha suggerito che così non avrei imparato niente. E ho scelto un’altra strada: [Octopress](http://octopress.org) di Brandon Mathis, una versione ingentilita e facilitata di [Jekyll](http://jekyllrb.com), il sistema che genera le pagine di [Github](https://github.com). Una possibile via di mezzo accettabile tra la voglia di ampliare le mie esperienze e l’esigenza di ripartire senza metterci un mese di lavoro.

Octopress, a differenza di [Wordpress](http://wordpress.org) o [TextPattern](http://textpattern.com), è un motore statico. Sotto non c’è un database e le pagine web sono vere pagine web. Su Wordpress non ci sono pagine web, non del tutto almeno: c’è un database che produce di volta in volta in tempo reale la pagina che poi uno vede nel *browser*.

La struttura di Octopress è più semplice, dunque, ma più vicina a chi utilizza il motore e con ampia possibilità di personalizzazione e potenziamento. In compenso il sistema è più ruspante da installare e dà per scontato che si abbia esperienza di Terminale e di comandi Unix.

Per arrivare ad avere Octopress funzionante, ho familiarizzato con GitHub; ho installato una versione specifica di [Ruby](http://www.ruby-lang.org/), un sistema per gestire le installazioni di Ruby chiamato [rvm](https://rvm.io) e ho risolto un sacco di problemi di librerie e dipendenze software con l’aiuto di [Homebrew](http://mxcl.github.io/homebrew/).

Ora ho sul disco rigido una cartella apposita che contiene tutto il mio sito e ho una serie di comandi Unix per generare nuovi documenti (scritti, volendo, in [Markdown](http://daringfireball.net/projects/markdown/)) e aggiornare. Una impostazione, quando tutto è pronto, spedisce automaticamente i dati che sono cambiati verso il mio *hosting*, dal quale è possibile leggerli *online*.

Chi me lo fa fare, mi si dirà. Ragione e istinto insieme. Voglio padroneggiare meglio il lato tecnico della pubblicazione *online* e questo nuovo assetto mi spinge a farlo, oltre a essere molto più accessibile nel caso decida di volerlo fare. Qualsiasi miglioramento e aggiunta sarà farina del mio sacco, magari destramente trovata in rete, ma installata e personalizzata personalmente. Qualsiasi problema o difetto sarà mia responsabilità.

Questo non significa cedere sulla qualità dei contenuti e farò del mio meglio per continuare a scrivere cose vagamente interessanti per chi abbia piacere di leggerle e commentarle. La struttura del sito migliorerà piano piano, almeno nelle intenzioni, un passo alla volta, fino ad arrivare anche alla personalizzazione (tipo)grafica.

Accetto consigli, aiuti, critiche, suggerimenti, dileggi e incoraggiamenti. Non riuscirò a fare tutto quello che vorrei, ma ogni cosa sarà un passo avanti. Diverso dal ripetere tutti i giorni gli stessi clic nello stesso Wordpress e dire tutti i giorni *pubblica* senza essere almeno minimamente cresciuto. Ora mi trovo in un insieme <a href="https://github.com/imathis/octopress/wiki/Octopress-Sites">infinitamente più piccolo</a> di persone che usano lo stesso motore, rispetto ai milioni di utilizzatori di Wordpress, e la cosa mi piace. Non per un senso elitario, ma perché si fa più interessante. Con tutto il rispetto per chi preferisce Wordpress (anche perché ci alimento tre siti diversi per ragioni professionali ed è un signor sistema).

Una cosa che vorrei evitare, ma accadrà: ogni tanto farò casino, scombinerò qualcosa, sbaglierò una installazione. Me ne scuso in anticipo e farò il possibile perché tutto vada per il meglio e migliorando.

Non pretendo che sia tutto comprensibile né condivisibile. Mi sentivo di dovere una spiegazione.

Un grosso grazie per essere arrivati fino a qui. E ora si (ri)comincia, con l’istinto e la ragione a braccetto, spero riconciliati.