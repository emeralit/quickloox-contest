---
title: "Una cosa per volta"
date: 2018-07-09
comments: true
tags: [iPad, IIsi, Mac]
---
La sfida di [riportare alla vita un Mac IIsi del 1990](https://arstechnica.com/features/2018/07/classic-computing-joyride-cruising-through-modern-workloads-on-a-macintosh-iisi/) e metterlo su Internet nonché praticare le tipiche attività del computer di oggi, o almeno approssimarle, è sempre avvincente, va da sé. Ci sono il fatto tecnico e le soluzioni ingegnose.

Allo stesso tempo, mi ripeto, diventa rapidamente stucchevole. Bellissimo il retrocomputing e lo adoro, però ormai è stato fatto più o meno tutto quello che può essere fatto. L’ennesimo computer apparentemente inadeguato che diventa adeguato, va bene, purché ci sia almeno qualcosa di intrigante. Che ho trovato verso la fine dell’articolo di *ArsTechnica*.

>Le limitazioni della macchina, a malapena abbastanza potente per fare funzionare più di una applicazione per volta, richiedono di focalizzare al cento percento l’attenzione su una singola attività. Paradossalmente, ho sentito spesso di essere più produttivo quando avevo molte meno risorse a disposizione. Questo teneva la mia attenzione su un singolo problema. […] Mac IIsi ha da tempo un posto solamente nella storia. Comunque, può evidentemente insegnarci ancora una cosa o due.

Per esempio, la ragione del successo di iPad e di iOS. Un certo grado di multitasking è doveroso; per esempio, lavorare su due documenti di tipo diverso, affiancati, è una singola attività, anche se comporta avere due applicazioni sullo schermo. Da tempo spero con forza che arrivi su iOS una versione accettabile del Terminale. Ma mi auguro che iPad non diventi un computer totalmente multitasking come Mac.

Mac e il multitasking totale hanno un sacco di grandi perché; iPad è la macchina ideale per essere produttivi, nonostante il mantra per cui sarebbe adeguato solo al consumo di informazione. Invece è la macchina perfetta per la concentrazione su un problema specifico. Una cosa per volta.

Rispetto profondamente i miei amici appassionati di retrocomputing, ma mi appassiona di più Viticci quando spreme come un limone un iPad nuovo di fabbrica [in modi che il comune mortale nemmeno riesce a concepire](https://www.macstories.net/tag/ipad-diaries/).