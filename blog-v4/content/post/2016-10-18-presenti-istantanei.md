---
title: "Presenti istantanei"
date: 2016-10-18
comments: true
tags: [Gand, iBooks, Author, scuola, ebook, iPad]
---
Parlavo giusto ieri del [lancio di un libro a pagamento](http://macintelligence.org/blog/2016/10/17-futuri-possibili/) e torno in tema, solo che oggi parlo di un libro gratis e già disponibile.<!--more-->

[Periferie](https://itunes.apple.com/us/book/id1131348016) me lo segnala [Gand](http://www.freesmug.org) (lunga vita al software libero per macOS e pure per iOS), che lo conosce bene in quanto trattasi del…

>catalogo della rassegna ideata e organizzata nell’ambito dei corsi di fotografia dell’IPSSCSI Kandinsky di Milano in collaborazione con la Biblioteca del Centro culturale Cascina Grande di Rozzano, [che] propone i lavori degli studenti di Grafica dell’istituto e di importanti fotografi italiani sul tema delle periferie.

Il libro è stato realizzato con iBooks Author ed quindi visto al massimo della resa su un iPad, anche se iBooks lo esegue senza problemi su Mac, iPhone, iPod touch e domani chissà che altro.

Sapere che è un lavoro scolastico incoraggia il pregiudizio. Sfogliarlo lo fa passare. Riporto un paio di considerazioni di Gand:

>Dell’esperienza scolastica ti sottolineo la presenza di fotografi noti a livello internazionale come Francesco Cito, Cesare Colombo, Uliano Lucas, insieme agli studenti della scuola (merito dei docenti di fotografia); i testi delle didascalie elaborate sempre dagli studenti (merito del docente di italiano) ed i progetti grafici delle locandine (merito del docente di grafica).

>Tre discipline coinvolte che hanno generato una mostra ed un catalogo abbastanza unici nel panorama didattico italiano.

E c’è da riflettere sulla qualità che oggi permettono gli strumenti che abbiamo. Su come possono servire a una scuola, su come la scuola può approfittarne per il bene e la crescita degli studenti, se appena sulle cattedre siedono persone attente. Come Gand. E il cerchio si chiude con mille possibilità che si aprono alle classi, ai docenti, a tutti.

Sarà sempre troppo tardi quando a scuola gli strumenti, invece di fare un milione di domande prevenute per boicottarne l’adozione e venerare un secolo già lontano, saranno semplicemente usati e valutati, buttati nel cestino se sono sbagliati e invece valorizzati se portano valore.