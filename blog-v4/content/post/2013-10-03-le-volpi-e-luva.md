---
title: "Le volpi e l’uva"
date: 2013-10-03
comments: true
tags: [iOS]
---
iPhone 5S è [il primo computer da tasca al mondo con processore a 64 bit](http://www.apple.com/it/iphone-5s/features/). Gli altri hanno solo promesso (a cose fatte) e arriveranno, ma non oggi e neanche il mese prossimo.<!--more-->

Anand Chandrasekher, *senior vide president* e *chief marketing officer* di Qualcomm, così ne pensa [secondo Techworld](http://news.techworld.com/personal-tech/3471543/apples-64-bit-a7-chip-a-marketing-gimmick-qualcomm-exec-says/):

>Penso che sia un trucco del marketing. I vantaggi per il consumatore sono zero. […] Per lo più serve per amministrare più di quattro gigabyte di Ram.

L’[opinione di Extremetech](http://www.extremetech.com/gaming/166244-iphone-5s-the-64-bit-a7-chip-is-marketing-fluff-and-wont-improve-performance):

>Il passaggio a 64 bit di iPhone 5S è quasi interamente fuffa di marketing. Non è la chiave dei miglioramenti in prestazioni. Questi verranno dai miglioramenti architetturali che Apple integrerà nei prossimi sei-dodici mesi relativamente alla gestione del codice a 32 bit.

In altre parole, l’attuale [classifica di velocità](http://9to5mac.com/2013/09/19/iphone-5s-a7-chip-only-dual-core-but-its-still-the-fastest-phone-out-there/) non sarebbe dovuta alla scelta dei 64 bit, che però contribuirà ad aumentare le prestazioni. Insomma, il divario rispetto agli altri crescerà in virtù del solo lavoro dei programmatori, a parità di hardware, e già sembra una buona ragione per andare a 64 bit.

Ma davvero la questione dei 64 bit è unicamente di Ram? Su ZDNet Adrian Kingsley-Hughes [scrive](http://www.zdnet.com/why-apple-went-64-bit-with-the-iphone-5s-7000020662/) che la funzione di ripresa video al rallentatore presente in iPhone 5S, molto esigente dal punto di vista del calcolo, potrebbe trarre giovamento dal processore a 64 bit. Vale lo stesso per Touch ID, se i dati delle impronte digitali vengono difese dentro iPhone da cifratura di alto livello, anch’essa affamata di potenza di calcolo (il dito va riconosciuto e codificato in pochi attimi, o l’usabilità è persa e la sicurezza pure).

Mike Ash [affronta la questione](http://www.mikeash.com/pyblog/friday-qa-2013-09-27-arm64-and-you.html) a livello estremamente tecnico. Ci sono tanti distinguo da fare e questo seppellisce definitivamente l’idea che conti solo la Ram. Condenso due passaggi traducibili per noi umani:

>I 64 bit possono migliorare le prestazioni di certi tipi di codice e rendere più praticabili certe tecniche di programmazione. Tuttavia possono anche diminuire le prestazioni a causa del maggiore uso di memoria. […] Non sono un trucco di marketing ma neanche una rivoluzione straordinaria che abilita una classe interamente nuova di applicazioni. La verità, come accade spesso, sta nel mezzo.

Sempre secondo Chandrasekeher di Qualcomm, [riporta Computerworld](http://www.computerworld.com/s/article/9242879/Apple_39_s_64_bit_A7_chip_is_a_39_marketing_gimmick_39_?pageNumber=1), costruire chip a 64 bit ha senso da un punto di vista di efficienza ingegneristica. In pratica Apple lo ha fatto più per sé che per l’utenza.

La domande è perché non lo abbia fatto prima Qualcomm, allora.

Probabilmente hanno guardato i 64 bit là in alto e li hanno dichiarati acerbi.