---
title: "A bella posta"
date: 2014-09-13
comments: true
tags: [Fabio, Mailbox, Mac, iOS]
---
Grazie a **Fabio** ho potuto provare una copia di [Mailbox per Mac](http://www.mailboxapp.com). Mentre è liberamente scaricabile su iOS, per Mac la beta richiede il possesso di una *betacoin*, una monetina virtuale.<!--more-->

L’idea è che, se Mailbox per Mac fosse efficace quanto lo è per iOS, uno potrebbe considerarne l’adozione.

Al momento tuttavia è ancora troppo presto, per me. Gli *account* maneggiati sono solo quelli iCloud e Gmail e ho esigenze più allargate. Inoltre ho incontrato qualche problema nella spedizione di allegati in un momento cruciale dove serviva, ed è finita che il mio progetto di vivere una settimana dentro Mailbox è durato mezza giornata.

Le mie esigenze tuttavia non fanno testo e una beta è sempre una beta, per cui una difficoltà con gli allegati potrebbe anche essersi già risolta. E Mailbox ha qualcosa da dire in termini di velocizzazione dell’amministrazione e di interfaccia. Dove il programma funziona, tendo ad apprezzarlo.

Può darsi benissimo che persone con esigenze diverse dalle mie lo apprezzino.

Insomma, ho tre *betacoin* da distribuire ad aspiranti *beta tester* di Mailbox per Mac.

Se qualcuno è interessato, lo scriva nei commenti e aggiunga un indirizzo di posta cui spedire il *betacoin*.

C’è tempo fino a mezzanotte, dopo di che invierò i *betacoin*. Se necessario, estrarrò a sorte tra i richiedenti.

Chi non fornisce un indirizzo di posta resta fuori dall’estrazione.

Intanto aspetto un mesetto e poi riprovo con la mia copia, a vedere l’effetto che fa.