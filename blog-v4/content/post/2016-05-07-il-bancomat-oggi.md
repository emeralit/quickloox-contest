---
title: "Il Bancomat oggi"
date: 2016-05-07
comments: true
tags: [Bancomat, Dos]
---
Solo per dire che un oggetto moderno ha sistemi di riavvio automatico e autodiagnostica, che gli consentono di riprendersi autonomamente dalla maggioranza delle situazioni di errore.

Dovrebbe avere.

 ![Bancomat in errore](/images/bancomat.jpg  "Bancomat in errore") 