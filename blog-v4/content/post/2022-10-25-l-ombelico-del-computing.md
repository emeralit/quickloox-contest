---
title: "L’ombelico del computing"
date: 2022-10-25T01:27:48+01:00
draft: false
toc: false
comments: true
categories: [Software, Hardware]
tags: [Mori, Riccardo, Riccardo Mori, Mac, iPad, macOS, Stage Manager]
---
Il [giorno degli aggiornamenti](https://macintelligence.org/posts/2022-10-21-un-anno-da-venturieri/) è passato è l’osservazione più interessante, come spesso accade, è di **Riccardo** che titola [Il mio prossimo Mac potrebbe essere l’ultimo](http://morrick.me/archives/9667).

Molto in sintesi, l’argomentazione è che lo sviluppo software di Mac non è migliorativo come lo sviluppo hardware; che la qualità di macOS non è più la stessa; che non è questione di abbandonare la piattaforma ma, piuttosto, rimanere a quando doveva ancora iniziare l’omogeneizzazione di macOS e iPadOS.

Il merito non è in discussione. Per quanti siano i problemi si possono citare altrettanti miglioramenti, però è indubbio che i problemi siano particolarmente vistosi in questo periodo; Stage Manager, prima che una funzione disfunzionale, è un sintomo della difficoltà attuale di Apple di produrre software di sistema con un design superiore.

Che cosa farei al posto di Riccardo? Forse punterei maggiormente sul lavoro via Terminale, oppure penserei al mio adorato FreeBSD. Alla fine, per chi lavora intensamente sul computer, il Terminale è il fulcro più potente per le leve del software. Per me vale da quando Mac OS X era una beta pubblica e doveva ancora finire il ventesimo secolo; non è certo Stage Manager a suggerire una scelta.

Oppure, il mio adorato FreeBSD. Alla fine sarebbe poso diverso dal puntare sul Terminale. In ogni caso sarebbe possibile installare un ambiente grafico di tutto rispetto.

Il problema è a monte e penso che anche in Apple si ragioni molto in questo modo. Il baricentro del computing di Apple, l’ombelico del suo ecosistema, non è più Mac. Sta fuori da un apparecchio specifico, in una sorta di punto lagrangiano tra Mac, iPad, iPhone, watch e mettiamoci pure tv.

Per Riccardo è la scelta di Apple di semplificarsi le cose producendo nella sostanza lo stesso software per tutti. Per me è guardare al complesso dell’ecosistema e creare software che in funzione di esso, più che dei singoli apparecchi. Questo non scusa Stage Manager, ma spiega il perché a volte si prendono direzioni sbagliate; si ragiona meno con l’apparecchio specifico al centro dell’attenzione e questo porta gli svantaggi, oltre ai vantaggi.

Non è certo la prima volta che lo scrivo: da anni metà del mio computing passa da iPad, metà da Mac. Il mio baricentro computazionale non sta davanti allo schermo di Mac e non ci sta da molto tempo. Scrivo questo pezzo a letto su iPad e tra poco lo mando su Mac via Terminale per la pubblicazione. La sveglia per domattina sta su watch. iPhone è a ricaricarsi per il giorno dopo.

Domani sera potrei essere nella stessa situazione, però a scrivere su Mac, anche solo perché ho voglia di stare seduto alla scrivania più che sdraiato sotto le lenzuola. Mi rendo conto di guardare molto meno di una volta all’apparecchio che uso; l’attenzione è rivolta al documento, al compito da svolgere, non più all’hardware o al software. Su iPad, dove non ho BBEdit, uso almeno tre editor di testo diversi a seconda del contesto.

Sono anche un privilegiato che può permettersi di scegliere la piattaforma giusta per ciascuna situazione, sicuro. Ma sono privilegiato per gli strumenti che uso, non per il loro numero; avere più computer è estremamente comune, nel 2022. Succede del resto anche a Riccardo, solo che il suo mix è diverso dal mio e il suo baricentro pure.

È per questo che, forse, il suo prossimo Mac sarà l’ultimo. Nel mio caso, non ho questa nozione. Il mio prossimo Mac arriverà se e quando servirà un Mac. Altrimenti arriverà un iPad o qualcos’altro, magari un RaspBerry 4 come piattaforma di scrittura pura. Più di ombelico dell’ecosistema dovremmo parlare di asse, come quello terrestre. Che si sposta nel tempo.