---
title: "Supremo Enigma"
date: 2021-04-05T00:56:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [watch SE, iPhone SE, iPhone 6s, iOS 14] 
---
Apple afferma chiaramente che watch è compatibile *con iPhone 6s o successivo con iOS 14 o successivo*.

Sarà compatibile con [iPhone SE edizione 2016](https://en.wikipedia.org/wiki/IPhone_SE_(1st_generation)), che condivide il processore di [iPhone 6s](https://en.wikipedia.org/wiki/IPhone_6S) ed è uscito sei mesi dopo? Si direbbe di sì perché è successivo, ma sto ancora cercando una conferma.

La cerco perché mi interessa il modello cellulare e la [tabella degli operatori che lo supportano](https://www.apple.com/it/watch/cellular/#table-se) rinvia per l’Italia a Vodafone, però attraverso un link che non funziona.

La cosa è rilevante perché, per usare la funzione di telefonia in autonomia, watch SE deve comunque essere apparentato a un iPhone, il quale deve utilizzare lo stesso identico provider.

L’unica eccezione a questa regola è per i membri di In Famiglia, per i quali la restrizione non si applica. Neanche a dirlo, il supporto per avere watch cellulare configurabile in modo diversi per i membri di In Famiglia, in Italia deve ancora arrivare.

Ho perfino posto una domanda su [Apple Discussions](https://discussions.apple.com/welcome), che è il primo passo verso lo sviluppo di uno stato depressivo.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*