---
title: "Pesce di ottobre"
date: 2014-10-03
comments: true
tags: [Windows8, Microsoft, Windows10, Windows95, Windows98, Windows9]
---
Per togliersi prima possibile dai pasticci e dai dissensi che ha provocato Windows 8, Microsoft ha chiamato la versione successiva Windows 10, saltando a piè pari il numero nove.<!--more-->

Gira su Internet una versione per cui ci sarebbe anche o solo una ragione tecnica: un sacco di programmi guardano se il nome del sistema operativo comincia con *Windows 9* interpretando la risposta affermativa come l’inizio di *Windows 95* o *Windows 98*. Così si sarebbe preferito evitare di generare ambiguità, perché il testo darebbe risultato positivo ma il sistema operativo sarebbe di una versione ben diversa da quella attesa.

Ora non so più se ridere o piangere davanti al [pesce d’aprile 2013 di Infoworld](http://www.infoworld.com/article/2613504/microsoft-windows/microsoft-windows-microsoft-skips-too-good-windows-9-jumps-to-windows-10.html).