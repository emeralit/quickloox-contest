---
title: "Chi apprezza compra"
date: 2015-08-02
comments: true
tags: [iPad, Pages, Numbers, Keynote, Sheets, Docs, Sky, Premium, Calculator, GeoGebra, Wolfie, Virtuoso, Tydlig, Dictionary, Earth, MathStudio, Planets, PocketCAS, Slides]
---
Sento che sua figlia *l’anno prossimo andrà a scuola con iPad*. Buona cosa. *È che mi tocca spendere un botto in programmi*.<!--more-->

Eh, accidenti. Chiedo quanto ci sarà da spendere in libri e rabbrividisco. Naturalmente libri di carta, certo con dentro il loro bravo Cd, certo con il sito a supporto, ma la carta guai a chi la tocca. Non è che eliminando la carta un libro costerebbe zero, però sicuramente potrebbe costare il 25-30 percento in meno senza discussioni. Con le discussioni si potrebbe anche fare meglio. Con un taglio di un quarto della spesa sui libri di carta si potrebbero comprare app in quantità e qualità inusitate.

Eh, ma i libri *ci vogliono*, mi risponde. Non capisco il senso dell’espressione (a parte quello letterale, *i libri vogliono noi*, leggermente fuori luogo), cambio discorso e rifletto brevemente dalla sdraio.

[Pages](https://appsto.re/it/EysIv.i), [Numbers](https://appsto.re/it/7mrIv.i), [Keynote](https://appsto.re/it/ODmIv.i): con un iPad nuovo, sono regalati. Non piacessero, ci sono le alternative di Google, [Docs](https://appsto.re/it/qimpY.i), [Sheets](https://appsto.re/it/zTnpY.i), [Slides](https://appsto.re/it/wv8A0.i). Gratis. Si dovessero comprare i programmi di Apple, si tratterebbe di 9,99 euro ad app, totale 29,97 euro.

Metà di un libro di testo. Un mese di televisione Sky o Premium, più o meno, costa quindici-venti euro, modello base. Dove ho visto le promozioni sul lungomare, trenta euro potevano pagare al massimo due-tre giorni di ombrellone. A essere bravi può uscire un cinema per due in un multisala ultimo modello. Credo che la cifra sia appena sufficiente per avere un giorno di agosto a mezza pensione nella locanda più modesta affacciata sull'Adriatico. Forse esce un tragitto breve sull’alta velocità ferroviaria, forse un singolo volo per Parigi o Berlino che più low-cost di così devi portarti il paracadute da casa.

Il software su un iPad costa poco, rispetto alla vita. Se la scuola conta, sembra un problema superabile.

Di più: a volte i programmi vengono mandati brevemente in promozione gratis e possono essere presi a costo zero al prezzo di un poco di attenzione. [MyScript Calculator](https://itunes.apple.com/it/app/myscript-calculator-handwriting/id578979413?l=en&mt=8), [Merriam-Webster Dictionary HD](https://itunes.apple.com/it/app/merriam-webster-dictionary/id438477986?l=en&mt=8), [GeoGebra](https://macintelligence.org/posts/2014-11-26-la-spesa-in-istruzione/), [GraphMe HD](https://itunes.apple.com/it/app/graphme-hd-graphing-calculator/id679469147?l=en&mt=8), [MathStudio](https://itunes.apple.com/it/app/mathstudio/id956033107?l=en&mt=8), [Planets](https://itunes.apple.com/it/app/planets/id305793334?l=en&mt=8), [Mathematics with PocketCAS Pro](https://itunes.apple.com/it/app/mathematics-pocketcas-pro/id517250782?l=en&mt=8), [Quick Graph](https://itunes.apple.com/it/app/quick-graph-your-scientific/id292412367?l=en&mt=8), [SkySafari](https://itunes.apple.com/it/app/skysafari-3/id437108143?l=en&mt=8), [StarMap 3D+](https://itunes.apple.com/it/app/starmap-3d+-guide-to-night/id351986992?l=en&mt=8), [Tydlig](https://itunes.apple.com/it/app/tydlig-calculator-reimagined/id721606556?l=en&mt=8), [Vector Illustration](https://itunes.apple.com/it/app/vector-illustration/id775891075?l=en&mt=8), [Virtuoso](https://itunes.apple.com/it/app/virtuoso-piano-free-3/id391994966?l=en&mt=8), [Piano Practice with Wolfie](https://itunes.apple.com/it/app/piano-practice-with-wolfie/id868060859?l=en&mt=8) certamente non li ho pagati. Qualcuno è gratis e non me lo ricordo o è diventato gratis nel frattempo, però fa poca differenza. E ho solo curiosato nei meandri del mio iPad, assolutamente non ottimizzato per la scuola dell’obbligo.

[Google Earth](https://itunes.apple.com/it/app/google-earth/id293622097?l=en&mt=8) eclissa qualsiasi atlante classico ed è gratuito.

Evito di entrare ulteriormente nel merito delle singole materie e applicazioni. Il punto non era neppure questo. È che lì fuori, se esistesse una *app* di storia perfetta per la quinta elementare, e costasse un'esagerazione tipo nove euro e novantanove, ti guarderebbero come se ci fosse in gioco una donazione di midollo o l‘acquisto della seconda casa. Tirando fuori con l’altra mano senza dire niente cifre esorbitante per libri di testo la cui qualità, lo posso dire?, talvolta è discutibile.

Più in generale, quando sento padri di famiglia fremere sulla difensiva di fronte ad *app* da *novantanove centesimi*, capisco che non comprendono lo strumento nelle loro mani. Se però non capiscono lo strumento che è nelle mani della prole, la questione è più grave.
