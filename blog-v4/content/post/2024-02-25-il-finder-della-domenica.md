---
title: "Il Finder della domenica"
date: 2024-02-25T01:25:26+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Finder]
---
Io lo so che arrivo sempre per ultimo; in compenso le cose ovvie mi paiono illuminazioni.

Mi sono appena reso conto che nella barra strumenti del Finder è possibile inserire *applicazioni*. Come il Dock, come la barra laterale, solo che il primo spesso è fuori mano e la seconda è prioritariamente assegnata ai miei riferimenti di cartelle fisse e cartelle smart.

A istinto, non ho messo app ovvie o strausate: per esempio, in ogni momento ho sul desktop numerosi file che basta cliccare per fare partire BBEdit. Non mi serve una scorciatoia per fare partire il programma che uso di più. Di solito è già in funzione e, non fosse, è un attimo.

Mi sembra una collocazione ideale per quelle app che non servono continuamente, ma sono ricorrenti e in genere devono togliere un fastidio, da Contatti a Freeform, da Books a Music.

Non ti risolve la domenica scoprire l’acqua calda, però il caffè della mezzanotte sembra più gustoso.