---
title: "Lo sprint infinito"
date: 2015-01-09
comments: true
tags: [Arment, Jalkut, iOS, OSX, sprint, Serlet, Federighi, AppleWatch, watch, SnowLeopard]
---
La polemica lanciata da Marco Arment sulla qualità del software Apple (riassumibile in [non è più come una volta](http://www.marco.org/2015/01/04/apple-lost-functional-high-ground), senza sottovalutarla in alcun modo) ha forse consentito lo svelamento di [qualche informazione](https://news.ycombinator.com/item?id=8837102) su come avviene lo sviluppo di OS X e iOS all’interno di Apple, per voce di un ex sviluppatore.<!--more-->

>Il cambiamento maggiore nella metodologia di sviluppo si è avuto quando Bertrand Serlet è stato sostituito da Craig Federighi. Con Bertrand si lavorava a gigantesche versioni monolitiche dove ogni gruppo buttava quello che aveva pronto e che venivano prodotte internamente ogni notte. […] Il risultato erano versioni estremamente in ritardo con una tonnellata di bug su cui accumulavamo riparazioni man mano che passava il tempo. Craig ha riorganizzato il lavoro in uno *sprint system*, dove si sviluppano nuove funzioni per due settimane e si cacciano bug la settimana successiva. Dopo dieci o dodici o sedici cicli, il software era pronto. A mio giudizio produceva software più stabile ma più conservazionale, dove era molto difficile introdurre nuove funzioni molto grosse o grandi riscritture e questo non accadeva prima che si fosse tipo a due terzi del ciclo di sviluppo.

Un’altra voce [sostiene](https://news.ycombinator.com/item?id=8837323) (potenzialmente da dentro Apple) che c’è più attenzione a funzioni poco utili rispetto a quelle essenziali e più importanti.

Conta molto, credo, il [parere di Daniel Jalkut](http://bitsplitting.org/2015/01/05/the-functional-high-ground/): prima passava più tempo tra una versione e l’altra e si arrivava prima o poi a una versione stabile. Si stava su una versione stabile per mesi o anni prima che arrivasse l’edizione seguente, mentre adesso con i cicli annuali di uscita questa stabilizzazione non avviene più e la qualità del software, in positivo e in negativo, è complessivamente la stessa.

Probabilmente non sarebbe male un altro giro stile Snow Leopard: zero nuove funzioni annunciate, tutta rifinitura e sterminio di *bug*. Il prossimo ciclo di rinnovo potrebbe essere appropriato, visto che sarà uscito anche watch.

L’idea del sistema di sviluppo sprint comunque è interessante. Un po’ come se Apple fosse passata dal ciclismo su strada a quello su pista, da passisti a *pistard*.