---
title: "Il pesce clone"
date: 2014-04-02
comments: true
tags: [Roku, Htc, Samsung]
---
Sembrerebbe il peggiore dei pesci di aprile, invece è vera. Htc ha fintamente annunciato un [guanto computerizzato](http://www.htc.com/www/go/gluuv/) che sa di film di fantascienza di serie B. Un bel pesce d’aprile, curato e gustoso.<!--more-->

Samsung, quella accusata spesso di copiare a destra e a manca, soprattutto da Apple, ha preparato un pesce d’aprile: [*un guanto computerizzato*](http://global.samsungtomorrow.com/?p=35430).

A dire il vero, non pare che Samsung abbia veramente copiato, con al massimo paio d’ora a disposizione prima di perdere il treno. Per lo meno, non questa volta.

Ma c’è una questione più problematica. Per esempio Roku [ha annunciato](https://twitter.com/rokuplayer/status/451001746202972160) un orologio computerizzato, sempre come pesce d’aprile.

<blockquote class="twitter-tweet" lang="en"><p>The newest addition to the Roku family - introducing the <a href="https://twitter.com/search?q=%23RokuWatch&amp;src=hash">#RokuWatch</a>. <a href="http://t.co/Roz3USh7pI">http://t.co/Roz3USh7pI</a> <a href="http://t.co/zDWHoxUP55">pic.twitter.com/zDWHoxUP55</a></p>&mdash; Roku (@RokuPlayer) <a href="https://twitter.com/RokuPlayer/statuses/451001746202972160">April 1, 2014</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Manca la *fantasia*. Innovazione zero, creatività zero, persino per fare uno scherzo. Il che dice infinite cose sui prodotti reali.