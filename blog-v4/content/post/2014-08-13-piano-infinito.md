---
title: "Piano infinito"
date: 2014-08-14
comments: true
tags: [Samsung, Apple, Micromax, Xiaomi, iPhone]
---
Notizia fondamentale per tutti quanti confrontano un iPhone con un Samsung e la buttano sul prezzo.<!--more-->

In India, la casalinga Micromax [ha superato Samsung nelle vendite](http://in.mobile.reuters.com/article/idINKBN0G50C720140805?irpc=932). In Cina, la casalinga Xiaomi [ha superato Samsung nelle vendite](http://blogs.wsj.com/digits/2014/08/04/xiaomi-overtakes-samsung-in-china-smartphone-market/).

In ambedue i casi l’ascesa è dovuta alla proposta di prodotti dalle prestazioni similari, a prezzo decisamente inferiore.

Il compromesso sulla qualità è un piano inclinato senza fine. Si può sempre avere ancora meno e pagare ancora meno, per trovarsi in tasca sempre meno. Pur avendo, per così dire, risparmiato.