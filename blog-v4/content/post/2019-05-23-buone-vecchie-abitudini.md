---
title: "Buone vecchie abitudini"
date: 2019-05-23
comments: true
tags: [MacBook, Pro]
---
Solo John Gruber può prendere un [banale annuncio](https://www.apple.com/newsroom/2019/05/apple-introduces-first-8-core-macbook-pro-the-fastest-mac-notebook-ever/) di *speed bump* dei MacBook Pro per ricavarne una serie interessante di deduzioni, non tutte banali.

Intanto, uno *speed bump* è poca cosa, ma è anche importante; chi dipende nel lavoro dal Mac più veloce possibile vuole i processori migliori sul mercato e adesso un MacBook 15” può avere una Cpu con otto nuclei di elaborazione. Prima, no.

Il comunicato non lo dice, ma Gruber fa sapere che sono state modificate ulteriormente le tastiere dei MacBook Pro. Il cambiamento è invisibile e riguarda i materiali più che i meccanismi. L’affidabilità e la risposta delle nuove tastiere dovrebbero migliorare e intanto le procedure di sostituzione di tastiere difettose presso gli Apple Store sono state velocizzate ed esiste un [programma di sostituzione e riparazione della tastiera](https://www.apple.com/it/support/keyboard-service-program-for-mac-notebooks/) che prevede riparazione e sostituzione a costo zero nonché rimborso per chi avesse pagato.

Tutte buone notizie, che poi confermano lo stato effettivo delle cose: l’affidabilità delle nuove tastiere è minore di quella delle tastiere precedenti, ma i numeri, per quanto più alti in assoluto, sono minimi a livello relativo. Altrimenti gli Apple Store non reggerebbero l’urto e le iniziative sarebbero diverse.

Gruber prosegue elucubrando sui possibili annunci di hardware a Wwdc e in particolare di Mac Pro, atteso da lungo tempo. Inoltre commenta su una ammissione non ufficiale di Apple, che si è resa conto di dover dedicare più tempo e attenzione agli aggiornamenti hardware dei Mac.

Processori più veloci, migliorie al prodotto, aspettative future, un bel *post*: è così che dovrebbe funzionare. Buone vecchie abitudini che è bello riscoprire.