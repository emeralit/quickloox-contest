---
title: "Scrive bene chi scrive giusto"
date: 2016-10-14
comments: true
tags: [Jobs, Stem]
---
Attingo dal numero di ottobre, versione su carta, di [Scientific American](https://www.scientificamerican.com/), pagina 6, rubrica *Science Agenda*, titolo *Science Is Not Enough*, la scienza non è abbastanza. Originale:

>Steve Jobs, who reigned for decades as a tech hero, was neither a coder nor a hardware engineer. He stood out among the tech elite because he brought an artistic sensibility to the redesign of clunky mobile phones and desktop computers. Jobs once declared: “It is in Apple’s DNA that technology alone is not enough—that it’s technology married with liberal arts, married with the humanities, that yield us the result that makes our heart sing”.

Traduzione mia:

Steve Jobs, che ha regnato per decenni come un eroe della tecnologia, non era un programmatore né un ingegnere hardware. È emerso nell’élite tecnologica per avere portato una sensibilità artistica nella riprogettazione di goffi telefoni mobili e computer da scrivania. Una volta Jobs ha dichiarato: “Nel Dna di Apple è scritto che la sola tecnologia non è abbastanza; è invece la tecnologia sposata alle arti liberali e all’umanesimo a portare il risultato che fa cantare il nostro cuore”.

Ecco perché mi batto ovunque perché a scuola sia insegnata la programmazione, esattamente come l’italiano, le lingue, la matematica, la musica, l’educazione fisica e pure la religione (gli atei intelligenti che conosco hanno tutti una profonda conoscenza della religione). La programmazione fa parte del corredo standard, della cultura generale, e non è più una specializzazione.

Nel contempo bisogna conoscere perfettamente l’italiano e al meglio possibile qualsiasi lingua aggiuntiva; capire di storia e di filosofia; capire anche di tipografia e calligrafia. Mac ha avuto sempre il primato nella tipografia perché Jobs frequentò corsi universitari di calligrafia, a perdere, che tornarono a galla anni dopo. Capire Html, ma anche i Css. Conoscere la sintassi di AppleScript, ma anche quella del nostro scrivere. Un programma con un errore di sintassi funziona male o non funziona; l’italiano pure, con la sola differenza che nel caso del programma veniamo avvisati, dunque è più facile riparare.

Sta per cominciare la campagna natalizia e, se posso permettermi un suggerimento, alterniamo un regalo tecnologico e uno umanistico. Arriviamo al 2017 più consapevoli delle nostre doti umane oltre che più orgogliosi di quello che abbiamo imparato davanti allo schermo.