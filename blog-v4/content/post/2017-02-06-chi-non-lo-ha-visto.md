---
title: "Chi non lo ha visto"
date: 2017-02-06
comments: true
tags: [Brady, Patriots, Falcons, Super, Bowl, Surface]
---
Molte grandi squadre, come i New Englands Patriots, imparano a esercitare la propria superiorità attraverso una gestione molto efficiente della partita. Imparano cioè a vincere di misura, ma vincere sempre o quasi sempre, senza sprecare sforzi aggiuntivi oltre all’essenziale.

Queste squadre chiudono il loro ciclo con una partita dove gli avversari  improvvisamente rendono inadeguata la gestione efficiente di cui sopra. La disabitudine a compiere sforzi straordinari costa la partita decisiva. Questo è quanto NON è accaduto ai Patriots, che hanno visto i Falcons correre il doppio di loro, ma hanno applicato la loro economia di gestione e hanno raggiunto i supplementari, per vincere, dopo essere stati in uno svantaggio apparentemente tombale. Quelli che non hanno visto che cosa succedeva sono stati i Falcons.

Ugualmente, avevo [scritto in passato](https://macintelligence.org/posts/2016-10-19-tavoletta-rasa/) della sponsorizzazione della Nfl da parte di Microsoft, che ha pagato la lega del football per mettere in mano alle squadre tavolette Surface. Con risultati discutibili.

Ero curioso di vedere quanta rilevanza avrebbero avuto le tavolette durante la partita.

Posso essermi perso più di qualcosa, dato che nelle pause tendo a seguire il *second screen*. Sono altresì convinto di avere visto un’unica inquadratura con dentro un Surface in mano a un assistente allenatore, e niente più.

Si è vista più tecnologia durante lo show di Lady Gaga a metà partita.