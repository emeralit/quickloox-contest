---
title: "Il bicchiere mezzo"
date: 2014-04-18
comments: true
tags: [iPhone, BlackBerry, Htc, Sony, Samsung, Galaxy, Z30, 5s, 5c, Z1, Nexus, S4, Xperia, G2, Lg]
---
Accendere un iPhone 5C da sedici gigabyte appena tirato fuori dalla scatola permette di constatare che, al netto del sistema operativo e delle *app* preinstallate, ci sono a disposizione 12,6 gigabyte liberi.<!--more-->

Su un Nexus 5 sono un po’ meno, 12,28 gigabyte. Ma siamo lì. Come nel caso di iPhone 5S, 12,2 gigabyte. Differenze minime. Comincia a diventare interessante con un Xperia Z1 Sony, che lascia sgombri 11,43 gigabyte. Rispetto a iPhone 5C oltre un gigabyte in meno, su sedici teorici, su dodici pratici. Si comincia a sentire.

Su uno Z30 BlackBerry i giga liberi sono 11,2. Prendiamo uno One Mini Htc e si scende a quota 10,44. Rispetto al capoclassifica si viaggia verso il venti percento di capacità in meno. Un G2 Lg offre 10,37 gigabyte liberi su sedici teorici.

Il massimo, *pardon*, il minimo della [classifica di Which](http://blogs.which.co.uk/technology/phones-3/phone-storage-compared-samsung-s4-still-in-last-place/) viene raggiunto dal Galaxy S4 di Samsung: 8,56 gigabyte liberi, la metà del totale, due terzi di un iPhone 5C. Quattro gigabyte in meno. Che vengono pagati come se fossero a disposizione.

 ![Spazio libero negli smartphone](/images/which.jpg  "Spazio libero negli smartphone")

Il bicchiere mezzo pieno sarà quello del software a disposizione o dello spazio libero utilizzabile? Se il mio iPhone 5 mi offrisse quattro giga in meno di quelli che ha, sarei alla disperazione informatica.

Differenze come questa sono numerose e finiscono per sommarsi. Ogni tanto si confrontano i prezzi e va sempre bene. Se ogni tanto si guardasse anche a quello che arriva in cambio.