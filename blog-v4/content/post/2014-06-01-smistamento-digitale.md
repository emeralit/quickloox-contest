---
title: "Smistamento digitale"
date: 2014-06-01
comments: true
tags: [Thompson, Stratechery, Wwdc, iCloud, Android, iOS, Beats, CarPlay]
---
Ben Thompson ha scritto un [bell’articolo su Stratechery](http://stratechery.com/2014/wwdc-expectations/) con le proprie aspettative per [Wwdc](https://developer.apple.com/wwdc/), il ritrovo globale di chi programma cose Apple.<!--more-->

Trovo interessante la visione che vuole iPhone come nuova incarnazione del *digital hub*, il centro di smistamento della vita digitale, che una volta era Mac e poi è passato a essere nelle intenzioni iCloud.

Qui tuttavia non si parla di dati ma di interazione con gli apparecchi che abbiamo attorno e non sono necessariamente computer ma anche auto ([CarPlay](http://www.apple.com/it/ios/carplay/)), audio ([l’acquisto di Beats](https://www.apple.com/it/pr/library/2014/05/28Apple-to-Acquire-Beats-Music-Beats-Electronics.html)), elettrodomestici (si vocifera di qualcosa legato alla *smart home*, la casa intelligente) più naturalmente l’inevitabile televisore, orologio e chi più ne ha.

Android presuppone invece un mondo modulare dove gli apparecchi si parlano tra loro relativamente e non c’è un vero e proprio smistamento, perché tutto è delegato a un’idea di cloud basato su standard attraverso il quale avviene il dialogo.

Situazione ideale quando il mercato è maturo; quando non lo è, come abbondantemente ora, avviene una compartimentalizzazione. Ogni apparecchio parla con qualcuno o qualcosa, ma non con tutto e finisce come in una metropoli piena di quartieri periferici in assenza di un governo centrale, laddove ci sono quartieri ottimi, altri pessimi e manca il dialogo.

Un giorno gli standard saranno più evoluti e metteranno in crisi una proposta integrata come quella di Apple. Ma non è oggi e neanche questo biennio.
