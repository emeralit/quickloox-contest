---
title: "Another app bites the dust"
date: 2021-03-06T01:56:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Queen, tv, AirPort Express, YouTube, Google, Altroconsumo, iPhone 6] 
---
L’altroieri la mia tv ha perso un pezzo; mi ha informato che la app YouTube non è più supportata. È già successo e [se ne parla](https://9to5mac.com/2021/03/02/apple-tv-3-losing-signal/): la mia tv è quella di terza generazione, messa fuori produzione l’anno scorso dopo essere stata fortemente ribassata nel prezzo.

Se dura ancora un anno, cosa che accadrà certamente salvo problemi hardware, ne compirà dieci, essendo del 2012. Il fatto che manchi la app YouTube è dovuto alla cessazione di supporto da parte di *Google*.

Ma qualcuno, di fronte a uno scatolotto che dura dieci anni e perde per strada il software di altri, parlerà di obsolescenza programmata. Dopotutto [Altroconsumo ha lanciato una class action](https://www.agi.it/economia/news/2021-01-25/altroconsumo-apple-obsolescenza-programmata-11150136/) a proposito degli iPhone 6 limitati nelle prestazioni per tutelare batterie che accusano il peso del tempo, in modo che la batteria duri più a lungo. Secondo la *class action*, la mossa servirebbe a incoraggiare illecitamente l’acquisto di iPhone nuovi. Facendo durare di più quelli vecchi. Logica ineccepibile.

Ma già il fatto che si parli di obsolescenza programmata. A casa mia il Wi-Fi lo distribuisce una [base AirPort Express](https://en.wikipedia.org/wiki/AirPort_Express) che neanche ricordo più quando ho comprato. Potrebbe essere il 2007. Quattordici anni fa. Cattiva Apple, chissà per quale scopo oscuro la fai durare.

<iframe width="560" height="315" src="https://www.youtube.com/embed/rY0WxgSXdEE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*