---
title: "In viaggio con Wi-Fi"
date: 2019-03-05
comments: true
tags: [ Wi-Fi]
---
Passerò diversi giorni tra Stati Uniti e Messico e sono curioso soprattutto di verificare le condizioni dei Wi-Fi di bordo sugli aeromobili.

Sono passati i tempi quando dovevi spegnere spietatamente ogni apparecchio elettronico durante il decollo pena alluvioni e invasioni di cavallette. Apparentemente, gli aerei continuano a stare in aria nonostante la presenza di passeggeri che comunicano e, se disgraziatamente cadono, lo devono a fattori ben più importanti.

Sarà interessante. Spero.
