---
title: "Come volevasi impaginare"
date: 2015-04-09
comments: true
tags: [Word, Pages, Libreoffice]
---
Come [anticipato](https://macintelligence.org/posts/2015-04-04-serendipita/), inizio la traduzione di un tomo consistente sulla programmazione in iOS.<!--more-->

Mi aspettavo di utilizzare [LibreOffice](http://www.libreoffice.org). Invece l’editore mi ha messo a disposizione file perfettamente formattati in… [Pages](https://www.apple.com/it/mac/pages/).

La complessità del libro, più di trecento pagine fitte di listati, tabelle, rimandi, note a pié di pagina, inserti è rimarchevole.

L’idea di Word come software professionale contrapposto agli altri che non lo sono, di cui è impossibile fare a meno, che tocca usare anche controvoglia eccetera. Il novantanove percento di chi lo sostiene potrebbe farne a meno nel novantanove percento dei casi. E il novantanove percento delle volte sarebbe meglio per tutti.

Fatti i calcoli, Word serve davvero tre volte su cento. Il resto è illusione o autocondizionamento.

(Tutto ciò non toglie che sia cosa buona, giusta e doverosa sostenere attivamente [LibreItalia](http://www.libreitalia.it) e il software libero).