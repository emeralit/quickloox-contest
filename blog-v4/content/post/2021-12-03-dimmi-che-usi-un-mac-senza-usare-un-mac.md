---
title: "Dimmi che usi un Mac senza usare un Mac"
date: 2021-12-03T01:00:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Amazon, Ec2, Elastic Cloud, Mac mini] 
---
Lancio [una istanza di Mac mini M1 dentro Elastic Cloud 2 di Amazon](https://aws.amazon.com/ec2/instance-types/mac/): un Mac immateriale che esiste fino a che lo uso e posso eventualmente salvare sul servizio per riaccenderlo un’altra volta.

Un Mac che [pago a tempo](https://aws.amazon.com/ec2/dedicated-hosts/pricing/).

Dicono che i Mac siano computer chiusi? Beh, quello su Elastic Cloud lo configuri come ti pare e, se vuoi cambiare la quantità di Ram o di spazio di archiviazione, giri una (simbolica) manopola.

Tutta roba che esiste da anni, eh. I Mac Intel sono istanziabili su Amazon da tempo.

La notizia è che sono arrivate anche le istanze di Mac mini M1 e questo dice molto.

Finora Mac è stato per lo più l’epitome del computer individuale, non tanto inadatto o inadeguato quanto proprio estraneo alla logica del lavoro in cloud. Le politiche di licenza per Apple hanno enfatizzato fino agli anni recenti questa distinzione, impedendo a vario titolo la virtualizzazione di macOS.

Le cose sono cambiate e dicono anche di come stia andando Mac a livello di uso, diffusione, rilevanza: se Amazon si scomoda al livello di stare dietro allo hardware, c’è un mercato. Magari di nicchia, ma nicchia percepibile anche da un gigante.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._