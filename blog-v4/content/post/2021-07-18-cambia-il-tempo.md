---
title: "Cambia il tempo"
date: 2021-07-18T01:50:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Stephen Hackett, 512 Pixels, Kickstarter] 
---
Cominciamo a prepararci al meglio per la prossima stagione. Partecipiamo al Kickstarter di Stephen Hackett di 512 Pixels, che lancia il [calendario 2022 dello hardware Apple](https://www.kickstarter.com/projects/ismh/2022-apple-hardware-calendar).

Il calendario nasce dalla sua ampia raccolta di vecchi computer e apparecchiature varie marchiate Apple; per ogni mese ci sono tutti i riferimenti a date di uscita, di annuncio, di ritiro, corredati da belle foto.

Mancano ancora due settimane al completamento della raccolta fondi, che ha già abbondantemente superato la copertura minima richiesta. Il calendario, dice Hackett, è digitalmente già pronto e deve solo essere stampato, per chi lo volesse su carta. E sarà spedito a novembre, in tempo per entrare nell’anno nuovo già muniti del necessario.

Con sedici dollari ci si porta a casa un calendario 2022 per una volta davvero originale.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*