---
title: "Azioni coraggiose"
date: 2019-04-25
comments: true
tags: [Apple, Avalon]
---
La Apple di questo secolo dispone di denaro pronta cassa in quantità sconvolgenti e c’è sempre qualcuno che spiega all’azienda che altra azienda dovrebbe comprare.

Fortunatamente, come [racconta Above Avalon](https://www.aboveavalon.com), Apple impegna la propria fortuna in modo diverso: acquista… se stessa. Anno dopo anno, vengono acquistate da Apple azioni Apple sul mercato e si va verso i *quattrocento miliardi* di dollari.

È facile vedere la situazione da un punto di vista meramente finanziario. Il riacquisto delle azioni tende ad aumentare il valore intrinseco di quelle ancora sul mercato e a garantire dividendi più elevati agli azionisti (paradossalmente spendendo *meno* in dividendi, visto che circolano meno azioni).

Preferisco vederla in altro modo e sognare una Apple impossibilmente, ma idealisticamente proprietaria di se stessa al cento percento. Libera di prendere decisioni coraggiose e operare per il lungo termine e nel modo più giusto, anche se nel breve magari significa una contrazione dei guadagni, qualche recensione negativa oltre che miope, profezie di sventura da parte di quelli che sanno tutto e mai azzeccano alcunché.

Il sogno è chiaramente irrealizzabile, per un motivo: possiedo una azione Apple e non sono disposto a venderla. Ma resta una direzione assai più salutare e positiva di acquisizioni buone solo ad avere un titolo grosso sui media.
