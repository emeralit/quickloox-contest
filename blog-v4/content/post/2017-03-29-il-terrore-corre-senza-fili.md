---
title: "Il terrore corre senza fili"
date: 2017-03-29
comments: true
tags: [Wi-Fi]
---
Un’ora di attesa nel grande centro commerciale, pare il più grande d’Europa, certamente il più grande che io abbia mai visto in Europa. La sfrutto e provo il Wi-Fi libero a disposizione con iPad.<!--more-->

Quello ufficiale del centro gira a vuoto. Non succede niente e non ho rete. Come spesso accade, ricevo notifiche dai programmi, che però non visualizzano l’informazione piena per mancanza di rete.

Ok. Provo il Wi-Fi di De Donaldi. Si noti che la *food court* è vuota; intorno a me ci sono non più di quattro persone. Non ci sono problemi di banda o di affollamento. De Donaldi ha un sistema di accesso esoterico con password complicate, ma entro facilmente perché ne ho già una. Apro Editorial e preparo cinque righe di testo da salvare su Dropbox. Non si può. Arrivo allo stesso risultato via web dove tutto o quasi è permesso, ma sono un po’ irritato. Capisco le problematiche di un accesso pubblico e compreso nel prezzo; però un file da un chilobyte non dovrebbe essere un problema.

Improvvisamente la rete sparisce, ma vedo tre tacche sulla barra di iPad. Saranno passati dieci minuti. De Donaldi non fornisce più servizio e sono tornato automagicamente sotto la rete ufficiale del centro, che non va.

Allora provo l‘accesso del Mondo dei Media. Impossibile accedere. Troppo lontana, forse? Strano. In ogni caso, non va.

Ok. C’è sempre la Casa sulla Strada. Mi collego al loro Wi-Fi. Pochi minuti e compare una schermata: scarica la nostra app e fotografa il codice QR qui mostrato.

Vado per scaricare la app. Ma non ho rete…

Va da sé che avevo la connessione cellulare e ho lavorato con quella. Solo, mi chiedo che costi abbia mantenere un impianto Wi-Fi con login lenti e inefficienti, difesi da password bizantine che arrivano per Sms e uno dovrebbe memorizzare per digitare in campi testo microscopici, tipicamente su un computer da tasca.

Cinque minuti prima di andare via ho visto un movimento sullo schermo. La schermata di collegamento della rete ufficiale. Neanche tre quarti d’ora, ci ha messo.

Autenticazione facile con Twitter, buona velocità, un piacere. Peccato, se avessi avuto due ore sarebbe stata comoda. Prima di alzarmi, un ultimo test: salvo cinque righe di testo da Editorial su Dropbox.

Non si può. Parliamone, del fatto che è il 2017.
