---
title: "La lentezza del fossile"
date: 2023-06-24T01:14:17+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [Mastodon, Twitter]
---
Dicevo: *guarda che Twitter deve perdere almeno due ordini di grandezza e Mastodon deve guadagnarne altrettanti, prima di fare confronti*. Altrimenti è come farli tra mele e pere. Incidentalmente, è molto difficile che questo succeda in tempi rapidi e per Mastodon il compito è di difficoltà ancora maggiore.

Passano i mesi e qualcuno si chiede, anche con un buonissimo post, [perché la migrazione #TwitterMastodon è fallita](https://blog.bloonface.com/2023/06/12/why-did-the-twittermigration-fail/). Sarebbe troppo facile fare appello al buonsenso e fare notare che nessuna migrazione era in programma; semplicemente si è dato per scontato che tutti avrebbero abbandonato Twitter (ci sta) e che tutti avrebbero cercato di rivivere la stessa esperienza su un aggeggio che più o meno funziona in modo identico.

La seconda parte è puro *wishful thinking*: si sarebbe andati su Mastodon perché era open source e soprattutto perché ci andavano anche gli altri. Questa voglia di rivoluzione, la suggestione delle masse virtuali che con il loro movimento cambiano il corso della Storia, ogni tanto ritorna. Le circostanze sono diverse, il clima patetico sempre quello, le aspirazioni del *popolo di Internet* puntualmente frustrate dalla realtà.

Non c’era bisogno di talento per la profezia, bastava guardare i numeri; quando si parla di centinaia di milioni di persone, non è che lanci una campagna o sei trendissimo in quel momento e, schiocco di dita, le sposti. La gggente è pigra, non vuole imparare un’altra interfaccia, vuole cose facili e rigetta le cause di principio a buon mercato. Sui grandi numeri, ovvio, ma è così.

Nel post si trova molto più di questo. Aggiungo, prima di chiudere, che sono un sostenitore conclamato dell’open source e sono sempre lieto di utilizzare software libero dove è opportuno. il fatto è che, quando dici *vai su Mastodon che è open source e Twitter invece no*, non stai convincendo la gente a usare un programma, ma un *servizio*. La valutazione da parte della gente si articola su quello che ricava dal servizio, non sul poter guardare dentro il software (cosa che il novantanove virgola novenovenovenovenovenove non sa fare, non ha interesse a farlo e, se qualcuno gli desse una mano, comunque non capirebbe che cosa sta leggendo). Questo vale anche per la decentralizzazione, l’assenza di pubblicità e varie altre situazioni.

Magari Twitter finirà per crollare sotto il peso del suo ingombrante proprietario oppure Mastodon azzeccherà una strategia clamorosamente originale per guadagnare decine di milioni di seguaci *e anche* la capacità di scalare la struttura per accoglierli degnamente tutti. Il futuro è imprevedibile. La prossima volta che qualcuno invita alla transumanza, però, ricordiamo che in informatica le tendenze a breve termine sono sempre più lente a dipanarsi di quanto ne dia conto il commentatore intermettiano tipico bisognoso di tanti clic e di spararle grosse per ottenerli. 