---
title: "Forward to School"
date: 2018-03-24
comments: true
tags: [Stefano, iPad, Notability, Pencil]
---
Ricevo da **Stefano** e con gratitudine riproduco:

§§§

Per vari motivi in questo periodo sono tornato sui banchi di scuola, con appunti, slide, compiti a casa eccetera, dopo più di vent’anni di assenza.<!--more-->

Mi ritrovo con iPad, Apple Pencil e [Notability](http://gingerlabs.com) che svolgono egregiamente attività di registrazione audio della lezione sincronizzata con le note prese sulle slide e incorporate con screenshot di commenti alla lavagna del professore di turno.

Vent’anni fa tutto questo non esisteva: al massimo registrazione audio e fogli di carta stampati sui quali trascrivere le proprie note.

È cambiato tutto, in meglio: seguire una lezione risulta maledettamente più facile, anche a posteriori. Questa è vera (silenziosissima) innovazione.

§§§

Mi viene in mente il [Newton MessagePad 2100](https://512pixels.net/2015/08/apple-newton-guide/) su cui ancora esistono appunti presi durante riunioni redazionali dei tardi anni novanta, scritte in inchiostro digitale. E le dispense fotocopiate di appunti a mano che giravano, a costi sovente stellari, in università.

Questa è davvero l’innovazione silenziosa e viene difficile credere che a parità di tempo e capacità dello studente, alla lunga questo equipaggiamento non sposti in alto l’asticella dei requisiti minimi per fare il meglio a scuola.
