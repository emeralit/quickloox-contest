---
title: "L’Internet delle persone"
date: 2017-03-19
comments: true
tags: [Martina, IoT]
---
Mercoledì 29 marzo mi troverò al Copernico di Milano, uno dei *coworking* in voga nella citta, per la [presentazione di un libro](https://www.eventbrite.it/e/biglietti-come-usare-al-meglio-internet-delle-cose-32719026489) di cui mi è accaduto di scrivere la prefazione.

(La prefazione è il compito più ostico da svolgere per un libro perché non ci guadagni niente, devi spiegare in due pagine concetti che l’autore ha spiegato in cento e, se il libro non funziona, è colpa tua).

Il tema è l’Internet delle cose, solo apparentemente tangenziale al mondo Apple: una auto con CarPlay o watch che in aeroporto avvisa dell’inizio degli imbarchi sono esempi ancora acerbi ma già saporiti per chi vuole rendersi conto del fenomeno.

Se qualcuno passasse in zona (vicino alla stazione Centrale) dalle 18:30, sono lì. Una dose di Internet delle persone di tanto in tanto, nonostante una crescente misantropia di fondo, continua a riuscirmi gradita.
