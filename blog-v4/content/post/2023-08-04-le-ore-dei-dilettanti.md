---
title: "Le ore dei dilettanti"
date: 2023-08-04T00:35:44+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [A2, Strozzi, Marin, Roberto Marin, Filippo Strozzi, Stage Manager]
---
Ci volevano [Filippo](https://www.avvocati-e-mac.it) e [Roberto](https://marchdotnet.wordpress.com) per farmi [riprovare Stage Manager su iPad](https://macintelligence.org/posts/2023-03-31-e-un-caro-saluto-al-manager/), grazie alla registrazione di una nuova puntata di [A2](https://a2podcast.fireside.fm).

Forse ho trovato un possibile caso d’uso. Forse la funzione è sufficientemente migliorata da quando l’ho abbandonata.

Di sicuro ho ritrovato tempo di qualità con ottimi amici, quando iniziamo a parlare di Wwdc e a un certo punto bisogna ammettere che è il momento di avere il coraggio di tagliare. È che le cose da dire sono tante e dirle nella compagnia giusta è piacevole; il tempo passa e lo si capisce solo a posteriori.

Insomma, abbiamo impegnato le ore dei dilettanti. Nel senso del diletto, che ci si diverte molto. Quanto al dilettantismo, se l’utente medio Mac ne sapesse la metà di Roberto o Filippo, Apple non saprebbe più che cosa inventare pur di offrire qualcosa di nuovo.