---
title: "I nuovi vestiti dell’imperatore"
date: 2018-07-22
comments: true
tags: [Microsoft, Slashdot]
---
Nel cercare di obliterare qualsiasi forma di vita software fuori da sé, Microsoft svolse assolutamente il proprio lavoro. Solo che violò le leggi antitrust, strangolò decine di concorrenti invece di offrire prodotti migliori, cercò di appropriarsi di Internet e in generale di farsi il deserto intorno. Cercare di dominare il mondo in sé è legittimo; la slealtà, il gioco sporco, i comportamenti illegali non lo sono. Microsoft non era un problema per quello che voleva, ma per come ci lavorava.

Oggi sarebbe una nuova Microsoft, tutta apertura, open source, vogliamoci bene, tutti benvenuti.

E i nuovi sistemi? Solo più subdoli, più mediati, più sottotraccia. Per esempio, ecco [uno dei sommari su Slashdot oggi](https://ask.slashdot.org/story/18/07/21/0336258/ask-slashdot-should-i-ditch-php).

Un *lettore di lunga data* si dichiara *sempre più frustrato dagli ignoranti e dagli sprovveduti nella comunità di PHP*. Viene fuori brutto codice disfunzionale, che mai viene migliorato o aggiornato eccetera.

Risponde un altro lettore:

>Penso che .NET sia un linguaggio molto più chiaro per lavorare, con l’eccellente ambiente di programmazione e debugger Visual Studio di Microsoft […] molti grandi progetti nella mia città assumono sviluppatori .NET il quale, essendo un linguaggio a forte tipizzazione, produce un codice generalmente migliore di quello di PHP.

C’è tutto, al posto giusto: il problema, la sua soluzione, l’accenno ai posti di lavoro, quello agli strumenti. In altre parole non è una scambio di opinioni come tanti su Slashdot. L’editor ha composto un perfetto contenuto commerciale.

È pubblicità.

La pubblicità erogata come contenuto si chiama *nativa*. Il problema è che, in quanto pubblicità, per quanto somigli a contenuto, si dichiara contenuto commerciale in un modo o nell’altro. Qui invece abbiamo un sommario di Slashdot esattamente come gli altri.

Se Microsoft ha pagato per avere pubblicità occulta *usa .NET e butta via PHP*, lo ha fatto in modo altrettanto occulto, che sarebbe grave.

Se non ha pagato e il tutto è spontaneo, siamo davanti a propaganda così subdola e ben organizzata da convincere le persone a pensare nella direzione che desidera. Un bel successo per l’azienda, vagamente preoccupante per le persone libere.

Se poi domani davvero PHP finisse dimenticato, ecco scomparire una comunità che al momento Microsoft non controlla e non dirige, a favore di strumenti propri. Dici che ci vorrà un sacco di tempo? Forse in una sala riunioni qualcuno ha posto la stessa obiezione e gli è stato risposto *possiamo permetterci di lavorare a lungo termine*.

Microsoft voleva controllare il mondo del software ed eliminare ogni resistenza. Ha solo cambiato abito.