---
title: "Prove di stregoneria spicciola"
date: 2021-01-15T02:30:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Coleslaw, QuickLoox, Lisp, rsync, Common Lisp, emacs] 
---
Domani torno al solito formato e mi limito a commentare qualcosa a margine rispetto alla transizione. Solo per oggi, una piccola soddisfazione per essere riuscito ad arrivare a questo [primo prototipo](https://macintelligence.org/posts/Il-giovedì-delle-ceneri.html).

Non sono ancora al lavoro in Common Lisp come spero di arrivare a fare, però ho iniziato a lavorare sul file di configurazione, che non è esattamente trasparente, e ho un comando rsync di stregoneria spicciola che porta sul server del sito il risultato della generazione del blog senza combinare pasticci o, almeno, facendone pochi e poco visibili per ora.

La lista dei *to do* è molto lunga. Priorità ora: portare nel nuovo motore tutta la base dati precedente, anche grezzamente, e reinserire la possibilità di commentare. Probabilmente partirò da questa. Qui sotto si vede probabilmente Disqus, ma non mi piace e cercherò qualcosa di diverso. Non perderci tempo.