---
title: "Questione di buonsenso"
date: 2020-02-15
comments: true
tags: [Apple, Iowa, iPad, Nevada, Caucus, Google, Forma, Voorhees, MacStories]
---
Per evitare i [disastri avvenuti in Iowa a causa di una app mezza cruda](https://www.npr.org/2020/02/12/804825254/after-iowa-debacle-lefts-tech-experts-say-dems-need-a-strategic-shakeup?t=1581724061816), in Nevada hanno deciso di lavorare sulle primarie presidenziali democratiche in modo semplice e pratico: [un iPad per seggio e Google Forms](https://www.cnet.com/news/nevada-democrats-to-use-google-forms-based-calculator-to-track-caucus/).

Apple ha varato [Swift Playgrounds per Mac](https://www.apple.com/swift/playgrounds/). Era da tempo una cosa logica. Avrebbero dovuto anzi farlo da molto prima, senza aspettare di approfittare di [Catalyst](https://developer.apple.com/mac-catalyst/).

John Voorhees ha scritto su *MacStories* un [articolo talmente bello su come usare i Comandi rapidi in iOS per generare link in modo automatico a partire da testo Markdown](https://www.macstories.net/stories/shortcuts-rewind-linking-tricks-using-markdown-and-rich-text/), che a leggerlo con attenzione diventa un corso per imparare a lavorare degnamente con i comandi rapidi. Difficilmente si troverà qualcosa di più sistematizzato e coerente su web; qualunque persona seriamente interessata a ricavare il massimo dal proprio iPad dovrebbe dare almeno una scorsa alla pagina. Perché i Comandi rapidi sono un amplificatore pazzesco di possibilità.

Tre casi di normalità lineare talmente dissonanti con la mentalità comune da fare notizia.
