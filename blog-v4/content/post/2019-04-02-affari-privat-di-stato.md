---
title: "Affari privati di stato"
date: 2019-04-02
comments: true
tags: [Fabio, Andy, Android]
---
Di app che pongono un rischio privacy se ne trovano un soldo la dozzina; diverso è il caso in cui il committente è nientemeno che lo stato (rigorosamente con la minuscola come allusione a che ne penso).

Me l’hanno segnalata in due la notizia, prima **Fabio** e poco dopo **Andy**, che mi hanno indirizzato presso [Ansa](http://www.ansa.it/sito/notizie/tecnologia/tlc/2019/03/30/sofware-ha-spiato-centinaia-italiani-e-made-in-italy_5321697d-355c-4cee-991d-909e15228a35.html) e [il Giornale](http://m.ilgiornale.it/news/2019/03/30/centinaia-di-utenti-italiani-intercettati-per-errore-con-lapp-del-cell/1671445/), ambedue stranamente capaci di fornire descrizioni dei fatti che valga la pena di leggere.

Niente di strano che un apparato statale abbia bisogno di strumenti per intercettare, solo che l’approssimazione e l’incuria con cui è stata gestita, per così dire, la diffusione di certe applicazioncine è tutta italiana.

Come Fabio e Andy mi hanno fatto notare, c’è un modo di ritrovarsi cittadini almeno un poco più liberi: usare un iPhone. Le piacevolezze di questo software-spia sono riservate interamente ai felici frequentatori di Google Play Store.

Vedo all’orizzonte un epilogo spiacevole: finirà che verremo giudicati evasori, non fiscali ma personali, per non mettere le nostre faccende a disposizione del primo funzionario pigro, incompetente o corrotto che capita, per giunta senza giustificazione. Pazienza, correrò il rischio.
