---
title: "Programmare per dimenticare"
date: 2021-04-15T00:45:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Scripting, programmazione, Perl, Wired, Larry Wall] 
---
Oltre a [giocare per entrare in uno stato mentale speciale](https://macintelligence.org/posts/Giocare-per-dimenticare.html) (vorrei dire *superiore* ma si può fraintendere), si può fare anche dell’altro.

[Scrivono su *Wired*](https://www.wired.com/story/healing-power-javascript-code-programming/) che *per alcuni di noi – isolati, felici nell’oscurità – il codice è una terapia, una via di fuga e un percorso verso la speranza in un mondo travagliato*.

Dire *terapia* è esagerato, stare nell’oscurità mi rende generalmente poco felice, ma mi è impossibile dissentire da una affermazione come questa:

>Scrivere codice allevia la pressione perché può fornire controllo in momenti nei quali il mondo sembra entrare in una spirale negativa. Parlandone in forma riduttiva, la programmazione consiste nel risolvere piccoli enigmi. Non enigmi inerti come quelli su una rivista, ma enigmi che che respirano con una strana forza vitale. Enigmi che fanno accadere cose, che completano compiti, automatizzano il lavoro tedioso o permettono di pubblicare parole a disposizione del mondo.

Le mie competenze di programmazione rivaleggiano con quelle di uno scacciamosche (manuale), però quando sono riuscito a completare del codice funzionante, sì, è stato così.

Tutto questo ha a che vedere con le [tre virtù del programmatore](http://threevirtues.com), enunciate da Larry Wall, il creatore di [Perl](https://www.perl.org):

1. **Pigrizia**: la qualità che ci spinge a sforzarci al massimo per ridurre il consumo energetico complessivo. A scrivere programmi per risparmiare fatica, che altri troveranno utili, documentati in modo che non ci sia da rispondere a troppe domande.
2. **Impazienza**: la rabbia che si prova dinanzi alla pigrizia del computer e ci fa scrivere programmi che non si limitano a reagire ai nostri bisogni, ma li anticipano. O almeno ci provano.
3. **Hubris**: la qualità che ci fa scrivere (e curare) programmi di cui gli altri non vorranno dire cose cattive.

A loro volta le tre virtù del programmatore hanno a che vedere con doti particolarmente utili in questo momento davvero difficile per molti, doti di cui parlerò domani se mi ricordo.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*