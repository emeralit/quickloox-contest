---
title: "Sono traguardi"
date: 2019-06-15
comments: true
tags: [Python, Mastro35, Real, Terminale, argparse]
---
Interrompiamo momentaneamente le trasmissioni per fare grossi complimenti a **Mastro35**, che ha saputo pubblicare un proprio articolo su *Real Python*.

In inglese, fortemente tecnico, nel rispetto di linee guida rigide come sono solitamente quelle dei media anglosassoni, retribuito, su una testata autorevole e curata.

In Italia non sono molti quelli che possono ambire a un traguardo così e soprattutto raggiungerlo parlando di [come realizzare interfacce a riga di comando in Python con argparse](https://realpython.com/command-line-interfaces-python-argparse/). Non è il tema libero insomma, c’è da conoscere bene l’argomento.

Bravissimo l’autore e compito a casa per tutti: leggere l’articolo.
