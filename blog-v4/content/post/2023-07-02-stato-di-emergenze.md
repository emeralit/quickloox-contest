---
title: "Stato di emergenze"
date: 2023-07-02T23:54:40+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [LLM, AI, intelligenza artificiale]
---
Nell’ennesimo scambio di opinioni su quanto siano intelligenti le intelligenze diverse dalla nostra, mi è stato portato con entusiasmo l’esempio delle *abilità emergenti*, che un modello linguistico non ha a priori e sviluppa attraverso… attraverso cosa?

Mi è stato passato questo [elenco di 137 abilità emergenti](https://www.jasonwei.net/blog/emergence), la cui definizione è *non presenti nei modelli piccoli ma presenti in quelli grandi*.

Non sono certamente la persona con più esperienza di utilizzo dei modelli attuali, però qualcosa l’ho fatto e sono un po’ scettico, per dire, sull’effettivo emergere del *riconoscimento dell’ironia* o dell’*ordinamento delle parole*. Quest’ultimo fatico a vederlo come emergente; o lo fai giusto o lo fai sbagliato, o non è un ordinamento (ho appena chiesto al solito noto di ordinarmi alfabeticamente i giorni della settimana, in inglese, e ha sbagliato).

Poi ci sono varie altre abilità, come *conoscenza dell’hindu*, *proverbi inglesi*, *verifica dei fatti sulla vitamina C*, *comprensione delle metafore*, per le quali azzarderei una spiegazione che a me pare ovvia: nel modello piccolo non ci stavano, sono stati messi nel modello più grande e quindi *emergono*. Anche qui fatico a vederlo come qualcosa di clamoroso o anche sottile; se domani in un modello più grande ci fossero i post del mio blog, sarebbe in grado di fare fare riferimenti al mio blog. Ma è come dire che ha studiato una cosa in più e la sa, non è una abilità emergente.

Ognuno potrà farsi la propria idea su tutte le altre categorie e naturalmente compiere le proprie prove. Rimango dell’idea che, se in una prova di aritmetica il sistema dà la risposta esatta il novanta percento delle volte, non c’è una abilità emergente, ma una base dati sufficiente ad aumentare la fortuna probabilistica. Per un software, sofisticato quanto si vuole, non dare la risposta esatta a un quesito numerico è una dimostrazione di ignoranza più che di intelligenza.

La cosa peggiore non è tanto tutto questo, quanto che ciascuna esperienza concreta con il sistema mi risulta al più deludente. Il tono di voce è ampolloso e a volte pedante e, mentre nell’uso per avere uno scheletro di struttura su cui lavorare non c’è alcun problema, come con qualsiasi strumento, quando provo inconsciamente e stupidamente a scrivere un input come se là dietro ci fosse davvero qualcuno o qualcosa, la conversazione è scialba, scontata, piena di buchi anche quando formalmente arriva una risposta compiuta.

La vera emergenza non è quelle delle abilità: è che sono sistemi noiosi e anodini, deprimenti. E vengono esaltati non per quello che sanno fare, ma per quello che si vorrebbe sapessero fare. Che non sanno fare.