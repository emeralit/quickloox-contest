---
title: "Giorni contati"
date: 2024-02-14T00:31:48+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Backblaze]
---
BackBlaze ha pubblicato il [rapporto sui guasti dei suoi dischi di backup](https://www.backblaze.com/blog/backblaze-drive-stats-for-2023/) durante il 2023, che copre duecentosettantamila dischi per quasi mezzo miliardo di giorni/disco. Se qualcuno ha accumulato dell’esperienza empirica sul funzionamento degli hard disk, sono loro.

Forse la cosa più interessante è il motivo per cui alcuni dischi in dotazione non vengono conteggiati. Una certa marca ha concluso il 2023 con zero guasti, ma è rimasta esclusa dal rapporto perché i dischi erano solo duececentoquattro - troppo pochi – e tutti installati nella seconda metà dell’anno, così c’erano solo cinquantamila giorni/disco di funzionamento. Assolutamente troppo pochi: tutte le marche e i modelli con meno di duecentomila giorni/disco di operatività sono stati esclusi dalla statistica.

I dischi in fase di testing per capire se ammetterli in fase di produzione non vengono considerati, così come quelli esposti a temperature di lavoro molto alte – finiscono in una categoria a sé – e i modelli presenti in meno di sessanta esemplari.

Se qualcuno tenesse statistiche di funzionamento sui quattro dischi di casa sua, prima di dare consigli.