---
title: "Tutto si tiene"
date: 2020-06-23
comments: true
tags: [Apple, WWDC, Arm, Cook, Federighi, Apple, Park, Big, Sur]
---
Ore dopo il keynote mi accorgo che sì, il cambio di piattaforma è storico, gli annunci sono tutti all’altezza e che però a interessare è altro.

A nessuno è sfuggita l’ambientazione del keynote. Il mondo si adattava a rinunciare temporaneamente agli spazi fisici ed ecco l’accento forte su Apple Park, il sottolineare la propria impronta dall’alto, il luogo dove tutto accade, persino il laboratorio segreto arredato per l’attività di ingegneri e progettisti.

Un altro accento sulle persone, che si mostravano con orgoglio percepibile. L’azienda esperimento sociale, inclusiva al mille per cento, di nuovo si materializzava, questa volta attraverso i corpi più che gli edifici.

E poi l’accento, chiaro, sulle trasformazioni del software, che lentamente porta verso un approccio visivo comune nonostante tante differenze dell’hardware.

La chiave di tutto è l’impressionante coerenza dell’insieme, che qualunque strada prenda, buona o cattiva, è dichiarata, condivisa.

In molti hanno ironizzato sulla iOSizzazione dell’ecosistema. Qualcuno si chiede chi svilupperà buon software per Mac quando è così facile trasformare un buon programma per iPad in un cattivo programma per Mac. Un cambiamento di questa portata si porta dietro interrogativi, per forza. Comunque sia, avviene un passo per volta, partendo da tante identità che convergono. Dietro la testa di Craig Federighi si trovavano iOS, tvOS, watchOS, iPadOS, macOS. Un approccio certo diverso da chi ha voluto mettere un solo sistema _everywhere_ con risultati, diciamo, non esaltanti.

C’è tanto da studiare questa settimana, per capire dove sta il gusto e se c’è fatica nascosta. Certo, questa Apple che non innova più, persa senza Steve Jobs, dedita al solo iPhone, nemica dei professionisti, con in mano una famiglia di processori progettata in casa per servire computer, terminali tascabili, orologi, console per la tivù e tavolette. Se la coerenza premiasse anche il parlare. Se tutto si tenesse anche nelle teste.

Dichiaro: bella partenza e la strada annunciata merita fiducia.