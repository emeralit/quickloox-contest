---
title: "Algoritmo dà, algoritmo toglie"
date: 2022-08-27T17:06:28+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Apple Watch]
---
Così contento che watch mi abbia [riconosciuto l’atto del nuotare](https://macintelligence.org/posts/2022-08-12-agosto-apple-watch-ti-riconosco/) ed ecco che il fedifrago non si accorge quando sto facendo due tiri a canestro.

Potrebbero esserci dei distinguo, per il fatto che era la prima volta in assoluto e poi il mio ritmo non era effettivamente da playoff NBA.

Rimane il fatto che sono dovuto andare mestamente nell’area degli *altri* workout e scegliere esplicitamente *basketball*.

O è il fisico che non è più quello di una volta.

O sono finite le scuse per watch.