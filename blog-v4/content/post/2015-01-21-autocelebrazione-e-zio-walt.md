---
title: "Autocelebrazione e zio Walt"
date: 2015-01-21
comments: true
tags: [Mossberg, MacBookPro, Re/Code]
---
Chiedo scusa per fare un *post* egoisticamente autocelebrativo.<!--more-->

Dopo anni passati a scrivere *compra quello che ti serve quando ti serve e se non c’è quello che ti serve, aspetta* leggo l’ultimo [articolo di Walt Mossberg](http://recode.net/2015/01/20/you-dont-have-to-ride-the-tech-upgrade-treadmill/) su Re/Code, una delle icone del giornalismo tecnologico mondiale, decano della categoria:

>Se sei a posto con quello che usi, se funziona bene per te, non aggiornare.

Ciliegina sulla torta, zio Walt racconta di un suo collega che scrive *su un MacBook Pro del 2009, dopo avere sostituito disco e batteria*. Beh, *anch’io* scrivo su un MacBook Pro del 2009, prossimo a compiere sei anni, cui ho sostituito disco e batteria, e che finora non ha perso un colpo. Ho sentito l’empatia scorrermi lungo la schiena.

Magari, adesso che lo ha scritto lui, c’è persino rischio che qualcuno ci pensi.