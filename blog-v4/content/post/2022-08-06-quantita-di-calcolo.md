---
title: "Quantità di calcolo"
date: 2022-08-06T01:05:57+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Emacs]
---
Posto che ci sia ancora (e che altrimenti [si installa facile](https://willschenk.com/articles/2021/getting_emacs_working_on_osx_monterey/)), nel tuo Mac c’è un unico software che consente di usare una calcolatrice, una calcolatrice con notazione polacca inversa, una calcolatrice da tavolo avanzata, una modalità abaco (!) e la possibilità di installare un foglio di calcolo, una addizionatrice di colonne di numeri, una calcolatrice di interessi sui mutui, un sistema per operare su matrici numeriche e financo un software per sistemare punti e virgole decimali al posto giusto.

[Una risposta unica](https://www.emacswiki.org/emacs/?action=browse;oldid=EmacsCalculators;id=CategoryCalculators). Qual è?