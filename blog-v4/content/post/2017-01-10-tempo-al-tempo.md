---
title: "Tempo al tempo"
date: 2017-01-10
comments: true
tags: [watch, Stefano]
---
Scrive **Stefano:**

**Ecco qui la mia personale prova sul campo:**

Apple Watch Nike+<br />
Modello: Space Gray Aluminum Case with Black/Cool Gray Nike Sport Band - 42 Case<br />
OS: watchOS 3.1

Giorno 1: 100 percento di carica alle nove del mattino<br />
Attività: notifiche Facebook + WhatsApp + Times of India + LinkedIn<br />
Notifiche mail di Exchange + Mail (circa 40 notifiche ricevute)<br />
Tracciamento battito cuore tramite: HeartWatch<br />
Palestra: 20 minuti di corsa + 20 minuti ellittica + 20 minuti cyclette con monitoraggio workout tramite HeartWatch<br />
Indossato tutta notte in modalità Airplane dalle 23:00 alle 8:45 per continuo tracciamento del sonno tramite AutoSleep.

Giorno 2<br />
Attività: notifiche Facebook + WhatsApp + Times of India + LinkedIn<br />
Due telefonate di due minuti ciascuna dal polso<br />
Notifiche mail di Exchange + Mail (circa le stesse quantità di email del giorno precedente).<br />
Palestra con monitoraggio workout con HeartWatch per circa un'ora e mezza.<br />
Attività: notifiche Facebook + WhatsApp + Times of India<br />
Notifiche mail di Exchange + Mail (circa 40 notifiche ricevute)

Morale: ho indossato Apple Watch per trentasei ore ininterrotte, notte inclusa, con due attività di palestra circa un’ora e mezza l’una ed un utilizzo intenso di notifiche durante il giorno. Alle 21:16 del giorno 2 ho raggiunto il cinque percento di batteria: **vi invito a fare meglio.**

Direi che le critiche ad watch per la sua richiesta, beh, quasi quotidiana di ricarica perdevano di vista l’utilità potenziale dell’attrezzo e l’effettiva portata del problema.
