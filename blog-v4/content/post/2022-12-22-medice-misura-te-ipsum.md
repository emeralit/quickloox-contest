---
title: "Medice, misura te ipsum"
date: 2022-12-22T15:22:58+01:00
draft: false
toc: false
comments: true
categories: [Internet, Hardware]
tags: [Apple Watch]
---
Giusto ieri spiegavo a un anziano tra lo scettico e lo stupefatto che, volendolo, si ottiene un elettrocardiogramma – per quanto base – dall’orologio. Certamente semplificato e meno denso di dati di un elettrocardiogramma tradizionale, ma attendibile.

Ora uno studio annuncia una percentuale molto alta di successo [nell’uso di watch per individuare livelli anomali di stress](https://www.frontiersin.org/articles/10.3389/fdgth.2022.1058826/full#B10).

Sette anni fa sarebbe suonato come fantascienza. Qualcuno si azzarda a segnare i confini di che cosa potrebbe fare a livello medicale un watch in uscita nel 2029?

Come al solito, il punto non è sostituire questo o sconvolgere quell’altro, ma dare alle persone autorità sui propri dati. E in questo Apple, con tutte le critiche avanzabili, non ha rivali.

A noi usare con misura le capacità di misurazione, senza diventare dipendenti né pretendere di sostituirsi a una persona competente. Nel momento in cui serve la persona competente, però, se abbiamo misurato bene resteremo padroni di noi stessi grazie alla tecnologia. Non nonostante, né contro.

È qualcosa di molto grande.