---
title: "Prepararsi al dopo"
date: 2019-01-04
comments: true
tags: [iPhone, Cina, Engadget, Asymco, Dediu, WeChat]
---
Grande risalto all’[annuncio di Apple](https://www.apple.com/newsroom/2019/01/letter-from-tim-cook-to-apple-investors/) per cui le vendite di iPhone saranno ben sotto le previsioni originarie e posso capire che i miliardi di fatturato mancanti stimolino i polpastrelli. Eppure sembra che nessuno voglia veramente inquadrare la notizia nella sua giusta dimensione, men che meno Engadget quando titola [Apple sa che l’epoca del cambio annuale di iPhone è finita](https://www.engadget.com/2019/01/03/apple-iphone-upgrades-slowing-down/).

Non è neanche mai iniziata. Lo scorso marzo Horace Dediu di Asymco aveva stimato [la vita media di un apparecchio Apple](https://macintelligence.org/posts/2018-03-04-curve-pericolose/) in poco più di quattro anni, aggiungendo che due terzi di tutti gli apparecchi mai venduti da Apple erano in attività.

Ognuno arriva con la spiegazione che aveva in mente già *prima* e attendeva solo l’occasione per esprimere: non c’è più innovazione dai tempi di Steve Jobs, i prezzi sono troppo alti, la qualità non è più la stessa, la concorrenza è sempre più vicina e vai con il valzer del disco rotto, ripetere la stessa cosa ogni anno la fa sembrare un po’ più vera.

Nessuno cui venga in mente che, se fossero vere *tutte* queste motivazioni, ognuna di esse sarebbe fin troppo blanda e, quindi, brandirla a mo’ di clava come se fosse decisiva si rivelerebbe comunque sbagliato.

Una cosa che Apple certamente sapeva, ma Engadget si è ricordata solo dopo il titolo, è che il recente programma di ricambio batteria di iPhone a prezzo ridotto ha spinto molte persone a cambiare batteria invece che comprare un iPhone nuovo. Altra cosa nota sono i problemi in Cina, che esistono da sempre perché in Cina iOS non ha un reale vantaggio competitivo: lì conta solo usare WeChat, usato in mille modi che trascendono la messaggistica, e la piattaforma su cui gira conta poco. E la Cina attualmente è l’unico mercato possibile dove cercare crescita reale.

Come al solito Apple si distinguerà, nel bene o nel male, non tanto per quanti iPhone continuerà o meno a vendere, ma per come si preparerà al futuro dopo iPhone, che arrivi domani o tra un quarto di secolo.