---
title: "Il sorpasso"
date: 2017-11-24
comments: true
tags: [iPod, watch, Asymco, Dediu]
---
Domande meno ovvie che sono quelle più interessanti: quando watch supererà iPod?<!--more-->

[Se lo chiede](http://www.asymco.com/2017/11/08/when-watch-surpassed-ipod/) Horace Dediu su Asymco. In termini di fatturato, è già accaduto. Parlando di unità vendute, ci vuole tempo. Con un buon ritmo di crescita, potrebbe accadere nel 2020, più probabilmente uno o due anni dopo. iPod arrivò a vendere 55 milioni di unità l’anno, mentre attualmente watch – stima – è a sedici milioni.

Dà da pensare che oggi iPod sia assente dalla percezione pubblica e non più a catalogo. Quasi come se non fosse mai esistito. Ai tempi, aveva assunto proporzioni iconiche: Apple era l’azienda che produce iPod.

Questo era naturalmente già successo: Apple è stata l’azienda di Apple ][ di Macintosh, di iPod, oggi di iPhone. E sempre qualcuno ha sollevato il problema di questa eccessiva dipendenza dal singolo prodotto. Che succederebbe se improvvisamente…?

iPod è scomparso. Non se ne accorge nessuno. Se scomparisse iPhone, istantaneamente, con un colpo di bacchetta magica, ovunque, Apple sarebbe più grande di Microsoft e non di poco.

Proprio in Microsoft ai tempi di iPod, c’era Bill Gates. Disse che quel fenomeno non avrebbe potuto durare. E infatti è scomparso. Però ha visto galleggiare sul fiume i cadaveri di almeno tre strategie musicali diverse escogitate da Bill Gates

Oggi c’è ancora qualcuno che chiede provocatoriamente *a che serve watch*. Dediu ritiene (e convengo) che difficilmente Apple diverrà nel linguaggio comune l’azienda che fa watch. Ma se superasse negli anni tutti e diventasse il prodotto più venduto, o magari il secondo o il terzo, ci sarebbe da stupirsi meno di quello che appare.