---
title: "Futuro anteriore, o: le quattro stagioni"
date: 2023-11-17T18:13:51+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Comandi rapidi, Shortcuts]
---
Estate. Qualche difficoltà con le tabelline. Tempo di spostamenti e gite, in macchina o sui mezzi pubblici, comunque con molto tempo a disposizione.

Il papà abbozza in tempi ridotti un Comando rapido che sceglie un numero a caso da uno a dieci; sceglie un altro numero a caso da uno a dieci; chiede a voce *quanto fa il primo numero moltiplicato per il secondo numero?*

Lo chiama internamente *la macchina delle tabelline* ed ecco creato uno strumento capace, anche da un watch se capita, di suscitare ripasso delle tabelline per un tempo indefinito.

Qualità del programma, infima; utilizzo, intenso; risultato, non abbiamo più difficoltà con le tabelline.

Autunno. Nessun problema, ma tanta complessità con le coniugazioni dei verbi. Tanti tempi, forme particolari, casi speciali, padroneggiare tutto impegna.

Il papà guarda alla *macchina delle tabelline* e immagina un meccanismo solo leggermente più complesso, che sceglie a caso tra il verbo essere e il verbo avere (o da una lista precompilata); sceglie a caso un tempo da una tabella precompilata; chiede a voce *coniugazione del verbo secondo il tempo*. La complessità è più che altro quantitativa. Il papà immagina *la macchina dei tempi*.

Inverno. Se il papà fosse più veloce, la *macchina dei tempi* potrebbe arricchirsi di varie opzioni. Per esempio, esercitarsi su tempi o modi specifici. Potrebbe proporre a sorpresa domande-trappola come il participio passato di *dirimere*. Potrebbe tenere nota dei risultati o avere livelli di difficoltà.

Primavera. Il papà potrebbe parlarne a scuola alle maestre. Non perché abbia fatto chissà che cosa (e poi a chi interessano i Comandi rapidi in una scuola dove i Mac sono l’eccezione?), ma perché creare piccoli ausili per l’apprendimento dei ragazzi, senza intrusioni esagerate di tecnologia, senza richiesta di risorse sempre mancanti, si può fare. La macchina delle tabelline è una scemata; scriverla in Python in quinta elementare sarebbe un buon esercizio di *coding*.

Le maestre diranno ah, carino, sì, ma il metodo, noi preferiamo fare in altro modo, abbiamo le Lim, programmare ci pensiamo l’anno prossimo.

E un anno sarà passato (futuro anteriore del verbo *passare*).