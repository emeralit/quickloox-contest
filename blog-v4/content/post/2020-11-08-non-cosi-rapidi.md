---
title: "Non così rapidi"
date: 2020-11-08
comments: true
tags: [Shortcut, Comandi, Rapidi]
---
Alle note già dedicate all’[arrivo di iOS 14](https://macintelligence.org/blog/2020/10/31/e-arrivata-la-quattordicesima/) sui miei sistemi, devo aggiungere che alcuni dei miei Comandi rapidi hanno parzialmente smesso di funzionare.

C’è qualche problema di retrocompatibilità, oppure ho usato metodi che funzionavano pur essendo scorretti e hanno ceduto alla prima variazione.

Ora provo a metterci dell’attenzione e anche a fare qualche prova con iPhone, dove uso i Comandi rapidi assai meno e magari ci sono differenze di codice rispetto a iPadOS.

Puntualizzo che si parla di iOS 14.2, che iPad e iPhone mi hanno proposto, inaspettatamente, prestissimo e così sono già installati. Alla voce *dettagli inutili*, il pulsante Indietro di Facebook è tornato a posto.