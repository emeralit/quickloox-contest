---
title: "Trent’anni dopo"
date: 2015-10-18
comments: true
tags: [Ibm, Mac, iOS, Gruber]
---
È [cosa nota](https://macintelligence.org/posts/2015-05-29-persino-ibm/) che in Ibm stiano entrando quantità consistenti di Mac, nell’ordine delle decine di migliaia di unità.<!--more-->

Arrivano [altri dettagli](http://appleinsider.com/articles/15/10/15/only-5-of-mac-users-at-ibm-need-help-desk-support-compared-to-40-of-pc-users). Ibm sta installando 1.900 Mac a settimana e, se contiamo anche iOS, sono già 130 mila gli apparecchi Apple in funzione presso quella che una volta era Big Blue.

Il bello è che questi 130 mila apparecchi sono appoggiati da un team di supporto di… ventiquattro tecnici, uno ogni cinquemila macchine e oltre.

Ibm è sempre più convinta di Mac perché le spese di supporto, manutenzione, assistenza di un Mac in ufficio sono vastamente minori di quelle dei PC e per questo motivo l’ingresso di un Mac al posto di un PC equivale a un *risparmio* per Ibm.

Un documento molto più dettagliato sulla faccenda, ignorato da molte delle fonti che si leggono, è [condiviso su Google Docs](https://docs.google.com/document/d/1ufpf_yfXpRD7Qcid4ft2c-cGd_6XgYEZy_FPP78lXQQ/preview?pli=1).

Avevo in mente una chiusa molto più cattiva di questa, ma voglio essere clemente e allinearmi al [commento di John Gruber](http://daringfireball.net/linked/2015/10/15/ibm-macs).

>Avverto una grande perturbazione nella Forza. Come se milioni di voci gridassero improvvisamente “Sono trent’anni che ve lo diciamo!”