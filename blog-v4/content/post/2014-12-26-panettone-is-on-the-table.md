---
title: "Panettone is on the table"
date: 2014-10-26
comments: true
tags: [Natale, Badland, AppleTv, iTunes, Samsung, Lumia, featurephone, Amazon, Elio, Hdmi]
---
Se ho fatto bene i conti, Natale è trascorso in compagnia di sei iPhone, due iPad, un iPad mini, due Lumia, un Samsung e quattro *featurephone* (vecchia maniera).

Ho giocato a Badland in *multiplayer* locale e ricevuto un bellissimo regalo via Flipagram. Nella colonna sonora sono passati anche Elio e le Storie Tese.

Tanti regali arrivati via Amazon e un paio, ordinati per altri canali web, che invece si fanno ancora attendere. Amazon è impressionante nella sua organizzazione.

Babbo Natale informatico è stato munifico e ha lasciato nella caldaia una Apple TV più un cavo di raccordo Hdmi che mi tornerà utile lavorativamente, oltre a una scheda regalo iTunes. Apple TV cambierà probabilmente anche alcune abitudini casalinghe e costituirà un esperimento interessante in una casa dove il consumo televisivo è tradizionalmente distinto da quello digitale.

Tutti dormono ora. Il vento festeggia la sua parte fuori dalla finestra. È festa anche se il silenzio è parziale.

Sì, Natale è stato buono.