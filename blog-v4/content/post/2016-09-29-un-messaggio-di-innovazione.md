---
title: "Un messaggio di innovazione"
date: 2016-09-29
comments: true
tags: [iOS, Messaggi]
---
Come d’abitudine, per installare [iOS 10](http://www.apple.com/it/ios/ios-10/) ho aspettato il sollecito da parte di iPhone. Sarà anche l’unica installazione a breve termine, perché iPad di terza generazione è rimasto fuori dall’aggiornamento e la sua sostituzione, non essendo necessaria, avverrà più avanti.

Non ho una recensione in mano, solo i primi minuti di utilizzo. Che sono consistiti in una *full immersion* in Messaggi. (Lo sblocco dello schermo via tasto Home mi ha richiesto un paio di occorrenze per abituarmi; sono abituato).

Sono sorpreso perché un conto è vedere le dimostrazioni, le schermate, gli annunci, un conto è trovarsi l’oggetto sotto le mani.

Se qualcuno pensava che fosse finito il tempo di innovare e che gli aggiornamenti di sistema si riducessero a cosmetica e qualche sistemazione di difetti, ecco che una applicazione viene ribaltata come un calzino e trasformata in una piattaforma di app dentro la piattaforma di app che già è iPhone.

Sembrano *gadget*, i messaggi finto scritti a mano, le miniapp, il collegamento diretto con la musica presente sull’apparecchio? Non lo sono. Chi comunica come lavoro sa benissimo quanto contano i dettagli e quanto l’atmosfera di un messaggio valga anche più del contenuto vero e proprio. Qui ci sono nuovi linguaggi testualvisivi (o visualtestuali) che è possibile sviluppare per trasmettere una informazione in modo più completo, convincente, avvolgente, anche più ridicolo e spiritoso se ne vale la pena.

Questo lo chiamo un lavoro fatto bene e sono impressionato. Positivamente.

Una sola annotazione generale: su iPhone 5, per ora, la velocità del nuovo sistema è del tutto pari a quella vecchia. Niente rallentamenti o saltelli dell’interfaccia. Però per vedere tutto mi serve ancora del tempo.