---
title: "Se telefonando"
date: 2015-07-30
comments: true
tags: [Zimperium, Android, BlackHat, DefCon, Android, Google, Pdf, phishing, Stagefright]
---
Devo premettere che Zimperium [scrive di Stagefright](http://blog.zimperium.com/experts-found-a-unicorn-in-the-heart-of-android/) in regime di conflitto di interessi, perché vende sicurezza e propaganda falle. Altra premessa, vedremo le prove di quanto affermato non prima del 5 agosto, durante il raduno [Black Hat Usa](https://www.blackhat.com/us-15/schedule/briefings-5.html) (per chi fosse scomodo lì, lo spettacolo si replica a [Def Con](https://www.defcon.org/html/defcon-23/dc-23-schedule.html) il 7 agosto).<!--more-->

Si parla comunque di *novecentocinquanta milioni* di apparecchi Android esposti a un problema di sicurezza che permette a un pirata di operare in libertà all’interno di un apparecchio *conoscendone unicamente il numero di telefono*.

>L’attacco ha solo bisogno del numero di telefono, attraverso il quale possono eseguire codice a distanza mediante un file su misura inviato via Mms. Un attacco con tutti i crismi potrebbe persino cancellare il messaggio prima che lo si possa leggere, così che l’utente vedrebbe solo la notifica di consegna. […] A differenza del phishing in cui la vittima deve aprire un Pdf o un file inviato dal criminale, questa vulnerabilità può essere sfruttata mentre la vittima dorme. Prima che si svegli, l’aggressore avrà eliminato ogni traccia di compromissione dall’apparecchio, che verrà usato normalmente, ma sarà stato infettato.

Infettato significa, nel 2015, che l’aggressore farà di tutto per causare zero danni all’apparecchio e al suo proprietario, e installare un software assolutamente discreto e silenzioso, controllabile a distanza per effettuare azioni come collaborare a spedizioni di massa di posta indesiderata, attacchi di massa contro server innocenti e altre piacevolezze.

Google è stata avvisata del problema – che affligge potenzialmente quasi un miliardo di apparecchi – e ha già preso contromisure. Disgraziatamente, nel mondo di Android la parola *tempestivo* non ha grande valore:

>Gli aggiornamenti del firmware per gli apparecchi Android impiegano tradizionalmente molto tempo prima di raggiungere gli utenti. Gli apparecchi più vecchi di diciotto mesi potrebbero non vedere mai un aggiornamento del firmware.

Se tutto questo verrà confermato, ci capiterà nei prossimi mesi di chiacchierare amabilmente con qualche possessore di apparecchio Android. Ci dirà che non è nuovissimo ma funziona benissimo, che per quello che è costato va benissimo e che alla fine fa tutto quello che fanno altri apparecchi più costosi.

Anzi, il suo apparecchio fa persino qualcosa che gli altri più costosi non fanno: partecipa suo malgrado a una rete gestita da pirati informatici, grazie a un messaggio telefonico che non sa neanche di avere ricevuto.

Gli faremo i complimenti per la sua scelta oculata, in omaggio a quel vecchio proverbio cinese dove il marito dovrebbe picchiare la moglie anche senza motivo: lui non sa il perché, ma lei sì.