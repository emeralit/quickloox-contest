---
title: "Venti anni e non sentirli"
date: 2019-05-22
comments: true
tags: [Edge, ArsTechnica, Microsoft, Mac, Windows]
---
L’ultima volta che Microsoft ha scritto un browser per Mac dovevamo entrare ancora in questo secolo e per Apple era quasi questione di vita o di morte.

Venti e più anni sono passati invano, perché [Microsoft ci riprova](https://arstechnica.com/gadgets/2019/05/microsoft-publishes-first-edge-for-macos-preview-promises-to-make-it-truly-mac-like/), con un ircocervo basato sul motore di Chrome che per ragioni sconosciute uno dovrebbe installare invece di installare Chrome e la promessa di essere *Mac-like*.

Se non altro, Apple è in buona salute e Microsoft promette bene solo sul lunghissimo termine, quando finalmente ci saremo liberati di Windows e sarà una degnissima azienda che vende degnissimi servizi di cloud senza l’idea del dominio totale globale.

Chiamatemi se, e quando, Edge supporta AppleScript.
