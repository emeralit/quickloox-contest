---
title: "La macchina delle illusioni"
date: 2020-05-25
comments: true
tags: [Gassée, Monday, Note, Apple, Car, Toyota, Tesla]
---
Da quando [è stato avviato il progetto Titan](https://www.macrumors.com/roundup/apple-car/) ho fatto due figlie e portato la prima alle soglie della scuola primaria.

A interrompere il trend arriva [l'articolo di Jean-Louis Gassée](https://mondaynote.com/apple-car-speculation-the-missing-pieces-bb26b0f94acf) che fa giustizia di tante stupidaggini date in pasto ai lettori e spiega che la Apple Car, salvo meraviglie segrete che ancora nessuno ha saputo anticipare, è lontana e soprattutto molto improbabile. Finalmente (anche se ulteriori ipotetiche paternità sarebbero benissimo accolte).

Che una società abituata a profitti del trenta percento abbia interesse a competere in un mercato dove bisogna essere grandi maestri per fare il dieci (ci riesce solo Toyota, con un fatturato superiore a quello di Apple), e dove una Tesla ha impiegato anni per uscire dai profitti negativi, è improbabile. E se nel cilindro di Tim Cook c'è _one more thing_ a riguardo, deve essere veramente clamorosa. Nessuno l'ha mai scoperta. Se include anche un impianto di produzione di auto mai visto prima sul pianeta, questo è avvolto nella più totale segretezza, invisibile a un satellite, protetto da una catena del silenzio perfetta. Un conto è nascondere una funzione di macOS, un altro un impianto industriale.

Ma tranquilli: se cronicamente incapaci di amare alla follia i propri acquisti, al punto di volerli conoscere intimamente e fare coppia per lavorare e divertirsi insieme meglio di prima, e costretti a sospirare di sogni impossibili o improbabili per trovare un senso alla vita, nessun problema: la macchina delle illusioni ha già aperto un nuovo fronte con [Apple Glass](https://www.dezeen.com/2020/05/21/apple-glass-leak-augmented-reality-product-technology/). Escono nel 2021, nel 2022, che importanza ha? L'importante è avere un clic per distrarsi.

Sento i congiunti, metti che sia in arrivo qualche pargolo.