---
title: "La libertà richiede polso"
date: 2015-12-02
comments: true
tags: [Londra, watch, iPhone, Easyjet, Wallet]
---
In aeroporto ho passato tutti i controlli e le registrazioni tranne l’ultimo tramite esibizione della carta di imbarco su [watch](http://www.apple.com/it/watch/). La [*app* di Easyjet](https://itunes.apple.com/it/app/easyjet-mobile/id483568103?mt=8) ha trasmesso graziosamente il documento a Wallet, che lo ha replicato sul quadrante da quarantadue millimetri ma più che sufficiente. (In seguito mi sono accorto che Easyjet ha la versione watch della sua *app*, la quale però non replica la carta di imbarco sull’orologio).<!--more-->

Al *gate* di imbarco, il lettore non era esposto come ai controlli di sicurezza e raggiungerlo con il polso era impossibile, cosí ho dovuto ripiegare su iPhone. Questo è probabilmente il motivo per cui Easyjet non replica la carta: comunque non sarebbe leggibile.

Fa tutto impressione perché la carta di imbarco su iPhone era un miraggio non molto tempo fa e ricordo il triste rito della stampa del foglio a casa prima di uscire, guai a dimenticarsene. Ora il check-in si fa in treno dalla *app*, ché perfino il divano di casa oramai è destinato a usi più motivanti.

Non è questo il punto; chi viaggia abitualmente oramai dà tutto questo per scontato e io sono un aggregato dell’ultima ora. Il finanziere del controllo documenti ha scorso la carta di imbarco sul minischermo con assoluta naturalezza e indifferenza.

Il punto è il sentimento. Forse per qualcuno accreditarsi con l’orologio è segno di distinzione, o di autogratificazione, o di pompaggio dell’autostima.

Lasciare tutto in tasca, ruotare il polso ed essere libero, di procedere, di spostarsi, di vivere, a me sa di libertà. Lontano dai timbri, dagli arroganti in divisa (mica tutti; gli arroganti), dalle fotocopie, dalle code umilianti e ingiuste.

Questo è un senso di watch: terminale di un ecosistema che finalmente lavora per te invece di condannarti alla burocrazia e all’inefficienza per colpa di esistere e pretendere pure di fruire di un servizio.