---
title: "Note sul XXI secolo"
date: 2018-02-22
comments: true
tags: [iPad, Pro]
---
Al banco di regia dell’evento aziendale meno Mac del solito, per quanto sempre maggiori di zero. Le foto sono venute peggio del solito e per una volta conto sulla fiducia.<!--more-->

Mi ha colpito di più un iPad Pro utilizzato come blocco note assieme ad Apple Pencil. Naturalezza totale; un osservatore distratto forse neanche avrebbe pensato a uno schermo.

La vera digitalizzazione delle note a penna, dopo quarant’anni di compromessi, è arrivata. E sì, digitalizzare compiutamente le attività elementari è più difficile.

 ![Note su iPad Pro con Apple Pencil](/images/notes-ipad-pro.jpg  "Note su iPad Pro con Apple Pencil") 