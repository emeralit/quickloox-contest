---
title: "Lo standard deindustriale"
date: 2014-12-20
comments: true
tags: [Windows, Mac, iPhone, iPad, Sony, hacker]
---
Fosse sfuggito a qualcuno, un gruppo di pirati [ha preso di mira Sony](http://www.cnet.com/news/sony-hack-said-to-leak-47000-social-security-numbers-celebrity-data/) e le ha fatto tanto, tanto male. Si sono presi terabyte di dati, hanno il traffico postale interno, qualche film, documenti finanziari, coordinate dei dipendenti, numeri di conti bancari, di assistenza sociale, di tutto. Sony non sa che pesci prendere ed è tenuta per il collo da questi buontemponi forse nordcoreani.<!--more-->

*TechCrunch* titola [I dipendenti di Sony Pictures ora lavorano in un ufficio di dieci anni fa](http://techcrunch.com/2014/12/17/sony-pictures-employees-now-working-in-an-office-from-ten-years-ago/). Hanno terrore di usare strumenti tecnologici che potrebbero essere compromessi. Qualcuno è ricorso al fax; invece che inviare messaggi vocali si parla faccia a faccia con l’interlocutore; si tirano fuori le vecchie stampanti quando serve passare un documento.

Con poche eccezioni.

>“Per lo più l’ufficio funziona. Stiamo lavorando giorno per giorno. Ci hanno appena ridato la messaggeria vocale. Dopo lo shock iniziale, sono tutti un poco più calmi. A un paio di persone hanno requisito il computer, ma chi usava Mac era tranquillo”, riferisce una dipendente intervistata. Spiega che la maggior parte del lavoro viene svolta su iPad e iPhone.

Caro Babbo Natale, fai leggere l’articolo di TechCrunch a tutti quelli che usano Windows *per risparmiare* e chiedigli di stimare l’entità del danno per Sony.

Caro Babbo Natale, fai leggere l’articolo di TechCrunch a tutti quelli che definiscono Windows *standard industriale* e spiega loro che, mai fosse disgraziatamente standard, è deindustriale. Un attimo e regredisci di dieci anni. Prima di renderti conto che in fondo, dieci anni indietro lo eri già.