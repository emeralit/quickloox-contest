---
title: "Orizzonti perduti"
date: 2024-01-27T02:15:56+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet, Games]
tags: [boardgame, Ai, intelligenza artificiale]
---
Ogni volta che torno da una piacevole serata di boardgame (questa sera avevamo Dungeons & Dragons in pausa), mi metto a fantasticare su come sarebbe lo stesso gioco reso su un’interfaccia testuale, che consentisse in pratica di giocare circa come su un *adventure game*.

Faccio poca fatica a immaginare le strutture dati, perché più un boardgame è riuscito più è semplice nel design e le sue regole sono chiare e leggere.

Faccio più fatica a immaginare come avverrebbe l’interazione, perché si partirebbe inevitabilmente da una descrizione della situazione da parte del programma, cui corrisponde un input da parte del giocatore, interpretato dal gioco che aggiorna la struttura dati interna e passa al giocatore seguente.

Probabilmente occorrerebbe un lungo lavoro di rifinitura per capire come rendere ottimale la descrizione per il giocatore tanto la descrizione quanto le regole di input, che dovrebbero essere flessibili nel consentire libertà espressiva quanto complete, a livello di interpretare correttamente le decisioni del giocatore, dirimere le ambiguità, correggere le imprecisioni eccetera. Il tutto senza frustrare chi gioca, senza perdersi in mille passaggi schematici eccetera. Complicato.

Poi c’è la parte di assistenza e supporto al giocatore, che potrebbe andare da semplici reminder sull’andamento del gioco, a richiesta o meno (ancora due turni prima della mossa speciale; il giocatore Y potrebbe vincere al prossimo turno; sono uscite Z carte mostro e così via) a consigli strategici o valutazioni posizionali. Questa parte va molto oltre le mie capacità immaginative.

Quando sono apparsi i sistemi generativi da ChatGPT in poi, ho pensato: stai a vedere che un aggeggio del genere, a dargli in pasto tutte le imformazioni su un gioco, riesce a sistemare una gran parte di queste problematiche, soprattutto quelle di maggiore complessità.

Ahimé. Sono sufficienti pochi esperimenti per capire che i chatbot sanno fare tante cose, ma non di questo tipo. Al massimo potrebbero forse servire in appoggio al gioco, per scovare una regola in un manuale lungo o descrivere le caratteristiche di un elemento del gioco quando servissero al giocatore per compiere una scelta. E ci fermiamo proprio qui.

A convincere qualche supermanager di Google, potrebbe fare scatenare sulla rappresentazione teorica del gioco la potenza di [Deepmind](https://deepmind.google). Però ne verrebbe fuori un bot-mostro dalla strategia definitiva, capace di dominare qualsiasi giocatore umano, privo delle capacità di interlocutore e assistente di cui sopra. Non è quello che mi interessa.

Possiederebbe qualche grado di intelligenza, un sistema come quello che immagino? O solo una notevole complessità interna? Di fatto non conosco un assistente di questo genere e se non è stato scritto, o è lontanissimo da quello che intendo, significherà qualcosa.

Come minimo, che i sistemi attuali alla fine non sono pronti né per l’intelligenza né per una complessità elevata. E per vedere qualcosa all’orizzonte occorrerà riuscire a guardare molto più lontano di adesso.