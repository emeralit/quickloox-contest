---
title: "Curve e controcurve"
date: 2014-09-30
comments: true
tags: [Bézièr, Locida]
---
Non ho voglia di discutere. Piuttosto, di qualcosa di bello, nel senso dell’estetica.

Allora, il [gioco delle curve di Bézièr](http://bezier.method.ac) per perdersi nell’armonia delle curve e poi una perfetta spiegazione delle [differenze esistenti tra i *font* della famiglia Lucida](http://lucidafonts.com/pages/faq). Cogliere i minuti dettagli che rendono la tipografia un settore di eccellenza è una sfida che concilia il sonno, nel senso più positivo del termine.