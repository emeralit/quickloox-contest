---
title: "Alla fine l’ufficio"
date: 2021-08-23T02:04:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple] 
---
Apple [ha rinviato a gennaio il ritorno in ufficio dei dipendenti](https://www.bloomberg.com/news/articles/2021-08-20/apple-delays-office-return-to-at-least-january-after-covid-surge). I quali, una volta tornati, potranno comunque lavorare da altrove il mercoledì e il venerdì di ogni settimana.

Apple è anche l’azienda probabilmente più legata alla modalità di lavoro in ufficio. Eppure, tra varianti e convenienze, a gennaio saranno quasi due anni che i suoi dipendenti hanno facoltà di lavorare da remoto. E i conti non sono andati così male.

Gli uffici saranno sempre con noi, ma l’epoca dell’ufficio come luogo canonico per lavorare è tramontata per sempre. Se qualcuno insiste per averti in ufficio cinque giorni a settimana, ha un problema. Anche più di uno.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*