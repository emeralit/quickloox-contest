---
title: "A cuore sospettoso"
date: 2017-12-23
comments: true
tags: [watch, MelaBit, Antennagate]
---
La cronaca registra (via *9to5Mac*) un ennesimo esempio di persona [salvata da problemi cardiaci gravi](https://9to5mac.com/2017/12/15/apple-watch-saves-life-managing-heart-attack/) tramite un preallarme dato da watch.<!--more-->

Ma c’è da fidarsi di un aggeggio che alla fine dei conti è realizzato con lo scopo di essere venduto per ampi margini di guadagno?

Poi c’è il lato scientifico: le [modalità di pubblicazione di certe ricerche](https://melabit.wordpress.com/2017/07/05/la-cattiva-scienza-dei-sensori-da-polso/) lasciano grande spazio alla perplessità (articolo di *MelaBit* in italiano di cui consiglio fortemente la lettura). Dunque, quanto possiamo fidarci di watch?

La domanda è quella sbagliata. Sarebbe meglio chiedersi che cosa sta succedendo e perché.

Siamo di fronte a uno sviluppo della medicina diagnostica senza precedenti, nel quale gli strumenti arrivano nelle mani delle persone comuni in autonomia rispetto ai loro medici. Questo processo è appena cominciato.

watch (a sineddoche di tutti gli aggeggi da polso che hanno capacità simili) non è lo strumento *definitivo* per queste applicazioni. È il *primo*. Lo stesso articolo su *9to5Mac* fa riferimento ad accessori per watch approvati dalle autorità sanitarie, che forniscono le garanzie di affidabilità assenti per l’orologio (per quanto le sue letture risultino accurate).

Girano voci di modelli di watch capaci di produrre un elettrocardiogramma e chissà se sono vere. Anche se non lo fossero, la direzione è questa e arriveranno anche le approvazioni ufficiali.

Altro dubbio: non è che Apple baderà ai profitti invece che alla bontà degli apparecchi? E se milioni di persone finissero per prendere come buone misure che in effetti non lo sono? Se per contenere i costi si lesinasse sulla qualità delle funzioni legate alla salute?

Qui c’è una differenza fondamentale tra Apple e gli altri: la soddisfazione del cliente. Per Apple è una misura vitale in cui investire un’enormità di risorse per restare in cima alle relative classifiche. La ragione è semplice: se la gente è talmente insoddisfatta di iPhone, o Mac, o tv, da abbandonarlo e scegliere la concorrenza, Apple è fuori dal gioco.

Si è già visto quanto danno abbia causato una vicenda sommariamente insulsa come l’Antennagate. Apple non cercherà di vendere fumo sui sensori di watch, perché non se lo può permettere.

Il primo cardiopatico che finisse nei guai per via di un watch non all’altezza del lavoro scatenerebbe infatti un effetto valanga potentissimo più di qualsiasi *keynote*.

Ecco perché è bene essere scettici sulle capacità dei nuovi sensori da polso, ma ottimisti sulla loro finalità e sul perché vengono sviluppati. Certo, se poi uno va a comprarli da un’azienda produttrice di telefoni che esplodono, è tutt’altra questione.
