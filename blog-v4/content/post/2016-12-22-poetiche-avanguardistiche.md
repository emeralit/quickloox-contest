---
title: "Poetiche avanguardistiche"
date: 2016-12-22
comments: true
tags: [Venerandi, Quinta, Copertina, Poesie, Elettroniche]
---
Fuori dai classici, i libri di poesia che ho reperito si contano sulle dita di una mano.

Oggi ne ho reperito uno in più: [Poesie elettroniche](http://www.quintadicopertina.com/index.php?page=shop.product_details&flypage=flypage_images.tpl&product_id=411&category_id=6&keyword=poesie&option=com_virtuemart&Itemid=56&vmcchk=1&Itemid=56) di Fabrizio Venerandi.<!--more-->

Fabrizio ha fatto un lavoro che oso definire unico al mondo fino a che qualcuno mi mostrerà che ho torto. Perché ha preso lo standard Epub3 per la produzione dei libri elettronici e, invece di considerarlo una gabbia dentro cui contorcersi, ne ha fatto una rampa di lancio per spingere al massimo l’abbinamento tra letteratura e software. Non per il gusto di spingere la tecnologia in modo fine a se stesso, ma per trovare nuove soluzioni espressive e costruire nuovi modi di fare poesia.

Nella pagina di presentazione dell’opera si auspica che i lettori smontino il meccanismo dell’ebook in modo da vedere come è fatto dentro e magari scrivano le proprie poesie utilizzando lo stesso motore.

Sono un pessimo poeta e non mi azzarderò sulla seconda parte. Quanto alla prima, vale dieci corsi di introduzione alla programmazione. Considerato che il libro è addirittura in promozione a 3,49 euro (tre euro e quarantanove centesimi), va considerato non spesa ma investimento.

Per qualcuno in poesia, per altri in software. Anche entrambi, volendo.