---
title: "Con i se e qualche ma"
date: 2015-08-15
comments: true
tags: [Ifttt, IoT]
---
Sono entrato da neofita nel magico mondo di [Ifttt](https://ifttt.com), *If This Then That*. È un sistema geniale di *ricette* condizionali: se (*if*) accade una certa cosa, allora (*then*) deve accaderne un’altra (*that*).<!--more-->

Posto che in rete esistano le informazioni necessarie, fornite da servizi e app disparate, il meccanismo è senza limiti. Si può pensare a banalità tipo *se segnano un gol in serie A mandami una notifica* oppure *se pubblico una foto su Instagram, fai partire un tweet di annuncio dal mio account*.

Le cose veramente intriganti arrivano con la [Internet of Things](https://it.wikipedia.org/wiki/Internet_delle_cose), il complesso degli oggetti intelligenti e collegati alla rete. Una ricetta del tutto realizzabile teoricamente: *se abbiamo passato Aosta, accendi il riscaldamento della casetta a Courmayeur*. Per non dire del classico *se non c’è più latte nel frigorifero, aggiorna la lista della spesa*. Il sistema è a costo zero, vengono realizzate ricette a getto continuo, promette tantissimo. La *mailing list* di Iftt invia una ricetta esemplificativa al giorno e iscriversi è il modo migliore di farsi un’idea.

Sul piano della *user experience*, tuttavia, si può molto migliorare.

Mi sono lasciato tentare da una ricetta che invia una mail al giorno con l’elenco delle top app su App Store in promozione gratuita (le informazioni arrivano da [AppZapp](http://www.appzapp.net)). Trattandosi di iOS, ho scaricato la *app* su iPad.

Sono andato di fretta e posso avere commesso errori, però mi sembra che la *app* non memorizzi la *password*. Al primo giro di tentativi mi è stato chiesto di scaricare una *app* chiamata Boxcar, che sembrava indispensabile e invece non lo è.

Bisogna stare maledettamente attenti al *wording*, a come viene descritta la ricetta, perché hanno l’icona sempre uguale ma una ti manda la email, quell’altra la notifica, un’altra riguarda Android eccetera eccetera. Rileggi tre volte tutto per essere sicuro e alla fine, quando sei sicuro di avere selezionato la ricetta giusta, ne leggi la descrizione estesa, che è inesatta e corrisponde a quella di una ricetta diversa.

Muoversi nella *app* richiede una certa pazienza. C’è una lente corrispondente in clamorosa evidenza a un pulsante di ricerca e un rettangolo vicino che sembra un campo testo. Invece è un altro pulsante e il testo da cercare si può inserire se si tocca la lente.

Il finto campo testo alterna la modalità Browse (navigazione tra le ricette disponibili) a quella Manage (organizza quelle adottate). La prima vuole richiamare App Store e francamente è molto ma molto più inutile della pagina iniziale di App Store. La seconda contiene una serie di ambiguità e sottigliezze anche non proprio sottili, tra la quali uno storico delle operazioni organizzato a cronologia inversa come un blog (ma quando mai?), dove per come sono mostrate le informazioni è facilissimo fraintendere la situazione.

Forse sono io un pessimo utente e potevo andare meno di fretta e anche stare più attento ai dettagli. La realtà è che il sistema è semplicissimo: se-questo-allora-quello. Mi aspetto una *app* semplice persino per me.