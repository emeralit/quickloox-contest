---
title: "La tensione ai dettagli"
date: 2019-04-08
comments: true
tags: [Mac, Mojave, macOS, Gruber, Daring, Fireball, iBook]
---
Anni fa, tanti anni fa, spiegavo che parallelamente a macOS gratuito avrei volentieri pagato per una versione Gold del sistema operativo Mac, neanche tanto evoluta per funzioni quanto robusta e curata per assenza di bug e ineccepibilità del design.

Mi viene in mente mentre leggo John Gruber [prendersela](https://daringfireball.net/2019/04/bad_ui_mojave_release_notes), come è sacrosanto, con la finestra di dettaglio degli aggiornamenti di sistema di macOS. Siamo nel 2019, può contenere quantità di testo molto consistenti, ma mostra nove righe per volta e non è ridimensionabile, né permette di fare clic sugli indirizzi inseriti nel testo.

Nessun bug, ma design veramente dimenticato da anni, indegno dell’attenzione ai dettagli che siamo abituati a chiedere ad Apple  in cambio dei nostri favori. Anzi, tensione ai dettagli, perché l’attenzione può esserci e non esserci, mentre la tensione ti prende ogni mattina e, anche se durante il tuo lavoro di sviluppo hai trovato e risolto diciotto errori, sei all’erta ugualmente come se il diciannovesimo fosse il primo della giornata.

*Un iPhone SE mostra più testo*, scrive Gruber, e a ragione, se si pensa che qualcuno vedrà la finestrella di aggiornamento di Mojave su uno schermo 4K, posto che sulla scrivania c ne sia uno soltanto.

Quando parlavo dell’edizione Gold mi riferivo come esempio all’antichissimo bug che costringeva a scambiare due floppy disk innumerevoli volte prima che il sistema riprendesse a lavorare, anche su Mac la cui RAM avrebbe consentito di risolvere qualsiasi problema di sistemazione dati da due floppy in una passata sola. Bug che rimase, anche perché nel frattempo si smise di usare i floppy. Ci si creda o meno, c’è gente che tuttora [risolve bug presenti nella ROM dei primi Mac](https://www.bigmessowires.com/2013/10/17/fixing-30-year-old-apple-rom-bugs/).

Ecco: questo è un altro problema simile, una schermata che sarebbe stata bene su un iBook di inizio secolo, 640 x 480, e ci becchiamo anche su lenzuoli da tremila per duemila. Apple, please, tensione ai dettagli, grazie.
