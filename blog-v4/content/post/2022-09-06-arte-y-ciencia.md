---
title: "Arte y ciencia"
date: 2022-09-06T02:15:49+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [iPod Shuffle, eMac]
---
Da consegnare alla piccola storia di questa estate la presenza di due autentici pezzi da museo di Apple nella dotazione del [Museo della Scienza Príncipe Felipe](https://www.cac.es/en/home/la-ciutat/cacsa-la-empresa/cronologia/museu-de-les-ciencies.html)

![iPod nano](/images/ipod-nano.jpeg "Per qualche anno, il sinonimo di musica digitale.")

![eMac](/images/emac.jpeg "Non ha avuto molto seguito, ma rimane inconfondibile.")

Ammetto che su uno dei due ho dovuto pensarci anche un attimo, prima che venisse alla mente il suo nome.