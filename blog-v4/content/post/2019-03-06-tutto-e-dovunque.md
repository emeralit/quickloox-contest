---
title: "Tutto e dovunque"
date: 2019-03-06
comments: true
tags: [PureOS, Througthon-Smith, Marzipan, Marzipanify]
---
Merita attenzione il lavoro di PureOS, impegnata nel sogno utopico eppure necessario di offrire una piattaforma interamente libera, *open source*, dal primo hardware all’ultimo software. Attenzione doppia nel momento in cui annuncia un *framework* che rende *responsive* le app come fossero siti web; in funzione dello schermo in cui si trovano i menu si allargano, restringono, diventano pulsanti, spariscono per essere suscitati da un gesto eccetera. Il codice è uno solo e la app offre sempre la migliore interfaccia possibile.

Risultato lodevole per una iniziativa che non ha miliardi da spendere e si regge sull’entusiasmo. Diverso è pensare che siano più avanti degli altri. Steve Throughthon-Smith, che è uno sviluppatore solitario e miliardi da spendere ugualmente non ne ha, ha appena presentato lo strumento *Marzipanify*, che sfrutta il lavoro di Apple (progetto Marzipan) dedicato a fare funzionare su Mac le app di iOS per applicarlo velocemente alle app già esistenti.

Questo lavoro è inevitabilmente provvisorio perché Apple cambierà tutto quello che ritiene sia da cambiare mentre ci si avvicina al raduno mondiale degli sviluppatori, Wwdc. Eppure la fattibilità è dimostrata e si tratterà solo di smussare gli angoli dì Marzipanify per arrivare agli utenti comuni.

Nota interessante: l’evoluzione va evidentemente nella direzione di avere app che girano dovunque e non un sistema operativo comune tra desktop e mobile. Cioè vanno in fumo anni di sciocchezze propalate da analisti e giornalisti davvero poco preparati sull’idea dell’inevitabile fusione tra iOS e macOS.

Dove funzionano computer da tasca oggi si trovano iOS oppure Android, che hanno come contraltare sul desktop macOS oppure ChromeOS. C’era anche Microsoft, unica a sostenere l’idea del sistema operativo unico. Oggi il sistema operativo Microsoft, a livello di computer da tasca, è irrilevante.

Giusto un promemoria per i ragazzi di PureOS che sono gente simpatica. OK le app, ma è meglio un sistema operativo dedicato all’apparecchio su cui gira che un ibrido con la pretesa di fare funzionare tutto e dovunque come se i compromessi n0n facessero più parte del lavoro di sviluppo software.






