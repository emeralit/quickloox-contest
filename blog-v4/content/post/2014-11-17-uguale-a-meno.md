---
title: "Uguale a meno"
date: 2014-11-17
comments: true
tags: [Gruber, iPhone6, Android]
---
John Gruber [posta su Twitter](https://twitter.com/gruber/status/533693712719962112) la scoperta di gente che vende iPhone 6 contraffatti a 250 dollari.

Con su Android truccato per somigliare a iOS.

Con specifiche di lettura ebook nei formati di Office (sic).

Con schermo di risoluzione inferiore a quella del primo iPhone Retina.

Qualcuno ci casca sicuro. Costa così poco, dev’essere per forza un’occasione.

<blockquote class="twitter-tweet" lang="en"><p>“The quality, finish &amp; performance of this phone is the same as the Apple iPhone 6.” <a href="http://t.co/KVhtrvDjTN">http://t.co/KVhtrvDjTN</a></p>&mdash; John Gruber (@gruber) <a href="https://twitter.com/gruber/status/533693712719962112">November 15, 2014</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>