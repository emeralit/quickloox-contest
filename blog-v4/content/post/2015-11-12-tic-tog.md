---
title: "Tic Tog"
date: 2015-11-12
comments: true
tags: [Norman, Tog, Tognazzini, iOS, Macity, iPhone, interfaccia, Jobs]
---
Devo cercare di riassumere, per controbattere la [sbrodolata di testo](http://www.fastcodesign.com/3053406/how-apple-is-giving-design-a-bad-name) prodotta, uno non ci crede, da Don Norman e Bruce *Tog* Tognazzini su FastCompany. Due grandi nomi dell’usabilità, due mostri sacri dell’interfaccia umana. E però il tempo, si vede, non ha pietà per nessuno.<!--more-->

La sintesi della sintesi: Tog, che ha lasciato Apple poco dopo il ritorno di Steve Jobs, schianta i criteri di interfaccia di Apple pochi anni dopo che Jobs ha lasciato (definitivamente) Apple.

Già non suona bene.

Va peggio. L’articolo è, oltre che interminabile, caotico e poco scorrevole. Si accusa Apple di usare font difficili da leggere sugli schermi piccoli di un iPhone e ci sta tutta, a patto di non usare per l’articolo un font difficile da leggere, con didascalie minuscole e una impaginazione sciatta.

Le argomentazioni rasentano il delirio. L’idea di fondo, condenso moltissimo, è che si dovrebbero applicare a un iPhone con schermo *touch* le medesime regole di interfaccia che si applicano a un computer con il mouse. Non ci vuole un master in interfaccia umana per capire che è un’idea cretina.

Si possono trovare molti spunti di critica alle attuali interfacce umane di Apple; se però l’accusa è di pensare solamente a produrre apparecchi belli, siamo al mestierante da bar cresciuto a pane e Macity.

Il *post* sta uscendo caotico come l’articolo, ma sfido a riassumere quest’ultimo in modo lineare ed elegante senza passarci sopra una nottata almeno. A un certo punto vedo un paragrafo che mai mi aspetterei di trovare scritto da due firme così:

>Criticare una azienda di alta tecnologia con cicli di prodotto rapidi è una sfida. Effettivamente, nell’ultima versione del proprio sistema operativo mobile, iOS 9, sono stati affrontati diversi dei problemi che abbiamo descritto.

Mi rendo conto che una parte non piccola dell’articolo diventa superata da iOS 9. Mi chiedo, quando è stata scritta questa roba? La data sta *in fondo al pezzo* e bisogna superare un filmato fastidioso per arrivarci. La data è il 10 novembre e questo significa che hanno avuto a disposizione un mese per aggiornare l’articolo. Non lo hanno fatto, accontentandosi di un paragrafino a mo’ di toppa.

Ma visto che iOS 9 porta delle soluzioni bisogna pur controbattere, no? E che fanno? Scrivono *perché ci hanno messo così tanto?*. Che è un’altra osservazione da coatto, più che da *guru* dell’interfaccia.

L’incredibile arriva poco dopo. Si parte da un articolo titolato *iOS 9 ha 25 grandi funzioni segrete*. L’accusa è ovvia: perché rendere segrete funzioni utili? Peccato che su Mac si siano scritti centinaia, migliaia di articoli su funzioni nascoste e comandi non visibili al grande pubblico. Eppure, a sentire loro, quello è il modello che è stato tradito.

Ci sono anche punti di critica fondati, eh? Ma il cioccolato è mescolato inestricabilmente a un’altra sostanza marrone, per aumentare la consistenza. Purtroppo ne risente l’odore.

Che è di lontananza dal concreto, dal quotidiano, dal presente. Spiace perché sono due che hanno fatto la storia. E però ora sono a fare il tiro al bersaglio su un bersaglio grande grande, così da fare tanto rumore. Da Tog la critica come riflesso condizionato non me la sarei mai aspettata. Ma davvero, a volte il tempo sembra galantuomo e invece piega le ginocchia a chi crede o vuol far credere che tutto sia eternamente come è stato una volta, quando bastava poco per essere felici. Non si era felici, si era presenti a sé stessi e al mondo.