---
title: "Darwinismi"
date: 2017-06-28
comments: true
tags: [Darwin, Windows, Stefano]
---
Grazie a **Stefano** per la sua email, qui in **grassetto** all’inizio e alla fine.

**Ero in conference call con un amico in Italia** che gestisce una azienda di sviluppo sistemi Enterprise Resource Planning mediante un tool che gira solo su Windows.<!--more-->

Ieri gli è partito Windows Update su Windows 10 alle 8:30 del mattino e l’aggiornamento è durato sette (sette!) ore: nessuna possibilità di ulteriore lavoro quel giorno.

In più, finito l’aggiornamento, è apparso un messaggio che informava come l’account di Amministratore fosse corrotto, con necessità di riavviare il tutto in Modalità Provvisoria, perdendo tutte le sue configurazioni precedenti. Il tutto su un portatile HP iCore 7 del 2015. **A me sembra fantascienza**.

Citavo [un’esperienza simile in febbraio](https://macintelligence.org/posts/2017-02-13-o-paghi-o-ti-aggiorno/), con commenti controversi. C’è chi ritiene questa situazione giustificabile, in genere motivando che una persona capace di usare il computer non avrà problemi.

È un’idea geniale, da applicare a passaggi pedonali, ambulatori medici e ovviamente scuole: quelli bravi, i più adatti, prosperano comunque; gli altri, quelli immeritevoli, si arrangino.

Affascinante il darwinismo, solo che la civiltà è tale solo ed esattamente quanto tutela e sostiene proprio i più deboli. Sono da celebrare i grandi inventori delle tecnologie su cui si regge l’informatica; e insieme a loro quelli che l’hanno messa a disposizione del maggior numero possibile di persone. Va da sé che i geni di Windows, messi davanti a un’interfaccia *assembly* x86, non saprebbero neanche da che parte cominciare.

Non sto neanche a parlare dell’accessibilità, cartina di tornasole del concetto. Se un Mac o un iPhone aiutano un non vedente molto più di Windows, il pensiero che c’è sotto Windows è identico: non ce la fai? Amen, io sì.

Darwin finisce, si vede bene, dove inizia Windows. Il mio darwinismo è diverso e ne vado orgoglioso: i più adatti, quelli che sopravvivono, devono essere tutti. O almeno, il massimo numero possibile, in attesa di migliorare ancora.