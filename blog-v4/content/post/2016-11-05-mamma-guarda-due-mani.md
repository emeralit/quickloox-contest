---
title: "Mamma, guarda: due mani!"
date: 2016-11-05
comments: true
tags: [MacBook, Pro, Touch, Bar, macOS, Asymco, iOS, iPad, iPhone, Microsoft, Surface, Studio]
---
Horace Dediu di Asymco ha una [teoria suggestiva](http://www.asymco.com/2016/11/02/wherefore-art-thou-macintosh/) sull’evoluzione di Mac a seguito di Touch Bar.

Apple, sostiene, non è interessata a perseguire un Mac con schermo sensibile al tocco o inventarsi ibridi e convertibili come accade nel mondo PC, per la ragione che i PC sono apparecchi fini a se stessi e invece Mac fa parte di una famiglia di prodotti. Nella quale gli apparecchi interamente touch ci sono già, a partire da iPhone e iPad. E un iPad con tastiera può tranquillamente sostituire un Mac in una vasta serie di attività.

La parte più stimolante è questa: gli apparecchi iOS sono *a input diretto* e i Mac invece sono *a input indiretto*: l’azione delle mani è lontana dallo schermo.

Su iOS, specialmente se consideriamo iPad, è possibile agire con due mani contemporaneamente: su Mac no… fino a che arriva Touch Bar e diventa possibile applicare input indiretto allo schermo con due mani. Non si parla ovviamente di scrittura ma di interazione con gli elementi presenti sullo schermo.

E perché Mac sceglie questa strada diversa? Perché il *computing* da scrivania per le masse è già diventato *mobile* e Mac serve unicamente a un gruppo ristretto di persone per compiti specifici, invece di essere la macchina per tutti e per fare tutto. Invece che rincorrere iPhone e iPad, compito impossibile che snaturerebbe l’essenza di Mac, quest’ultimo si evolve in una macchina sempre più efficiente per l’input indiretto. E in questo senso, l’introduzione della seconda mano è una grande novità.

Si colloca allora meglio il Surface Studio di Microsoft, con il tocco, la penna, la rotella; è un apparecchio per fare input diretto con due mani. Non è quindi un concorrente di iMac come scrive il gregge, ma un tentativo di portare la logica di iPad e iPad Pro su schermi ancora più grandi. E andrà valutato per quanto rimpiazza l’acquisto di un iPad Pro o di una tavoletta Surface.

Bonus per la lettura: una serie impressionante di statistiche su Mac nella storia e nella storia di Apple, a partire dal fatto che nessun altro produttore ha una linea di prodotto attiva da trentadue anni.