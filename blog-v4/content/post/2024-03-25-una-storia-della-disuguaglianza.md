---
title: "Una storia della disuguaglianza"
date: 2024-03-25T00:11:56+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [John Gruber, Gruber, Daring Fireball, Vonnegut, Kurt Vonnegut, Harrison Bergeron]
---
Apple è sotto indagine dell’Antitrust americana in quanto, semplifico, avrebbe costruito un monopolio nei computer da tasca grazie alla superiorità di iPhone sulla concorrenza. Esatto: c’è più complessità nella storia, ma si può riassumerla con l’idea che Apple possa essere processata per avere creato un prodotto migliore degli altri.

Succede che un lettore [segnali a John Gruber su Daring Fireball](https://daringfireball.net/linked/2024/03/23/harrison-bergeron) un racconto breve dello scrittore di fantascienza Kurt Vonnegut, ambientato in un futuro dove tutto viene reso uguale al resto, amaramente in carattere con l’indagine su Apple.

Gruber si dice fanatico di Vonnegut ma non ricorda di avere mai letto il racconto. Allora [ne impagina una copia e la rende disponibile dal suo sito](https://daringfireball.net/misc/2024/03/Harrison_Bergeron.pdf), curata tipograficamente; una storia bene presentata si legge meglio. È la cosa più giusta e naturale.

Sempre Gruber scrive *godetevela su un iPad o, meglio ancora, stampatela*. Ritrasmetto l’invito.

Condivisione di letteratura tipograficamente gradevole. Improvvisamente Internet torna ad avere un perché.

(Per confronto, il racconto è disponibile anche [in italiano e a tipografia sciatta](https://www.poliscritture.it/2017/11/14/harrison-bergeron/). Provare).