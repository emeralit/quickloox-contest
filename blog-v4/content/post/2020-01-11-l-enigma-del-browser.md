---
title: "L'enigma del browser"
date: 2020-01-11
comments: true
tags: [Roll20, Safari, Chrome, iPad, D&D]
---
Dopo due anni abbondanti mi sono ritrovato master per una serata di Dungeons & Dragons e una differenza fondamentale è che, fino a due anni fa, usavo [Roll20](https://roll20.net/) su Mac. Ora il mio portatile è un iPad Pro e di conseguenza mi sono preparato con l’obiettivo di riuscire a usare Roll20 nel modo più completo possibile.

Una cosa positiva è che in questo intervallo Roll20 è migliorato in modo perfettibile. Una meno positiva è che sono state introdotte incompatibilità importanti con Safari.

Ho testato tutto quello che avevo il tempo di testare perché un sistema per assistere i giocatori di una sessione di D&D ha davanti clienti con esigenze pressanti, a partire dal fatto che gli chiedono di semplificare le parti complicate senza togliere il divertimento. Su iPad di una volta il supporto di Roll20 era dichiaratamente limitato, ma coerente con le limitazioni stesse. Oggi mi sono trovato con due software, Safari e Roll20, ambedue cresciuti moltissimo e però su strade divergenti.

Fortunatamente ho trovato la soluzione: Chrome su iPad è altamente compatibile con Roll20 e mi ha permesso di portare a casa la serata con successo.

La domanda è questa: sapevo che Apple aveva consentito la presenza di browser diversi da Safari su App Store, a patto che usassero tutti il motore di Safari. Tuttavia, in tutta evidenza, Chrome si comporta in modo diverso. Se usasse lo stesso motore dovrebbe funzionare allo stesso modo, come invece non è.

Al tecnico in accolto chiedo se le mie osservazioni siano ingenue e una spiegazione possibile della faccenda. Dieci punti esperienza a chi mi chiarisce le idee.
