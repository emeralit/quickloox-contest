---
title: "Un addio esagerato"
date: 2021-02-22T02:54:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [AppleScript, The Eclectic Light Company, MarK Twain, Steve Jobs, Swift, Python, JavaScript] 
---
[Addio, AppleScript](https://eclecticlight.co/2016/11/18/goodbye-applescript/).

Lo scriveva *The Eclectic Light Company* nel novembre 2016 e, come già fecero Mark Twain e Steve Jobs, oggi AppleScript potrebbe commentare che la notizia del suo abbandono è stata leggermente esagerata.

Molti punti dell’articolo sono indubbiamente validi. Apple non sembra dedicare grande attenzione ad AppleScript (non che sia la prima volta), molte risorse di programmazione sono andate nello sviluppo di Swift e, aggiungerei, l’uso di linguaggi di scripting più vicini a un linguaggio di programmazione classico (Python, per non fare nomi) è letteralmente esploso. Proprio AppleScript, dopotutto, può essere impostato per l’uso di JavaScript al posto di se stesso.

Ciononostante, la tesi di fondo del pezzo è errata, o almeno in cospicuo ritardo:

>Mi aspetto che nel 2017 verranno confermate la morte di AppleScript e la sua sostituzione a opera di un nuovo sistema di scripting basato sui playground Swift, che non solo funzionerà su macOS ma offrirà nuove possibilità a chi usa iOS.

Ciononostante, mi piace pensare che Apple sia poco motivata su AppleScript ma una lezione o due l’abbia imparata. E stia sviluppando lentamente qualcosa di meglio del semplice abbandono di AppleScript.

Dalla profezia funesta sono passati più di quattro anni e potrebbe sembrare anche un buon segno. Magari ci fosse una strategia di scripting ad ampio raggio e a regola d’arte, che si sta sviluppando anche se richiede molto tempo per via delle tante considerazioni di cui tenere conto, relativamente al passato, alla compatibilità, all’opportunità di creare ponti tra apparecchi diversi.

Voglio pensare che AppleScript abbia cose utili da dire, anche attraverso una trasformazione radicale, perché no?, e che il momento dell’**end tell** sia ancora lontano.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*