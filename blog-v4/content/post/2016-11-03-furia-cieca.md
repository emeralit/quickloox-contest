---
title: "Furia cieca"
date: 2016-11-03
comments: true
tags: [MacBook, Pro, accessibilità]
---
L’annuncio dei [nuovi MacBook Pro](http://www.apple.com/it/macbook-pro/) è rimarchevole per la sua capacità di suscitare ogni genere di reazioni negative. Tra le quali spicca l’accusa di *AppleWorld.Today*, a seguito della lettera di una lettrice cieca: [Apple ha fissato i prezzi dei nuovi MacBook Pro fuori portata da molti appartenenti alla comunità dei disabili](http://www.appleworld.today/blog/2016/11/1/apple-has-priced-the-new-macbooks-out-of-reach-of-many-in-the-disabled-community).

L’argomento è che molti disabili, anche in possesso di talenti straordinari, sono disoccupati e quindi per loro un nuovo MacBook Pro costa troppo a causa dell’aumento di prezzo che è stato praticato.

Apple continua a offrire prodotti accessibili a livello unico nel settore tecnologico e probabilmente anche fuori da questo. Il [trattamento riservato all’accessibilità](https://macintelligence.org/posts/2016-10-29-cattiverie-accessibili/) durante il *keynote* parla da solo: la concorrenza, su questo, è diversi giri indietro.

Credo però che se gli aumenti di prezzo di Apple hanno messo i MacBook Pro fuori portata dai disabili disoccupati, lo hanno fatto anche per i disoccupati normodotati. Come succede per le Bmw nelle auto, gli alberghi a cinque stelle, gli *home cinema*, l’ingresso a Disneyland.

Alcuni beni costano più di altri e alcune categorie di beni costano più di altre. Un MacBook Pro dovrebbe essere acquistato se serve; se serve, probabilmente occorreranno sacrifici più lunghi o importanti. Ma andrà preso comunque.

Se non serve, si prende qualcos’altro che sia più a portata. In che cosa un nuovo MacBook Pro è specialmente utile a un disabile, rispetto a un MacBook Pro usato, per esempio? Chiamare in causa i disabili come categoria qui mi sembra fuori luogo, se non per cavalcare l’onda dei commenti e metterci il proprio timbro.

Piacerebbe anche a me che i Mac fossero un servizio sociale erogato con grandi agevolazioni alle persone più bisognose, ma ancora non è così. Piuttosto, perché non lanciare una *startup* con la missione di raccogliere fondi per l’acquisto di tecnologia dove è necessario?