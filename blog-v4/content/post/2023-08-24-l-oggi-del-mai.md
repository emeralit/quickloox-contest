---
title: "L’oggi del mai"
date: 2023-08-24T03:04:02+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Giovanni Sartori, Sartori, Gene Munster, Munster]
---
Parlavamo di [catastrofismo viziato dall’impossibilità di traguardare la propria analisi oltre la normale aspettativa di vita](https://macintelligence.org/posts/2023-08-23-il-futuro-dei-miopi/) e in un certo senso costretto a posizionare temporalmente le catastrofi troppo vicine a noi.

[Giovanni Sartori](http://www.giovannisartori.it) è stato un insigne politologo e sociologo, campi nei quali ha lasciato un’impronta importante e autorevole.

Ha ottenuto riconoscimenti a pioggia; nondimeno, ha voluto trascorrere gli ultimi anni della sua vita come editorialista del *Corriere della Sera*, dove spesso e volentieri scriveva dell’imminente rischio da sovrappopolazione.

La demografia è scienza complicata, con tempi lenti. Ma una tendenza italiana è chiara dagli anni settanta; la natalità e la popolazione sono in declino, sempre più netto. Una tendenza mondiale è ugualmente in atto: l’incremento della popolazione rallenta e nel giro credo di un secolo sarà l’intero pianeta ad avere un saldo negativo tra nascite e morti.

La bomba demografica paventata da Sartori, mancato nel 2017, non è scoppiata. Possiamo pensare che scoppi nonostante le tendenze in atto, ma potrebbero magari volerci decine di anni. E c’è da guardare al rischio con gli occhi dei dati. A fianco di luoghi dove l’incremento demografico è sostenuto e mette in crisi strutture e risorse, si iniziano a vedere Stati dove la carenza di nascite è arrivata generazione dopo generazione a mettere in questione la capacità di funzionamento strutturale dello Stato sociale. Una bomba demografica che implode anziché scoppiare.

Viene da chiedersi come mai Sartori fosse ossessionato dalla sovrappopolazione. La risposta mia non ha niente di scientifico, è una sensazione: era diventato uno di quegli anziani che si sentono al termine della vita e faticano ad accettare l’idea che il mondo possa andare avanti senza essi. Per Sartori ogni bambino appena nato era una ferita alla sua vecchiezza; ogni nascita era di troppo, per un signore in attesa di confrontarsi con la morte. L’eccesso di popolazione doveva essere un pericolo e doveva esserlo *subito*.

Umanamente comprensibile, scientificamente poco fondato. Catastrofista nocivo.

Mi viene da fare un parallelo con [Gene Munster](https://tcbmag.com/tcb-100-people/gene-munster/), l’analista con una carriera luminosa nel settore, a oggi ancora in divenire, che per anni si perse dietro all’annuncio del televisore Apple.

Trimestre dopo trimestre Munster adeguava le sue previsioni a una realtà che non le inverava. Spiegava perché Apple non aveva presentato il televisore e perché ciò sarebbe puntualmente accaduto all’occasione successiva.

Su che cosa si basava Munster? Su una sua idea, forse maturata in base a una soffiata magari fatta ad arte per confondere le acque, che non aveva il fegato di riconoscere infondata e così facendo perdere parte della sua autorevolezza percepita.

Può darsi benissimo che Apple presenterà un televisore. Certamente, se avverrà, la cornice temporale non sarà quella presagita da Munster. Anche per lui, il televisore doveva arrivare entro il tempo che lui riteneva utile per dare corpo alle sue previsioni. Se avesse scritto del televisore in arrivo entro quarant’anni, dati alla mano ovviamente, le sue probabilità di azzeccarci sarebbero state molto più alte. Ma non poteva. Il catastrofista, anche alla rovescia come in questo caso, deve fare in fretta. E questo lo connota come dannoso per la comunità.

*Gli aggiornamenti in agosto del blog potrebbero essere irregolari o infrequenti.*