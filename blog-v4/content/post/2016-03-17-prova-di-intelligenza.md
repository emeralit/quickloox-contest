---
title: "Prova di intelligenza"
date: 2016-03-17
comments: true
tags: [Dunst, iCloud, hacker]
---
Diciamo che fai l’attrice e hai un iPhone. Improvvisamente diventano pubbliche tue foto private in cui sei nuda, presumibilmente presenti sul tuo iPhone, con relativa copia di sicurezza su iCloud. A commento, scrivi [un tweet](https://twitter.com/kirstendunst/status/506553772114317312) di questo tenore:<!--more-->

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Thank you iCloud🍕💩</p>&mdash; Kirsten Dunst (@kirstendunst) <a href="https://twitter.com/kirstendunst/status/506553772114317312">September 1, 2014</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Nonostante Apple [affermi che iCloud non è stato violato](http://tech.firstpost.com/news-analysis/apple-says-icloud-wasnt-hacked-in-celebrity-nude-photo-leaks-232044.html), grazie a reazioni come questo *tweet*, fioccano titoli come [Lo scandalo delle foto delle celebrità nude mette gli utenti di iCloud sulla difensiva](http://www.standard.net/Entertainment/2014/09/02/Celebrities-react-to-leaked-nude-photos-scandal).

Molti mesi dopo [si appura](https://www.justice.gov/usao-cdca/pr/pennsylvania-man-charged-hacking-apple-and-google-e-mail-accounts-belonging-more-100) che un trentaseienne ha spedito posta fintamente proveniente da Apple o Google alle celebrità in questione, per farsi dare le coordinate dei loro account. Il trucco gli ha permesso di forzare 50 account iCloud e 72 account Gmail (naturalmente nessuno ha titolato di *utenti Gmail sulla difensiva*).

In pratica, le tue foto private volano su Internet perché hai fornito la tua autenticazione iCloud a uno sconosciuto che si è spacciato per un’azienda.

Questo prova in maniera definitiva la tua intelligenza fuori dal *set*.