---
title: "L’argomento della diagonale"
date: 2012-11-05
comments: true
tags: [Cantor, Nexus7, iPad2, Siri, iPadmini, Guerrera, TheParrot, codec, Gsm, Lte, iFixit, Glonass, Pages, Numbers, Keynote, iOS5, diagonale]
---
Rubo il titolo a Georg Cantor, il matematico che mostr&#242; come <a href="http://www.lidimatematici.it/blog/2010/12/30/un-infinito-piu-grande-i-reali-largomento-diagonale-di-cantor-parte-3/">l&#8217;infinito dei numeri reali sia pi&#249; grande di quello dei numeri naturali</a>. Lo applico a cose pi&#249; pedestri, per&#242;. Parlo arbitrariamente di <i>argomento della diagonale</i> quando, per dare un parere su qualcosa, faccio un riferimento obliquo. Esempio:<!--more-->

>Compro un Nexus 7 perch&#233; iPad mini ha Siri e iPad 2 no.

Nexus 7 &#232; concorrente di iPad mini (semplifico, avrei cose da dire, me le tengo). iPad mini ha Siri. Al limite potrebbe essere un punto a favore di iPad mini.

Argomento diagonale, invece. Tra iPad mini e Nexus 7 decide iPad 2. Perch&#233;?

Sar&#242; maligno: l&#8217;articolo dell&#8217;altrimenti bravo Piersandro Guerrera su *The PaRRoT* (<a href="http://www.theparrot.it/2012/11/03/siri-potrebbe-dar-voce-al-tuo-ipad-ma-non-lo-fa/">Siri potrebbe dare voce al tuo iPad ma Apple non vuole</a>) a me pare molto <i>ho un iPad 2 e sono frustrato perch&#233; non c&#8217;&#232; Siri</i>. Sentimento personale pi&#249; che legittimo. Purtroppo porta a giudizi di merito poco centrati.

La sua sintesi di iPad mini:

>[&#8230;] in realt&#224; un iPad 2 con uno schermo pi&#249; piccolo, fotocamere migliorate e una nuova antenna Wi-Fi. [&#8230;] &#232; stata inoltre sostituita l&#8217;antenna GSM con una [&#8230;] LTE ma, per il resto, ci troviamo a tutti gli effetti di fronte a un iPad 2.

Pi&#249; avanti si parla addirittura di *hardware sostanzialmente riciclato*. Sar&#224; vero?

<table>
	<tr align=left>
		<th></th><td>&#160;</td><th>iPad 2</th><td>&#160;</td><th>iPad mini</th>
	</tr>
	<tr align=left>
		<th>Bluetooth</th><td>&#160;</td><td>2.1 + Edr</td><td>&#160;</td><td>4.0</td>
	</tr>
	<tr align=left>
		<th>Gps</th><td>&#160;</td><td><strike>No</strike>Sì</td><td>&#160;</td><td>S&#236;</td>
	</tr>
	<tr align=left>
		<th>Glonass</th><td>&#160;</td><td><strike>No</strike>Sì</td><td>&#160;</td><td>S&#236;</td>
	</tr>
	<tr align=left>
		<th>Sim</th><td>&#160;</td><td>Micro</td><td>&#160;</td><td>Nano</td>
	</tr>
	<tr align=left>
		<th>Connettore</th><td>&#160;</td><td>30 pin</td><td>&#160;</td><td>Lightning</td>
	</tr>
	<tr align=left>
		<th>Speaker</th><td>&#160;</td><td>Mono</td><td>&#160;</td><td>Stereo</td>
	</tr>
	<tr align=left>
		<th>Batteria</th><td>&#160;</td><td>25 watt/ora</td><td>&#160;</td><td>17 watt/ora</td>
	</tr>
</table>
<br />

Non &#232; vero. (Glonass &#232; il Gps russo).

Ma il processore &#232; quello, la Ram &#232; quella e quindi, si argomenta, se Siri gira su uno e non sull&#8217;altro &#232; perch&#233; cos&#236; vuole il *marketing*.

*Potrebbe* essere vero.

Tuttavia so benissimo che Siri funziona solo se c&#8217;&#232; Internet. Fuori dall&#8217;ovvia capacit&#224; di elaborazione di base, processore e Ram *non contano*. Quello che fa di impegnativo Siri, lo fa sui server Apple. iPad manda l&#8217;audio su Internet dopo averlo codificato, con una circuiteria detta in gergo *codec*.

iPad 2, <a href="http://www.ifixit.com/Teardown/iPad+2+Wi-Fi+Teardown/5071/2">sostiene iFixit</a>, monta un codec audio identificato da Apple con la sigla 338S0940. iPad mini <a href="http://www.ifixit.com/Teardown/iPad+Mini+Teardown/11423/3">contiene invece</a> un 338S1116. iPhone 4S, capace di Siri, <a href="http://www.ifixit.com/Teardown/iPhone+4S+Teardown/6610/2">contiene un</a> 338S0987, versione superiore a quella di iPad 2. iPhone 4, che non ha Siri, <a href="http://www.ifixit.com/Teardown/iPhone+4+Teardown/3130/3">contiene un</a> 338S0589, versione inferiore.

I codec audio su iPad 2 e iPad mini sono diversi. Non è una prova, ma gli indizi sono coerenti: l&#8217;assenza di Siri su iPad 2 *potrebbe* avere ragioni tecniche e sto ancora cercando informazioni sui rispettivi microfoni. Tuttavia l&#8217;idea che Apple stia *castrando funzionalit&#224;* su iPad 2 &#232;, come minimo, ipotetica fino a dimostrazione.

Quando si comincia con il partito preso &#232; facile scivolare. L&#8217;articolo si conclude infatti affermando che le edizioni aggiornate di Pages, Numbers e Keynote non funzionino su iPad di prima generazione, fermo a iOS 4. Peccato che iPad di prima generazione funzioni con iOS 5 e le usi perfettamente, a smentita dell&#8217;impianto stesso dell&#8217;articolo.

Il che non c&#8217;entra assolutamente nulla con iPad 2 e Siri. Ma appunto, quando si giudica A tirando in ballo B &#8211; argomento diagonale &#8211; c&#8217;&#232; qualcosa che non torna.