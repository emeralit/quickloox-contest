---
title: "Chi non tocca muore"
date: 2023-01-05T00:27:27+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [CarPlay, BlackBerry, iPhone, Ballmer, Steve Ballmer]
---
Non capisco perché sia tornata in auge in questi giorni la notizia di uno studio secondo cui [i comandi fisici sulle auto sarebbero più semplici e sicuri da usare di un touchscreen](https://www.caranddriver.com/news/a40949962/car-buttons-easier-than-touchscreens-study/).

Scommetto che nel 2007 sarà uscito qualche studio sulla superiorità della tastiera fisica di BlackBerry su quella logica di iPhone. Tastiera logica che, ricorderemo Steve Ballmer quando faceva il comico invece che il proprietario di squadre di basket, [nessuno nel mondo business avrebbe comprato in quanto privo di tastiera](https://macintelligence.org/posts/2013-09-22-sei-anni-di-differenza/).

Dove la lucidità dell’analisi si indebolisce è nell’ignorare che va considerata l’interfaccia utente nel suo complesso. Un touchscreen è un pezzo dell’interfaccia: uno strumento di input. Che altro succede, nel sistema? Si potrebbe argomentare infallibilmente che una tastiera è molto più efficiente di un mouse nell’impartire comandi; bravo, accendi la macchina del tempo, vai nel 1984 e cancella il progetto Macintosh (con uno schermo a due colori, poi).

Cattive interfacce, che si appoggiano in modo sbagliato a un touchscreen, sono sicuramente inferiori a buone interfacce che ne fanno l’uso migliore. Posso dire per esperienza che certi comandi fisici caratteristici di certe auto letteralmente implorano di essere sostituiti da un comando digitale, o vocale, o da un gesto nell’aria, o da…

Un’interfaccia utente era più di uno strumento di input. Oggi, è molto di più. Toccare è talvolta inferiore a premere o girare o tirare; talvolta no. Demonizzare il tocco a prescindere è avere capito poco.