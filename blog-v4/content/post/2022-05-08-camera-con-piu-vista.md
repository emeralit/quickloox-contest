---
title: "Camera con (più) vista"
date: 2022-05-08T02:01:24+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Camo, Obs, Fëarandil]
---
Come proprietario di un Mac mini e di uno schermo privo di webcam integrata, mi sono interessato alle soluzioni di telecamera virtuale per usare un iPhone o un iPad come webcam di Mac e, grazie a [Fëarandil](https://www.twitch.tv/fearandil), ho scoperto [OBS](https://obsproject.com) con cui da molti mesi mi trovo molto bene, in congiunzione con [Camera for OBS Studio](https://apps.apple.com/it/app/obs-studio-iphone-video-source/id1352834008) su iOS. L’unica lamentela che posso avere è mancare del tempo per approfondire il funzionamento di un software completissimo e spaziale nelle sue possibilità.

L’unico problema di OBS è che funziona con *quasi* tutte le applicazioni su Mac. L’esempio più eclatante sulla mia scrivania è quello di [Miro](https://miro.com/), che non riconosce la telecamera virtuale dentro la propria app. Così mi tocca usarlo dentro il browser; funziona benissimo eh, però per tutta una serie di ragioni non è ottimale.

Per chi avesse problemi analoghi, attenzione a [Camo](https://reincubate.com/camo/). È meno versatile di OBS e, in versione Pro, è a pagamento, non proprio regalato.

In modalità gratuita, tuttavia, con le sole funzioni essenziali attivate, funziona bene… e funziona anche su Miro.

Sicuramente resto su OBS che almeno in prospettiva mi consente di guardare più lontano rispetto alle cose da imparare: come eccezione, Camo è ottimo per chiudere la falla.

Iniziare à gratis, installare è veramente molto semplice e interamente guidato, i comandi a disposizione sono praticamente nessuno nel livello gratuito. Paradossalmente, vale quasi più questo che avere a pagamento le opzioni più evolute.