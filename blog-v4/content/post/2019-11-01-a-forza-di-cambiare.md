---
title: "A forza di cambiare"
date: 2019-11-01
comments: true
tags: [AirPods, Pro, Apple, watch]
---
Apple ha segnato un nuovo [record di sempre per il quarto trimestre fiscale](https://www.apple.com/newsroom/2019/10/apple-reports-fourth-quarter-results/) dell’anno, in un momento di flessione di iPhone, il prodotto da cui dovrebbe essere dipendente e troppo legata secondo numerosi sedicenti analisti.

A farla crescere, e si parla di un gigante da duecento miliardi, sono i servizi e poi prodotti come Watch oppure AirPods.

John Gruber, nel provare in anteprima AirPods Pro con la cancellazione del rumore, la modalità Transparent che cancella selettivamente, nuovi comandi e nuova qualità, scrive che la differenza tra questi e i vecchi AirPods [è come tra il giorno e la notte](https://daringfireball.net/2019/10/airpods_pro_first_impressions).

Tim Cook dichiara in un’intervista che domani il contributo più grande che sarà riconosciuto ad Apple [sarà quello per la salute delle persone](https://arstechnica.com/gadgets/2019/10/apple-earnings-q4-2019-tim-cook-says-health-will-be-what-apple-is-remembered-for/). Si parla dell’azienda che ha cambiato il computing, la musica, la telefonia, la stessa che è diventata la più capitalizzata di sempre ma è lontana dal sedersi sugli allori e giocare di rimessa.

Nel frattempo watch continua a salvare vite qua e là, ultimo caso quello di un californiano caduto da una scogliera; il suo computer da polso [ha riconosciuto la caduta violenta e chiamato da solo il numero delle emergenze](https://arstechnica.com/gadgets/2019/10/apple-earnings-q4-2019-tim-cook-says-health-will-be-what-apple-is-remembered-for/).

Piaccia o no, e han voglia i nostalgici a tirare fuori la mancanza di innovazione, la frontiera del cambiamento tecnologico oggi sta sul polso o nelle orecchie. Apple ha trasformato in computer l’orologio da polso e sta facendo lo stesso per gli auricolari.

Gli altri, nella sostanza, seguono. I più disperati copiano e basta; Xiaomi arriva oggi con un orologio intelligente che, ma guarda, [somiglia moltissimo a un watch](https://arstechnica.com/gadgets/2019/10/apple-earnings-q4-2019-tim-cook-says-health-will-be-what-apple-is-remembered-for/).

Ci si può lamentre dei prezzi, o dei bug degli ultimi sistemi operativi, anche a ragione. Nel complesso, però, è solo Apple che insiste nel voler cambiare seriamente e in meglio le cose. Qualcosa che non può più accadere nei mercati ultradecennali come quello dei portatili, dove al massimo si registrano miglioramenti incrementali.
