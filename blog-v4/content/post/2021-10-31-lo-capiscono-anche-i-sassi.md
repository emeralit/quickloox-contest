---
title: "Lo capiscono anche i sassi"
date: 2021-10-31T01:59:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [AI4Mars, Facebook, Meta] 
---
In questo periodo ho seguito più comunicazione social del necessario e, tra vaccini, decreti legge e metaversi, ho decisamente accusato il colpo.

Faccio già ora un proposito per il nuovo anno: passare più tempo a contornare rocce su [AI4Mars](https://www.zooniverse.org/projects/hiro-ono/ai4mars) che perdere tempo dietro a chi si esprime su Facebook e compagnia seppure afflitto da evidenti e futili problemi personali.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*