---
title: "Altre notti interminabili"
date: 2017-08-23
comments: true
tags: [Python, DataCamp]
---
Se proprio [leggere non funziona](https://macintelligence.org/posts/2017-08-21-notti-interminabili/), c’è sempre l’[apprendimento interattivo di Python](https://www.datacamp.com/courses/intro-to-python-for-data-science) orientato alla *data science*.

Ci ho provato e sono arrivato per alcune serie di esercizi fino alle conversioni di tipo. Sapevo già fare tutto e il mio giudizio potrebbe essere viziato, ma l’ho trovato veramente a portata di chiunque.

Tutto avviene dentro il *browser*, volendo ci si registra con Google o Facebook, è gratis, nel *browser* c’è una *shell* interattiva per fare esperimenti e non sbagliare prima di dare la soluzione, insomma, iniziare con Python è veramente una cosa che si può fare senza paura, anche se non si è mai scritta una riga di codice in tutta la vita.

Il corso è in inglese. Primo: è inglese semplicissimo. Secondo: è una occasione perfetta per imparare rudimenti di inglese intanto che si imparano quelli di Python. Come scusa è meglio altro.

(L’*agent provocateur* aggiunge che per uscire dalla massa, compiere una scelta controcorrente, cantare fuori dal coro e collezionare luoghi comuni c’è a disposizione anche un [tutorial interattivo per imparare Lisp](http://cliki.net/online+tutorial)).