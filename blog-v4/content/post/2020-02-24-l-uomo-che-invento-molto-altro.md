---
title: "L’uomo che inventò molto altro"
date: 2020-02-24
comments: true
tags: [Jobs, Parc, Xerox, Tesler, Post, Markoff, Friden, Time, Nyt, Markoff, Wi-Fi, Newton, Gypsy]
---
Prima di lasciare definitivamente [riposare in pace Larry Tesler](https://macintelligence.org/posts/2020-02-20-intelligenza-e-interfaccia/) suggerisco una ripassata al [necrologio scritto da John Markoff per il New York Times](https://www.nytimes.com/2020/02/20/technology/lawrence-tesler-dead.html).

>Oltre a contribuire allo sviluppo di Lisa e Macintosh, Mr. Tesler fondò e diresse l’Advanced Technology Group di Apple, dopo il quale guidò la progettazione di Newton, anche se questo non fu un successo. Il gruppo creò anche molta della tecnologia che sarebbe divenuta lo standard wireless Wi-Fi e Mr. Tesler diresse una joint venture tra Apple e altre due aziende per la creazione di Acorn Risc Machine, una partnership che intendeva fornire un microprocessore per Newton. […] L’architettura creata per la partnership è oggi il progetto di microprocessore più usato al mondo.

Newton.

Wi-Fi.

Arm.

Un gigante. Senza contare tante intuizioni e iniziative che ebbero meno successo oppure non raggiunsero le giuste orecchie.

Giusto per non congedarlo come *l’inventore del copia e incolla*, importante, certo. Ci fu molto altro.

P.S.: agli amici del *Post*: Markoff spende quattro righe per avvisare di avere scritto *Frieden* al posto di *Friden* e di avere corretto, con tanto di giorno e ora. Un po’ diverso dal [metodo discutibile](https://macintelligence.org/posts/2020-02-23-deontologia-portami-via/) che usa, ehm, una certa redazione.

P.P.S.: **matt2bono** chiedeva una foto della giornata delle nozze. Le sto aspettando, per ora ho questa. :-)

 ![In diretta dagli imenei](/images/nozze.jpeg  "In diretta dagli imenei") 