---
title: "Morte da mille ferite"
date: 2014-07-04
comments: true
tags: [iOS, Android, Htc, Ux]
---
Trovo impossibile sintetizzare adeguatamente l’articolo di Jackson Fish Market [Un progettista di esperienze utente passa da iOS ad Android](http://jacksonfish.com/2014/05/22/a-user-experience-designer-switches-from-ios-to-android/) perché è lunghissimo e pieno di punti interessanti, persino troppi. Quindi lo farò sgocciolare di tanto in tanto nelle prossime settimane. Tutto il sapere che c’è dentro deve essere preservato.<!--more-->

Basti per ora sapere che l’autore del *post* ha vissuto su Android per oltre due mesi ed è un esperto del settore, non un parolaio.

>Sto usando il mio Android come unico telefono da oltre due mesi e ritengo di avere colto il succo dell’esperienza. La progettazione di esperienza utente, secondo me, è artigianato. Parte arte, parte scienza. Come la cucina. Dove quello che ha un buon sapore è soggettivo. Non sostengo di avere l’ultima parola sulle cose che ho scoperto, ma ritengo che le mie osservazioni abbiano del merito.

Per ora, la sua conclusione:

>Il mio telefono Android Htc è buono. Non è terribile. Non è grandioso. È buono. Sostanzialmente funziona. Ma è la morte da mille ferite. Quei momenti di esperienza utente fuori luogo si sommano e alla fine non amo il mio telefono Android. Non mi piace usarlo. Preferisco non usarlo per navigare. Non sono entusiasta di installare nuove app. E le cifre sull’e-commerce e sulla navigazione, di Android contro iOS, lo mostrano.

Se non altro, un’ennesima spiegazione del perché il mondo esplode di terminali Android, ma il traffico web resta drammaticamente inferiore a quello di iOS. Morte da mille ferite.