---
title: "Un compleanno perfetto"
date: 2023-11-14T13:21:01+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [FreeCiv]
---
Secondo la ricerca del blog, questo è il [quattordicesimo post dedicato a FreeCiv](https://macintelligence.org/search/?query=freeciv), gioco strategico open source ispirato alla storia della civilizzazione umana, semplice da cominciare e infinito da padroneggiare.

Il percorso è stato talvolta accidentato e si è parlato di mancanza di fondi, oppure di mancanza di sviluppatori, o ancora di mancanza di sviluppatori Mac. Tenere in piedi a livello di passione e di hobby il lavoro di sviluppo e di manutenzione attorno a un gioco grande e complicato è un risultato straordinario, di cui chiunque abbia data almeno minimamente una mano può vantarsi. Un bello scacco all’entropia.

Questo compleanno può considerarsi lieto. [C’è una versione Mac](https://macintelligence.org/posts/2023-07-17-grandi-temi-piccole-vite/) e [testimonio che funziona](https://macintelligence.org/posts/2023-04-18-marcia-indietro-prima-avanti-di-nuovo/); il gioco è totalmente accessibile [online via browser](https://www.freecivweb.org) anche su iPad.

Il sito per giocare online e il [sito del progetto](http://www.freeciv.org) accettano volentieri contributi di ogni genere, da una mano sulle traduzioni a bug fix del codice al bieco denaro, che serve anche lui. Le donazioni sul sito del progetto coprono i costi di hosting, condiviso assieme a un altro gioco, [Warzone 2100](https://wz2100.net), un altro strategico però 3D e giocato in simultanea, anche lui giocabile su Mac. Il sito per giocare online è accessibile del tutto gratis; i benefattori possono abbonarsi per sostenerlo, a partire da due euro al mese. Stanno arrivando le feste e un gioco online di qualità accessible a chiunque, dal codice aperto e disponibile, è una causa molto meno frivola di quanto si possa pensare.

Un buon compleanno dunque, per una iniziativa in funzione, che progredisce, vitale.

Il ventottesimo compleanno (quanti programmi possono vantare la stessa longevità?). [Numero perfetto](https://www.treccani.it/enciclopedia/numero-perfetto_(Enciclopedia-della-Matematica)/). Il prossimo, gente, è 496; decisamente un auspicio ambizioso.