---
title: "58 minuti per caricare"
date: 2021-12-27T02:52:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple Watch] 
---
Molti anni fa affermai che dover mettere sotto carica watch tutti i giorni fosse un non-problema. Concordo con me stesso e rilancio: non è un problema.

Un particolare proprio della Serie 7 è che quella volta al giorno dura meno di un’ora. Di fatto tolgo l’apparecchio al mattino, faccio doccia e colazione e da lì la batteria è come minimo sopra l’ottanta percento. Un attimo di pazienza e si arriva a carica completa. È una cosa che amo.

Al reparto delusioni metto invece il modulo di analisi del sonno. Il mio ritmo è estremamente irregolare e dormo meno del giusto su base quasi sistemica: sono curioso di avere una vista di insieme, comoda e automatica come quella che watch fornisce in altri campi (per esempio la saturazione dell’ossigeno nel sangue; non faccio mai il test, ci pensa lui da solo, nel momento in cui avessi la curiosità sarebbe sempre già pronto un dato vecchio al massimo di qualche ora).

Invece la funzione sonno è costruita attorno a persone con un ritmo di sveglia regolare, con insistenza sulla sveglia. Praticamente il contrario di quello che mi serve.

Momenti di frustrazione o di adorazione di watch che si inseriscono in un bilancio comunque molto positivo e soddisfacente. Lo avevo lasciato come strumento ausiliario che semplificava la vita e umanizzava le notifiche ed è rimasto così, in primis, con tanta usabilità in più e una ottima maturazione del sistema operativo. Le feste in famiglia sono state un ottimo banco di prova.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per Qui
ckLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._