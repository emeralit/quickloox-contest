---
title: "Impressioni di ottobre"
date: 2019-10-19
comments: true
tags: [ iPad, Pro, iOS, 13]
---
È passato molto tempo prima che iPad Pro si aggiornasse a iOS 13. Lascio che sia la macchina a occuparsene, visto che sa farlo, solo che vuole farlo di notte e spessissimo il mio iPad non è in carica, condizione vincolante per l’aggiornamento. Ci siamo infine arrivati e da pochissimo tempo sono su iOS 13.

Due prime impressioni generali: finalmente tutto quello spazio sullo schermo da 12"9 viene impiegato come si deve; iPadOS 13 è veramente un nuovo inizio, con tutto quello che ne consegue.

La nuova schermata Home tiene un numero siderale di icone in vista, tanto che sto riorganizzando la disposizione delle app. È una notizia, perchè non lo facevo circa dalla notte dei tempi. La batteria sembra leggermente più durevole, il sistema un pochino più veloce; deve esserci qualche miglioramento alla gestione tipografica, perché i caratteri sembrano più nitidi e le tastiere software più reattive. Potrebbe essere suggestione, eh.

iPadOS è una mossa saggia e lungimirante, che darà i suoi frutti nel tempo. Ora il sistema è promettente, che non vuol dire compiuto, e qua e là inciampa nel tentativo di offrire un servizio migliore. Mi è capitato di risvegliare la macchina e vedere lo schermo distorto per qualche secondo. In una occasione la parte destra del Dock, dove alloggiano le ultime app aperte, non rispondeva. Tutto si è risolto da solo, ma lascia un senso di incompiutezza.

I lavori in corso si notano anche su Safari e su Mail. Ho torchiato la nuova navigazione desktop-style sui siti che davano più fastidio, per esempio WordPress e Mailchimp: i miglioramenti sono molti più dei nuovi problemi. Ancora, però, non è esattamente come navigare su un Mac. Mail è migliorato in molti punti e però ora non legge più gli allegati MIME di testo: ricevo una mailing list in formato digest Mime e ho dovuto adottare la modalità testo, in quanto per il programma sono allegati e tutt’al più vanno inoltrati a qualche altra app. Il blocco dei pop-up, con la richiesta di abilitarli sito per sito quando si presenta la necessità, mi piace; protegge il *comfort* della navigazione con invasività minima.

Con la app Files la questione di non avere accesso al filesystem è definitivamente chiusa; Dropbox, iCloud Drive, pCloud e via dicendo si amministrano in scioltezza e davvero non importa più dove stia un documento; lo si lavora e basta.

Il *dark mode* è apprezzabile. Non sono un estimatore e non lo userei sempre; lasciare che si imposti durante la notte è gradevole e rende ancora più comfortevole fare qualcosa a letto prima di dormire.

Non ho avuto alcun problema di compatibilità.

Complessivamente il voto è alto, non da perfezione. iPadOS è avviato molto bene verso un destino significativo, che gli faccia valorizzare la macchina per tutto quanto può dare. Il percorso è appena iniziato e qua e là ci sono dettagli da sistemare, nulla però che faccia venire voglia di tornare indietro.

Non ho perso niente ad aspettare, molti giorni, che iPad si aggiornasse da solo a iOS 13. Ad aggiornare, comunque, ci si guadagna e lo consiglio.
