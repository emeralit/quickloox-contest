---
title: "Libertà di scelta"
date: 2020-08-23
comments: true
tags: [Goldman, Sachs, Apple, San, Francisco, NYT, Times, Helvetica, Microsoft, Arial, Roman, Times, Macintosh]
---
Goldman Sachs che [si fa disegnare un font su misura](https://www.nytimes.com/2020/08/21/business/goldman-sachs-font.html?utm_source=nextdraft&utm_medium=iosapp) è il segnale chiaro che l’aspetto dei propri contenuti oramai quanto la sostanza.

Essendo una banca, arriva buona ultima. L’articolo dà abbondantemente conto di come lo abbiano già fatto infinite aziende importanti e anche più volte, tra cui naturalmente Apple che ha al suo attivo il recente font [San Francisco](https://macintelligence.org/blog/2015/05/27/san-francisco-aiutaci-tu/) e che, [con il primo Macintosh](http://www.storiesofapple.net/the-first-fonts-of-the-macintosh.html), ha svelato al mondo che i computer potevano scrivere con un carattere diverso da quello della matrice di punti sullo schermo.

Essendo una banca, Goldman Sachs si è fatta realizzare un carattere noioso e prevedibile, per quanto impeccabile nelle tabelle numeriche. In pratica un lavoro inutile, solo che ne hanno sentita ugualmente l’esigenza.

Times e Helvetica hanno segnato la rivoluzione del desktop publishing e sono venerabili. Tuttavia, al giorno d’oggi, sono diventati talmente classici che bisogna veramente saperli portare, oppure è meglio una scelta di altro tenore. In salsa Microsoft (Arial e New Times Roman) vogliono solo dire che l’autore non si è curato del proprio contenuto nella forma e, con tutta probabilità, la sostanza è sotto media.

Mai come oggi siamo liberi di scegliere il _nostro_ font. Per distinguerci in modo ancora più efficace. Meglio farlo oggi che è libertà, perché più si va avanti più si avvicinerà l’obbligo.