---
title: "Razionalità e razionamento"
date: 2024-02-16T20:01:35+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [ChatGPT]
---
ChatGPT ha un prompt di sistema, messo in pratica all’inizio di una sessione, e adesso [ne conosciamo il contenuto](https://twitter.com/AlphaSignalAI/status/1757466498287722783).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Finally, someone cracked it. The ChatGPT system prompt.<br><br>If you were wondering why GPT became so bad in the past 6 months, its because &quot;laziness&quot; is part of the system prompt: <br><br>1. &quot;When asked to write summaries longer than 100 words write an 80-word summary.&quot;<br><br>2. &quot;DO NOT list or… <a href="https://t.co/HCCm8qdfKv">pic.twitter.com/HCCm8qdfKv</a></p>&mdash; Lior⚡ (@AlphaSignalAI) <a href="https://twitter.com/AlphaSignalAI/status/1757466498287722783?ref_src=twsrc%5Etfw">February 13, 2024</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Riporto alcune perle:

>Se ti chiedono riassunti più lunghi di cento parole, scrivi un riassunto da ottanta parole.

>Non elencare le descrizioni o riferirti a esse, né prima né dopo avere generato le immagini.

>Crea una sola immagine, anche se l’utente te ne chiede di più.

C’è molto più di questo, compreso un diluvio prevedibile di *politically correct*. Il punto è che questa non è razionalità nell’uso delle risorse; è razionamento. I venti dollari al mese dei paganti bastano a malapena per garantire a tutti un servizio minimo e non penso che la situazione migliorerà nel tempo, a partire dalle dimensioni dei modelli.

Nel 2021 uscì lo [studio](https://dl.acm.org/doi/10.1145/3442188.3445922) in cui per la prima volta i chatbot basati su grandi modelli di testo venivano chiamati *pappagalli stocastici*. L’espressione è finita sulla bocca di chiunque, mentre lo studio lo hanno letto in pochi: in pratica è una critica al crescere indiscriminato delle dimensioni dei modelli e del loro consumo energetico. Come se, in sessantaquattresimo, il Club di Roma si fosse occupato dei chatbot.

Nessuno ne ha più fatto parola; i modelli sono proliferati, le loro prestazioni sono aumentate, così come le loro dimensioni e i loro consumi.

Che ChatGPT voglia venti dollari al mese per funzionare (a stecchetto) potrebbe essere un campanellino di allarme: fino a quando e come resteranno sostenibili le *intelligenze artificiali*? Un cervello umano se la cava con spaghetti pollo insalatina e una tazzina di caffè…