---
title: "Professionisti addio"
date: 2016-03-11
comments: true
tags: [Mudec, Sharpe, iPhone]
---
Sempre vivo, il meme che vuole Apple essersi dimenticata dei professionisti.

Posso solo riferire per avere letto del [videoclip](https://www.youtube.com/watch?v=OigOrLjDp7I) di [Edward Sharpe & The Magnetic Zeros](http://www.edwardsharpeandthemagneticzeros.com), [interamente girato con mezza dozzina di iPhone 6S più accessori](http://mashable.com/2016/03/10/olivia-wilde-iphone-music-video/#s1l67eHpmOqC).<!--more-->

<iframe width="560" height="315" src="https://www.youtube.com/embed/OigOrLjDp7I" frameborder="0" allowfullscreen></iframe>

Nel mio piccolissimo, qualche sera fa presenziavo a una *convention* aziendale presso il [Museo delle Culture di Milano](http://www.mudec.it/ita/) e questa era la console del DJ set.

 ![alt](/images/mudec.jpg  "title") 