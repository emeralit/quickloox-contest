---
title: "Il cloud è nudo"
date: 2014-09-03
comments: true
tags: [Dropbox, iCloud, Apple, Cubrilovic]
---
Un po’ tutti sono saltati in groppa alla storia delle attricette nude per guardarsi le foto con una buona scusa e soprattutto per massacrare il bersaglio grosso, cioè iCloud, troppo saporito per rinunciare alla tentazione di mordere anche un po’ a casaccio.<!--more-->

La verità è che iCloud c’entra relativamente. Apple ha ragione a [dichiarare](http://www.apple.com/it/pr/library/2014/09/02Apple-Media-Advisory.html) che il sistema non è stato violato; contemporaneamente sono però emerse alcune debolezze della struttura che *potrebbero* avere facilitato il compito.

Volevo scrivere una cosa più lunga e dettagliata, solo che la nuova arrivata in famiglia a volte sconvolge l’agenda in modo ancora notevole. Ci stiamo organizzando, ma può ancora accadere che il *post* parta alle quattro del mattino e soprattutto con scarsa lucidità residua.

Mi limito dunque ad annotazioni veloci, sperando di tornare sul tema prossimamente con maggiore agio.

* iCloud e altri sistemi (tra cui per esempio Dropbox) non sono stati violati, altrimenti si parlerebbe di milioni di foto e non solo delle attricette; sono stati condotti attacchi mirati verso gli individui, non verso il sistema. Questo comporta per esempio che l’anello debole sia un portatile o un telefono, non il cloud.

* Esiste una Internet dei bassifondi dove si muovono decine, centinaia, migliaia di soggetti, che si cimentano in imprese come questa da molti anni e alimentano un sistema di commerci notevole. Lo scandalo pubblico è cosa di ieri, ma si trafugano foto personali dai servizi cloud da quando esistono e le foto comparse in rete in questi giorni hanno richiesto anni di sforzi.

* Comunque sia, i dati su Internet sono robusti quanto la *password* che si usa. È legittimo chiedere sistemi sicuri, ma poi la *password* deve essere ottima, specialmente se si ritiene di poter finire nel mirino in quanto personaggio pubblico.

* Se hai foto nude, per quanto sia forte la *password*, conviene non tenerle in rete. Mai, qualunque sistema tu stia usando.

* Gli unici dati sicuri in rete, *password a parte*, sono quelli cifrati.

Fino a nuovo ordine, la lettura per capire la portata del fenomeno è [Notes on Celebrity Data Theft](https://www.nikcub.com/posts/notes-on-the-celebrity-data-theft/) di Nik CUbrilovic.