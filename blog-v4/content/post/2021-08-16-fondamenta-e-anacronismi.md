---
title: "Fondamenta e anacronismi"
date: 2021-08-16T00:00:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [curricolo, scuola, Lidia, Nive, Google, cetonia dorata] 
---
Ho ricevuto diverse obiezioni alla mia proposta di [curricolo digitale per i due cicli della scuola primaria](https://macintelligence.org/posts/Un-curricolo-per-lestate.html), che ruotano attorno a un argomento: l’opportunità di esporre bambini e ragazzi al computer in modalità che potrebbero essere troppo sofisticate per loro oppure arrivare troppo presto, o entrambe le cose.

Sono d’accordissimo nell’evitare di tirare in ballo i computer quando non serve o prima del tempo. Con un quaderno a quadretti si possono fare miracoli per introdurre idee di *coding* e certamente qualsiasi ragazzo dovrebbe imparare a tirare righe dritte a mano, con l’ausilio di un righello, prima di affidarsi a qualsiasi programma di disegno.

A un certo punto, tuttavia, il computer deve arrivare, perché i fondamenti sono una cosa diversa dagli anacronismi.

Un giorno i dirigenti della società sportiva dove giocavo a basket mi proposero di occuparmi del minibasket. Accettai e la faccenda implicava vari passaggi burocratici, nonché la redazione di documenti con una macchina per scrivere. Non ne avevo mai vista una e imparai, un foglio dopo l’altro.

Quella conoscenza tornò molto utile anni dopo, durante il servizio militare. Ma il punto è un altro; durante le elementari e le medie, non avevo mai visto una macchina per scrivere.

Le mie figlie non hanno mai visto una macchina per scrivere in funzione (conserviamo con rispetto una Olivetti di design). Ma le tastiere sono oggetti di familiarità quotidiana, su cui hanno messo le mani (giocando) già prima della scuola materna.

Sarebbe così strano se la scuola dell’obbligo prevedesse, quando è il momento, ore di dattilografia? Non mi pare. Siamo tutti d’accordo sul fatto che per prima cosa ci sia da imparare a scrivere a mano. È un *fondamento*. Ma se tenessi le tastiere fuori dalla scuola, causerei un *anacronismo*. Sono cresciuto senza tastiere; ma i tempi sono molto cambiati e oggi si deve crescere tenendo conto del fatto che le tastiere ci sono.

Altro esempio: sempre le mie figlie si sono prestate come attrici per girare un video di pochi secondi, con gli auguri di Ferragosto per i nonni lontani. I loro genitori hanno in tasca un iPhone a testa; che in certe situazioni vengano girati video o scattate delle foto è una normalità assoluta. Girato il video, vogliono vedere come è venuto. Se parlando viene fuori il ricordo di una cosa fatta insieme, può capitare che ci sfogli l’archivio per guardare insieme le foto relative.

Sarebbe così strano se a scuola ci fosse un momento in cui si impara qualcosa di basilare sulla fotografia o sul video? Lo troverei normale. Con Lidia abbiamo giocato a creare un piccolo racconto animato girato a passo uno. iPhone sul treppiede davanti alla scena, spostiamo appena quello che vogliamo fare muovere, uno scatto. E ripetiamo. Abbiamo girato pochi secondi, in pochi minuti. Poi, giustamente, è terminato lo spazio di attenzione e siamo passati ad altro. Ma lei era in età prescolare. In quinta elementare, un progetto di video a passo uno è improponibile? E in terza media?

Di sicuro, prima, abbiamo scoperto le animazioni come successione veloce di fogli. Il fondamento. Ma se lasciamo che ai nostri dodicenni finisca in tasca un aggeggio capace di riprendere, senza insegnargli nulla su come usarlo bene, anacronismo.

Un’ultima cosa, di cui si riparlerà. Quando la primogenita vuole sapere che aspetto abbia la cetonia dorata, per prima cosa prende – da sola – dal suo scaffale il libro degli insetti. Lo sfoglia, trova la cetonia dorata, legge, guarda, fa domande.

Poi viene dal papà e chiede di cercare immagini della cetonia dorata. Insieme apriamo iPad e le chiediamo a Google Immagini. Guardiamo decine di foto, i video se ce ne sono, soddisfiamo ogni curiosità visiva soddisfabile sulla cetonia dorata.

Certo che si va per prima cosa a cercare sui libri. È un fondamento. Ma se poi non si familiarizza con un motore di ricerca, si cade nell’anacronismo. Lidia sa molto bene che si possono cercare foto su Internet. Insegnarle come si cerca, ovviamente al momento giusto, dovrebbe essere un dovere.

Ecco perché a un certo punto l’ipertesto, Html, gli editor di testo a scuola ci vogliono. Si può discutere, si *deve*, del quando. Non del se.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*