---
title: "Mente e carta"
date: 2016-02-13
comments: true
tags: [QR, sosta, Milano]
---

Raramente vado in auto a Milano, troppo traffico e poco spazio. Ricordo distintamente che molti anni fa si improvvisava un parcheggio sperando di evitare la multa.<!--more-->

Successivamente è stata introdotta l’opzione innovativa del [gratta e sosta](http://www.atm.it/it/ViaggiaConNoi/Auto/Pagine/Modalitadipagamento.aspx): _tessere cartacee_ ricoperte da uno strato di colore da grattare con una moneta per evidenziare l’ora del parcheggio. Molto comodi se acquistati in anticipo e tenuti nel cruscotto, inutili se arrivi di sera senza averci pensato e tutti i punti vendita sono chiusi.

Fortunatamente era possibile pagare anche inviando un Sms: una cosa semplicissima. A patto di avere acquistato una vetrofania da esporre, registrarsi, attivare un Pin, collegarlo alla targa dell’auto e qualche altra cosa ancora. Molto comodo per chi ha preparato tutto con meticolosità, inutile se arrivi di sera senza averci pensato e scopri la procedura nel momento in cui ti informi.

Ho scritto _era possibile_ perché da quest’anno il macchinoso sistema degli Sms è stata sostituito da convenzioni con _app_ veramente ingegnose. Ne ho provata una: richiede l’inserimento della targa dell’auto (ineccepibile), una registrazione anagrafica (incomprensibile) e l’accredito di una somma da scalare per il parcheggio e altri servizi, via carta di credito (Ok).

Ne ho approfittato; era sera, ero a Milano, ero parcheggiato. Ho inserito, registrato, accreditato per scoprire che avrei ricevuto via posta elettronica un [codice QR](https://it.wikipedia.org/wiki/Codice_QR) da esporre sul cruscotto. Niente di strano; tutti viaggiamo con una stampante in auto sempre pronta alla bisogna, vero?

È finita che ho improvvisato un parcheggio sperando di evitare la multa.

Mi chiedo quale autorità, quale carica, quale funzione continui a ideare procedure immancabilmente legate alla produzione e al dispendio di carta, quando persino il controllo della tassa di circolazione viene effettuato consultando un database online. Qualsiasi vigile ha uno _smartphone_ in tasca.

Mi chiedo chi sia questa mente. Mente-carta.
