---
title: "Posti limitati"
date: 2020-08-05
comments: true
tags: [Schiller, Kay, Wozniak, Norman, Fellow, Apple, Joswiak, Joz, Ive, Capps, Kawasaki]
---
Il compimento della carriera di Phil Schiller in Apple è molto diverso da quello, per dire, di Jonathan Ive, che a un certo punto si sentiva gli abiti troppo stretti e voleva più libertà di azione.

Schiller, invece di essere stato un manifesto ideologico di Apple, ne è stato un’anima, dietro le quinte così come sul palco. Infatti non si congeda con rapporti un po’ fumosi di collaborazione che sembrano più di forma che altro; non si congeda proprio. [Diventa un Apple Fellow](https://www.apple.com/newsroom/2020/08/phil-schiller-advances-to-apple-fellow/).

I Fellow continuano a interagire con Apple, rispondono a Tim Cook e hanno un ruolo ufficiale, ma godono di totale libertà di azione e hanno l’unico obbligo di condividere la loro visione a intervalli più o meno regolari. È come se fossero pagati per il privilegio di poterli vedere in Apple.

Nella storia ultraquarantennale di Apple i Fellow sono una dozzina e comprendono nomi del calibro di Alan Kay, Donald Norman, Guy Kawasaki, Steve Capps, Steve Wozniak. Sono un capitolo pesante nella storia dell’informatica, non in quella dell’azienda.

Per Schiller è un onore e per noi è una boccata di aria fresca. Una buona notizia è che la successione sembra in buone mani, quelle di Greg Joswiak.

Da che cosa lo si deduce? Ha un soprannome.