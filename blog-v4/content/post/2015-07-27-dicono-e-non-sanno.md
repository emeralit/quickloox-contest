---
title: "Dicono e non sanno"
date: 2015-07-29
comments: true
tags: [watch, Moltz, WindowsPhone, Android]
---
Ci raccontano che watch [sia un flop](http://www.dailymail.co.uk/sciencetech/article-3152514/Apple-Watch-FLOP-Sales-gadget-fallen-90-April-report-claims.html), non si sa bene rispetto a quali metriche e nonostante il fatto che Apple non fornisca dati di vendita ufficiali (e abbia [detto a ottobre](http://www.businessinsider.com/apple-wont-disclose-apple-watch-sales-2014-10) che non li avrebbe forniti).<!--more-->

Di queste conclusioni tratte dopo neanche un mese di vendita a tappeto [scrive bene John Moltz](http://verynicewebsite.net/2015/07/flopping-into-the-lead/):

>Apple guida il gruppo, ma il gruppo è pur sempre un gruppo di smartwatch. Nessuno sa realmente quanto diventerà grande questo mercato e quanto a lungo durerà. Lasciatemelo ripetere per enfasi: nessuno lo sa veramente. Per questo sentiamoci liberi di ridere in faccia a chiunque stimi le vendite di smartwatch per i prossimi cinque anni. Ricordate quando Windows Phone era [destinato a superare Android](http://verynicewebsite.net/2013/01/hows-that-going/)?

Il resto del *post*, brillantemente sintetico, elenca tutte le considerazioni da ponderare a monte di qualsiasi stima delle vendite di watch ed è ampiamente raccomandato.