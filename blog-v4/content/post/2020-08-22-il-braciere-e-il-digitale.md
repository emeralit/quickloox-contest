---
title: "Il braciere e il digitale"
date: 2020-08-22
comments: true
tags: [Colleferro]
---
Ho frequentato la quinta classe primaria in una frazioncina di [Colleferro](https://www.comune.colleferro.rm.it).

Era tutto differente da dove sono cresciuto. Si parlava italiano ma praticamente qualsiasi sostantivo e i verbi principali arrivavano dal dialetto.

Gli altri bambini sono stati gentilissimi e mi hanno spiegato tutto, una parola per volta, o non sarei sopravvissuto.

I piccoli bulli erano bulli veri, mica caricature da libro dell’infanzia. In compenso avevano anche un cuore d’oro, quando lo esercitavano.

C’erano regole diverse per giocare con le biglie. Molto diverse. Ci ho messo un attimo a perdere il mio piccolo tesoro e molto più tempo a impararle.

Alla fine c’era una cultura diversa. Nè migliore né peggiore, era un’altra, in cui mi sono integrato per un anno. Mi hanno chiesto tutti come era diverso da dove arrivavo e poi mi hanno insegnato a fare come loro. Persone meravigliose relativamente al concetto di persona per degli  undicenni.

A scuola insegnavano molte cose cui ero abituato e alcune che non conoscevo. Una mattina ci hanno parlato di come si utilizza il braciere.

Non sapevo che cosa fosse un braciere. In molte case intorno alla zona, invece, era usatissimo. In inverno può fare molto freddo e una stufa può non bastare, o non esserci. Ma attenzione a non lasciarlo acceso la notte, per non rischiare incendi o avvelenamenti.

Il braciere era una realtà concreta di quella cultura, con vantaggi e rischi, da usare con la giusta consapevolezza. A scuola lo sapevano e maestre capaci e tranquille insegnavano a usare i bracieri, oltre a leggere e scrivere.

A casa mia ci sono due Mac, tre iPad, quattro iPhone, un watch, una tv. Sicuramente sono fuori media. Ma non conosco case che abbiano meno di un computer da tasca. Sicuramente ci sono più computer che, tanto tempo fa, c’erano bracieri. I computer portano vantaggi, portano rischi, vanno usati con consapevolezza.

Ora io vorrei avere un dialogo con un docente convinto che non andasse insegnato a scuola l’uso del braciere. Perché ritengo che quello che fa parte integrante della cultura e della società debba trovare posto nella scuola.

I docenti contrari al digitale non mi interessano, invece. Non saprei di che cosa parlare. Spero almeno che abbiano giocato a biglie, mi rifarei volentieri su loro. So come fare.