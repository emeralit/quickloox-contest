---
title: "Musica, maestri"
date: 2017-10-08
comments: true
tags: [Apple, Microsoft, iPhone, iTunes]
---
Horace Dediu traccia l’elenco delle aziende che fornivano servizi musicali negli ultimi quindici anni e [sono sparite o diventate irrilevanti](http://www.asymco.com/2017/10/04/orthogonal-pivots/).

Tra le altre, *AOL MusicNow, Yahoo! Music Unlimited, Spiralfrog, MTV URGE, MSN Music, Musicmatch Jukebox, Wal-Mart Music Downloads, Ruckus, PassAlong, Rhapsody, iMesh and BearShare*.

Tutte usavano un formato anticopia (Drm) di Microsoft e i loro brani, una volta cessato il servizio, sono divenuti illeggibili o, nel migliore dei casi, destinati a perdersi al primo problema software per impossibilità di avere un ripristino, un backup o comunque un contatto con i server che certificano l’integrità del formato.

Una delle ragioni di scomparsa di questi servizi è che Microsoft li accoltellò alla schiena quando, dopo avere concesso le licenze per l’uso del formato, le ritirò per fare posto al suo Zune Marketplace. Un fallimento, dopo di che il servizio si convertì in Xbox Music. Un altro fallimento e il suo nome divenne Groove.

Entro fine anno [Groove cesserà di esistere](https://www.techspot.com/news/71228-microsoft-shutting-down-groove-music-favor-spotify.html).

Apple e Microsoft hanno iniziato a occuparsi di musica circa nello stesso periodo, ma oggi Apple è la numero uno nel settore e Microsoft si avvia verso lo zero.

Da tempo la musica distribuita da Apple è priva di Drm: una scelta difficile da concretizzare per la diffidenza delle multinazionali della musica, che Steve Jobs contribuì a superare con una famosa lettera aperta.

Dediu avvia una riflessione sofisticata sulle difficoltà di fornire un buon servizio verticale quando le tue fondamenta sono orizzontali: come essere compatibile con l’universo e nel contempo offrire una qualità elevata su qualunque ramo dell’attività.

Più semplicemente, direi che che la musica è una cartina di tornasole: se riesci a offrire un servizio gradito, hai certo capito qualcosa di come pensano i tuoi clienti. Se, d’altro canto, tutto quello che hai da offrire è un euro in meno, probabilmente non sei il più adatto a occuparti di musica.

Ho in cuffia le [Piano Improvisations](https://www.youtube.com/watch?v=rGznl7Dfurw) di Keith Emerson. A parte la recente scomparsa del loro esecutore, non possono essere contenuto *cheap*. E a nessuno verrà mai in mente di dire che i pianoforti sono tutti uguali.

<iframe width="560" height="315" src="https://www.youtube.com/embed/yg2KjxNtAiM" frameborder="0" allowfullscreen></iframe>