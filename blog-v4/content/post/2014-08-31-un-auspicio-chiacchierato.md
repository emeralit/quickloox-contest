---
title: "Un auspicio chiacchierato"
date: 2014-09-01
comments: true
tags: [Messenger, Microsoft, Skype, Word]
---
Tanti anni fa nascevano i sistemi di chat e siccome vivevamo un’epoca oscura e triste, gran parte delle persone chiedevano di usare Messenger di Microsoft. Sembrava che senza Messenger fosse impossibile fare chat e che per fare chat fosse indispensabile Messenger.<!--more-->

Adesso, sia pure per via dell’adozione di Skype, Microsoft procede a [chiudere per sempre](http://www.forbes.com/sites/amitchowdhry/2014/08/31/msn-messenger-is-completely-shutting-down-on-october-31st/) Messenger, che il 31 ottobre cesserà di esistere.

Prevedo scene di panico e disperazione tra tutti quelli che senza Messenger non c’era vita sociale online.

Mai contento, nel constatare che è possibile vivere abbastanza a lungo per vedere scomparire un Messenger, guardo avanti con ambizione e aspetto il momento radioso in cui toccherà a Word.