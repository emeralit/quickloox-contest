---
title: "La danza dell’update"
date: 2015-12-10
comments: true
tags: [OSX, WatchOS, iOS, Fëarandil, iPhone, iPad, beta, update]
---
Ho sfruttato la pubblicazione di OS X 11.2 per abbandonare la beta pubblica del sistema operativo – che non mi servirà più prima dell’estate – e la dritta di **Fëarandil** ha funzionato a perfezione: via la spunta alla casella giusta delle preferenze di App Store e poi normale aggiornamento di sistema.<!--more-->

Bisogna evitare che una beta troppo avanzata rispetto alla versione ufficiale possa avere modificato il sistema in modo tale da causare problemi. Mi è successo: Foto era passato a una versione successiva e la app della versione normale, non ancora aggiornata, non apriva più la libreria. Ne sono uscito rientrando nel programma di beta pubblica e aspettando il momento giusto, appena c’è l’aggiornamento ufficiale e manca una beta più avanzata di quella. Macchinoso da leggersi, banale rispetto al ripristino da Time Machine suggerito da Apple e che grazie al cielo non è necessario effettuare.

Con tutta questa soddisfazione nell’animo ho il piacere di scrivere da un luogo pubblico naturalmente privo di qualsivoglia connessione. Niente campo su iPhone, che segnala la presenza di iOS 9.2 e WatchOS 2.1; niente campo su iPad, anche lui pronto con iOS 9.2.

Ne approfitto per chiedere ad Apple una nuova casella di selezione da inserire nelle preferenze di aggiornamento software. Il testo sarebbe tipo questo:

>Scarica in automatico gli aggiornamenti di sistema e installali automaticamente durante la prima notte a disposizione. Non dirmi niente e fammi trovare tutto fatto.

Profondo rispetto per chi vuole verificare a mano, installare a mano, organizzare a mano. È una danza intricata e affascinante che so di lasciare, volentieri, in buone mani.