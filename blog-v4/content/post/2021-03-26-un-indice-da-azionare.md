---
title: "Un indice da azionare"
date: 2021-03-26T01:25:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [software, Hii, Tempi moderni, Chaplin] 
---
Penso che dovrebbe esistere un qualche indice di inefficienza intrinseca dei programmi, misurato grossolanamente sulla base del numero di azioni elementari necessarie per compiere una azione, in rapporto all’entità della variazione apportata.

Un esempio *for dummies*: sono in un editor di testo e mi trovo una distesa di testo tutta in maiuscolo. Voglio avere le iniziali maiuscole e il resto della parola in minuscolo.

Un comando tipo *Cambia maiuscole e minuscole lasciando la maiuscola solo all’inizio del periodo, purché la preceda un punto*, data per scontata la selezione preventiva del testo su cui intervenire, richiede un clic (diciamo che il *clic* è l’unità di misura fondamentale per l’interazione umana con l’informazione, *Hii* acronimo di *Human Information Interaction*, come il bit è l’unità di misura fondamentale per l’informazione).

Un’interfaccia diversa potrebbe prevedere un *clic* per richiamare una finestra di dialogo *Maiuscole/minuscole*, un altro clic per selezionare un *radio button* corrispondente all’opzione di trasformazione che desideriamo e un terzo clic per dare OK.

La prima forma di interazione è evidentemente più efficiente della seconda.

Nell’ambito della misurazione globale dell’efficienza, questa operazione dovrebbe avere un peso minimo, dato che il cambiamento alla struttura dati la altera in misura relativa. Il suo peso, sempre per fare esempi, dovrebbe essere minore di quello di una operazione di generazione automatica di un outline a partire da un testo. Dato che la variazione strutturale dei dati è ben maggiore.

Moltiplicando i punti di analisi e adottando un qualche standard di definizione delle operazioni, potrebbe diventare un misuratore di efficienza da applicare in mille situazioni, per ragionare attorno al costo, oppure non tanto sulle capacità effettive del programma ma sul tempo che richiede per raggiungere risultati. E diventerebbe interessante il confronto tra programmi.

Come lo chiamiamo? Mi viene in mente il film [Tempi moderni](https://it.wikipedia.org/wiki/Tempi_moderni) con il suo protagonista al lavoro in catena di montaggio, a forza di pensare a tutti questi clic che volano. *L’indice di Chaplin*.

La parola a ricercatori, matematici, scienziati, esperti di design, che potrebbero codificarlo davvero in termini più rigorosi di questi.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*