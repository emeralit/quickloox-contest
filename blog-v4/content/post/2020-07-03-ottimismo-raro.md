---
title: "Ottimismo raro"
date: 2020-07-03
comments: true
tags: [Mori, Morridk, macOS, Wwdc]
---
Se l’amico Riccardo chiude la sua carrellata su Wwdc con una [visione pacatamente ottimistica sul futuro di Mac](http://morrick.me/archives/8916), vuol dire che è stato un successone.

Nessuno meglio di lui sa cogliere luci e ombre di Apple. Ed è il posto da visitare per sentire la voce di chi, spesso vede il bicchiere mezzo vuoto. Io tendo a vederlo mezzo pieno e leggere i suoi articoli ha sempre l’effetto di un riequilibrio.