---
title: "Mini ottimismi"
date: 2017-04-05
comments: true
tags: [iPad, Above, Avalon, mini]
---
Le vendite di iPad sono in diminuzione. Il prodotto ha avuto un *boom* enorme alla nascita, con un ritmo di vendita francamente insostenibile. E poi si è dimostrato mostruosamente durevole: a casa mia il primo iPad è ancora in funzione, affidato alla figlia, e io lavoro senza problemi (tranne qualche lentezza che inizia a farsi sentire) su un modello di terza generazione, che sta per compiere *cinque anni*. Il ricambio degli iPad, alla faccia di chi blatera di obsolescenza programmata, sarà pachidermico nei ritmi. In un certo senso neppure è iniziato.<!--more-->

E poi arriva lo studio di *Above Avalon*, a rivelare che in effetti stanno calando sensibilmente [le vendite di iPad mini](https://www.aboveavalon.com/notes/2017/3/29/apple-is-pushing-ipad-like-never-before). Gli altri iPad, pur sotto il ritmo infernale degli inizi, sono molto più in salute e la tendenza recente è persino di rialzo percepibile delle vendite.

Perché mai dovrebbero calare le vendite di iPad mini? Che domande, da iPhone 6 Plus in avanti sono arrivati computer da tasca con lo schermo quasi di quelle dimensioni e tutti i vantaggi del telefono cellulare. Tra un iPhone 7 Plus e un iPad mini non c’è gara. Ed ecco che improvvisamente gli ottimismi reiterati di Tim Cook sul futuro di iPad sembrano più giustificati.
