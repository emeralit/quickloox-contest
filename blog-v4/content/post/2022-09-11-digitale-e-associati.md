---
title: "Digitale e associati"
date: 2022-09-11T02:00:02+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Copernicani]
---
Una immagine tendenziosa dell’ultima assemblea dei [Copernicani](https://copernicani.it).

![Assemblea Copernicani](/images/copernicani.jpg "Mac è un pretesto, eh. Però, quando sovrintende a una presentazione, vederlo è un segnale positivo.")

Passato il momento professionalità-al-lavoro, si è fatto il punto su quanto fatto nel 2022 finora, che cosa ci aspetta, lo stato dell’associazione e in che direzioni muoversi.

Come in tutte le associazioni, ci sono cose che funzionano molto bene, altre meno bene, perplessità, certezze, simpatie, antipatie, solo che diversamente da una assemblea di condominio escono risultati.

Il sottoscritto è stato per esempio chiamato in causa per [il progetto Contra Chrome](https://macintelligence.org/posts/2022-07-14-come-cambia-il-fumetto/), fumetto dedicato ai rischi per la *privacy* posti da Chrome, ora dotato di una bella localizzazione in italiano che ha avuto ottima diffusione e – *spoiler* – sarebbe stato mostrato in preserata su una rete televisiva nazionale se solo l’autrice americana fosse una persona reale e non uno pseudonimo blindato.

Ma sono cose piccole. I Copernicani comprendono mille anime e ce ne sono anche alcune che parlano con la politica e l’industria, a livello nazionale e internazionale. In compenso, ci siamo trovati in uno *hacker space* di recente fondazione, in via Golgi 60 a Milano, [in un ambiente del tutto frugale](https://twitter.com/copernicani/status/1568603029871394820). Niente cravatte né sussiego, solo accoglienza e grande ascolto per chiunque.

<blockquote class="twitter-tweet"><p lang="it" dir="ltr">Alcuni Copernicani al &quot;battesimo&quot; di Hacker Open Space il nuovo spazio di creatività in progress a Milano <a href="https://t.co/C6NHrnMtSm">pic.twitter.com/C6NHrnMtSm</a></p>&mdash; Copernicani (@copernicani) <a href="https://twitter.com/copernicani/status/1568603029871394820?ref_src=twsrc%5Etfw">September 10, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Non conosco altra aggregazione con una combinazione superiore di competenze e informalità, con un sistema di votazione delle cariche che elimina il problema delle correnti o dei gruppi di pressione, dove tanti progetti cadono nel vuoto, altri procedono a passo di lumaca, oppure vanno avanti e hanno impatto reale sul digitale nell’Italia e nelle istituzioni. Il [fondatore dei Copernicani](https://twitter.com/quinta/status/1568528143765446656) è il papà di Spid.

<blockquote class="twitter-tweet"><p lang="it" dir="ltr">bellissimi progetti nel primo semestre 2022 dei <a href="https://twitter.com/copernicani?ref_src=twsrc%5Etfw">@copernicani</a> <br><br>bella assemblea a Milano, la prima in presenza dopo anni!</p>&mdash; Stefano Quintarelli (@quinta) <a href="https://twitter.com/quinta/status/1568528143765446656?ref_src=twsrc%5Etfw">September 10, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Nessuno dei lettori di questo post riceverà mai un un invito diretto ad associarsi, perché lo scopo non è questo.

Invito però chiunque sia arrivato fin qui a visitare il [sito dei Copernicani](https://copernicani.it) e farsi un’idea. Ci sono posti ben peggiori cui dedichiamo attenzione.