---
title: "Quel vecchio aggettivo"
date: 2015-04-19
comments: true
tags: [Fcpx, BBEdit, Keynote, Pages, Safari, iMovie, Numbers, Html, Focus, Smith]
---
Ogni tanto sento lamentarsi di [Final Cut Pro X](https://www.apple.com/final-cut-pro/) per quello che fa o per quello che non fa, o per tutt’e due le aree di lamento. E nella spiegazione ricorre sempre la parola *professionale*.<!--more-->

Credo di avere già chiesto una definizione verificabile di questo termine, senza avere ricevuto risposte plausibili. Ho la sensazione che serva a tanti per nobilitare il software che usano *loro* e finisca lì.

Sarà che ultimamente sto lavorando con [Pages](https://www.apple.com/it/mac/pages/) e [Numbers](https://www.apple.com/it/mac/numbers/), programmi che altrimenti uso molto di rado. Ho tenuto decine di presentazioni con [Keynote](https://www.apple.com/it/mac/keynote/), spesso pagato per tenerle. Ma ho tenuto anche un paio di presentazioni in Html, scritte con [BBEdit](http://www.barebones.com/products/bbedit/) e naturalmente rese a video con [Safari](https://www.apple.com/it/safari/). Safari è un *browser* professionale? Che *browser* professionali esistono oltre che (o invece di) Safari?

Divagazioni a parte, non conosco Final Cut Pro X; ogni tanto faccio qualcosina con [iMovie](https://www.apple.com/it/mac/imovie/) e finisce lì (in due occasioni sono stato pagato per farlo: iMovie sarà professionale?).

Vedo però che Apple pubblica una storia di lunghezza monumentale su un film interpretato da Will Smith, [montato interamente su Final Cut Pro X](https://www.apple.com/final-cut-pro/in-action/focus/) e finanziato da Warner Bros con un *budget* di cento milioni di dollari.

Stando sul sito Apple, sarà certamente un documento che tira acqua al mulino di Cupertino, evidenzia i lati belli, nasconde i difetti eccetera eccetera. Interessa poco: sembra un fatto concreto che il film esista e che sia stato lavorato da professionisti con Final Cut Pro X. Non trovo in Internet smentite da parte dei professionisti citati nella pagina. Sono portato a pensare che la narrazione sarà sicuramente orientata a favore di Final Cut Pro X invece che contro. Tuttavia è un fatto che questa narrazione *può avvenire*: il lavoro su [Focus - Niente è come sembra](http://it.wikipedia.org/wiki/Focus_-_Niente_è_come_sembra) è stato realmente compiuto.

Siccome voglio capire e imparare, spero che qualche critico di Final Cut Pro X vorrà smontarmi l’articolo e mostrarmi perché il programma non è professionale o non lo è abbastanza. Se volesse anche smontare la mia idea che *professionale* sia un aggettivo buono per il Ventesimo secolo – e ci siamo già mangiati più della settima parte del Ventunesimo intanto – ne sarò molto grato.