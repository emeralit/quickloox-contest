---
title: "Rinnovo (istruzioni) locali"
date: 2017-10-13
comments: true
tags: [Swift, Playgrounds]
---
Come spesso succede, il mio [libriccino su Swift](http://www.apogeonline.com/libri/9788850333387/scheda) era stato scritto per amore della materia e anche per interesse personale. Non di tipo finanziario: le vendite sono andate bene e ho coperto più che abbondantemente le bollette della luce per un anno, ma è chiaro che se la molla fosse quella, si scriverebbe niente.

Interesse personale di tipo formativo: avevo scritto nell’introduzione – e lo ripeto – che oggi, semplicemente, il tema della programmazione non è ignorabile. Neanche se lavori alle Poste o in un ferramenta. Se sei un ingegnere elettronico probabilmente ti interesserà di più e se sei un *sous chef* ti interesserà meno; zero, tuttavia, è impossibile. Come con l’inglese. Fatti tutti i distinguo e considerate tutte le situazioni, è impensabile per una persona attiva scantonare da un approccio almeno minimo, *the pen is on the table*.

[Swift](https://developer.apple.com/swift/) era appena arrivato e scrivere un libro di avvicinamento alla materia mi dava l’occasione per avvicinarmici davvero, senza alibi e anzi con il fiato dell’editore sul collo, e così scoprire il nuovo linguaggio per programmare le app.

In trenta mesi Swift è cresciuto in maniera smisurata. Si sono avvicendate tre versioni principali – oggi siamo alla 4, scrivevo sulla 1.2 – e il linguaggio, divenuto open source, si sta sviluppando anche fuori da Apple. In questo blog il tema è stato [toccato più volte](http://www.macintelligence.org/blog/categories/swift/).

Non ci sarà una nuova edizione su carta, perché se ne è perso il senso. Con [quello che è Swift oggi](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/index.html#//apple_ref/doc/uid/TP40014097-CH3-ID0) ci vorrebbe un tomo di quattrocento pagine, scritte da un programmatore professionista.

Tuttavia è ancora in circolazione [l’edizione online](https://itunes.apple.com/it/book/swift/id1003329339?mt=11) e ho cominciato un giro di correzioni e aggiornamenti. Anche se il sistema per approcciarsi a Swift nel modo più semplice resta [Swift Playgrounds](https://www.apple.com/swift/playgrounds/), desidero che il mio libriccino resti vivo come modestissima alternativa per cominciare semplicemente e raggiungere quella confidenza minima con la programmazione di cui parlavo.

Per esempio, ho appena effettuato la sostituzione di tutte le istruzioni `println` con `print`, in accordo al cambio della sintassi deciso con Swift 2.

L’idea è di arrivare tipo a fine mese ad avere una versione ragionevolmente priva di errori e aggiornata rispetto agli sviluppi di Swift. La cosa inciderà in modo tendente a zero sui proventi derivanti dal libro, ma appunto, le ragioni per scrivere sono altre.

Tutto ciò per dire che accetto segnalazioni di errori e inadeguatezze e che farò del mio meglio per arrivare a un ebook all’altezza di una persona che, oggi, priva di nozioni importanti di programmazione, voglia provare ad avvicinarsi a Swift.

Grazie anticipate.