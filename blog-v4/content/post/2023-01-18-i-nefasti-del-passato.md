---
title: "I nefasti del passato"
date: 2023-01-18T01:34:55+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Internet]
tags: [Apple, MacBook Pro, Mac mini]
---
Dopo avere quasi completato la transizione da Intel a Apple Silicon sullo hardware, Apple ha perfezionato anche quella degli utenti e commentatori.

Nel giro di poche ore ho visto cose che devono essere uscite da un tempio maledetto sepolto sotto il Grande Lago Salato dello Utah.

>Vuol dire che Apple non crede più nella filosofia All-in-one.

Un classico degli anni novanta rivisitato al meglio. Escono i primi e ti lamenti perché mancano i secondi, vai al mare e sottolinei la mancanza di montagna.

>Come è possibile che la mia macchina maxata a un terabyte comprata tre mesi fa ora valga €270?

L’esercito degli zombi che comprano Mac per rivenderli piuttosto che usarli, oppure aspettandosi che il loro valore aumenti nel tempo in virtù del novantunesimo principio della termodinamica, si è ahimé risvegliato. O forse sono quelli che siccome hanno comprato una macchina, gli altri non possono avere dell’altro per un tempo imprecisato.

Medaglia al valore per l’aspirante influencer che al mattino twitta una cosa tipo

>Quando sto al computer a controllare se escono gli annunci di Apple.

(Non è testuale, ma il concetto è quello).

Al che faccio una battuta ironica sulla nozione del fuso orario e l’aspirante non capisce che è ironica, non capisce che è una battuta e la butta, isolato, sull’insulto.

In effetti gli [annunci di MacBook Pro e Mac mini](https://www.youtube.com/watch?v=6Ij9PiehENA) sono molto interessanti. Mac mini con M2 Pro è goloso per tutti quanti non hanno bisogno di un Mac Studio ma di un muletto instancabile e versatile. Per la prima volta dal 1976 trovo interessante una macchina modello base: vero, otto gigabyte di Ram e duecentocinquantasei di disco sono pochi, ma settecentoventinove euro… Mac mini Intel ne voleva seicentonovantanove e va dieci volte più lento. L’unico Mac che batte perfino l’inflazione, roba da comprarlo comunque perché tanto, a quel prezzo lì, gli si trova sicuramente qualunque cosa da fare e lui si ripaga.

Annunci molto buoni, al punto da avere risvegliato tutti i morti viventi, compresi quelli – ne ho trovato uno, uno vero – con il coraggio di dire che Apple poteva spingersi più avanti, perché nei test che ha fatto lui (lui!) la macchina che ha provato si è scaldata pochissimo.

Recuperato dal congelatore dove erano custoditi, senza abbastanza cura evidentemente, i reduci dell’overclocking. Divertente da provare sui Pc usa e getta, fuori luogo e senza senso su Apple Silicon che non scalda per efficienza, non per scarso sfruttamento.

Ma vallo a spiegare a gente con il cervello annebbiato dall’animazione sospesa, che siamo nel 2023.

<iframe width="560" height="315" src="https://www.youtube.com/embed/6Ij9PiehENA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>