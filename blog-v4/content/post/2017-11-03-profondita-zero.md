---
title: "Profondità zero"
date: 2017-11-03
comments: true
tags: [iPhone, X, Panzarino, Face, ID]
---
Ho la sensazione che iPhone X lascerà il segno; si cerca in qualsiasi maniera di screditarlo prima che inizino le consegne in (relativa) massa e prima che arrivi veramente in tutte le mani possibili.

Il problema è che gli appigli appaiono sempre più pretestuosi. A confronto dell’oggi, le [illazioni sul ridimensionamento dell’efficacia di Face ID](https://macintelligence.org/posts/2017-10-27-le-volpi-del-marketing/) sembrano genialate.

Per esempio, *Tom’s Guide* sostiene che [sbloccare iPhone con Face ID è più lento che sbloccarlo con Touch ID](https://www.tomsguide.com/us/iphone-x-face-id-speed-up,news-26060.html).

Si parla di un secondo e mezzo invece che quasi un secondo, tanto per mettere in chiaro la rilevanza degli argomenti che vengono portati.

Incidentalmente, il recensore è convinto di dover premere un pulsante su iPhone X per accendere lo schermo e poterlo sbloccare. Il pulsante non serve.

Ma facciamo finta che sia come dicono e iPhone X sia più lento nello sbloccarsi. Certamente, per quanto lavoriamo su tempi minimi, essere più veloci è meglio che essere più lenti.

Vale anche per le notifiche. E, [riferisce Matthew Panzarino](https://twitter.com/panzer/status/925765177588379648), *se vuoi aprire una notifica, devi toccarla e poi usare Touch ID per aprirla. Con Face ID, quando tocchi è già aperta*.

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">If you want to open a notification, for instance, you have to tap it then Touch ID to open it. With Face ID, it&#39;s already open when you tap.</p>&mdash; Matthew Panzarino (@panzer) <a href="https://twitter.com/panzer/status/925765177588379648?ref_src=twsrc%5Etfw">November 1, 2017</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Per *Tom’s Guide*, tuttavia, il tempo eventualmente risparmiato con le notifiche – fondamentali nel funzionamento quotidiano di un computer da tasca – non è meritevole di cronometraggio.

Forse addentrarsi nel funzionamento del marchingegno fino a queste profondità estreme è considerato un approfondimento eccessivo.