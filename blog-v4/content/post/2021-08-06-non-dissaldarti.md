---
title: "Non dissaldarti"
date: 2021-08-06T01:51:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Pantone, MicroUSB, Lightning, iPhone, USB-C, iPad] 
---
Il cavo [Pantone 3 in 1](https://www.yeppon.it/p-pantone-pt-usb003y1-cavo-1053794/) è proprio una bella idea.

Da una parte è USB 2/3. Dall’altra è nativamente MicroUSB e così carica un sacco di aggeggi come dischi, luci da lettura, ventilatori da scrivania, accessori a non finire.

Sul connettore MicroUSB si può attaccare un convertitore che lo trasforma in Lightning. Così si caricano innumerevoli iPhone, per esempio. Il convertitore è attaccato al cavo principale tramite un filo di plastica, così è sempre a disposizione. Quando esco e devo portarmi un cavo da ricarica, questa è davvero una buona soluzione.

Anzi, ottima soluzione. Un altro filo di plastica tiene infatti attaccato al cavo principale un convertitore USB-C. Perfetto per il mio iPad Pro, per dire.

A inizio giugno, prima di partire per il mare, ho reperito un cavo Pantone 3 in 1 grazie a una raccolta punti di un ipermercato. Neanche sapevo che i cavi Pantone esistessero. L’ho usato tutti i giorni sulla scrivania, perfetto per caricare a fasi alterne iPhone e iPad Pro.

Ieri era il 5 agosto, poco meno di due mesi di utilizzo. Il filo del convertitore Lightning si è rotto da solo. Nessun trauma, nessun maltrattamento, nessun utilizzo borderline. Si è rotto e basta.

Sipario.

[Musica](https://www.youtube.com/watch?v=kQxqbwTScbY), maestro.

<iframe width="560" height="315" src="https://www.youtube.com/embed/kQxqbwTScbY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*