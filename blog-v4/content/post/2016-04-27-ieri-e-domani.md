---
title: "Ieri e domani"
date: 2016-04-27
comments: true
tags: [iPhone, watch, Lumia, Microsoft]
---
Apple avrebbe venduto nel primo anno di watch [un numero doppio di unità](http://www.wsj.com/articles/apple-watch-with-sizable-sales-cant-shake-its-critics-1461524901) rispetto al primo anno di iPhone, dodici milioni contro sei.<!--more-->

Microsoft offre [due Lumia al prezzo di uno](http://www.cio.com/article/3061393/windows-phone-os/microsoft-launches-two-for-one-lumia-phone-deal-to-slash-excess-inventory.html) per disfarsi dell’invenduto.

Poi si guardano i dati di quota di mercato come se fossero cose vere, dimenticando che c’è chi vende e chi regala. Chi lavora per il domani, chi è rimasto a ieri.