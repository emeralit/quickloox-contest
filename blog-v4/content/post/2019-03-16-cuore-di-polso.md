---
title: "Cuore di polso"
date: 2019-03-16
comments: true
tags: [Apple, Stanford, Watch]
---
È terminato lo studio congiunto di [Apple](https://www.apple.com/newsroom/2019/03/stanford-medicine-announces-results-of-unprecedented-apple-heart-study/) e [università di Stanford](https://www.businesswire.com/news/home/20190316005006/en/Apple-Heart-Study-Demonstrates-Ability-Wearable-Technology/) sull’individuazione con Apole Watch di irregolarità cardiache e specialmente fibrillazione atriale.

I risultati sono tecnicamente buoni ma è la cosa che interessa di meno. L’elemento che dovrebbe destare più sensazione è invece l’ampiezza della partecipazione, oltre quattrocentomila partecipanti. Non si era mai potuto allestire uno studio ugualmente controllato e di dimensioni analoghe.

Dei quattrocentomila, circa duemila hanno ricevuto notifiche di irregolarità nel battito e il consiglio di farsi visitare più accuratamente. In molte di queste situazioni, una visita successiva ha individuato fibrillazione atriale, una condizione pericolosa da monitorare.

C’era anche chi temeva una alluvione di falsi negativi e un assalto inutile alle strutture mediche, con spreco di tempo e risorse. Non è avvenuto.

Gli Apple Watch in gioco erano fino alla versione 3, cioè senza funzione Ecg che, almeno in teoria, dovrebbe restituire più accuratezza dei Led verdi.

Per quanto Apple insista a ricordare che non si tratta di apparecchiature mediche e che non bisogna crearsi diagnosi a casa sulla base dei dati di Apple Watch, sappiamo che lo deve fare per non ingenerare false aspettative e per evitare grane legali. Tra dieci anni questa sarà una *disruption* di prima grandezza, con tanti business di comodo che andranno in frantumi e tante posizioni di retroguardia improvvisamente scoperte di fronte ad apparecchi che possono prendersi cura di una persona ventiquattr’ore al giorno, condizione oggi sostenibile da parte della sanità solo per brevi tratti e per poche persone.

Tanti si ritroveranno a vivere più a lungo, che oggi non possono. Alla fine, l’unica cosa che conti, che smuova il cuore in quanto sede di emozioni, è questa.
