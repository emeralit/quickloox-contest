---
title: "La scuola dell'obbligo"
date: 2013-07-06
comments: true
tags: [Privacy, Android]
---
Ipotizziamo per un momento che io abbia una qualche fama di *rapper*. Tre giorni prima che esca il mio nuovo album, lo concedo in anteprima a chi volesse scaricarne i brani tramite una *app*.<!--more-->

Al momento di installarsi, la *app* chiede le seguenti autorizzazioni:

- modificare e cancellare contenuti sul telefono
- disabilitare a volontà lo *standby* del telefono e sapere che altre *app* sono in funzione
- conoscere la posizione geografica
- accesso pieno alla connessione Internet
- sapere con chi stiamo parlando al telefono
- partire automaticamente all’accensione del telefono
- tentare di accedere a spazi di memoria protetti contro la scrittura
- controllare la funzione di vibrazione del telefono
- sapere che account sono attivi sul telefono

E poi è naturalmente obbligatorio registrarsi con un *account* Facebook o Twitter.

Questo ecosistema Apple sta veramente esagerando con le sue smanie di controllo.

No, un momento: è [la *app* del *rapper* Jay Z](http://arstechnica.com/gadgets/2013/07/samsung-and-jay-z-give-the-internet-a-masters-class-in-how-not-to-make-an-app/), per apparecchi Samsung.

Dove tutto costa meno e si impara a comportarsi come si deve. Come zerbini ansiosi di ricevere sedici canzoni campionate a qualità appena decente.