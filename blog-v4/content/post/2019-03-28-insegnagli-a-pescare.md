---
title: "Insegnagli a pescare"
date: 2019-03-28
comments: true
tags: [Aprile, Microsoft, Apple, Capossela, Jobs, iPhone, Windows]
---
Apple [non ha mai fatto un pesce d’aprile pubblico](https://daringfireball.net/linked/2019/03/27/microsoft-april-fools). La parola chiave è *mai*; indica una posizione discutibile, ma chiara, coerente e slegata dalle condizioni a contorno. Il senso dell’umorismo non è mai mancato, solo è stato espresso in altri modi, dagli *easter egg* a certi filmati di apertura di *keynote* recenti. Più indietro nel tempo, basta ricordare Steve Jobs che annunciava iPhone e mostrava l’immagine di un ibrido caricaturale, metà telefono metà ghiera rotante stile iPod primi tempi.

Diversamente discutibile è la posizione di Microsoft, che quest’anno [vieta esplicitamente i pesci d’aprile visibili all’esterno](https://www.theverge.com/2019/3/27/18283674/microsoft-april-fools-day-ban-pranks-internal-memo). Fate quello che volete, ma che la gente non se ne accorga. Motivi?

>I dati ci dicono che queste bravate hanno impatto positivo limitato e possono in verità portare a giri di notizie sgradevoli.

*Se lo abbiamo fatto, è stato per convenienza*.

>Considerata l’aria che tira nel settore della tecnologia…

*Basso profilo e non facciamoci riconoscere*.

>Abbiamo più da perdere che da guadagnare nel tentare di essere divertenti in questo preciso giorno.

*Lo faremmo se ci convenisse*.

Fare festa ci distingue dalle specie meno evolute della nostra. Un animale fa la stessa cosa tutti i giorni; solo per l’uomo può esserci un giorno speciale, in cui lo schema quotidiano cambia. Il primo di aprile è una festa, con una tradizione che comporta un cambio della routine. Quello che leggi potrebbe essere una burla, quello che clicchi potrebbe funzionare in modo imprevisto, potresti sentirti preso in giro e accorgertene solo alla fine.

Certo, ci sono pesci d’aprile stupidi o irritanti. Certo, non è facile essere divertenti, originali ed efficaci senza offendere alcuno (a me viene un pesce d’aprile ogni venticinque). Certo, sui social il concetto di festa si è stravolto insieme a mille altre cose: scrivi π perché è il 14 marzo, fotografi l’asciugamano perché è il *towel day*, mostri la barba perché è il *no shave november* e via così, il calendario del nulla. La festa ha, dovrebbe avere, una dimensione profonda e incidere, magari superficialmente, sul quotidiano; invece la si è lobotomizzata, lasciando al suo posto una ricorrenza vuota per tornare alla routine immediatamente dopo il *like*. Più vicini di prima al regno animale.

Non mi piace Apple che ignora la festa. Non mi piace Microsoft che la tratta con opportunismo, non ci conviene quindi voliamo bassi.

Viene anche da dire che il senso dell’umorismo Microsoft è peculiare: sono loro che [celebrarono il funerale di iPhone](https://www.businessinsider.com/microsoft-iphone-funeral-2010-9?IR=T) in occasione del lancio dell’indimenticabile (e dimenticato) Windows Phone 7.

Certo, se la cultura aziendale è quella che è, il rischio di rimetterci reputazione con un pessimo pesce d’aprile è maggiore.

Qualche corso di *sense of humor*? Visioni collettive della produzione dei Monty Python (senza riferimenti alla programmazione)? Se hanno fame di buone trovate non togliergli la canna da pesca; insegnagli a pescare.