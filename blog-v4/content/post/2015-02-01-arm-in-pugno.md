---
title: "Arm in pugno"
date: 2015-02-01
comments: true
tags: [Intel, iPad, iPhone, iOS, Mac, broadband, Apple, Dilger]
---
Da un po’ di tempo si parla della possibilità che Apple decida di fare a meno di Intel per i processori dei Mac e procedere a sviluppare ulteriormente a questo scopo i processori Arm che già realizza per iPhone, iPad e il resto di iOS.<!--more-->

Mi è capitato di commentare distrattamente che magari ad Apple interessa più sostituire il contorno dei *chip* che stanno attorno al processore, più che il processore stesso. Così ho mancato clamorosamente il vero succo del discorso.

Perché Apple dovrebbe impegnarsi per sostituire il processore su una ventina di milioni di Mac ogni anno, quando [può agire con maggiore profitto su duecento o trecento milioni di apparecchi iOS?](http://appleinsider.com/articles/15/01/20/after-eating-intel-apple-could-next-devour-qualcomms-baseband-processor-business).

Questo articolo di Daniel Eran Dilger spiega come Apple potrebbe avere in mente di realizzare in proprio il *chip* che provvede alle funzioni *broadband* (comunicazioni cellulari) e così facendo risparmiare una decina di dollari in costi su ciascun apparecchio interessato. Per duecento milioni di apparecchi, sono due miliardi di dollari l’anno. Più che abbastanza per decidere di scavalcare qualsiasi fornitore consolidato.