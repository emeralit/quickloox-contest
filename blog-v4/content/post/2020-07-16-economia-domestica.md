---
title: "Economia domestica"
date: 2020-07-16
comments: true
tags: [Apple, Gruber, tasse, Farber, Estager, Corriere]
---
Il Tribunale dell’Unione Europea ha annullato la decisione della Commissione Europea che chiedeva ad Apple di pagare tredici miliardi di euro al fisco irlandese. Denaro che secondo l’Irlanda non è dovuto. Per il Corriere della Sera, sono [tasse con lo sconto](https://www.corriere.it/economia/aziende/20_luglio_15/tribunale-ue-annulla-multa-miliardaria-ad-apple-dell-antitrust-ue-ddd45948-c67b-11ea-a52c-6b2a448f1d2c.shtml) e vabbè, anche sui giornalisti oggi si risparmia di brutto.

John Gruber ha riassunto un articolo del più serio (nella circostanza) *Wall Street Journal* e [ha commentato](https://daringfireball.net/linked/2020/07/15/apple-ireland-taxes) in modo sintetico e definitivo.

>È perfettamente ragionevole e forse corretto sostenere che Apple, come oggi tutte le altre multinazionali titaniche, dovrebbe pagare più tasse. Solo che Apple non è una di quelle aziende capaci in qualche modo di accumulare una fortuna e pagare poco o niente al fisco; nella realtà è il più grande contribuente mondiale e penso veramente che paghino il dovuto, in tutto il mondo. Chi pensa che dovrebbe pagare di più deve occuparsi della legge, non del suo rispetto da parte di Apple o delle loro procedure contabili.

Il portavoce di Apple chiarifica ulteriormente, peraltro aderente alla posizione sostenuta da sempre:

>Questo caso non riguarda la quantità di tasse che paghiamo, ma dove ci viene richiesto di pagarle.

Più interessante la dichiarazione di [Markus Ferber](https://www.europarl.europa.eu/meps/en/1917/MARKUS_FERBER/home), europarlamentare europeo di provenienza cristiano-democratica (non estremista, non oltranzista, non esaltato), che nel 2016 ha appoggiato l’iniziativa della Commissione.

>Qualche volta il Commissario europeo alla concorrenza andrebbe consigliato di limitare la propensione a fornire buoni titoli per i giornali e pensare piuttosto a preparare meglio i propri dossier, così che possano reggere l’esame di un tribunale. L’annullamento di decisioni prese a questo livello è un vero disservizio alla causa della giustizia fiscale.

Da un europarlamentare tedesco, e va sottolineato tedesco. Gruber lo chiama, traduco un po’ liberamente, *uno schiaffo che brucia*.

Possiamo sapere quanto è costato il giro di carte per mettere in piedi il circo di un procedimento che, varato dal Commissario europeo, viene cassato dal Tribunale europeo, metaforicamente nella stanza accanto? Perché va bene cercare soldi, ma almeno prima stai un po’ attento a spendere saggiamente quello che hai. Almeno, conosco svariate famiglie oneste che funzionano in questo modo.