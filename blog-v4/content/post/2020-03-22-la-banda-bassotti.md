---
title: "La banda, bassotti"
date: 2020-03-22
comments: true
tags: [Netflix, Apple, TV+, Amazon]
---
Dicono che quando la vita ricomincerà a scorrere senza l’occhio quotidiano alle curve epidemiologiche, molte cose comunque non saranno più come prima.

Immagino si riferiscano in primo luogo ai politici che si sono distinti nelle primissime fasi della pandemia per i loro comportamenti e pronunciamenti scriteriati.

Penso però ad altro e noto che [per un mese Netflix ridurrà del 25 percento](https://www.theverge.com/2020/3/19/21187078/netflix-europe-streaming-european-union-bit-rate-broadband-coronavirus) il proprio traffico dati sulla rete europea.

Una mossa similare [viene adottata da Amazon](https://techcrunch.com/2020/03/20/amazon-follows-netflixs-lead-reducing-streaming-quality-in-europe/).

Anche [YouTube taglia la qualità dei propri video](https://www.reuters.com/article/us-health-coronavirus-youtube-exclusive-idUSKBN2170OP) in Europa.

[Così fa Disney Plus](https://variety.com/2020/digital/news/disney-plus-reduce-bandwidth-europe-france-delayed-1203541425/) in previsione del lancio nel Vecchio Continente.

[Succede anche con Apple TV+](https://9to5mac.com/2020/03/20/apple-tv-plus-streaming-quality-coronavirus/).

Non è un caso. [Lo ha chiesto l’Europa](https://twitter.com/ThierryBreton/status/1240353171748331523).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Important phone conversation with <a href="https://twitter.com/reedhastings?ref_src=twsrc%5Etfw">@ReedHastings</a>, CEO of <a href="https://twitter.com/netflix?ref_src=twsrc%5Etfw">@Netflix</a><br><br>To beat <a href="https://twitter.com/hashtag/COVID19?src=hash&amp;ref_src=twsrc%5Etfw">#COVID19</a>, we <a href="https://twitter.com/hashtag/StayAtHome?src=hash&amp;ref_src=twsrc%5Etfw">#StayAtHome</a><br><br>Teleworking &amp; streaming help a lot but infrastructures might be in strain. <br><br>To secure Internet access for all, let’s <a href="https://twitter.com/hashtag/SwitchToStandard?src=hash&amp;ref_src=twsrc%5Etfw">#SwitchToStandard</a> definition when HD is not necessary.</p>&mdash; Thierry Breton (@ThierryBreton) <a href="https://twitter.com/ThierryBreton/status/1240353171748331523?ref_src=twsrc%5Etfw">March 18, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Incapaci da tutto il continente hanno dato vita a un’Europa dove non c’è abbastanza spazio per tutti i dati in una situazione di emergenza.

Penso se succedesse con i tubi dell’acqua, le condutture del gas, i cavi dell’energia elettrica.

Spero di non sentire nel 2020 l’obiezione che la rete non è un bisogno primario. Lo manderei a spiegarlo alla gente che dalla sera alla mattina si è ritrovata a fare *smart working*. Che è telelavoro bieco, lo *smart working* è un’altra cosa.

Non parlo di me; lavoro autonomamente da tempo e so come ci si organizza, singolo e azienda, per andare in videoconferenza quando serve e nel modo più produttivo.

Parlo di chi è convinto di dover stare in videoconferenza otto ore e adesso va via l’audio, poi cade la linea, poi il video stenta, le orecchie dolgono, le slide finalmente vengono guardate da utenti finali invece che da autori; se in condivisione schermo le slide non si leggono, è segno che sono malfatte.

Ci sono intere economie a rischio che hanno bisogno disperato di poter funzionare online in questa situazione e la soluzione è tarpare le ali a Netflix, così che la gente chiusa in casa non abbia neanche uno streaming decente.

Quando la vita ricomincerà a scorrere ci vorrebbe un bel processo collettivo ai responsabili dell’infrastruttura di rete europea, da condannare a scavare loro l’ultimo miglio, con le unghie. Gente di statura politica infima e capacità nulla.