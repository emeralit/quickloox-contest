---
title: "Scuola, ultima frontiera"
date: 2015-01-31
comments: true
tags: [Swift, VisualBasic, Stanford, OSX, iOS, iOS8, iTunesU]
---
Bisogna prendere spunto dall’attualità perché costituisce un elemento fondamentale dell’argomentazione: l’università di Stanford offre su iTunes U un [corso per lo sviluppo di app per iOS 8 mediante il linguaggio di programmazione Swift](https://itunes.apple.com/us/course/developing-ios-8-apps-swift/id961180099).<!--more-->

[Swift](https://developer.apple.com/swift/), ricordo, esiste da un semestre e poco più. È l’ultima frontiera attuale dello sviluppo software per OS X e iOS.

Video e *slide* di livello universitario e soprattutto di livello assoluto. Gratis. Non pretendo certo di affermare che un corso in video possa sostituire l’insegnamento in aula da parte di un docente esperto. Sono altresì sicuro che se quel docente è veramente esperto, fa i salti mortali per usare *anche* quei video e quelle *slide* – di livello universitario, di livello assoluto, gratis – nell’ambito del suo materiale didattico.

La scuola che non entra in questo ordine di idee, che non va a prendere il meglio dove è disponibile in un mondo connesso e globalizzato, è condannata a una crescente irrilevanza. Quello che è peggio è che condanna all’irrilevanza i propri studenti, o a perdere del gran tempo.

L’esempio in sé è estremo; non è che tutti adesso debbano imparare a scuola Swift. È tuttavia necessario che imparino almeno i rudimenti della programmazione in generale. E che lo facciano con strumenti aggiornati, perché il mondo della programmazione corre veloce, come si scriveva [neanche due settimane fa](https://macintelligence.org/posts/2015-01-19-vuol-dire-svelto/).

E adesso rivolgiamo tutti assieme un pensiero affettuoso a quella docente che [vuole insegnare Visual Basic](https://macintelligence.org/posts/2014-10-29-qualcosa-di-meno-basic) perché lo fanno anche nella scuola del paese vicino.