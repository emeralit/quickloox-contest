---
title: "Smetto quando voglio"
date: 2015-11-26
comments: true
tags: [Pencil, iPad, Pro]
---
Ultimo giro di impressioni su [Apple Pencil](http://www.apple.com/it/apple-pencil/), perché poi ci si ripete. Concludo con il [Drogato della penna](http://www.penaddict.com/blog/2015/11/23/apple-pencil-review) che alla fine dichiara di essere stato sempre un *pen guy*, solo che ora è diventato anche un *Pencil guy*.<!--more-->

L’autore trova la Pencil facile e piacevole da tenere in mano, avrebbe preferito una clip per evitare il rotolamento ma giudica la Pencil perfettamente pesata, definisce il controllo dei tocchi impropri (della mano o del polso, mentre si disegna) in Note la *cosa più vicina che abbia mai visto a una vera esperienza di scrittura a mano su un apparecchio digitale*.

>Dire che non c’è ritardo sarebbe scorretto, ma siamo vicini quanto basta a percepire che funziona. Quando spostiamo la Pencil sullo schermo, la maggior parte delle linee viaggia appena appena dietro la punta. Ma il confronto con le precedenti esperienze di stilo su iPad è come quello tra il giorno e la notte.

Quello che voleva il drogato della penna era *poter scrivere sullo schermo in modo naturale, nella dimensione che siamo abituati a usare, testo che corrisponda visivamente a quello che ci aspettiamo di vedere*.

>And it does.

Si fa.

Ora smetto.