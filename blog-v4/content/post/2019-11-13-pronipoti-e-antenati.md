---
title: "Pronipoti e Antenati"
date: 2019-11-13
comments: true
tags: [Mac, IBM, Jamf, Previn]
---
Le difficoltà di adottare Mac in azienda: i costi, la compatibilità, la resistenza al cambiamento, gli standard di fatto.

Prendiamo per esempio una piccola-media azienda sconosciuta, una certa IBM. Dal 2015 a oggi [ha dispiegato solamente duecentonovantamila apparecchi Apple](https://www.appleworld.today/blog/2019/11/12/mac-use-enables-greater-productivity-employee-satisfaction-at-ibm), una cosetta. Chiaro che con numeri piccoli come questi non possano avere un quadro di insieme dei problemi che si incontrano, delle tastiere che si inceppano, delle batterie che si consumano, dell’applicazione a trentadue bit che dopo [dodici anni di avvertimenti](http://ww.macintelligence.org/blog/2019/10/11/la-crociera-costa/) ha deciso di non aggiornarsi (bisognerebbe avvisare prima… mica con un decennio di anticipo…) eccetera.

Si sa che i Mac costano di più; su una base di sole novantamila macchine, effettivamente meno affidabile del parere di [mio cuggino](https://elioelestorietese.it/canzoni/mio-cuggino/), in quattro anni IBM risparmia da duecentosettantatré a cinquecentoquarantatré dollari per macchina. Moltiplicare per novantamila.

I dipendenti con Mac sono più performanti di quelli con Windows, concludono contratti migliori con i clienti, chiedono meno software suppletivo, hanno più probabilità di continuare a restare in IBM e lavorare soddisfatti.

Per supportare duecentomila macchine Windows, IBM deve schierare venti ingegneri. Per duecentomila macchine Apple, sette. Windows richiede solo il centoottantasei percento di ingegneri in più, una minuzia. Cosa vuoi che contino gli stipendi di tredici ingegneri di alto livello, in un bilancio?

Il responsabile informatico di IBM, Fletcher Previn, ricorda di averlo già detto:

>Quando è diventato accettabile vivere come i [Pronipoti](https://it.m.wikipedia.org/wiki/I_pronipoti) a casa e come gli [Antenati](https://it.m.wikipedia.org/wiki/Gli_antenati) in ufficio?

Ecco, avrei da chiederlo a un tot di amministratori delegati e Chief Information Officer ossessionati dal fatturato e dai costi. Gente che compie la scelta più antiproduttiva per mancanza di alternative mentali.
