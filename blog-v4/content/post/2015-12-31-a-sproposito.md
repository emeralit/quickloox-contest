---
title: "A sproposito"
date: 2015-12-31
comments: true
tags: [Swift, Css, Lisp, Octopress, Jekyll, BBEdit, Wordpress, Twitter, LinkedIn, ]
---
Fine di un anno, inizio di un anno, piccolo sbilancio per capire su che disequilibri intervenire nel 2016. Almeno nelle intenzioni.<!--more-->

Il più grosso [impegno mancato del 2015](https://macintelligence.org/posts/2015-07-31-quasi-vacanza/) è stato il passaggio del blog a [Octopress 3.0](https://github.com/octopress/octopress). Per come stavano le cose ad agosto e se non mi sbaglio tuttora, di fatto si tratta di buttare via tutto, imparare a usare [Jekyll](https://jekyllrb.com) in completa antitesi agli intendimenti di Octopress 2.0 e ricostruire tutto, per iniziare a usare i nuovi strumenti di Octopress in corso di sviluppo.

Mi sono rapidamente perso in una selva di nozioni per le quali questa estate proprio non c’era il tempo e non ho più preso in mano la questione. Sarebbe il caso.

Più [Swift](https://swift.org). Più [Css](http://www.w3.org/Style/CSS/) e un pochino più di [Lisp](http://www.paulgraham.com/lisp.html). Anche progredire di poco, ma in modo netto, su uno solo di questi sarebbe straordinario.

Più flussi di lavoro. Mi spiego: faccio alcune cose nel Terminale, piuttosto bene. Ne faccio altre su BBEdit, bene. Altre su Wordpress, benino. Altre su Twitter, LinkedIn e i vari social di ordinanza, me la cavo. Altre ancora nel confezionamento di newsletter, il giudizio a chi le riceve. Scrivo libri in Html in modo collaudato usando Git e BitBucket, direi in modo soddisfacente.

Il problema è che i passaggi dall’una all’altra di queste attività sono tutti ad alto tasso di manualità. A fine 2016 vorrei che il maggior numero possibile di questi passaggi fosse affidato a hardware e software.

E poi più organizzazione, meno attenzione ai troll e alle bufale che girano, ma qui stiamo già entrando nella scontatezza degli spropositi per il nuovo anno.

Chi avesse voglia di condividere la propria lista è benvenuto. Guardo al prossimo anno con la voglia che sia pieno di interessi e di opportunità, da sfruttare o da creare, e condivido con tutti questo auspicio. Buon brindisi.