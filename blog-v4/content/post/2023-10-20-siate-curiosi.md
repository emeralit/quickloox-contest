---
title: "Siate curiosi"
date: 2023-10-20T00:15:45+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Whole Earth Catalog, Jobs, Steve Jobs]
---
Steve Jobs passa alla storia anche per l’iconico discorso all’università di Stanford in cui chiudeva ricordando come nella sua formazione sia stato fondamentale il *Whole Earth Catalog*.

Leggendo la sua descrizione, sappiamo che il *Whole Earth Catalog* era *un catalogo di riviste e prodotti di controcultura americana […] con una linea editoriale di autosufficienza, ecologia, istruzione alternativa, fai-da-te e olismo, sotto lo slogan “accesso agli strumenti”*.

La descrizione viene dal sito di Whole Earth, che da poco [permette di consultare in forma digitale tutta la sua passata produzione editoriale](https://wholeearth.info), compreso ovviamente tutto quanto è passato sotto gli occhi di Steve Jobs.

*Stay hungry, stay foolish*. Possiamo capirlo meglio, ora, se approfittiamo dell’opportunità per mantenerci anche *curious*.