---
title: "Soddisfare il partner"
date: 2013-11-05
comments: true
tags: [iPad]
---
L’ultimissima classifica di soddisfazione del cliente pubblicata per gli Stati Uniti da J.D. Power in tema di tavolette vede [Samsung battere Apple per 835 a 833](http://www.jdpower.com/content/press-release/Czb4Aa9/-2013-u-s-tablet-satisfaction-study.htm).<!--more-->

Ciò che J.D. Power rende pubblico del processo decisionale è una [tabella](http://www.jdpower.com/consumer-ratings/telecom/ratings/919201690/2013-Tablet+Satisfaction+Study/index.htm?sortBy=Overall%20Satisfaction%20desc) da cui si evince che Samsung batte Apple sul prezzo. C’è un pari nella soddisfazione complessiva; poi Apple batte Samsung nelle prestazioni, nella facilità di utilizzo, nel design fisico e nella dotazione.

Sembra un po’ strano che vincere quattro confronti e perderne uno ti faccia arrivare secondo, vero? 9to5 Mac si è insospettito e [ha chiesto lumi a J.D. Power](http://9to5mac.com/2013/11/01/did-jd-power-botch-its-tablet-rankings-giving-apples-crown-to-a-lower-scoring-samsung/).

La quale ha risposto in pratica che Apple vince di misura nelle sue categorie ma perde di molto sul prezzo, per cui alla fine succede che Samsung metta la testa avanti di un’incollatura. Il commento dell’articolista:

>Mi sembra strano, ma non potendo vedere i punteggi effettivi si può solo dire che sicuramente i possessori di iPad erano più soddisfatti delle loro tavolette, ma i proprietari di Samsung erano più soddisfatti del prezzo che hanno pagato.

Personalmente mi chiedo se J.D. Power, nel compilare questa classifica di soddisfazione del cliente, non avesse da soddisfare un qualche cliente suo.