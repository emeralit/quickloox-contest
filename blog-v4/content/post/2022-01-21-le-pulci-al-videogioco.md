---
title: "Le pulci al videogioco"
date: 2022-01-21T01:23:50+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Lidia e io abbiamo terminato vittoriosamente [Bugdom 2](http://pangeasoft.net/bug2/), gioco di azione di [Pangea](https://pangeasoft.net/index2.html) che compie vent’anni.

Pangea è stata un nome di rilievo a un certo punto della storia di Macintosh e per alcuni anni i suoi giochi sono stati inclusi nei nuovi Mac grazie a un accordo con Apple; nel 2008 l’azienda decise di puntare tutto su iOS. Sa allora non è successo moltissimo (l’ultima uscita è del 2014); in compenso ci sono ancora segni di vita. Il sito, d’altri tempi, funziona e i giochi sono regolarmente disponibili su iOS e iPadOS, a volte anche aggiornati alle ultime versioni dei sistemi operativi.

*Bugdom 2* è la lunga e paziente sfacchinata di una eroica cavalletta (Skip) alla ricerca di suo fagottino, rubatogli da un calabrone, principalmente nel giardino e parzialmente in una abitazione dove si trova almeno un bambino e almeno un cane. Non si vedono mai, ma un livello si svolge proprio attorno al collare del cane, dove si tratta di decimare pulci e zecche, e un altro contrappone a Skip soldatini di plastica e robogiocattoli.

I suoi alleati sono la lumaca Sam e la scoiattola Sally, che gli affidano missioni da compiere e salvano la situazione lungo il percorso.

Nè troppo facile né troppo difficile, il gioco è carino, ricchissimo di idee e particolari, adatto praticamente a tutte le età con un po’ di assistenza genitoriale perché la destrezza minima necessaria per andare oltre il primo dei dieci livelli è significativa e le missioni da compiere a volte sono banali, a volte meno; risolverle è importante perché fruttano chiavi per aprire nuove aree e procedere. Su Mac, posto che il gioco funzioni ancora (noi abbiamo giocato stabilmente su iPad di prima generazione), è presente un *kiddie mode* che seda i cattivi e diminuisce notevolmente la loro aggressività, consentendo a un bambino piccolo molta più libertà di movimento.

Di cattivi ce ne sono: pulci, zecche, mosche, gnomi da giardino, tarme, formiche, libellule, rane, persino lucidatrici anni sessanta. Tante farfalle danno altrettanti bonus per sopravvivere, svolazzare e lanciare moscerini killer a distanza; i livelli tipici prevedono di farsi largo in ambienti come il retro della casa, la piscina, il giardino e ci sono anche due livelli da batticuore (fognatura e grondaia) nei quali l’unica cosa da fare è evitare gli ostacoli posti lungo una discesa sempre più veloce.

Né facile né difficile dicevo, *Bugdom 2* richiede comunque un accorto uso delle risorse perché non è scontato arrivare a fine gioco con vite supplementari e abbastanza risorse per riuscire. Bisogna procedere con pazienza e metodo; le situazioni sono tutte facilmente padroneggiabili, ma basta una distrazione per ritrovarsi a perdere energie e vite. All’inizio ce ne sono da spendere a volontà, alla fine sempre meno e i punti di salvataggio sono relativamente pochi. Se si conclude male qualche livello, può essere necessario anche un salto indietro impegnativo per migliorare la situazione.

Lidia e io abbiamo condiviso i comandi (io ai movimenti, lei alle azioni, sparare, prendere, saltare, svolazzare) e ci siamo avventurati sempre un po’ più avanti, sera dopo sera. Non abbiamo fatto uso di mappe o suggerimenti; le une e gli altri sono sul sito Pangea per chi volesse facilitarsi il compito o avesse semplice curiosità.

Per arrivare in fondo servono diverse ore di gioco. Le parti cruciali che chiedono tempo sono il rifornimento tramite le farfalle volanti (regalano anche vite extra, che bisogna assolutamente prendere) e la soluzione delle missioni. A volte si tratta di liberare topini catturati dalle trappole apposite in giardino, a volte rimettere la testa caduta a uno spaventapasseri, oppure schiacciare more facendo gli equilibristi sopra una palla da baseball eccetera.

Il finale non è niente di che e non conta: la soddisfazione di essere arrivati alla fine dei dieci ambienti di gioco è certamente superiore a qualunque altra sensazione.

Per quanto si tratti di giochi evidentemente vecchi, *Bugdom 2* e i numerosi altri giochi Pangea sono un’offerta onesta e ancora valida per divertirsi senza troppo stress e senza frustrazioni. Su Mac (da verificare) e su iOS (per esperimento ho caricato il gioco su iPad Pro e funziona) possono costituire un’alternativa ancora fresca e interessante nonostante come anagrafe siano ormai giovani adulti. Se qualcuno mira al punteggio (costa molto tempo in più che risolvere semplicemente il gioco), noi abbiamo superato i 525 mila punti e lasciato indietro poco. Giusto come *benchmark*.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._
