---
title: "Nuove carte da giocare"
date: 2019-08-12
comments: true
tags: [HyperCard, Oren, Atkinson, Myst, Cyan, Drang, Shortcuts, database]
---
Ieri erano trentadue anni dal primo [annuncio di HyperCard](https://macgui.com/usenet/?group=14&id=4827), programma che ho molto amato e che, come è giusto fare con ciò che si ama veramente, bisogna saper lasciare andare per girare pagina.

È un invito agli ultimi nostalgici. Ce ne sono ancora diversi. La [storia del programma](https://arstechnica.com/gadgets/2019/05/25-years-of-hypercard-the-missing-link-to-the-web/) è senza dubbio affascinante, ma che cosa ci lascia effettivamente in eredità HyperCard, ora che le [orazioni funebri](https://due-diligence.typepad.com/blog/2004/03/a_eulogy_for_hy.html) sono diventate vecchie pure loro, in rete si trovano tutte le [risorse](https://hypercard.org/) possibili e immaginabili per pasticciare con gli stack in assenza del software e quest’ultimo è [emulabile via browser](https://hypercardadventures.com/)?

Un database, e va beh. Ricerca full text, e ne abbiamo fin troppa. Programmabilità, e le cose si fanno interessanti. Creazione di oggetti software a cui associare programmi: questo è il vero cuore dell’innovazione del 1987. Si poteva fare e soprattutto si poteva fare facile.

Il nostalgico userà i link sopra per cercarsi un succedaneo di HyperCard che oggi fa le stesse cose. Lui sta buono e noi passiamo all’evoluzione del concetto, invece, che consiste nell’applicare comandi a parti arbitrarie del sistema che, concatenati, portano a un risultato superiore alla somma delle parti.

Guardiamo a Dr. Drang che [usa i Comandi rapidi su iOS per semplificarsi la vita](https://leancrew.com/all-this/2019/08/unpaid-shortcut-redux/). La filosofia è ortogonale a quella di HyperCars. Ma la facilità di programmazione e la libertà di associare dati e funzioni presenti nel sistema e normalmente estranei… ecco, se Bill Atkinson oggi si mettesse di nuovo a programmare, forse cercherebbe una flessibilità di questo tipo.

Abbiamo nuove carte da giocare, nel cammino verso un computing personale e potenziato. Addio nostalgia.
