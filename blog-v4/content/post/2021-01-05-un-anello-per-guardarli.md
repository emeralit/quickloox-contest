---
title: "Un anello per guardarli"
date: 2021-01-05
comments: true
tags: [KJ-19, Oluminate, OlumiRing]
---
Il mio angolo di lavoro è molto confortevole ma poco illuminato per gli standard di una buona videoconferenza. Così per Natale mi sono regalato un piccolo OlumiRing di [Oluminate](https://oluminate.com).

Concepito per stare sopra a un iPhone e illuminare il volto dell’interlocutore, va benissimo anche su un iPad. L’oggetto è grande pressappoco come il palmo di una mano, leggerissimo per quanto di policarbonato Abs più che onesto, ed è un’unica grande clip per fissarsi se necessario anche a un portatile. Nel foro dell’anello passano tre dita della mano. (OK, diametro nove centimetri, spessore poco più di un centimetro, larghezza della corona circolare pure).

Eroga via Led (venti bianchi e venti gialli) tre diverse temperature di luce (da 3.500K a 6.500K), ognuna delle quali può essere moderatamente graduata in intensità. Al primissimo istante la luce può disorientare un attimo, ma poi non disturba e anzi, nell’utilizzo ho alzato l’intensità dopo averla abbassata.

La prima carica di OlumiRing, che arriva con un suo cavetto *ad hoc* per fare rifornimento tramite una porta Usb classica, ha richiesto un’oretta. Non ho ancora idea di che autonomia abbia; l’ho collaudata nel corso di una videochiamata di un’ora, durante la quale ha funzionato.

L’interfaccia è elementare: un pulsante, messo per fortuna nella posizione giusta, commuta tra le tre temperature di luce e lo spegnimento. Tenere premuto il pulsante diminuisce l’intensità. Tenere una seconda volta premuto il pulsante la aumenta. Vicino al pulsante si vede il connettore per la carica, durante la quale OlumiRing mostra una spia rossa. Al termine, si vede una spia verde. Il pulsante sporge leggermente e così si avverte al tatto: è possibile cercarlo e azionarlo intanto che si continua a guardare nell’obiettivo.

Dettaglio interessante per gli investigatori, il microdepliant allegato al prodotto, con istruzioni (del tutto minimali) in inglese e cinese: non c’è traccia di *branding* sul foglietto e l’oggetto viene denominato *Selfie Ring Light XJ-19*. Probabilmente qualcuno interessato a scoprirlo potrebbe tentare di identificare l’azienda produttrice, suppongo cinese o taiwanese.

Mentre scrivo, OlumiRing è in vendita sul suo sito a 19,82 euro. Le spedizioni *non* sono veloci come quelle di Amazon e bisogna prepararsi a qualche attesa. Arrivare arriva, comunque, e per ora la carta di credito non segnala stranezze.

OlumiRing è una soluzione molto semplice, compatta e pratica per stare bene illuminati in viso durante una videoconferenza; esattamente quello che mi serviva, dal momento che mi può accompagnare su iPad e iPhone in qualsiasi punto dello studio o di casa, nonché all’esterno. Il prezzo è al di là del bene e del male; se l’oggetto avrà una durata appena ragionevole, non c’è neanche da parlarne.

Se oggi può sembrare a taluni una accortezza da vanesi o un dettaglio esagerato, curare l’illuminazione del viso beh, cambierà presto. Le persone inizieranno, qualcuna ha già iniziato, a discriminare i setup e qualche manager di alto livello è già arrivato al velo di fondotinta, senza distinzione di genere. Come minimo, evitare di dialogare sotto forma di ombra che emerge dall’Ade è un atto di rispetto verso l’interlocutore.