---
title: "Tempi diversi"
date: 2015-07-02
comments: true
tags: [tempo, Amazon, Casio, watch, Facebook, YouTube, Boston, SanFrancisco, Vlc, VideolanClient, Attivissimo, iOS]
---
Il tempo di scrivere su Amazon la recensione di un orologio-calcolatrice Casio da quindici dollari per [equipararlo a watch](http://recode.net/2015/06/30/this-casio-calculator-watch-is-better-than-apple-watch-according-to-amazon/).<!--more-->

>Mio marito vuole un watch. Credo siano stupidi e costosi. Gli ho detto che avere un watch è come avere questo. Sorprendentemente gli piace. E lo porta. Funziona veramente bene.

Che è il tempo dei *mi piace* su Facebook, dei commenti a fondo perduto su YouTube. Le dita si muovono sulla tastiera senza disturbare il cervello.

Il tempo di [collaudare watch in un viaggio Boston-San Francisco](http://www.cio.com/article/2942470/smartwatches/why-apple-watch-is-a-business-travelers-best-friend.html) con annessi e connessi di alberghi, ristoranti, carte d’imbarco, pagamenti, promemoria, appuntamenti, musica, mappe.

>Nel giro di ore dopo avere lasciato casa, sono rimasto sorpreso di come il viaggio sia stato più tranquillo e sereno grazie al primo smartwatch di Apple.

Il tempo di [programmare Vlc perché supporti anche watch](https://itunes.apple.com/it/app/vlc-for-ios/id650377962?mt=8&app=itunes&ign-mpt=uo%3D4). Quando si muove l’*open source* e quando si muove rapidamente, vuol dire che è importante.

Il tempo usato da Paolo Attivissimo per definire quelli iOS [terminali “stupidi” e lucchettati](https://macintelligence.org/posts/2013-07-20-citofonare-paolo/) in quanto impossibilitati dalla cattiva Apple a fare girare software libero. Vlc è software libero.

Il tempo che è galantuomo a differenza di chi non sa riconoscere di avere detto una sciocchezza.