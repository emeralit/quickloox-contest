---
title: "La partita più pazza del mondo"
date: 2017-07-09
comments: true
tags: [football, Pioneer, 17776]
---
Apprezzo il football americano e la fantascienza, solo che non riesco a ricordare il titolo di quel libro dove il football si giocava alla morte nel campo costituito da una città deserta, nella quale il portatore di palla poteva nascondersi in un edificio o dentro un tombino e la squadra avversaria poteva schierare un cecchino alle finestre ([trovato](http://giurista81.blogspot.it/2016/11/recensioni-narrativa-boston-2010-xxi.html)).

Ne ne sarei fatto una ragione perché è arrivato [17776](https://www.sbnation.com/a/17776-football), un racconto distopico sull’evoluzione – o degenerazione – del football che è anni luce avanti. Mentre scrivo, la storia deve ancora completarsi. Quello che c’è già ha portato via la buona parte di una notte, come ogni lettura avvincente che si rispetti.

A questo proposito, adoro i libri di carta. Quello che si può fare con il web tuttavia, senza effettacci gratuiti ma con una storia che merita e un pizzico di creatività, beh, è un’altra cosa e non tornerei mai indietro al 1987, prima dei browser, prima dell’internet.
