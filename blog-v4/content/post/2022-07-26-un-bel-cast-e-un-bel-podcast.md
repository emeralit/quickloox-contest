---
title: "Un bel cast e un bel podcast"
date: 2022-07-26T02:04:17+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [Mac, Mac, M2, M1, Apple Silicon, Intel, Photo Continuity, iCloud Shared Library, macOS, Ventura, macOS Ventura, iPadOS, iOS]
---
Un bel grazie al [dinamico duo di A2](https://macintelligence.org/posts/2022-05-31-eravamo-tre-amici-alla-touch-bar/) che mi ha nuovamente coinvolto in un podcast dedicato ai postumi di Wwdc.

Avevamo una scaletta e l’abbiamo seguita sovvertendola, dal primo momento, e parlando di una ennesima milionata di cose.

In primo piano le novità dei prossimi sistemi operativi Apple, con digressioni su Mail, librerie condivise di iCloud, il gusto di Apple per la collaborazione e molto molto altro, tra blackout imprevisti, connessioni che imprevistamente hanno tenuto. arresti improvvisi, ripartenze subitanee, sorprese qua e là.

Diventerà un podcast fatto e finito e ne sono solo orgoglioso; condividere la banda con Roberto e Filippo è un vero piacere e c’è tanto da imparare nello stesso momento in cui c’è tanto da dire: non potrebbe succedere in alcun altro posto.