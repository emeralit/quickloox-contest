---
title: "Computer da sogno"
date: 2019-09-05
comments: true
tags: [Mac, Leolandia, PowerBook, G4, Titanium]
---
[Leolandia](https://www.leolandia.it/) è un posto dove genitori eroi (esclusi i presenti) portano i bambini a sognare.

L’audio di uno degli spettacoli era alimentato da un [PowerBook G4 Titanium](https://www.imore.com/titanium-powerbook-g4-power-and-sex).

 ![Un Titanium a Leolandia](/images/leolandia.jpg  "Un Titanium a Leolandia") 

Uno dei migliori Mac della mia vita, con cui effettivamente ho potuto sognare per molto tempo. Tutto si tiene.
