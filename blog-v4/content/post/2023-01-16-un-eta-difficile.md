---
title: "Un’età difficile"
date: 2023-01-16T00:05:33+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Safari]
---
A vent’anni si pensa di avere capito tutto senza avere capito gran che; ci sono le energie per cambiare il mondo e manca la conoscenza di come farlo effettivamente; si sottovaluta l’esperienza e si pensa di essere immortali.

Safari [ha compiuto vent’anni](https://mjtsai.com/blog/2023/01/13/20-years-of-safari/) e su certe cose sembra un ragazzo entusiasta e responsabile ma un po’ confuso su come lasciare effettivamente il segno nell’universo, *the Steve Jobs way*.

È tuttora il mio browser preimpostato anche se una piccola percentuale di siti dà problemi o semplicemente non lo accetta. La piccola percentuale è per me rilevante ([Roll20](https://roll20.net/welcome), per esempio, se vogliamo collegarci a distanza) però preferisco passare per l’occasione a Chrome o Firefox che abbandonarlo.

Ci sono numerosissime alternative, ma non mi convincono ancora e sarà forse che io vorrei un web dove il browser in effetti non contasse e quello che c’è da renderizzare nella finestra venisse renderizzato in modo preciso sempre e comunque, dunque un web dove usare Safari contasse o meno per la velocità, l’usabilità, la semplicità, l’immediatezza più che la compatibilità. Il mio usare Safari è un dire *ehi, lancio qualcos’altro se serve e va bene, ma guardate che il web non è un terreno di conquista, bensì un luogo di incontro.*

Sarebbe bello, visto che ci siamo dentro da trent’anni, vedere il superamento del concetto di browser. Lanciare qualcosa capace di condurci direttamente sull’informazione, condividerla, aggregarla ad altre in modo coerente e ispiratore, dirimere informazioni secondarie che nascono dalla consultazione di più pagine adiacenti per contenuto ma non appaiono con evidenza.

Parliamo, sì, anche di intelligenza artificiale. Purtroppo anche di ristrutturare pesantemente Http e Tcp/Ip e, a cascata, un sacco di altre cose.

Non trattengo il fiato e vado avanti con Safari, anche a salvaguardia di una diversità e libertà di piattaforma che è stata più volte a rischio e a cui tengo.

Buon compleanno giovane adulto e deciditi, con quella testa calda che ti ritrovi, a implementare WebRtc come lo intende Roll20.