---
title: "Il derby in cifre"
date: 2022-02-15T01:23:22+01:00
Draft: false
toc: false
comments: true
categories:
tags:
---
Durante il [Super Bowl](https://macintelligence.org/posts/2022-02-14-una-partita-da-divano/), oltre al titolo del football americano, si combatte anche una gara tra pubblicitari. Trasmettere uno spot durante la partita è costosissimo ma, per le aziende, può essere un’occasione veramente unica per sfondare. Probabilmente per questo si è giocato anche un derby tra trader di crittovalute, [Coinbase](https://www.coinbase.com) e [Crypto.com](https://crypto.com/eea/). Pur non avendo particolare interesse verso Bitcoin e compagnia, ho pensato di provare a farmi un account su ciascuna piattaforma, per vedere l’effetto che fa.

In sintesi: se fosse il mondo reale, ne porterei a casa una brutta impressione. Se fosse possibile fare lo spezzatino e montare una app con il meglio delle due, non andrebbe neanche male. Ma i difetti sono consistenti e i pregi ne risentono.

Prima di tutto, l’*onboarding*, per parlare come le istituzioni finanziarie: salire a bordo. Indecente.

C’è un livello di profilazione che in qualsiasi altro settore farebbe gridare allo scandalo. Qui bisogna capire, perché sono app che devono sottostare alle regolamentazioni di mezzo mondo – quelle europee non sono propriamente banali – e però, insomma, dal mondo digitale uno vorrebbe aspettarsi la sicurezza e la garanzia senza troppe vessazioni.

Qui iOS mi ha dato almeno il contentino: ambedue le app mi hanno chiesto se autorizzavo il tracciamento delle mie attività fuori dal loro ambito e ovviamente no.

Crypto.com è più furba e meno macchinosa. Si riesce ad aprire un account senza dover inserire una password. In compenso non abilita Face ID e chiede un suo passcode per sbloccare la piattaforma. La biometrica mi pare fondamentale per un caso d’uso come questo.

Coinbase abilita propriamente Face ID ed è un merito. Poi però è un martirio. Vuole – come Crypto.com – il cellulare e va bene. Chiede inoltre la foto fronte e retro di un documento di identità e anche un selfie assieme a un bigliettino su cui bisogna avere scritto a mano che la foto viene inviata per consentire il trading sulla app. L’effetto porno amatoriale è davvero demoralizzante.

Ho fatto tutto, ma è stata durissima. Durante la procedura della foto del documento, il controllo si alterna in un modo che non ho capito bene tra la app e il sito e la procedura, spesso e volentieri, si incanta; ho dovuto ripetere la doppia foto del documento almeno cinque volte prima di riuscire ad arrivare in fondo. E poi ho dovuto rifare, perché a dire del reparto autenticazione una delle due foto aveva una zona di ombra.

Crypto.com è stata molto più efficiente sul documento di identità; l’interfaccia è molto più umana e, inoltre, funziona. In compenso, prima di arrivare a quel punto mi sono ritrovato in un circolo vizioso dentro cui non riuscivo a passare la fase della convalida dell’indirizzo di email necessario per fare partire la procedura. Anche qui, ci avrà messo cinque o sei tentativi. Posso naturalmente avere un problema io di scarsa comprensione o di distrazione o altro; certo, per programmi che alla fine trattano soldi e risparmi, funzionare anche a prova di stupido sarebbe un pregio.

L’*accredito* dei fondi. La premessa qui è che, abituati male come siamo, si vorrebbe poter operare da subito. Coinbase è la app con le maggiori possibilità: si possono inserire un account PayPal, un conto corrente bancario e una carta di credito.

La cosa più veloce è naturalmente l’account PayPal; solo che consente unicamente il prelievo. Iniziando da zero e volendo depositare fondi, non è ideale. Ho comunque inserito l’account, per passare alla seconda opzione più veloce: la carta di credito.

Coinbase non la accetta. Ho provato una dozzina di volte, certamente con i dati corretti, niente. La app dà errore e non se ne parla.

Va beh; allora, il conto corrente bancario. Si può fare, ma per cominciare occorre bonificare un centesimo sul conto irlandese di Coinbase. Dopo di che, verificheranno l’operazione e magari domani mi diranno che posso operare.

In definitiva, buttata mezz’ora per arrivare a comprare crittovaluta via Coinbase, non lo posso fare. Se va bene, passerà la notte. Il digitale suggerisce di avere aspettative più alte.

Crypto.com è mooolto più amichevole. Inserire la carta di credito è stato un attimo (la stessa che Coinbase non riesce a riconoscere) e così ho evitato di attivare anche il conto bancario, che la seconda possibilità a disposizione. Ho allora scatenato l’emozione di comprare un decimillesimo di Bitcoin, pari a qualcosa più di tre euro.

Poi mi sono accorto che nella home della app gli importi riferiti all’account personale sono in cifra tonda. Figuro avere zero Bitcoin e non posso verificare di averne correttamente zero virgola zero zero zero uno. Non sono riuscito a trovare l’elenco delle transazioni. In altre parole, nella app *non riesco a capire se ho davvero comprato il mio granellino di Bitcoin*. Non è chiaro se ho speso i miei tre virgola euro oppure no, nonostante mi siano stati chiesti dalla carta i codici giusti. Anche del fatto che per effettuare un’operazione bisogna digitare tre o quattro codici di sicurezza bisognerà parlare prima o poi, perché la cosa sta sfuggendo di mano.

Dopo un’ora di sforzi, *forse* sono riuscito a comprare tre euro di Bitcoin, con una app su due. Se è questo il futuro, non mi piace.

Per quelli disposti a tutto: Coinbase arriva a regalare fino a quindici euro di Bitcoin se portiamo un amico che si iscrive e compie delle transazioni. Crypto.com è più avanti, fino a venticinque euro. Però i requisiti sono più stringenti.

Crypto.com è più avanti in altri due punti: primo, offre gratuitamente una carta di credito che sembrerebbe avere ottime condizioni di utilizzo. Una *vera* carta di credito, fisica, disponibile in tre colori diversi.

Secondo, su Crypto.com si possono commerciare Non Fungible Token, gli NFT che rappresentano l’ultima moda. Ho l’impressione che siano un modo per riuscire a far muovere crittovaluta più di quanto non accadrebbe, però non conosco così bene la materia e potrei sbagliare. In ogni caso, arrivare alla compravendita di NFT è molto più semplice.

Chi vince il derby della crittovaluta? Crypto.com, di misura, per la maggiore snellezza delle operazioni e una versatilità che a Coinbase manca.

Però che punteggio basso, che gioco frammentario, che squadre disorganizzate. Per misurarsi con le crittovalute occorre fiducia e qui bisogna veramente fare uno sforzo.
