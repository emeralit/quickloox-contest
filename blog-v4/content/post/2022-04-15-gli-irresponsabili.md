---
title: "Gli irresponsabili"
date: 2022-04-15T10:47:24+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [FramaPad, Copernicani, Etherpad]
---
Sono coinvolto in un lavoro di traduzione collettiva con l’associazione [Copernicani](https://www.copernicani.it), sull’argomento della privacy e del rispetto dei dati personali da parte del software.

Non descrivo la situazione per vantarmi, solo per collocarla: non è un gruppo di scappati di casa e l’ambito è diverso dagli hobby o dalle passioni amatoriali.

il lavoro completato andrà negli Stati Uniti dove risiede l’autore del testo originale, per le lavorazioni successive.

il software che abbiamo adottato è [EtherPad](https://etherpad.org) nella forma concreta di [FramaPad](https://framapad.org/abc/en/).

Requisiti di sistema zero, collaborazione al cento percento, problemi di compatibilità inesistenti, esigenze di aggiornamento assenti, riservatezza totale, comodità assoluta.

Se penso a quello che tanta gente paga in denaro, ingombro di dati, interfacce improbabili e inconvenienti tecnici vari, per fare lavoro persino più semplice in modo più difficile e involuto, mi viene il magone.

D’altronde, lo sperimento da tempo, una intera categoria di persone è determinata a rifiutare qualsiasi cambiamento, anche se lavora male, anche se sta male, anche se l’evidenza è chiara, comunque.

Questo è un problema, perché ognuno può fare quello che vuole. Ma sia chiaro chi è che rallenta gli altri, chi è di peso per la comunità, chi costringe tutti a fare tre passi avanti e due indietro. Non si parla di chi ha bisogno di aiuto, eh. Ma di chi ignora, consapevole, le proprie responsabilità comuni.