---
title: "Uno sguardo dal ponte"
date: 2014-05-03
comments: true
tags: [Hearthstone, WoW, Azeroth, Ithilgalad]
---
Durante il ponte ho avuto stranamente del buon tempo ludico a disposizione e l’ho trascorso provando [Hearthstone: Heroes of Warcraft](https://itunes.apple.com/it/app/hearthstone-heroes-warcraft/id625257520?l=en&mt=8), gioco di carte uno contro uno ispirato al mondo di [World of Warcraft](http://eu.battle.net/wow/en/).<!--more-->

Tecnicamente è un gioco di Virtual Combat Card, Vcc. Chi ha esperienza di Magic può saltare il resto di questo paragrafo. Ogni giocatore ha a disposizione un mazzo di carte dove ogni carta ha poteri e possibilità particolari. A turno i due avversari scoprono le loro carte e in funzione degli esiti si determina il punteggio.

Ispirato appunto da World of Warcraft, Hearthstone mette a disposizione di chi comincia classi di personaggio che provengono proprio da WoW: mago, irregolare, sacerdote, guerriero, *warlock*, druido eccetera. Ogni classe ha una dotazione di carte base. Ogni combattimento fa guadagnare esperienza; le partite contro altre classi, pilotate dall’intelligenza artificiale, permettono lentamente di espandere il mazzo di base.

Quando si arriva a un minimo di conoscenza dei meccanismi del gioco, si possono sfidare altri amici iscritti a [Battle.net](http://eu.battle.net/en/) oppure perfetti sconosciuti via Internet, scelti a caso oppure individuati dal sistema come più o meno al nostro stesso livello di capacità.

A quel punto il mazzo sarà composto da trenta carte, mescolate tra quelle base della classe scelta e carte neutrali, aperte a qualsiasi classe. Si possono comporre più mazzi diversi e giocare ogni volta con una classe diversa. Esiste un meccanismo di disincantamento delle carte, che gli adepti di WoW ben capiscono, che permette di disintegrare carte guadagnate ma ritenute inutili e, con la polvere magica derivata, materializzare altre carte diverse e più utili. Durante ogni partita il computer mescola il mazzo e distribuisce le carte in ordine casuale. Al giocatore sta comporre il mazzo in modo da minimizzare l’influenza del caso e riuscire a prevalere.

Tutto questo è possibile gratis, accumulando presenze e possibilmente vittorie, oppure a pagamento, per una volta in maniera equilibrata e discreta. Ho giocato a costo assolutamente zero e scalato le classifiche, salendo dal venticinquesimo livello e zero stelline al ventitreesimo e due stelline e pure portando a casa una monta che potrei utilizzare in World of Warcraft.

Roba all’avanguardia: grafica perfetta, giocabilità perfetta, un sacco di cose da fare anche senza spendere e tanta atmosfera. Il gioco è impreziosito da tanti piccoli particolari di interfaccia, effetti, situazioni che non voglio svelare (scaricarlo è *gratis*, provare costa niente). Confesso che sono stato tentato anche dal fatto che il tempo per stare su Azeroth è zero e ne ho approfittato per placare un pizzico di nostalgia.

Problemi non ce ne sono. Per dire, l’interfaccia è anche in italiano se si vuole. Mi è capitato del tutto occasionalmente un piccolo problema di ridisegno della plancia di gioco, senza effetti sulla partita. Altrettanto occasionalmente è successo di inviare una richiesta ai server di Battle.net (una volta per cercare un avversario, una volta per avere chiuso erroneamente la connessione e volerla riprendere) e restare con il gioco bloccato oppure nell’impossibilità di recuperare la situazione. Insomma il software non è superblindato nel funzionamento come [Lords of Waterdeep](https://macintelligence.org/posts/2014-02-24-varie-da-viaggio/), ma si tratta di problemi venialissimi. Ovviamente il massimo sfruttamento del gioco lo si ottiene quando c’è connessione.

Il gioco con l’intelligenza artificiale si impara in fretta ed è gratificante; quando si va nel mondo a incontrare avversari umani, la gratificazione richiede al contrario duro lavoro di perfezionamento (sempre ludico comunque). Chi mi volesse massacrare in allegria mi troverà con il nome d’arte di Ithilgalad.
