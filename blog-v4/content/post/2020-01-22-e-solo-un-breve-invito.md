---
title: "È solo un breve invito"
date: 2020-01-22
comments: true
tags: [Inc, PowerPoint, Keynote]
---
Per quanto mi affanni a spiegare nel dettaglio certe cose in aula o in una classe, vengo sempre guardato strano o male quando il soggetto diventa (l’abbandono di) PowerPoint.

Eppure, nel resto del mondo che evolve e va avanti, si pubblicano articoli come [È il 2020. Perché stai ancora usando PowerPoint?](https://www.inc.com/geoffrey-james/its-2020-why-are-you-still-using-powerpoint.html).

Sottotitolo: *I migliori comunicatori al mondo hanno smesso di usare PowerPoint. Perché sei fermo agli anni ottanta?*

Frasi semplici, comprensibili anche nei più retrivi uffici di marketing e comunicazione:

>La premessa che sta dietro PowerPoint (e i suoi cloni) è che il pubblico capirebbe e memorizzerebbe meglio le informazioni quando vede testo su uno schermo mentre uno speaker parla. Sembra intuitivamente vero, ma le evidenze dicono l’opposto.

Si citano studi in cui la memorizzazione delle informazioni trasmesse agli studenti è inferiore del 15 percento quando la lezione avviene con il, chiamiamolo, supporto di PowerPoint. Un altro studio non trova differenze nella memorizzazione a breve e a lungo termine quando viene allestita una presentazione PowerPoint, rispetto ad altri metodi più semplici. In altre parole, il tempo speso su PowerPoint, quando non è controproducente, è perso.

Per non parlare della maggiore difficoltà nell’interagire con il pubblico, nella demotivazione di quest’ultimo a prendere appunti e note che aiuterebbero la memorizzazione e così via.

Giusto per fare un esempio, Jeff Bezos – il signor Amazon – [non usa PowerPoint](https://www.inc.com/geoffrey-james/the-worlds-best-communicators-dont-use-powerpoint.html) e con lui molti altri.

Intendiamoci: lo strumento è solo il peggiore della sua categoria. Fornire informazione a voce e contemporaneamente informazione a video provoca un sovraccarico cognitivo nello spettatore e lo ostacola nell’ascolto e nella comprensione. Sempre. Usare Keynote è marginalmente meglio, ma non è una cura. È meno peggio.

Chissà se qualcuno che non ascolta me, almeno dà retta ai fatti.

Grazie a Franco Battiato e più in particolare a Manlio Sgalambro per [avere ispirato](https://www.youtube.com/watch?v=0bpO11FL7d4) il titolo.