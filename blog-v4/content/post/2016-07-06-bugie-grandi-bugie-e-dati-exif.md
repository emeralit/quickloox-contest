---
title: "Bugie, grandi bugie e dati Exif"
date: 2016-07-06
comments: true
tags: [Huawei, P9, Microsoft, Msi, Asus]
---
Torniamo un momento su [Microsoft che tarocca i risultati del suo *browser* Edge e Asus e Msi che passano ai recensori unità non corrispondenti alle specifiche vendute](https://macintelligence.org/posts/2016-06-24-il-paese-dei-baloccati/).

Huawei ha pubblicato una pagina sul suo computer da tasca P9 con una foto bellissima… scattata con una macchina professionale dal costo dieci volte superiore.

I furbetti non hanno scritto esplicitamente (e falsamente) che la foto era stata scattata con il loro apparecchio; l’hanno messa semplicemente lì a lasciare che suggestionasse il visitatore.

Tuttavia si sono dimenticati di taroccare i dati Exif e così chiunque avesse voluto controllare, poteva sbugiardarli. Così è stato.

Huawei [ha tolto la foto dalla pagina](http://www.androidpolice.com/2016/07/04/huawei-publishes-implied-p9-camera-sample-but-exif-data-reveals-4500-camera-took-it/). E il paese dei baloccati espande i suoi confini.

*[Se [il libro di Akko su macOS e dintorni](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/) si avverasse come progetto, spiegherebbe anche come ottenere il massimo dall’ecosistema. [Va divulgato e sostenuto](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac).]*