---
title: "Conservatore progressista"
date: 2014-04-22
comments: true
tags: [BBEdit, Textmate, Coda, SublimeText, PhpStorm, Dreamweaver]
---
Mi consigliano spesso questo o quell’*editor* di testo, ma resto su BBEdit anche quando sembra meno attraente (pur attento a sperimentare).<!--more-->

BBEdit sembra a volte la gallina vecchia, quella che fa pure buon brodo ma solo per non contestare un proverbio ancora più vecchio. Poi però succede sempre qualcosa, per esempio questo [intervento sulla *mailing list* dedicata al programma](https://groups.google.com/d/msg/bbedit/J1_IO6odhGs/Ndhwe92MT2YJ. Protagonista un file da 129 megabyte.

<blockquote>
BBEdit: due secondi per aprirlo, scorrimento fluido, tutto a posto.<br />
Textmate 2: 12 secondi per aprirlo, scorrimento fluido, pausa se ridimensiono la finestra, colorazione della sintassi veloce sulle prime centomila righe, ma poi ci ha impiegato minuti per il resto del file.<br />
Textmate 1: quasi uguale a Textmate 2, con cursore rotante se uso la funzione “Vai alla riga”.<br />
Coda 2: 15 secondi per aprirlo, niente colorazione della sintassi, ogni scorrimento mostra il cursore rotante, inusabile.<br />
PhpStorm 7.1: messaggio “File troppo grande”.<br />
Sublime Text 2: un minuto e mezzo per aprirlo, poi superveloce e reattivo anche con la colorazione della sintassi e la minimappa.
</blockquote>

Un altro test con cento kilobyte di codice Html:

<blockquote>
BBEdit: superveloce!<br />
Textmate: la colorazione della sintassi chiede un po’ di tempo…<br />
Sublime: tre secondi per aprirlo, poi superveloce.<br />
Coda: niente colorazione della sintassi, pigro, colloso, noioso.<br />
Dreamweaver CS 5.5: diciotto secondi per caricarsi, poi un minuto di cursore rotante dopo lo scorrimento fino a fine pagina… poi ok, anche nella finestra divisa tra testo e anteprima.
</blockquote>

Pochi secondi moltiplicati tante volte fanno molti minuti e allora mi tengo stretto il programma più antico, che a volte si rivela quello più avanti.