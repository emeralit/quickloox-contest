---
title: "La punizione per il curioso"
date: 2022-09-05T02:13:08+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Kosmik]
---
Ti parlano di [Kosmik](https://www.kosmik.app) e la pagina illustrativa sembra proprio interessante. Allora lasci l’indirizzo email per l’accesso alla beta e ricevi un messaggio cortese e simpatico che dice *controlla la posta e scopri come accedere*.

Controllo la posta: *condividi il link taldeitali, facci conoscere sui social, oppure prendi un appuntamento di trenta minuti con Tizia e ti diamo accesso*.

Nulla è gratis e ci mancherebbe, la visibilità sui social e in rete è un valore, giusto chiedere un favore in cambio di un favore.

Esigerlo, il favore, senza avere reso le cose chiare dall’inizio, è un pessimo modo per iniziare una conoscenza.

Questa volta la curiosità è stata punita con la pretenziosità. Non intendo rinunciare alla curiosità, che è uno dei segreti per una vita non importa quanto lunga, certo movimentata; di sicuro rinuncio a Kosmik. Spazio a chi ne voglia.