---
title: "modello-per-genitori"
date: 2014-11-30
comments: true
tags: [ArsTechnica, Yosemite]
---
I giochi per bambini che si ispirano al mondo degli adulti sono di due tipi: quelli che fanno il verso agli oggetti dei grandi (tipo il finto cellulare con i tasti che suonano oppure la spada laser di Guerre Stellari) e quelli propedeutici a un uso più serio o a una professione (il proverbiale piccolo chimico).<!--more-->

Non riesco a collocare [Happy Tab](http://www.chicco.it/prodotti/8058664032686.happy-tab.giocattoli.giochi-elettronici-e-musicali.html) di Chicco.

Se facesse il verso ai tablet veri, sarebbe un giocattolo, con un finto sistema operativo, icone che fanno il verso degli animali, luci giocattolo qua e là, suoni a sorpresa, l’armamentario tipico. Invece Chicco presenta Happy Tab come *un vero tablet con caratteristiche tecniche avanzate*. È una bugia (solo un dettaglio: schermo 1.024 x 600) ma non è questo il punto. È comunque un apparecchio vero, con sistema operativo Android, 26 applicazioni *pre-istallate* (direi nessuna dedicata all’ortografia) e altre quarantamila, si legge, su un *app store* dedicato.

Non fa il verso. Quindi è propedeutico? No. Che cosa dovrebbe insegnare? A usare una tavoletta? I bambini imparano da soli, fin troppo bene. Uno dirà: serve a lanciare *app* didattiche di quelle che impariamo giocando. Ok, ma queste sono presenti a stramilioni sugli *store* normali e si possono lanciare su tavolette normali. Le quali si prestano particolarmente alla collaborazione tra genitore e figlio: uno riprende del video in famiglia, l’altro fa vedere come montarlo, si aggiunge del testo ed ecco pronto un messaggio strappacuore per mamma o papà. Vorrei vedere fino a che punto Happy Tab permette queste cose. Rimanendo nell’esempio, le due fotocamere integrate sarebbero state di qualità nel 2007.

Alla fine siamo alle solite: il vero valore aggiunto è che i bambini non possono accedere a Internet (ma i genitori sì) e che si può limitare il tempo di utilizzo. Happy Tab è un parcheggio per bambini di genitori che non possono o non vogliono occuparsene.

O che credono di liberarsi dalle responsabilità comprando una tavoletta per bambini. Ahimé, questo è un modello *per genitori*.