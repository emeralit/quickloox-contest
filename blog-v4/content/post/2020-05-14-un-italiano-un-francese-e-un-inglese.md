---
title: "Un italiano, un francese e un inglese"
date: 2020-05-14
comments: true
tags: [Immuni, Nhs, Inria]
---
I francesi hanno pensato a una app di tracciamento contatti anticoronavirus, StopCovid. Hanno pubblicato [parti del codice sorgente su GitLab](https://gitlab.inria.fr/stopcovid19).

Gli inglesi hanno [pubblicato su GitHub](https://github.com/NHSX) il codice sorgente della loro app di tracciamento contatti anticoronavirus, in versione beta.

Gli italiani hanno scelto la app di tracciamento contatti anticoronavirus, [Immuni](https://www.agendadigitale.eu/cultura-digitale/immuni-come-funziona-lapp-italiana-contro-il-coronavirus/). Nelle barzellette sono i più furbi di tutti.