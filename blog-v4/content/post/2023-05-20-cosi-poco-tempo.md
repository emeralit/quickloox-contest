---
title: "Così poco tempo"
date: 2023-05-20T00:12:12+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [emacs, Reference Card]
---
Mi tocca andare sui social per lavoro e a volte mi trattengo più del necessario.

Tutte le volte che succede, se esistesse veramente l’intelligenza artificiale, dovrebbe arrivarmi un richiamo ufficiale e io dovrei passare un segmento di tempo successivo a imparare una riga in più della [Reference Card di emacs](http://www.damtp.cam.ac.uk/user/sje30/ess11/resources/emacs-refcard.pdf).

Serve molto di più.