---
title: "Una scelta divisiva"
date: 2022-01-31T00:26:15+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Appena finito di scrivere di [Monterey 12.3 che pensionerà Python 2](https://macintelligence.org/posts/2022-01-30-tre-is-megl-che-due/) con conseguenze variegate ma importanti ed ecco che salta fuori un *fork*, cioè una versione alternativa, di youtube-dl, [yt-dlp](https://github.com/yt-dlp/yt-dlp).

[youtube-dl](https://youtube-dl.org), per chi non sapesse, scarica audio e video da YouTube e un altro migliaio (!) di siti contenenti video. Il suo impiego è di ogni genere, da chi ha terabyte da vendere e cerca di riempirli con qualsiasi materiale filmico prodotto sulla faccia della Terra, a chi di tanto in tanto per ragioni di lavoro ha bisogno di prendere uno spezzone da un evento aziendale o da una presentazione. Il punto è che è usatissimo, un sacro Graal dello scaricamento video.

Il *fork* si deve proprio al fatto che il progetto originale è scritto in [Python](https://www.python.org) e il passaggio da Python 2 a Python 3 è delicato anche fuori dal mondo Mac.

Sarà anche un’occasione per apprezzare il valore di una interfaccia di alto livello come quella di macOS, che maschera il più possibile situazioni traumatiche come questa. Per la maggior parte delle persone gli aggiornamenti di sistema e applicazioni sono eventi poco significativi e normalmente trasparenti. Ma l’informatica è tutt’altro che questo e fuori dalle interfacce di tutt’altro livello la musica è un’altra, Unix in primo luogo.
