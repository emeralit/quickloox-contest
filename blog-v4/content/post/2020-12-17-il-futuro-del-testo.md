---
title: "Il futuro del testo"
date: 2020-12-17
comments: true
tags: [Augmented, Reader, Author, Liquid]
---
È qualche giorno che cerco di capire il vero valore del lavoro di [Augmented](https://www.augmentedtext.info). L’idea del *futuro del testo* è affascinante eppure non riesco a convincermi del tutto.

In sostanza, hanno creato un word processor in grado di produrre Pdf ricercabili e manipolabili come non ho visto fare a nessun altro, in termini di ricerca e modifiche.

Author, il word processor, facilita al massimo il lavoro sulle bibliografie e le citazioni, espande e nasconde sezioni di testo; Reader, il lettore Pdf, realizza la magia per chi consulta il lavoro realizzato. Interessante anche di per sé è Liquid, una barra di ricerca che promette di velocizzare enormemente l’attività di ricerca delle fonti, traduzione di termini, recupero di etimologie eccetera.

Con una quindicina di euro ci si porta a casa il sistema completo per Mac e lo avrei già fatto. Qui cominciano però le mie perplessità perché per esempio non vedo niente per iOS e tutto mi sembra molto lontano dal testo puro, che è la direzione in cui mi sono trovato a dirigermi costantemente dai tempi di AppleWorks e non per scelta ideologica o presa di posizione, quanto per naturale conseguenza.

Mi domando anche quanto abbia senso un lavoro come questo per luoghi come la comunità scientifica, dove però chi ha un minimo di cervello opta per LaTeX e l’ecosistema relativo degli strumenti con cui realizzare le stesse cose, forse con meno magia ma certamente con infinito controllo in più.

O forse le mie esigenze non sono quelle di un ricercatore e stare su Markdown, Html, testo marcato in generale sono il risultato della ricerca [per via di levare](https://macintelligence.org/blog/2015/03/23/per-via-di-levare/) che ha spogliato il mio lavoro con il testo di sovrastrutture inutili.

Oppure sono solo bisognoso di vacanze di Natale e [Neverwinter Nights](https://apps.apple.com/it/app/neverwinter-nights/id1466096721).