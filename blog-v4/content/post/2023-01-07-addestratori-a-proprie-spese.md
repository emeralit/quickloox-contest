---
title: "Addestratori a proprie spese"
date: 2023-01-07T00:13:49+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Adobe, Creative Cloud, Photoshop, TechCrunch]
---
Uno dei perché il 2023 potrebbe essere un buonissimo anno per liberarsi il più possibile del peso di Adobe: l’azienda, dice che no, però forse sì, non è chiaro, cioè qualcosa succede, [potrebbe utilizzare il contenuto realizzato con Creative Cloud per addestrare i suoi algoritmi di supposta intelligenza artificiale](https://techcrunch.com/2023/01/06/is-adobe-using-your-photos-to-train-its-ai-its-complicated/).

L’articolo di *TechCrunch* non è definitivo, perché la posizione di Adobe è, diciamo, sfumata. Ufficialmente l’azienda analizza i contenuti per aiutare l’utente, per esempio aiutandolo a taggare più in fretta del foto del gatto grazie al fatto che ha imparato a riconoscerlo. Niente di speciale, sono cose che fa anche Foto su Mac, non c’è nulla di male.

Ma, per caso, Adobe usa i contenuti di Creative Cloud per *addestrare i suoi modelli di machine learning*? Le cose qui si fanno più birichine. La documentazione afferma che no, i contenuti non vengono usati per addestrare le funzioni sperimentali di intelligenza artificiale generativa, che per capirci sono simili a quelle di [Stable Diffusion](https://macintelligence.org/posts/2022-12-04-farsi-aiutare/).

La stessa documentazione afferma però che sì, i contenuti vengono usati per addestrare algoritmi. Semplicemente, altri che non sono la funzione generativa sperimentale.

Già l’ambiguità del linguaggio consiglia di girare al largo più che si può.

Ma c’è di più: esiste una iniziativa apposita per Photoshop, Adobe Photoshop Improvement Program, in cui tutti gli utilizzatori di Photoshop si ritrovano a meno che si siano chiamati esplicitamente fuori. Naturalmente non è che Adobe li abbia avvisati in chiaro con un box a tutto schermo e caratteri cubitali.

*TechCrunch* consiglia, come minimo, di uscire da questa iniziativa, se si tiene giusto quel tanto alla privacy e soprattutto se non si gradisce che il proprio lavoro venga usato gratis (anzi: pagando fior di canone) per contribuire a strategie interne di Adobe del tutto avulse dal supporto e dall’interesse dell’utilizzatore.

Io consiglio di togliersi appena possibile, se appena è possibile, anche con sforzo, anche con fatica. Il costo di rifarsi una vita fuori da Adobe è nettamente inferiore a quello di rimanervi incastrati dentro.

Se è impossibile è un’altra questione. Ma almeno, ci si prenda un appunto: faticare per trovare le attività di addestratori a proprie spese nascoste in Creative Cloud ed eliminarle.