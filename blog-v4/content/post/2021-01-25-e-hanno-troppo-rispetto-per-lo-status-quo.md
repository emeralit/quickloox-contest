---
title: "E hanno troppo rispetto per lo status quo"
date: 2021-01-25T00:55:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Steve Jobs, Think Different, Dalian, Flash, AppleDaily, Adobe, China Railway Shenyang, Macintosh, Thoughts on Flash, It is better to be a pirate than join the Navy] 
---
Perché usare brutto software imbruttisce.

Dopo anni e anni di logoramento, finalmente Adobe [ha staccato la spina a Flash](https://www.theverge.com/2020/12/31/22208190/adobe-flash-is-dead). Hanno cominciato a farlo cinque anni dopo che Steve Jobs aveva pubblicato i suoi [Thoughts on Flash](https://newslang.ch/wordpress/wp-content/uploads/2020/06/Thoughts-on-Flash.pdf) e ci hanno messo altri cinque anni. La malapianta aveva messo radici fin troppo diffuse e ramificate.

Ora finalmente Flash, a parte la doverosa [conservazione museale](https://www.theverge.com/2020/11/19/21578616/internet-archive-preservation-flash-animations-games-adobe), è morto.

Invece no.

A me verrebbe da non crederci e non conosco il [cinese](https://hk.appledaily.com/china/20210116/VXRBXZVXNFCS3GFUATBVRUHBGA/), ma non capisco il perché dovrebbe essere scritta una *fake news* a questo proposito e così prendo per buona la versione inglese di questo articolo di *AppleDaily* secondo il quale [una rete ferroviaria cinese veniva controllata da un sistema scritto in Flash](https://hk.appledaily.com/news/20210117/FLXATT4LKVBGVEBRLAECJPTCHM/).

I funzionari di China Railway Shenyang (che esiste, [almeno in Wikipedia](https://en.wikipedia.org/wiki/China_Railway_Shenyang_Group); dovevo verificare, per forza) hanno avuto cinque anni di tempo per passare a qualche altro sistema e così, quando Adobe ha spento anche l’ultimo server, c’è stata una transizione, a suo modo, ordinata: *il traffico ferroviario si è bloccato per venti ore*.

Colpa di Adobe, certo. Come quando aggiorni macOS e il driver della stampante smette di funzionare: colpa di Apple.

Si sono riscattati in fretta, comunque: hanno trovato il modo di rimettere in piedi l’infrastruttura.

*Con una copia pirata di Flash*. Che così vivrà, in una nicchia ecologica tutta particolare, per anni ancora.

Steve Jobs aveva dichiarato ai tempi della creazione di Macintosh [meglio essere un pirata che arruolarsi in Marina](https://medium.com/thethursdaythought/cassandra-communication-in-innovation-aef5be92a730).

Poi, però, con la campagna [Think Different](http://www.thecrazyones.it/spot-en.html), aveva anche posizionato Apple come un marchio per chi *non ha rispetto per lo status quo*.

Il software brutto imbruttisce. Il sintomo più pericoloso e invalidante per la mente è cercare la conservazione dello status quo, oltre ogni limite di ragionevolezza.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*