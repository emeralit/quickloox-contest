---
title: "Una giornata, due estremi"
date: 2017-05-15
comments: true
tags: [Trenord, Foto]
---
Una giornata così, iniziata con gente che si impegna a mettere l’apostrofo sbagliato (persino peggio di quelli che usano l’apice).<!--more-->

 ![Un apostrofo che grida vendetta](/images/trenord.png  "Un apostrofo che grida vendetta") 

E finisce con Foto, tutto contento di sincronizzare un iPhone pieno di immagini appena scattate, tanto da impegnarsi il doppio.

 ![Una funzione di importazione di troppo](/images/photos.png  "Una funzione di importazione di troppo") 