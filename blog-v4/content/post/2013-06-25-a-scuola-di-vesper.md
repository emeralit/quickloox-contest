---
title: "A scuola di Vesper"
date: 2013-06-25
comments: true
tags: [Interfacce, Design]
---
Una eventuale ragione per comprare [Vesper](https://itunes.apple.com/it/app/vesper/id655895325?l=en&mt=8) potrebbe essere, certo, disporre su iPhone di un bel sistema di immagazzinamento e prioritizzazione di note testuali o miste.

Quella vera è che gli autori della *app* hanno raccontato con dovizia di particolari come è proceduto lo sviluppo: una vera serie di lezioni da inserire nel master di progettazione delle applicazioni.<!--more-->

Per esempio la [parte sul design](http://vesperapp.co/blog/how-to-make-a-vesper/).

>Ogni interazione, pixel e riga di codice è stata attentamente considerata, e nessuna cosa fatta era troppo preziosa per non essere gettata via [se inadeguata].

Le citazioni dovrebbero essere decine. Solo un’altra:

>John [Gruber] odiava la sfumatura [di arancione] che avevo scelto e ha passato letteralmente giorni a iterare fino a trovare esattamente l’arancione giusto per Vesper.

Alla fine del *post* si vede una schermata di icone candidate all’applicazione. L’idea suggerita da Gruber è che creare Vesper avrebbe dovuto somigliare più a girare un film che a scrivere una *app*. Ci hanno scherzato sopra fino a creare una locandina.

È una storia che dà un sacco da imparare.