---
title: "Terminale e gregoriano"
date: 2017-06-23
comments: true
tags: [Jobs, Logic, Terminale]
---
Quella cosa che ha detto Steve Jobs sullo stare all’intersezione tra la tecnologia e le arti liberali è probabilmente la più alta che ci ha lasciato. Spiegarla è però divenuto un problema perché se ne sono impadroniti i reparti marketing e il giornalismo trombone finto entusiasta tecnologico, che la ficcano dappertutto e la sviliscono.

Non provo quindi a rispiegarla per l’ennesima volta. Faccio parlare [Classic Music Reimagined](http://classicalmusicreimagined.com/2016/04/22/classical-music-tunes-in-voices-mac-terminal/). Partono dal banale uso del Terminale per richiamare la sintesi vocale di Mac per arrivare alle voci musicali, che cantilenano la frase da pronunciare in base a una melodia nota.

Al che decidono di adattare la voce emessa attraverso il Terminale al canto gregoriano. Sfoderano Logic Pro e cominciano a divertirsi.

Tutto semplice una volta che hai sottomano Mac, il Terminale, Logic Pro (o equipollente) e la conoscenza tanto della musica quanto almeno dell’esistenza del canto gregoriano.

Tutto semplice, se ti trovi all’intersezione eccetera eccetera. Trovarcisi è cosa rara e privilegiata.

<iframe width="560" height="315" src="https://www.youtube.com/embed/SFvmdAoiRt8" frameborder="0" allowfullscreen></iframe>