---
title: "Omaggio alla Catalogna"
date: 2016-02-25
comments: true
tags: [Catalogna, Elio, Mwc, mobile, Prince, iPhone, HP, X3, Elite, Atrix, Lumia]
---
Oggi termina il [Mobile World Congress](https://www.mobileworldcongress.com) edizione 2016, la fiera più surreale del mondo, dedicata a un mondo che deve il suo assetto odierno ad Apple, che vi svolge tuttora un ruolo di primissimo piano… e si guarda bene dal mettere piede a Barcellona, sede storica della manifestazione.<!--more-->

Si capisce perché grazie ad articoli come [Gli apparecchi un tempo noti come smartphone](https://techpinions.com/the-devices-formerly-known-as-smartphones/43768), che nel titolo fanno il verso a [Prince](https://www.discogs.com/artist/293637-The-Artist-Formerly-Known-As-Prince) mentre fanno l’effetto involontario di una delle genialità demenziali di [Elio e le Storie Tese](https://www.youtube.com/watch?v=xTlngoRaGQc). Leggo, anno duemilasedici:

>In questo contesto, pensare a uno smartphone più come a un apparecchio per il computing che a uno strumento di comunicazione sembra incredibilmente ovvio.

Ci vuole proprio il computing, per tenere il conto di quante volte ho discusso e magari litigato per chiamarli *computer da tasca*, invece che *cellulari*. Sono quasi dieci anni.

L’autore dichiara il proprio interesse per il nuovo modello Elite X3 di HP. La ragione è questa:

>Sui [meno recenti] Atrix e Lumia 950, le funzioni di computing costituivano aggiunte a posteriori a uno smartphone esistente. X3 sembra posizionarsi ed essere progettato primariamente come un computer, con le funzionalità da smartphone essenzialmente incorporate.

Alzi la mano ora chi sia in grado di nominare un computer da tasca progettato primariamente come un computer, nato nel 2007. Duemilasette.

Si spiega perché Apple diserta serenamente, da sempre, il Mobile World Congress. Si fanno discorsi vecchi di anni.

Cent’anni fa si diceva, con espressione brutta e però (allora) non del tutto infondata, *vengono giù dai monti*. Adesso si riuniscono in Catalogna.

<iframe width="420" height="315" src="https://www.youtube.com/embed/xTlngoRaGQc" frameborder="0" allowfullscreen></iframe>