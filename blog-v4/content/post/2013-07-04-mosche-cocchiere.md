---
title: "Mosche cocchiere"
date: 2013-07-04T00:31:28+01:00
draft: false
toc: false
comments: true
categories: [Internet, Hardware]
tags: [Which, Altroconsumo, iPhone, Geekbench]
---
**Stefano** mi ha messo sulle tracce di un articolo di *Melablog* dal titolo [iPhone 5, lo smartphone più lento del mercato secondo ‘Which?’](http://www.melablog.it/post/106429/iphone-5-lo-smartphone-piu-lento-del-mercato-secondo-which).<!--more-->

L’autore parla del *noto magazine inglese Which?*, che non ho mai sentito nominare. Certamente sono io che manco di cultura nel campo. Poi si lancia in un memorabile

>spesso definitivo l’Altroconsumo inglese

Però alla fine riesce ad arrivarci:

>Chi usa sistemi Apple non ha mai badato più di tanto a prestazioni, specifiche tecniche e benchmark.

Roba che sui computer da tasca ha importanza tendente a sottozero. Che se ne fa un Galaxy S4 di tutta quella potenza di elaborazione, per essere usato da gente che [non va su Internet](https://macintelligence.org/posts/2011-11-27-esseri-inutili)?

A leggere [l’articolo di Which?](http://blogs.which.co.uk/technology/apple/samsung-galaxy-s4-iphone-5-uk-fastest-phone/) si scopre che hanno fatto girare [Geekbench](http://www.primatelabs.com/geekbench/) su tutti i sistemi e morta lì. A proposito di Htc One, che ottiene il secondo posto in questo classificone, scrivono

>[le sue specifiche] forniranno potenza in abbondanza ai proprietari che usano Gmail, Facebook, Instagram e Angry Birds nello stesso momento.

Dove si vede come si va a finire con la cultura della specifica tecnica. Io che tengo [Goodreader](https://itunes.apple.com/it/app/goodreader-for-iphone/id306277111?l=en&mt=8) sulla prima schermata, devo sentirmi un alieno, un secchione o uno che si perde le grandi occasioni di utilizzo dello *smartphone*?

Sì, somigliano a quelli di Altroconsumo. [Quelli dei detersivi](https://macintelligence.org/posts/2011-11-03-quelli-dei-detersivi). Mosche cocchiere che vivono del galoppo altrui.