---
title: "Un presente infinito"
date: 2024-03-27T00:00:08+01:00
draft: false
toc: false
comments: true
categories: [Internet, Web]
tags: [X, Twitter, Orwell, Harvard]
---
Perché affaticarsi con i generatori di testo, quando Internet crea da sola i post per te?

[Dalla timeline di X/Twitter](https://twitter.com/sfmcguire79/status/1772660089335202265):

§§§

La facoltà di storia di Harvard chiedeva agli studenti di frequentare un corso introduttivo di due semestri sulla storia europea, come corso di ingresso, ma questo requisito è stato eliminato nel 2006.

Il nuovo corso introduttivo della facoltà si chiama ”Storia 10: una storia del presente”.

Si focalizzerà su temi come ”memoria” e ”genealogia”.

Il corso ”vuole insegnare agli studenti a pensare come uno storico”.

Che cosa significa? ”Empatia”, ma anche: ”Una delle regole fondamentali della storia… è che non c’è una risposta giusta, né c’è una risposta sola”.

La classe sarà anche dinamica, vale a dire che ogni mercoledì i professori scorreranno i titoli dei giornali e occasionalmente chiederanno agli studenti come terrebbero il corso!

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Harvard’s history department has created a new introductory course, and it sounds TERRIBLE.<br><br>The department used to require students to take a two-semester introduction to European history as a gateway course, but that requirement was cancelled in 2006.<br><br>“The idea that we would… <a href="https://t.co/aD5defQxxA">pic.twitter.com/aD5defQxxA</a></p>&mdash; Steve McGuire (@sfmcguire79) <a href="https://twitter.com/sfmcguire79/status/1772660089335202265?ref_src=twsrc%5Etfw">March 26, 2024</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

§§§

Poco sotto, [la risposta](https://twitter.com/willmurphy/status/1772665988313407950):

”La storia si è fermata. Niente esiste fuori da un presente infinito nel quale il Partito ha sempre ragione”. — 1984

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">&quot;History has stopped. Nothing exists except an endless present in which the Party is always right.&quot; - 1984</p>&mdash; Will Murphy (@willmurphy) <a href="https://twitter.com/willmurphy/status/1772665988313407950?ref_src=twsrc%5Etfw">March 26, 2024</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Non c’è altro da aggiungere. Se non che, mai avessi avuto i capitali per fare studiare qualcuno a Harvard, li avrei investiti in un fondo a scadenza 2100, quando finalmente una generazione di imbelli sarà stata cancellata e sarà arrivata una qualche nemesi.