---
title: "Industria che vai"
date: 2014-09-06
comments: true
tags: [Feld, Volk, iPhone]
---
Una delle componenti che hanno fatto la fortuna di iPhone e iPad sono la scarsissima varietà dell’offerta e la sua accessibilità universale, o quasi. L’operaio e il capitano di industria mirano allo stesso iPad e, al massimo, ci può essere una differenza nella Ram o nella presenza dell’alloggiamento della Sim. Poi, uguaglianza.<!--more-->

È nata allora tutta una industria delle custodie e degli accessori. Se il mio iPad è uguale a quello di tutti gli altri, che almeno sia rivestito in cuoio pregiato invece di neoprene.

Ma nessuno, per quanto ne so, aveva avuto l’idea di [Feld & Volk](http://www.feldvolk.com/en/): sostituire parti di iPhone con altrettante parti di lusso, dall’impiego di oro in poi, fino a creare oggetti praticamente unici che vengono venduti a migliaia di dollari.

Così adesso iPhone è l’oggetto più trasversale che si possa avere. E pure il più esclusivo.