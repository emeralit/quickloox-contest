---
title: "Come scegliere il monitor giusto"
date: 2021-04-20T00:39:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Amazon, Mediaworld] 
---
È il momento giusto per aggiornare lo schermo di Mac mini e passare a una vista desktop più spaziosa.

Amazon propone una milionata di alternative ma mi viene uno scrupolo e do anche un’occhiata sul sito di Mediaworld. L’ultima volta, [quando abbiamo comprato la stampante laser](https://www.macintelligence.org/posts/La-stampante-che-non-cè.html) per le attività scolastiche di Lidia, è emerso dopo pochi giorni che su Mediaworld avremmo risparmiato qualche euro rispetto ad Amazon. Niente che valga la perdita del sonno, però almeno togliersi la curiosità.

Anche Mediaworld propone una scelta ampia. Trovo lo stesso monitor che avevo visto su Amazon e anche il prezzo è uguale. Dai, compriamo su Mediaworld e vediamo che cosa succede.

Succede che faccio per accedere al sito e vengo informato che il mio account è valido, ma non è stato confermato. Avrei dovuto validare il mio account con la risposta alla classica email di verifica che viene spedita.

Il problema è che la cosa data a diversi anni fa. Cerco nella pagina l’opzione per farmi inviare una nuova email di verifica. Non c’è. Cerco la l’email di verifica nell’archivio. Non c’è.

Idea: se racconto di essermi dimenticato la password, potrò cambiarla e bypassare la validazione.

Eseguo e ricevo una password provvisoria per l’accesso al sito. Perfetto.

Accedo con la password provvisoria e mi viene detto che non ho ancora confermato l’account.

A questo punto, se hai seguito con attenzione, dovresti essere in grado di rispondere a una semplice domanda: su che sito ho ordinato il nuovo monitor?

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*