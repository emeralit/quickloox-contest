---
title: "L’enigma dell’ora"
date: 2016-09-04
comments: true
tags: [Nissan, Qashqai]
---
Il meccanico mi ha fornito un’auto di cortesia intanto che si occupa del mio ferrovecchio e si tratta di una bellissima e fiammante [Qashqai Nissan](https://www.nissan.it/veicoli/veicoli-nuovi/qashqai.html).<!--more-->

L’auto contiene una marea di elettronica rispetto a quella che sono abituato a guidare e sono rimasto piacevolmente sorpreso dal *design* dell’interfaccia. Moltissimo è, scusate il bisticcio, autoevidente, molto è intuitivo. La leggibilità è ottima, la chiarezza dei messaggi è soddisfacente. In nessun caso ho trovato difficoltà e, quando ho dovuto cercare qualcosa, mai è stata violata la [regola del minuto](https://macintelligence.org/posts/2005-09-29-la-regola-del-minuto/), che nel mondo automobilistico dove i tempi di reazione contano, direi equivale a tre secondi. Sono molto soddisfatto dell’auto di cortesia e grato all’officina.

Una sola cosa. L’orologio. Non era impostato sull’ora giusta e per scoprire dove intervenire, c’è voluto anche l’aiuto di Internet.

I nei aggiungono bellezza, intendiamoci, ed è stato pure divertente approfondire la conoscenza dell’interfaccia. Però dovrebbe bastare la prima sosta al semaforo per individuare la soluzione a un problema di questo tipo.