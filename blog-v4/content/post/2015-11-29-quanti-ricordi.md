---
title: "Quanti ricordi"
date: 2015-11-29
comments: true
tags: [AAA, iPad, NeXT, Lisa]
---
Ho fatto [quello che ho potuto](https://macintelligence.org/posts/2015-11-27-dovere-e-potere/): una veloce comparsata all’inaugurazione del museo Apple più bello del mondo.

L’importante era salutare gli amici. Missione compiuta, poi il rientro per ragioni di salute (niente di che, mal di gola, ma da domani devo essere in perfetta efficienza).

Poi le macchine erano accese ed è quello che mi piace, il museo che si dimentica di essere tale e diventa presente vivo. Non tutte, con poche cose interessanti a livello software oltre al sistema operativo, e però la versione 1.0 del nuovo All About Apple merita completamente la visita.

Nello spazio molto ampio antistante la sede è stata organizzata una sessione di musica e mi va di ricordare questo particolare perché mostra che sì, il museo, l’Apple I, NeXT, Lisa, ma non ci si aspettino polvere, ragnatele, nostalgie senili. È una iniziativa che vive in questo tempo e anzi è di [stringente attualità](https://macintelligence.org/posts/2015-10-25-post-pre-inaugurazione/).

 ![Un iPad per spartito](/images/ipad.jpg  "Un iPad per spartito") 