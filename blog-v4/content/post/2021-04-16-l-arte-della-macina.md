---
title: "L’arte della macina"
date: 2021-04-16T02:18:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Scripting, Jacob Kaplan-Moss, Penn, Teller] 
---
[Mi sono ricordato](https://macintelligence.org/posts/Programmare-per-dimenticare.html). Le tre virtù del programmatore, il codice come soluzione per entrare in uno stato mentale più elevato.

Poi però, serve una dote speciale ed eminentemente umana: la disposizione a macinare. [Ne scrive Jacob Kaplan-Moss](https://jacobian.org/2021/apr/7/embrace-the-grind/):

>A volte la programmazione somiglia alla magia: declami un qualche incantamento arcano e una flotta di robot ti obbedisce. A volte, tuttavia, la vera magia è quella di tutti i giorni. Se sei disposto ad apprendere l’arte della macina, puoi ottenere l’impossibile.

Ad *adottare l’arte della macina* corrisponde l’originale *embrace the grind*: accettare il fatto che per risolvere una situazione o portare a termine un lavoro si può solo dedicare un tempo e un impegno che chiunque altro nemmeno riuscirebbe a concepire.

L’ultima frase riporta alla magia, ma quella dei prestigiatori. Molti trucchi si basano su una preparazione meticolosa che può durare per settimane, talmente lontana dalla vita reale che difatti gli spettatori neanche iniziano a pensare. Si meravigliano per il trucco, che sarebbe evidentissimo… se solo fossero disposti mentalmente a contemplare il fatto che quel trucco di un minuto abbia richiesto settimane di lavoro.

Non svelo il trucco (niente di che) raccontato nell’articolo di Kaplan-Moss, il quale però cita un passo di un [articolo di Teller](https://www.smithsonianmag.com/arts-culture/teller-reveals-his-secrets-100744801/), mitico prestigiatore americano che ha fatto epoca a Las Vegas negli spettacoli condotti [assieme al suo compare Penn](https://en.wikipedia.org/wiki/Penn_%26_Teller):

>Un trucco che implica più tempo, denaro ed esercizio di quanto saremmo disposti a investire, ci ingannerà (assieme a numerose altre persone sanissime). Penn e io una volta facemmo uscire cinquecento scarafaggi vivi da un cilindro sulla scrivania di David Letterman. La preparazione del trucco richiese settimane. Assoldammo un entomologo che ci fornì scarafaggi dal moto lento, non eccitati dalle luci di scena, e ci insegnò a raccoglierli senza urlare come preadolescenti. Poi costruimmo un comparto segreto in [anima in schiuma](https://www.nauticexpo.it/fabbricante-barca/materiale-anima-schiuma-24482.html), materiale cui gli scarafaggi non riescono ad attaccarsi, ed elaborammo un trucco subdolo per inserire di nascosto il comparto dentro il cilindro. Più fatica di quanto valesse il trucco? Per una persona qualsiasi, probabilmente sì. Non per due maghi.

Questa è l’arte della macina: ci sono situazioni che neanche l’automazione, lo scripting, il machine learning può risparmiarci. Chi sia disposto ad affrontarle ugualmente con il giusto approccio, disporrà di una marcia in più e magari troverà più occasioni in tempi difficili.

Perché tutto questo? Perché [BBEdit ha compiuto gli anni di recente](https://macintelligence.org/posts/Divino-e-terreno.html). Se esiste un programma disposto ad assisterti quando ti tocca macinare, che non può abbreviare la pena, ma può darti il meglio per affrontarla e renderla persino piacevole, quello è BBEdit.

E poi esiste Internet, un luogo dove si può viaggiare tra editor di testo, automazioni individuali, giochi di prestigio, per compiere viaggi inaspettati. Ci tocca macinare, ma può valerne la pena.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*