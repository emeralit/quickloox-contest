---
title: "La paura fa X"
date: 2017-11-16
comments: true
tags: [iPhone, X, Face, ID]
---
Per quanto sia sempre accaduto, specialmente negli ultimi anni, [l’intento di scoraggiare l’acquisto o almeno falsare la percezione](https://macintelligence.org/posts/2017-11-03-profondita-zero/) di iPhone X viene perseguito con una intensità che non avevo ancora visto, salvo i tempi dell’<a href="https://macintelligence.org/posts/2010-07-19-la-presa-di-sale/">antennagate</a>. Dove però, almeno, c‘era qualcosa di cui parlare.

Qui invece si può solo inventare. Allora saltano fuori storie come la <a href="https://www.wired.com/story/hackers-say-broke-face-id-security/">maschera di silicone che sblocca Face ID</a>. In sé è una sciocchezza: dati il profilo 3D già pronto di una persona consenziente, centocinquanta dollari di materiali, cinque tentativi e finalmente l’annuncio (che nessuno ha verificato o riprodotto fuori dal laboratorio vietnamita, ma diamolo per buono). Certo, vale la pena di sequestrare un milionario è sottoporlo alla scansione 3D del volto per sbloccare il suo iPhone X. O tagliargli la testa; dopotutto [sono già state tagliate dita per trafugare impronte digitali](http://news.bbc.co.uk/2/hi/asia-pacific/4396831.stm).

Per tutti gli altri, che non sono milionari, non sono agenti segreti, non sono ministri, non sono amministratori delegati di multinazionali…? Non ne vale la pena e casca l’asino. Posto che Face ID sia stato violato, nessun meccanismo di sicurezza è inviolabile in assoluto. Bisogna invece chiedersi quanto è più efficace rispetto al meccanismo che c’era prima.

Violare Touch ID [era molto più semplice](https://www.cnet.com/news/iphone-5s-touch-id-hacked-by-fake-fingerprints/), a tempi e costi assai inferiori. Face ID è un progresso, quindi, e non stiamo neanche prendendo in considerazione la sua capacità di migliorare: più passa il tempo, più diventa preciso. Nessuno può dirci se la maschera di silicone funziona meglio o peggio dopo tre mesi di affinamento del profilo 3D. Touch ID, all’opposto, funziona l’ultimo giorno esattamente come il primo.

Quindi, anche il fatto che iPhone X sia appena arrivato – neanche due settimane – va messo in conto.

E proprio per questo, un’altra tattica di disinformazione consiste nello spostare da qualche altra parte le conversazioni. Appunto, neanche due settimane dall’uscita e c’è già l’analista che [pretende di conoscere le uscite iPhone del 2018](https://www.macrumors.com/2017/11/13/kuo-three-new-iphones-2018/).

[Effetto Osborne](http://www.zdnet.com/pictures/osborne-effects-death-by-pre-announcement/4/) da manuale: ti annuncio quello che arriverà per insinuare il dubbio e scoraggiare un eventuale acquisto negli indecisi. Serve a qualcosa ribadire che i rendering sono disegni iperrealistici e non realtà fisiche? È utile ricordare che le specifiche sono inventate?

Certamente no. In parte perché Internet è il luogo dove si parla di verità e balle nello stesso modo da parte delle stesse persone. Battaglia persa. In parte perché iPhone X, ho la sensazione, se la sta cavando bene nonostante le chiacchiere.

Il che, chiaro, mette ancora più ansia a chi ha bisogno di produrne.