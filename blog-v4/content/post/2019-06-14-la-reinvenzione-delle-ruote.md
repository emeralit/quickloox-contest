---
title: "La reinvenzione delle ruote"
date: 2019-06-14
comments: true
tags: [Mac, Pro, Venkatesan, Cube, G4, MacBook, HomePod, Power, G5, Unibody]
---
I critici di Mac Pro che lamentano la mancanza di innovazione di Apple, la sua indifferenza verso i professionisti (un insieme che usualmente include loro, i loro colleghi e i loro amici), i Mac che non sono più quelli di una volta eccetera, ci hanno tenuto nascosto un terribile segreto. Impossibile pensare che, dall’alto della loro competenza, non se ne siano accorti. Volevano essere i soli a saperlo.

Invece arriva questo Arun Venkatesan, che titola [Il nuovo Mac Pro è un remix di design](https://www.arun.is/blog/mac-pro/). Poffarbacco, neanche è cosa originale.

La componentistica montata su un telaio, cui si può accedere da ogni lato levando un unico pezzo di copertura esterna, è un concetto già applicato in [Power Mac G4 Cube](https://www.macworld.com/article/1153341/cube-10thanniversary.html).

Il raffreddamento, ottenuto attraverso la creazione di zone termiche separate all’interno del computer, che vengono monitorate e amministrate indipendentemente, arriva da [Power Mac G5](https://support.apple.com/kb/sp96?locale=en_US).

Il famigerato stand da novecentonovantanove dollari sfoggia un ingegnoso giunto che consente di mantenere invariata l’angolazione mentre si cambia l’altezza dalla scrivania. Proprio come faceva [iMac G4](https://www.apple.com/support/imac/g4/index.html).

Ci sono evidenti e pericolosi paralleli ingegneristici tra la griglia di Mac Pro 2019 e le strutture [Unibody](https://appleinsider.com/articles/08/10/14/apple_details_new_macbook_manufacturing_process) che Apple ha iniziato a usare per MacBook Pro dal 2008.

Il cavo di alimentazione è rivestito di stoffa come quello di [HomePod](https://www.apple.com/homepod/).

L’estetica di tastiera, trackpad e mouse reinterpreta, combinandoli, gli stili e gli accostamenti di colore delle ultime annate di prodotto.

Più o meno, tutte le idee che sono centrali a Mac Pro 2019 erano già nate tra il 2001 e il 2008. Tutta roba vecchia e già vista. Però, aspetta; questi erano secondo loro gli anni dell’innovazione, dell’attenzione ai professionisti, del primato di Mac ed era persino vivo Steve Jobs. Gli stessi principî, oggi, non vanno più bene. Uhm.

A meno che il problema siano le ruote opzionali. Quelle, sì, Apple non le aveva mai reinventate.