---
title: "Il futuro dei miopi"
date: 2023-08-23T19:45:29+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Club di Roma, I limiti dello sviluppo, Peccei, Aurelio Peccei, Asimov, Isaac Asimov, Trilogia galattica, Seldon, Hari Seldon]
---
I negazionisti costano tempo, fatica e stress; gente impermeabile ai fatti che si rifugia per inadeguatezza in una realtà parallela.

Oggi ho capito che paghiamo un altro debito e in misura maggiore del percepito: i catastrofisti. E ho capito perché.

*Qualsiasi catastrofista ragiona entro un arco temporale massimo paragonabile alla vita umana.*

Purtroppo passare da persona o organizzazione responsabile che lancia un allarme a catastrofista nocivo è semplice: diramare previsioni sbagliate.

E guarda caso, le previsioni sono sbagliate sempre in quanto troppo vicine nel tempo.

Inabitabilità climatica, esaurimento delle risorse, pandemia, guerra nucleare, sovrappopolazione, asteroide, invasione aliena eccetera sono tutti pericoli potenziali da considerare. ma nessuno scrive che l’uranio, faccio per dire, finirà nel 2300 o che nel 2500 non ci sarà più acqua a sufficienza. Ed è strano; possibile che la nostra generazione sia così unica e speciale che vedrà le catastrofi irreversibili, al contrario di tutte quelle passate?

Uno dice, *non ci sono dati per prevedere così lontano*. In effetti l’unico che ci abbia provato, a livello di fiction, è Hari Seldon, il fondatore della fantascientifica psicostoria attorno alla quale ruota la [Trilogia galattica di Isaac Asimov](https://tuttatoscanalibri.com/narrativa/isaac-asimov-trilogia-della-fondazione/).

Il problema è che *non ci sono dati per prevedere così vicino*. Eppure le previsioni per il lasso di tempo di una vita umana abbondano. E gran parte di esse *sono state sbagliate*.

Uno dice, *si può solo prevedere con i dati che ci sono*. Ma se i dati non sono abbastanza, non fai previsioni; spari alla luna. O si è consapevoli che i dati non sono sufficienti e allora ci si ferma, oppure si lavora, si innova, si azzarda e si aprono nuove strade. Sarebbe un progresso sconvolgente, ogni volta. E invece.

Mi si darà del *crackpot* e lo accetto volentieri, ma tengo il punto; tutto è cominciato con [il Club di Roma e il suo rapporto sui *limiti dello sviluppo*](http://www.arteideologia.it/04-FORNITURE/Capitolazioni/Club%20di%20Roma%20-%20I%20limiti%20dello%20sviluppo%20(1072).pdf). Il messaggio era che le risorse terrestri sono in quantità finita e dunque uno sviluppo indefinito non è sostenibile. Quando l’ho letto mi è parso un truismo, un’ovvietà; forse però era un’idea nuova per la cultura dominante negli anni sessanta e allora la prendo per meritevole, sia pure con il beneficio del dubbio.

Se si fossero fermati qui, riconoscerei anch’io dei meriti a Peccei e compagni. Disgraziatamente hanno costellato il rapporto di grafici e tabelle con ipotesi di esaurimento di risorse primarie al ritmo dei consumi del tempo.

L’esempio più eclatante e ovvio, il petrolio, che stando alle curve sarebbe terminato tipo trent’anni dopo il rapporto. Sono passati i trent’anni, ne sono passati circa altri trenta, ne passeranno altri trenta come minimo. Una ipotesi sbagliata del duecento percento e forse di più, così sono capace anch’io. Sarai anche un pool di scienziati di rinomanza mondiale, ma siamo al livello di chi nello stesso periodo, con l’Italia che era sostanzialmente un Paese agricolo, ha progettato le autostrade a due corsie senza prevedere nel progetto di riservare lo spazio per la terza.

Se il Club di Roma avesse avuto la capacità di inserire nelle sue tabelle l’idea di nuove tecnologie, l’ipotesi della scoperta di nuovi giacimenti, di motori e procedimenti di raffinazione più efficienti, di una razionalizzazione dei consumi, della scoperta di giacimenti sulla Luna (oggi sembra una sparata, ma in quegli anni si era in piena corsa allo spazio e la fantascienza considerava il tema senza problemi) e avessero scritto *rischiamo di finire il petrolio entro centocinquant’anni*, per oggi sarebbero degli eroi.

Invece hanno scritto che sarebbe finito entro trent’anni, di fatto perché – anche se inconsciamente – il petrolio *doveva finire entro le aspettative di vita di chi scriveva il rapporto*.

La morale è: non fidarsi dei castrofisti a breve termine. Non fanno previsioni, semplicemente desiderano senza rendersene conto che il mondo se ne vada insieme a loro. Non succederà.

Volevo inserire una serie di altri esempi, ma si fa troppo lunga. Magari più avanti.

Che c’entra con il Mac? Quasi niente, a parte chi dava Apple per spacciata secondo la propria testa invece che secondo i dati, oppure riguardo alle stime di adozione di una tecnologia, o di diffusione di una piattaforma. La questione di avere previsioni realistiche è trasversale. Solo che del declino di Apple interessa a quattro gatti e invece se si tirano fuori le terre rare diventa una questione catastrofista che arriva pure sui quotidiani. Le terre rare sono rare, i cinesi giocano sporco, le tensioni sono alte… intanto però girano il mondo alcuni miliardi di apparecchi che usano le terre rare e le aziende intendono produrne altri. Anche questo dato andrebbe considerato.

*Gli aggiornamenti in agosto del blog potrebbero essere irregolari o infrequenti.*