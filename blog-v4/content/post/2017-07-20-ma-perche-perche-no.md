---
title: "Ma perché? Perché no!"
date: 2017-07-20
comments: true
tags: [iPad, Pro]
---
Rimane vivo il dibattito sulla capacità di iPad di sostituire il computer, tenendo presente che siamo vicini a quello sulla capacità del computer di sostituire iPad; Tuttavia *Motley Fool* alza con decisione l’asticella della sofisticazione dell’argomento.

Non è da tutti osare proporsi con un pezzo intitolato [Non comprerò il nuovo iPad Pro](https://www.fool.com/investing/2017/07/17/why-im-not-buying-the-new-apple-inc-ipad-pro.aspx) articolato su ben due argomenti:

* Quello del 2018 potrebbe essere più bello.
* Lo uso poco.

Come livello siamo a Jannacci: *vengo anch’io! No, tu no.*

Mi aspettavo di vedere qualcosa di ancora più radicale, tipo *non mi piace il colore* o anche *oggi è giovedì*, ma forse è solo questione di attendere qualche firma più audace.
