---
title: "Sei aperture"
date: 2015-03-25
comments: true
tags: [ LibreItalia, LibreOffice, FreeSmug, ViviScienza, Paolo, Morevna, Synfig, Blender, MyPaint, Remake, Gimp, Mac, Windows, Unix, Flash, Chrome]
---
Ho l’imperativo morale di ricordare di tanto in tanto che rientro nel comitato tecnico-scientifico di [LibreItalia](http://www.libreitalia.it) e sostenere l’associazione, nonché sostenere [LibreOffice](http://www.libreoffice.org) e di conseguenza il software libero (anche attraverso iniziative come [FreeSmug](http://www.freesmug.org), è una buona cosa per chiunque sia in ascolto.<!--more-->

Lo spunto di oggi arriva da [Paolo](http://viviscienza.it) e dalla sua segnalazione del *demo* filmico [The Beautiful Queen Marya Morevna](http://www.synfig.org/cms/en/gallery/shorts/the-beautiful-queen-marya-morevna-demo/). Interamente realizzato con software libero.

Per la precisione sono stati usati sei programmi: Synfig Studio, [Blender](http://www.blender.org), [Pencil](http://pencil.evolus.vn), [MyPaint](http://mypaint.intilinux.com/?page_id=6), [Remake](https://github.com/morevnaproject/remake) e [Gimp](http://www.gimp.org).

Tutto sontuosamente disponibile per Mac, a dispetto di chi ancora [pensa di trovarsi nel 1995](https://macintelligence.org/posts/2015-03-12-attivita-di-nicchia/). Anzi, per alcuni di questi programmi è più impegnativo funzionare su Windows, dove non esiste uno strato Unix nativo e bisogna installarlo appositamente.

Giusto una tiratina d’orecchie agli autori del sito perché hanno codificato il video in Flash (per guardarlo devo lanciare Chrome).
