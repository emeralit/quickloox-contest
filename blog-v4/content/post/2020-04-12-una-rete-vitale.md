---
title: "Una rete vitale"
date: 2020-04-12
comments: true
tags: [Asymco, Pasqua, iPhone]
---
Buona Pasqua per credenti, agnostici, atei: l’idea del passaggio, della ripartenza, di una possibilità nuova dovrebbe essere comprensibile a chiunque in un momento come l’attuale.

È una giornata di contatti e quasi tutti avverranno per via telematica. Le bimbe hanno preparato le uova di cartone decorate da mostrare ai nonni in videoconferenza; giusto ieri la primogenita è stata invitata a una videofesta di compleanno. La quotidianità è cambiata un bel po’ e chiunque aspira a tornare alla normalità. Gli unici a essere delusi saranno quelli che vorrebbero tornare a com’era prima; ci torneremo, ma sarà diverso.

Prendo a prestito la riflessione del giorno [da Horace Dediu](http://www.asymco.com/2020/03/18/lasts-longer-2-0/):

>Più si è fisicamente isolati più è probabile contare sul telefono per le connessioni sociali. Durante i momenti difficili, i prodotti cui si rivolgono le persone non sono quelli frivoli o di lusso. Piuttosto, guardano a un prodotto che ti tiene informato e connesso, ti permette di aiutare altri e magari ti salva la vita.

>È nei momenti difficili che un prodotto mostra la sua vera attitudine. Lo smartphone, deriso, vilipeso e accusato di ogni sorta di male sociale, è il nostro primo riferimento per evitare di ammalarci.

Siamo a casa ma fuori dai luoghi comuni e riconosciamo il valore della tecnologia, certo con i suoi limiti e le sue problematiche da risolvere. Usiamola per vivere una bella giornata in compagnia di persone che per noi contano. Grazie a chiunque passa di qui.
