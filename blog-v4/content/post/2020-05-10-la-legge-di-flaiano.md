---
title: "La legge di Flaiano"
date: 2020-05-10
comments: true
tags: [Flaiano, Gruber, BirchTree, iMac]
---
Probabilmente per carenza di attualità di rilievo, John Gruber [ha rispolverato una recensione del primo iMac](https://daringfireball.net/linked/2020/05/08/bray-original-imac), datata 1998 (!). Vi si legge:

> iMac non prevede un lettore di floppy disk per eseguire backup o scambiarsi dati. È una dimenticanza sconvolgente da parte di Steve Jobs, che dovrebbe saperla più lunga di così.

Matt Birchler su _BirchTree_ ha citato un'altra perla:

> Scommetterei che il 98 percento degli utenti di computer usa un floppy di tanto in tanto. iMac semplicemente li esclude e si rivolge a una “élite” disposta a pagare di più per avere di meno, se c'è sopra un marchio Apple.

E ha fornito in coda un proprio commento ironico, che riporta al tempo presente:

> Come utilizzatore di iPad, questa citazione colpisce duro oggi così come fece allora.

Per giudicare la dotazione di un computer, ieri e oggi, si dovrebbero cortocircuitare ancora una volta tecnologia e arti liberali, per tirare in ballo [Ennio Flaiano](http://www.aforismatico.it/Ennio-Flaiano/1615):

> La felicità consiste nel non desiderare che ciò che si possiede.

In una recensione ci si dovrebbe concentrare su quello che c'è, non su quello che manca o sembra mancare. Quando si lamenta una mancanza, è probabile che si sia semplicemente rimasti indietro.