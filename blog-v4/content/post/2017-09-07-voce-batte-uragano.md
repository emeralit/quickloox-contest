---
title: "Voce batte uragano"
date: 2017-09-07
comments: true
tags: [Siri, Harvey, Texas, anemia, drepanocitica, Cortana, Alexa, Amazon, Microsoft, Tyler, Frank]
---
Mi era sfuggita la notizia di una [quattordicenne in crisi da anemia drepanocitica salvata grazie a Siri](http://edition.cnn.com/2017/09/04/health/siri-harvey-rescue-sickle-cell/index.html) dalla devastazione dell’uragano Harvey; per fortuna ha rimediato **Flavio**, che ringrazio molto.

La casa di Tyler Frank era allagata e nessuna delle organizzazioni di soccorso tradizionali rispondeva alle chiamate, o per via di linee danneggiate o per le richieste di aiuto che soverchiavano le forze a disposizione.

La ragazzina ha avuto la presenza di spirito di pensare alla Guardia costiera; Siri ha permesso di risolvere la situazione con la frase *chiama la Guardia costiera*, in quel momento più veloce di qualsiasi altra alternativa.

Sembrano dettagli, ma Tyler era in piena crisi, con la febbre altissima, e anche risparmiare pochi secondi o pochi minuti nella sua situazione può darsi abbia fatto la differenza. Un attimo dopo e l’elicottero decisivo avrebbe potuto trovarsi da qualche altra parte.

Siri si è già dimostrata utile in una infinità di altre situazioni, quasi sempre meno urgenti o meno critiche di questa. La sua capacità di elaborazione continua a migliorare grazie all’apprendimento meccanico e l’intelligenza artificiale continua a restare un’altra cosa. Ma si può dire già oggi che questo decennio verrà ricordato come quello dell’ascesa delle interfacce vocali, dopo che quello scorso ha visto prima diffusione di massa di quelle *touch*.

Nessun test può essere veramente esaustivo per giudicare la qualità di un assistente rispetto a un altro, con banche dati immense alle spalle, la difficoltà di valutare il riconoscimento del linguaggio naturale, l’impossibilità di sapere in quanto tempo e con che precisione questo o quello impareranno da un errore per correggersi.

Tuttavia l’essenziale è che queste interfacce continuino a imparare e a maturare. Altrimenti si finisce come Microsoft e Amazon, che si scambiano i favori: [da Cortana si può aprire Alexa, da Alexa si può aprire Cortana](https://blogs.microsoft.com/blog/2017/08/30/hey-cortana-open-alexa-microsoft-amazons-first-kind-collaboration/).

In un’ottica di *volemose bbene* sembra una notizia positiva: che bello, la collaborazione, si parlano, più libertà per tutti.

In realtà è l’ammissione di un fallimento. Non parliamo di database della pubblica amministrazione, ma di assistenti vocali. Immaginiamo per un momento la scena: una ragazzina con la febbre alta, in una casa allagata, cerca soccorsi.

*Cortana, chiama la Guardia costiera!*

*Mi dispiace, non ho capito che cosa desideri* [o qualunque altra risposta dia Cortana].

*Cortana, apri Alexa*.

*Eccomi, Alexa al tuo servizio!* [o qualunque altra cosa dica Alexa].

*Alexa, chiama la Guardia costiera!*

E siamo esattamente dove eravamo un minuto prima, senza avere concluso niente. Peggio: non è scontato che in una situazione di emergenza si sappia qual è il servizio giusto da contattare. Se per ipotesi neanche Alexa avesse una buona risposta, avremmo semplicemente più che raddoppiato il tempo perso.

A chi giova una mossa del genere? A Microsoft e Amazon, i cui database si arricchiscono più velocemente di informazioni sui clienti. Informazioni che certe servono a migliorare il servizio, ma anche a concludere accordi più lucrosi in ambito marketing e pubblicitario. Cosa che Apple, con Siri, non contempla.