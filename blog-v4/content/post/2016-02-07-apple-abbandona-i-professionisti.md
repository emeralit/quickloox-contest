---
title: "Apple abbandona i professionisti"
date: 2016-02-07
comments: true
tags: [Mac, Parenti, teatro]
---
Con l’eccezione dei tecnici incontrati al [teatro Franco Parenti](http://www.teatrofrancoparenti.it) di Milano.

La foto è dilettantesca, presa da lontanissimo, senza luce, con un iPhone dall’obiettivo danneggiato, ma sul mio onore si tratta di due Mac.

 ![alt](/images/parenti.jpg  "title") 
