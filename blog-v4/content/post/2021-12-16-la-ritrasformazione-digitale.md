---
title: "La ritrasformazione digitale"
date: 2021-12-16T00:55:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Wwcc, Newton, MessagePad 2100, Worldwide Newton Conference Online, Vic-20] 
---
*Trasformazione digitale* è uno dei termini di moda e difatti nessuno sa più che cavolo significhi.

Tuttavia c’è una cura semplice, intensa e piacevole: partecipare alla imminente [Worldwide Newton Conference Online](http://wwnc.online), 28 e 29 dicembre.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Hello Newtonians and Apple Newton enthusiasts!<br><br>We want invite you to join Worldwide Newton Conference 2021!<br><br>More details on the website ;)<a href="https://twitter.com/hashtag/wenc2021?src=hash&amp;ref_src=twsrc%5Etfw">#wenc2021</a> <a href="https://twitter.com/hashtag/AppleNewton?src=hash&amp;ref_src=twsrc%5Etfw">#AppleNewton</a> <a href="https://twitter.com/hashtag/applenewtonmessagepad?src=hash&amp;ref_src=twsrc%5Etfw">#applenewtonmessagepad</a> <a href="https://twitter.com/hashtag/apple?src=hash&amp;ref_src=twsrc%5Etfw">#apple</a> <a href="https://t.co/HrDFs90eek">pic.twitter.com/HrDFs90eek</a></p>&mdash; 🦎 Apple Newton Fan 🏴󠁧󠁢󠁳󠁣󠁴󠁿 (@AppleNewtonFan) <a href="https://twitter.com/AppleNewtonFan/status/1471086884701024264?ref_src=twsrc%5Etfw">December 15, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Uno spettacolo, gente che ha fegato e motivazione (gli si perdona anche il refuso in bella vista sulla pagina). Solo che, se si dedicassero, che so, al Vic-20, sarebbero dei [semplici nostalgici](https://www.vic-20.it). Bravi, simpatici, anche geniali, ma nostalgici.

Invece Newton, rivisto oggi, è una macchina del tempo parcheggiata per sbaglio in un’epoca troppo precedente. Quello che viviamo oggi, i cambiamenti epocali, l’evoluzione imprevista e le mille opportunità che ancora ci sfuggono e però arriveranno, era tutto lì dentro, in quello schermo verde, in quello stilo che serviva veramente, a differenza di tutti gli altri (Apple Pencil compresa).

Il mio iPad di prima generazione, che ha compiuto undici anni e ancora funziona senza problemi, con la batteria in forma, usato tutti i giorni dalle figlie, ovviamente non è più una macchina performante né aggiornata; eppure, con uno sforzo ragionevole, potrebbe ancora portare a termine diversi compiti lavorativi.

Lo stesso posso dire del mio [Newton MessagePad 2100](https://everymac.com/systems/apple/messagepad/stats/newton_mp_2100.html). Che però ne ha compiuti ventiquattro. E accidenti.

Newton sì, la trasformazione digitale l’ha annunciata, mostrata, incarnata. Da riscoprire, chi se lo fosse perso, per comprenderne il significato.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*