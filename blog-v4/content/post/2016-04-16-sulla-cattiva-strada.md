---
title: "Sulla cattiva strada"
date: 2016-04-16
comments: true
tags: [coreutils, gshuf, StackOverflow, Homebrew, AppleScript, Gnu, Terminale]
---
Un file contenente molte righe di testo, che voglio ridistribuire in modo casuale dentro il documento.<!--more-->

È il giorno perfetto per buttare giù un AppleScript. Tranne che per il tempo: invece di stringere, strangola.

Allora baro e vado a cercare [la pappa pronta su StackOverflow](http://stackoverflow.com/questions/2153882/how-can-i-shuffle-the-lines-of-a-text-file-on-the-unix-command-line-or-in-a-shel).

Scopro che esiste [Gnu Coreutils](http://www.gnu.org/software/coreutils/coreutils.html), come installarlo su un Mac dotato di [Homebrew](http://brew.sh) sia difficile quanto scrivere nel Terminale `brew install coreutils`, la durata dell’installazione (una manciata di secondi) e la disponibilità dentro coreutils del comando `gshuf`.

`gshuf documento > nuovaversione` ed è cosa fatta.

Problema risolto e vergogna perché non è esattamente la strada giusta per imparare, semmai per sfruttare l’apprendimento di altri. Però ci si diverte.