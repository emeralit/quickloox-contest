---
title: "Conflitto di interessi"
date: 2020-08-01
comments: true
tags: [Apple]
---
Ebbene sì, ero proprietario di una azione Apple. Questo ha notoriamente accecato le mie capacità di giudizio e mi ha reso un fanatico che chiaramente scriveva cose insostenibili in difesa delle proprie ricchezze, in particolare quei trenta o quaranta centesimi che arrivano come dividendo ogni tre mesi.

Ero proprietario perché Apple ha annunciato uno split azionario e, invece di detenere una azione Apple da quattrocento dollari, ne avrò quattro da cento. Sempre chiaramente, questo è destinato ad aggravare la mia posizione di fanatico.

Il 23 dicembre 1997 le azioni Apple toccarono il punto più basso della loro storia, quaranta centesimi di dollaro. Adesso che è avvenuto lo split, per mantenere le proporzioni rispetto al passato possiamo dire che è come se quella volta una azione Apple [fosse valsa dieci centesimi](https://twitter.com/macjournals/status/1288946809549848578), rispetto ai diecimila centesimi di oggi.

Dieci centesimi. A malapena si potevano giocare in una slot machine di strada a Las Vegas.

Adesso che quel valore è cresciuto di mille volte in ventitré anni, Apple [ha superato in capitalizzazione perfino Saudi Aramco](https://www.cnbc.com/2020/07/31/apple-surpasses-saudi-aramco-to-become-worlds-most-valuable-company.html) e resto un fanatico, chiedo una autodefinizione da parte di quelli che avevano capito tutto, erano le persone più obiettive del mondo e scrivevano che Apple sarebbe fallita presto, Wintel era lo _standard di fatto_, che la _guerra dei Pc_ era finita, che la quota di mercato di Mac era irrilevante. E quanto hanno guadagnato.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">We’ve periodically reminded that Apple’s lowest stock price was 23 Dec 1997, when it closed at a currently-adjusted price of $0.40 per share.<br><br>After 31 Aug 2020 and the 4:1 split, the new adjusted record low price on that date will be $0.10/share.<br><br>TEN CENTS PER SHARE.</p>&mdash; MacJournals.com (@macjournals) <a href="https://twitter.com/macjournals/status/1288946809549848578?ref_src=twsrc%5Etfw">July 30, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>