---
title: "Una buona scusa"
date: 2019-11-25
comments: true
tags: [Wired, MacBook, Pro]
---
Per una volta, *Wired* (UK) ha scritto una recensione degna di lettura, [di MacBook Pro 16”](https://www.wired.co.uk/article/16-inch-2019-macbook-pro-review). Le cose come stanno secondo loro, nel bene e nel male, senza giri di parole e limitando il pregiudizio negativo che tradizionalmente coltivano verso Apple.

Certo, il titolo è *Una scusa per i fallimenti passati*, ma in sostanza si ritirano in ballo le tastiere ovunque possibile e poco più.

Interessante il confronto con Windows. Di solito qualcuno scrolla le spalle e racconta di potere avere la stessa dotazione a prezzo molto inferiore. *Wired* critica la scelta della scheda grafica e sconsiglia l’acquisto ai *gamer*, perché con la stessa cifra si può avere un Pc con scheda più potente.

Al tempo stesso, si parla con dovizia del sottosistema audio come del migliore che sia mai stato inserito in un portatile, in un’altra categoria anche rispetto ai modelli passati di MacBook Pro, che comunque erano i migliori (lo scrive *Wired*, non io).

Il trackpad è *gigantesco*, con tecnologia di clic migliore di qualunque altra.

Ci sono lodi per la batteria, effettivamente vicina nell’uso reale alle undici ore dichiarate da Apple. Sempre secondo *Wired*, l’ultimo Pc con lo stesso processore che hanno testato tira al più tre ore e mezzo. Non è male.

Si fa ironia sul fatto che l’oggetto non abbia avuto una presentazione sul palco come i suoi concorrenti, così che Tim Cook non ha dovuto scusarsi pubblicamente per i MacBook Pro passati come avrebbe dovuto. Sarebbe stata comunque una buona scusa; MacBook Pro 16” *è uno dei migliori computer grandi-ma-portatili*.

Come difetti, il prezzo e il probabile desiderio di alcuni di avere tasti con più corsa. Sì, davvero, una cosa che c’è sempre stata e una che magari c’è, magari no. Nient’altro.

Neanche una parola sulla velocità. *MacDailyNews* [scrive](https://twitter.com/MacDailyNews/status/1197751474870194176):

<blockquote class="twitter-tweet"><p lang="da" dir="ltr">Our MacBookPro16,1 (Intel Core i9-9980HK @ 2.40 GHz) scored 1217 (single core) and 7233 (multi-core) in Geekbench 5. <a href="https://twitter.com/hashtag/16InchMacBookPro?src=hash&amp;ref_src=twsrc%5Etfw">#16InchMacBookPro</a> <a href="https://t.co/9g0hhoJqfL">https://t.co/9g0hhoJqfL</a></p>&mdash; MacDailyNews (@MacDailyNews) <a href="https://twitter.com/MacDailyNews/status/1197751474870194176?ref_src=twsrc%5Etfw">November 22, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Questo, forse, avrebbe dato fastidio ai lettori tipici di *Wired* più delle chiacchiere sulle tastiere.
