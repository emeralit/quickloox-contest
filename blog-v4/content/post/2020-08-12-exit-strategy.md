---
title: "Exit strategy"
date: 2020-08-12
comments: true
tags: [Cook, Barron’s, Apple, Italia]
---
Scrive [Eric J. Savitz su Barron’s](https://www.barrons.com/articles/apples-is-nearly-worth-2-trillion-should-you-sell-the-stock-51596846734):

>Apple vale veramente duemila miliardi? Le sue azioni sono arrivate vicinissime allo storico traguardo la scorsa settimana, con la chiusura di venerdì a 444 dollari, che portava il titolo al 5 percento di distanza dalla soglia. Si tratta comunque di una capitalizzazione di mercato di 1,9 migliaia di miliardi, grosso modo il prodotto interno lordo dell’Italia.

Mi chiederei più che altro se *l’Italia* valga duemila miliardi. se i Padri fondatori avevano pensato a una possibile *exit strategy* e se magari ci sia spazio per una acquisizione.