---
title: "Il tipografo sbagliato"
date: 2023-07-13T13:02:21+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [scuola, briand06, macmomo, Word, LaTeX]
---
**Macmomo** e anche **briand06** hanno risposto con [osservazioni interessanti](https://macintelligence.org/posts/2023-07-08-il-tipografo-perduto/#!/comments:posts-2023-07-08-il-tipogra/pero-restando-sul-realisti) e [pratiche](https://macintelligence.org/posts/2023-07-08-il-tipografo-perduto/#!/comments:posts-2023-07-08-il-tipogra/concordo-e-conoscendo-la-s) a quelle contenute nel [Tipografo perduto](https://macintelligence.org/posts/2023-07-08-il-tipografo-perduto/).

Ho provato a rispondere nei commenti ma si faceva lunga e così eccoci qui.

Html ha un valore didattico elevato perché insegna a distinguere le parti strutturali che compongono un testo: paragrafo, colonna, tabella, capoverso e così via.

A scuola si deve imparare Html e a scrivere Html, lavorando sul materiale essenziale, cioè il testo, con strumenti essenziali, cioè editor di testo (essenziali ma di grande potenza in prospettiva). Utopia vorrebbe che *tutta* la scuola producesse Html, aperto, immortale, aggiornabile, libero. La mia generazione non farà in tempo a vederlo, né svariate delle prossime. Insegnare tipografia come leggere, fare di conto, suonare porterà forse un giorno a persone illuminate in grado di portare un cambiamento radicale. Il *forse* vale la pena di impegnarsi.

Proseguo: come si insegna storia dell’arte, c’è da insegnare tipografia. Come è fatto un carattere, come è fatto un font, i punti, gli spazi, le legature e poi l'uso corretto della tipografia, esattamente come si insegna l'uso del colore.

Siccome lavoriamo su testi Html, introduciamo la tipografia aggiungendo i CSS, ovviamente le parti che servono a partire dalle basi.

Partire da testi Html e poi passare a Word è evidentemente fuori luogo, per tre ragioni: Word, come tutti i programmi di alto livello, maschera il codice di base, proprio il tramite che abbiamo per portare l’insegnamento; Word altera un sorgente e introduce arbitrariamente costrutti artificiosi, inefficienti e arbitrari. Se non guardiamo più il codice abbiamo perso, perché da lì impariamo a usare Word invece che imparare la tipografia. Se guardiamo il codice abbiamo perso, perché è diventato un blob indecente e spesso incomprensibile senza un esame accurato.

La terza ragione è che Word è inadeguato a fare tipografia (un indizio: riviste e libri non si impaginano, professionalmente, con Word). Html e Css servono per portare i concetti nelle scuole medie, perché sono più facile da apprendere. Alle superiori bisognerebbe introdurre [LaTeX](https://www.latex-project.org).

La quarta ragione dopo averne preannunciate tre è provocatoria, ma la sento molto. Siamo pieni di adulti che usano Word anche per togliersi i peli dalle orecchie. Anche adulti che hanno praticato Word a scuola, è il 2023. Analfabeti di ritorno in percentuale altissima. Vogliamo guardare la loro performance tipografica, senza allargare il campo a come sanno presentare un concetto o organizzare una tabella?

Eccoci: usano gli apostrofi dei programmatori. Nonostante siano seduti su un correttore ortografico, scrivono *perchè*. Oppure apostrofano! *perche’* (con l’apostrofo da programmatore). Non sanno usare le maiuscole accentate. Sanno usare solo un font. Non è difficile trovare talenti che tabulano usando gli spazi o che usano lo spazio prima della punteggiatura. Quanti trattini esistono? Uno. Quanti spazi esistono? Uno.

Mi dite che questa è al novanta percento ortografia, non tipografia, ed è verissimo. Questa è l’ortografia con cui esce da scuola la gente che usa Word. E si vorrebbe usare Word per insegnare la tipografia. Immagino i risultati. Accetterò la proposta quando vedrò qualcuno impaginare con Word una riedizione di [Zang Tumb Tumb](https://www.infonotizia.it/zang-tumb-tumb-poesia-di-filippo-tommaso-marinetti-futurismo/) (mica Manuzio, cantinari della Milano del 1914, e ho detto tutto).

Vorrei vedere un documento Word di quelli che girano negli uffici, nella scuola appunto, nella pubblica amministrazione, nelle case editrici (!) con un foglio stile, la scelta dei font e delle spaziature tra caratteri, margini appropriati, scelta dei rientri di paragrafo, legature, crenatura dove serve. Non c’è *niente*. Word è il programma migliore per lavorare a tipografia zero. La prova è empirica: nessuno neanche ci prova.

L’idea che proporre Word serva a facilitare il percorso verso i docenti e i direttori didattici è certo pratica, ma dico una cosa che deriva dall’esperienza che ho cumulato finora. La scuola è convinta che lo scopo di averlo a scuola sia imparare a usare Word. I docenti, che usano Word, sono sommamente resistenti a imparare qualsiasi cosa *in più* su Word (vale per tutto Office). Se gli proponi un corso dove tutto è descritto, spiegato, è possibile che lo accettino come attività di aggiornamento professionale. Basta che non comprenda Word o Excel. Loro *lo sanno già usare*. Sono convintissimi.

L’unico modo per insegnare davvero tipografia è togliere di mezzo Word e lottare di conseguenza per portare i docenti a toccare con mano qualcosa di nuovo, che li faccia sentire più preparati di prima.