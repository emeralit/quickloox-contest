---
title: "Il senno di mai"
date: 2016-11-13
comments: true
tags: [Ballmer, iPhone, Nokia, Bloomberg, Re/Code, Linux, Microsoft]
---
Oggi Steve Ballmer si diverte. È un miliardario [proprietario di una squadra di basket professionistico](http://www.bloomberg.com/news/articles/2014-10-16/steve-ballmers-new-life-as-owner-of-nbas-most-expensive-team) che lavora a un sacco di progetti che gli piacciono.

Ieri fu amministratore delegato di Microsoft e viene giustamente ricordato per la [risata](https://macintelligence.org/posts/2013-09-22-sei-anni-di-differenza/) con cui accompagnò l’arrivo di iPhone, un telefono che nessuno avrebbe comprato in azienda perché non aveva la tastiera. Oltre che per avere [sabotato, comprato a prezzo di svendita e distrutto](https://macintelligence.org/posts/2013-09-03-mission-accomplished/) il ramo di affari cellulare di Nokia e avere definito Linux, sineddoche per il software libero, [cancro della proprietà intelletuale](http://www.theregister.co.uk/2001/06/02/ballmer_linux_is_a_cancer/).

Dobbiamo ringraziarlo perché ha fatto un favore al mondo. Microsoft aveva un potere subdolo e immenso e stava soffocando qualunque alternativa con il suo software indegno eppure pervasivo, grazie ai metodi monopolistici usati per stroncare ogni tipo di diversità. Invece la sua incompetenza ha permesso tra L’altro il fiorire del software libero e l’avvento di un mercato cellulare nuovo in cui Microsoft conta niente: un contributo a una informatica più varia, più libera, più aperta, più progredita e più creativa.

Oggi il nuovo, spensierato Ballmer ha concesso una intervista a Bloomberg in cui afferma di non avere capito che iPhone si sarebbe affermato grazie a un nuovo modello commerciale nel quale il costo del terminale, sostanzialmente, spariva dentro la bolletta dei consumi cellulari.

A *Re/Code* ci sono persone di buon senso e nel [commento all’intervista](http://www.recode.net/2016/11/7/13548032/steve-ballmer-apple-iphone-bloomberg) spiegano come in realtà quel modello fosse applicatissimo anche dagli altri produttori e iPhone si sia imposto non tanto per il sussidio del *provider* che scherma il compratore dal costo reale dell’oggetto, quanto per via della sua potenza superiore.

Ballmer *ancora oggi* non ha capito perché iPhone ha avuto successo. Tra cent’anni non lo avrà capito. Non lo capirà mai.

Iddio ce lo conservi a lungo e che il suo fulgido esempio possa ispirare tanto *management* a Microsoft, tuttora bisognosa di una robusta regolata a favore della libertà e del benessere tecnologico collettivo.

