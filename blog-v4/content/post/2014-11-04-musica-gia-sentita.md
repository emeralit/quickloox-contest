---
title: "Musica già sentita"
date: 2014-11-04
comments: true
tags: [iPhone, iTuned, major, Nokia, Sony-Ericsson, Apple, Cio, AmericanExpress, token, ApplePay, GoogleWallet]
---
Quando uscì iPhone nel 2007, argomentai che il settore era fermo a una sorte di truffa nei confronti del consumatore. iPhone portava una serie di innovazioni, nessuna delle quali tecnologica: tutto quello che serviva a mettere insieme un iPhone era ampiamente disponibile alle Nokia e alle Sony-Ericsson. Bastava farlo! Apple non ha inventato nulla, solo messo insieme quanto gli altri guardavano senza capirne il potenziale. O senza *volerlo* capire, nell’intento di propinare al mercato prodotti di concezione vecchia, meno costosi da sviluppare a parità di profitto.<!--more-->

iPhone è stato eclatante. Ma già ai tempi di iTunes si era svolta la stessa sceneggiata, solo in forma più discreta. Apple, senza inventare niente, aveva messo in piedi quello che le case discografiche avrebbero potuto comunque realizzare da sole. Ma non capivano, o forse non *volevano* capire, per continuare a distribuire i Cd e restare aggrappate alla vendita dei supporti fisici.

Il bello è che le case discografiche strinsero gli accordi di licenza con Apple, per poi tentare di mettere in piedi strutture proprie di vendita di musica online. Tutti bagni di sangue.

Ecco. Leggo su Cio che American Express intende arrivare a [sostituire i pagamenti via carta di credito con quelli via token](http://www.cio.com/article/2842594/seeking-security-american-express-aims-to-swap-card-numbers-with-tokens.html#tk.rss_all).

Detto semplicemente, [Apple Pay](http://www.apple.com/apple-pay/) permette di pagare con carta di credito senza avere in tasca la carta di credito; basta un iPhone, che interagisce con il lettore del negozio e organizza una transazione sicura e confidenziale. Attraverso, chi lo avrebbe mai pensato, un *token* (una sequenza di dati monouso).

Nel frattempo le banche si affrettano, con tutta la competenza di chi è arrivato ultimo, a [pubblicare le proprie app per il pagamento attraverso il computer da tasca](http://www.pcworld.com/article/2841892/major-banks-ready-their-own-mobile-payment-apps.html). Nel tentativo di opporsi ad Apple Pay (ma anche a Google Wallet), che hanno autorizzato con l’altra mano. Che era lì pronto da fare e bastava farlo, senza bisogno di inventare niente.

Il nuovo iTunes, le nuove *major*, la vecchia miopia. La stessa musica.