---
title: "Parliamo di Siri"
date: 2017-09-11
comments: true
tags: [Siri, Gruber, Macintosh, uncanny, valley]
---
A [commento di un pezzo di Wired](https://daringfireball.net/linked/2017/09/08/siris-voice) sui notevoli miglioramenti in naturalezza della voce di Siri, Gruber nota che le prestazioni dell’assistente vocale non sono ancora all’altezza e chiosa:

>Se si potesse scegliere tra un Siri che suona meglio ma funziona come ora, o un Siri che suona uguale a prima ma funziona meglio, non conosco alcuno che scarterebbe la seconda opzione.

A Gruber ho scritto solo una volta e chiaramente non mi conosce. Però conosce benissimo Apple e dovrebbe sapere come lavora; ugualmente, avendone visti di prodotti e di iterazioni di prodotto, dovrebbe ricordarsi che Apple lavora per la gallina domani e il critico tipico da due soldi su un sito da due soldi [se la prende con l’uovo oggi](https://macintelligence.org/posts/2016-12-02-preparati-al-peggio/).

Se uno pensa al primo Macintosh privo dei tasti per spostare il cursore e al mouse monopulsante, al primo iPhone con scarsa connessione scarsa fotocamera e scarse disponibilità di software, al primo iPad che – orrore – non accettava chiavette, a MacBook Air con una porta sola, la strada è chiarissima.

Per Apple ogni prodotto ha una funzione o una caratteristica che è la più importante in assoluto. Un portatile deve durare almeno dieci ore con la batteria, il primo Macintosh era un totem per la venerazione dell’interfaccia grafica da usare con il mouse, su iPhone il touch valeva più di ogni altra cosa, iPad doveva collegarsi a Internet invece che ai dischi di scrivania o flash Usb e via dicendo.

Stabilita la priorità assoluta, si itera su tutto il resto per renderlo sempre migliore. iPhone ha una fotocamera sempre migliore, i portatili devono diventare sempre più leggeri e sottili, watch deve avere una risposta software sempre più scattante e avanti così.

Scommetto che, in base ai dati dell’esperienza, al fiuto di chi ci lavora o chissà che altro, in Apple ritengono che la priorità assoluta per Siri sia la naturalezza della voce. E su tutto il resto poi si iteri.

Teniamo presente che esiste il fenomeno della [uncanny valley](http://theconversation.com/uncanny-valley-why-we-find-human-like-robots-and-dolls-so-creepy-50268): quando una cosa è evidentemente robotica, può riuscirsi perfino simpatica. Quando una cosa è robotica e indistinguibile dal corrispondente umano, non ci accorgiamo che è robotica e tutto fila. Quando una cosa robotica è *quasi* perfettamente umana, ma non del tutto, si scatena una reazione di diffidenza e inquietudine.

Siri deve arrivare a essere così naturale da confondersi con un umano. Se rimane un passo prima, molti ci si troveranno a disagio. Che poi riesca o meno a capire quello che gli si dice è ovviamente importantissimo. Ma prima arriva l’interfaccia, poi la funzione.