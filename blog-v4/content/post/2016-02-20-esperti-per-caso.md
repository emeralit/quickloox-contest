---
title: "Esperti per caso"
date: 2016-02-20
comments: true
tags: [Cio, Windows, Apple]
---
Che bello questo *incipit* di Cio sull’andamento della [vendita di computer da tasca](http://www.cio.com/article/3035048/smartphones/android-leads-windows-phones-fade-further-in-gartners-smartphone-sales-report.html) nello scorso trimestre natalizio.<!--more-->

>Una volta predestinati a sfidare Apple nel mercato degli smartphone, i telefoni Windows sono affondati a poco più dell’uno percento degli smartphone nel mondo, secondo nuovi dati di Gartner.

Vale quel che vale, perché a dire a parlare del 2018 [è la stessa Gartner](http://www.gartner.com/newsroom/id/2791017) che oggi dà Windows all’uno percento. Sono i buffoni più pagati del mondo, assieme a Idc, che vede nel 2018 i telefoni Windows [oltre il sei percento](http://www.pcworld.com/article/2241991/idc-predicts-modest-rise-in-windows-phones-by-2018.html).

Importante notare che potrebbe anche succedere, ma nelle loro tabelle magiche non c’è traccia dei dati attuali. Ovvero, se succedesse, ci avrebbero preso per puro caso.