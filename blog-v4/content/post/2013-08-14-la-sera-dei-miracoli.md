---
title: "La sera dei miracoli"
date: 2013-08-14
comments: true
tags: [Mac, OSX]
---
>È la sera dei miracoli, fai attenzione:

Se questo *post* si legge, la chiavetta ha trovato un filo di banda nella cella di una piccola ma sovraffollata località balneare quasi al culmine del delirio agostano.

>qualcuno nei vicoli di Roma ha scritto una canzone.

Io no. In compenso nella giornata ho riveduto parti del prossimo libro su OS X Mavericks, visitato amici, spostato temporaneamente la residenza di qualche centinaio di chilometri, [ricominciato](https://macintelligence.org/posts/2013-08-12-un-nuovo-backup/) a fare *backup* regolare su Time Machine, fatto un timido (e per ora inutile) tentativo di cominciare un personaggio su [Runescape](http://www.runescape.com/beta) e provato iBooks e Mappe sul Mavericks stesso.

>È la sera dei cani che parlano tra di loro,

A qualcuno sembra poco, Mavericks. Non si ricorda di quando Apple faceva solo Mac (vabbeh, e iPod) e faticava a stare dentro i due anni per presentare una nuova versione.

>della Luna che sta per cadere

Di cadente magari mi capita qualche stella residua delle Perseidi. Se non arriva niente, mi consolerò con qualche [sito Nasa](http://ssd.jpl.nasa.gov/).