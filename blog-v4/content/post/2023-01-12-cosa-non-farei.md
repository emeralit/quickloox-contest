---
title: "Cosa non farei"
date: 2023-01-12T00:54:13+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [Apple Pencil, Freeform, Filippo Strozzi, Strozzi, Roberto Marin, Marin]
---
Da molto tempo firmo certi documenti di routine con firme scandite e memorizzate, da usare con Anteprima su Mac o con gli strumenti corrispondenti su iPhone e iPad.

Ieri, per la prima volta, ho fatto la stessa cosa ma sul momento, con [Apple Pencil](https://macintelligence.org/posts/2022-12-30-le-magie-della-punta/). Non mi sono trovato istantaneamente ma sono bastati pochi tentativi. La vera cosa da imparare è che la vera differenza tra Pencil su schermo e penna su foglio è la scarsità di attrito nel primo caso. Appena si impara a essere leggeri, firmare con Apple Pencil è uno spasso. Mai avrei detto che un giorno mi sarebbe capitato.

Altra cosa che non pensavo avrei fatto, almeno a questo livello di coinvolgimento, è stata dedicare tempo per approfondire delizie e deliri di [Freeform](https://macintelligence.org/posts/2022-10-27-forma-e-funziona/). Ho scoperto alcune cose che non mi aspettavo, qualcuna poco piacevole, un paio interessanti e una formidabile.

Però non dico niente perché i grandissimi Filippo e Roberto mi hanno proposto di intervenire nel [loro podcast](https://macintelligence.org/posts/2022-08-15-du-is-megl-che-uan/) a parlare appunto di Freeform e voglio godermi per intero l’occasione. Abbiamo condiviso appositamente una lavagna di Freeform per collaborare su appunti, prove, bug, scoperte, interrogativi, prove pratiche ed è stato piacevole come un aperitivo rinfrescante prima di una cena saporita. Ci sarà da divertirsi e ne vale la pena.