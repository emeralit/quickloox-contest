---
title: "Zero alla seconda"
date: 2015-01-27
comments: true
tags: [iPhone, TouchID, 64bit, Android, Siri]
---
Mi scrive **Stefano** (da grassetto a grassetto):

**Siamo** alla seconda generazione di iPhone con Touch ID e ancora non vedo rivali sul mercato: zero assoluto.<!--more-->

Eppure da utilizzatore assiduo ritengo questa tecnologia la killer application per i computer da tasca. Già ora con gli acquisti on line e lo sblocco del telefono fatto centinaia di volte al giorno mi dimentico cosa c’è sotto il mio pollice. 

A mio avviso se Siri funzionasse alla perfezione non sarebbe così d’impatto per il mio utilizzo quotidiano dell’iPhone. 

Senza contare quando arriverà la possibilità dell’acquisto nel
negozio reale con la sola impronta digitale: e chissà che **altro.**

Risparmio la pletora possibile di citazioni di articoli che, il giorno dopo la presentazione di iPhone con Touch ID, si affrettavano a dire che c’era anche Android, che se non c’era era imminente, avrebbe funzionato uguale e costato meno.

Un po’ come i 64 bit, del resto. L’innovazione vera è quella che esce dai laboratori e arriva nelle tasche.