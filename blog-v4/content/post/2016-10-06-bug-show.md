---
title: "Bug show"
date: 2016-10-06
comments: true
tags: [watch, bug, Messaggi, iPhone]
---
Quando chiedo a Siri di inviare un messaggio via watch, capisce correttamente il nome del destinatario, ma il risultato è il seguente:<!--more-->

 ![Bug di Messaggi su Apple Watch](/images/bug-watch.jpg  "Bug di Messaggi su Apple Watch") 

Il messaggio, ovviamente, non parte.

Se uso Messaggi senza usare Siri, per rispondere a un messaggio esistente, funziona.

Da iPhone l’uso di Messaggi con Siri funziona.

Ho provato a resettare la sincronizzazione dei dati su watch, senza risultato.

Ho provato ad azzerare e reinstallare watch, senza risultato.

Finora non ho individuato una trattazione del problema in rete.

I prossimi passi sono scrivere sui forum Apple e magari interpellare il supporto.

Il bug è veramente fastidioso – inviare messaggi via Siri da watch è una comodità notevole – e, al momento, la sua risoluzione appare davvero criptica.

Il tutto su watch di prima generatione e watchOS 2.