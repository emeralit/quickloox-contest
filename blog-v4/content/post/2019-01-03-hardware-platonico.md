---
title: "Hardware platonico"
date: 2019-01-03
comments: true
tags: [IKEA, Mac, Tablet]
---
L’esposizione di Ikea utilizza spesso la sagoma di un computer per aggiungere verosimiglianza all’arredamento delle stanze-tipo.

La loro idea di un computer-tipo è un Mac, senza il minimo spazio per i dubbi.

 ![Facsimile di Mac per gli arredamenti Ikea](/images/tipo-mac.jpg  "Facsimile di Mac per gli arredamenti Ikea") 

La tavoletta-tipo, invece, è un *concept* astratto, simile a tutto, uguale a niente.

 ![Facsimile di tavoletta per gli arredamenti Ikea](/images/tipo-tablet.jpg  "Facsimile di tavoletta per gli arredamenti Ikea") 

Fatico a credere che Ikea improvvisi sui dettagli o agisca a caso, specialmente nell’organizzare l’esposizione: è la loro fabbrica di vendite.

Mi chiedo quindi perché tanta concretezza nel riferirsi a Mac, per inseguire invece una immagine platonica di tavoletta.
