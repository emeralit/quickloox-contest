---
title: "La compilation decisiva"
date: 2017-11-14
comments: true
tags: [iPad, Continuous]
---
Si trovasse a vagare per i campi un sopravvissuto dell’esercito *iPad non è un computer perché non ci si può programmare sopra*, tempo di assestargli il colpo di grazia.

[Continuous](http://continuous.codes) è un ambiente di sviluppo autodefinito professionale per iPad, riservato a programmatori [C#](https://docs.microsoft.com/en-us/dotnet/csharp/) e [F#](http://fsharp.org).

>Continuous esegue continuamente il codice così si può vedere come cambiano gli oggetti programmati intanto che si scrive.

>Continuous supporta i progetti di tipo Application (Console e iOS) e di tipo Library nei formati standard .csproj e .fsproj.

La app parla con le librerie di programmazione native per iOS.

Soprattutto:

>Tutta la compilazione avviene sull’apparecchio. Questo mantiene il codice al sicuro e inoltre garantisce di non annoiarsi in aereo mai più.

Unico difetto, per me definitivo: sono linguaggi Microsoft. Altrimenti, un sacco di gente di bocca più buona della mia programmerà, compilerà ed eseguirà codice su iPad senza alcun vincolo né alcuna risorsa esterna di supporto.

È sempre un po’ triste chiudere definitivamente un argomento, ma pare proprio che non ci sia più nulla da discutere.