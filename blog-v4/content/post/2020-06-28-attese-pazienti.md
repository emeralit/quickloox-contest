---
title: "Attese pazienti"
date: 2020-06-28
comments: true
tags: [Facebook]
---
Per lavoro devo aprire Facebook. A parte questo, da diversi giorni non lo apro più per alcuna altra ragione.

Ho scoperto l’acqua calda. Il terminale che ho nelle mani o sotto controllo trabocca di ebook, siti salvati per un momento migliore, studi scientifici da scorrere, riviste online, zine app, corsi, newsletter, articoli, pagine di quotidiano scandite per non perderle, conversazioni su Slack, wiki, manuali, tutorial e così via.

È materiale per almeno dodici vite.

Mi picco spesso di voler evitare le nostalgie gratuite e le rimembranze di mezza età, i come eravamo giovani, il retrocomputing come esorcismo antivecchiaia invece di passione ingegnosa.

Se però un ebook attende da un anno di essere aperto, quello non è passato. È un appuntamento sulla fiducia, da parte di hardware e software pazienti e ignari di esserlo.

Ho tanti appuntamenti arretrati che sono un oggi, solo in attesa. Devo presentarmi più spesso e, se riesco a farlo, migliorerò.