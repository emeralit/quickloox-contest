---
title: "Il filmato impossibile"
date: 2014-03-23
comments: true
tags: [MacPro, FCPX]
---
Alcuni di noi vivono vite dove le capacità del software Apple sono spinte al limite. Quanto può essere lungo un documento Pages? Quante canzoni riesce ad amministrare iTunes? Quanti Pdf può aprire Anteprima prima di impazzire e noi con lui? A volte è curiosità, a volte lavoro, come **avariateedeventuali** per il quale Numbers è uno [strumento importante di amministrazione](https://moot.it/quickloox#!/blog/blog/2014/02/10).<!--more-->

Su fcp.co hanno messo alla prova [Final Cut Pro X](https://itunes.apple.com/it/app/final-cut-pro/id424389933?l=en&mt=12) per vedere [se e quali limiti abbia](http://www.fcp.co/final-cut-pro/news/1372-pushing-apple-s-new-mac-pro-and-final-cut-pro-x-to-their-limits) in termini di montaggio video, sopra [Mac Pro](http://www.apple.com/it/mac-pro/).

È saltato fuori che i limiti software sostanzialmente non esistono. Il vecchio Final Cut Pro 7 aveva un limite di 99 tracce; Final Cut Pro X, che ragiona in un altro modo, ha tranquillamente collegato 1.600 clip 4K (full HD moltiplicato due) prima che gli autori dell’esperimento decidessero di passare ad altro.

Final Cut Pro X lavora tranquillamente con fotogrammi a 16k (otto volte Full HD). Ma anche a 500.000 x 1.080 pixel! E sopporta pacificamente la creazione di una *timeline* lunga 558 giorni.

Risulta che il collo di bottiglia a limiti così elevati da risultare assurdi – oggi – è assolutamente l’hardware: nessun computer di prezzo umano è in grado attualmente di erogare dati in quantità così ingenti, neppure un Mac Pro.

A volte spingere il software oltre i limiti fa dannare; a volte fa scoprire nuove frontiere.