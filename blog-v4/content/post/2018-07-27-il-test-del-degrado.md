---
title: "Il test del degrado"
date: 2018-07-27
comments: true
tags: [blancco, iPhone, Android, Andy]
---
Ringrazio molto **Andy** perché mi ha fatto scoprire un [report interessante di Blancco](https://download.blancco.com/download/en-rs-q4-2017-state-of-mobile-device-repair-and-security.pdf) sull’affidabilità dei computer da tasca. Il documento contiene vari punti interrogativi e ha bisogno di una lettura attenta, tuttavia sembra una realizzazione onesta con vari punti degni di nota. Per esempio, iPhone 6 e 6s sarebbero sensibilmente più propensi a guastarsi degli altri modelli; Samsung ha dati orribili rispetto ai concorrenti e però sta migliorando; eccetera.

Meglio non cercare coperture giornalistiche del report, tuttavia. Ci si imbatterebbe in un [articolo di Bgr](https://bgr.com/2018/07/20/ios-vs-android-most-reliable-data-2018/) che dal report, interessante proprio perché non fa di ogni erba un fascio, rabbercia fuori una patetica storia su un presunto mito della maggiore affidabilità di iPhone su Android che cadrebbe. Storia che ci può stare, se non fosse che il report parla di tutt’altre cose.

A insistere e volersi veramente fare del male, si arriva a *telefonino.net* che titola [Un iPhone meglio di Android? Dati alla mano, è un falso mito](http://www.telefonino.net/Apple/Notizie/n50627/iphone-vs-smartphone-android.html) ed elargisce una pagina che fa cadere le braccia – per stare sopra la cintura – quanto a scemenze, truismi, frasi fatte, ignoranze e banalità. Una pagina per un pubblico di mentecatti, scritta con il coraggio di proporre frasi come *[un report] basato su dati assolutamente empirici*. Roba da sghignazzare, se non fosse tragedia culturale e testimonianza di un degrado dei cervelli che fa paura. Perché sono guasti che nessuno ripara più, altro che i telefonini.