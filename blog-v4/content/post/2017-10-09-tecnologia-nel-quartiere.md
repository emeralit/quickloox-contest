---
title: "Tecnologia nel quartiere"
date: 2017-10-09
comments: true
tags: [Skacciakanazza, Canazza, Legnano]
---
Ho messo una piccolissima mano nell’organizzazione di una festa di quartiere a Legnano, [Skacciakanazza](http://www.skacciakanazza.it), il che mi ha permesso di guardare dietro le quinte.

Una volta il tema principale per una festa di zona era l’approvvigionamento del cibo. Poi c’erano i banchetti degli sponsor, poi il complessino (direbbe Elio), la lotteria benefica, il torneo di qualcosa (poco importa se scacchi, scopa, beach volley, burraco) e si finiva lì.

Oggi sono cose che sì, vanno coordinate, ma sono date pressoché per scontate. Mentre l’ingresso della tecnologia è ingente e stupefacente.

Alla lotteria analogica era affiancata anche quella interamente digitale. Laboratori di robotica per bimbi, che hanno saturato ogni spazio possibile di partecipazione e sono andati a casa con gli starter kit che permettono di cominciare (in una festa ogni spicciolo conta, ma mettere in mano ai piccoli il minimo per cominciare a giocare al *maker* costa davvero poco).

Poi il laboratorio sulla realtà virtuale che coniugava la creazione dell’ambiente software con la storia del territorio. I check point con codice QR per accumulare punti validi nel contesto della festa.

Un’azienda raccoglieva curriculum interessanti sul lato tecnologico e organizzava laboratori di cibersicurezza in ambiente *mobile*. Alla festa del quartiere. E funzionavano.

 ![Cibersicurezza mobile alla festa di quartiere](/images/skacciakanazza.jpg  "Cibersicurezza mobile alla festa di quartiere") 

So che l’intenzione era di fare decollare droni a scopo dimostrativo oltre che per raccogliere foto dall’alto e aggiungere spettacolo allo spettacolo. Questo non è accaduto perché la legislazione italiana è killer: puoi fare volare un drone come ti pare, a patto di rispettare una serie infinita e vessatoria di condizioni che lo rendono completamente impossibile. Se anche prevalesse il buonsenso per quanto in violazione della legge (sostanzialmente, se si vede un drone in aria in Italia, la probabilità che sia illegale è del 99,99 percento), basterebbe un assessore tremebondo a bloccare tutto.

E alla lotteria si vincevano tavolette.

Insomma, perfino la festa per strada è diventata tecnologica e c’è pane per la riflessione. Più salamella, ovvio.

È stato fatto un grande lavoro sul piano musicale e per tutta la giornata si sono alternati su un piccolo palco *band* e singoli a livello veramente alto, con *jam session* improvvisate, bluesman, rockettari veri, tutta gente che nelle serate riempie i locali di mezza Lombardia. Il tutto mixato e controllato con tecnica e capacità, diversamente dalla tipica festa di paese.

Sul banco di regia c’era naturalmente un Mac.

 ![Il Mac sul palco per la musica](/images/regia-kanazza.jpg  "Il Mac sul palco per la musica") 