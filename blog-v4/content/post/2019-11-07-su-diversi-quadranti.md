---
title: "Su diversi quadranti"
date: 2019-11-07
comments: true
tags: [Xiaomi, watch, privacy, Mi, Watch]
---
Mi aspetto di leggere confronti tra watch e Mi Watch Xiaomi basati sulle specifiche tecniche, come ha fatto *Ars Technica* nel suo articolo [Il clone di Apple Watch di Xiaomi elimina quello che c’è di buono in Apple Watch](https://arstechnica.com/gadgets/2019/11/xiaomis-apple-watch-clone-removes-everything-good-about-the-apple-watch/).

Sarebbe bello che qualcuno confrontasse le [pagine dedicate alla privacy da Apple](https://www.apple.com/it/privacy/) con [quelle di Xiaomi](https://privacy.mi.com/all/it_IT/), che non sono nemmeno le peggiori in giro, e scrivesse chiaro che parliamo di oggetti provenienti da galassie diverse.

Anche se i quadranti, per usare un eufemismo, si assomigliano.
