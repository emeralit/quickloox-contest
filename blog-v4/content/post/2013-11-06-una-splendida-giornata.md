---
title: "Una splendida giornata"
date: 2013-11-06
comments: true
tags: [OSX, Server, AppleII]
---
Ricorda che se installi Server lui fa cose inenarrabili nel disco e, a Server spento con tutti i servizi disattivati, una installazione non andata a buon fine è comunque in grado di creare problemi (per esempio un diluvio di messaggi di errore che rallentano a dismisura l’attività e *log* del processo *devicemgrd* che riempiono il disco rigido al ritmo di cinquecento megabyte al minuto).<!--more-->

Per la mia esperienza, qualsiasi installazione di Server non va a buon fine, nel senso che funziona tutto al 99 percento e l’uno percento che manca è in grado di succhiarti la giornata. Che ho passato a modificare file di configurazione nascosti a profondità inusitate.

La soddisfazione è che ho usato con profitto una cosa appresa facendo la mia parte per il [sushi su OS X Server](http://sushi.apogeonline.com/ebook/os-x-server) edito da Apogeo. Non perché sia da comprare il sushi – qui non si fa autopubblicità o almeno si cerca di restare a un livello decoroso – ma perché se è servito a me, probabilmente sarà servito o servirà a qualcun altro e questo vale una giornata di sudori freddi.

Invece, il consiglio da acquistare è questo, gratuito: attento a Server. La crosta è amichevole, semplice e funzionale. Appena si incrina, sei nel Terminale, in Console, in Monitoraggio attività, dentro i forum Apple e in generale su Google. E devi essere disposto a imparare cose più complesse del solito per tirartene fuori.

È anche una buona notizia per l’avvenire dell’informatica: prima che il software di server diventi amichevole come quello da scrivania, devono passare sotto i ponti ancora un bel po’ di bit. A chi se occupa non mancherà lavoro per un pezzo.
