---
title: "Il suono di una sola tavoletta"
date: 2014-06-13
comments: true
tags: [Rai, iPad, Npr]
---
In un apologo Zen molto citato, il maestro del tempio chiede al giovane alunno di fargli sentire [il suono di una sola mano](http://www.101storiezen.com/21-il-suono-di-una-sola-mano.html).<!--more-->

National Public Radio pubblica una [storia bellissima e commovente](http://www.npr.org/blogs/ed/2014/06/11/320882414/ipads-allow-kids-with-challenges-to-play-in-high-schools-band) di come iPad aiuta ragazzi autistici e variamente disabili a prendere parte a una banda musicale e attraverso questo a esprimersi, inserirsi e superare per quanto possibile la disabilità. Racconta un insegnante che l’aiuto è anche nel ricevere nuova stima e considerazione da parte dei compagni:

>Una volta gli altri studenti tendevano a vedere solo la disabilità: “ragazzo in carrozzina, ragazzo in carrozzina”. Adesso è “ragazzo in carrozzina con iPad? Interessante”.

Qualcosa che con gli strumenti tradizionali non si riesce sempre a raggiungere, senza che si abbia una idea molto chiara del perché.

Visto che [se ne parlava](https://macintelligence.org/posts/2014-06-10-specialisti-al-lavoro-televisivo/), Npr è la radio statale degli Usa. Fa servizio pubblico vero e racconta storie come questa. Che nel nostro servizio pubblico sembrano davvero il suono di una sola mano.