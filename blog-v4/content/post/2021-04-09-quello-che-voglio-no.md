---
title: "Quello che voglio(no)"
date: 2021-04-09T00:16:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [iPhone, jailbreak, Procter & Gamble, App Tracking Transparency] 
---
I tempi eroici del *jailbreaking*, quando aprivano la sicurezza degli iPhone per poter caricare app non previste da App Store.

L’ho fatto anch’io: avevo ottenuto un iPhone di prima generazione, che senza *jailbreak* non avrebbe mai potuto funzionare fuori dagli Stati Uniti e senza un contratto con At&T.

C’era chi sosteneva che il *jailbreak* fosse una questione di libertà dell’utente. Perché *l’ho comprato e ho il diritto di farci quello che voglio*.

Ci parlerei volentieri oggi, quando sono cambiate un po’ di cose e, per esempio, [Procter & Gamble partecipa in Cina a test di una tecnologia di tracciamento pubblicitario](https://macdailynews.com/2021/04/08/procter-gamble-worked-with-china-on-tech-to-sidestep-apple-privacy-rules/) in grado di acquisire dati delle persone senza il loro consenso e in barba alle prossime nuove regole di Apple.

>La mossa rientra in una iniziativa più ampia del colosso nella vendita al dettaglio di beni di consumo, che vuole prepararsi per un’epoca nella quale nuove regole e preferenza del consumatore limitano i dati a disposizione dei reparti marketing.

Un jailbreak di oggi potrebbe certo consentire l’installazione di app che scavalcano le regole di Apple per tracciare senza consenso del tracciato.

*L’ho comprato e ho il diritto di fargli fare quello che vogliono*. Senza neppure sapere bene che cosa facciano. Un po’ meno accattivante, come slogan.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*