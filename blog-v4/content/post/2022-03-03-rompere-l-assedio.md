---
title: "Rompere l’assedio"
date: 2022-03-03T01:18:48+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [AppleGazette, iWork, Office, Handoff]
---
A volte una questione si può inutilmente complicare quando, invece, è estremamente semplice.

A volte, una nozione molto semplice non si trasmette perché sembra ovvia, o per mancanza di coraggio, o proprio per la sua semplicità. Invece bisogna sempre dare fiducia alla semplicità.

Penso all’articolo di *AppleGazette* [Tre ragioni per cui preferisco iWork a Microsoft Office](https://www.applegazette.com/iwork/3-reasons-why-i-prefer-iwork-to-microsoft-office/). Dal titolo si potrebbe pensare a uno sfogo personale, mentre un paragrafo interno titola con saggezza *Le ragioni per cui iWork è una buona scelta per la maggior parte delle persone*.

Sono, appunto, tre, semplici ma non ancora banali, quindi perfette:

- sincronizzazione con iCloud;
- gratuità;
- interfaccia più pulita.

La gratuità effettivamente è *borderline* verso la banalità, ma ci sarebbe molto da dire sugli altri due punti, a partire dall’integrazione di iCloud con tutto l’ecosistema e da comodità tipo Handoff, per iniziare un documento su Mac e proseguirlo su un iPad eccetera.

Discutibile? Sicuramente. Tuttavia, sto sulla quarta ragione, non compresa nell’articolo se non inconsapevolmente:

![Circondati da ogni lato](/images/office-banners.jpg "Circondati da ogni lato")

L’articolo sul preferire iWork è semisepolto dai banner pubblicitari di Office.

Office è un assedio, un abbraccio mortale, una infestazione. Anche dove avesse un senso, i casi dove è semplicemente frutto di una pressione bieca e soffocante sono sufficienti a oscurare qualunque buona ragione.

La quarta ragione è il primo obiettivo, per chi si ritrovi assediato: rompere l’assedio.