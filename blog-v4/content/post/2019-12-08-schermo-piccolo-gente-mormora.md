---
title: "Schermo piccolo, gente mormora"
date: 2019-12-08
comments: true
tags: [Drang, Python, iPhone, Pythonista]
---
C’è Dr. Drang che si diverte a [scrivere un programmino in Python](https://leancrew.com/all-this/2019/12/python-countdown/) per risolvere la prova di abilità linguistica di una trasmissione televisiva.

(Programmino nel senso della lunghezza; la dissertazione è interessante e certamente ci ha speso sopra ben più di un quarto d’ora).

Alla fine mostra un output del programmino.

Su un iPhone, dentro [Pythonista](https://omz-software.com/pythonista/).

La sensazione è quella di un computer al lavoro sul software che gli è stato ordinato di eseguire. Ma forse mi sbaglio ed è solo un telefono.