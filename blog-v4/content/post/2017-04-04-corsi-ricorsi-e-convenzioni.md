---
title: "Corsi, ricorsi e convenzioni"
date: 2017-04-04
comments: true
tags: [Touch, Bar, MacBook, Pro]
---
C’era una volta un sistema operativo chiamato Mac OS X 10.7 Lion che, visto che cosa succedeva con iPad e iPhone al momento di fare scorrere una pagina con il dito, adottò lo *scorrimento naturale* e invertì l’effetto che si era sempre ottenuto chiedendo lo scorrimento al mouse o al *trackpad*. (Rimase l’opzione di disattivarlo).<!--more-->

L’opinione si divise in due partiti: chi si [si era abituato](http://www.johnchow.com/natural-scrolling-vs-reverse-scrolling-which-do-you-prefer/) praticamente subito al nuovo corso e chi lo riteneva invece [innaturale](http://www.businessinsider.com/mac-os-x-lion-natural-scrolling-2011-7) e sbagliato.

Passati diversi anni, il nuovo scorrimento è la norma incontrastata. Nessuno più lo critica. A ben guardare, non è per niente naturale. Neanche lo scorrimento precedente era innaturale. Semplicemente c’era una convenzione che è stata cambiata con una convenzione diversa, più adatta alle nuove condizioni a contorno.

Oggi abbiamo la Touch Bar su MacBook Pro. Chi la avversa, chi la gradisce. Qualcuno ha provato a chiedere opinioni su Twitter e il referendum è praticamente pari. Un migliaio di risposte, *OK* il 51 percento, *KO* il 49 percento.

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Split right down the middle, at near a thousand votes <a href="https://t.co/K3TmXpWQMM">pic.twitter.com/K3TmXpWQMM</a></p>&mdash; Steve T-S (@stroughtonsmith) <a href="https://twitter.com/stroughtonsmith/status/848793587886239745">April 3, 2017</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Passeranno diversi anni e nessuno criticherà più la Touch Bar. Una convenzione più adatta alle nuove condizioni a contorno della convenzione precedente.