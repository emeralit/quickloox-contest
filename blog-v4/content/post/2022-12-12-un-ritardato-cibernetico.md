---
title: "Un ritardato cibernetico"
date: 2022-12-12T01:57:53+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [OpenAI, Eliza]
---
Fatta qualche prova con la [chat di OpenAI](https://chat.openai.com/chat), la trovo un onesto sistema esperto per il 2022, addestrato su un modello di dati abbastanza vasto da suggestionare le menti più labili dal punto di vista dell’analisi, con tutte le tare e i difetti che i sistemi aperti avevano negli anni novanta. Più dati a disposizione, poco o nient’altro.

Basta poco per trovare innumerevoli modi per fare venire alla luce le lacune nel modello di addestramento. La chat non impara da se stessa e non registra le infinite conversazioni cui viene sottoposta, il che nega uno dei presupposti non solo della presunta intelligenza artificiale come viene spacciata, ma proprio il *machine learning* in se stesso. Per essere sostanzialmente un sistema di machine learning, il fatto che non impari la degrada a curiosità temporanea, limitata e limitante per chi ci rimane invischiato. Succedevano le stesse cose con [Eliza](https://web.njit.edu/~ronkowit/eliza.html) tanti anni fa; il sistema era solo più stupido e in grado di abbindolare gente più stupida. Le soglie si sono alzate.

Se fosse tutto qui, niente di nuovo, circolare, non c’è niente da vedere.

Invece, ci si è accorti che la chat può scrivere codice e compiere altre azioni che potrebbero effettivamente fare risparmiare tempo a uno sviluppatore.

Non tutti si accorgeranno però che il sistema produce tranquillamente errori, che non tutti hanno voglia di verificare. Un esempio:

![I conteggi sbagliati sono frutto di qualche bug brutto e non di mancanza di addestramento](/images/openai.png "I conteggi sbagliati sono frutto di qualche bug brutto e non di mancanza di addestramento.")

Solo per esempio, nella prima frase c’è visibilmente una sola lettera *v*, mentre la chat ne conta due.

Nella seconda frase, le *b* sarebbero quattro. Ancora, se ne vede una sola. All’opposto, le *u* risulterebbero una, quando sono cinque. Non ci sono *a* nei numeri in inglese da zero a nove; la frase iniziale ne contiene una. La seconda è l’indicazione del numero delle *a*… che sarebbero *sette*.

Ho anche pensato che il sistema non riuscisse a ragionare ricorsivamente e considerasse la mia frase di input invece che la sua risposta; anche qui, le cifre non tornano.

Ci sono problemi grossi sia a livello di rappresentazione interna dei dati sia nei meccanismi sedicenti intelligenti. Contare le lettere dentro una breve frase è un compito elementare alla portata di umani impegnati nella scuola primaria che solo riescano a mantenere un minimo di concentrazione.

In pratica abbiamo a che fare con un ritardato cibernetico. Mentre i ritardati umani sono persone speciali da supportare e sostenere anche più delle altre perché possano realizzare pienamente il loro potenziale e inserirsi con tutto il successo possibile nella società, quello cibernetico è un frutto bacato dell’arroganza intellettuale di chi pensa che un magazzino di dati dotato di qualche regola di inferenza possa avvicinare il funzionamento di un’intelligenza.

Auguri a chi ci voglia programmare o spacciarlo per intelligenza artificiale e ci rivediamo tra qualche anno, non necessariamente pochi.