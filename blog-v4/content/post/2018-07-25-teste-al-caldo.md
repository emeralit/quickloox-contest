---
title: "Teste al caldo"
date: 2018-07-25
comments: true
tags: [MacBook, Pro]
---
Pochi giorni di polemiche, per lo più sterili, e arriva [l’aggiornamento firmware che sistema la regolazione delle temperature](https://www.bloomberg.com/news/articles/2018-07-24/apple-unveils-fix-for-macbook-pros-not-hitting-advertised-speeds) sui nuovi MacBook Pro. Alcuni avevano constatato che le macchine limitavano la potenza del processore a un livello che portava le prestazioni sui livelli del modello precedente.

Il problema, ha spiegato Apple, è nella dimenticanza di una firma digitale. I dettagli non sono stati forniti e le ipotesi più plausibili ruotano attorno all’idea che il *chip* ausiliario T2, esclusivo di Apple e presente sulle nuove macchine, controlli l’interno del computer attraverso collegamenti cifrati e in assenza di un certificato sbagli oppure ometta operazioni vitali per avere il miglior compromesso tre potenza e temperatura. Apple si è pure scusata:

>Con ogni cliente che abbia rilevato sul proprio nuovo sistema prestazioni inferiori a quella ottimale.

Dicevamo, arriva un aggiornamento *firmware*.

È che ho letto decinaia e decinaia di pareri concordi sull’errore di mettere un processore i9 in un computer così sottile. E avessi letto un solo saputello dire *Già, mi sono sbagliato, lo spessore non c’entra*. Neanche uno.

Gli esperti di raffreddamento della domenica soffrono il caldo estivo più di altri.