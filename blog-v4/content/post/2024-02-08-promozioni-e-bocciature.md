---
title: "Promozioni e bocciature"
date: 2024-02-08T00:31:33+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [Super Bowl, Super Bowl LVIII, Kansas City Chiefs, San Francisco 49ers, 49ers, Chiefs, Dazn, Nfl Game Pass]
---
Guardo il [Super Bowl](https://www.nfl.com/super-bowl/) *sempre* e figuriamoci se mi perdo questa domenica [una sfida per il titolo tra Kansas City e San Francisco](https://www.nfl.com/super-bowl/event-info/).

Negli ultimi anni [mi sono trovato bene con NFL Game Pass](https://macintelligence.org/posts/2022-02-08-è-tutto-un-prepararsi/), novantanove centesimi per accesso al Super Bowl e ai contenuti collegati. Così mi sono dispiaciuto nel constatare via app che la lega americana ha demandato la gestione del Game Pass a Dazn, la quale da diversi anni trasmette l’evento in streaming, ovviamente a pagamento.

Dazn, con tutto il rispetto, non mi piace.

Prima di tutto, non è stato immediato registrare un account per pagare esclusivamente il Game Pass del Super Bowl. Mi sono trovato di fronte agli abbonamenti standard del servizio, di cui non ho titolo per valutare la convenienza, che comunque non mi interessano.

Poi ho visto che su vari media il Game Pass a novantanove centesimi [viene fatto passare come promozione speciale](https://www.punto-informatico.it/dove-e-come-vedere-il-super-bowl-2024-in-italia/). Non lo è. Costa la stessa cifra da almeno due anni, forse più ancora. NFL non lo ha mai definito *promozione*. Non credo che chi ha scritto abbia inventato di testa propria; più probabilmente ha copiato un comunicato stampa, scritto a questo punto per infinocchiare i poco informati e gli sprovveduti.

Infine, durante la registrazione dell’account, mi sono trovato davanti a due caselle molto tipiche. La prima: clicca per ricevere notizie dalla NFL. OK.

La seconda: clicca per NON ricevere comunicazioni da Dazn. (il maiuscolo è mio).

Due caselle dal contenuto logicamente simile che però richiedono due comportamenti opposti per avere lo stesso risultato. Queste strategie d’accatto per raccogliere profili di gente distratta si chiamano *dark pattern*, sono deprecate a qualsiasi livello del marketing, del web, delle *user experience*, ovunque.

Per il Super Bowl si fa tutto (legale e sensato), ma assicurarsi il posto a vedere per questa edizione ha lasciato un retrogusto sgradevole.