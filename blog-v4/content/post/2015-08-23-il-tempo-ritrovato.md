---
title: "Il tempo ritrovato"
date: 2015-08-23
comments: true
tags: [watch, Windows, Mac, Matteo]
---
**Matteo** indossa un [watch](http://www.apple.com/it/watch/) da tempo ben superiore al mio e mi ha inviato le sue considerazioni, che riporto di seguito da **grassetto** a **grassetto**, ringraziandolo per la condivisione.<!--more-->

**Sono entrato in possesso di Watch da quando è stato possibile averlo in Italia.**

È da circa un mese che avrei voluto condividere le mie impressioni, che rispecchiano poi anche [quelle di Dalrymple](https://macintelligence.org/posts/2015-06-18-ci-piace-vincere-facile/) quando scrive che il computer da polso ha avuto un effetto profondo sulla sua vita.

Lo stesso posso dire per la mia, e naturalmente in meglio, perché in ciò che scrive l’americano sulla maggiore attenzione prestata alla qualità della propria vita, io mi ci trovo in pieno.

È comodo sapere quanto si è attivi, così come è comodo terribilmente non dover estrarre il telefono ad ogni pié sospinto per messaggi o mail più o meno interessanti.

Lo stesso vale per le indicazioni stradali, quando, camminando per il centro storico di una città sconosciuta, guardare semplicemente l’orologio risparmia ‘fatica’.

Tra l’altro, in tutti questi casi, ad avvantaggiarsene è la batteria dell’iPhone, il che non è male.

A proposito invece della sua batteria, non mi è mai capitato, in circa due mesi, di scendere sotto il 20 percento di carica, pur indossandolo per circa 17 ore e praticando almeno  mezz’ora di attività fisica, lasciando attivo il rilevamento della frequenza cardiaca per una maggiore accuratezza sulla misurazione delle calorie.

Per ultimo aggiungo che, pur avendo io scelto online il modello (ma già partivo convinto di acquistare uno Sport nero, l’unico dubbio era la taglia), devo dire che sono rimasto piacevolmente sorpreso dalla morbidezza del cinturino in elastomero, superiore alle mie aspettative.

Concludendo, posso solo dire che la soddisfazione che finora mi ha dato lo Watch è pari solo a quella che mi ha dato il mio primo Mac dopo dieci anni di Windows, ovvero provo la sensazione che, fino al momento del suo arrivo, **ho solo sprecato il mio tempo**.

*Tempo ritrovato* è uno degli slogan usati da Apple. Funziona perché, così come lo scopo di iPhone non è telefonare, l’intento di watch non è mostrare l’ora.