---
title: "Consigli di seguito"
date: 2017-01-29
comments: true
tags: [Mashable, Tidbits, 5k, LG, Apple, Reddit]
---
Su Reddit [scrivono](https://www.reddit.com/r/apple/comments/5p3srp/apple_shutters_reviews_of_lg_ultrafine_5k_display/) che Apple cancella le recensioni negative su Apple Store di un monitor 5k di LG in quanto ha problemi di funzionamento con Mac.

Naturalmente, la notizia corre veloce per tutta la rete. Solo che non è una notizia, bensì una invenzione. L’estensore la completa con una correzione:

>Apple ha appena autorizzato le recensioni del monitor. È anche emerso che non erano mai state autorizzate [quindi, non essendo mai stato pubblicato alcunché, è impossibile, che sia stato cancellato qualcosa]. La mia asserzione che Apple abbia deliberatamente spento le recensioni era scorretta.

Uno si chiede come mai scrivere una cosa del genere senza verificare che sia vera ma via, è domenica, stiamo rilassati.

Mentre la notizia della censura correva velocissima, quella della bufala procede con i piedi di piombo. Siti come TidBits [verificano e correggono](http://tidbits.com/article/17017). Altri, come *Übergizmo*, [ignorano gli sviluppi](http://www.ubergizmo.com/2017/01/apple-removing-bad-lg-ultrafine-5k-display-reviews/).

Io seguirei TidBits.
