---
title: "Se questo ė un gioco"
date: 2017-06-27
comments: true
tags: [Cyoa, Zork, Myst, librogame, Locusta, Colombini, Medusa]
---
Noi li chiamavamo *librogame* e gli americani *Choose Your Own Adventure*, scegli la tua avventura, ma le differenze finiscono qui. Sono nati assieme alle avventure testuali per computer, cronologicamente, quando la carta poteva ancora rappresentare una alternativa valida.<!--more-->

Hanno segnato la crescita di tanti ragazzi e ora qualcuno ne ha [analizzato la struttura](http://samizdat.cc/cyoa/) con diagrammi e grafi che ne fanno apprezzare la bellezza circa come quando vediamo una geometria perfetta in natura.

Viene voglia di mettersi al tavolo e creane uno solo per il gusto di tracciare schemi così. E d’improvviso si azzera la distanza tra oggetti esteticamente privi di qualunque fascino e mondi che ne trasudano a litri, irreali e inquietanti, come [Myst](http://cyan.com/games/myst/): il fascino era solamente nascosto nella struttura.

Chissà che schemi uscirebbero nel caso della magnifica [Locusta Temporis](http://www.quintadicopertina.com/index.php?option=com_content&view=article&catid=38:locusta-temporis&id=61:locusta-temporis) di [Enrico Colombini](http://www.erix.it/) e del suo motore [Medusa](http://www.erix.it/medusa.html). Con una estate davanti, potendo investire quei critici tre euro e cinquanta, l’investimento definitivo. I ricchi potrebbero persino scialacquarne sette di euro, per l’[edizione speciale](http://www.quintadicopertina.com/index.php?page=shop.product_details&flypage=flypage_images.tpl&product_id=25&category_id=6&keyword=locusta&option=com_virtuemart&Itemid=56&vmcchk=1&Itemid=56).

Chiamiamoli giochi se ci sentiamo più tranquilli. Sono comunque molto di più.
