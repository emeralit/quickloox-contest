---
title: "Dalla parte dei deboli"
date: 2024-04-08T16:03:20+01:00
draft: false
toc: false
comments: true
categories: [Software, Blog]
tags: [Comma, Muut, Quickloox, Calca, BBEdit, WordPress]
---
Se ripeti sempre le stesse cose, sei vecchio. Se ripeti per dare loro enfasi, in un contesto adatto, consapevole di farlo, sostieni le tue opinioni.

Così linko i post [Software francescano](https://macintelligence.org/post/2013-12-15-software-francescano/) e [Software francescano per iPad](https://macintelligence.org/post/2014-01-01-software-francescano-per-ipad/) a ribadire un principio che mi ha folgorato all’università: partire dagli strumenti deboli.

È una parte delle ragioni per cui mi trovo oggi a bloggare con [Hugo](https://gohugo.io) e commentare con [Comma](https://github.com/Dieterbe/comma). È una parte della risposta a varie domande: perché niente database, perché non un sistema facile, perché niente [Markdown](https://www.markdownguide.org) e così via.

Alcune risposte sono contingenti (per esempio il supporto di Markdown è in arrivo), altre vanno più nel profondo, come il mio rapporto con il bricolage. Mio padre era un fenomeno del fai-da-te e ha letteralmente trasformato la sua casa, dall’impianto elettrico ai mobili. Era meno bravo nel rapportarsi con i collaboratori: mi chiedeva di aiutarlo, *passami il cacciavite*, io ne vedevo sette e gli chiedevo quale, lui seccato rispondeva *quello a stella, no?* ed erano quattro. Dopo poco si arrabbiava dato che *gli toccava fare tutto da solo*. Io, dodicenne, mi ritiravo dentro qualche libro.

Ho sviluppato un rispetto profondo per i bricoleur, che riescono a fare cose incredibili, e una grande estraneità al bricolage, che evito ovunque io possa. Preferisco cose già pronte, che risolvono il mio problema e non mi chiedono tempo.

Chiaro che capisco benissimo chi si trova nel mondo del software e chiede cose già pronte, che risolvono il problema e non chiedono tempo. È quello che faccio io nella vita vera. Invece, nel digitale, sono bricoleur appena posso, spesso per procura a causa dei miei limiti, ma sempre pronto a spendere una notte a implementare un sistema, così come non sono disposto a farlo per raddoppiare una presa del salotto o sistemare la molla dello sciacquone.

L’analogia non è perfetta. Nel digitale c’è un enorme problema di ignoranza. C’è un enorme problema di massificazione. C’è un problema immenso di strumenti, inadeguati ai compiti. Perché fanno troppo, fanno male, si tengono il controllo invece di potenziare chi li usa.

La mia economia personale genera poche decine di fatture l’anno. Mi serve un database? Un foglio di calcolo? Sta tutto su un file [Calca](http://calca.io). Aggiungo i dati del mese e ho la situazione precisa di fatturato, quota tasse, andamento per mese, per cliente. È un file di testo Markdown di pochi kilobyte, lo gestirebbe anche uno [ZX81](https://www.computinghistory.org.uk/det/184/Sinclair-ZX81/). Il raffronto anno per anno è una sezione dove ogni anno aggiungo una riga nuova ed è tutto pronto. Chiaro che a una azienda vera serve un gestionale. A me no.

Il blog. Come ho trovato i due post linkati all’inizio? Con la ricerca. La ricerca di Quickloox si mangia quella di WordPress a colazione, grazie al lavoro di [Mimmo](https://muloblog2.netlify.app). Se fallisse, avrei a disposizione quella di [BBEdit](https://www.barebones.com/products/bbedit/), che è di eccellenza. Per le modifiche di massa ho le espressioni regolari, su una base di file di testo puro che occupano il minor ingombro possibile. Se è un blog, e non una casa editrice, non serve un database. Lo stesso varrà per i commenti di Comma. Sono file di testo dentro una cartella.

Ho insistito per un sistema di gestione dei commenti locale. Negli anni ho bloggato più volte per altri enti, con altre infrastrutture. Una alla volta, sono sparite tutte. Anche la mia è sparita. Sai che è successo? Siccome avevo il controllo di quello che ci stava sopra, ho preso un’altra infrastruttura e ho proseguito.

La grande promessa tradita dell’informatica è stata dare a ciascuno gli strumenti per realizzare *il proprio* sistema. Invece si insiste per dare a tutti strumenti tutti uguali, per fare cose tutte uguali. Si sfugge a questo gorgo solo con la conoscenza e sono determinato a detenere abbastanza conoscenza per fare in modo diverso da come fanno tutti e come si è sempre fatto. Concetti che vanno bene per aziende paurose di fare meglio, se proprio.

Comma migliorerà. Se non migliorerà, può darsi che un giorno lo cambieremo. Magari avremo avuto sei mesi di tempo per costruire un nuovo sistema e non i pochi giorni di preavviso che abbiamo ricevuto da Muut. Un giorno magari non ci sarà più Hugo, il cambiamento fa parte della nostra vita. Ma preferisco sapere che il miglioramento del blog dipende da me, non da una punto-com costretta a produrre strumenti forti, sempre più forti, sempre più percentualmente inutili.

Per me strumenti deboli, grazie. Non so se ho risposto ai quesiti, ma d’altronde chissà chi è arrivato fino a qui.
