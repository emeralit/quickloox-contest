---
title: "Le magie della punta"
date: 2022-12-30T20:46:58+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Apple Pencil]
---
Oltre a [un nuovo iPhone](https://macintelligence.org/posts/2022-12-29-un-nuovo-piano-quinquennale/), è arrivata anche una [Apple Pencil](https://www.apple.com/it/apple-pencil/) di seconda generazione.

Non per me. Se ho dei talenti, risiedono in ambiti diversi dal disegno a mano libera; preferisco di gran lunga scrivere a macchina che a mano e ho sempre considerato uno dei punti di forza di iPad liberarci dallo strumento di input, che diventa il tocco diretto anzoché richiedere la mediazione di un apparecchio. Amo la sensazione di libertà che dà il controllo di iPad con le dita e non la scambio con l’indubbia precisione di una punta scrivente.

Ciò detto, ammiro la magia che pervade questo oggetto. Dovendo funzionare su iPad Pro è una Apple Pencil di seconda generazione, che si attacca magneticamente sul bordo destro di iPad (tenendo a destra i pulsanti del volume) e con lo stesso gesto inizia a ricaricarsi.

Il feeling mi piace e il setup è, lo dicevo, magico: si attacca la Pencil a iPad (sulla costa, ad aumentare la larghezza, non appoggoata sopra il bordo ad aumentare lo spessore). Sullo schermo, vicino alla Pencil, ne appare una riproduzione, che poi sparisce. Fatto: configurazione effettuata.

L’unica altra cosa da sapere è che il collo della Pencil è una zona sensibile al tocco, che ha le funzioni di un pulsante per cambiare modalità. Secondo la app in uso o la configurazione nelle Impostazioni passa da scrivere a cancellare, oppure da matita a pennarello, o cambia colore, dipende. Il pulsante è del tutto invisibile e bisogna ricordarsene.

Ho fatto qualche prova banale e, per quanto non sia la mia tazza di tè, usare la Pencil su iPad Pro è un piacere. Latenza impercettibile, tratto sicuro, naturalezza. Più di me conta la reazione delle due giovanissime svergognate che facevano la posta al papà per mettere le mani sulla novità. In capo a un quarto d’ora ciascuna, quattro anni e otto anni, disegnavano senza ritegno così come amano fare con carta e matite. Tutto quello che ho fatto io è dirgli di usare Note.

Sicuramente Apple Pencil costa più di una matita, centoquarantanove euro. Fosse stato per me, mai sarebbe arrivata sulla mia scrivania. Eppure non trovo alcuna ragione per sconsigliarla; nel giro di qualche giorno comincerà a essere impiegata – non da me – per lavoro serio e potremo forse rivedere il giudizio. Nel frattempo, per il poco che mi sono concesso, è tutta soddisfazione.