---
title: "Esche proibite"
date: 2023-08-20T13:18:25+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Web]
tags: [Florida Atlantic University, Gruber, John Gruber, Daring Fireball, Apple Watch]
---
Dopo una estate sufficientemente sudata posso solo concordare con la ricerca della Florida Atlantic University sullo [sporco presente sui cinturini degli orologi e l’efficacia dei mezzi di pulizia](https://www.scirp.org/journal/paperinformation.aspx?paperid=125218). Nel mio caso, il cinturino di pelle di watch ha vero bisogno di una ripulita. Sporco, naturalmente, significa carica batterica.

Come [ha notato John Gruber](https://daringfireball.net/linked/2023/08/19/watch-bands-get-dirty), nello studio ci sono quattordici occorrenze di *apple* e, in tutti e quattordici i casi, si parla di *apple cider vinegar*, aceto di sidro di mele, come metodo di pulizia.

In altre parole, lo studio si occupa di *qualsiasi* cinturino di *qualsiasi* orologio.

Questo non ha impedito al *New York Post* di titolare [I cinturini di Apple Watch e Fitbit trasportano livelli scioccanti di batteri: parlano gli esperti](https://nypost.com/2023/08/17/apple-watch-fitbit-wristbands-carry-shocking-levels-of-bacteria-experts/) e a *9to5Mac* di rilanciare l’articolo come [Secondo un nuovo studio, il tuo cinturino del tuo Apple Watch potrebbe facilmente essere ricoperto di batteri](https://9to5mac.com/2023/08/18/apple-watch-bands-bacteria-cleaning-study/). Come se il problema fosse specifico. (Non ho voglia di andare a recuperare vecchi link ora, ma è tradizione che appaiano periodicamente articoli sulla carica batterica di tastiere, mouse, schermi, qualsiasi cosa venga usata intensamente e in continuazione senza pulirla, come se fosse una sconvolgente rivelazione)

Il *clickbait* è sempre più noioso, e stancante. Inoltre, il regolamento vieta da sempre di colpire sotto il cinturino. 

*Gli aggiornamenti in agosto del blog potrebbero essere irregolari o infrequenti.*