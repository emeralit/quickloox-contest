---
title: "L’emulo dubbioso"
date: 2021-12-18T19:10:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Sheepshaver, Mac OS 9, Columbia, PCE.js] 
---
Ho consigliato a un amico desideroso di emulare Mac OS 9 il ricorso a Sheepshaver, come mi pare [ben sintetizzato in questa pagina](http://www.columbia.edu/~em36/macos9osx.html) della Columbia University.

Confesso però che non ho contatti con Sheepshaver da un bel po’ e le mie esperienze in materia sono talmente lontane dal risultare teoriche.

Qualcuno ha toccato con mano più di recente e sa corroborarmi, per favore? L’amico ha progetti abbastanza seri di utilizzo e quindi serve una emulazione solida, non progetti anche recenti e spettacolari come [PCE.js](https://jamesfriend.com.au/pce-js/) però poco concreti.

Grazie anticipate.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per Qui
ckLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._