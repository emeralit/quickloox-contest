---
title: "Il grigio che vince"
date: 2019-10-02
comments: true
tags: [Brooklyn, Nets, Nba, design, Jobs]
---
La maggior parte delle diatribe attorno a Apple si decodifica facilmente se si pensa al tema del design (*how it works*, diceva Steve Jobs, come funziona).

I detrattori di Apple ragionano in modo industriale (non è un insulto, né una *diminutio*, ma una impostazione che ha molto senso in molti campi professionali) e pensano che prima nasca il prodotto e poi si pensi a come funziona.

Apple ragiona in senso opposto e per questa ragione viene accusata di essere *design-oriented*: prima si pensa a come funziona e in base a quello nasce il prodotto.

*Design-oriented* non dovrebbe essere una accusa, ma una constatazione: la diversità di Apple sta nella constatazione che sì, fabbrica computer. Ma non *vende* computer, bensì apparecchi di varia utilità. Ed è qui che si consuma la distinzione: il detrattore vede Mac o iPhone come un computer, un insieme di parti assemblate. Apple non vede le parti assemblate, ma il complesso del loro funzionamento.

 Le conseguenze sono evidenti, appena uno ci pensa: un iPhone è più veloce di un Android (il complesso del funzionamento), ma il detrattore dirà che Android è superiore perché contiene più Ram. Che venga usata con meno efficacia, è un dettaglio.
 
 Per comprendere meglio l’idea, cambiamo campo, in senso letterale, e andiamo su quello dei Brooklyn Nets, cestisti NBA. La squadra è un prodotto: è basket professionistico e muove un giro miliardario. Gli spettatori pagano per seguire gli atleti (in realtà seguono la palla, solo chi è addentro impara a seguire veramente i giocatori per capire il gioco, ma non divaghiamo). Nondimeno, la società ha speso molto in tempo e risorse per [decidere l’aspetto del campo](https://www.espn.com/nba/story/_/id/27683304/brooklyn-nets-going-gray-fresh-new-look).
 
 Che sarebbe stato grigio. Una novità, destinata a contraddistiguere istantaneamente il campo della squadra. Ma doveva essere chiaro il giusto e scuro il giusto, per restituirsi in modo ottimale alle riprese televisive e fare risaltare i giocatori (di norma nell’NBA una delle due squadre veste di bianco). E poi: aree più scure oppure la zona di tiro da due punti? E come scrivere il nome della squadra a bordo campo?
 
 Chi lavora vendendo basket spende cifre ingenti anche per il campo di gioco, e non solo per la qualità del parquet. Dopo di che si spendono cifre ancora più ingenti per i giocatori, che sono il prodotto. Ma la squadra vende e gli spettatori comprano qualcosa che è più della somma delle parti.
 
 Certo, qualcuno farà il confronto tra squadre e concluderà che sono tutte uguali perché il numero dei giocatori è lo stesso. Oppure sommerà i centimetri e definirà la squadra migliore quella più alta. Non funziona così e i conti, alla fine, si fanno anche sul fatto che il campo di Brooklyn risulterà gradevole a tutti gli spettatori, in tribuna e alla TV. Nessuno degli spettatori dirà *che bel campo*, tutti guarderanno alla squadra e alle stelle della squadra. Ugualmente, se avranno avuto una bella esperienza sarà stato merito anche del campo, studiato in tutti i dettagli per funzionare e apparire al meglio. Inutile di per sé, fondamentale nel contribuire al successo della squadra e ultimativamente dell’NBA. Il design è come funziona.
