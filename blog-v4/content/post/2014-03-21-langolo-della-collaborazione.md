---
title: "L'angolo della collaborazione"
date: 2014-03-21
comments: true
tags: [Textastic, Php, BBEdit, DraftCode, Kodiak, Koder, CodeMaster, DietCoda]
---
Mi è arrivata una richiesta che non ho saputo soddisfare al meglio: un buon *editor* per programmare [Php](http://www.php.net) su iPad.<!--more-->

Quel poco che faccio, lo faccio con compiacimento su Textastic in edizioni differenziate [per iPad](https://itunes.apple.com/it/app/textastic-code-editor-for/id383577124?l=en&mt=8) (7,99 euro) e [per iPhone](https://itunes.apple.com/it/app/textastic-code-editor-for/id550156166?l=en&mt=8) (pure). C’è anche in [edizione Mac](https://itunes.apple.com/it/app/textastic/id572491815?l=en&mt=12), 7,99 euro, complemento molto utile per la sincronizzazione iCloud per esempio, solo che quando sono su Mac guai a chi mi tocca [BBEdit](http://www.barebones.com/products/bbedit/).

Oltre non saprei andare, perché non ho provato estesamente e seriamente altro software. Constato che esistono [Codeanywhere](https://itunes.apple.com/it/app/codeanywhere/id436736239?l=en&mt=8), [Code Master - Source File Editor](https://itunes.apple.com/it/app/code-master-source-file-editor/id502404926?l=en&mt=8), [DraftCode Php Ide](https://itunes.apple.com/it/app/draftcode-php-ide/id593757593?l=en&mt=8), [Koder Code Editor](https://itunes.apple.com/it/app/koder-code-editor/id439271237?l=en&mt=8), [Kodiak for Php](https://itunes.apple.com/it/app/kodiak-for-php/id542685332?l=en&mt=8), Naturalmente [Diet Coda](https://itunes.apple.com/it/app/diet-coda/id500906297?l=en&mt=8) eccetera. Tuttavia non ne ho esperienza diretta.

Chi ha voglia di aiutarmi? Gratitudine totale per tutto quello che potrò imparare. Grazie. 