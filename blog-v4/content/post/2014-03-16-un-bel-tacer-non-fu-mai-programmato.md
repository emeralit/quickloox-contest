---
title: "Un bel tacer non fu mai programmato"
date: 2014-03-18
comments: true
tags: [Lisp, Carmack, iPad, Sudoku]
---
In che angolo devono andare a nascondersi quelli che *su iPad non si può programmare*?<!--more-->

<blockquote class="twitter-tweet" lang="en"><p>Wrote a sudoku solver in Lisp on my iPad while traveling today. Filling idle bits of time with gainful exercise...</p>&mdash; John Carmack (@ID_AA_Carmack) <a href="https://twitter.com/ID_AA_Carmack/statuses/352649075528712193">July 4, 2013</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>