---
title: "Sviluppi editoriali"
date: 2019-05-13
comments: true
tags: [ia, writer, modes]
---
Non è solo che Editorial [manca da tempo di aggiornamenti significativi](https://macintelligence.org/posts/2019-02-19-la-voce-dell-intraprendenza/): scrivo molto e dunque le antenne sono sempre ritte nella direzione di programmi interessanti per scrivere su iPad come piace a me.

Questo [articolo di MacStories](https://www.macstories.net/ios/ia-writer-5-2-better-typography-and-external-library-locations/) mi ha riacceso l’interesse verso [IA Writer](https://ia.net/writer), che non ho mai considerato e però ha messo nel tempo dalla sua parte un componente importante: la tipografia. Che si aggiunge a flessibilità e potenza notevoli, come scrive Viticci:

>[La configurabilità del programma] mi consente di lavorare su articoli del team di MacStories in un text editor che supporta un modello personalizzato simile nell’aspetto al sito vero e proprio.

La cosa che mi trattiene è l’esistenza del programma per qualsiasi piattaforma concepibile, il che mi fa dubitare della effettiva efficacia di IA Writer su una piattaforma specifica. D’altronde di Viticci ci si può solamente fidare.

Intanto un piacevole effetto collaterale dell’articolo è stata la scoperta di [Kodex](https://kodex.app/), editor gratuito che si deve ancora sgrezzare – per esempio non conta le parole o i caratteri – e però promette moltissimo su alcuni aspetti, per esempio l’uso delle espressioni regolari e la modalità di lavoro multicursore. L’ho scaricato perché so che non lo userò in pianta stabile, ma mi tornerà utile.
