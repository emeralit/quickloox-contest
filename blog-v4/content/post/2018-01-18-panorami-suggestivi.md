---
title: "Panorami suggestivi"
date: 2018-01-18
comments: true
tags: [iPhone, Laforet]
---
Il fotografo Vincent Laforet, [scrive Kottke](https://kottke.org/18/01/gorgeous-50-megapixel-panoramas-shot-on-an-iphone-at-20000-feet), pubblica sul proprio account Instagram foto panoramiche scattate dal ventre di un aereo a sette chilometri di quota, con risultati affascinanti.<!--more-->

Sono certo che sia possibile farlo anche con computer da tasca diverso da un iPhone. Certo è che, con frequenza che in qualche misura è persino allarmante, quando appare qualcosa di fotograficamente straordinario (letterale) sul web, c’è di mezzo un iPhone.