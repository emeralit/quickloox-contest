---
title: "Siamo fuori registro"
date: 2023-11-10T22:52:32+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Axios]
---
Per avere una instantanea del dissesto etico complessivo in cui versa l’Italia, come al solito e purtroppo, si possono trovare infiniti esempi nel settore privato, ma nel pubblico si trovano le antologie.

Una delle parti più eticamente dissestate nel Paese è la scuola e non stupisce che i registri elettronici, adottati per ogni dove a sostegno della grande e progressiva trasformazione digitale, siano di qualità abissalmente inferiore rispetto a qualsiasi standard di mercato. Le interfacce utente e la famigerata User eXperience, in particolare, quando va bene inducono il vomito. L’utente, semplicemente, non esiste o è un errore di configurazione che disturba la stabilità olimpica dei registri stessi, perfetti quando nessuno li usa.

Ma parlavamo di etica. Con il nuovo anno, la scuola delle mie eredi ha adottato la versione 2.0 del registro in uso come 1.0 fino all’ultima estate (continuo a pensare che il numbering sia sbagliato e che abbiamo la versione 0.2 dopo la 0.1, ma tant’è).

Devo ancora approfondire su chi sia l’autore di una delle modifiche, tra preside, software house, ministero della presunta istruzione, che mi immagino allineati come in uno dei quiz gialli di [Pera Toons](https://www.mondadoristore.it/Chi-ha-ucciso-Kenny-Pera-Toons/eai978886790302/), *chi ha ucciso Kenny, Cacca o Sedere?*

Chiunque sia il colpevole, ora posso consultare il registro elettronico, con i voti, le assenze, i compiti, le lezioni e tutto il resto *solo se dichiaro di avere preso visione di tutte le circolari emesse dalla scuola*.

Il preside dice che purtroppo c’è un problema di genitori che non leggono le comunicazioni della scuola, e quindi. Peccato che ci siano due modi di prendere visione: uno è aprire la circolare e l’altro è un semplice *ok* che mostra nulla.

Così mi delizio nella lettura degli avvisi sul corso di nuoto (che mia figlia ha fatto l’anno scorso), per la materna che la secondogenita non frequenta, quelli già scaduti ma che tocca leggere uguale, la convocazione degli consigli interclasse per la secondaria (noi siamo in primaria) e poi forse anche qualcosa che ci riguarda, sì, magari ininfluente, o che avevo già letto attraverso uno dei vari altri canali a disposizione, oppure che avrei letto volentieri in un momento successivo perché ora come ora ho tempo di mettere la giustificazione ma non di leggere circolari.

il genitore da rieducare, in questo modo, si deresponsabilizza totalmente. Mette l’ok cieco e fa quello che voleva fare, dopo di che probabilmente avrà anche meno voglia di riprendere in mano il registro elettronico dato quello che lo aspetta.

Ed ecco l’Italia, quella dove tutti se ne fregano, comprese le autorità, le quali per lavarsene le mani mettono in piedi un sistema semicoercitivo, demoralizzante e sminuente, che peraltro non funziona.

Quando avrò una settimana libera parlerò di quanto è sbagliato il mio registro elettronico lato user experience.