---
title: "In carica per un tempo limitato"
date: 2021-01-22T00:42:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Mac, M1, Big Sur, Marco] 
---
C’è stato il tempo in cui andavo regolarmente in giro per lavoro con un PowerBook 100 nello zaino e due batterie, una installata e una di ricambio. La volta che dimenticai il portatile alla fermata dell’autobus lo recuperai grazie a una striscia di nastro adesivo presente sulla batteria di ricambio, che serviva a distinguerla dall’altra, e di cui riportai l’esistenza alla gentile ma diffidente signora che lo aveva ritrovato.

Più avanti, capitava di abbassare la luminosità del monitor per strappare qualche minuto in più di autonomia. Il disco rigido veniva fermato appena possibile, per i momenti in cui il computer poteva lavorare senza dovervi accedere. In caso di emergenza imparavi a scommettere sulla quantità di lavoro che conveniva produrre senza salvare, per prolungare il tempo a disposizione, e però a salvare davvero prima che l’autonomia terminasse.

Oggi si discetta sull’amministrazione automatica della batteria da parte di Big Sur a partire dal fatto che [il software tende a concentrare lo stato di carica operativo attorno all’ottanta percento](https://www.macworld.com/article/3602899/macos-big-surs-battery-optimization-you-cant-do-much-to-tweak-laptop-settings.html), visto che andare a cento può ridurre la vita complessiva della batteria.

Ci si lamenta inoltre della scarsa accessibilità ai parametri di controllo della carica. La mia modesta opinione è che gli ingegneri Apple sappiano molto meglio di me come funzionano le batterie dei Mac e come trattarle nel modo dovuto.

D’altronde ci sono i Mac M1. Per principio non incrocio le informazioni di questo blog con quelle che girano nel mio [gruppo Slack](https://app.slack.com/client/T03TMRQRC), salvo eccezioni, e faccio eccezione per **Marco** (grazie!) che ha appena scritto:

>Aggiornamento velocissimo sul nuovo MacBook Air M1. Oggi è stato usato dalle 8 alle 18 e la batteria segnava ancora 78%. È vero, non uso intensivo. Solo tabelle, gestionale, AirDrop, messaggi e Safari, ma con il mio Pro di tre anni fa me lo sogno di stare due o tre giorni senza caricare la batteria.

Così abbiamo portatili che possono tirare una giornata consumando un quarto della batteria se il carico di lavoro è leggero.

Così abbiamo il sottosistema di alimentazione che punta a caricare la batteria a quattro quinti e poi si rilassa, rallenta, riduce lo stress sull’apparecchiatura. La batteria, oltre ad avere un’autonomia senza rivali, avrà vita più lunga.

Tutto ciò dall’azienda che, si legge nei giorni dispari, ha cessato di innovare e indulge nell’obsolescenza programmata.