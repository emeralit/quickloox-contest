---
title: "Invisi ai naviganti"
date: 2017-02-14
comments: true
tags: [iPhone, navigatore]
---
Mi dispiace per la foto, poco chiara a causa dei sobbalzi dell’auto, la scarsa luce e l’obsolescenza dei mezzi.

Si intravede comunque il navigatore di serie del veicolo indicare un orario di arrivo che si discosta di un’ora da quanto indica iPhone, su un viaggio di due ore.

Aveva ragione iPhone, poiché circa un quarto d’ora più tardi il navigatore di serie ha aggiustato il proprio orario di arrivo. Ma un quarto d’ora è tanto, o è lento, secondo come si vuole interpretarlo.

La forza di iPhone sta nel poter assumere dignitosamente qualunque veste, da quella fotografica a quella di assistente di viaggio. Ma se surclassa l’equipaggiamento dedicato, vuol dire che è ancora più forte. Oppure, che i fabbricanti non hanno capito e producono oggetti per il loro fatturato, più che per i propri clienti.

 ![iPhone contro navigatore](/images/mappe.jpg  "iPhone contro navigatore") 