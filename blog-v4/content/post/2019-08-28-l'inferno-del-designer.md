---
title: "L’inferno del brutto designer"
date: 2019-08-28
comments: true
tags: [Bosch, Norman, Caffettiera, masochista]
---
Tendi a conoscere il funzionamento delle cose intorno a te e, quando sei in vacanza, puoi ritrovarti circondato da cose aliene, delle quali scopri il funzionamento per la prima volta o per breve tempo. L’esperienza complessiva colpisce per lo stato comatoso in cui versa il design delle cose di ogni giorno.

Ho accostato perfettamente al casello autostradale, uno di quelli automatici. Il lettore del Bancomat era ai limiti della mia apertura alare e ho completato la manovra a fatica. Per una persona brevilinea sarebbe stato impossibile, senza alzarsi in piedi dentro l’auto o scendere. È la prima volta che mi capita, ma non dovrebbe essercene una. Chi ha posizionato quel lettore era confuso, distratto o cattivo dentro.

Tra case vacanza, hotel, amici ospitanti e altro ho girato numerose chiavi in altrettante serrature e usato ancora più miscelatori in bagni e cucine. Occhi bendati, un tentativo: sicuro di ottenere l’acqua calda, o aprire? *Think again*, ripensaci. Le tue probabilità sono orrendamente molto più vicine al cinquanta percento di quanto credi. Menzione d’onore per certe porte con maniglia a tre stadi: in alto chiudi, in basso apri, in mezzo il neutro. L’interazione del meccanismo con la chiave è pura serendipità, per capirla devi rinunciare a cercarla.

Potrei andare avanti a lungo ma preferisco chiudere con il *top* assoluto: il tagliaerba sicuro.

Una maniglia e un pulsante. Il tagliaerba funziona solo se ambedue sono sollecitati, una stretta, l’altro premuto. Il bello però è che parte *solo* se uno dei due elementi viene azionato per primo.

Quale dei due? C’è un aiuto meccanico a spiegarlo, vero? Un componente è bloccato e si sblocca solo se l’altro componente è azionato.

L’aiuto *non c’è*. Maniglia e pulsante sono completamente indipendenti. Se cominci dall’elemento sbagliato il tagliaerba non parte, senza un perché. Ci sono numeri su maniglia e pulsante, a indicare il primo da utilizzare? No. Un’icona? No. C’è una ragione evidente e logica per partire obbligatoriamente da un componente? No, perché servono tutti e due e se ne molli uno l’apparecchio si spegne. Eppure.

Ti salvi solo con il pensiero laterale: ipotizzi che a concepire il sistema sia stato un emulo di Lewis Carroll che si divertiva, come nel [racconto di Alice](https://library.weschool.com/lezione/personaggi-alice-nel-paese-delle-meraviglie-lewis-carroll-alice-in-wonderland-16313.html), a smontare la logica e ricostruirla per divertimento a testa in giù. E allora, senza ragione alcuna, cambi la sequenza di impugnatura. Il tagliaerba parte e sembra un piccolo miracolo.

Dici che devi essere intelligente? No. Il sistema deve essere a prova di stupido. Ho visto una persona controllare le lame del tagliaerba elettrico, in cerca di un intoppo, senza scollegare il cavo… non c’erano intoppi, se non un design criptico. Quella persona si è esposta involontariamente a un rischio maggiore del dovuto, a causa del cattivo design. Il metodo per fare partire il tagliaerba deve essere ovvio come quello per fermarlo.

L’inferno è pieno di brutti designer di brutti strumenti, condannati per l’eternità a leggere [La caffettiera del masochista](https://www.amazon.it/caffettiera-masochista-design-oggetti-quotidiani/dp/8809785037). A rovescio.