---
title: "Il fine giustifica i mezzi"
date: 2014-10-14
comments: true
tags: [Apple, iOS, iPhone, iPad, Idc, Mac]
---
Apple, che pensa solo a iPhone e iPad volutamente trascurando Mac, non lascia niente di intentato pur di seppellire finalmente la gloriosa piattaforma trentennale.<!--more-->

Sono arrivati al punto di entrare (secondo Idc, tutto una grande stima da prendere con le molle, ma vale sempre) nei [primi cinque produttori di personal computer al mondo](http://www.itproportal.com/2014/10/09/apple-overtakes-asus-to-snag-fifth-place-for-pc-sales/). Il tutto in un quadro di [lieve diminuzione delle vendite complessive](http://www.pcworld.com/article/2824112/pc-shipments-down-just-slightly-in-the-third-quarter-idc-says.html#tk.rss_all), quindi in controtendenza.

Mai successo prima. Se necessario, pur di fare fuori Mac, secondo me sono capaci anche di conquistare il primo posto.