---
title: "Mi sento più leggero"
date: 2021-05-21T01:23:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Adobe, Creative Cloud] 
---
Ho deciso di correre un piccolo rischio.

Ho messo quello che potrebbe servirmi di Adobe dentro una macchina virtuale.

Ho buttato tutto il resto.

Niente più richieste di *riparazione* da parte di Creative Cloud, gigabyte tornati liberi, un gran senso di leggerezza.

Vediamo se riesco a farlo durare.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               