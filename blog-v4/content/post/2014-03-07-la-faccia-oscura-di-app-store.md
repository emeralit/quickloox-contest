---
title: "La faccia oscura di App Store"
date: 2014-03-07
comments: true
tags: [MacForge, Sourgeforge, Mac]
---
Facile lasciarsi prendere da App Store e dalla sua facilità. Mi lascio prendere anche da [MacForge](http://www.macforge.net), praticamente una spremuta di [SourceForge](http://sourceforge.net) per distillare applicazioni *open source*, libere e gratuite, selezionate per il fatto di funzionare su Mac.<!--more-->

Onestamente, nel caso, andrei direttamente su SourceForge. MacForge aveva un sito vecchio già ai tempi; adesso sembra ancora più vecchio e dà l’impressione di essere stato lasciato lì senza troppe cure. Tuttavia quei due o tre esperimenti di ricerca che ho provato hanno dato buon esito.

Non è questo, tuttavia; è vedere un numero di impressionante di programmi a disposizione di Mac eppure quasi totalmente misconosciuti da una parte e vestigia di quando un sito così faceva la differenza dall’altra. Un po’ come certe rovine archeologiche tuttora buone per ospitare un concerto o una rappresentazione teatrale; si vede che i tempi sono cambiati, ma resta un’atmosfera particolare.