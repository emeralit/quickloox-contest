---
title: "Oltre le regole"
date: 2021-06-28T10:26:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Dr. Drang, Keyboard Maestro, Mail, AppleScript] 
---
Quanto vale saper applicare lo scripting al computing di ogni giorno?

Uno dei vanti di [Hey.com](https://www.hey.com/index.html), che costa novantanove dollari l’anno, è la capacità di fare screening della posta elettronica in modo da semplificare l’esclusione di messaggi indesiderati.

Dr. Drang ha fatto una cosa del genere con [Keyboard Maestro](https://www.keyboardmaestro.com/) e poi, non soddisfatto, l’ha [replicata in puro AppleScript](https://leancrew.com/all-this/2021/06/hey-i-sped-up-apple-mail-rules/) per andare oltre le regole di Mail applicate manualmente.

Keyboard Maestro costa trentasei dollari. Hey.com offre un sacco di altre funzioni, naturalmente. Si potrebbe argomentare, abbastanza biecamente, che una buona conoscenza di AppleScript vale qualche decina di dollari, moltiplicata per i casi in cui la si mette a frutto.

Non è valore che interessa molto ad Apple, che punta su altri modi per ottenerne. Eppure dovrebbe. Una platea di utenti capace di fare scripting, più capace di altre platee, porterebbe _molto_ valore aggiuntivo alla piattaforma. Speriamo che i Comandi rapidi siano un nuovo inizio.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*