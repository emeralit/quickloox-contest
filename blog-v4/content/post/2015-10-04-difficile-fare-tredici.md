---
title: "Difficile fare tredici"
date: 2015-10-04
comments: true
tags: [Apple, Samsung, Android, JavaScript, iPhone, 6s]
---
Il tema non è che Apple venda [tredici milioni di iPhone 6s](http://www.apple.com/it/pr/library/2015/09/28Apple-Announces-Record-iPhone-6s-iPhone-6s-Plus-Sales.html) in svariate nazioni del mondo nel giro di un fine settimana.<!--more-->

È che riesce a farlo con una capacità di amministrazione di ingegnerizzazione, produzione e logistica la quale, pur di raggiungere i clienti, non esita a impiegare [due versioni diverse dello stesso processore](http://9to5mac.com/2015/09/28/a9-samsung-tsmc-speed/) per superare le difficoltà di approvvigionamento.

Con qualità finale tale che il più veloce apparecchio Android oggi esistente – il più veloce, immaginare i più lenti – [esegue JavaScript cinque volte peggio di un iPhone 6s](https://meta.discourse.org/t/the-state-of-javascript-on-android-in-2015-is-poor/33889) e pareggia neanche un umile iPhone 5 di tre anni fa (come quello che uso). Per chi non se ne fosse accorto: il web moderno trabocca di JavaScript.

(Si spiega anche un po’ di più perché la gente che compra Android poi lo usa su Internet molto meno di quelli che comprano un iPhone).

Vendere tredici milioni di computer da tasca in un fine settimana, cosa che qualsiasi altra azienda venderebbe l’anima al diavolo per riuscire a fare, è niente; farlo con capacità tecniche avanti di anni rispetto al resto del mercato, e qualità al vertice, è cosa vicina al prodigio.