---
title: "Cartiglio contro puntiglio"
date: 2023-02-25T20:31:45+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Mappe]
---
*Dimmi che sei tecnoumanista con un lato nerd perverso senza dirmi che sei tecnoumanista con un lato nerd perverso*.

Lungo tratto autostradale essenzialmente rettilineo, poco traffico, velocità costante. La app Mappe in funzione su iPhone accompagna il tragitto con uno scorrimento regolare, uguale a sé stesso.

Conosco il tratto autostradale a memoria, l’avrò percorso più di cento volte. La noia è totale.

Guardo lo schermo per un attimo. Sopra la rappresentazione dell’autostrada si vede il cartiglio verde con la sigla dell’autostrada stessa (tipo A99) in bianco. Noto per la prima volta che il cartiglio scende assieme alla mappa verso la parte bassa dello schermo, come se facesse parte della rappresentazione grafica. Per la prima volta mi chiedo che cosa succeda quando il cartiglio arriva in fondo allo schermo e scompare.

Il programmatore mancato in me suggerisce che ricomparirà di nuovo in alto, contestualmente alla sparizione in basso. Infatti va così.

A volte.

Altre volte si materializza dal nulla dentro lo schermo, in genere verso i quattro quinti dell’altezza, sporadicamente più in basso, una volta praticamente al centro dell’immagine.

Ma è successo anche che la mappa sia rimasta senza cartiglio per molti secondi prima che ne riapparisse uno.

Riassumo. Il cartiglio non fa parte della mappa, perché a volte appare dal nulla dentro lo schermo. Quindi viene controllato dal software.

Il software non segue un ciclo legato all’uscita del cartiglio dal basso dello schermo, né osserva tempistiche prefissate (a un certo punto ho ipotizzato che disegnasse un cartiglio ogni tot secondi in modo che, data la velocità dell’auto, più o meno ce ne sarebbe stato uno sullo schermo in ogni istante. Ma andavo a velocità costante e il cartiglio non ricompariva in tempi costanti). Neanche la successione dei diversi comportamenti è ciclica.

Come cavolo viene regolato il disegno del cartiglio della sigla dell’autostrada sopra la rappresentazione della carreggiata?

Gloria imperitura per chi avanzerà ipotesi interessanti. Ci sto perdendo il sonno.