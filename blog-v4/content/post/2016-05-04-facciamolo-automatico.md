---
title: "Facciamolo automatico"
date: 2016-05-04
comments: true
tags: [9to5Mac, Automator, OS, X, automazione]
---
Senza automazione dei compiti base, l’ho già detto, non c’è vantaggio competitivo. Senza automazione di quelli evoluti, non c’è profitto vero.<!--more-->

A titolo del tutto esemplificativo segnalo il tutorial di *9to5Mac* dedicato alla creazione di un [servizio OS X per Automator che ridimensiona da solo una immagine](http://9to5mac.com/2016/04/27/how-to-quickly-resize-images-mac-using-automator-service/). O mille imagini, perché la vera occasione d’oro è quella.

È anche indicativo che tutti si sentano in dovere di dire la loro sul confronto tra un Mac e un PC sulla base del prezzo ma, come parli dell’automazione, il dialogo si spegne immediatamente. Un sistema Windows, tirato fuori dalla scatola, non ha niente di lontanamente paragonabile ad Automator, che funzioni con la stessa efficacia e con campo d’azione altrettanto vasto. E in un dialogo tra persone consapevoli, questa dovrebbe essere una delle questioni fondamentali.