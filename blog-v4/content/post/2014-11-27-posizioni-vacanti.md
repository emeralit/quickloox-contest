---
title: "Posizioni vacanti"
date: 2014-11-28
comments: true
tags: [Freeciv, Crpgaddict]
---
Vorrei avere abbastanza tempo e doti tecniche da portare [Freeciv](http://freeciv.wikia.com/wiki/Main_Page) per Mac a livello degli altri sistemi operativi, ossia con un semplice installatore binario da doppio clic. Attualmente c’è da seguire una [procedura non garantita e fastidiosa](http://freeciv.wikia.com/wiki/Install-MacOSX).<!--more-->

(Si può comunque giocare la [versione online](http://play.freeciv.org), che funziona anche su un iPhone).

Vorrei scambiare l’esistenza per un semestre con il tipo che uno dopo l’altro, in ordine cronologico, sta praticando [tutti i giochi di ruolo](http://crpgaddict.blogspot.it) usciti per personal computer dai primordi ai giorni nostri. Quelli più antichi li ha anche *finiti* tutti. Lettura affascinante amando il genere e voglia di emularlo.