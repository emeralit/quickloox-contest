---
title: "Primi e secondi"
date: 2016-04-02
comments: true
tags: [Apple, Dediu]
---
L’[analisi di Horace Dediu su Asymco dei prossimi quarant’anni di Apple](http://www.asymco.com/2016/03/28/the-next-40/) parte inevitabilmente da un riepilogo dei quaranta appena compiuti.<!--more-->

Abbondano i dati, che difficilmente si possono leggere da qualche altra parte: in quarant’anni, l’azienda ha venduto un miliardo e mezzo di computer (se contiamo come computer anche iPhone e iPad).

Ancora più interessante: nessuna altra azienda che venda computer è riuscita a farlo per quarant’anni. E nessuna azienda ha venduto una cifra altrettanto grande. Eppure, eccezion fatta per due annualità, Apple non è mai stata al primo posto nella classifica.

Questo – e molto altro – fa pensare che in realtà Apple non sia una venditrice di hardware, come vuole vulgata, bensì una *creatrice di clienti*. Tre quinti dei quali sono clienti nuovi, a dispetto delle panzane sui tifosi e sui *fanboy*, con tutte le incognite del caso.

Dediu chiude chiedendosi se e quanto spazio ci sia per una crescita ulteriore del *business* e offre diverso risposte. Di mio aggiungo una umile constatazione: seguire Apple è interessante proprio perché è azienda diversa dalle altre nel suo settore, con una vita e una parabola diverse. E questo articolo lo dimostra, in modo forte, una volta di più.
