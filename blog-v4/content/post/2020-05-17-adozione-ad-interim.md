---
title: "Adozione ad interim"
date: 2020-05-17
comments: true
tags: [Dropbox, iCloud, Drive, Drafts, Editorial]
---
Questo post – e altri che verranno – è scritto con Drafts su iPad.

Per ora l'unica ragione vera è che [Drafts](https://getdrafts.com) riesce a salvare (non a sincronizzare, ma è già qualcosa) su iCloud Drive mentre [Editorial](http://omz-software.com/editorial/) si limita a Dropbox, cosa che crea problemi quando, come [mi è accaduto](https://macintelligence.org/posts/2020-04-29-sincronizzazione-e-dolori/), Dropbox ha iniziato a presentare numerosi, troppi errori di sincronizzazione.

Si tratta per ora di una adozione ad interim. L'interfaccia di Drafts è stata progettata da un negromante allucinato, ma voglio avere tempo di esplorare meglio le possibilità di automazione della app prima di cambiare cavallo. Un altro problema potenziale è che mi servono anche funzioni da editor generico che vada oltre Markdown; per ora non ci penso, Editorial continua a essere presente su iPad e se non altro scaricare il blog su Drafts risolve almeno la metà dei casi di utilizzo giornalieri.

Prossimi candidati: [IA Writer](https://ia.net/writer) e [1Writer](http://1writerapp.com).