---
title: "Succo di angoscia"
date: 2015-05-06
comments: true
tags: [Zork, AvventuraNelCastello, Angband, TheEnsign]
---
Sono cresciuto con le avventure di testo, a partire da [Zork](https://itunes.apple.com/it/app/lost-treasures-of-infocom/id577626745?l=en&mt=8) e [Avventura nel Castello](http://www.erix.it/retro/storia_cast.html).<!--more-->

Poi ho raggiunto la maturità (anagrafica) con i *roguelike*. Qualunque cosa si possa dire di me, sono arrivato in fondo a [Angband](http://rephial.org) e non me lo toglie nessuno.

Non capita spesso che esca una cosa come [The Ensign](https://itunes.apple.com/it/app/the-ensign/id908073488?l=en&mt=8), che riunisce in un certo senso i due generi e ti piazza in una avventura testuale cruda e spietata con la grafica di un *roguelike*. Si muore di brutto e in modo cattivo, la difficoltà è frustrante, per orientarsi una mappa in caratteri Ascii e sopra a tutto una trama avvincente e complicata che si dispiega durante il gioco solo per chi sappia prestare attenzione nonostante l’ostilità del meccanismo.

Ci sono caduto dentro e ignoro se e quando ne uscirò. Una manciata di queste schermate ridotte all’osso e cariche di angoscia vale mille computergrafiche e diecimila effettacci sonori.
