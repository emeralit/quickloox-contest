---
title: "Che brutta cornice"
date: 2020-04-30
comments: true
tags: [Altroconsumo, iPad]
---
Altroconsumo, quelli che nel 2010 dopo avere visto la presentazione del primo iPad lo definirono con grande preveggenza [la miglior cornice digitale di tutti i tempi](https://macintelligence.org/posts/2010-02-08-quelli-degli-antiforfora/), oggi va a caccia di nuovi soci e li alletta con [il regalo di un Tablet Android con tastiera](https://regalotablet.altroconsumo.it/) con schermo da otto pollici.

Cornice per cornice, era meglio un iPad 2010. Peccato si siano dimenticati della loro intuizione originale.