---
title: "Una stella e quattro zeri"
date: 2020-09-15
comments: true
tags: [Agenzia, Entrate]
---
Devo chiedere il duplicato della tessera sanitaria per una figlia. Dopo mezz’ora di trekking tra l’Abisso della Regione, il Picco dell’Agenzia delle Entrate, il canyon Fisconline e la palude di Asl, non ho trovato un modo chiaro di farlo via web. L’account Spid mi ha mandato un vocale per chiedermi se potevo fare una pausa, per numero di accessi e autenticazioni gli pareva di lavorare per un *influencer*.

È vero che una parte del sito del ministero delle Finanze non funziona al momento e magari era pure decisiva però, ecco, una parte del sito del ministero delle Finanze non funziona.

Valutazione servizio: una stella.

Importo pagato di tasse quest’anno (come molte altre persone ovviamente): a quattro zeri.