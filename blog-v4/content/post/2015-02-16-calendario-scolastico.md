---
title: "Calendari scolastici"
date: 2015-02-16
comments: true
tags: [Scratch, MIT, Erix, Colombini, DeAgostini, Coding, LaBuonaScuola, Arkansas, emergenza, VirtualArkansas]
---
Qua e là si aprono spiragli di luce sulla scuola. Di recente [Enrico](http://www.erix.it) mi ha segnalato [Coding](http://deagostiniscuola.deascuola.it/primaria?layout=book&ref=41&book=11847), libro di testo per insegnare finalmente le basi della programmazione nella scuola di base, con il linguaggio [Scratch](https://scratch.mit.edu/) creato dal [Massachusetts Institute of Technology](http://web.mit.edu/) (pronto [per iPad](https://appsto.re/it/Esay1.i), a disposizione di chi sostiene che sulla tavoletta non si possa programmare).<!--more-->

Se Enrico – che è coautore – mette la firma su un libro di testo per le scuole vuol dire che le scuole senza quel libro rimangono indietro un giro, ma il punto è un altro: siamo tutti indietro di un numero di giri veramente pericoloso e recuperarne uno è solamente l’inizio di una rincorsa di lunghezza già drammatica. *Coding* contiene due percorsi di apprendimento, uno per bambini e uno per insegnanti, e da una parte si è contenti che in De Agostini ci abbiano pensato, dall’altra la nozione che agli insegnanti debbano pensare loro fa paura.

Ma chi sono io per spiegare a quelli de [La Buona Scuola](https://labuonascuola.gov.it/), pasciuti e compresi nei loro incarichi strapagati e alieni, che l’urgenza non è assumere precari con il cartello *precario* bensì chiunque con il cartello *bravo*? Che non bisogna sperperare denaro in tecnologia già morta per lezioni frontali ma creare ambienti per apprendere in collaborazione? Che per ogni burocrate parassita congedato si liberano risorse per dare la banda larga a una scuola in più?

Spero che possano prendere in considerazione almeno l’esempio dello Stato americano dell’Arkansas, la cui Camera ha approvato all’unanimità un [disegno di legge](http://www.arkleg.state.ar.us/assembly/2015/2015R/Bills/HB1183.pdf) che vincola tutte le scuole superiori a offrire un corso di *computer science* a partire da questo autunno, se non in aula almeno *online* tramite [Virtual Arkansas](http://virtualarkansas.org). In fondo al testo (quattro pagine per una legge sono sufficienti, in Arkansas) compare una *condizione di emergenza*:

>È stato determinato […] che informatica e abilità tecnologiche sono di vitale importanza per rispondere alle crescenti esigenze della forza lavoro; che gli studenti della scuola pubblica hanno bisogno di opportunità di sviluppare queste abilità per poter essere concorrenziali nel futuro; e che questo disegno di legge è immediatamente necessario […] per definire gli standard accademici di insegnamento per l’anno scolastico 2015-2016. Esiste pertanto un’emergenza ed essendo questo disegno di legge immediatamente necessario per preservare pace, salute e sicurezza pubblica, diventerà legge [appena approvato].

Nel Pdf de *La Buona Scuola*, la parola *emergenza* ricorre tre volte in 136 pagine. A proposito di assunzioni, assunzioni, assunzioni.