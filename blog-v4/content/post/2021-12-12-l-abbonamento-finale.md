---
title: "L’abbonamento finale"
date: 2021-12-12T02:00:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple] 
---
Ho sognato (giuro) che Apple offriva abbonamenti mensili a settantanove, novantanove, centoventinove, centosettantanove e duecentodiciannove, comprensivi di cloud, musica, film, fitness _e computer_.

Ora mi piacerebbe sapere se ho previsto il futuro come sarà dal duemilatrenta oppure se dovevo evitare il peperone.

Oppure se per il fatto che devo cambiare Mac mini e prendere un M1 pur sapendo che uscirà un nuovo modello l’anno prossimo. Quindi pensare all’idea di un leasing dell’M1 fino a quando esce il nuovo modello.

Penso pure di non avere bisogno di prestazioni di punta. L’attuale Mac mini è stupendo, solo che contiene un i3 e fatica percettibilmente appena si cerca di fare più di una cosa per volta. Inoltre gli ultimi due anni hanno visto una esplosione delle videoconferenze, settore in cui un mini Intel non dà esattamente il meglio.

Pagheresti un solo abbonamento per avere anche il computer come servizio?

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per Qui
ckLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._