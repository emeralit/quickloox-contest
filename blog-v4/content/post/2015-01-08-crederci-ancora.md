---
title: "Crederci ancora"
date: 2015-01-08
comments: true
tags: [Gartner, Windows, Microsoft, iOS, Android, BlackBerry]
---
Nel 2011 Gartner diceva che, arrivato il 2015, [Android avrebbe detenuto il 49 percento del mercato](http://www.gartner.com/newsroom/id/1622614).<!--more-->

Secondo i dati Idc del terzo trimestre 2014, [Android detiene l’84 percento](http://www.idc.com/prodserv/smartphone-os-market-share.jsp) del mercato. Sbagliato del 73 percento.

iOS avrebbe avuto il 17 percento del mercato. Sembra invece che sia appena sotto il 12 percento. Un errore del 32 percento.

BlackBerry sarebbe stata all’11 percento. I dati la pongono allo 0,5 percento. Errore del 95 percento.

Microsoft (Windows Phone) doveva trovarsi al 19,5 percento. Invece sta al 2,9 percento. Errore dell’85 percento.

Gartner deve essere una azienda motivata, con gente pagata per crederci. Ma quelli che pagano Gartner, che cosa credono?