---
title: "Il Safari di riserva"
date: 2016-04-08
comments: true
tags: [Safari, WebKit]
---
Ho sempre trovato comodo tenere nelle applicazioni anche quello che si chiamava Webkit, la versione di Safari derivata dal lavoro degli sviluppatori sull’omonimo [motore di *browser* *open source*](https://webkit.org).<!--more-->

Comodo perché di fatto è un Safari di riserva, dove si può controllare che cosa si potrà fare nella prossima versione, lanciabile parallelamente a Safari in situazioni particolari, certo cosapevoli che si tratta di una versione in lavorazione e non sempre al massimo della stabilità.

La recente decisione di Apple di valorizzarlo con il nome di [Safari Technology Preview](https://developer.apple.com/safari/technology-preview/) fa solo piacere perché è più facile da trovare e gli aggiornamenti, erogati via App Store, sono molto più comodi dello scaricare manualmente una nuova versione o armeggiare con i *nightly build* creati automaticamente dal sito: il codice più avanzato disponibile al momento e anche quello che come capita capita, un giorno va bene e un altro chissà.

Raccomando con decisione Safari Technology Preview. Porta via poco spazio ed è utile.