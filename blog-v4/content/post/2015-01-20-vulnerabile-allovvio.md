---
title: "Vulnerabile all’ovvio"
date: 2015-01-20
comments: true
tags: [iPhone, jailbreak, Siri]
---
Più curioso che altro, il [minitrattato](http://arxiv.org/ftp/arxiv/papers/1411/1411.3000.pdf) che mostra come violare la privacy di un iPhone usando mezzi speciali per alterare il sonoro che va verso Siri.<!--more-->

Praticamente impossibile che accada nella vita reale. Un particolare va notato, però: l’attacco è possibile unicamente se l’apparecchio è stato sottoposto a *jailbreak*.

Nessuno attaccherà il telefono via Siri se l’iPhone è sotto *jailbreak*. Certo però che equivale ad avere un cartello *Ingresso libero* sulla porta di casa. E dovrebbe essere più ovvio.