---
title: "La prova del sito"
date: 2023-06-04T03:24:07+01:00
draft: true
toc: false
comments: true
categories: [Software, Internet]
tags: [Wix]
---
Chiama un amico e spiega la situazione: deve tirare su un sito nel più breve tempo possible. Come presentazione di un alloggio-vacanza, vuole che si vedano un po’ di belle foto. Vuole un minimo di grafica di buona qualità. Vuole almeno un form di contatto con gli utenti. Vuole mostrare la classica mappa interattiva di Google Maps per mostrare il posto.

Ha piani per il futuro, ma ora la priorità è che il sito sia in piedi, con le cose dette, il più velocemente possibile.

Che sistema usare?