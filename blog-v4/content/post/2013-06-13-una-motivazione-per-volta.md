---
title: "Una motivazione per volta"
date: 2013-06-13
comments: true
tags: [WWDC, video, app]
---
Questo <a href="http://www.youtube.com/watch?v=PGtP6ZQ6Lt8">filmato</a> non è stato proiettato durante il <a href="http://www.apple.com/apple-events/june-2013/">keynote della Wwdc</a>.

Sono contento che sia stato girato. Spiega molte cose.

<iframe width="480" height="270" src="http://www.youtube.com/embed/PGtP6ZQ6Lt8" frameborder="0" allowfullscreen></iframe>