---
title: "Una giornata con Chrome"
date: 2013-05-26
comments: true
tags: [Browser]
---
Normalmente uso Safari e nessun altro *browser* se non in situazioni eccezionali. Solo che il *browser* di Apple non si comporta al massimo con i commenti delle <a href="https://drive.google.com">Google Apps</a> e siccome risultavano essenziali, ho lavorato per una giornata con il *browser* di Google.

Direi che questo o quello, per me pari sono. Con qualche piccola sfumatura.<!--more-->

<a href="http://www.apple.com/it/safari/">Safari</a> ha il bello di poter scegliere un Preferito presente nella barra con i numeri della tastiera. Comando-1 porta al primo Preferito, Comando-2 al secondo e cos&#236; via. Per me &#232; quasi una seconda natura premere Comando-1 per visitare Google Immagini o Comando-3 per mandare una pagina su <a href="http://www.instapaper.com">Instapaper</a>. <a href="https://www.google.com/intl/it/chrome/browser/">Chrome</a> invece usa le stesse combinazioni per navigare tra i *tab* (le linguette) di ciascuna pagina e lo trovo sprecato, perch&#233; la stessa cosa si ottiene rapidamente con i tasti freccia e quando il sistema smette di essere conveniente, ovvero quando i *tab* aperti superano la decina, anche il sistema dei numeri perde di efficacia.

Uso poco Elenco lettura di Safari e non mi &#232; mancato su Chrome. Per&#242; ho dovuto montare una estensione per Instapaper, altrimenti mi mancava l&#8217;ossigeno. Delle tante ho optato per <a href="https://chrome.google.com/webstore/detail/send-to-instapaper/liamajdghafnpofaconeimppimbdbhgi?hl=en-US">Send to Instapaper</a>, molto essenziale.

L&#8217;esperienza di lavoro in Chrome, dopo queste cose, &#232; sostanzialmente uguale. Solo una cosa da pignolo dell&#8217;interfaccia: il pulsante di chiusura del *tab* messo all&#8217;estremit&#224; destra del *tab* stesso, proprio non si tiene. Peccato veniale, tanto io far&#242; sempre e comunque Comando-W.

Anzi, un&#8217;altra, peggiore: la barra dei Preferiti pu&#242; essere visibile oppure no. Safari la mostra oppure no, con un comando nel menu Vista. Chrome ne ha uno uguale. Solo che Safari mostra la barra oppure la nasconde. Invece il comando di Chrome parla di mostrare sempre la barra dei preferiti e si pu&#242; decidere se spuntarlo oppure no. L&#8217;effetto finale &#232; lo stesso, ma gi&#224; cercare di spiegare come si comporta Chrome complica la digestione.