---
title: "Buttato vietare"
date: 2021-09-07T01:09:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Viktoria Leontieva, Microsoft, Apple] 
---
Sono cambiate tante cose. L’azienda usa i Pc; si lavora in ufficio otto ore al giorno; devi usare il computer che ha deciso l’azienda. Tutti dogmi che si sono rivelati miti, grazie a aperture liberali come il Bring Your Own Device oppure a mazzate pandemiche che hanno smontato la presenza in ufficio obbligatoria e hanno sbriciolato i pezzi.

Dimostrazione pratica, lo [spazio di lavoro domestico di Viktoria Leontieva](https://www.workspaces.xyz/p/084-victoria-leontieva), _design systems topologist_ per Microsoft.

Leontieva lavora interamente da casa, su piattaforma interamente Apple; e la sua scrivania fa impressione per quanto è piccola e nel contempo funzionale.

Quando in azienda si usano frasi fatte a proposito dell’ambiente di lavoro, dell’opportunità di stare in ufficio, dell’uniformizzazione di hardware e software, ecco, mostri queste foto e tutto va in briciole.

Sto anche sperimentando di persona in queste ore quanto sia difficile fare passare questi concetti e fare aprire gli occhi a persone autoincaricatesi della difesa di un proprio orticello recintato dalla resistenza al cambiamento e seminato a pigrizia, paura, miopia.

Si può sempre vietare qualcosa nel tentativo di fermare il cambiamento dei tempi e delle convenienze. È tempo buttato, sono soldi sprecati.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*