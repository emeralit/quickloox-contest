---
title: "Avvistamento renne"
date: 2015-12-17
comments: true
tags: [Santa, Tracker, Google, Drafts, Graphic]
---
Fonti natalizie degne di attenzione mi lasciano intendere che Babbo Natale stia preparandomi, lato tecnologico, [Drafts 4](https://itunes.apple.com/it/app/drafts-4-quickly-capture-notes/id905337691?l=en&mt=8) per iOS e Graphic per [iOS](https://itunes.apple.com/it/app/graphic-illustration-design/id363317633?l=en&mt=8) e [Mac](https://itunes.apple.com/it/app/graphic/id404705039?l=en&mt=12).<!--more-->

Se le voci troveranno conferma, renderò conto.

Nel frattempo posso solo accendere il [Google Santa Tracker](https://santatracker.google.com/).