---
title: "Il concetto di chiusura"
date: 2016-03-03
comments: true
tags: [Ibm, Apple, Swift, cloud, Bluemix]
---
Ieri mi è stato che Apple è *un’azienda chiusa*. Non i prodotti, l’azienda. Con un miliardo di apparecchi attivi, circa altrettanti account iTunes (con vere carte di credito, di gente vera, niente robot, niente fantocci). Come faccia a essere chiusa un’azienda che serve clienti a centinaia di milioni, non so.<!--more-->

Intanto leggo notizie come [IBM che adatta Swift al cloud per scrivere applicazioni su misura per un proprio ambiente di elaborazione](http://www.ibm.com/cloud-computing/bluemix/swift/).

Una multinazionale tra le più grandi esistenti, per decenni concorrente acerrima di Apple ovunque, sta usando un linguaggio di programmazione sviluppato da Apple, che Apple ha reso *open source* aprendo il codice a chiunque sia convinto di poterci fare qualche cosa di buono.

Sul concetto di chiusura ho bisogno di ripetizioni.