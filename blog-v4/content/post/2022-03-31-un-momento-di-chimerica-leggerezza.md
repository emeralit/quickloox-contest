---
title: "Un momento di chimerica leggerezza"
date: 2022-03-31T01:16:43+01:00
draft: false
toc: false
comments: true
categories: [Software, Hardware]
tags: [Dogcow, Clarus, iOS, iPadOS, emoji, Moof, Messaggi, Mac OS]
---
Avvicinandocisi al momento buono per le uova di Pasqua, ci si allontana per un momento da guerre pandemie e gossip sulla notte degli Oscar per trovare l’oblio sulla tastiera di iOS e di iPadOS.

Se in Messaggi scriviamo sulla tastiera *Clarus* oppure *Moof*, negli emoji suggeriti compaiono un cane e una mucca, [segnala 512 Pixels](https://512pixels.net/2022/03/ios-keyboard-suggests-dog-and-cow-emoji-when-clarus-is-typed/).

Ho verificato, è proprio così e non mi è mai passato per la testa che si potesse trovare un *easter egg* riferito al periodo glorioso in cui Clarus il *dogcow*, il cane-mucca, lavorava come [anteprima del comando di stampa](https://apple.fandom.com/wiki/Dogcow) del Mac OS di allora, mostrando con la sua posizione l’effetto che avrebbero avuto le modifiche ai parametri del comando (foglio orizzontale o verticale, immagine capovolta, negativa eccetera). *Moof* era il suo verso, ovviamente una sintesi di *moo* e *woof*.

Scrivendo *dogcow*, la chiara specie di appartenenza di Clarus, non accade niente di speciale.

Ultima curiosità: l’ordine degli emoji è inverso. Compaiono prima la mucca e poi il cane, mentre dovrebbe essere l’opposto. Come invece succede correttamente se si scrive nel campo di ricerca della tastiera emoji.

Nostalgie zero, ma ricordi affettuosi a carrettate; Clarus rimane per sempre nei nostri cuori anche se è scomparso dalle finestre modali della stampa.