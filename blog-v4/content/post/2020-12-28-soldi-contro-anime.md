---
title: "Soldi contro anime"
date: 2020-12-28
comments: true
tags: [Among, Us, Cluedo, Apple, videogiochi]
---
Dimostrazione plastica del perché Apple non produrrà macchine dedicate al gaming mentre insisterà nello spingere prodotti che servono perfettamente una fascia molto vasta di persone su numerose attività, tra cui incidentalmente anche i giochi.

Novembre 2020, ci dice *SuperData*, [è stato il più lucroso di sempre per i giochi digitali](https://www.superdataresearch.com/blog/worldwide-digital-games-market). Le vendite hanno totalizzato la quota record di undici miliardi e mezzo di dollari tra computer, console e *mobile*.

Apple ha fatto un 2020 [ben sopra i duecentosettanta miliardi di dollari](https://www.macrotrends.net/stocks/charts/AAPL/apple/revenue), ma diciamo che siano duecentoquaranta, per arrotondare a venti miliardi al mese tutti i mesi. In altre parole, in termini di fatturato, Apple da sola è grande *il doppio* dell’intero mercato dei giochi digitali.

Uno dei titoli che hanno mosso più interesse tra i videogiochi più sofisticati è [Call of Duty: Black Ops Cold War](https://www.callofduty.com/blackopscoldwar), che in novembre ha venduto 5,7 milioni di copie.

Di converso, sempre novembre ha visto *mezzo miliardo* di persone giocare a [Among Us](http://www.innersloth.com/gameAmongUs.php). Per ogni giocatore di *Call of Duty*, ce ne sono cento di *Among Us*.

Dei cento, a giocare su computer sono *tre*. Gli altri novantasette sono su altro, in prevalenza *mobile*. Centinaia di milioni di persone, una fetta consistente dei quali hanno giocato da un iPhone.

Mettiamoci sulla poltrona di Tim Cook e chiediamoci se vogliamo dedicare tempo, sviluppo e ingegneria a qualche milione di appassionati di *Call of Duty* che, in quanto tali, giocano e fanno poco altro, o a qualche centinaio di milioni di persone che oltre a lanciare Among Us per distrarsi un’ora fanno varie altre cose.

Il numero di persone disposte a pagare un extra per giocare a *Call of Duty* su Mac è vastamente inferiore a quello di quanti han voglia di giocare gratis a *Among Us* su iPhone, per il quale avere i giochi più diffusi è una molla di diffusione molto importante.

Avere i giochi più costosi, beh, può solo interessare poco. Apple si muove in una logica di fatturato generato da ogni singolo apparecchio. Più apparecchi, più fatturato.

Apple guadagna soldi a palate perché non va dove sono i soldi, ma dove sono le persone.