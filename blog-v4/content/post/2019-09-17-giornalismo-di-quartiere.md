---
title: "Giornalismo di quartiere"
date: 2019-09-17
comments: true
tags: [NYT, Times, New, York]
---
Ogni tanto ti ricordi perché esiste il giornalismo, che in Italia è merce sempre più rara.

Il *New York Times* ha pubblicato un reportage sulle [feste di quartiere che sono tradizione estiva di New York](https://www.nytimes.com/interactive/2019/09/15/nyregion/block-parties-nyc.html).

L’articolo è anodino, niente di che. Però contestualmente hanno sguinzagliato venti fotografi che hanno prodotto una raccolta di immagini da toccare il cuore e l’immaginazione.

Prego, osservare la navigazione dell’articolo. Banale a vederla dopo, eppure la superiorità sulla carta è questa; basta volerla sfruttare e saperla pensare. Bonus: funziona perfettamente su iPad Pro, senza una esitazione, senza un problema.

Feste di quartiere. Vivo nell’hinterland di Milano, saturo di paesi grandi e piccoli dove capita sempre una festa del santo patrono, una sagra, un happening di partito (questi ultimi fortunatamente sempre più rari, il piacere della salamella non dovrebbe essere legato a una ricerca di consensi). Tuttavia, nel leggere i titoli di coda del *Times*, viene spiegata la *metodologia* di scelta delle feste e raccolta delle foto. Non sono andati a casaccio o dove il taxi faceva prima; hanno utilizzato un criterio preciso e coerente.

Vien voglia di diventare newyorchesi, di abbonarsi alla *Gray Lady*, come chiamano il quotidiano. Lì ci lavorano ancora dei giornalisti che sanno fare il loro mestiere e sanno farlo anche sul web.