---
title: "I soliti non professionisti"
date: 2017-08-27
comments: true
tags: [Oceania, Matteo]
---
Devo il titolo a **Matteo**. Anche il testo:

>Ti invio prova della non “professionalità” dei portatili Apple… stanno solo registrando la musica di [Oceania](https://it.wikipedia.org/wiki/Oceania_(film))…

E pure la foto.

 ![Un momento della registrazione della colonna sonora di Oceania, con un Mac in bella vista](/images/oceania.png  "Un momento della registrazione della colonna sonora di Oceania, con un Mac in bella vista") 