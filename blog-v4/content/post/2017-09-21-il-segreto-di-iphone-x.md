---
title: "Il segreto di iPhone X"
date: 2017-09-21
comments: true
tags: [iPhone]
---
Chi è preoccupato del supposto proliferare di modelli di iPhone, in contrasto con la celebre matrice 2x2 che Steve Jobs disegnò al momento di rilanciare Apple, dovrebbe dare un’occhiata a questa [chiarissima rappresentazione](https://www.paintcodeapp.com/news/ultimate-guide-to-iphone-resolutions) di come stanno le cose da un punto di vista opposto: quello dello sviluppatore che deve supportare le risoluzioni di più schermi diversi.<!--more-->

Viene fuori che i modelli veri e propri, in effetti, sono non più di sei. Due dei quali sono di fatto fuori mercato. Ho un iPhone 4S e trovare una app moderna che ci giri sopra, a parte quelle ovvie delle Google e delle Facebook, è una impresa.

Bonus: si capisce molto bene quale sia la vera ragione, eventualmente, per scegliere un iPhone X. Ha uno schermo incredibilmente ricco di pixel. Fisicamente, però è ultracompatto, poco più grande di un iPhone SE.

Uso un iPhone 5 e, se dovessi cambiare domani, molto probabilmente prenderei un iPhone SE, perché trovo ideale questa dimensione dell’apparecchio. Sarei disposto però a dare una possibilità a iPhone X, che offre molto di più in cambio di un ingombro solo marginalmente superiore.