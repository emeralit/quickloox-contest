---
title: "Giornalisti e Radio-Shack-amatori"
date: 2015-05-16
comments: true
tags: [RadioShack, Apple, ODG, spam]
---
La mitica, gloriosa catena americana di negozi di elettronica di consumo (e non) è arrivata a una fine ingloriosa e quello che ha ancora del valore viene venduto per tacitare i creditori più fortunati.<!--more-->

Succcede una cosa che non si era mai vista e che, sarà un caso, per la prima volta fa Apple: entra a gamba tesa e [afferma il diritto alla confidenzialità dei clienti congiunti](http://www.appleworld.today/blog/2015/5/14/apple-steps-into-radio-shack-sale-to-protect-is-customer-data). Tradotto: non vuole che i dati di chi ha comprato materiale Apple presso Radio Shack vengano venduti, come finirà per i dati di tutti gli altri.

Si faccia avanti un’altra azienda con lo stesso approccio, una qualsiasi.

Per inciso: è solamente l’osservazione di una singolare coincidenza. Mi sono iscritto alla piattaforma digitale dell’Ordine dei giornalisti e nel giro di pochi istanti mi è arrivata una mail di *spam* che non avevo mai visto prima, proprio sull’indirizzo che ho usato nella registrazione.

Coincidenza, eh.