---
title: "Pubblicare meraviglie"
date: 2017-09-18
comments: true
tags: [Jobs, Park, Theater, Gruber, iPhone, watch]
---
John Gruber ha scritto su *Daring Fireball* alcune [osservazioni](https://daringfireball.net/2017/09/welcome_to_the_steve_jobs_theater) particolarmente centrate sull’[evento di presentazione di iPhone 8](https://www.apple.com/apple-events/september-2017/).<!--more-->

Trova lo Steve Jobs Theater straordinario, seppure con difetti da sistemare, e giustamente lo colloca tra i temi centrali dell’evento, [come si diceva](https://macintelligence.org/posts/2017-09-15-il-convitato-di-vetro/).

È stata la prima volta che Apple ha parlato di Jobs durante un evento pubblico dal 2011. Lo ha fatto lasciando parlare Jobs: una frase pronunciata in una riunione interna, non in un *keynote*.

>Uno dei modi in cui ritengo che le persone esprimano il loro apprezzamento verso il resto dell’umanità è fare qualcosa di meraviglioso e pubblicarlo.

Scrive ancora Gruber:

>Tra cinque, dieci anni watch serie 3, iPhone 8 e persino iPhone X saranno semplicemente vecchi prodotti dentro un cassetto. Ma il debutto di Apple Park, l’inaugurazione dello Steve Jobs Theater e il primo tributo pubblico dell’azienda al proprio fondatore… sono ciò che ricorderò maggiormente.

Raramente, molto raramente ci si ricorderà di un *keynote* cinque anni o dieci anni dopo. Apple Park è più significativo di quello che si legge. Quanto a me, dopo avere allacciato le scarpe a Gruber ove me lo concedesse, mi trovo nella testa queste parole che rimbalzano incessanti:

>fare qualcosa di meraviglioso e pubblicarlo.

È talmente cogente alla mia attività che temo mi rimarrà impresso per molto tempo. A leggere in rete quello che viene pubblicato quotidianamente, mi pare di vedere problemi con la prima metà della nozione e ambiziosa disinvoltura invece con la seconda.

Non so gli altri. Io mi voglio impegnare su tutti e due i fronti.