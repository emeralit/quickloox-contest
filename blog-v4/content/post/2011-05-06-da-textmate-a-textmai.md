---
title: "Da TextMate a TextMai"
date: 2011-05-06
comments: true
tags: [Scrivere, Script]
---
Sono notoriamente un intenso consumatore di <a href="http://www.bbedit.com">BBEdit</a>. Cionostante mi scoccia non vedere ancora all&#8217;orizzonte TextMate 2.<!--more-->

Questo perch&#233; la concorrenza fa bene a tutti e TextMate, quando &#232; nato nel 2006, prometteva di portare novit&#224; interessanti per svecchiare positivamente il mondo degli *editor* di testo.

Il problema &#232; che a maggio 2011 tutto quello che si sa di TextMate 2 &#232; che l&#8217;autore ci sta lavorando. Parola sua, del&#8230; <a href="http://blog.macromates.com/2009/working-on-it/">14 giugno 2009</a>.

Il tempo che &#232; passato non sarebbe di per s&#233; un problema, se non fosse che la pagina contiene affermazioni come *la fine del lavoro &#232; in vista*, *diciamo che il 90 percento dei moduli sta arrivando a compimento* e cose cos&#236;. Cresce la sensazione che una versione 2 di TextMate potrebbe non arrivare mai, o metterci talmente tanto tempo che sarebbe abbastanza lo stesso.

Infatti gli altri non stanno fermi. Produrre qui una rassegna di tutte le alternative disponibili per l&#8217;*editing* di testo, la programmazione e lo sviluppo web &#232; oramai impresa superiore a qualsiasi forza. Ed &#232; una situazione fantastica, l&#8217;apoteosi della libert&#224; di scelta.

Tra le ultime novit&#224; e le varianti pi&#249; curiose in tema segnalo comunque la *beta* di <a href="http://www.sublimetext.com/2">Sublime Text 2</a>, l&#8217;ancor meno pronto <a href="http://www.vicoapp.com/">Vico</a>, il creativo sistema di abbreviazioni di testo <a href="http://code.google.com/p/zen-coding/">Zen Coding</a> e l&#8217;enigmatico <a href="http://chocolatapp.com/">Chocolat</a> (di cui deve ancora arrivare la *beta*).

Devo anche dire che, dovessi spostarmi da BBEdit, mi dirigerei con decisione verso l&#8217;ambito grafico-visivo, su programmi stile <a href="http://panic.com/coda/">Coda</a>; oppure compierei una scelta fortemente monastica, verso *editor* ancora pi&#249; complessi e puri come emacs (dentro il Terminale), magari in una delle sue incarnazioni pi&#249; *Mac-like* come <a href="http://aquamacs.org/">Aquamacs</a>.