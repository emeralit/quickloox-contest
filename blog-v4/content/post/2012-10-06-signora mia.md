---
title: "Signora mia"
date: 2012-10-06
comments: true
tags: [MobileMe, Jobs, Cube, Ping]
---
*Se ci fosse stato Steve non sarebbe andata così*.

È diventato il nuovo *Apple sta per fallire*. Grande spazio sui media per i *fanatici*, i *cultisti*, gli *evangelisti*, i *predicatori* sedotti dal fascino perverso di Apple, che acquistano per moda essendo apparentemente dotati di risorse infinite, tali da poter rinnovare un corredo completo di prodotti Apple ogni anno.<!--more-->

Quelli che invece sfruttano ogni occasione, comprese quelle false, incluse quelle inventate o destituite di fondamento, loro sono giudici imparziali e pieni di saggezza. E, di fronte alla qualunque, snocciolano il sapere: *Da quando non c’è Steve Jobs Apple non è più la stessa*.

Vedo adesso che chi proviene da MobileMe <a href="http://support.apple.com/kb/HT5527?viewlocale=it_IT&locale=it_IT">godrà per un anno di spazio extra gratuito su iCloud</a>. MobileMe non è stato esattamente un grande successo e non sempre ha funzionato in modo straordinario. Ci fosse stato Steve sarebbe andata così, perché Steve c’era.

Apple sotto Jobs ha perso centinaia di milioni di dollari su <a href="http://www.everymac.com/systems/apple/powermac_g4/specs/powermac_g4_450_cube.html">Power Macintosh Cube</a>, prodotto certo non capito, ma che comunque non ha reso come doveva.

E l’<a href="http://mashable.com/follow/topics/antennagate/">Antennagate</a>? Continuo a credere che sia stata una faccenda molto più mediatica che effettiva, come testimonia il mio iPhone 4, perfettamente all’altezza del compito due anni dopo. Chi crede sia stata una *dêbacle* deve ammettere che l’amministratore delegato di Apple era Jobs.

Il 30 settembre scorso ha <a href="http://www.macrumors.com/2012/10/01/apples-ping-social-network-officially-closes/">chiuso i battenti Ping</a>, *social network* basato su iTunes e indubitabilmente nato nell’era di Steve. Non ha funzionato.

Apple non è (mai) stata infallibile. Steve Jobs non è (mai) stato infallibile. L’idea che lo siano serve solo a chi vuole criticarla: così, visto che niente è perfetto, si troverà certo qualcosa che non va. Signora mia, quelli erano tempi.

Si noti che tra i maggiori adoratori di *andava meglio prima* albergano immancabili quelli che, all’epoca, erano i più accaniti detrattori.

Meglio valutare con attenzione quello che serve, quando serve, e scegliere Apple quando vogliamo e quando possiamo avere il meglio. Che non è l’assoluto. Ma in confronto al resto dell’offerta è sicuramente il meglio. Ho vissuto gli ultimi giorni tra convegni e conferenze, a notare un sacco di esempi di uso di tecnologia a tutti i livelli. Su singole sfaccettature si può discutere; nel complesso, non c’è partita.