---
title: "Il razzismo supremo"
date: 2017-06-25
comments: true
tags: [Beats, News, App, Store, Snell, Six, Colors]
---
Il razzismo in generale è idiota. Alcuni casi particolari, tuttavia, fanno eccezione e sono da coltivare e incoraggiare.

Prendiamo per esempio Beats 1, una stazione radio basata su *deejay* di valore assoluto che [selezionano la musica migliore](https://macintelligence.org/posts/2015-06-27-il-tocco-musicale/) da proporre agli ascoltatori.

Oppure Apple News, un flusso di notizie per governare il quale [sono state assoldate figure di rilievo](https://macintelligence.org/posts/2017-05-28-umanamente-impossibile/) nel panorama editoriale americano.

A questa coppia si aggiunge App Store: Apple ha annunciato l’intenzione di fare pulizia e l’ultima notizia è che ha già [eliminato centinaia di migliaia](https://techcrunch.com/2017/06/21/apple-goes-after-clones-and-spam-on-the-app-store/) di *app* tra cloni di programmi già esistenti, fuffa, trappole per utenti distratti e prodotti di qualità insufficiente. Oltre ad avere diramato [l’ultima chiamata](https://macintelligence.org/posts/2017-06-04-velocita-sessantaquattro/) per le *app* a 32 bit, oramai buone soltanto per [ecosistemi arretrati e irresponsabili](https://macintelligence.org/posts/2017-06-10-nostalgia-dei-sedici/).

Se questa parte odora un po’ di tecnico, ecco Jason Snell da *Six Colors* a ricordare che anche App Store [si avvia ad avere una vetrina ricercata](https://sixcolors.com/post/2017/06/in-ios-11-app-store-editorial-comes-out-of-the-shadows/), di livello editoriale, con curatori professionisti.

Ecco come Apple si rivela splendidamente razzista, del razzismo supremo: sostenere che gli esseri umani sono superiori agli algoritmi, alle macchine che (*soi-disant*) imparano, alle reti neurali, alle cosiddette intelligenze artificiali. La scommessa è che manca qualunque dimostrazione scientifica di questa superiorità; eppure basterà ascoltare Beats 1, leggere Apple News o spulciare App Store per constatare qualità indiscutibilmente migliore di quella offerta dai negozi automatici, dagli store gestiti con uno script, dai depositi di contenuti che hanno come unico scopo *profilarti*.

Il razzismo tra umani è stupido. Il razzismo verso gli algoritmi è doveroso. Per avere buona musica, e tutto il resto, ci vuole un umano.