---
title: "Un Kickstarter per cambiare il libro"
date: 2016-06-15
comments: true
tags: [Kickstarter, Akko, Accomazzi]
---
Ha ancora senso pubblicare libri di informatica? Se sì, come deve essere un libro nel XXI secolo?

L’amico Akko ha deciso di rompere gli schemi e provare a ripensare all’idea tradizionale del libro sul nuovo sistema operativo di Mac. Per trasformarlo in un progetto che ruota attorno a un libro, solo più interessante e promettente.<!--more-->

C’è un ma: si può fare solo [se ci sono i soldi per farlo](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac). Perché un editore tradizionale, oggi, semplicemente, non ha l’abito mentale per concepire e supportare una cosa del genere.

Se ci sono abbastanza soldi, darò anch’io una mano. Tuttavia trovo il particolare ininfluente.

Questo è un progetto che [merita di essere sostenuto](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac) perché mostra una strada, potrebbe contribuire allo svecchiamento di una parte consistente della nostra industria editoriale. E perché nessun altro può fare un libro sul mondo Apple migliore di quello che scriverà Akko, detto con il massimo rispetto per tutti.

L’iniziativa ha un potenziale molto maggiore di quello che sembra e per questo invito ognuno a [contribuire](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac), secondo le proprie possibilità e il proprio apprezzamento per un tentativo di cambiare le cose.

I link vanno tutti allo stesso Kickstarter e basta cliccarne uno. Si può contribuire con un euro, milleduecentocinquanta, quelli che si vogliono. Il mio parere è che i libri vecchi, oggi, abbiano sempre meno senso. Ma fare buona divulgazione, con quello che gira su internet, serve più che mai.