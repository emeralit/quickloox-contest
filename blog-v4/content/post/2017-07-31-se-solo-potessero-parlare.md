---
title: "Se solo potessero parlare"
date: 2017-07-31
comments: true
tags: [Gundotra, Sinofsky, Apfs, iPhone, Android]
---
Vic Gundotra ha appena espresso in un [post su Facebook](https://www.facebook.com/vicgundotra/posts/10154573133695706) [giudizi pesanti](https://9to5mac.com/2017/07/31/iphone-versus-android-photography/) sullo stato della fotografia in campo Android. Soprattutto, ha tessuto le lodi di iPhone con espressioni come questa:

>Per la maggior parte delle persone la fine delle [Digital Single-Lens Reflex](https://www.ridble.com/fotocamere-dslr-come-funzionano/) è già arrivata. Ho lasciato a casa la mia fotocamera professionale e scattato queste foto della cena con il mio iPhone 7 usando la fotografia computazionale (o modalità ritratto come la chiama Apple). Difficile non dire di queste foto (in un ristorante, scattate con un telefono mobile senza flash) che colpiscono. Bel lavoro, Apple.<!--more-->

>L’innovazione maggiore non accade neanche più a livello hardware ma a quella della fotografia computazionale (Google era all’avanguardia cinque anni fa […] ma ha perso terreno.

>Riassunto: se veramente avete a cuore le vostre fotografie, avete un iPhone. Se non è un problema essere indietro di qualche anno, comprate Android.

Vic Gundotra era un altissimo dirigente di Google fino a poco tempo fa. Che [non le mandava a dire](https://www.youtube.com/watch?v=IIUfINq2Qmo) quando si trattava di parlare di Apple.

Ora non lavora più a Google. Possiede un iPhone, parla bene di Apple, male di Android.

La cosa interessante è che all’incirca la stessa cosa è accaduta di recente quando Steven Sinofsky, già responsabile di Windows in Microsoft e ora in altre faccende affacendato, [ha tessuto le lodi di Apple](https://macintelligence.org/posts/2017-06-12-recensioni-divertite/) per la transizione di iOS al nuovo filesystem Apfs.

Si tratta di personaggi con qualifiche e competenze altissime, ai vertici del loro settore.

La conclusione può essere solo una. *Lo sapevano già*. Anche prima di congedarsi o essere congedati.

Ah, se gli attuali dirigenti di Microsoft, Samsung, Google potessero parlare. Invece ci tocca aspettare che cambino lavoro.

(Per inciso, autorevoli ex dirigenti Apple come Scott Forstall hanno avuto finora [ben altro atteggiamento](https://www.theverge.com/2017/6/21/15844286/scott-forstall-interview-apple-iphone-steve-jobs) nei confronti della ex azienda).