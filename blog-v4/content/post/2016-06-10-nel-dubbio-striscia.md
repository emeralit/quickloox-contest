---
title: "Nel dubbio, striscia"
date: 2016-06-10
comments: true
tags: [iPhone]
---
*Come faccio per cancellare più foto per volta su iPhone?*

Apri Foto, tocca Seleziona, tocca le foto da cancellare, tocca il cestino, tocca Cancella.<!--more-->

*Non c’è un modo più veloce?*

Dopo avere selezionato la prima foto, strisci il dito in verticale e tutte le righe di foto che attraversi vengono selezionate.

*Bello! Non possono scriverle da qualche parte queste cose?*

Se io ti dessi un [manuale di iPhone](https://help.apple.com/iphone/9/), lo leggeresti?

*No, non ho tempo.*

Ecco. Allora, perché non ci provi, a passare un dito sullo schermo e vedere che succede?

*Come fa a venirmi in mente di passare un dito sullo schermo?*

Non so, forse il fatto che usi da anni iPhone usando il dito?