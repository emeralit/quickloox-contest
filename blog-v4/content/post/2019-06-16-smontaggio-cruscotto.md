---
title: "Smontaggio cruscotto"
date: 2019-06-16
comments: true
tags: [Dashboard, JavaScript]
---
Non conosco il giapponese e il testo mi risulta incomprensibile, ma è sufficiente guardare le figure del blog *Mac Otakara* per capire che, dopo quattordici anni di onorato servizio, [si prepara la pensione per Dashboard](http://www.macotakara.jp/blog/mac_os_x/entry-37677.html).

All’inizio l’idea dei widget scritti in HTML e poco più, capaci di galleggiare sullo schermo in una sorta di scrivania ausiliaria, sembrava promettente. Ci avevo investito una discreta quantità di tempo ed erano comparsi anche widget interessanti. All’epoca del massimo splendore una pagina del sito Apple li elencava tutti ed erano oltre tremila.

Poi il tempo è passato. In molti sono pronti a stigmatizzare l’epoca presente per mancanza di alternative *for the rest of us* alla programmazione dura e pura. Di questi, nessuno si è messo a spremere il meglio da una tecnologia che richiedeva niente più che linguaggio di marcatura e una spruzzata di JavaScript per le funzioni da programmare. Forse era persino troppo facile, poco adatta a caratteri ambiziosi.

Era anche un’altra epoca, chiaro. Oggi con tutto quello che si trova e si fa direttamente su Web, a chi può interessare fare galleggiare un piccolo widget su una grande scrivania?

Sia reso l’onore delle armi a Dashboard comunque, che fu anch’esso alfiere della superiorità di Mac come esperienza di utilizzo e interfaccia utente.
