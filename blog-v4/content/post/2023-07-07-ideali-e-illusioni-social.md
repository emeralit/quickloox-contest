---
title: "Ideali e illusioni - social"
date: 2023-07-07T00:43:51+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Meta, Mastodon, Threads, Twitter, Musk, Elon Musk]
---
Sia ben chiaro. Preferisco l’open source. Sono socio di [LibreItalia](https://www.libreitalia.org) (iscriviti!). Tuttavia il mondo presenta aspetti di concretezza che ci ci piacerebbe poter ignorare e invece, se si vuole vincere alla fine, vanno tenuti in considerazione.

Non c’è bisogno di rievocare troppo i disastri combinati da Elon Musk dopo che ha comprato Twitter. Né la grancassa suonata da quel momento in poi a favore di [Mastodon](https://joinmastodon.org). Che è decentrato, è libero, autogestito, estraneo alle multinazionali, non in vendita e viva la rivoluzione.

Non lo linko né lo scrivo e lo lascio invece come esercizio. Guarda quando Musk ha acquistato Twitter e quanto tempo è passato per arrivare agli oltre dieci milioni di utenti di Mastodon oggi [secondo Statista](https://www.statista.com/statistics/1376022/global-registered-mastodon-users/#:~:text=Mastodon%3A%20number%20of%20registered%20users%202022%2D2023&text=As%20of%20March%202023%2C%20decentralized,300%20percent%20within%20five%20months.).

Meta ha lanciato nei soli Stati Uniti un clone di Twitter, Threads, che ha raccolto [trenta milioni di iscritti](https://tech.slashdot.org/story/23/07/06/209237/threads-passes-30-million-sign-ups-in-less-than-24-hours), il triplo di Mastodon. In ventiquattro ore, svariate centinaia di volte più velocemente di Mastodon.

Aggiornamento: [sessanta milioni](https://www.quiverquant.com/threadstracker/), il sestuplo di Mastodon, in due giorni. Del confronto tra le due velocità non stiamo neanche a parlare.

Faccio il tifo per Mastodon, per la decentralizzazione e per qualsiasi cosa stia fuori da Meta. Al tempo stesso, se parliamo di piattaforme desiderose di ospitare le chiacchiere di un miliardo di persone, pensare che bastino la buona volontà degli autogestiti e il pregio del software decentralizzato è una illusione o anche peggio. Non si copre un pianeta senza strategia, investimenti, organizzazione, decisioni nette, volontà di affermazione.

Threads ha doppiato due volte Mastodon in una giornata. Facile che a fine estate abbia dieci o venti volte più iscritti. Come sia fatto il software o come siano gestiti i server dovrebbe avere una priorità evidentemente inferiore.