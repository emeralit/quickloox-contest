---
title: "La stretta zuccherosa"
date: 2023-10-13T19:01:56+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Microsoft, Activision, Blizzard, World of Warcraft, Candy Crush, Diablo, Overwatch, Ars Technica, Tencent, Sony]
---
La società che realizza il software di ufficio più usato dai pendolari alla fine ce l’ha fatta e si comprata, tra le altre cose, *Candy Crush*, il software più usato dai pendolari fuori ufficio.

Si è comprata anche *Diablo* e *Overwatch*, la società identificata con e dal software di produttività personale. Suppongo anche *World of Warcraft* e *Hearthstone*, che anni fa mi hanno procurato diverse ore di intrattenimento molto apprezzato.

Nel riportare la notizia della [acquisizione compiuta di Activision/Blizzard da parte di Microsoft](https://arstechnica.com/gaming/2023/10/microsoft-finally-owns-candy-crush-as-it-closes-69b-activision-blizzard-deal/), *Ars Technica* osserva che ora Microsoft possiede trenta studi produttori di videogiochi ed è la terza forza nel *gaming* dopo Tencent e Sony.

Ricorda anche come, lentamente, dopo tutte le promesse fatte per arrivare in fondo a ciascun accordo, anno dopo anno i giochi scivolano sempre più nell’ecosistema Microsoft per lasciare fuori i possessori di altre console.

Sappiamo che sogno hanno a Redmond. *Candy Crush* deve diventare giocabile solo dove, quando e come fa comodo a loro, divertente e zuccheroso per quanto inaccessibile da piattaforme hardware e software non gradite.

Grazie al cielo ci sono quattro miliardi di computer da tasca a complicare i sogni di dominio totale. Nel mio piccolo, un allungamento della lista di prodotti e giochi da non avvicinare.