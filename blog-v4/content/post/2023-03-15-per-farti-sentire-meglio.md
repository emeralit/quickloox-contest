---
title: "Per farti sentire meglio"
date: 2023-03-15T00:08:11+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Samson, Q2U, OBS, Scandolin, Matteo Scandolin, XLR, Polly Samson, Gilmour, David Gilmour]
---
Andy Warhol aveva predetto per tutti quindici minuti di celebrità ma, come ogni buon profeta, ha sorvolato sui dettagli, come il fatto che avrebbero potuto essere quindici minuti ripetuti e che piano piano avremmo avuto bisogno di uno studio di registrazione fatto in casa per fruire del nostro nuovo diritto mediatico.

C’era forse arrivato il sottoscritto, che peraltro non sarebbe degno di fare la polvere ai bagni della Factory warholiana, quando diceva che nel futuro di Internet ciascuno avrebbe avuto il proprio canale televisivo, che nessuno si sarebbe dato la pena di guardare.

La seconda parte era una mezza battuta, anche se è vero che oggi, per ogni influencer da centomila seguaci, contiamo milioni di morti di like che agognano a un riconoscimento qualsiasi, anche un singolo cuoricino messo da un parente impietosito.

La parte televisiva comunque era vera. Dopo avere adottato [OBS](https://obsproject.com) come studio di regia (usato all’uno percento della capacità, ma dipende da me, non da lui) e iPhone come videocamera, è arrivato sulla scrivania un microfono [Q2U Samson](http://www.samsontech.com/samson/products/microphones/usb-microphones/q2u/). Diversi segni nelle ultime settimane mi avevano reso chiaro che gli auricolari con microfono di iPhone vanno benissimo, tuttavia più il messaggio è chiaro e pulito e più ha chance di arrivare ai destinatari. E così abbiamo coperto il sottosistema voce.

La scelta è stata ispirata dai consigli del podcasting guru [Matteo](https://scandol.in), che posso solo ringraziare molto dal basso della mia ignoranza tecnica e pratica.

Q2U costa qualcosa meno di cento euro su Amazon e, se ho capito, si colloca in una scala di ottima qualità per podcasting e registrazioni di parlato, senza essere un mostro da studio di registrazione professionale (che può arrivare a costare anche due o tremila euro).

Nella confezione si trovano, oltre al microfono, un cappuccio di gommapiuma da applicare per minimizzare schiocchi audio, un’asta a treppiede da scrivania, semplice ma più che dignitosa, più due cavi, uno XLR per collegarsi a mixer o a impianti audio professionali e uno USB per attaccare il microfono diritto al computer, più un depliantino di istruzioni sommarie che ho fatto bene a leggere perché ho scoperto di poter attaccare un paio di cuffie o auricolari al microfono stesso e così poter sentire come arriva la mia voce a chi ascolta.

Il microfono in sé è in formato *gelato* come dicono in televisione, bello, solido, robusto anche se leggero, dà un’ottima impressione. L’unica nota particolare che posso aggiungere è la presenza di un pulsante fisico per accenderlo e spegnerlo.

Fare entrare l’apparecchio nel setup da scrivania è stato questione di un respiro. Cavo USB in una porta di Mac mini ed ecco che OBS lo ha riconosciuto immediatamente. Nell’interfaccia gli ho dato nome *Polly* in omaggio alla [moglie di David Gilmour](https://www.pollysamson.com).

Ho già affrontato alcune videoconferenze e il riscontro è stato più che positivo. All’inizio c’è qualche ansia per la mancanza di abitudine ad avere il microfono in un posto diverso dal penzolare dagli auricolari. Poi non ci si fa più caso. Secondo il produttore, il cardioide è costruito per catturare fedelmente il suono davanti a sé e trascurare gli altri intorno. Non ho ancora provato la funzione monitor del microfono e non ho udito la mia voce in cuffia; proverò. Per ora mi è sufficiente il feedback di OBS, per il quale il livello di input è adeguato anche se non incollo le labbra all’oggetto come un cantautore *d’antan*.

Anche se l’acquisto era mirato esclusivamente alla scrivania e sono soddisfattissimo, ho provato per becera curiosità ad attivare il microfono su iPad, senza successo. In teoria il mio hub USB-C permetterebbe di collegare il cavo del Q2U e persino l’alimentazione, fosse necessaria. Nella pratica, da GarageBand a Voice Recorder, non ho combinato alcunché. Samson dice che è sufficiente collegare un *Lightning Camera Adapter*, indicazione che oramai vale più per iPhone. Forse lo hub, che è di buona qualità ma assolutamente cinese, non è abbastanza certificato? O è un problema di alimentazione? Non ho risposta al momento. YouTube contiene diversi video che mostrano serenamente l’uso con successo del Lightning Camera Adapter. Magari serve proprio quello invece di uno hub più generico? Oppure un ingresso USB-C in iPad non è ancora supportato? Lo scoprirò con calma, senza perderci il sonno, dato che ho quello che mi serviva: un buon microfono da scrivania per comunicare al meglio durante riunioni, workshop, assemblee, podcast.