---
title: "L’Internet di una volta"
date: 2022-01-17T01:04:56+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Mi dicono che sia futile insistere su [Wordle](https://macintelligence.org/posts/2022-01-12-un-bel-wordle-dura-poco-ma-apre-un-mondo/), che è un *giochino*.

Trovo, al contrario, che sia una dimostrazione perfetta delle possibilità di Internet, meno come mezzo tecnico e più come piattaforma unificatrice della nostra comunicazione, che potrebbe essere usata per dare il meglio.

Talmente perfetta che cinque anni fa uno sviluppatore ha lanciato una app di nome Wordle!, che non si è filato nessuno.

Uscito il *giochino* che per browser e gratuito, un esercito di *wannabe* si è buttato sulla app, a pagamento, con l’idea che fosse la stessa cosa.

Che cosa avresti tu se fossi stato lo sviluppatore di Wordle!, la app, nel vedere che improvvisamente entrano soldi in tasca per merito di qualcos’altro?

Lo sviluppatore di Wordle!, la app, ha contattato quello di Wordle, il *giochino* per browser, e [gli ha proposto di donare in beneficenza gli incassi della app](https://www.theverge.com/2022/1/16/22886523/wordle-wardle-app-donate-charity-popularity-spike) stessa. Duemila dollari, eh, mica due milioni. Chi donerebbe in beneficenza duemila euro incassati inaspettatamente dalla propria app?

Questa è l’Internet che c’era un tempo, dove si andava per dare più di quello che si prendeva. Dove si dialogava nel rispetto delle altre persone. Dove magari ci si scannava nei newsgroup, però sempre pronti a reincontrarsi virtualmente, magari solo per scannarsi ancora. È che ci si scannava guardando l’altro, invece che se stessi, il che fa tutta la differenza del mondo.

Non tornerà mai più quella Internet lì. Eppure siamo chiamati a valorizzare e sostenere tutte le istanze della Internet di oggi capaci di dare qualcosa più di quello che prendono. E a ispirare il nostro sistema di valori a questo principio che negli anni ottanta o novanta, con i nostri ridicoli modem capaci di impiegare molte decine di secondi per caricare una singola immagine, era un principio scontato.

La modernità di Wordle (e di [Wordle!](https://apps.apple.com/it/app/wordle/id1095569891)) è spiegarci l’Internet di una volta, di quando non c’erano le app e neanche il web, meno che mai i browser. Però c’era un altro spirito.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._
