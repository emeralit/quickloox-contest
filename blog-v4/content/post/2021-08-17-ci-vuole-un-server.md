---
title: "Ci vuole un server"
date: 2021-08-17T01:41:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [curricolo, scuola, BBEdit, JEdit, Atom, Pandoc, Markdown, Etherpad] 
---
Provo a dare qualche suggerimento concreto per una scuola lungimirante, tanto da [insegnare a scrivere anche in versione digitale](https://macintelligence.org/posts/Un-curricolo-per-lestate.html), trattando testo puro e arrivando in un secondo momento a generare HTML per dare tipografia ai contenuti.

L’ideale è avere requisiti talmente bassi da essere soddisfatti a costo zero su qualsiasi piattaforma. Di editor di testo elementari se ne trovano a manciate e ognuno ha il suo preferito. Nel lungo periodo sarebbe preferibile una scelta che consenta un minimo grado di automazione per l’inserimento di tag HTML, ma si fa sempre in tempo a cambiare programma più avanti se si crea questa situazione. Aggiungo che numerosi editor di testo validissimi sono open source e lecitamente scaricabili senza pagare.

Il lato etico del supportare materialmente il buon software libero, anche se non viene chiesto esplicitamente denaro per usarlo, è una questione importante. Se una scuola destinasse a supporto del software libero quello che pagherebbe altrimenti per un mese di Office 365, farebbe qualcosa di molto buono. E dal mese successivo non pagherebbe più. Immagino che il quadro legale sia tipicamente italiano e complichi la vita agli onesti; spero si limiti a questo anziché rendere l’operazione impossibile.

In qualche caso il software potrebbe essere proprietario, se lascia liberi di fare le cose bene. Su Mac, per esempio, BBEdit è scaricabile con licenza dimostrativa, che dopo trenta giorni consente l’uso ma disattiva le funzioni più specialistiche. Quello che rimane e rimane gratis per sempre è un ottimo editor di testo. Comprare magari licenze per i docenti e lasciare lavorare i ragazzi con la versione gratuita potrebbe essere un compromesso decente.

Sul fronte open source si deve fare un discorso diverso. Fermo restando che qualsiasi progetto ha piacere nel ricevere denaro, ci sono tanti modi per procurare beneficio tangibile attraverso attività a costo quasi zero. Anche il solo menzionare il software sul sito della scuola e promuoverlo verso i genitori è moltissimo; poi è possibile impegnarsi a qualsiasi livello di profondità su software e contenuti, per migliorare una versione italiana, contribuire alla manualistica o anche, dove esistono risorse, di dare una mano a livello di codice. Qui non c’è una soglia minima da rispettare; qualsiasi attività diversa dall’indifferenza è preziosa.

C’è anche da chiedersi, per come deve essere organizzata una scuola, se sia meglio concentrarsi sulle scelte software locali oppure ignorarle e puntare tutto sull’uso via webapp. Ne parlo al prossimo giro.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*