---
title: "Toccata e fuga"
date: 2021-05-13T00:28:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [World of Warcraft, Runescape, Hearthstone, Battle for Polytopia, Dungeons and Dragons] 
---
Niente batte una serata di [Dungeons and Dragons](https://dnd.wizards.com) con gli amici.

Impegnare cinque minuti di pausa con [Battle for Polytopia](https://polytopia.io) è semplice e stimolante. (Cerchiamo sempre nuovi adepti per sempre nuove battaglie; il gioco in rete costa poco più di un euro, anche se si può prendere come accessorio di una Tesla).

Se serve un gioco facile da imparare e difficile per eccellere, misurarsi con [Hearthstone](https://playhearthstone.com/) è una sfida alla pazienza, alla costanza, al metodo.

È possibile giocare in millemila altri modi; uno dei migliori come combinazione di interazione sociale, gioco in solitaria, vastità dell’esperienza e concentrazione su obiettivi specifici è il gioco di ruolo di massa online.

Mi manca molto [World of Warcraft](https://worldofwarcraft.com/en-us/); giocarlo con giusto impegno, tra lavoro, famiglia e varie-ed-eventuali, bordeggia verso l’impossibile; quantomeno dovrei svolgere un lavoro di quelli dove all’ora prestabilita si chiude tutto e ci si può dedicare ad altro. Non è il mio caso.

Solo che sta per succedere qualcosa che aspettavo da anni: si può già preordinare su App Store [Runescape per iOS e iPadOS](https://info.runescape.com/en-GB/p/mobile), che debutterà il 17 giugno.

Se mi danno un gioco di ruolo di massa di profondità ineccepibile, supercollaudato, ultrapopolato, giocabile occasionalmente da una tavoletta o estraendolo da una tasca, con prezzo di ingresso zero e una interfaccia touch che funziona come si deve, potrei fuggire dal mondo reale molto più spesso e più intelligentemente di ora.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               