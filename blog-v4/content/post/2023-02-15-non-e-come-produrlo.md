---
title: "Non è come produrlo"
date: 2023-02-15T02:13:54+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [iPhone, Cina, India, Taiwan, Shenzhen, Shanghai, Acer, PowerBook]
---
Un amico chiede *ma perché, con le problematiche attuali, Apple non sposta la produzione dalla Cina in altri Paesi?*

Perché non è come dirlo.

In un’altra vita ho avuto la fortuna di essere invitato da Acer per una settimana a Taiwan e conseguentemente a Shanghai, a vedere le fabbriche di computer e assistere dal vivo alla nascita di Shenzhen, una metropoli della tecnologia progettata per alloggiare milioni di persone, creata letteralmente dal nulla, tutta in una volta, strade, edifici, infrastrutture, tutto insieme.

La fabbrica che produceva bancali e bancali di computer HP era la stessa fabbrica che produceva bancali e bancali di PowerBook. *A-ha! Visto che tutti i computer sono uguali?* sghignazza il collega a fianco. Pazientemente, l’interprete spiega che la fabbrica è la stessa, ma i disciplinari sono diversi. Diverse le specifiche, diverse le tolleranze, diverso il costo di produzione, diversa la qualità. Uguali un cavolo.

Questo per dire che il rapporto di Apple (e non solo) con l’Estremo oriente si intreccia da decenni e in decenni succedono tante cose. Apple Silicon viene progettato da Apple, ma la produzione avviene a Taiwan. Le fabbriche capaci di produrre processori di quel livello di precisione e raffinatezza si contano in tutto il mondo sulle dita di una mano e sono il terminale di linee di approvvigionamento complesse, che chiedono infrastruttura intorno, personale competente a molti livelli, canali di fornitura affidabili. Costruire una nuova fabbrica altrove? Si fa, ma occorre individuare un sito adeguato e poi occorrono molti, molti anni per raggiungere la qualità che oggi i taiwanesi, con decenni di esperienza e collaborazione, offrono subito.

Questo se parliamo di chip, di processori, ovvero componenti estremamente sofisticate. Sarà così difficile spostare la produzione di pezzi più semplici in parti del mondo meno problematiche geopoliticamente?

Dipende. Apple, che è consapevole di essere eccessivamente dipendente dai cinesi, poco importa se isolani o continentali, è al lavoro per spostare in India la produzione delle scocche di iPhone. Solo che, al momento, [la metà dei pezzi prodotti è difettosa](https://9to5mac.com/2023/02/14/iphone-casings-produced-in-india/). In India c’è una considerazione del lavoro diversa, il personale va formato, chi lo forma deve avere tempo di capire le sfumature giuste da dare a un rapporto con una cultura molto differente eccetera eccetera.

Sono cose che si superano, come le hanno superate in Cina decenni fa. Però si stima che un contributo percettibile dell’India alla produzione di scocche di iPhone possa arrivare tra tre anni, di investimenti, esperimenti, trattative, contratti che possono rivelarsi una perdita secca di tempo e denaro, situazioni politiche che magari si deteriorano, rischi ambientali non sempre chiari dall’inizio e così via. Non è affatto facile.

Pensare di produrre da qualche altra parte non è come produrre davvero altrove.