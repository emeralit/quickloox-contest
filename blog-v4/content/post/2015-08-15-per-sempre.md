---
title: "Per sempre"
date: 2015-08-16
comments: true
tags: [iPad, CoC, tablet, iCloud, Mac, Apple]
---
Come è noto, le vendite di iPad sono in diminuzione anno su anno e qualcuno ha già iniziato i lamenti funebri, non di iPad ma del *tablet*. A dimostrazione che il gergo è la tomba dell’intelligenza.<!--more-->

Ho passato il Ferragosto a riposare e, nel mentre, a scoprire che [Clash of Clans](https://macintelligence.org/posts/2015-08-10-sotto-la-doccia/) funziona perfettamente sul mio [iPad di prima generazione](http://tablets.specout.com/l/1/Apple-iPad-1st-Gen).

Il mio iPad è nato a maggio 2010. Clash of Clans, riporta la rete, il [2 agosto 2012](https://it.wikipedia.org/wiki/Clash_of_Clans).

Ovviamente andrei più lento. Chiaramente mi mancherebbero servizi importanti, vedi iCloud. Evidentemente l’interfaccia utente sarebbe più indietro rispetto all’attuale. Certamente alcune *app*, anche fondamentali, resterebbero indietro.

Eppure, dato qualche accorgimento e un cucchiaino di buona volontà, potrei svolgere su un iPad di cinque anni fa quasi le stesse attività che pratico ora sul mio iPad ufficiale. Sottolineo che da mesi l’aggeggio resiste come componente della cesta dei giochi di mia figlia, per la quale – un anno ancora da compiere – è nient’altro che un blocco di metallo da maltrattare, a volte misteriosamente luminoso.

Che è un [terza generazione](http://tablets.specout.com/l/125/Apple-iPad-3rd-Gen), datato fine 2012. Non perde un colpo ed è del tutto attuale.

La situazione è diversa rispetto a un Mac. Perché un iPad dedica la gran parte delle risorse di elaborazione a una *app* per volta (prossimamente [anche due](https://www.apple.com/it/ios/ios9-preview/#ipad), ma ci si ferma lì). Anche un iPad anzianissimo possiede risorse ancora sufficienti per una *app*.

Sul mio Mac girano in questo momento ventiquattro applicazioni. Nessun problema ma, ove servisse la massima velocità, dovrei chiuderne venti, o sacrificare l’efficienza.

Nel creare iPad, Apple ha dato vita a una categoria di apparecchi estremamente surdimensionata rispetto alla durata nel tempo. Alla faccia dell’[obsolescenza programmata](https://macintelligence.org/posts/2013-09-10-dieci-trucchetti-che-non-ho-capito/).