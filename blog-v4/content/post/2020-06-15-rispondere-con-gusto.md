---
title: "Rispondere con gusto"
date: 2020-06-15
comments: true
tags: [Evans]
---
Insegnare è una cosa difficile. Insegnare online è diabolicamente difficile. Perché il diavolo si nasconde dove ben sappiamo.

Per questo, gli insegnanti bravi si fanno domande su tutto. Ogni aspetto è degno di riflessione e suscettibile di miglioramento. L'ottimo è nemico del bene ovviamente e alla fine una lezione bisogna averla, anche se non c'è stato tutto il tempo che si sarebbe voluto eccetera. Nel contempo, è incredibile come pochi dettagli messi a posto finiscano per fare una grande differenza.

Da guardare Julia Evans, che doveva trasmettere nozioni su Udp e sockets e ha cominciato a chiedersi [come avrebbe potuto migliorare l'esperienza di apprendimento](https://jvns.ca/blog/2020/06/14/questions-to-help-you-learn/). Il tema è tecnico – c'è da imparare, per chi vuole! – e però nulla vieterebbe di adottare lo stesso approccio con i Cartaginesi, la partita doppia, le proprietà della sfera o la geografia astronomica.

Interessante più di tutto che il suo lavoro sia improntato sullo studente. Su quello che potrebbe volere, le risposte che potrebbe dare, la gratificazione che fa bene all'imparare. Il pulsante _Ho imparato qualcosa!_ è semplice, candido, fantastico. Lo studente che prova gusto nel rispondere impara meglio.

Tutto quanto può essere copiato e riutilizzato con poca fatica. A parte qualche gioco di animazione divertente e non strettamente necessario, siamo a Html e Css, la base, la scuola primaria dell'informatica, quello che dovrebbe essere [insegnato e usato nelle scuole al posto di Office](https://macintelligence.org/blog/2019/11/29/tre-cose-da-imparare/), al posto di Google Docs, al posto di tutto.

Detto questo, si capisce bene quanto siano assurde le idee di mettere mezza classe in aula e mezza a casa a guardare l'altra mezza o altre genialate buttate lì in questo periodo per vedere se si rivolta abbastanza opinione pubblica oppure se si possono fare passare senza troppo baccano. Idee di gente che palesemente non si è mai posta domande come Julia Evans. Non che debba essere requisito per la docenza, eh; anche se dovrebbe rientrare nella cultura generale.