---
title: "Macchina vecchia fa buon brodo"
date: 2023-02-06T17:46:13+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [iPad, Terra2]
---
Va di moda discettare di intelligenze artificiali, test di Turing, coscienze sintetiche.

L’intelligenza umana, intanto, è facile da rilevare. Una persona intelligente non si libererebbe mai del suo iPad di prima o seconda generazione, anche se sono passati dieci anni.

[Un iPad torna sempre utile](https://twitter.com/Terra2itter/status/1622321078595997698), qualche volta più di un Mac.

<blockquote class="twitter-tweet"><p lang="it" dir="ltr">Guardo serie vecchie su un iPad di 12 anni la cui batteria va ancora che è una bellezza e iOS è appena stato aggiornato.<br>Voi lo usate ancora il vostro tablet comprato nel 2011. Vero? <a href="https://t.co/d5WgFZY2wE">pic.twitter.com/d5WgFZY2wE</a></p>&mdash; Terra2 (@Terra2itter) <a href="https://twitter.com/Terra2itter/status/1622321078595997698?ref_src=twsrc%5Etfw">February 5, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>