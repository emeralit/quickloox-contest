---
title: "La vera attualità"
date: 2019-11-14
comments: true
tags: [FreeCiv, Gruber, Arment, MacBook, Pro]
---
Consiglio, visto che molti fibrillano giustamente per [il nuovo MacBook Pro](https://www.apple.com/it/macbook-pro/), di leggere [Arment](https://marco.org/2019/11/13/mbp16) (*Per la prima da anni […] possiamo constatare che Apple ama i computer quanto noi*) e [Gruber](https://daringfireball.net/2019/11/16-inch_macbook_pro_first_impressions) (*Questo è un MacBook che una volta ancora si potrebbe mettere in discussione come il miglior laptop acquistabile*).

La vera attualità consiste tuttavia nel ventiquattresimo compleanno di [FreeCiv](http://freeciv.org/). Ognuno decida se e come festeggiare, magari anche donando un paio di centesimi al progetto.
