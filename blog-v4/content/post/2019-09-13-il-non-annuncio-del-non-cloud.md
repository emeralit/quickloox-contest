---
title: "Il non annuncio del non cloud"
date: 2019-09-13
comments: true
tags: [U1, iPhone, Roemmele, De, Biase, Dini, Sole, 24, Ore, Wired]
---
Non è poi così assurdo l’articolo di Luca De Biase sul Sole 24 Ore, [Apple a caccia della sua «terza vita» dopo l’era di computer e iPhone](https://www.ilsole24ore.com/art/apple-caccia-sua-terza-vita-l-era-computer-e-iphone-AF4ikBE) ed è decisamente sopra la media della indecente produzione italiana. Anche Antonio Dini ha scritto [qualcosa di buono](https://www.wired.it/mobile/smartphone/2019/09/10/emozione-apple-dove-sei/) su *Wired*, solo che glielo hanno rovinato con il titolo.

Purtroppo anche i migliori finiscono per ricadere nei commenti da copiaincolla annuale (*manca la sorpresa, una noia*, dice Antonio) e De Biase scrive *In effetti, dalla Apple si pretendono innovazioni più radicali.*

Che bello se, per una volta, si fosse fatto giornalismo di ricerca invece che di chiacchiera, per quanto curata. Si sarebbe potuto per esempio scrivere [Che cos'è il nuovo chip U1 di Apple e perché è importante](https://www.quora.com/What-is-the-new-Apple-U1-chip-and-why-is-it-important/answer/Brian-Roemmele), sottotitolo *L’annuncio più grosso di Apple di Apple è ciò che Apple non ha ancora annunciato. Per adesso*.

La fine: 

>Accelerometro, GPS e sensori di prossimità su iPhone hanno contribuito a definire l’ultima generazione di prodotti. Il chip U1 contribuirà concretamente a definire la generazione seguente.

Il bello arriva ora:

>Sento con una certa intensità che U1 verrà visto un giorno come l’aspetto più importante del keynote del 10 settembre 2019. Lo vedremo come l’inizio del computing HyperLocal, che finirà per ridurre la domanda di cloud. Nel computing HyperLocal e HyperContextual memorie a cristalli olografici e alte velocità di elaborazion3 locale renderanno ridondante e meno utile il cloud come lo conosciamo. Apparecchi capaci di memorizzare petabyte di dati, potranno memorizzare tutti i dati del proprietario e una parte utile di Internet in un chip locale. Questo va molto oltre l’edge computing della Internet delle cose, di cui si parla molto, e il chip U1 risulterà determinante per la sua attuazione. Ancora una volta abbandoneremo i mainframe e diventeremo cloudless, senza cloud.

Uno scenarietto da nulla, la fine dell’epoca del cloud. Sembra fantascienza e magari lo è; certo, approfondire e formulare qualche ipotesi sarebbe più faticoso che scrivere della mancanza di emozioni, ma servirebbe molto di più. Se a Luca De Biase non pare uno scenario abbastanza radicale, perché non scrive che cosa ha in mente? Farebbe sensazione.

Oppure finirà che Phil Schiller esclamerà sul palco ancora una seconda volta [Apple can’t innovate anymore my ass!](https://macintelligence.org/posts/2016-09-20-con-permesso/) e molto più a proposito di come è andata con la prima.
