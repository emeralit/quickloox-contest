---
title: "A chi dare soldi"
date: 2013-10-19
comments: true
tags: [OSX, iOS]
---
A [Spiderweb Software](http://www.spidweb.com) che produce giochi straordinari, che sembrano vecchi e invece dentro hanno una forza che avvince. A fine mese arriva [Avadon 2: The Corruption](http://www.spidweb.com/avadon2/index.html) per Mac (su iPad l’anno nuovo). C’è appena tempo per comprare il [primo episodio](http://www.spidweb.com/avadon/index.html) della saga e non restare indietro.<!--more-->

A [Panic Software](https://panic.com) che non ne sbaglia una e tiene a lavorare gente con mente fervida e creativa pur essendo *nerd* fatti e finiti. Tra [Status Board](https://itunes.apple.com/it/app/status-board/id449955536?mt=8)[Prompt](https://itunes.apple.com/it/app/prompt/id421507115?mt=8), [Coda](https://panic.com/coda/) e [Transmit](https://panic.com/transmit/) e il resto, è un dovere morale possedere almeno un programma prodotto da Panic (io ne ho due).

A Brett Terpstra e ai suoi [progetti](http://brettterpstra.com/projects/), molto particolari, o non servono a niente o salvano una carriera. Mai banali, però.

A [omz:software](http://omz-software.com) perché con [Pythonista](https://itunes.apple.com/it/app/pythonista/id528579881?mt=8) ed [Editorial](https://itunes.apple.com/it/app/editorial/id673907758?mt=8) ha cambiato il modo di guardare alla programmazione su iPad. E ha spinto alla programmazione (su iPad!) gente che mai se lo sarebbe aspettato. Ci fosse un Nobel per l’evangelizzazione della programmazione.

E poi a…