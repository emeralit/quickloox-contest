---
title: "Le piccole cose"
date: 2024-04-03T02:42:31+01:00
draft: false
toc: false
comments: true
categories: [Internet, Web]
tags: [Eclipses, Nasa]
---
Ci arrabattiamo in mille piccole cose – per scaramanzia non parlerò per esempio di quella funzione visibile in fondo a questo post – da piccoli umani, quando tutto quello che ognuno dovrebbe chiedere a sé stesso di fare è incollarsi al [sito della NASA sulle eclissi](https://science.nasa.gov/eclipses/), usare questi giorni per meravigliarsi con il cervello e poi, al momento giusto, meravigliarsi con gli occhi.

Fanno chiaramente eccezione gli amici e le amiche che ci leggono o ci leggeranno nei prossimi giorni da Texas, Tennessee e compagnia.