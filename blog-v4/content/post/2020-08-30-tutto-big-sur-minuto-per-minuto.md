---
title: "Tutto Big Sur minuto per minuto"
date: 2020-08-30
comments: true
tags: [Mori, Morrick, Big, Sur]
---
A dire che Apple non è più quella di una volta ci vuole un momento e basta sputare una sentenza gratis.

Le persone serie formulano un giudizio solo dopo avere la padronanza della materia, come sta facendo **Riccardo** con la beta di [macOS Big Sur](https://www.apple.com/macos/big-sur-preview/).

[Introduzione](http://morrick.me/archives/8945), [installazione](http://morrick.me/archives/8954), [Control Centre](http://morrick.me/archives/8962), [Finder](http://morrick.me/archives/8979), [Interfaccia utente](http://morrick.me/archives/8989) (più molto altro) e penso che la serie proseguirà.

Un lavoro minuzioso e puntuale, attento ai dettagli, con i raffronti giusti rispetto al passato, equilibrato, profondo, competente, il tutto nel rispetto dell’accordo di non divulgazione cui deve sottostare chiunque passi da una beta di macOS.

Sulla valutazione dell’operato di Apple ho spesso idee diverse da quelle di Riccardo. Nondimeno, per confrontarsi ci vuole un terreno di confronto e lui va preparando ettari di analisi critica del sistema. Nessuno può stare ai livelli delle leggendarie [recensioni di John Siracusa](https://arstechnica.com/author/john-siracusa/) (peraltro pubblicate dopo l’uscita della versione ufficiale), specie nella parte sotto il cofano, ma il lavoro di Riccardo è di gran lunga uno dei più documentati e completi che sia dato di leggere da diversi anni. Oltretutto è fatto per gente che macOS lo usa, invece di dissezionarlo.

Superconsigliato anche per chi non ha neanche aggiornato a Catalina. E grazie.