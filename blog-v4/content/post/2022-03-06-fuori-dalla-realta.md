---
title: "Fuori dalla realtà"
date: 2022-03-06T01:20:53+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [Samsung, Dell, Xps15, i7, i9, MacBook Pro, Android, M1]
---
Chi vive in una realtà parallela e chi in quella canonica, certificata, di riferimento? A volte vengono dubbi.

Vedo su _Ars Technica_ la [presentazione dell’Xps 15 di Dell](https://www.theverge.com/22958468/dell-xps-15-2021-oled-review), _il concorrente che usa Windows più vicino a MacBook Pro 16” che sono riusciti a trovare_.

Macchina che _non è altrettanto potente né altrettanto efficiente_ rispetto a MacBook Pro. Come dire, siamo messi bene. In compenso, lato efficienza, durante i vari test effettuati, la temperatura della CPU è stata stabilmente _sui novantanove-cento gradi_.

Il raffreddamento funziona, nessuno si ustiona, il voto finale è alto. Eppure qualcuno sceglie di spendere tipo duemila euro per mettersi sulla scrivania un fornetto per il pane, certo dotato di processore i7 (c’è una opzione per avere l’i9, ma chi lo raffredda?), che i fornetti convenzionali non considerano. Chi è fuori dalla realtà?

Oppure parliamo di Samsung, che ha ammesso di [limitare sui suoi telefoni la velocità di diecimila app Android](https://twitter.com/GaryeonHan/status/1499009797035008002):

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Samsung created an app called GOS and used the app to limit game performance, making the gaming experience worse. However, according to what the Korean community found out today, Samsung confirmed that it has put performance limits on more than 10,000 apps... <a href="https://t.co/U58AreZZoo">pic.twitter.com/U58AreZZoo</a></p>&mdash; 한가련 (@GaryeonHan) <a href="https://twitter.com/GaryeonHan/status/1499009797035008002?ref_src=twsrc%5Etfw">March 2, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Quando si è saputo, la società coreana ha annunciato l’arrivo di una opzione per fare scegliere le app da prioritizzare rispetto alle prestazioni.

Vale a dire che non c’è modo di scappare dalle limitazioni, tutte; solo indicare app da graziare, che possono funzionare a velocità normale, intanto che le altre vengono limitate.

Chi vive fuori dalla realtà? Gente come il sottoscritto, con un Mac di cui mai ha sentito la ventola in tre mesi, con un iPhone? O gente con cento gradi di portatile sulla scrivania e un telefono con dentro app zoppe _by design_? Viene da pensare che l’epoca delle recensioni sia persino tramontata. Già solo accettare un confronto sembra un atto fuori dalla realtà.