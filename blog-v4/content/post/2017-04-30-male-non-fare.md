---
title: "Male non fare"
date: 2017-04-30
comments: true
tags: [Windows]
---
E quando ho scritto degli [aggiornamenti troppo aggressivi a Windows 10](https://macintelligence.org/posts/2017-02-13-o-paghi-o-ti-aggiorno/) sono stato pure criticato.<!--more-->

 ![Non aggiornare a Windows 10](/images/no-windows-10.jpg  "Non aggiornare a Windows 10") 

Questo è un grosso rivenditore di materiali da pavimentazione nell’hinterland milanese.

La foto è sfocata e mi scuso, ma questioni di discrezione e fretta hanno condizionato l’inquadratura.

Ma quanto è fuori fuoco Windows 10, per suscitare questa reazione?