---
title: "Originali a tutti i costi"
date: 2014-04-08
comments: true
tags: [Threes, 2048]
---
Non c’è solo la retorica di [Think Different](https://www.youtube.com/watch?v=MDM0AzjU_9g): se si sceglie Apple ci sono ragioni che vanno oltre il superficiale, area nella quale ci si trova benissimo con tutte le alternative omogeneizzate che circolano.<!--more-->

Una di queste è la creatività. Mai come negli ultimi anni, complice specialmente App Store, le idee software più eleganti e curiose sono comparse nell’universo Apple.

Ne scrivono gli autori di [Threes](https://itunes.apple.com/it/app/threes!/id779157948?l=en&mt=8) e fanno anche qualcosa in più: pubblicano una [pagina di lunghezza inusitata](http://asherv.com/threes/threemails/#letter) nella quale riportano le mail, gli abbozzi, i progetti grafici, le discussioni, i momenti cruciali dello sviluppo del programma, che è costato un anno e mezzo di lavoro. Una lettura appassionante e istruttiva, un dietro-le-quinte difficile da trovare altrove con altrettanti particolari.

Parlando anche dei cloni, quelli progettati in una settimana scopiazzando l’idea e usciti a decine, forse centinaia dopo Threes. Cloni che oltretutto copiano l’idea, ma non la ricchezza vera del programma:

>Volevamo che i giocatori potessero giocare a Threes per molti mesi, se non per anni. Abbiamo battuto entrambi [2048](https://itunes.apple.com/it/app/2048/id840919914?l=en&mt=8) al primo tentativo. Scommettiamo che la maggior parte di quelli che sono riusciti a totalizzare un 768 o persino un 384 in Threes potrebbero fare lo stesso con la fantomatica “strategia dell’angolo”. Probabilmente puoi riuscirci anche tu! Prova semplicemente a toccare “su” e poi “destra” alternativamente fino a quando ti blocchi. A quel punto premi “sinistra”. Magari non arrivi a 2048, ma potresti battere il record personale.

>Quando un comportamento automatico batte da solo il gioco una volta su cento, beh, il gioco è una fetecchia. Threes è meglio? Pensiamo di sì. A oggi, più o meno solo sei persone in tutto il mondo hanno visto un 6144 e nessuno è ancora riuscito a “battere” Threes.

Uno dirà: perché mai sprecare denaro per un gioco che poi passa quando si può giocare gratis a un clone con dentro la pubblicità che tanto passa lo stesso e la meccanica del gioco è sempre quella. Ecco, non è sempre quella, come mostrato; e parliamo della somma di euro uno virgola settantanove per godersi l’originale, frutto di inventiva e di lavoro creativo, al posto del clone gratuito, scopiazzato e malriuscito.

Non ho scaricato 2048 né altri rompicapo di quel tipo. Se lo avessi fatto, quanto meno renderei omaggio ai veri inventori. Solo loro che rendono speciale un iPhone o un iPad, non il codazzo che li insegue.