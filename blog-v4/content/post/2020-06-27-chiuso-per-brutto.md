---
title: "Chiuso per brutto"
date: 2020-06-27
comments: true
tags: [Microsoft, Store, Apple]
---
Microsoft [chiude definitivamente tutti i suoi negozi fisici](https://www.theverge.com/2020/6/26/21297400/microsoft-retail-stores-closing-cities-open), a eccezione di quattro che verranno _reimmaginati_ come _centri di esperienza_.

Forse li useranno per studiare come mai pensavano che per fare successo come quelli di Apple sarebbe bastato aprirli, a brutta imitazione, negli stessi posti. Dopotutto un negozio è un negozio, no? Un po’ come i computer, alla fine dentro sono tutti uguali. Beh, un attimo…