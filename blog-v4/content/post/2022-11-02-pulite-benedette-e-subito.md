---
title: "Pulite benedette e subito"
date: 2022-11-02T01:05:00+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Keynote]
---
Ci sono zilioni di sistemi assolutamente magici per generare slide a partire da testo Markdown e li adoro.

Ma arriva il momento in cui ti serve una presentazione fatta e finita, pregevole, in poco tempo.

Oggi ho provato a lavorare in modalità Outline. Ho abbozzato il testo in pochi attimi, letteralmente. Poi ho adeguato gli stili del layout scelto al font che volevo e infine ho inserito le immagini.

Pochi ritocchi finali, neanche due ore e avevo tutto praticamente pronto. Le mie slide sono sempre minimaliste, però c’è un elemento di complessità anche in questo.

Keynote su iPad Pro dà la sensazione di maneggiare un panetto di plastilina, che manipoli e ci fai quello che ti pare. Sensazione che su Mac, per quanto sia amichevole e scorrevole, proprio non provo.