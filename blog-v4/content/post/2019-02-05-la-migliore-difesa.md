---
title: "La miglior difesa"
date: 2019-02-05
comments: true
tags: [Super, Bowl, Rai, iPad]
---
Il [Super Bowl](https://www.nfl.com/super-bowl) LIII è stato cerebrale più che spettacolare: hanno regnato le difese in una guerra di trincea che alla fine ha premiato chi, secondo pronostico, nel momento che conta avrebbe saputo esattamente che cosa fare e lo avrebbe fatto con precisione.

La classica partita che delude il pubblico ma compiace i tecnici. Per chi sa leggere appena appena dietro le quinte del football americano, la partita a scacchi tra coach offensivi e difensivi è stata affascinante, anche se visivamente noiosa. E sempre più coinvolgente, perché con l’andare dei minuti si capiva che sarebbe bastata una singola mossa, geniale o ottusa, a decidere tutto. Così è stato e forse ha vinto il migliore; certamente ha vinto il più preparato ed efficace per un arco di tempo più lungo.

Passando al lato tecnologico, rispetto agli anni scorsi mi tocca dire qualcosa di positivo sulla Rai e mi sorprendo. Intanto hanno acquisito l’evento da trasmettere (per il resto, a quanto mi risulta, c’era solo lo *streaming* di Dazn) ed è molto, per l’emittente di stato; mi trovavo a seguire la partita in un posto con connessione *cheap*, senza pretese, e su Mac – con oltre un centinaio di finestre di Safari aperte – continuavo a perdere fotogrammi; su iPad Pro invece tutto è andato liscio e la [app di Raiplay](https://itunes.apple.com/us/app/raiplay/id501323740?mt=8) si è impuntata solo una volta, solo pochi secondi.

Un velo pietoso piuttosto, parlando di Rai, sulla copertura, con stantii siparietti in studio dettati dalla pubblicità americana, condotti da un giornalista effettivamente incompetente sulla materia, piazzato lì per dovere e ospiti ancora più noiosi della partita, con la sola eccezione dell’eterno Guido Bagatta a cui sembra non esserci alternativa per avere un commento minimamente informato.

Per un Super Bowl vale comunque la pena di sopportare qualche sproloquio inutile; dopotutto la stessa Nfl, molto ben pagata per farlo, ci infligge da qualche anno pubblicità di Surface di cui [si è già scritto quello che serviva](https://macintelligence.org/posts/2015-09-22-basta-pagare/). E neanche la difesa dei Patriots è sufficiente a evitarla.