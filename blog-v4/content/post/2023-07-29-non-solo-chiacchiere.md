---
title: "Non solo chiacchiere"
date: 2023-07-29T12:45:25+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Reddit]
---
Ogni tanto leggo che i blog sono morti, perché i social, le piattaforme di collaborazione, il cloud, adesso l’AI e domani il gomito che fa contatto col piede.

Più che di blog è questione di spazi web personali. Sì, costano qualche euro al mese. Sì,hanno bisogno di cure e di imparare cose, proprio come un vaso di fiori sul davanzale. Sì, è anche per questo perché insisto sul lavorare con il testo, con HTML, con CSS, non con le sedicenti applicazioni per la produttività personale. Quelle non sono adatte per pubblicare su web.

Perché uno spazio web personale? Perchè uno deve essere libero di perdere e buttare commenti, chiacchiere e thread come e quando gli pare. Almeno ha una responsabilità personale. Almeno decide il quando e il come.

Altrimenti l’alternativa è affidarsi, che so, a Reddit che di punto in bianco [elimina le chat anteriori al 2023](https://mjtsai.com/blog/2023/07/28/reddit-deleted-years-of-chat-history/).

Come se fossero nient’altro che chiacchiere. Invece no.