---
title: "Testardo come un emulatore"
date: 2013-12-28
comments: true
tags: [Amiga, Mac, browser, Css3, Atzeni, Linux, JavaScript, Html, Ibm, Dos, Windows]
---
Due settimane fa si accennava alla possibilità di avere [emulazione di Amiga 500 dentro un browser](https://macintelligence.org/posts/2013-12-13-retromomenti/), purché fosse Chrome.<!--more-->

Per non lasciare indietro nessuno aggiungo [Mac Plus con System 6 e applicazioni varie](http://jamesfriend.com.au/pce-js/) (con variante di Pc Ibm con Dos 1.0 o Windows 3.0). L’autore offre una carta varietà di [emulatori molto vecchi](http://www.hampa.ch/pce/download.html) di Mac, Pc e… Atari.

Probabilmente avevo già accennato da qualche parte a una [emulazione di Linux in JavaScript](http://bellard.org/jslinux/), funzionante anche su iPhone e iPad.

Va anche detto che più si torna indietro nel tempo, più diventa troppo facile. La finestrella dell’emulatore di Mac Plus dentro Safari sembra piccina e invece corrisponde alla risoluzione reale di quel tempo.

Mi entusiasma di più, per quanto del tutto incompleta, la [ricostruzione di un avvio di Lion](http://www.alessioatzeni.com/mac-osx-lion-css3/) creata da Alessio Atzeni con il solo uso di istruzioni Css3. Si può arrivare a ovunque con il moderno Html, se si è abbastanza testardi e capaci.

Chissà come si indigneranno quelli che guai a toccare il *plugin* Flash.