---
title: "Un quiz per palati delfini"
date: 2022-09-02T00:12:20+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [Valencia, Oceanografic, Kahoot]
---
Nell’attesa che cominciasse lo spettacolo al delfinario, l’organizzazione ha proposto un quiz a risposte multiple, per ingannarla, l’attesa.

Avevo già sperimentato [Kahoot](https://kahoot.com), solo però in ambienti controllati, come un’aula o una sala riunioni (dieci-venti-trenta persone), con pubblico relativamente limitato e selezionato (alunni, dipendenti). Qui invece si parlava a un paio di centinaia di possessori di computer da tasca, assemblato del tutto casualmente tra i visitatori.

Il Pin per entrare nel gioco (basta solo quello) è stato mostrato sul megaschermo a bordo vasca e così le domande. Alla fine è stato visualizzato il podio, formato da chi aveva dato più risposte esatte in meno tempo.

Tutto piuttosto usuale nell’ambito di software di questo tipo; la curiosità reale era vedere come performava in una situazione molto aperta, disordinata, con numeri ampi di partecipanti.

Ho partecipato a due sessioni e sono rimasto positivamente impressionato. Kahoot non ha perso un colpo, ha permesso a tutti di partecipare nel giro di pochi secondi, ha tenuto a bada almeno centocinquanta partecipanti effettivi, è stato sempre reattivo e preciso. Complimenti.

Nella prima sessione ho sbagliato un paio di domande e naturalmente il piazzamento ne ha risentito. Ho partecipato alla seconda sapendo in anticipo quasi tutte le risposte (il quiz era parzialmente diverso da quello precedente e anche qui, complimenti) e in sostanza gareggiando sul tempo di risposta. Sono arrivato terzo e questo mi ha permesso di valutare grossolanamente anche la capacità di Kahoot di differenziare con efficacia il valore delle risposte in base alla loro velocità.

In totale, era una situazione complessa, proprio perché semplicissima dal punto di vista dei partecipanti, ed è stato molto istruttivo trovarsi nella situazione. Se mi troverò a operare in eventi con grandi numeri di partecipanti, sicuramente terrò presente le potenzialità di Kahoot anche da un punto di vista della fiducia riversabile sul codice, altrimenti difficile da validare prima di provarci.

Lo spettacolo dei delfini forse avrebbe sarebbe riuscito meglio con tempi un po’ più serrati e una conduzione più galvanizzante, però le ragazze erano stracontente e conta solo quello.

Contento anch’io, però, accidenti, terzo su centocinquanta, quando sarebbe bastato accelerare di un decimo di secondo o due. Proprio, non si può avere tutto.