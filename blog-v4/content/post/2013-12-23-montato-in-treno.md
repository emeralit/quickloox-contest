---
title: "Montato in treno"
date: 2013-12-23
comments: true
tags: [iPhone, iMovie]
---
Mi scrive Federico di avere pubblicato questo [filmato](https://www.facebook.com/photo.php?v=10202815635701423) e la cosa degna di nota è come.<!--more-->

>Montato in treno su iPhone 5 al ritorno da un pranzo nuziale.

Dopo una cosa del genere, la domanda *quanto hai pagato iPhone* può avere come risposta solo *molto meno del gusto che si prova*.

(E a saperlo avrei certamente cercato di imbucarmi. No, non per il pranzo.)