---
title: "Così non impari"
date: 2023-04-19T00:48:48+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [AI, intelligenza artificiale, Artificial Intelligence, IA, LLM, The Gostak, ChatGPT, Dædalus, Christopher D. Manning, Manning]
---
Uno dei modi di generare fumo attorno a un argomento è alzare il livello del discorso fino al punto che nessuno più è in grado di seguirlo compiutamente.

Per esempio, tale Christopher D. Manning, secondo il quale

>non possono esserci dubbi sul fatto che i modelli linguistici preaddestrati imparino significati.

*Imparare significati*, nel quadro delle ricerche sull'intelligenza artificiale, è una cosa grossa. Manning deve essere un luminare, poiché [il suo articolo](https://www.amacad.org/publication/human-language-understanding-reasoning) su *Dædalus* (che già non è *Hardware Upgrade*) è un saggio ponderoso sulla storia dell'elaborazione del linguaggio naturale e sulle sue applicazioni in ambito, appunto, di sistemi informatici pensanti, senzienti, autocoscienti, quello che si vuole.

Con chiarezza: le capacità dei sistemi generativi attuali di trattare il linguaggio naturale, sistemi a disposizione di tutti gratis o semigratis, sono straordinarie e non si è mai visto prima niente del genere, su nessuna scala. I sistemi suddetti sono eccellenti per un ventaglio delimitato di applicazioni e, usati con giudizio, fanno risparmiare tempo e fatica. *Imparare significati* è però un altro campionato. Manning ne è cosciente e scrive di non essere d’accordo con chi considera l’output dei sistemi generativi *un rigurgito di fatti distribuzionali o sintattici*.

Per poter formulare impunemente l’affermazione, Manning deve prima fornire una definizione di significato che si adatti alla sua visione delle cose:

>comprendere il significato consiste nel comprendere reti di connessioni di forme linguistiche.

Ho il sospetto che, sotto sotto, si stia dicendo *i sistemi generativi funzionano così e quindi il così deve potersi chiamare comprensione del significato*. Però non sono uno studioso, il mio livello di istruzione è modesto e potrei avere un precondizionamento negativo nei confronti dell’articolo e di conseguenza dell’autore.

Da ignorante, tuttavia, posso inventare un esperimento veloce: fare giocare ChatGPT (o qualunque altro assistente) a [The Gostak](https://iplayif.com/?story=https%3A%2F%2Fifarchive.org%2Fif-archive%2Fgames%2Fzcode%2Fgostak.z5), di cui [abbiamo parlato pochi giorni fa](https://macintelligence.org/posts/2023-04-15-twas-brillig/): una avventura testuale scritta per metà in inglese e per metà in un linguaggio inventato.

**Delcot**  
*This is the delcot of tondam, where gitches frike and duscats glake. Across from a tophthed curple, a gomway deaves to kiloff and kirf, gombing a samilen to its hoff.*
 
*Crenned in the loff lutt are five glauds.*

Questo è l’inizio di *The Gostak*. La sfida, rispetto a una avventura testuale convenzionale, consiste come sempre nell’arrivare in fondo, solo che ciò è possibile unicamente dopo avere *imparato il significato* delle parole inventate. *Frike* e *Glake* sono quasi certamente verbi, *kiloff* e *kirf* potrebbero essere magari direzioni. Insomma, chiunque di noi inizia a presupporre i significati e poi a compiere esperimenti che man mano, lo ripeto a martello intenzionalmente, ci portano a *imparare il significato* della parte inventata del testo.

*Reti di connessione di forme linguistiche*. Ci vuole poco, davvero, per scoprire che esiste la parola *jirf* e sospettare che abbia una connessione con *kirf*. Non vado oltre per non rovinare il piacere dell’esplorazione. A me pare ingenuamente l’inizio di una rete di forme linguistiche connesse.

Quindi mi sono messo a fare da guida a ChatGPT per farlo giocare a *The Gostak*. Gli ho spiegato la situazione, gli ho spiegato come formulare il prompt in un’avventura testuale classica, gli ho scritto che ci sono termini sconosciuti da decifrare, ho cercato di fare tutto il possibile per raggiungere il risultato.

Zero.

Attendo qualcuno che mi mostri prompt raffinati per farmi capire come e dove ho sbagliato. Ciò che ho ottenuto io è zero. Di fronte alla rete di connessione di forme linguistiche presente in *The Gostak*, ChatGPT ha imparato esattamente nulla. Ha iniziato a farsi delle idee, in generale di ottimo livello, sul significato generico dei termini sconosciuti e me lo aspettavo, da un maestro del linguaggio naturale: il riconoscere *deaves* come un verbo dipende, chiaramente, dalle parole che ci sono intorno. Se non sapesse fare questo, non andrebbe da nessuna parte. È una base del sistema.

Solo che poi, il deserto, l’abisso, il limbo, la lobotomia. Una volta che ha per le mani *deaves*, il sistema di generazione di testo non sa che farsene. Non fa parte del suo addestramento. Non elabora un prompt valido, per limitarsi a dare descrizioni generiche delle sue intenzioni. Non fa un esperimento. Non riesce a mettere in relazione *kirf* e *jirf*, come invece a un umano ignorante salta all’occhio in modo clamoroso. Gli sfugge totalmente, rumore di caduta dell’asino, il *significato* delle parole sconosciute.

La situazione si fa patetica. Sembra di tornare a sessant’anni fa, quando [Eliza](http://psych.fullerton.edu/mbirnbaum/psych101/eliza.htm) iniziava a girare in tondo appena si forzava un momento il banale algoritmo del programma.

Alle prese con *The Gostak*, ChatGPT non ha compreso alcuna connessione di rete delle forme linguistiche. Non ha imparato alcun significato. È rimasto nel suo campetto di centosettantacinque miliardi di punti di informazione, insufficienti a svelare il mistero di *kirf*.

A me sembra una confutazione diretta e definitiva delle affermazioni di Manning. Qui girano plurilaureati, programmatori, ricercatori, ingegneri, imprenditori, giornalisti, avvocati, persone di intelligenza superiore qualsiasi sia la loro qualifica. Per favore, qualcuno mi mostri che mi illudo e che ChatGPT riesce a giocare a *The Gostak*. Ovviamente senza dirgli *noi* che cosa vuol dire *delcot*, o senza creargli i prompt giusti, altrimenti la vittoria è troppo facile. Nondimeno, se il sistema arriva a usare appropriatamente il linguaggio inventato solo una volta che gli abbiamo fornito i significati, dovrebbe voler dire che ChatGPT e i suoi confratelli non imparano né comprendono alcun significato e sì, rigurgitano mirabilmente fatti sintattici e distribuzionali, non oltre però.

Se basta un ignorante con un *divertissement* trovato su Internet a fare saltare la costruzione di Manning, la situazione è davvero grave. Chiedo aiuto a qualcuno disposto a superare le mie capacità e dimostrarmi che le cose stanno diversamente da come le capisco. Grazie.