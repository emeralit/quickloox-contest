---
title: "Un bel conformismo"
date: 2023-08-27T02:18:55+01:00
draft: false
toc: false
comments: true
categories: [Software, Hardware]
tags: [Asahi Linux, M1, M2, OpenGL ES 3.1]
---
Personalmente preferisco un Mac con macOS, ma giudico vitale che esista sempre la possibilità di installare sistemi operativi open source sui computer Apple.

C’è un’ottima notizia a questo riguardo: sono stati pubblicati i [nuovi driver grafici OpenGL per Asahi Linux su Apple Silicon](https://rosenzweig.io/blog/first-conformant-m1-gpu-driver.html). È così ottima da essere ancora migliore: non si tratta del gioco di prestigio di un maghetto dello hacking che con un salto mortale carpiato riesce a fare partire qualcosa pieno di bug e che non partirà a nessun altro; i driver – versione OpenGL ES 3.1 per i precisi – sono *conformant*, ovvero hanno passato infinite batterie di test previsti dalle organizzazioni che amministrano gli standard Linux.

Sono driver standard Linux, ufficiali, testati, sicuri, funzionanti.

Il Linux delle acrobazie serve a fare notizia; quello conforme e standardizzato aiuta a rendere il mondo (informatico) un posto migliore.

Ah, *MacRumors* parla di [gaming update](https://www.macrumors.com/2023/08/23/macs-running-linux-receive-major-update/): con tutto il bene che voglio ai giochi per Mac, non saranno nuovi driver a fare diventare Mac una macchina da *gaming*.

Farla diventare un’ottima macchina Linux, invece, è tutt’altra questione e pure interessante.

*Gli aggiornamenti in agosto del blog potrebbero essere irregolari o infrequenti.*