---
title: "La libertà"
date: 2014-04-23
comments: true
tags: [Reddit, SektionEins, Esser, iPhone, iTunes]
---
Su Reddit è apparsa la [discussione](http://www.reddit.com/r/jailbreak/comments/23b7qs/what_is_unflod_its_a_mobile_substrate_addon_that/) relativa a un *malware* di origine cinese che cerca di succhiare l’identità Apple del possessore sprovveduto di iPhone sottoposto a *jailbreak*.<!--more-->

Stefan Esser su SektionEins [analizza il *malware*](https://www.sektioneins.de/en/blog/14-04-18-iOS-malware-campaign-unflod-baby-panda.html) con rara chiarezza e completezza.

*Malware* che nulla può contro un iPhone normale, non sottoposto a *jailbreak*.

Non mi interessa sapere quanta gente sia stata colpita. Mi interessa il fatto che domani uno di questi si ritroverà privato del proprio *account* iTunes e darà la colpa all’insufficiente sicurezza di Apple.

A consigliarlo sarà stato probabilmente qualche sostenitore della *libertà*, intesa come *l’ho comprato, sono libero di farci quello che mi pare*.

Fino a che si parla di App Store, tutto bene. Per favore però, non essere il mio vicino di condominio e non applicare il tuo concetto di libertà alla tua cucina a gas. Grazie.