---
title: "Giochi come una volta"
date: 2022-10-22T03:11:00+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Pillars of Garendall, Cythera, Maelstrom, Marathon, Bungie, Insider Gaming, Angband, Baldur’s Gate, Dark Castle, Oids, Myst]
---
Se tolgo [World of Warcraft](https://worldofwarcraft.com/en-us/) e [Hearthstone](https://hearthstone.blizzard.com/en-us), che fanno abbondantemente storia a parte, le mie esperienze di gioco coinvolgente su Mac sono limitate e distanti nel tempo. Fa eccezione anche [Angband](https://rephial.org/), che è in realtà un gioco da Terminale, più che da Mac.

Rimangono titoli come [Maelstrom](https://www.libsdl.org/projects/Maelstrom/), [Pillars of Garendall](https://en.wikipedia.org/wiki/Pillars_of_Garendall), [Baldur’s Gate](https://apps.apple.com/it/app/baldurs-gate/id568196938?l=en&mt=12), [Oids](https://www.macworld.com/article/153334/oids.html), [Cythera](https://tvtropes.org/pmwiki/pmwiki.php/VideoGame/Cythera), a volte giocati su hardware persino precedente a PowerPC. Senza bisogno di tornare indietro a [Dark Castle](https://classicreload.com/mac-dark-castle.html). Ah, e [Myst](https://cyan.com/games/myst/).

Diciamo che se fossero veri i segnali di un [*reboot* di Marathon da parte di Bungie](https://insider-gaming.com/bungie-to-revive-marathon/), proverei a essere interessato sperando di trovare un posticino appropriato in agenda.