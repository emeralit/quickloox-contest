---
title: "La notizia incredibile"
date: 2021-06-21T00:36:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [iPad] 
---
*[Ho passato due settimane a cercare il mio iPad](https://twitter.com/aqaworldwide/status/1406651300595453958)*.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">I spent two weeks looking for my iPad 😭😭 <a href="https://t.co/PfGbxVPq97">pic.twitter.com/PfGbxVPq97</a></p>&mdash; AQA (@aqaworldwide) <a href="https://twitter.com/aqaworldwide/status/1406651300595453958?ref_src=twsrc%5Etfw">June 20, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Perché incredibile? Beh, io non avrei resistito due ore.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*