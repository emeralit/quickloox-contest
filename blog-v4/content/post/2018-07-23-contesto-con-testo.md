---
title: "Contesto con testo"
date: 2018-07-23
comments: true
tags: [LambdaMOO, Curtis, Mud, Moo]
---
Si potrebbe avanzare critiche a come funzionano i social o su come è la vita, appunto, sociale in rete se si fosse frequentato per un tempo significativo un Mud e familiarizzato con la comunicazione esclusivamente testuale in un ambiente a tema.

Si avrebbe l’idea di che cosa può essere la rete per favorire la comunicazione, anche tra sconosciuti, e la nozione di esistenza di contesti nei quali è possibile la convivenza e anche qualcosa in più.

A dire il vero inizio a essere convinto che per avere convivenza civile su Internet occorra un contesto gamificato, come quello di un Mud o, più modernamente, di un gioco di ruolo immersivo alla World of Warcraft (lo dico ma sono parzialmente convinto, dovrei meditarci sopra).

Il problema dei Facebook e succedanei è che non c’è gamificazione, che poi è una infrastruttura che rende assolutamente chiara la futilità di prendersi sul serio oltre un certo punto: l’antidoto alla crisi comunicativa di oggi.

Per i non convinti suggerisco [Una magione piena di mondi nascosti: quando Internet era giovane](https://undark.org/article/wilo-evans-broad-band/). Gran bell’articolo sull’inizio dell’epopea Mud.

Resisto alla tentazione di suggerire un Mud specifico, sono troppi ancora oggi. Meglio [dare un’idea](http://www.mudconnect.com).