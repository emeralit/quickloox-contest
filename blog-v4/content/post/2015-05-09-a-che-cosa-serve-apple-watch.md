---
title: "A che cosa serve Apple Watch"
date: 2015-05-09
comments: true
tags: [Watt, watch, iPhone, Usher]
---
A poterlo spiegare è più che mai titolata [Molly Watt](http://www.mollywatt.com/blog/entry/my-apple-watch-after-5-days).<!--more-->

Testo bianco su fondo nero. Configurabilità delle scorciatoie e delle schermate. Facilità di comunicazione con altri attraverso la ghiera digitale o la trascrizione di parlato in testo.

La possibilità di essere avvisati da una vibrazione che è arrivato un messaggio, senza il rischio di perdere la vibrazione dato che iPhone è in tasca. E poi…

>Posso ricevere indicazioni senza guardare o sentire, tramite gli impulsi tattili che mi arrivano da watch: dodici impulsi significano girare a destra, tre coppie di due impulsi ciascuna significano girare a sinistra.

Molly è affetta da [Sindrome di Usher](http://www.legadelfilodoro.it/chi-aiutiamo/sordocecita-e-sindromi/sindromi/sindrome-di-usher); sente solo con aiuti meccanici e vede abbastanza male da essere legalmente cieca.

Era orgogliosa del suo orologio meccanico con quadrante tattile che le consentiva di leggere l’ora senza doverla chiedere.

watch le consente qualcosa in più, come potersi spostare in modo largamente indipendente grazie alle mappe e ai *feedback* del computer da polso o poter inviare molto rapidamente codici prestabiliti a parenti e amici se si trova in una situazione di difficoltà.

Francamente watch avrebbe un senso anche se Molly fosse l’unica persona sul pianeta a usarlo. Poi si possono aggiungere gli altri milioni.