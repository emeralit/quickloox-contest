---
title: "Bene o male"
date: 2017-04-25
comments: true
tags: [Telegraph, Guardian, Apple, News]
---
Apple News [fa guadagnare traffico e abbonamenti](http://digiday.com/media/apple-news-the-telegraph/) a The Telegraph.<!--more-->

The Guardian [abbandona Apple News](http://appleinsider.com/articles/17/04/21/uk-site-the-guardian-drops-apple-news-in-bid-to-boost-ad-subscription-revenues) (e gli Instant Article di Facebook) dopo un esperimento, giudicandoli inutili per aumentare il fatturato.

La mia esperienza con Apple News è stata fino a qui poco indicativa.

 ![Aspettando Apple News](/images/apple-news.png  "Aspettando Apple News") 

Tuttavia mi sento di dire che lo strumento in sé può essere usato bene o usato male ed evidentemente c’è chi lo usa meglio.