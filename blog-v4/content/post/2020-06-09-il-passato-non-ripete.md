---
title: "Il passato non ripete"
date: 2020-06-09
comments: true
tags: [Microsoft, MSN, IBM, AI, machine, learning]
---
Un vecchio adagio dell'informatica aziendale recitava che [nessuno è mai stato licenziato per avere scelto IBM](https://www.ibm.com/ibm/history/ibm100/us/en/icons/personalcomputer/words/). Scelta collaudata, inossidabile, l'azienda fa così e basta. Nessun bisogno di pensare o porsi un problema, risposta automatica.

Nel tempo, si è cominciata a dire [la stessa cosa per Microsoft](https://wiki.c2.com/?NobodyEverGotFiredForBuyingMicrosoft), profetizzando che sarebbe diventata vera per Google e in realtà rivelando il principio sottostante: non è la scelta migliore e neanche una scelta, semplicemente la via più breve a coprirsi il didietro.

Il passato però non si ripete all'infinito; la realtà cambia, evolve, porta situazioni nuove.

Così avviene che Microsoft [sostituisca i curatori umani delle news di MSN con la propria intelligenza artificiale](https://macintelligence.org/blog/2020/06/01/non-c-e-piu-trippa/). Ed ecco i licenziamenti, a decine, di gente che costa più di un algoritmo.

Pochi giorni e arriva la prima _gaffe_ dell'intelligenza artificiale, che [mette la foto sbagliata a corredo di un articolo sulla questione razziale](https://www.theguardian.com/technology/2020/jun/09/microsofts-robot-journalist-confused-by-mixed-race-little-mix-singers).

Scuse e correzioni, ma non c'è modo di sistemare una scelta sbagliata in partenza. Non si tratta di intelligenza artificiale, ma di _machine learning_: apprendimento automatico capace di comprendere il passato e, in base a quello, riconoscere il presente. Siano foto di gatti, targhe di macchine, risultati sportivi, partite a scacchi o a Go, articoli di attualità.

Siamo andati tutti a scuola a fare _human learning_ e imparare a scrivere, nello stesso modo: abbiamo imparato a distinguere le lettere e a riscriverle in una serie codificata di modi. In famiglia, prima di andare a scuola, abbiamo imparato a contare, a distinguere tra caldo e freddo e mille altre cose.

La differenza è che abbiamo elaborato, a partire dal concetto: qualcuno di noi ha sviluppato un modo tutto suo di scrivere la g minuscola, per dire. Il machine learning non lo sa fare: sa solo imparare a riconoscere la nuova g minuscola, assieme a tutte le altre.

Mettere il machine learning a scrivere le notizie significa presumere che le notizie siano uguali a se stesse, sempre, infinite variazioni di cose già viste, già sentite, già scritte.

Peccato che il passato non si ripeta all'infinito; la realtà cambia, evolve, porta situazioni nuove.

Arriverà sempre una notizia che l'algoritmo non è preparato a trattare, perché non è mai esistita e non ha potuto essere imparata. L'umano lo sa bene. La macchina no e non cambierà, almeno fino a quando arriverà una _vera_ intelligenza artificiale.

Grazie al cielo, gli algoritmi sostituiscono gli umani in un sacco di modi utilissimi. Altrimenti dovrei ricordarmi migliaia di indirizzi di posta elettronica o passare una giornata a sistemare una foto male illuminata.

Pensare che possano sostituire la creazione di storie, pardon, la scrittura di notizie, è triste. Per chi perde un lavoro che avrebbe potuto conservare. Per chi lo pensa, evidentemente allontanatosi dai suoi fratelli umani e al tempo stesso incapace di sostituirli in modo efficace. Diffido di gente del genere e invito a diffidarne.

Grazie a **Mimmo** per l'ispirazione.