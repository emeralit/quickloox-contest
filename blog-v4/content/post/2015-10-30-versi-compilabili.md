---
title: "Versi compilabili"
date: 2015-10-30
comments: true
tags: [Venerandi, coding, Dante, programmazione]
---
Non posso copiare il *post* di Fabrizio Venerandi e incollarlo per intero qui. Ergo, [ecco il link per leggerlo](http://www.quintadicopertina.com/fabriziovenerandi/?p=403). Posso citarne un paragrafo.<!--more-->

>Dante Alighieri era un poeta. Non solo quello, ma era un buon poeta. Ha scritto alcune pagine in versi su cui si è poggiata l’intera letteratura italiana successiva. Ancora oggi c’è chi distingue tra poeti contemporanei “dantisti” e poeti contemporanei “petrarchisti”, per dire. Tutti noi, con buona pace di Odifreddi, lo abbiamo letto e lo studiamo a scuola. Il farlo ci permette di capire parecchie cose sulla storia medievale e sulla nascita della lingua italiana, per dirne due. Questo significa che siamo tutti poeti? No, non siamo tutti poeti, non è detto. Però ci è concesso di scrivere versi. Abbiamo la possibilità, avendo letto Dante e tanta altra gente che lo ha fatto prima di noi, di comporre poesie. Queste poesie, in primo luogo, servono a noi. Per capire meglio chi siamo, per comunicare cose che non possiamo dire in maniera diversa, per creare un habitat confortevole in cui muoverci. La stessa cosa, identica, vale per il coding. Non siamo tutti programmatori. Ma abbiamo ugualmente la possibilità di scrivere la parte di codice che ci è concessa, un codice che serve a noi, per esprimerci, per creare un habitat digitale confortevole. In fondo il listato altro non è che una serie di versi compilabili.

Chi non si legge tutto il *post* originale rimane indietro e ne riceve danno. Chi potrebbe e non si [iscrive al corso relativo](http://www.ebookdesignschool.it/?page_id=197), perde un’occasione d’oro. Chi vorrebbe e potrebbe ma non si iscrive al corso, Dante avrebbe ben saputo dove collocarlo.

Versi compilabili. Ovvio. Adesso.