---
title: "Cosa vedevamo"
date: 2022-01-24T03:16:19+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Soprattutto, [che cosa vedevamo](https://twitter.com/NanoRaptor/status/1485419275888513026). L’interfaccia che avremmo a disposizione se macOS Catalina funzionasse su una risoluzione di 512 x 342 pixel, quella del primo Macintosh.

Seguendo la timeline di @NanoRaptor si notano altri paragoni suggestivi in tema.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">macOS Catalina on 512x324, the same resolution as the original 128k Mac. <a href="https://t.co/SkiIsza1PJ">pic.twitter.com/SkiIsza1PJ</a></p>&mdash; Dana Sibera (@NanoRaptor) <a href="https://twitter.com/NanoRaptor/status/1485419275888513026?ref_src=twsrc%5Etfw">January 24, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._
