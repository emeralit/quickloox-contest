---
title: "Un po' water un po' proof"
date: 2015-07-18
comments: true
tags: [watch, Hockenberry, Apple]
---
Strepitoso [articolo](http://furbo.org/2015/07/14/a-watch-water-and-workouts/) di Craig Hockenberry su watch e l’acqua.<!--more-->

Apple afferma pubblicamente che l’orologio può sopportare al massimo un metro d’acqua per un massimo di trenta minuti e sconsiglia generalmente l’uso in acqua.

In realtà pare proprio che le specifiche siano state sottodimensionate. È noto che molti proprietari di watch ci fanno tranquillamente la doccia, ma qui si parla addirittura di nuoto e di tuffi da quaranta metri.

Come bonus, una spiegazione scientifica e comprensibile del perché l’acqua fa male all’elettronica, i consigli da seguire per usare watch al mare (sintesi: sciacquare dopo l’uso) e anche qualche consiglio agli ingegneri Apple per le prossime evoluzioni dell’interfaccia.

Totalmente da leggere. E quando del resto, se non a metà luglio?