---
title: "Per metà scoperta"
date: 2022-06-02T01:34:24+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [iPadOS, Viticci, Federico Viticci, macOS, MacBook Pro, iPad Pro, MacStories, multitasking, Magic Keyboard, Apple Silicon, Catalyst, iPadOS]
---
Federico Viticci di *MacStories* ha riscoperto Mac: da sei mesi lo *iPad guy* [ha in mano un MacBook Pro con M1 Max](https://www.macstories.net/stories/rediscovering-the-mac/) e apprezza vari aspetti della vita su Mac che su iPad non sono altrettanto soddisfacenti oppure sono da interpretare in tutt’altro modo.

La sicurezza di App Store e dell’ecosistema di app è soddisfacente ma a volte si può avere voglia di installare anche software non conforme, specie se a volerlo è una persona capace, conscia dei rischi oltre che delle possibilità. L’insieme di tastiera e trackpad su MacBook Pro è superiore a quello che offre la Magic Keyboard di iPad. Il multitasking di Mac è più leggero e così via.

Questo significa tutt’altro che rinnegare iPad o dismetterlo e Federico lo scrive più e più volte. Ci sono infatti contesti dove un iPad si comporta meglio di un Mac. Senza andare lontani, quando non serve una tastiera, o è preferibile l’input con Apple Pencil, o con app fatte apposta pensando alla specificità di iPad, iPad funziona meglio di un Mac. Se bisogna ricorrere ad argomenti ancora più biechi, dove fa comodo una fotocamera dorsale, Mac non c’è. Se bisogna ruotare lo schermo di novanta gradi, Mac non lo fa. Se sei in piedi dentro un cantiere a fare rilievi, ti conviene avere un iPad.

Una cosa interessante del lavoro di Viticci verso la ricerca del meglio dei due mondi è che, su Mac, gli danno fastidio le finestre sovrapposte. Giustamente lui la presenta come una preferenza personale; ma chi ha mai gradito realmente le finestre sovrapposte? In verità, abbiamo sempre avuto schermi troppo piccoli per il lavoro che volevamo svolgere.

Lui ha affrontato il problema su Mac lavorando rispetto a quello che vorrebbe su iPad: aprire facilmente app che si dispongono ordinatamente sullo schermo, con un set di scorciatoie che adeguano rapidamente ogni finestra alla situazione.

Così facendo, indica la strada. Ci sono Mac e iPad e infiniti flussi di lavoro, che trovano il miglior compimento in qualche punto a metà strada tra le due piattaforme.

Viticci stesso spiega che il suo rinnovato interesse per Mac deriva per un lato da una insoddisfazione verso la crescita di iPadOS come è avvenuta finora e, per un altro, dalla disponibilità di tecnologie come Apple Silicon, Catalyst (app che girano su iPad e Mac) senza le quali l’abbinamento tra iPad e Mac sarebbe stato più acerbo e complicato.

Non ho certo seguito il mio percorso nella maniera approfondita e competente di Viticci; più modestamente e in modo abbastanza ignorante, ho seguito le esigenze del lavoro e i metodi che mi apparivano più naturali e produttivi di lavorare. Dove mi ha condotto il percorso? A grandi linee, per il cinquanta percento del mio tempo sono su Mac e per l’altro cinquanta percento su iPad.

Non tutti hanno il privilegio di poter usare più apparecchi e, per chi deve compiere una scelta univoca, consiglio una accurata analisi dei propri flussi di lavoro, apprendimento, divertimento, per stabilire senza preconcetti quale sia l’ambiente che soddisfa la maggior parte dei requisiti.

Voglio dire che ero già arrivato mesi fa dove sta Viticci adesso? Certamente no. Lui ha svolto un lavoro di ricerca e di documentazione che io nemmeno ho cominciato. Dal mio modesto punto di vista, condivido al novantanove percento le sue osservazioni.

E questo post è nato a letto, con iPad, per concludersi alla scrivania, con Mac. Mica per capriccio; per naturalezza.