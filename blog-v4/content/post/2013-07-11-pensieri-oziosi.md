---
title: "Pensieri oziosi"
date: 2013-07-11
comments: true
tags: [iTunes, Html]
---
Un paio di brani dentro il mio iTunes accusano una leggera caduta di volume. Può darsi che il disco si sia danneggiato in un settore che interessa il brano e che lo stesso vada rinfrescato. Conservare comunque i Cd, sfruttare Time Machine, usare iTunes Match…? Bello avere tante soluzioni a un problema.<!--more-->

Come attribuire virgolette tipografiche a un testo Html senza creare confusione con le virgolette dentro i comandi Html stessi, che devono restare quelle diritte dei programmatori? C’è chi [scrive un complicato filtro Perl](https://groups.google.com/forum/#!topic/bbedit/oA0oehbE8eQ) e chi invece [se la cava con una espressione regolare](https://groups.google.com/forum/#!topic/bbedit/EQhI0QkR5sM), molto più semplice (almeno se non ci si spaventa di fronte a una espressione regolare. Bello avere soluzioni potenti a un problema.

Il Pentagono [fatica a pagare i propri soldati](http://preview.reuters.com/2013/7/9/wounded-in-battle-stiffed-by-the-pentagon) a causa di otto milioni di righe di codice Cobol presenti nei suoi sistemi e che non si riescono a manutenere e aggiornare. Il punto è che, come ben sa [Carmelo](http://www.linkedin.com/pub/carmelo-di-pasquale/37/756/520), Cobol è un linguaggio vecchissimo ma tuttora pesantemente in uso su sistemi usati da decenni per compiti vitali. Peccato però che ai nuovi studenti nessuno parli di Cobol. Bello avere tecnologia che evolve, a patto di non dimenticare il passato.