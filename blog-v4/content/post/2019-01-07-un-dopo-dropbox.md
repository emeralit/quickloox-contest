---
title: "Un dopo Dropbox"
date: 2019-01-07
comments: true
tags: [pCloud]
---
Sono rimasto incuriosito da pCloud e ho provato a eseguire l’installazione completa. Ovviamente non manca l’opzione di [invitare terzi a usare il servizio](https://my.pcloud.com/#page=register&invite=Azk3ZpAgyIk) e, nel farlo, guadagnare gigabyte di archivio.

Non sono mai stato interessato a rastrellare gigabyte a qualsiasi prezzo né lo sono ora; sarebbe solo interessante, per curiosità e completezza, se uno di quanti leggono accettasse l’invito, con un clic sul link qui sopra.

Il tutto si inserisce in una riflessione che sto conducendo rispetto ai sistemi di archivio online che ho in uso. Attualmente faccio un grande uso di Dropbox e un modesto uso di iCloud Drive. Mi piacerebbe mollare il primo e adottare integralmente il secondo, solo che dovrei cambiare parte del software che uso ora su iOS e il gioco probabilmente non vale la candela.

Per capire quanto la candela valga, sto esplorando opzioni alternative a Dropbox e appunto.

Grazie in anticipo a chi deciderà – mi raccomando, solo se realmente interessato – di regalare un clic.