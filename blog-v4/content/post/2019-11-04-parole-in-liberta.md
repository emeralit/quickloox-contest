---
title: "Parole in libertà"
date: 2019-11-04
comments: true
tags: [Venerandi, Tolkien, Lotr, computer, iPhone, watch, AirPods]
---
(Questo post è intenzionalmente privo di link).

Fabrizio Venerandi, che adoro e considero amico anche senza avere titolo di sua frequentazione e nonostante lui potrebbe a buon diritto negarmelo, sostiene che un apparato sia un computer quando è possibile usarlo per programmare l’apparato stesso; un Mac sarebbe un computer perché è possibile programmarvi sopra una app per Mac, mentre un watch non lo sarebbe, perché non può essere usato per programmare il suddetto watch.

Beh, no; si chiama *computer*. Qualunque cosa sia, *computa*. Cioè calcola, elabora, maneggia, gestisce (detesto la parola usata in ambito informatico, ma qui ci deve stare). La programmazione è un’altra cosa. Se elabora informazioni, è un computer e pazienza se è infilato in un orecchio come un AirPod invece di stare su una scrivania.

Ho molte più argomentazioni di questa, però questa dovrebbe bastare. Ometto i link proprio perché c’è talmente tanto materiale in rete che dovremmo considerare l’argomento definitivamente chiarito. Il linguaggio è uno strumento formidabilmente flessibile ma la possibilità di piegarlo a piacimento è vincolata al mantenimento ragionevole di un rapporto con il resto della comunità. Se non lo chiamano *programming machine*, ci sarà pure una ragione.

E questo discorso del linguaggio mi porta (per collegamento e non per associazione; quanto sotto ha niente a che vedere con quanto sopra) alla nuova traduzione italiana del *Signore degli Anelli*, che sta suscitando controversie. Da tolkieniano convinto per quanto superficiale, ho provato ad approfondire. Di link in link è emersa una storia brutta.

Il problema ha infatti smesso di essere la bontà della traduzione quanto invece la voglia di presidiare l’opera di Tolkien con la propria visione politica. A me pare una cosa abominevole, non in se stessa – il diritto di fare politica attraverso il proprio lavoro può sfociare nel cattivo gusto, ma in sé è sacrosanto – quanto perché si tratta di Tolkien; uno che ha scritto a chiare lettere e in termini inequivocabili di non considerare il *Signore degli Anelli* un’allegoria o un rimando al mondo reale. Leggere Tolkien con un pregiudizio politico è ignorare l’indicazione dell’autore; figuriamoci tradurlo.

Consci di questo, i perpetratori dell’operazione ideologica hanno manovrato per presentarsi sopra ogni sospetto, buttandola sulla fedeltà della traduzione, che sarebbe più accurata e filologicamente rispettosa di quella precedente.

Non ho titoli per verificare le affermazioni dei nuovi e dei vecchi traduttori, anche se si vede chiaramente come il dibattito sia degenerato nella solita rissa tra schieramenti contrapposti. Di Tolkien, si capisce, importa poco e si vuole più portare il comsemso verso la propria parte più che rendere correttamente i toponimi della Contea.

L’asino, tuttavia, casca. Il nuovo traduttore ha alle spalle un curriculum prestigioso, riconoscimenti, studi approfonditi, tutto. Per ogni nuova traduzione ha pronta una spiegazione che riporta alle nostre radici latine, ai diversi significati dei diversi termini nelle diverse lingue, alla coerenza con la guida per i traduttori scritta dallo stesso Tolkien e così via.

Il libro però contiene poesie, che a volte sono canzoni. Le poesie, o canzoni, hanno una metrica. Spesso sono cantate intorno a un fuoco, o rimembrate da un personaggio, o risuonano dentro un cunicolo, o sono incise in un anello eccetera eccetera. Le poesie del *Signore degli Anelli* ci trasmettono il *sentire* dei personaggi, le tradizioni e la cultura delle popolazioni della Terra di Mezzo, quella parte di sapere che sui libri si trova incompleta perché ha bisogno del nostro cuore e della nostra interpretazione di viventi: la *cultura*.

Il nuovo traduttore ha lavorato come nessun altro sulla filologia. E ha levato la metrica dalle poesie.

Domanda da ignorante: accetteresti una traduzione della *Divina Commedia* priva di metrica? E una di *Bocca di rosa* di De André? Puoi pensare di trasmettere il senso della canzone senza trasmetterne il ritmo?

L’asino casca perché, senza metrica, resta poco più che una esercitazione letteraria. Non è più malinconia degli Elfi o orgoglio nanico, o storia diventata leggenda. Potrebbe essere un qualunque Bret Easton Ellis, avvincente, coinvolgente, attuale, irrinunciabile. Ma i suoi personaggi parlano la sua voce. Vale per qualsiasi scrittore moderno. Quando i popoli cantano, ricordano, celebrano, lo fanno con una metrica.

Se si traduce poesia, prescindere dalla metrica è quantomeno strano. Douglas Hofstadter ha bene illustrato la problematica in *Le ton beau du Marot*.

Tolkien ha lavorato decenni a fare parlare i suoi personaggi con la *loro* voce, a partire dalla *loro* cultura. Il nuovo traduttore vuole imporre la sua cultura e per questo l’operazione, per quanto ammantata di sapienza, è subdola e meschina.

Il linguaggio è una piattaforma universale di interscambio. Chi vuole controllare il linguaggio, insegna Lewis Carroll, vuole il potere. Ci sono milioni di altri libri su cui esercitare questa avidità; farlo su Tolkien è un colpo basso e cattivo, assestato su materiale scritto con il preciso intento contrario.

Comsiglio a chiunque di imparare inglese sufficiente a leggere *The Lord of the Rings* al posto di dare soldi a questa gente piena di ideologia e vogliosa per brama di potere di negare qualunque cultura diversa dalla loro. Intendono incatenare i lettori del *Signore degli Anelli* con la neutralizzazione della storia dell’Anello del potere, attraverso la neutralizzazione delle culture della Terra di Mezzo.

Speriamo in tanti umili Frodo che preferiscano la versione originale, o un buon usato della vecchia traduzione. Vado a mettere sottovuoto la mia copia, ché rimanga leggibile a lungo.
