---
title: "Serial solver"
date: 2023-03-14T01:28:16+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Concetti fluidi e analogie creative, Fluid Concepts and Creative Analogies, Hofstadter, Douglas Hofstadter, ChatGPT, intelligenza artificiale, AI, IA, Sage, WolframAlpha]
---
Ieri si parlava di una [sequenza di numeri presentata da Douglas Hofstadter in Concetti fluidi e analogie creative](https://macintelligence.org/posts/2023-03-13-pattern-chiari-amicizia-lunga/): `0, 1, 2, 720!…`

(`720!` è il *fattoriale* di 720, ovvero `1 x 2 x 3 x 4 x 5 x 6 x 7 x 8… x 720`).

La sfida era, in parte, risolvere la sequenza e capire la regola che la governa e, soprattutto, riflettere su come agiamo e pensiamo per riconoscere lo schema nascosto da indovinare, alla luce del fatto che una supposta intelligenza artificiale è rimasta lontanissima da una soluzione efficace.

Perché l’intelligenza, Hofstadter ha ragione, comunque la si voglia definire, comprende in sé la capacità di riconoscere schemi e spiegarli. Questa sequenza è interessante perché la sua prima parte è del tutto anonima. La serie `0, 1, 2…` si presta a infinite soluzioni, una ovvia come il conteggio dei numeri interi, ed è talmente aperta da risultare veramente poco interessante.

Per questo la nostra intelligenza tenderà a spostarsi sul quarto termine. `720!` è talmente fuori schema da risultare *molto* interessante e accendere la curiosità, perché sappiamo che – sotto – uno schema c’è.

Una minima infarinatura di matematica focalizza inoltre l’attenzione sul fatto che il nuovo elemento della serie è un fattoriale; ma lo è anche la sua radice. 720 è uguale a `6!`, cioè `1 x 2 x 3 x 4 x 5 x 6`.

Quindi possiamo scrivere `720!` nella forma `6!!`; il fattoriale del fattoriale di 6.

Qualcuno nota che 6 è il risultato di un fattoriale? Per la precisione, `3!`.

Dunque possiamo scrivere `720!` nella forma `3!!!`, fattoriale del fattoriale del fattoriale di 3.

(L’autore ama la ricorsività come concetto e si vede. Probabilmente anche la capacità di vedere ricorsivo è un sintomo di intelligenza).

Ora usciamo dalla matematica e guardiamo a che cosa abbiamo scritto: tre, seguito da tre punti esclamativi. Può darsi che questa identità tra numero e quantità di punti esclamativi sia qualcosa di significativo: un pattern nel pattern.

Che succederebbe se scrivessimo la serie come `0, 1!, 2!!, 3!!!`…? Tutto funziona. La serie si spiega se pensiamo a mettere dopo ciascun numero tanti punti esclamativi quanti ne vale il numero. L’elemento successivo della serie sarà allora `4!!!!`. Ecco il pattern svelato. Funziona sia matematicamente (ciascun fattoriale genera i numeri della serie proposta in prima battuta) sia visivamente. 0 è un elemento legittimo in quanto dobbiamo associargli un numero di punti esclamativi equivalenti al suo valore. Appunto, zero.

Per l’intelligenza presunta artificiale, 720! è interessante quanto 2 o 1 e questo la tiene lontana da qualunque possibile approfondimento, men che meno un ragionamento ricorsivo. Ecco perché di intelligente, lì, c’è davvero poco.

Dal punto di vista visivo, chiedere all’intelligenza di proseguire la serie `0, 1!, 2!!, 3!!!…` produce una risposta corretta. Peccato che poi si perda in spiegazioni come mostrare che il prodotto di `3!!!` è uguale a `3 x 1`. Bene avere riconosciuto la sequenza già svelata, pessimo scrivere numeri e operatori a casaccio, ehm, in modo probabilistico.

Un grande grazie a **briand06** per essersi cimentato e avere proposto una soluzione alternativa interessante ed elegante.

Per la cronaca, `720!` vale 2 601 218 943 565 795 100 204 903 227 081 043 611 191 521 875 016 945 785 727 541 837 850 835 631 156 947 382 240 678 577 958 130 457 082 619 920 575 892 247 259 536 641 565 162 052 015 873 791 984 587 740 832 529 105 244 690 388 811 884 123 764 341 191 951 045 505 346 658 616 243 271 940 197 113 909 845 536 727 278 537 099 345 629 855 586 719 369 774 070 003 700 430 783 758 997 420 676 784 016 967 207 846 280 629 229 032 107 161 669 867 260 548 988 445 514 257 193 985 499 448 939 594 496 064 045 132 362 140 265 986 193 073 249 369 770 477 606 067 680 670 176 491 669 403 034 819 961 881 455 625 195 592 566 918 830 825 514 942 947 596 537 274 845 624 628 824 234 526 597 789 737 740 896 466 553 992 435 928 786 212 515 967 483 220 976 029 505 696 699 927 284 670 563 747 137 533 019 248 313 587 076 125 412 683 415 860 129 447 566 011 455 420 749 589 952 563 543 068 288 634 631 084 965 650 682 771 552 996 256 790 845 235 702 552 186 222 358 130 016 700 834 523 443 236 821 935 793 184 701 956 510 729 781 804 354 173 890 560 727 428 048 583 995 919 729 021 726 612 291 298 420 516 067 579 036 232 337 699 453 964 191 475 175 567 557 695 392 233 803 056 825 308 599 977 441 675 784 352 815 913 461 340 394 604 901 269 542 028 838 347 101 363 733 824 484 506 660 093 348 484 440 711 931 292 537 694 657 354 337 375 724 772 230 181 534 032 647 177 531 984 537 341 478 674 327 048 457 983 786 618 703 257 405 938 924 215 709 695 994 630 557 521 063 203 263 493 209 220 738 320 923 356 309 923 267 504 401 701 760 572 026 010 829 288 042 335 606 643 089 888 710 297 380 797 578 013 056 049 576 342 838 683 057 190 662 205 291 174 822 510 536 697 756 603 029 574 043 387 983 471 518 552 602 805 333 866 357 139 101 046 336 419 769 097 397 432 285 994 219 837 046 979 109 956 303 389 604 675 889 865 795 711 176 566 670 039 156 748 153 115 943 980 043 625 399 399 731 203 066 490 601 325 311 304 719 028 898 491 856 203 766 669 164 468 791 125 249 193 754 425 845 895 000 311 561 682 974 304 641 142 538 074 897 281 723 375 955 380 661 719 801 404 677 935 614 793 635 266 265 683 339 509 760 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000.

Il termine seguente, della serie `4!!!!`, è lunghetto. Persino un ambiente matematico come [Sage](https://www.sagemath.org) si sfila dalla faccenda.