---
title: "Mac e banche private"
date: 2016-04-13
comments: true
tags: [Sdr16, Mac, 3D]
---
Sono stato – esperienza interessante – al [Salone del Risparmio 2016](http://www.salonedelrisparmio.com/home). Se si dovesse fare un discorso ad ampio raggio, partirei da uno stand che offriva scansioni 3D del corpo dei visitatori e relativa stampa 3D del modellino di sé, nonché dai robot che adornavano varie scrivanie. Silenti – mostrati unicamente come simboli dei nuovi sistemi di consulenza bancaria semiautomatica – e però eloquenti. Robot e scansioni 3D in un ambiente di promotori finanziari, banchieri privati e fondi di investimento. Sta cambiando qualcosina.<!--more-->

Ma questo era il discorso ad ampio raggio. Quello piccolo riguarda l’abbondanza di Mac in un campo dove nel passato neanche si sarebbero potuti nominare. In un ambiente che non era esattamente la fiera dell’usato da riciclare e dove la quantità di professionisti davanti e dietro gli stand era cento percento. Giusto qualche foto a ricordo della circostanza.

 ![Mac a FinanciaLounge](/images/financialounge.jpg  "Mac a FinanciaLounge") 
Non esattamente un servizio rivolto a dilettanti.

 ![iMac](/images/imac-robot.jpg  "iMac") 
Più che il robot che sembra seguirti con lo sguardo, iMac a tutto schermo per reclamizzare *strategie innovative di investimento*.

 ![Operatori audiovideo](/images/operatori.jpg  "Operatori audiovideo") 
Da Cnbc in giù fioccavano videocamere, microfoni, interviste, dichiarazioni. Più strumenti professionali per l’editing e il montaggio quasi istantaneo.

 ![In sala stampa](/images/sala-stampa.jpg  "In sala stampa") 
Qui si vince facile: la sala stampa, popolata – almeno formalmente – da professionisti.

 ![Una foto piena di Mac](/images/indovina-quanti.jpg  "Una foto piena di Mac") 
Quanti Mac si possono vedere in questa inquadratura volutamente di campo ridottissimo? Sempre in sala stampa.