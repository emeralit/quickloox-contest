---
title: "Una grandezza nelle piccole cose"
date: 2015-09-03
comments: true
tags: [Garmin, watch, Apple]
---
Chiedo scusa per la pessima inquadratura, effettuata in pieno sole contro una vetrina che era tutta un riflesso.<!--more-->

 ![Smartwatch Garmin](/images/garmin.jpg  "Smartwatch Garmin") 

La località è balneare, di una qualche notorietà (qualcuno indovinerà guardando i palazzi riflessi nella foto). Si capisce che oramai i computer da polso siano considerati prodotti da proporre tranquillamente al cittadino medio.

Una cosa che mi ha lasciato perplesso è la dimensione, almeno della [linea Garmin](https://buy.garmin.com/it-IT/IT/indossabili/c10002-p0.html): in tutti i quadranti circolari visibili nella foto trova posto comodamente un watch da quarantadue millimetri. Quello da trentotto ci nuota dentro.

Domanda alle signore: mettereste al polso un computer con questo ingombro?