---
title: "Un problema doppio in meno"
date: 2013-11-14
comments: true
tags: [iCloud, Mac, iOS]
---
**Fabio** aveva un problema di duplicazione di contatti: nella *app* omonima apparivano due volte tutti i gruppi iCloud e relativi nominativi.

Ha risolto. Forse a qualcun altro potrà capitare e dunque pubblico il suo flusso di lavoro, nel caso serva. Faccio anche tanto di cappello, perché al suo posto non ce l’avrei mai fatta.

<ul compact>
	<li>scollego da Internet il Mac</li>
	<li>eseguo il ripristino della rubrica dal backup corretto dei contatti (l’ultimo era duplicato, ovviamente)</li>
	<li>le esporto in formato vCard</li>
	<li>mi collego a Internet</li>
	<li>accedo via web a iCloud e seleziono la rubrica</li>
	<li>cancello i dati presenti  di default</li>
	<li>importo le schede vCard</li>
	<li>attivo la sincronizzazione</li>
</ul>

Così commenta:

>Fastidioso l’aver perso tempo, intrigante aver trovato la soluzione, soddisfatto per saperne un po’ di più di OS X.

La prima purtroppo succede, ma la seconda ripaga e la terza segna una buona giornata.