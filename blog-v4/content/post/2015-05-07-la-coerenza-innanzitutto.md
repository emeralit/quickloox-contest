---
title: "La coerenza innanzitutto"
date: 2015-05-07
comments: true
tags: [watch, Apple, Samsung, Segall, iPod, iPhone, iPad]
---
Ken Segall ha scritto l’[osservazione definitiva](http://kensegall.com/2015/05/one-day-theyll-understand-apple/) sulla possibilità di comprendere il funzionamento di Apple da parte di certi critici.<!--more-->

>Sbagliarsi sul futuro di Apple spesso affonda le radici negli sbagli commessi sul passato di Apple. Chi non riesca a comprendere che cosa ha portato ai successi passati avrà difficoltà a vedere quelli futuri.

Qui non si tratta di critiche più o meno fondate, ma del ripetere pedissequamente le osservazioni con cui si prefigurava il fallimento di iPod nel 2001 per preannunciare il fiasco di watch nel 2015. Le stesse, uguali uguali uguali.

E i ragionamenti a pera sulle quote di mercato, e se-c’era-Steve-non-succedeva, e la fine della capacità di innovare eccetera. Invece, come da esempio su watch:

>Con Gear, Samsung ha lavorato in modo eccellente per dimostrare di non essere Apple. Determinata ad arrivare per prima sul mercato, ha fatto l’ovvio: ha ridotto il telefono alle dimensioni dell’orologio, fotocamera compresa. Per loro era semplicemente una sfida tecnologica.

>Apple ha lavorato bene alla dimostrazione di non essere Samsung. Come aveva fatto con iPod, iPhone e iPad, ha studiato il settore e immaginato come reinventarlo, a partire dalla comprensione che un orologio è sia tecnologia che moda.

La morale:

>In realtà Apple è una delle aziende più coerenti sulla Terra. Quando il suo comportamento attuale risulta incomprensibile, può diventare chiaro se solo si guarda al passato.

Non è difficile, per chi vuole capire.