---
title: "Il settimo sigillo"
date: 2021-09-04T02:48:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [D&D, McSweeney, Wendy Molyneux, Dungeons & Dragons, Roll20, Ingmar Bergman] 
---
Ci siamo riuniti in sei, nella nostra [serata di Dungeons & Dragons in presenza](https://macintelligence.org/posts/Il-canto-delle-sirene.html) a seguito di diciotto mesi di sessioni remote.

Ai sei vaccini si è aggiunto la mattina dopo un tampone, eseguito (per tutt’altri motivi, solo coincidenza) da un partecipante.

Negativo. Nemmeno ci è rimasta l’incertezza.

Non è certo un campione statisticamente significativo. Come dicevo, però, ho provato nuovamente la libertà di scegliere. Ed è stata una conferma in solido di quello che insegnano i campioni statisticamente significativi. I rischi del proteggersi sono sovrastati dai vantaggi del farlo.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*