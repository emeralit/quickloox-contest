---
title: "Minimi comuni denominatori"
date: 2023-01-04T23:55:48+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [ChatGPT]
---
Ho le idee progressivamente più chiare su [ChatGPT](https://macintelligence.org/search/?query=chatgpt) e compagnia.

Per una persona sopra la media, può risolvere un sacco di grattacapo perditempo che richiedono il lavoro su qualcosa che già c’è. Si racconta di buoni risultati ottenuti da gente che aveva (invento) un plugin in C e doveva rifarlo in Python; invece la persona si è fatta un caffè e ha lasciato lavorare ChatGPT.

Per una persona nella media, né scarsa né capace, né carne né pesce, confondibile con altre e tranquillamente sostituibile, ChatGPT farà risparmiare lavoro quotidiano. Mettiamo che si tratti di pubblicare news di settore anonime e anodine, scrivere comunicati stampa con lo stampino, preparare relazioni che nessuno leggerà, riempire la sezione cronaca di un sito eccetera. Il sistema produce questo tipo di output piuttosto bene e lo fa, ancora una volta, mentre l’umano sta in panciolle dopo avere fornito l’input.

Per persone sotto la media, ChatGPT significa l’esclusione dalla comunità. Svolge meglio e in meno tempo qualsiasi tipo di lavoro sottopagato dove la qualità è irrilevante e nessuno fa caso a un errore. Per quelle persone abituate a vendere articoli divulgativi generalisti scritti in modo generico senza fantasia e senza creatività, come fossero copiati da Wikipedia, non c’è più spazio; si farà con ChatGPT, errori compresi.

L’algoritmo produce un eccellente minimo comune denominatore; un lavoro che, privo di qualunque qualità speciale e originalità, non dà fastidio a nessuno e non può suscitare alcun contraddittorio. L’acqua fresca.

Già sta partendo il treno della monetizzazione: corsi a pagamento per imparare a interrogare il motore, prestazioni professionali dichiaratamente realizzate con l’ausilio di ChatGPT (inspiegabilmente, vantandolo come qualcosa in più), startup che attaccheranno il sistema a frigoriferi e caricabatterie.

Presto un quantitativo notevole di persone produrrà senza ritegno contenuti che in realtà arrivano dal machine learning. Saranno tutti uguali o si somiglieranno tutti. Quello che oggi ha poco valore, lo perderà del tutto.

I computer si avviano a essere tramite di persone che scrivono, disegnano, suonano, fanno qualsiasi cosa al minimo comune denominatore, quello che farebbero tutti, cose prive di personalità e originalità, a ogni livello.

L’unico modo per salvarsi è, sarà, usare il computer per portarsi a livello di eccellenza nel proprio campo. Sarà impossibile, in un certo senso, fare *meglio* di ChatGPT. Se dobbiamo produrre una ricerca di prima media sul pipistrello, è antieconomico cercare di superare l’apprendimento meccanizzato sul reperimento delle informazioni.

Ma fare *oltre* ChatGPT, eh, si aprono praterie sconfinate. Un libro scritto dall’algoritmo sarà grammaticalmente ineccepibile. Ma pensare che potrà prendere il lettore allo stomaco a pagina quattro così come cullarlo a pagina otto, beh, è da tecnoillusi. Si apre un’epoca disastrosa per il lavoro di massa e, potenzialmente, un Eden per i veri creativi, quelli realmente capaci di agire diversamente dalla volta prima. Non so quanto sarà grande l’Eden ma, ancora una volta, i più talentuosi avranno le maggiori probabilità.

Attenzione, ora più che mai, a scegliere quello che scelgono tutti, stare nel gruppo, omologarsi, uniformarsi, dare per scontata la scelta sicura. In campo tecnologico, il rischio marginalizzazione non è mai stato così elevato. Per i minimi comuni denominatori non c’è più spazio.