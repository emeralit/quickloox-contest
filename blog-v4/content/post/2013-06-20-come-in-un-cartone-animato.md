---
title: "Come in un cartone animato"
date: 2013-06-20
comments: true
tags: [iOS, iPhone]
---
Rubo a piene mani da questo <a href="http://www.manton.org/2013/06/multiplane.html">intervento di Manton Reece</a> e c’è ben poco altro da dire.

>Nel 1940 Ub Iwerks, l’animatore dietro i primi cortometraggi di Topolino, ritornò negli studi Disney dopo dieci anni di assenza. […]<!--more-->

>Una delle sue invenzioni concepite durante la lontananza da Disney venne chiamata la ripresa multipiano. Perfezionata da altri fino a venire impiegata nella realizzazione di Biancaneve, permetteva con l’uso di un supporto per la telecamera alto più di tre metri di suddividere uno sfondo in livelli. Sul primo livello venivano dipinti a vetro magari gli alberi in primo piano, sotto di esso i personaggi, più indietro un edificio e altri livelli più indietro le colline, il cielo.

>Ottant’anni dopo l’invenzione di Ub, il multipiano è vivo in iOS.