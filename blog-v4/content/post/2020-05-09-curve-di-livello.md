---
title: "Curve di livello"
date: 2020-05-09
comments: true
tags: [Inkscape]
---
Forse è arrivato il momento di dare una nuova possibilità a [Inkscape](https://inkscape.org), che dopo tre anni di lavoro della comunità è finalmente uscito con la versione 1.0.

Per un programma open source si tratta spesso di una soglia decisiva, che separa i progetti meno efficaci e organizzati da quelli più preparati e con una buona visione del futuro.

Da fuori Inkscape pareva perso e la versione Mac è sovente una cartina di tornasole; fermo da anni alla 0.93, neanche più aveva una installazione Mac-like. L’interfaccia soffriva del peggior difetto per una app open, ovvero la poca attenzione per l’interfaccia utente, l’usabilità, il design dell’esperienza. Ad aprirlo, il programma assaliva l’occhio con una accozzaglia di icone non sempre comprensibili e mal disegnate.

Chi vorrà prendersi la briga di leggere le note di pubblicazione scoprirà che ci sono molte funzioni nuove e altrettante potenziate; chiunque, anche non facendolo, verrà accolto da una schermata di lavoro ancora sotto standard per estetica e utilità… ma si vede che è stato un lavoro ciclopico di razionalizzazione e semplificazione.

In passato l’ho usato per missioni speciali e, dopo pochi minuti, c’era il rischio emicrania. Questa versione, aperta per giocarci all’indomani dell’annuncio, lascia molta più serenità nello spirito.

L’open source è una forza salvifica nella nostra società e va difeso e sostenuto. Inkscape tuttavia non si poteva proprio guardare, potente e repellente al tempo stesso.

È ugualmente potente e assai meno repellente, oggi. Vale assolutamente la pena di esaminarlo. Torna su Mac un’alternativa in più per il disegno vettoriale e più valida di prima.
