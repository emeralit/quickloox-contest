---
title: "Difetto di semplificazione"
date: 2013-09-27
comments: true
tags: [iPhone]
---
Mancava una semplificazione del dibattito sul lettore di impronte digitali di iPhone 5S. È arrivata [grazie a Mario](https://multifinder.wordpress.com/2013/09/24/niente-di-piu-facile/). Altre considerazioni aggiungono solo confusione.<!--more-->

(I prossimi paragrafi, anzi, capoversi come ben mi insegna **Frix**, sono svolazzi che aggiungono niente a quanto appena detto).

Oltretutto altre considerazioni ingenerano nei già confusi l’idea che si debba trattare di una protezione inviolabile. Senza il sospetto che, così fosse, tutti la avrebbero già da vent’anni nella porta di casa, sull’auto, sulla carta di credito al posto di quella futile firma.

Invece deve essere una comodità in più. A dispetto dei milioni di esperti di impronte digitali, già commissari tecnici della Nazionale nonché analisti Apple, per i quali si dovrebbero inserire autenticazione a due fattori e altre tecniche esoteriche, fantastiche per entrare in un laboratorio di massima sicurezza. Quando uno vuole sbloccare il telefono, invece, vuole sbloccare il telefono, non recitare in *Mission Impossible*.

Se proprio si amano le complicazioni, si legga [Perché ho violato TouchID e continuo a pensare che sia sorprendente](https://blog.lookout.com/blog/2013/09/23/why-i-hacked-apples-touchid-and-still-think-it-is-awesome/).