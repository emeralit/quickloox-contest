---
title: "L’anno che verrà"
date: 2023-09-26T13:45:45+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [The Eclectic Light Company, Sonoma, macOS 14]
---
Mi farei pochi problemi a installare [macOS 14, alias Sonoma](https://www.apple.com/it/macos/sonoma/), che esce oggi, anche perché i goliardi di *The Eclectic Light Company* hanno già annunciato [la cronologia dei futuri aggiornamenti del sistema](https://eclecticlight.co/2023/09/26/welcome-to-the-year-of-sonoma/).

Insider? Cacciaballe? Clickbait? Veggenti? Gente che spara nel buio sperando di centrare comunque una preda?

Semplicemente, gente esperta di come vanno le cose a livello tecnico nello sviluppo dei sistemi operativi per Mac da quando esce una nuova edizione ogni anno. Più o meno i tempi sono sempre quelli, le esigenze e le soluzioni contenute nei vari aggiornamenti si somigliano e, dopo anni, è possibile guardare a una regolarità.

Questo post ci insegna a guardare agli aggiornamenti di sistema in modo diverso da un tradimento della fiducia (*lo sapevo che c’era qualcosa che non andava*) o da una diminuzione della qualità. Sono una necessità e un bene, perché la complessità del software è tale che ci sono sempre bug da sistemare ed è possibile migliorare funzioni, aggiungerne se arrivano in ritardo e così via.

L’aggiornamento di macOS non è un fulmine a ciel sereno o un imprevisto che mette in cattiva gli sviluppatori, quanto un fatto della vita informatica talmente consolidato che una persona competente può anche scriverne al futuro.

Ovviamente nella cronologia mancano riferimenti specifici. I perché degli aggiornamenti e le loro motivazioni invece ci sono, e la lettura è istruttiva per quanto sintetica.