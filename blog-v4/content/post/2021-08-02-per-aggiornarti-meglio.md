---
title: "Per aggiornarti meglio"
date: 2021-08-02T00:45:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [The Eclectic Light Company, Mac, Big Sur, macOS, Intel, M1, Apple Silicon, Universal Binary] 
---
C’è ancora qualcuno dotato di buona volontà. Nello specifico, su The Eclectic Light Company hanno iniziato a spiegare perché gli aggiornamenti di macOS siano così corposi e, nello specifico dello specifico, perché [quelli di Big Sur superano ogni record](https://eclecticlight.co/2021/07/28/why-are-big-sur-updates-so-large/).

Intanto, *tutti gli aggiornamenti di Mac sono universali*. Contengono tutte le varianti necessarie per tutti i modelli di Mac. Il principio è che ogni aggiornamento debba essere bastante per l’intera gamma di modelli da aggiornare.

A questo si aggiunge che *Apple pubblica aggiornamenti firmware solo dentro gli aggiornamenti di macOS*. Ogni update contiene tutti gli aggiornamenti firmware per ciascun modello supportato.

Naturalmente, in questo momento di transizione, *gli aggiornamenti sono Universal Binary*: il codice da eseguire è in doppia versione, per Intel e per M1. Per M1 la situazione è ancora peggiore perché ragioni tecniche impongono lo scaricamento di un volume consistente di dati all’inizio dell’aggiornamento, dati che per Intel non servono.

Sono solo alla metà delle ragioni fornite nell’articolo, ragioni che diventano progressivamente più tecniche e articolo che si chiude aprendo più domande di quelle che meritoriamente è riuscito a chiudere.

Tutto questo non è giustificazione ma spiegazione e aiuta a chiarire almeno in parte come mai ultimamente qualunque aggiornamento di macOS richieda giga su giga. C’è da augurarsi che Apple arrivi in fretta a sistemare i suoi meccanismi di aggiornamento per renderli più astuti ed efficienti, che mandino a ciascun Mac solo quello che effettivamente gli serve.

Il rischio è che, di fronte a routine di update che diventano sempre più onerose, l’utilizzatore medio inizi a vederle come il lupo cattivo e a starne alla larga, il che rende completamente inutile l’aggiornamento. Torniamo a come era una volta, quando un aggiornamento di Mac era non dico una festa, però si stava tranquilli e contenti.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               