---
title: "Se lo dice Mario"
date: 2014-02-05
comments: true
tags: [Unread]
---
Impossibile arrivare su tutto da soli, anche a questo servono gli amici. Così è [Mario](http://multifinder.wordpress.com) a segnalarmi [Unread](http://jaredsinclair.com/unread/), *per iPhone con iOS 7*:<!--more-->

>È una delle app più *belle* che abbia mai usato su qualunque OS.

Se lo dice lui, merita. Arrivo sulla pagina del programma e leggo la seguente introduzione:

>Sono probabilmente un pazzo a fare così, ma l’area occupata da questo paragrafo – dove la maggior parte dei siti di *app* inserisce una rassegna rotante di schermate, o un video, o un elenco di funzioni – è intenzionalmente vuoto. Unread vi sorprenderà e delizierà in molte maniere, ma non è importante. È invece importante che Unread vi aiuti a trovare un angolo di pace ogni giorno, dedicato a una lettura quieta e attenta. — Jared

Non ho mai sborsato i 2,69 euro susseguenti con maggiore sollecitudine.

È una delle *app* più belle che abbia mai usato su qualunque OS.

**Aggiornamento**: inutile scrivere una recensione breve perché ci ha già pensato [Riccardo](http://morrick.me/archives/6581) e ha detto esattamente ciò che serviva.