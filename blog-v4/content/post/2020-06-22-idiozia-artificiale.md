---
title: "Idiozia artificiale"
date: 2020-06-22
comments: true
tags: [WoW, Wired]
---
Dopo avere giocato per anni a World of Warcraft, me ne sono allontanato quando il gioco iniziava a trascurare i _casual player_ come me. Capaci magari di stare una notte sul gioco e vedere sorgere il sole su un sentiero battuto dagli elfi, meno inclini all’alta specializzazione del personaggio e a partecipare a raid fortemente organizzati.

Molto più di questo, comunque, è stato la perdita del gusto dell’esplorazione, della comunità, del senso delle distanze. Trovarsi in un punto di ritrovo per capire con chi scendere in un sotterraneo, spostarsi per un quarto d’ora vero fino a raggiungere un luogo, nuotare con pazienza in acque pericolose per esplorare un continente proibito. Procedure veloci, interfacce, teletrasporti hanno nel tempo aggiunto l’istantaneità al mondo virtuale. Che dunque ha smesso di essere un mondo, per diventare un parco a tema. Lo dico in modo assolutamente rispettoso e positivi nei confronti di tutti quelli che preferiscono così e vogliono quel tipo di fruizione, semplicemente diverso dal mio. In un’altra configurazione di vita e lavoro avrei potuto tranquillamente unirmi al gruppo dei supergiocatori.

Il tema della velocizzazione di WoW non è solo mio, tanto che a un certo punto la casa produttrice Blizzard [ha aperto WoW Classic](https://macintelligence.org/blog/2019/04/10/classica-tentazione/), una edizione del gioco (quasi) come era agli inizi, proprio per ricatturare un certo tipo di giocatori.

WoW Classic è purtroppo afflitto dalle [scorrerie dei bot](https://arstechnica.com/gaming/2020/06/bot-mafias-have-wreaked-havoc-in-world-of-warcraft-classic/). Giocatori senza scrupoli pilotano personaggi sintetici incaricati di razziare risorse per farci denaro (vero).

Blizzard ne ha falciati decine di migliaia, solo che ovviamente è un gioco tra guardie e ladri e per ogni strumento usato dalle guardie arriva un nuovo stratagemma dei ladri. Distinguere a colpo d’occhio un personaggio umano da un bot è difficile. Eliminare ingiustamente un personaggio vero, pagante, è chiaramente una cosa da evitare. Ci vorrebbe una bella intelligenza artificiale come si deve, ma l’impressione è che Blizzard non ci sia arrivata o che sarebbe troppo costoso.

Quando giocavo, certi miei amici perfezionisti mi facevano storcere il naso perché usavano i _mod_, moduli che si innestano nell’interfaccia per fornire comandi e informazioni non previste nell’originale. Radar a segnalare elementi del gioco che altrimenti si dovrebbero trovare, cruscotti più densi con più comandi a disposizione e così via. Facilitatori. Storcevo il naso anche di fronte alle macro, pur previste dal gioco.

Sono un evangelista dello scripting e dell’automazione personale. Tuttavia, ogni volta che aggiungi automazione a una comunità, togli umanità ed è quello che è accaduto a World of Warcraft, ora anche Classic. Ci sono sempre stati i razziatori di risorse, [poveri che passavano notti a raccogliere con il proprio personaggio](https://www.cracked.com/personal-experiences-2228-im-paid-to-play-mmorpgs-its-nightmare-5-realities.html) per poi rivendere e fare due soldi. Ma erano umani, per quanto commettessero un illecito.

Ora sono bot. E sono i discendenti agli steroidi dei mod, delle macro, dei facilitatori.

Un gioco per umani computerizzato può essere una fonte di coinvolgimento assoluto. Ma se lo rendi _computerizzabile_, lo spingi per una china. Diventa solo questione di tempo.

Giocare contro intelligenze artificiali, quando avverrà, sarà interessante. Per ora ci si ritrova tra i piedi un’idiozia artificiale, descrizione ineccepibile di un bot programmato per razziare una mappa di risorse, ed è solo frustrante.