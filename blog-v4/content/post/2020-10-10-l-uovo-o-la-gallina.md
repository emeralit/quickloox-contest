---
title: "L’uovo o la gallina"
date: 2020-10-10
comments: true
tags: [iNetHack2, Pathos, Nethack, roguelike]
---
[NetHack](https://www.nethack.org), a dirla tutta, andrebbe giocato su Mac per essere pienamente apprezzato.

Solo che, quando hai avuto in mano per abbastanza tempo un iPad Pro, non vuoi fermarti davanti a niente e nessuno. Tranne, a volte, l’incertezza.

Si potrebbe infatti cominciare da [Pathos](https://pathos.azurewebsites.net), in effetti un *remake* dell’originale, validissimo.

Oppure da [iNetHack2](https://pathos.azurewebsites.net), in tutto e per tutto l’esperienza dell’originale.

Il fatto è che, per cominciare, con Pathos si impara più in fretta. Ma sono certo che con l’andare del gioco, la sua interfaccia maggiormente verbosa (in senso visivo) diventerà pesante.

Con iNetHack2 invece, una volta sufficientemente addentro il gioco, si andrà come treni, sempre a livello di interfaccia. A patto di sopportare l’inizio, perché la curva di apprendimento iniziale è frustrante.

Si va per l’uovo oggi o la gallina domani? Beninteso, NetHack si può anche giocare [online](https://alt.org/nethack/hterm/) (su iPad previa disponibilità di tastiera esterna) e in altri modi numerosi oltre il bisogno di elencarli in questa sede. Ma ogni tanto una domanda oziosa di design ludico ci può stare.