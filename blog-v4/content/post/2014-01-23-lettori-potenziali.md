---
title: "Lettori potenziali"
date: 2014-01-23
comments: true
tags: [iPod]
---
*The Unofficial Apple Network* [ha rispolverato](http://www.tuaw.com/2014/01/21/an-incredible-2001-ipod-review-accurately-predicted-the-ipods-i/) un vecchio [articolo](http://reviews.cnet.com/4520-6450_7-5020659-2.html?tag=subdir) pubblicato su Cnet da Eliot Van Buskirk il 2 novembre 2001, pubblicato sotto la sezione *Mp3 Insider*, per dire dove eravamo.<!--more-->

L’articolo si intitolava *Come iPod cambierà l’informatica* e scriveva come gli scettici fossero in torto: *iPod è rivoluzionario in più di una maniera e i suoi discendenti sostituiranno il Pc*.

>Alcuni dettagli mi fanno chiedere se iPod non sia il capostipite di un nuovo tipo di apparecchi, senza correlazione con l’ascolto di Mp3. Ci sono caratteristiche di iPod che mi portano a predire come questo lettore sia un segno delle cose che verranno: può contenere file di ogni genere; iPod è il primo lettore Mp3 con sincronizzazione automatica verso il computer, prerogativa tra le più importanti di palmari e cellulari; […] iPod è un prototipo dei portadati che tutti avremo in tasca da qui a un decennio. Si sincronizzeranno con i computer e permetteranno di portarsi in giro raccolte di musica e video. […] I discendenti di iPhone comprenderanno un hard disk e il processore e il sistema operativo di un Pc da rete. […] I successori di iPod rimpiazzeranno il PC come centro della vita digitale.

\2001. Vengono i brividi. A pensare che per strada incontri tuttora persone scettiche su iPhone. Convinte, forse, che iPod fosse solo un lettore senza altre potenzialità.