---
title: "Una pagina dal futuro"
date: 2013-12-03
comments: true
tags: [Editorial, iPad, Python, Markdown]
---
Dice *nella mia classe i ragazzi adorano Android perché possono scrivere programmini e mandarseli per posta*.<!--more-->

Bello, ma un insegnante dovrebbe anche accompagnarli nel loro secolo, i ragazzi.

Un esempio: [Editorial](https://itunes.apple.com/it/app/editorial/id673907758?l=en&mt=8). Un programma per scrivere testo in [Markdown](http://daringfireball.net/projects/markdown/), niente di sconvolgente, che incorpora un interprete di linguaggio [Python](http://python.org) e questo già è più interessante.

Con Python nel cofano di Editorial si possono creare flussi di lavoro automatici che fanno cose da soli liberando tempo e moltiplicando le possibilità. Una sorta di [Automator](http://www.macosxautomation.com/automator/) su misura per il programma. Federico Viticci ne ha scritto su *MacStories* una [recensione](http://www.macstories.net/stories/editorial-for-ipad-review/) che è praticamente un [libro](https://itunes.apple.com/it/book/writing-on-ipad-text-automation/id697865620?l=en&mt=11).

Supponiamo di avere scritto un flusso di lavoro Editorial che prende un indirizzo Internet contenuto in un messaggio *Twitter* e lo inserisce automaticamente in documento.

Qualcuno [lo ha già fatto](http://editorial-app.appspot.com/workflow/5789678192033792/mSX8MIDCD9I).

Ci si collega alla pagina con un iPad dove sia installato Editorial. Si dà il consenso ed è fatta: il flusso di lavoro si installa nella *app*.

Ai ragazzi del 2013 si deve insegnare questo, non la posta elettronica.