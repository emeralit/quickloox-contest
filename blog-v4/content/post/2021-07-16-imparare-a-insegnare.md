---
title: "Imparare a insegnare"
date: 2021-07-16T01:26:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Invalsi] 
---
*Disclaimer*: i discorsi a livello nazionale non necessariamente rispecchiano situazioni locali. Chi scrive è molto soddisfatto della scuola della figlia (a parte la tendenza al valzer delle cattedre di cui non capisco la razionalità, umana, economica, logistica, didattica eccetera).

Lo stato della scuola italiana non è allegro. Secondo la [valutazione Invalsi](https://invalsi-areaprove.cineca.it/docs/2021/Rilevazioni_Nazionali/Rapporto/14_07_2021/Sintesi_Primi_Risultati_Prove_INVALSI_2021.pdf), il livello della scuola secondaria di primo grado è questo:

- il trentaquattro percento degli studenti ha risultati inadeguati in italiano;
- il quaranta percento degli studenti ha risultati inadeguati in matematica;
- il ventidue percento degli studenti ha risultati inadeguati nell’inglese letto;
- il quaranta percento degli studenti ha risultati inadeguati nell’inglese ascoltato.

Sconfortante, vero? Eppure sento già la soluzione: *colpa della DaD*.

Non so come dirlo: questi sono *i dati del 2019*.

Chi propugna il ritorno alla scuola in presenza perché ci vuole la scuola in presenza, in Italia, oggi, propugna un sistema malfunzionante per uno studente su tre.

Avversari acerrimi della didattica digitale? Ci sta. Convinti che [L’attimo fuggente](https://www.comingsoon.it/film/l-attimo-fuggente/39547/scheda/) sia la rappresentazione della quotidianità scolastica italiana? Benvenuti. Insegnanti magnetici che moltiplicano la capacità didattica per il carisma e per la fisicità? Averne.

Il mondo che volete e che rivolete è questo, malfunzionante una volta su tre.

Mi confronto molto volentieri con chiunque, ma prima chiedo una lista delle cose che la scuola ha fatto nel 2020 e nel 2021 per uscire dalla situazione angosciante del 2019. Perché nessuno vuole tornare a un sistema bacato, giusto? Per ogni studente mal preparato ci sono dietro famiglie, fatiche, un futuro più difficile, una risorsa minore per la collettività. Ogni studente che non siamo in grado di formare bene è un pezzo di Italia che si degrada e attualmente formiamo male centinaia di migliaia di studenti.

Quando si accusa la didattica digitale di dare cattivi risultati o risultati persino peggiori di questi, raffiguratevi il quadro: persone incapaci di insegnare bene messe a insegnare su un sistema che sono incapaci di usare. Che cosa può andare storto?

Vogliamo tornare in aula? Torniamoci. Ricordiamoci che – globalmente – lasciamo impreparati un terzo degli studenti. Il problema sarà il *digital divide* o la nostra capacità di insegnare? Le nostre aule miracolose sono l’approdo sperato o un banco di sabbia dove ci areniamo?

Diamo alla didattica digitale tutte le colpe del mondo. Però mi dite che cosa avete fatto in questi venti mesi per quegli studenti lì, quelli inadeguati, che lo sono diventati in presenza, con la lezione frontale, con il rapporto personale, con gli sguardi, le buone vibrazioni, con tutte quelle cose che durante la pandemia sono mancate e, sento, erano indispensabili. Indispensabili a che, se lasciano per strada un terzo dei ragazzi?

Mia modesta proposta: imparate ad applicare la didattica digitale fino a quando siete così bravi che funziona esattamente come quella analogica. Poi la distruggete con critiche inappellabili e insegnate come volete. Però all’Invalsi restano indietro in cinque su cento, non trentatré. Abbiamo un accordo?

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*