---

title: "E grazie per tutte le dissolvenze"
date: 2022-12-17T15:14:34+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [iMovie, DaVinci Resolve, Blackmagic]
---
Mi è ricapitato un piccolissimo progetto video: una trentina di secondi prodotti a passo uno, a partire da una sequenza fotografica.

Si tratta di tecnica di base e mi aspetto sia risolvibile comodamente e in tempo umano su un programma di base. Invece iTunes mi ha fatto penare.

Nella versione Ventura ho apprezzato la presenza dell’export a 1080p; meno il fatto che uscisse sistematicamente un errore di Compressor, che non mi faceva generare il video definitivo. Ho dovuto lavorare su una copia del progetto e riavviare iMovie prima di arrivarci.

L’impostazione della durata dei fotogrammi va cercata, se uno lo sa beninteso, nelle… Preferenze. Se qualche fotogramma deve fare eccezione, occorre duplicarlo, magari più volte. Titoli e dissolvenze hanno da sempre una interfaccia utente discutibile che, sommata al resto delle scomodità, si sopporta meno.

Basta. Il prossimo microprogetto video lo lavorerò dentro [DaVinci Resolve](https://www.blackmagicdesign.com/products/davinciresolve/), che sembra ben più complesso ma molto più al passo di iMovie. Vedremo se anche al passo uno.

I ragazzi di Blackmagic sono stati anche così solleciti da fare uscire la [versione per iPad](https://www.macrumors.com/2022/12/22/davinci-resolve-ipad-available-app-store/) appena dopo la mia decisione. Tanta considerazione non l’ho avuta nemmeno in Apple.

Ciao iMovie e grazie per tutto. È ora che tu ti prenda un sabbatico e cresca un po’, prima di tornare.