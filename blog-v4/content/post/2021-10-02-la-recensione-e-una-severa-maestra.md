---
title: "La recensione è una severa maestra"
date: 2021-10-02T15:37:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [iPad, iOS, Federico Viticci] 
---
Riga più riga meno, sono finalmente arrivato in fondo alla [recensione di iOS e iPadOS versione 15 scritta da Federico Viticci su MacStories](https://www.macstories.net/stories/ios-and-ipados-15-the-macstories-review/).

E ho compreso quanto mi manchi per usare a imparare iPad (o iPhone, per quello) a un livello veramente competitivo.

Le ventitré paginate di recensione (Ok, una di credits, una introduttiva…) trattano tutti gli aspetti rilevanti dell’aggiornamento, Focus, Promemoria, Comandi Rapidi, estensioni di Safari, widget, schermata home e quant’altro. Mi rendo conto che, pur lavorando metà del tempo con iPad, in pratica lo uso come o quasi come il suo primo fratello maggiore del 2010.

Questo è grave, perché significa lavorare come lavorano tutti; per fare un passo avanti è necessario riuscire a lavorare come non tutti. Viticci riesce a lavorare su iPad come nessun altro e, guarda caso, è un’autorità mondiale la cui conoscenza di questo argomento è sufficiente a garantire reddito per sé e per una piccola azienda, quella che ruota attorno a MacStories.

Certo serve il tempo per leggere ventitré pagine e poi per approfondire le cose più promettenti, che servono, che potrebbero servire.

Come mi sfotteva amichevolmente [kOoLiNuS](https://koolinus.net/blog/) giorni fa, poi rischi di passare una giornata per *non* automatizzare una cosa che ti avrebbe chiesto cinque minuti a mano, o automatizzarla così male da rendere controproducente l’automatismo. Eppure la vista a breve mostra – giustamente – la difficoltà a trovare il tempo necessario per fare quelle cose in modo più producente, mentre fatichiamo a guardare in là a distanza sufficiente per vedere il beneficio.

Alla fine il lavoro è sempre questione di risolvere un problema per conto di una comunità. Più la soluzione è geniale, più diventi bravo e più la tua comunità ne beneficia.

Sotto con i *widget* e grazie a Robert Heinlin per l’[ispirazione](https://it.wikipedia.org/wiki/La_Luna_è_una_severa_maestra).

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*