---
title: "Imprese"
date: 2015-05-01
comments: true
tags: [Expo, InternetArchive, GödelEscherBach]
---
Lo ignoravo: l’Internet Archive detiene, appunto, un [archivio di 2.603 giochi dell’èra Ms-Dos](https://archive.org/details/softwarelibrary_msdos_games). Il bello è che basta un clic per giocarci dentro il *browser*. Non sto neanche a segnalare perle, ce ne sono a decine e decine e provare è questione di un momento.<!--more-->

Hanno realizzato un [*ebook*](https://github.com/ccpaging/books/tree/master/GEB1983) presumo non ufficiale – e non so quanto legale – di [Gödel, Escher, Bach: Un’Eterna Ghirlanda Brillante](http://www.adelphi.it/libro/9788845907555), uno dei Libri Fondamentali. Ha qualche problema su iBooks per Mac, mentre su iPad si apre senza problemi. È su GitHub e dunque chi volesse potrebbe persino perfezionarlo o metterlo più a posto di quanto non sia ora.

La pagina delle [domande e risposte frequenti per Expo 2015](http://www.expo2015.org/it/informazioni-utili/faq/biglietti-ingresso) è lunga più di cinquantamila caratteri. In pratica un romanzo breve. Da essa si desume che i disabili non possono acquistare un biglietto online. Né possono farlo gli studenti. I bambini da zero a tre anni entrano gratis ma hanno bisogno di un biglietto e questo biglietto non è disponibile online. Questo è lo stato della biglietteria online poche ore fa.

 ![Biglietteria online Expo 2015](/images/expo.png  "Biglietteria online Expo 2015") 

Delle tre, qual è l’impresa più incredibile?