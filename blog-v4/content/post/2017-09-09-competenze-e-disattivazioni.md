---
title: "Competenze e disattivazioni"
date: 2017-09-09
comments: true
tags: [MailMaster, Bergamo, Scienza, Eventbrite]
---
È un momento così. Ha scritto anche **MailMaster C.** e gliene sono grato.

**Vedo che sul tuo blog è il momento degli screenshot**, quindi partecipo anche io! :)

Provo a registrarmi per una conferenza gestita dal festival della scienza a Bergamo e navigando il sito scopro che:

1. Una ricerca con il motore interno del sito del nome di uno dei relatori dà zero risultati. Devo arrivarci tramite Google.
2. Al momento di registrarmi compare la schermata in allegato. Siamo il 7 settembre e hanno già perso tutte le utenze fatte la settimana scorsa?
3. La registrazione chiede una montagna di dati personali obbligatori: cellulare, data di nascita, luogo di residenza, sesso, professione e nazionalità. Ma che se ne fanno di queste informazioni? Io devo solo partecipare ad una conferenza! Mi vogliono schedare prima per sapere se sono un potenziale terrorista?
4. Ma usare piattaforme già pensate per questo scopo, tipo [Eventbrite](https://www.eventbrite.com), troppo complicato?
5. E si tratta di un festival, mi pare di capire, di un certo rilievo. Se solo le prenotazioni sono gestite così, mi chiedo il resto…

 ![Disattivazione delle, ehm, vecchie utenze di Bergamo Scienza](/images/bergamo-scienza.png  "Disattivazione delle, ehm, vecchie utenze di Bergamo Scienza") 

Però non sembra essere ottimizzato per Internet Explorer! :)

Ho anche scritto chiedendo spiegazioni sulla necessità di avere tutti quei dati personali solo per prenotare un posto a sedere, ma ad oggi ancora nessuna risposta.
*Mah.*

La risposta all’ultima domanda credo di averla: gli sponsor pagano bene per una buona profilazione dei partecipanti, venduti a loro insaputa.

La risposta a tutte le altre domande non riesco a trovarla. Continuo a formulare ipotesi troppo oltraggiose per essere pubblicate. Apparentemente, la disattivazione degli utenti va di pari passo con quella delle competenze.