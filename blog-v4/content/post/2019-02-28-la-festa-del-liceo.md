---
title: "La festa del liceo"
date: 2019-02-28
comments: true
tags: [Apple, Mac, iPad]
---
Per chi ricordasse i tempi della festa di fine anno della scuola, con l’immancabile band di studenti che schitarrava su un palco improvvisato, i tempi sono un po’ cambiati.

Adesso, soprattutto se si studia a Huntington Beach, dalle parti di Los Angeles, l’idea è preparare uno show con cover e canzoni originali per un totale di trentasette pezzi, preparati e presentati dagli studenti che lavorano con qualità professionale a ogni dettaglio.

*Playlist* [è giunto alla settima edizione](https://www.apple.com/newsroom/2019/02/huntington-beach-educators-mold-musicians-of-tomorrow/).

Lascio immaginare che tipo di hardware e software venga usato.