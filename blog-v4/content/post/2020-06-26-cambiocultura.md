---
title: "Cambiocultura"
date: 2020-06-26
comments: true
tags: [Apple, Sparks]
---
Ha ragione Steven Sinofsky a dire che Apple ha effettuato un annuncio monumentale con la transizione ad Arm, o ha ragione David Sparks che, poche ore dopo il _keynote_, era – [parole sue ](https://www.macsparky.com/blog/2020/6/walking-and-chewing-gum)– in un podcast a lamentarsi della mancanza di un pulsante di condivisione in Mail?

Hanno ragione fino a un certo punto tutti e due, ma un certo punto vale la [chiosa di Jason Snell](https://sixcolors.com/link/2020/06/apple-microsoft-and-compatibility/):

> La cultura Apple è basata sul cambiamento e sull’andare avanti; e ha avuto la forza di rompere situazioni consolidate e fare impazzire la gente per starle dietro. Non significa che abbia sempre ragione né cambia la spiacevolezza di attendere decisioni strategiche senza sapere che succede là dentro. Ma questo è il cuore di Apple. È una larga parte di utilizzatori ha imparato ad apprezzarla o ad accettarla come parte della natura dell’usare prodotti Apple.

Non penso che lo avrei scritto molto meglio. Possiamo rimanere sorpresi, delusi o arrabbiarci perché Apple introduce un cambiamento; giusto o sbagliato che sia, però, dobbiamo avere il coraggio e l’apertura di aspettarcelo e magari essere preparati.