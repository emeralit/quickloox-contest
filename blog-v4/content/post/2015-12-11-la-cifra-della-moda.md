---
title: "La cifra della moda"
date: 2015-12-11
comments: true
tags: [Telegram, Android, iMessage, Linux, Mac, iOS, Apple]
---
Non ricordo bene come e perché, sono entrato in [Telegram](https://telegram.org), rete messaggistica che si vuole distinguere per la sicurezza e la non intercettabilità delle comunicazioni.<!--more-->

Sono poco attivo e nel mio elenco contatti è entrata una manciata di persone. Le quali, a mia conoscenza, usano Mac o iOS.

Esiste sicuramente la questione multipiattaforma. Capita di conversare con persone che usano un non-Mac. Molti hanno un Mac ma un computer da tasca Android, oppure un iPhone e un computer Linux, o altro.

Al tempo stesso, iMessage cifra le comunicazioni esattamente come fa Telegram e offre lo stesso livello di sicurezza. Se entrambi i conversatori sono in iMessage, Telegram è un’aggiunta gradevole, di gran moda e perfettamente inutile.

Tanto che Apple ha avuto in proposito pure qualche [seccatura con le autorità americane](http://betanews.com/2015/09/08/apple-cant-give-real-time-imessage-data-to-the-fbi-because-texts-are-encrypted/), che vorrebbero buttare un occhio sulle comunicazioni via iMessage, ma non possono: sono cifrati.