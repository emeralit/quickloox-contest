---
title: "Il bello di Android"
date: 2013-09-26
comments: true
tags: [Android]
---
Perché farsi schiavizzare da App Store per scaricare i programmi, quando Android è così tanto libero? Si possono scaricare programmi da ovunque, farseli passare dall’amico con la chiavetta, tutelare la propria privacy dagli sguardi concupiscenti di iTunes.<!--more-->

E, per esempio, si trova [la app che permette di usare Messaggi anche su Android](https://play.google.com/store/apps/details?id=com.huluwa.imessage). Visto? La funzione esclusiva di iOS e OS X possono averla tutti, basta essere furbi a sufficienza.

Furbi come l’autore della *app*, che per entrare nel protocollo di Messaggi traveste l’applicazione come se fosse quella legittima a bordo di un Mac mini.

<blockquote class="twitter-tweet"><p>So it looks like that iMessage on Android hack is super sketch and is spoofing iMessage requests as a mac mini: <a href="http://t.co/TYT6Djumdv">pic.twitter.com/TYT6Djumdv</a></p>&mdash; Adam Bell (@b3ll) <a href="https://twitter.com/b3ll/statuses/382369290243952640">September 24, 2013</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Come [fa notare](https://plus.google.com/u/0/116098411511850876544/posts/UkgaXa1oa6M) Saurik, il creatore di Cydia per gli apparecchi iOS sottoposti a *jailbreak*, i dati acquisiti dalla *app* se ne vanno verso un server cinese e da lì in poi chissà dove.

<blockquote class="twitter-tweet"><p>A 3rd-party iMessage app was just released on Android; I believe all data sent from/to Apple is resent to/from China for processing: beware.</p>&mdash; Jay Freeman (saurik) (@saurik) <a href="https://twitter.com/saurik/statuses/382387551144652800">September 24, 2013</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Mentre la *app* è in funzione, può scaricare sull’apparecchio software a propria discrezione.

<blockquote class="twitter-tweet"><p><a href="https://twitter.com/SteveStreza">@SteveStreza</a> it can download apks in the background: <a href="https://t.co/TXL9GyTeCv">https://t.co/TXL9GyTeCv</a> and the apk is obfuscated</p>&mdash; Adam Bell (@b3ll) <a href="https://twitter.com/b3ll/statuses/382369777638854656">September 24, 2013</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Sembra un disastro pronto ad accadere. Scarichi la *app* furbetta, questa scarica a sua volta qualcosa di molto brutto e il tuo *smartphone* diventa schiavo di una *botnet* o peggio.

Il bello di Android è che il proprietario dello *smartphone* infetto, visto ciò che accade usando Messaggi, darà la colpa ad Apple.
