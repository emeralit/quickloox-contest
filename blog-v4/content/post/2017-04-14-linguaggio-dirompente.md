---
title: "Linguaggio dirompente"
date: 2017-04-14
comments: true
tags: [Swift, Android, Ibm]
---
Ho scoperto tardi la notizia del [primo corso al mondo](http://www.swift.srl/corsi/corso-android-in-swift) di programmazione per Android in linguaggio… Swift. Corso creato da italiani, tra l’altro.<!--more-->

Per molti sviluppatori si tratta di un uovo di Colombo per risparmiare tempo e fatiche, dal momento che grazie a Swift sarà più semplice scrivere bene la stessa *app* due volte, una per iOS e una per Android. Ma voglio fare una riflessione appena più ampia.

Normalmente Apple sviluppa tecnologie e prodotti per una clientela anche molto ampia in termini assoluti, ma sempre ristretta nei confronti del mercato totale. Si sono venduti un miliardo di iPhone che è una cifra assurda, il prodotto più venduto della storia; eppure per ogni iPhone ci sono in giro cinque Android.

Quelle rare volte che la tecnologia o il prodotto è diretta al mercato intero, senza autolimitarsi, succedono cose grosse. L’esempio più eclatante è l’accoppiata iPod/iTunes, che ha conquistato il mondo appena ha superato i confini dell’area Mac.

Qui si è parlato di Swift [molte volte](http://www.macintelligence.org/blog/categories/swift/). Da linguaggio di programmazione Mac-only, è diventato *open source*, è stato adottato su Ibm, arriva su tanti iPad moderni (da iOS 10 in avanti).

Se Swift prendesse un buon abbrivio sulla programmazione Android, costituirebbe una novità straordinaria e dirompente, per quanto misconosciuta all’utenza.