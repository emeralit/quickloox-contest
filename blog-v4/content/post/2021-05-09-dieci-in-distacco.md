---
title: "Dieci in distacco"
date: 2021-05-09T01:13:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple Gazette, iOS] 
---
È stata una giornata molto intensa per una miriade di motivi, nessuno dei quali legato ai temi che è opportuno trattare qui.

L’unica cosa che serve ora è rilassarsi. Esamino i [dieci giochi minimalisti per iOS](https://www.applegazette.com/apps/best-minimalist-ios-games/) di *Apple Gazette* e se sembra un obiettivo semplicistico, è assolutamente vero. Spina staccata fino a domattina.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               