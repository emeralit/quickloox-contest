---
title: "Non cambia mai"
date: 2016-12-03
comments: true
tags: [iMac, Bondi, Aqua]
---
Comprerei volentieri [Aqua and Bondi - The Road to OS X & The Computer That Saved Apple](https://aquaandbondi.com) perché racconta un momento cruciale della storia di Apple, dal momento del salvataggio della barca grazie all’acquisto di NeXT al ritorno di Steve Jobs e, appunto, all’uscita del primo iMac.

Ho avuto un iMac *bondi blue* e ne sono rimasto soddisfattissimo. L’apice del suo utilizzo è stato trasportarlo diverse volte per una quarantina di chilometri in auto, per raggiungere una redazione dove nasceva una testata dedicata al business digitale. Su quell’iMac si impaginava la rivista, praticamente in tempo reale. Qualcosa di eminentemente professionale.

Sono peraltro ricordi personali. Si può parlare del primo iMac in un altro modo: un computer sottodimensionato, inferiore per specifiche e superiore per costi alle alternative Windows, praticamente impossibile da aggiornare ed espandere (l’ho smontato tre volte per un problema con la Ram e non lo augurerei al peggior nemico), con dettagli frivoli, un mouse che piaceva solo a me, una tastiera che nemmeno a me e privo dell’indispensabile lettore di floppy disk. Come si sarebbe potuto fare, se improvvisamente si fosse palesato un collega o un cliente con un floppy da inserire?

Come cambiano le cose, negli anni.