---
title: "Biglietti e bigliettoni"
date: 2013-11-24
comments: true
tags: [Apple, Jobs, Dropbox, Wwdc]
---
[Dropbox](https://www.dropbox.com) è alla ribalta essendo [alla ricerca di 250 milioni di dollari](http://dealbook.nytimes.com/2013/11/18/dropbox-an-online-storage-start-up-seeks-8-billion-valuation/?_r=0) per finanziare lo sviluppo del *business*.<!--more-->

Ha anche ottenuto una certa esposizione mediatica. Il suo fondatore Drew Houston ha recentemente partecipato a un evento di rilievo nel mondo professionale chiamato Dreamforce. L’articolo di *IT Business* in merito si intitola [Steve Jobs fece voto di ‘uccidere’ Dropbox con iCloud](http://www.itbusiness.ca/blog/steve-jobs-vowed-to-kill-dropbox-with-icloud/44881). Un bel biglietto da visita, eroi sopravvissuti alla furia di Steve Jobs.

Secondo Houston, racconta l’articolista, Jobs avrebbe proposto di acquisire Dropbox e, al rifiuto, annunciato la guerra. Abbiamo solo la sua testimonianza ed è possibile che le cose siano andate in questo modo. L’articolo prosegue:

>Sei mesi dopo quell’incontro, Houston stava guardando un discorso pubblico di Jobs e lo vide mantenere la promessa. “Ci stava chiamando per nome e dicendo che ci avrebbe ucciso con iCloud”.

Purtroppo esiste solo una occasione possibile: l’ultimo *keynote* di Steve Jobs in apertura del convegno mondiale degli sviluppatori (Wwdc) a giugno 2011, visibile per intero [su YouTube](https://www.youtube.com/watch?v=gfj7UgCMsqs), nel quale annunciò appunto iCloud.

Posso sbagliare, ma in quelle due ore Jobs non nomina Dropbox né minaccia di volerla uccidere.

Bugiardi per aumentare le *chance* di trovare soldi, è un brutto biglietto da visita.

<iframe width="560" height="315" src="//www.youtube.com/embed/gfj7UgCMsqs" frameborder="0" allowfullscreen></iframe>