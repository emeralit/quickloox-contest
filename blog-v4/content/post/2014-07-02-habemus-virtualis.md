---
title: "Habemus virtualis"
date: 2014-07-03
comments: true
tags: [Yosemite, OSX, Tomlinson, VirtualBox, MacBookPro]
---
Dopo una certa fatica sono riuscito a produrre una macchina virtuale [Yosemite](https://www.apple.com/it/osx/preview/) dentro [VirtualBox](https://www.virtualbox.org).<!--more-->

Le migliori istruzioni che ho trovato a disposizione su web sono [quelle di Jacob Tomlinson](http://www.jacobtomlinson.co.uk/2014/06/07/how-to-install-os-x-yosemite-developer-preview-in-virtualbox/) cui ho niente da aggiungere, tranne che il procedimento sembra estremamente fragile e assetato di Ram. Più volte tutto si è interrotto a metà strada senza ragione chiara e sono arrivato in fondo solo dopo avere generato nuovamente l’immagine truccata per l’installazione del sistema. Tutto in regola però.

Sul mio MacBook Pro posso dare quattro gigabyte di Ram alla macchina virtuale e lasciarne quattro al sistema; così è tutto estremamente lento (l’installazione di OS X 10.10 dentro VirtualBox ha preso una dozzina di ore). Consigliatissimo se disponibile un Mac da sedici gigabyte o superiore, dove è possibile largheggiare.

Se ci sono domande o curiosità, rispondo più che volentieri. Al momento ho ultimato anche il primo aggiornamento alla Developer Preview e tutto fila liscio per quanto può su una macchina virtuale lentuccia.