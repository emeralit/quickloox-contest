---
title: "Nasce, rinasce"
date: 2014-01-24
comments: true
tags: [Mac, Aaa, AllAboutApple]
---
C’è quel trentesimo anniversario che qualcuno ha già strillato ieri, l’altroieri, anche una settimana fa, a comprovare la propria ineleganza. Fa niente, siamo abituati.<!--more-->

C’è un altro anniversario, anzi, evento: [Dopo due anni riapre in darsena a Savona il più grande museo Apple del mondo](http://www.ivg.it/2014/01/finalmente-la-nuova-sede-riapre-savona-il-piu-grande-museo-apple-del-mondo/).

[All About Apple](http://www.allaboutapple.com) ha vissuto due anni travagliati e itineranti. Ce l’ha fatta anche stavolta.

Mi è materialmente impossibile presenziare all’inaugurazione, alle 11:30 in piazza De André 12, ovviamente a Savona. Saluto e mi complimento in contumacia con Alessio, con Alberto, con le persone meravigliose e incredibili che proseguono un sogno iniziato dodici anni or sono.

È il museo Apple più grande del mondo, ora con una sede affascinante, amministrato da amici straordinari. Chi può sia presente e porti anche le mie congratulazioni, se me lo merito. Grazie.