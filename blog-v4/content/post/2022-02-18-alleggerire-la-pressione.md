---
title: "Alleggerire la pressione"
date: 2022-02-18T00:52:33+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Shortcuts, Dr. Drang, Comandi rapidi, Note, CSV, BBEdit]
---
La cosa più interessante che succede in questo periodo nell’ecosistema Apple è il progresso che si fa nello scripting con i Comandi rapidi o Shortcuts che dir si voglia.

Perché Automator funziona più o meno con lo stesso principio, ma non ha la stessa portata dei Comandi rapidi, non è multipiattaforma e poi non riceve l’attenzione dei più bravi.

Che sono quelli capaci di fare le cose più semplici, quelle che semplificano la vita e rendono il senso preciso di che cosa abbiamo la possibilità di fare e saremmo pazzi a sprecare.

Qui è Dr. Drang alle prese con un compito piccolo piccolo: [segnare in una tabellina il giorno corrente e la misurazione della pressione arteriosa](https://leancrew.com/all-this/2022/02/a-simple-shortcut-for-daily-data-entry/).

È cosa da poco. Si può aprire Note con un gesto, da iPhone o iPad moderni. Si crea una tabella, basta toccare un pulsante. Trenta secondi per inserire data e dati, fatto, a domani.

Eppure si può fare più semplice, più comodo, *ancora* più semplice. La data, per dire, si può avere automaticamente e resta solo da digitare la misura della pressione. Il Comando rapido apposito è lungo tre mattoncini di comprensione elementare.

Però Drang vuole ricavarne dei grafici, che a lui viene comodo produrre a partire da un file CSV. Ricavare un CSV in [BBEdit](https://www.barebones.com/products/bbedit/index.html) a partire da Note è una cosa facilissima, solo che lui non vuole doverlo fare ogni volta.

E se si aggiornasse automaticamente il file CSV, senza passare da Note… ?

I mattoni per questo Comando rapido sono addirittura sei, di totale banalità. Il post li elenca uno per uno, è sufficiente copiare e rifare.

Il risultato è un Comando rapido che si richiama con un tocco dalla schermata di iPhone, dove sui digita la misura della pressione. Il file si aggiorna da solo.

Niente Note, niente tabella, la data è solo un ricordo. Se prima ci volevano trenta secondi, adesso sono dieci.

Il principio inoltre si può applicare a zilioni di situazioni in cui è necessario inserire dati in un file. I mattoni di Comandi rapidi sono facilmente personalizzabili.

A saperla vedere, la piccola soluzione di Dr. Drang potrebbe fare risparmiare globalmente milioni di ore di *data entry* su iPhone e iPad di tutto il mondo.

Lasciare che il computer lavori per noi e liberare il nostro tempo. Se c’è qualcosa che il computing personale non fa mai abbastanza né troppo in fretta, è questo. Ma dipende anche da noi.
