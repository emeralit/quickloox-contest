---
title: "Dare lustro al buonsenso"
date: 2019-08-25
comments: true
tags: [U2, Uproxx]
---
*Passati cinque anni*, scrive *Uproxx*, *è tempo di ammettere di [avere esagerato nelle nostre reazioni](https://uproxx.com/indie/u2-songs-of-innocence-apple-iphone-free-album-anniversary/) verso Apple che aveva regalato a tutti i nostri telefoni un album degli U2*.

A me pareva di avere scritto [più o meno la stessa cosa](https://macintelligence.org/posts/2014-09-23-il-trionfo-della-volonta/) un lustro fa. Mica per sembrare intelligente a scoppio ritardato; solo per dire che bastava il buonsenso.
