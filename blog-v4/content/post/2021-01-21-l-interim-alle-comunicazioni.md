---
title: "L’interim alle comunicazioni"
date: 2021-01-21T01:01:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Coleslaw, Muut, Disqus, isso, Lisp, GitHub] 
---
Mi scuso per non avere ancora i commenti in ordine. Non è una cosa facile per le mie capacità, ma ci tengo e infatti tutto il resto, che sarebbe più facile, è dietro nella lista delle priorità.

[Coleslaw](https://github.com/coleslaw-org/coleslaw) contiene di suo gli agganci per usare [Disqus](https://disqus.com) oppure [isso](https://posativ.org/isso/). Il primo, da utente, lo detesto e non voglio infliggerlo ad altri, anche se mi risolverebbe tutti i problemi con un nome utente e una password. Oltretutto non mi piacciono [cose che leggo in giro](https://fatfrogmedia.com/delete-disqus-comments-wordpress/) a suo riguardo. Il secondo dovrei installarlo da qualche parte per poi innestarlo sul blog. Nessun problema tecnico sull’installazione, che è banale persino per me; però poi divento un portatore sano di dati altrui e non è il mio mestiere.

Invece sto studiando per portare [Muut](https://muut.com), il motore della precedente incarnazione del blog. Per due ragioni: lo trovo gradevole da usare e, secondo, potrei portarmi dietro tutti i commenti precedenti una volta inseriti nel nuovo blog tutti i vecchi post. Sarebbe una cosa bella.

Solo che Coleslaw non prevede in partenza un plugin per Muut. Lo devo sviluppare.

È un problema risolvibile. Il plugin tipico è composto da una manciata di righe. Posso guardare a come sono fatti i plugin di Disqus e isso, per poi scrivere qualcosa di simile, che tratti Muut.

Prima però mi è necessario capire l’infrastruttura del blog dietro all’utilizzo dei plugin. Ci sto arrivando; il tempo è a tratti poco e la materia è a tratti densa. Una volta capita la teoria, aggiungo alla mia installazione locale un paio di file Lisp ben formati e da lì si dovrebbe funzionare come prima. Vorrei anche fare le cose per bene e poi presentare su GitHub una soluzione che tutti possano usare, ma questo arriva dopo. Insomma, sono in cammino e le cose procedono, solo lentamente.

Nel frattempo.

Non penso che tutti fremano ansiosi di scrivermi. Tuttavia è bello e utile scambiarsi opinioni. E poi capita che sia io a fremere ansioso di poter leggere qualcosa. Così, ecco che cosa si può fare durante l’interim.

Il [canale Slack](https://app.slack.com/client/T03TMRQRC/C03TMRQU0) è un posto accogliente, con traffico più che ragionevole, persone tranquille. Chi mi manda un indirizzo email verrà invitato senza problemi.

Per mandarmi un indirizzo email, si può usare l’email. lux at mac punto com, lvcivs at gmail punto com. Anche direttamente per scrivere e basta, senza inviti e canali alternativi.

Un metodo più veloce ancora è ricorrere alla messaggistica. Su Messaggi, Telegram e Signal esisto come Lucio Bragagnolo; se bisogna cercarmi per email, gli indirizzi sono sempre uno dei due sopra. Sorry, WhatsApp zero, ne ho già fin troppo. Accetto volentieri Google Chat ma mi serve un avviso, perché normalmente è spento. Previo avviso, a malincuore ma cosa non si fa per un contatto umano, posso accendere anche Skype o Teams e pensare a come è cattivo il mondo, ma esserci ugualmente.

I sistemi di messaggistica permettono di creare gruppi ma non sono interessato; c’è già quello Slack che è perfetto.

Sui più comuni social sono presente, solo che non mi sembrano il massimo per parlarsi in maniera occasionale. Niente in contrario, comunque.

Grazie per la pazienza e per la gentilezza che ho visto in tutti quelli che mi hanno contattato in questi giorni di transizione.

P.S.: comunicazione di servizio che non c’entra niente ma riguarda comunque la comunicazione in un certo qual modo. Come scritto in passato, recluto sempre persone disposte a giocare a Battle for Polytopia, giochino di strategia a turni molto carino, privo di pubblicità, semplice ma tutt’altro che banale, gratuito da giocare individualmente e che, se convinti, con 1,09 euro si apre al gioco multiplayer. Siamo un gruppo di una manciata di persone, teniamo in vita una manciata di partite, ogni tanto ne finiamo una, ne lanciamo un’altra, turni da ventiquattr’ore, nessun obbligo, tutto molto tranquillo. (Se qualcuno vuole invitarmi in un gruppo proprio, il mio alias è *lvcivs*).

**marcomassa** e **elring86** si sono aggiunti al gruppo ma non ho ancora chiesto loro se vogliono entrare in un gruppo Telegram chiamato *Battlers for Polytopia*, dove sostanzialmente facciamo le congratulazioni a chi vince, ci accordiamo su chi lancia una nuova partita e pochissimo altro. Il traffico è veramente basso e il disturbo minimo. Su richiesta, li invito.