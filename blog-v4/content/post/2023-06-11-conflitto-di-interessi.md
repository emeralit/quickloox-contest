---
title: "Conflitto di interessi"
date: 2023-06-11T02:06:48+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Editorial, Dropbox, Zorn, Ole Zorn]
---
Pomeriggio di editing intenso su un file di grandi dimensioni, condotto via Editorial su Dropbox in una situazione di scarsa connessione.

Risultato: sette copie del file in conflitto tra loro. Decisamente la cosa va contro i miei interessi di scrittura. Dropbox verrà dismesso, ma non subito. [Editorial](https://macintelligence.org/posts/2013-08-18-utile-e-inutile/) è un gioiello senza tempo grazie al suo scripting incorporato, solo che da sempre non sa gestire una sincronizzazione mancata. Ole Zorn non si rimetterà di sicuro a sistemare la questione, dopo anni che [ha annunciato la fine dei lavori sulla app](https://macintelligence.org/posts/2019-08-14-pensieri-chiaramente-oziosi/).

Sarebbe bello avere un editor ugualmente funzionale nell’inserire formattazione semplice HTML, magari proprio grazie allo scripting, che costi quello che deve costare e che non sia in abbonamento. Andrò in esplorazione.