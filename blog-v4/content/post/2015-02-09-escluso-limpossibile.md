---
title: "Escluso l’impossibile"
date: 2015-02-11
comments: true
tags: [AppleTV, Fleishman, Macworld, Holmes]
---
>Il 19 gennaio ho ricevuto una mail da iTunes Store contenente un elenco di brani musicali che avevo acquistato di recente. L’unico problema è che non le avevo acquistate.<!--more-->

Comincia così un [articolo di Glenn Fleishman su Macworld](http://www.macworld.com/article/2881456/the-mysterious-case-of-the-unauthorized-itunes-store-purchases.html), che la mette subito sulla paranoia, la sicurezza, l’autenticazione a due fattori, il supporto Apple. E chi non lo farebbe, con il sospetto che qualcuno stia piratando la ID Apple che avrebbe dovuto restare segreta e confidenziale?

La soluzione dell’enigma è curiosa e quasi incredibile: per pilotare un sintetizzatore TV, Fleishman usa un vecchio telecomando Apple che per ragioni misteriose – ma forse c’è stato di mezzo un *blackout* – si è apparentato a sua insaputa con la Apple TV di casa.

Così, mentre pilotava il sintonizzatore, a sua insaputa navigava anche su Apple TV, comprando musica senza accorgersene.

Parafrasando Sherlock Holmes, quando l’impossibile è stato scartato quello che rimane, per quanto improbabile, deve essere la soluzione.

C’è da pensare che cosa sarebbe potuto succedere se, invece che a un giornalista informatico navigato, fosse capitato a una persona comune incapace di determinare la causa dell’incidente ma pronta a sfogarsi sul forum di qualche sito spazzatura.