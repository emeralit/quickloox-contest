---
title: "Il coraggio di cambiare"
date: 2014-09-14
comments: true
tags: [Apple, Google, password, Mac, iOS, privacy]
---
Mi ha punto vaghezza di aggiornare le mie password principali di Google e Apple, dopo avere trovato certi miei indirizzi in alcuni database circolati di recente in rete.<!--more-->

Non c’era un vero pericolo di sicurezza; nel caso di Google, sono registrato con quell’indirizzo di posta Gmail a centinaia di siti e molto probabilmente è uno di quelli che ha ceduto a un attacco. Altrettanto probabilmente era un sito di cui mi importa niente e di conseguenza “protetto” con una password cretinissima.

Comunque sia ne ho approfittato per abilitare l’autenticazione a due fattori anche sull’account Apple.

Google batte Apple due a zero in efficienza e brillantezza del sistema.

Prima di tutto, Google pratica l’autenticazione a due fattori attraverso una [app](https://itunes.apple.com/it/app/google-authenticator/id388497605?mt=8), sistema molto più elegante che inviare un Sms. (Anche Google ha il mio numero di cellulare e in generale la cosa non è in questione; la *privacy* dipende da ben altro).

Secondo, tutti i programmi e i sistemi su Mac e su iOS hanno iniziato tutti insieme a chiedermi di inserire la nuova *password* Apple.

Mentre per Google non è accaduto; gran parte delle situazioni è governata da password specifiche, che Google eroga una volta per sempre e non sono la password principale. Anche cambiando la password principale, quasi tutto ha continuato a funzionare.

Un buon sistema mette ragionevolmente al sicuro senza complicare inutilmente la vita e su questi aspetti Google è più avanti.