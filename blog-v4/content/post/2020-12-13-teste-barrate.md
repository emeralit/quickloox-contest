---
title: "Teste barrate"
date: 2020-12-13
comments: true
tags: [Word]
---
Ora di scrivere un libro sui danni provocati dal software che offre una finta libertà e, con la scusa del professionale, consente alle teste di offuscarsi nonché all’analfabetismo funzionale di prosperare insolente.

Ho appena finito di parlare [di Excel](https://macintelligence.org/blog/2020/12/11/tandard-e-sformati/), volontariamente incapace di dare compatibilità su una qualsiasi tabella di testo; e si capisce che la questione dell’incompatibilità delle macro o di funzioni esoteriche è un problema falso, falsissimo; il pozzo è avvelenato fin dalla superficie.

Poi c’era WordPress, che per tenersi la quota di mercato e fare contenti tutti, soprattutto chi ha da fare soldi a spese dei meno senzienti, consente la circolazione di [plugin a livello di qualità sottoterra](https://macintelligence.org/blog/2020/12/06/il-dado-e-trattino/). Finisce che consulenti senza scrupoli li comprano con i soldi degli ignoranti in buona fede, spennati dai contratti di manutenzione, aggiornamento, bug fix di cui ci si è appena assicurati una fornitura perenne. Intanto la gente si abitua a software marrone e puzzolente come se fosse normale, premessa per dargli in futuro roba ancora peggiore, tanto si fa l’abitudine a tutto.

L’ultima in ordine di tempo è la conferma che le menti semplici finiscono dentro la spirale di degrado dell’esperienza, volteggiano fino in basso, pensano di essere raffinati perché usano funzioni sofisticate.

La scena. File Word in uno di quei meravigliosi passaggi interaziendali dove ciascuno infierisce sul documento senza ritegno perché tanto è attivo il tracciamento delle modifiche e alla fine il documento è perfetto, a patto di non guardare le modifiche fatte, pena diventare istantaneamente daltonici.

Entra il personaggio analfabeta funzionale. Lui deve cancellare del testo e a che cosa lo ha abituato il fantastico software di videoscrittura con cui trastullarsi in qualsiasi cosa diversa dallo scrivere? A cancellare e vedere il testo barrarsi. Perché è attivo il tracciamento, ovvio.

Che cosa fa lui per distinguersi? Decide di andare oltre il software, compiere la ribellione suprema in nome della specie che si illude di rappresentare come se fosse ancora al nostro livello di coscienza.

Per cancellare il testo, lo seleziona e *lo formatta come barrato*. Così è uguale a quello che gli fa vedere Word a modifiche aperte, no?

Certo, c’è anche anche chi Word lo sa usare. (Fammi vedere un documento con gli stili impostati. Un capolettera. Un font diverso da Arial. Fammi vedere qualcosa che non potrei fare su TextEdit o su WordPad, se esiste ancora).

Ma non è questo il punto. Non è più il punto. È software che incoraggia la dispersione del pensiero, spalanca le porte all’incuria e alla sciatteria, abbraccia la mediocrità come base del proprio successo. Non è che uno barri per cancellare perché usa Word. Ma tutti quelli che lo fanno, cento percento, sono su Word. E a questo punto, bisogna fare qualcosa con Word, non con loro. Loro sono le vittime.