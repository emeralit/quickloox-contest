---
title: "Controllo di realtà"
date: 2022-10-31T01:24:39+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [scuola]
---
Sono tenuto alla confidenzialità. Posso almeno dire che un amico lavorerà nei prossimi mesi con un istituto scolastico di buone dimensioni per popolare da zero il laboratorio di informatica prossimamente pronto nella struttura.

La cosa notevole è che si tratta di una scuola che si pone il dubbio e si mette in ascolto: che cosa andrebbe inserito per contrastare l’obsolescenza, svolgere un servizio effettivo per i ragazzi, essere rilevante, favorire gli insegnanti, avere costi sostenibili…?

Il mio amico non sta nella pelle. Sarà un controllo di realtà formidabile rispetto a quello che oggi si può fare *realisticamente* nella scuola e non solo in teoria, sempre bella e importante, che però va anche concretizzata prima o poi.

Il fatto che la lavagna sia vuota è una circostanza irripetibile, fortunata e sfidante.

Riporterò al mio amico qualsiasi commento utile e interessante.