---
title: "Con prezzo del pericolo"
date: 2022-02-07T00:47:35+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
I programmatori indipendenti di [Hello Games](http://hellogames.org) hanno conosciuto un certo successo grazie al loro gioco [No Man’s Sky](https://www.nomanssky.com). Il quale ha venduto più che abbastanza da indurli a trascurare la loro prima creazione in assoluto, *Joe Danger*, che pure aveva venduto un milione di copie.

Tra quel milione c’è anche il padre di Jack, un bambino di otto anni, legatissimo a <em>Joe Danger</em> come può esserlo un autistico che, accade molto spesso, usi un videogioco per mediare il suo difficile rapporto con la realtà e con le altre persone.

Nel momento in cui la mancanza di aggiornamenti del gioco gli ha impedito di funzionare, il padre di Jack ha scritto agli sviluppatori.

Letta la lettera, loro [hanno lavorato per riportare in vita Joe Danger](https://threadreaderapp.com/thread/1486669718127878149.html), *una lenta ricostruzione pezzo per pezzo attraverso otto anni di cambiamenti tecnologici*.

Il milione di persone che lo aveva scaricato potrà semplicemente aggiornarlo, a costo zero. Compreso il padre di Jack.

Per tutti gli altri, sono [tre euro per l’Action Pack con dentro le due versioni esistenti del gioco, o quattro euro per un gioco singolo](https://apps.apple.com/it/app-bundle/joe-danger-action-pack/id917636938).
