---
title: "Sempre ragazzo"
date: 2023-07-20T17:09:41+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Mitnick, Kevin Mitnick]
---
Modifico le trasmissioni (niente si interrompe, mai) perché Kevin Mitnick [ha trovato l’Accesso Finale](https://www.bbc.com/news/world-us-canada-66263235) ed è stato una persona con cui ho parlato e condiviso degli spazi, due volte.

La prima volta era giovane, fresco di grandi imprese e di accanimento giudiziario nei suoi confronti. Pensava svelto e aveva il dono penso innato di governare le piccole conversazioni: anche in un ambito convegnistico di domande e risposte da pubblico neutro e abbondantemente smaliziato, portava comunque il dialogo dove gli interessava, o forse semplicemente lo divertiva portarlo.

Non si faticava a immaginarlo nella camera di un alberghetto appositamente fetido a compulsare elenchi telefonici alla ricerca dell’aggancio giusto per avvicinarsi a un bersaglio. Erano veramente tempi mitologici, in cui elenchi di password venivano stampati e buttati nel bidone fuori dalla sede o avevi tempo e modo di familiarizzare con centralinisti, receptionist, operatori telefonici per acquisire un’informazione non dovuta in più o ottenere un favore.

Le esperienze *alla Mitnick* che avevo compiuto (stanno sulle dita di una mano), per quanto superficiali rispetto a quello che aveva combinato lui, permettevano di provare genuina ammirazione per chi, certo, era semplicemente anni avanti a chiunque nel capire i punti deboli dei sistemi informativi di allora e semplicemente ne approfittava. Ma oltre al talento c’erano pazienza, gestione del rischio, metodo, sangue freddo. Molto mi era sembrato innato, uno nato e cresciuto così. I risultati però sono risultati.

Ho capacità di ingegneria sociale con cui fatico a entrare in una pizzeria dove ho prenotato, ma un pochino le persone riesco a leggerle anch’io. L’impressione era che amasse il riconoscimento, godesse a essere celebrato, ma sotto sotto avrebbe preferito trovarsi altrove, possibilmente da solo. Gli piaceva essere ammirato, ma non stare sotto i riflettori.

La seconda volta è stato più di recente. Da ribelle e vittima perseguitata ingiustamente, era passato a pagato e pasciuto consulente per la sicurezza, con staff e ingaggi più che lauti (meritatissimi). Da imprevisto per i sistemi era diventato ingranaggio del Sistema.

Non è un giudizio, intendiamoci. Il sistema americano, se hai qualcosa da vendere, ti incoraggia a farlo e ti permette di estrarne tutto il valore concepibile. Per queste stesse ragioni tende a disumanizzare la vendita, proprio perché si punta al centouno percento. Se rosicchi tutta la carne, si vede l’osso. Poi, se hai fatto l’esperienza della prigione, forse ancora più quella di un processo ingiusto e di una condanna politica, non è detto che la si voglia ripetere.

Anche l’età ha il suo peso. Non aveva perso smalto, ma era più tranquillo, magari più allineato sul messaggio aziendale. Certamente riteneva di avere già dato, rispetto al vivere al limite, rischiare grosso, violare istituzioni delicate. Aggiungiamo pure che l’ingegneria sociale è più viva che mai, ma i codici di attacco sono cambiati e di brutto. Non si arriva più ai documenti scottanti di un’azienda con il presentarsi alla *reception* e fare un numero di alta scuola con la centralinista.

Il nuovo Mitnick era più integrato, più inevitabile, più appagato forse e sempre dava quella sensazione di non voler essere lì, che stavolta toccasse al pragmatismo e alla ragion di azienda prevalere, ma che il suo interesse reale fosse altrove.

Ora che è Altrove, possiamo dire che ha fatto la storia, ha portato insegnamenti tuttora preziosi per quanto possano essere cambiati i tempi, non è diventato supereroe né martire né ha voluto diventare guru.

Penso che il suo altrove fosse in un nocciolo di cuore un tantino ingenuo e puro, che non aveva considerato *a priori* i rischi giudiziari delle sue imprese e successivamente ha lasciato senza dannarsi troppo l’anima che il mercato ne facesse una figura commerciale. Completare la ribellione, rifiutare l’integrazione andavano oltre, non intendeva portare le cose alle estreme conseguenze, in nessuno dei due casi.

Kevin Mitnick, se lo si chiede a me, ha attraversato il mondo come un ragazzo, nonostante il mondo abbia fatto di tutto per farne un uomo, non importa se galeotto o imprenditore. Un ragazzo, fino all’ultimo giorno.