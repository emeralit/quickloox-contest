---
title: "Persino Ibm"
date: 2015-05-29
comments: true
tags: [Mac, Ibm, MacBookPro, MacBookAir]
---
Leggo che in Ibm operano al momento circa [quindicimila Mac](http://9to5mac.com/2015/05/28/apple-ibm-macs-pc/) usati dai dipendenti. E che entro la fine dell’anno si prevede l’aggiunta di altre cinquantamila unità.<!--more-->

Un memo interno all’azienda abilita i dipendenti a scegliere come proprio computer un PC oppure un MacBook Pro o un MacBook Air.

Alfieri del Mac inadatto all’uso aziendale, paladini del Pc perché *è professionale*, sostenitori degli *standard di fatto* esistenti solo nella vostra testa, palesatevi. E magari chiedersi perché, se è vero che i Mac costano di più, una Ibm butta via i soldi così. In assenza di lumi, suggerisco la risposta. Qua sotto.

<blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr">Benefits of <a href="https://twitter.com/hashtag/IBM?src=hash">#IBM</a> <a href="https://twitter.com/hashtag/Apple?src=hash">#Apple</a> partnership. Employees can now choose between MAC or PC. <a href="https://twitter.com/hashtag/betterwaytowork?src=hash">#betterwaytowork</a></p>&mdash; John Collins (@johncollins2014) <a href="https://twitter.com/johncollins2014/status/603840150657998848">May 28, 2015</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>