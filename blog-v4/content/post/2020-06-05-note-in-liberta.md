---
title: "Note in libertà"
date: 2020-06-05
comments: true
tags: [Voorhees, MacStories, Shortcuts, Note, iOS, macOS]
---
_Sul mio computer voglio la libertà di fare come mi pare_ è una goliardata. Merita rispetto chi sul computer (come in altri ambiti) vuole _salire a un livello superiore_.

Uno dei bersagli preferiti dei paladini della libertà apparente è iOS, perché il filesystem, perché il desktop, perché _non funziona come sono abituato quindi è sbagliato_.

iOS, anche se in misura minore di macOS (che, a essere di manica larga, ha il doppio degli anni), dispone degli strumenti per salire a un livello superiore, a partire dai Comandi rapidi.

Sembrano una cosetta e invece vengono sfruttati a un livello che non avrei mai immaginato. Si guardi John Voorhees su _MacStories_  che riepiloga [una serie di Comandi rapidi che potenziano e raffinano l'esperienza di utilizzo di Note](https://www.macstories.net/stories/shortcuts-rewind-working-with-apple-notes-part-1/).

Note di per sé è una app utile, che fa molto bene alcune cose ma non fa tutto quello che ci piacerebbe. Con i Comandi rapidi, tuttavia, diventa qualcosa di più potente, utile, comodo; questa è libertà. Prendere l'esistente e fare meglio.

P.S.: reduce nella giornata da una discussione con una persona incapace di salvare una schermata sul suo computer e determinata a non volerlo imparare. Libera di non farlo, come di non imparare una lingua o non cucinare (è il mio caso, per esempio). Poi però ti ritrovi con due figlie in cerca di cibo e la tua libertà di non cucinare è una mezza fregatura.

P.P.S.: si apre la stagione dei viaggi e tradizionalmente si verifica qualche intoppo nella pubblicazione. Cercherò di assicurare la regolarità e, comunque, terrò fede al proposito di inizio anno di produrre un post al giorno. Magari in differita, ma nessun giorno di calendario verrà lasciato indietro.