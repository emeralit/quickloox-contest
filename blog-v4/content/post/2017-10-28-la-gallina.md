---
title: "La gallina"
date: 2017-10-28
comments: true
tags: [Windows, Mac, Computerworld, Ibm, Sap, Walmart, Delta, Ge]
---
Un classico del mondo Windows quando si fa notare l’avanzata di Apple nelle aziende è l’alzata di spalle seguita da qualche frase fatta attorno al luogo comune di *standard di mercato*.

È che un [articolo di Computerworld](https://www.computerworld.com/article/3234770/apple-ios/windows-fades-as-business-develops-a-taste-for-apple.html) fa a pezzi il luogo comune di cui sopra con una serie di dati e di aziende che non avevo mai visto tutti assieme, a formare un quadro impressionante se si pensa all’irrilevanza che per decenni hanno attribuito ai Mac.

Lascio il piacere della scoperta dei contenuti. Mi limito a riportare che vengono citati nomi come Ibm, Sap, Walmart, Delta, General Electric, Bank of America. I numeri di adozione sono sempre significativi. La ragione del cambio di marea?

>Il trionfo del pensiero a lungo termine sui benefici immediati.

Le aziende più evolute e consapevoli hanno capito che la gallina di domani vale più dell’uovo oggi. E pianificano negli anni invece di vivere alla giornata.
