---
title: "E tutto va bene"
date: 2020-10-30
comments: true
tags: [iPadOS]
---
iPad Pro mi ha notificato di essere pronto ad aggiornare il [sistema operativo](https://www.apple.com/it/ipados/ipados-14/).

Voleva farlo ieri notte, ma aveva rinunciato per mancanza di alimentazione via cavo. Di giorno tengo la macchina sulla scrivania a caricarsi e la sera me lo appoggio sul comodino, non alimentato (è stracarico e non c’è alcun bisogno). Così ho aperto la notifica e ho confermato di volere installare.

Di norma delego gli aggiornamenti completamente a iPad, solo che altre volte si è andati avanti per giorni con lui che ogni mattina mi diceva di non avere installato per mancanza di cavo di rete. Un tap sul link di installazione, e solo quello, è un buon compromesso.

Al risveglio, tutto bene. Un paio di avvisi su come organizzare i widget e per il resto niente da segnalare.

Ah, è stato installato direttamente iPadOS 14.1, con salto del primo aggiornamento tappabuchi uscito poco dopo l’annuncio ufficiale.

iPhone ancora tace. Arriverà anche il suo momento.