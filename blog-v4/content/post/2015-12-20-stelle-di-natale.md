---
title: "Stelle di Natale"
date: 2015-12-20
comments: true
tags: [CoC, clan]
---
Con malcelato orgoglio, questa notte per la prima volta in carriera ho ottenuto tre stelle che contano durante una guerra in [Clash of Clans](http://supercell.com/en/games/clashofclans/) (significa avere raso al suolo il cento percento di una base nemica prima che un alleato ottenesse anche una o più stelle su quella stessa base, così da dare il massimo contributo al punteggio del clan e all’eventuale vittoria finale).<!--more-->

Un traguardo che aspettavo da settimane, giunto grazie agli aggiornamenti dell’esercito che a un certo punto hanno reso la vittoria veramente inevitabile. Fino a un certo punto, però avrei dovuto sbagliare veramente molto per non farcela.

Il nostro clan Golden Eagle (identificativo #YV2UVG99) cerca sempre rinforzi e predilige giocatori agli inizi, di basso livello, visto che i fedelissimi presenti finora stanno tutti crescendo e un clan ha più probabilità di essere vincente se al suo interno c’è una distribuzione su più livelli.

In ogni caso sono tutti benvenuti fatte salve le [premesse che avevo posto tempo fa](https://macintelligence.org/posts/2015-09-30-clan-in-espansione/).

Devo dire che, per essere un gioco di raccolta e amministrazione risorse, Clash of Clans ha più profondità da offrire e meno monotonia di quanto mi attendevo. Giocandolo in compagnia, naturalmente, i suoi difetti passano in secondo piano e questa è la ragione principale per cui si prosegue.
