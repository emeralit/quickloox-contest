---
title: "Senza tante storie"
date: 2020-10-04
comments: true
tags: [Cook, Williams, Apple, MacRumors, Jobs]
---
MacRumors nota le gratifiche che Tim Cook e altri dirigenti chiave di Apple [hanno ricevuto per il loro rendimento lavorativo](https://www.macrumors.com/2020/09/29/apple-ceo-tim-cook-rsus-2025/). Di solido non c’è neanche un dollaro; sono tutte opzioni di riscatto di azioni, esercitabili da qui al 2025.

Se l’andamento di Apple sarà positivo, potrebbero diventare decine di milioni di dollari; molto meno in caso contrario. Negli Stati Uniti è una prassi consolidata di compenso, che mira a favorire la permanenza in azienda dei più validi.

In una recente intervista, Cook ha dichiarato che certamente arriverà il momento di prendere decisioni diverse, ma ora come ora gli riesce impossibile concepire l’idea di essere fuori da Apple. Il che porta rapidamente a chiedersi se, per il futuro prossimo, sia il giusto amministratore delegato per l’azienda che fu di Steve Jobs.

Di polemiche se ne sono viste diverse e pure di critiche. Di sicuro è persona diversa da un visionario e possiede talento personale enorme, ma nessuna genialità e una competenza tecnologica ordinaria. La sua diversità da Jobs è totale, eppure il risultato netto per Apple sembra essere positivo.

Tim Cook è un uomo d’ordine, all’opposto di un irregolare come Jobs. È incapace di dirigere in prima persona lo sviluppo di un progetto e di prendersi rischi a fronte di guadagni potenziali. Mai avrebbe scommesso o scommetterebbe il futuro di Apple come Jobs ha fatto con iPhone.

Tuttavia ha fatto la cosa giusta nel valorizzare le proprie capacità invece di imitare quelle del suo predecessore. Cook lavora di diplomazia ed equilibrio per avere nei posti giusti gente capace di prendere i rischi di cui sopra e mandare avanti i progetti. Jobs accentrava, lui delega.

Al momento ha firmato, figurativamente, Apple Watch e AirPods, per lo scaffale dedicato alle classiche proposte hardware di Apple. In più ha lanciato i servizi e progetti che sembrano noiosamente orientati al soldo, Apple Card su tutti, e però sfruttano in maniera puntuale e molto efficace L’ecosistema che si è creato negli anni. Si sono già venduti miliardi (miliardi) di apparecchi Apple e sarà sempre più difficile che possa continuare a succedere; il suo _opus magnum_ consiste nell’avere creato strutture e premesse per garantire stabilità e prosperità regolari, per quanto è possibile prevedere del futuro, a partire da una catena di produzione e distribuzione superiore a qualunque altra.

Insomma, un uomo di _governance_, per parlare in aziendalese; _supply chain_ e _operation_, dove Jobs era tutto _micromanagement_ e _industrial design_. D’altronde Cook ha semplicemente continuato a far quello che faceva da Chief Operating Officer, solo applicandolo all’intera azienda invece che alla catena di produzione: tendere alla massima efficienza e precisione.

Molto è andato per il verso giusto, qualcosa poteva certo andare meglio (e la cronaca attuale degli ultimi rilasci software mostra che ci sono spazi per migliorare). Complessivamente il business è cresciuto a livelli spaziali, il titolo è salito, gli indici di soddisfazione restano altissimi.

Cook è il primo amministratore delegato nella storia del mondo ad avere portato un’azienda sopra i duemila miliardi di dollari di capitalizzazione. Inoltre ha compreso appieno la necessità di dare all’azienda una voce sociale ed etica; nel 2020 non si possono fare affari senza sbandierare il _green_ o la lotta alle diseguaglianze; a suo merito e a differenza di molti altri colleghi, lui sembra ragionevolmente sincero e a tratti fin troppo calato nella parte.

Di Jobs si citava spesso il campo di distorsione della realtà, la capacità di farti vedere cose che non c’erano o di fartele sembrare più grandi, più belle, più preziose del vero. In Tim Cook questo tratto è completamente assente; peraltro, eccelle nel governare e moderare un team capace di portare avanti il lavoro e realizzare prodotti capaci di distinguersi. È importante ricordare che ha anche saputo correggere la sua rotta in vari momenti, per esempio sul _recruiting_; a inizio mandato ha commesso diversi errori nella scelta di persone che hanno faticato a condividere la logica di Apple, o nemmeno si sono sforzate. Ora le scelte sono da tempo più azzeccate e sicure.

Tante parole per dire che, con uno stile sobrio e lineare, senza tante storie Tim Cook ha preso la barra del timone dalle mani di Steve Jobs e ha navigato in modo produttivo e apprezzabile. Si può sempre fare meglio e tutti hanno difetti; nondimeno, pensarlo a capo di Apple da qui al 2025 suona rassicurante e persino promettente; c’è da attuare un ricambio generazionale, c’è da riposizionare l’azienda nell’epoca dal coronavirus, c’è da cogliere l’evoluzione dell’informatica personale che va verso direzioni sempre meno prevedibili. Io voto Tim Cook.