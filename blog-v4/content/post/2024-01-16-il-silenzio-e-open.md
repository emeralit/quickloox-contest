---
title: "Il silenzio è open"
date: 2024-01-16T13:52:51+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Ferret, Cornell]
---
Mentre si chiudeva l’anno, qualcuno si è accorto dell’esistenza di [Ferret](https://mjtsai.com/blog/2023/12/29/apples-ferret-mllm/), *large language model* (megamodello?) alla base dell’intelligenza artificiale come intesa in questo scorcio di decennio, sviluppato da Apple insieme alla Cornell University.

Ferret è *multimodal*, ovvero lavora anche su media diversi dal testo. Proprio come [Gemini](https://deepmind.google/technologies/gemini/#introduction), il sistema presentato con grande fanfara da Google. Lavora con le immagini e anche con parti di esse arbitrariamente selezionate.

A differenza di Gemini, tuttavia, Ferret è stato pubblicato su GitHub in silenzio e, chiaramente, come *open source*. Chiunque può avventurarsi nel codice, cosa che Google o Microsoft sono più restie a concedere.

Se qualcuno pensa che Apple sia indietro sul fronte dei modelli linguistici abbinati all’elaborazione del linguaggio naturale, comprese le immagini, è un po’ diverso da così: Apple non munge gli utenti attraverso questa tecnologia e la utilizzerà nei propri prodotti solo se e quando sarà di qualche utilità per l’ecosistema.

I curiosissimi possono avventurarsi, ancora, [nell’installazione di Ferret su Mac](https://scalastic.io/en/ferret-apple-mac-llm/), cosa al momento tutt’altro che semplice e destinata alla pura sperimentazione, perché come tutti questi modelli anche lui è affamato di RAM e risorse (grazie a [Sabino](https://melabit.wordpress.com/author/melabit/) per la dritta).

Da tempo iPhone e compagnia montano un Neural Engine hardware… credo che Apple aggiungerà qualcosa ai suoi sistemi operativi quando sarà eseguibile in locale, con il dovuto rispetto per la privacy.