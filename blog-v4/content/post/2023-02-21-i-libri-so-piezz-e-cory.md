---
title: "I libri so’ piezz’ ’e Cory"
date: 2023-02-21T01:06:37+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Cory Doctorow, Doctorow, L’uomo che vendette la Luna, The Man who Sold the Moon, Heinlein, Robert Heinlein]
---
Brutta cosa quando si opera su un libro per motivi ideologici, politici, di parte e lo si spersonalizza, o peggio lo si stravolge.

È quanto [accade in questi anni alla trilogia del Signore degli Anelli](https://macintelligence.org/posts/2019-11-04-parole-in-libertà/).

Ignoravo che si potesse andare persino oltre e scippare direttamente il titolo di un libro celebre per veicolare qualche altro messaggio. O per ragioni biecamente commerciali.

Cercavo [L’uomo che vendette la Luna](https://it.wikipedia.org/wiki/L%27uomo_che_vendette_la_Luna), [The Man Who Sold the Moon](https://www.amazon.com/Man-Who-Sold-Moon/dp/0671578634), romanzo breve di fantascienza firmato da Robert Heinlein nel 1949.

A seguire il link in inglese, o a compiere una normale ricerca con Google, non ci sono problemi.

In italiano, invece, a parte il link a Wikipedia qui sopra, la prima pagina di ricerca di Google non dà nessun aiuto. Mostra invece un libro con lo stesso titolo, scritto pochi anni fa da Cory Doctorow.

Uno dirà, sarà un problema di traduzione del titolo. Niente affatto: il [titolo inglese di Doctorow](https://www.goodreads.com/en/book/show/26883861) è identico a quello di Heinlein. Semplicemente, il plagio non si impone su Google così come fa nelle ricerche in italiano.

A voler pensare male, sia pure senza aver titolo per farlo in modo documentato, viene da pensare che Heinlein è sempre stato considerato autore reazionario e inviso a una certa parte politica. La stessa entro cui si muove Doctorow.

I mandanti dello sfregio a Tolkien attraverso la nuova traduzione hanno dichiarato a chiare lettere la loro volontà di compiere una operazione politica nella più completa indifferenza per l’opera letteraria: siccome *Il Signore degli Anelli* nella loro lettura è sempre stato appannaggio di una certa parte politica, hanno agito per portarlo dalla loro parte e, oltretutto, trasformare il *fantasy* epico in *fiction* fantastica come quella di millanta altri autori che pubblicano storie di tipo analogo.

Non ho approfondito, ma l’impressione dagli *abstract* sui siti di vendita sia che la trama di qualsiasi cosa abbia scritto Doctorow sia piuttosto simile a quella del libro di Heinlein, con un po’ di aggiornamenti sulla tecnologia e nessun’altra novità.

La mia opinione su Doctorow [non è mai stata molto lusinghiera](https://macintelligence.org/posts/2020-02-10-il-guasto-per-la-provocazione/). Qui posso sbagliarmi per difetto di documentazione e un buon approfondimento potrà smentirmi. Ne sarei felice.

In ogni caso, quel titolo poteva essere richiamato, parodiato, modificato, parafrasato, riformulato, rimodulato, destrutturato, rimescolato, amplificato e altro ancora. Quel titolo non è tuo, Cory. Non è una operazione etica, anche se la tua novella fosse la più originale mai apparsa su questo pianeta e qualsiasi siano i tuoi scopi, nonché quelli dei tuoi editori.