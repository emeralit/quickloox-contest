---
title: "Il bug dell’estate"
date: 2023-08-22T04:04:09+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [Prompt, Panic, CarPlay]
---
Mi trovo in un’auto che abilita [CarPlay](https://www.apple.com/ios/carplay/) solo se iPhone è collegato via cavo.

Uso l’iPhone in questione per lanciare [Prompt](https://panic.com/prompt/) e aprire un collegamento SSH con un Mac remoto.

Aperta la connessione, inizio a scrivere il percorso di un file e uso il tasto tabulatore per l’autocompletamento, come è prassi comune in Unix.

Premo Tab e il Terminale non autocompleta. In compenso, sullo schermo di CarPlay compare la parola *Advertisement*. Solo quella, nessuna pubblicità reale. Intanto però la radio di bordo si è fermata, come se dovesse davvero passare uno spot.

Una cosa così, se avessi dovuto inventarla, mai mi sarebbe venuta in mente. E ora mi divertirò a segnalarla a Panic.

*Gli aggiornamenti in agosto del blog potrebbero essere irregolari o infrequenti.*