---
title: "Prendere i voti"
date: 2013-08-01
comments: true
tags: [Android]
---
David Pierce [recensisce su The Verge](http://www.theverge.com/2013/7/26/4558626/google-nexus-7-review-2013) la nuova edizione della tavoletta Nexus 7 di Google e inizia ricordando il modello vecchio.<!--more-->

>Poi ho guardato il Nexus 7 che ho comprato l’anno scorso, che mi è molto piaciuto. Ma è in sonno da sei mesi. La batteria è morta, può darsi per sempre. Ho anche graffiato per bene lo schermo. Ma un anno è lungo e può darsi che questo [Nexus 7] sia quello giusto. Dovevo scoprirlo.

La recensione è positiva. Il voto finale è nove su dieci.

Nel 2012 The Verge [ha recensito a firma di Joshua Topolsky](http://www.theverge.com/2012/6/29/3125396/google-nexus-7-review) il Nexus 7 comprato da Pierce. Quello con la batteria morta, inattivo da sei mesi, lo schermo graffiato.

Voto finale: 8,8 su dieci.

Un voto dovrebbe anche avere un significato. Se è solo un numero, i voti è meglio prenderli, per ritirarsi a vita contemplativa.
