---
title: "Spendaccioni squattrinati"
date: 2019-11-12
comments: true
tags: [AppleWorld]
---
Sono tutt’altro che uno spendaccione; compro hardware rarissimamente, solo se serve proprio e non sono squattrinato né benestante; la classe media all’apice della sua medietà.

Dall’alto (dal basso? Dal mezzo?) di questo profilo raccomando caldamente i [Deals di AppleWorld.Today](https://deals.appleworld.today).

È un posto strano dove trovare a prezzi assolutamente competitivi prodotti hardware e software non mainstream.

Ne ho approfittato di recente per comprare spazio cloud e una VPN: due licenze *lifetime* a prezzo scontato oltre il 90 percento, da produttori che dopo qualche sommaria ricerca preventiva sono sembrati sufficientemente affidabili.

*Lifetime* è una parola grossa e somiglia più a una scommessa, certi servizi durano che persino li dimentichi, altri ti lasciano a piedi dopo sei mesi. È un po’ come assicurarsi, solo che l’assicurazione è una scommessa comunque persa (paghi se le cose vanno bene, riscuoti se le cose vanno male) e invece qui c’è anche un elemento di azzardo, succede di rado ma potresti azzeccare la combinazione fortunata.

Devo fare riparare il vetro dell’iPad di terza generazione che sarà probabilmente la macchina della primogenita alle elementari (sì, l’obsolescenza programmata, quella che un iPad 2012 lo usi con profitto pensando al 2020, e fanno apposta, per farti spendere in continuazione, maledetti capitalisti avidi) e così guardavo alle offerte hardware, trovando una pletora di iPad ricondizionati, assolutamente vecchi e assolutamente accessibili, molto probabilmente con una spesa inferiore a quella di una riparazione convenzionale.

Forse c’è voglia di fare un regalo di Natale interessante ma senza spendere troppo, forse si è alla ricerca di un servizio da usare saltuariamente, anche qui senza esporsi in eccesso, forse si cerca un corso online (ci sono anche quelli…) eccetera eccetera eccetera. Qui i consigli per gli acquisti sono rari, ma stavolta mi pare ne valga la pena.

Ci guadagno, eventualmente, un istante di gratitudine, avendo zero legami e interessi rispetto ad AppleWorld.Today. Istante che però sarebbe preziosissimo, se appena per qualcuno ha funzionato.