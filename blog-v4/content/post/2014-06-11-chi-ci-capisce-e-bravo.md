---
title: "Chi ci capisce è bravo"
date: 2014-06-12
comments: true
tags: [Wwdc, Williams, Glassboard, Swift, iOS7, iOS, OSX, Extensions, CloudKit]
---
Di fronte a un [Wwdc](https://developer.apple.com/wwdc/), evento per programmatori, è facile dare giudizi sull’onda del momento, più basati sugli annunci ufficiali che sulla sostanza effettiva, e lasciarsi sfuggire il contenuto vero e solido dell’intera settimana.<!--more-->

Justin Williams, sviluppatore [Second Gear](http://www.secondgearsoftware.com) e coautore di [Glassboard](http://glassboard.com), è partito per il Wwdc con [questo stato d’animo](http://carpeaqua.com/2014/06/08/the-next-five-years/):

>La mia confidenza nella piattaforma iOS era andata affievolendosi negli ultimi dodici mesi. iOS 7 era un aggiornamento duro da mandare giù in termini di cambiamenti e lavoro da essi provocato. Una rivisitazione visiva discutibile e pochi miglioramenti a una esperienza di base che alla fine si percepiva indietro a quello che andava offrendo la concorrenza.

Così Williams è tornato:

>Poi, in due ore, Apple mi ha zittito. Hanno offerto praticamente una soluzione per ciascun singolo problema che avevo rimuginato negli ultimi cinque anni. Extensions, CloudKit, un nuovo iTunes Connect. E Swift, un linguaggio di programmazione interamente nuovo che con tutta probabilità darà forza al futuro dello sviluppo iOS e OS X per gli anni a venire.

Non è che sia caduto folgorato sulla strada verso San Francisco; il *post* è ben più articolato di così e contiene anche una nuova lista di cose per le quali lamentarsi nei prossimi cinque anni. Tuttavia trattasi di parere concreto, fondato, di un professionista e non tra gli ultimi arrivati.