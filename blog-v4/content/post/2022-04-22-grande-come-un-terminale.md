---
title: "Grande come un Terminale"
date: 2022-04-22T01:18:32+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [ZX Spectrum, Lisp, Psion Chess, Rubik, Basic]
---
Avevo terminato il servizio militare e dovevo iniziare a decidere che cosa fare da grande. Ero un uomo di lettere, che durante i pomeriggi inutili in caserma aveva ritrovato una passione per le scienze, grazie a un commilitone assai più pigro di me, ma laureando in fisica, insieme al quale dissezionammo certe proprietà del [cubo di Rubik](https://rubiks.com) (se hai risolto il cubo tranne due facce, puoi risolverlo tutto senza toccare la parte risolta e ruotando solo le due facce ancora in disordine?).

Nei negozi di microelettronica erano apparsi i microcomputer, oggetti misteriosi per i quali era scoppiata una mania. Mi innamorai di uno scatolotto nero con i tasti di gomma ruvida, capaci di produrre una intera parola chiave del Basic con una pressione.

Un piccolo televisore di casa divenne il mio schermo di “lavoro”: duecentocinquantasei per centonovantadue pixel, o trentadue per ventiquattro caratteri. Se apro una finestra del Terminale standard oggi, è grande più del doppio e sembra un francobollo sul mio monitor.

Il registratore a cassette più sgangherato della casa era perfetto per leggere i programmi dai nastri, anche quelli più problematici, avendo una sola unica dote: un accesso facilissimo alla vitina che governava l’allineamento perpendicolare della testina al nastro.

Avrei da raccontare aneddoti per pomeriggi, ma non bisogna inquinare l’aria con la nostalgia sciocca, specie se considero come scrivo queste note ora e come lo avrei fatto sul mio ZX Spectrum 48k nel 1982.

L’[oggetto](https://worldofspectrum.org) compie comunque quarant’anni e vanno ricordati. Una macchina capace di fare andare un interprete Lisp nell’epoca in cui si vendevano calcolatori dedicati, le cosiddette [Lisp Machine](https://wiki.c2.com/?LispMachine).

Grande come un Terminale, perché proprio come un Terminale di oggi ha aperto strade incredibili e inusitate a un numero incalcolabile di persone capaci di uscire dall’ovvio e provare qualcosa di diversamente creativo.

Anche se, lo ammetto, non cambierei mai il mio [Ql](http://www.dilwyn.me.uk) con il mio Spectrum.