---
title: "Causa ed effetto"
date: 2019-05-31
comments: true
tags: [Corea, Microsoft, Windows]
---
Uno può divertirsi a pensare, chiaramente per assurdo, che ci sia un collegamento diretto tra i messaggi di Microsoft riguardanti un [nuovo sistema operativo *moderno*](https://blogs.microsoft.com/blog/2019/05/28/enabling-innovation-and-opportunity-on-the-intelligent-edge/), con aggiornamenti costanti e pressoché invisibili che avvengono in *background*, e la decisione del governo sudcoreano – nazione tecnologicamente avanzata – di [abbandonare Windows](https://www.dailypioneer.com/2019/technology/south-korean-government-to-ditch-windows-7-for-linux.html) a favore di software *open source*.

La domanda, nel caso, sarebbe quale dei due eventi ha causato l’altro.
