---
title: "Stella fissa"
date: 2023-04-30T01:25:40+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Eva Giovannini, Giovannini, ISS, Stazione Spaziale Internazionale]
---
Salutiamo l’anniversario della giornalista Eva Giovannini che comunica [l’abbandono della Stazione Spaziale Internazionale da parte della Russia](https://macintelligence.org/posts/2022-05-06-eva-né-scienza/).

Russia che intanto [ha comunicato il proprio supporto alla Stazione fino al 2028](https://twitter.com/letiziadavoli/status/1652086182497378304).

<blockquote class="twitter-tweet"><p lang="it" dir="ltr">La <a href="https://twitter.com/hashtag/ISS?src=hash&amp;ref_src=twsrc%5Etfw">#ISS</a> resterà operativa fino al 2030, anni in cui si compirà il passaggio a Stazione Commerciale. La Russia ha confermato la partecipazione fino al 2028. <a href="https://t.co/U5PsIBVYFs">pic.twitter.com/U5PsIBVYFs</a></p>&mdash; Letizia Davoli 🚀 (@letiziadavoli) <a href="https://twitter.com/letiziadavoli/status/1652086182497378304?ref_src=twsrc%5Etfw">April 28, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Si noti che nel 2030 la Stazione diventerà commerciale e quindi non avrà più lo status di cui pariamo oggi. Praticamente i russi abbandoneranno la stazione solo con qualche anticipo in più rispetto a tutti gli altri.

Mentre Giovannini continua a brillare come una stella fissa nell’azzurro cielo della Rai.