---
title: "Lo sviluppo dei limiti"
date: 2022-01-26T00:32:10+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
La dimostrazione perfetta che perseguire qualsiasi utilizzo della tecnologia solo perché è possibile farlo, è un danno per la comunità.

Su Twitter è apparso un bot, @Wordlinator, che insulta variamente chi condivide l’esito di una sua partita di [Wordle](https://www.powerlanguage.co.uk/wordle/). C’è una logica: è legittimo infastidirsi se qualcuno abusa della libertà di comunicare fornita dai social media allo scopo di inviare informazioni non interessanti.

Solo che il bot, alla fine degli insulti, comunica anche la parola che verrà usata da Wordle il giorno dopo. Twitter ha deciso per questo (e non per gli insulti) di bannarlo. Infastidire chi infastidisce è una cosa; rovinare il gioco a chiunque sia in ascolto è un’altra cosa.

Il punto dirimente della faccenda è che, se carpire a Wordle la parola da indovinare domani fosse un prodigio di hacking, ci sarebbe da stigmatizzare @Wordlinator e fargli tanto di cappello.

Invece, no; basta fare un giro su GitHub per trovare cloni di Wordle a decine, varianti di ogni tipo, emuli per tutte le stagioni. Faccio solo un esempio: [Wordle è arrivato anche su emacs](https://github.com/progfolio/wordel).

Il gioco, inoltre, è un programma JavaScript dentro una pagina web. Accedere al codice sorgente e vedere da dove arrivano le parole usate richiede perizia tecnica a livelli modesti.

In altre parole (ehm), Wordle è un successo planetario in virtù della sua debolezza. Il programma non fa alcuno sforzo per celare i suoi segreti; diventa godibile e acquista valore solo se si rispettano i suoi limiti. Solo se si hanno il buonsenso e l’amore per le cose belle per evitare di rovinarlo.

L’[Internet di una volta](https://macintelligence.org/posts/2022-01-17-l-internet-di-una-volta/). Costruire è molto più difficile che distruggere. Quando vedi qualcosa che distrugge, vale poco. Stai in rete per dare più di quello che prendi. O almeno per dare qualcosa, oltre a prendere.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._
