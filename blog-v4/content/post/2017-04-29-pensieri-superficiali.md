---
title: "Pensieri superficiali"
date: 2017-04-29
comments: true
tags: [Mac, Microsoft, Surface, Studio, Appke]
---
Mesi fa vari professionisti hanno indicato Surface Studio come un esempio dell’innovazione che Apple avrebbe dovuto portare nei computer desktop e invece niente.

Oggi leggo che [le vendite di tutta la linea Surface sono in netto declino](https://arstechnica.com/gadgets/2017/04/why-is-microsoft-trying-to-turn-its-surface-business-into-the-next-nokia/). Non è che partissero da volumi di vendita stellari. L’articolo (di *Ars Technica*, mica tifosi obnubilati) getta ombre sulla volontà di Microsoft di continuare a investire sulla gamma, ricorda la triste fine che ha fatto il *business* acquistato da Nokia e oggi praticamente azzerato, cita varie mancanze che affliggono Surface dalla nascita a partire da processori insufficienti, connettori arretrati, specifiche non all’altezza della concorrenza.

A un certo punto si arriva a scrivere che praticamente Microsoft con Surface sta facendo come Apple su MacBook Pro.

Capisco che Surface Studio non era evidentemente questo gran che di innovazione. Che se ne vendono proprio pochi. Che i professionisti lo hanno evidenziato entusiasti e poi lo hanno ignorato.

Capisco che Surface, oltre che una linea di computer, è anche un modo di pensare.
