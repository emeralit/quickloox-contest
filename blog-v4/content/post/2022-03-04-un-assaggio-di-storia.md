---
title: "Un assaggio di storia"
date: 2022-03-04T02:54:14+01:00
draft: false
toc: false
comments: true
categories: [Blog]
tags: [Mimmo, QuickLoox, Hugo, Octopress]
---
Due mesi fa qui dentro c’era solo un post, seduto su un motore che praticamente funzionava e null’altro.

Adesso c’è un blog che si avvicina a essere come si deve e soprattutto ci sono anni di post: un assaggio di un pezzo di storia dell’informatica, di Apple e di Internet.

Niente ringraziamenti espliciti, che si sono già fatti e diventano noiosi, ma chi ha fatto lo sa bene.

Niente proclami per il futuro, si continuerà a migliorare e a scrivere con tutte le intenzioni di fare bene.

Ho portato la schermata standard del blog a visualizzare dodici abstract, dai sette precedenti. Così ora le schermate accessibili sono solo duecentoquarantasette, *and counting*. Perfino [Hugo](https://gohugo.io), che finora aveva compiuto qualsiasi operazione in modo istantaneo, con una velocità davvero degna di nota, ora rigenera il blog in alcuni secondi. ([Octopress](http://octopress.org) ci metteva minuti, intendiamoci).

Sono soddisfazioni ed era tanto tempo che speravo di arrivarci. Il cammino comincia, più che compiersi, però qui ci siamo arrivati.