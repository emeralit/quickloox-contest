---
title: "Cinquanta, venti, dieci"
date: 2019-07-24
comments: true
tags: [Cook, iBook, Luna, Apollo, Williams, Jobs]
---
Leggo che qualcuno si sta divertendo a azzardare il nome del [successore di Tim Cook](https://observer.com/2019/07/apple-ceo-tim-cook-successor-apparent-coo-jeff-williams/) e allora divertiamoci, sul quando, dato che il chi mi interessa poco.

Cinquant’anni dopo [il primo allunaggio umano](https://macintelligence.org/posts/2019-07-21-il-lato-illuminato/) e vent’anni dopo [il debutto di iBook](https://512pixels.net/2013/03/omm-ibookg3/), prevedo che Tim Cook lascerà la poltrona di Ceo il 5 ottobre 2021.
