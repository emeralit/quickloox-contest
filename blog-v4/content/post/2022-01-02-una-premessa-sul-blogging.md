---
title: "Una premessa sul blogging"
date: 2022-01-02T02:01:02+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
All’ingresso nel 2022, [il servizio di posta elettronica Exchange di Microsoft si è bloccato](https://www.itnews.com.au/news/microsoft-kicks-off-2022-with-email-blocking-exchange-bug-574388).

Chi ha programmato il motore di scansione dei messaggi per la ricerca di malware, ha pensato che fosse sensato fare della data una sequenza di caratteri numerici – tipo *2201020207* per dire *duemilaventidue, gennaio, giorno due, alle ore due e sette minuti* e *trattarla nel programma come se fosse un numero*.

Non è sensato. Il peggio è che non ha fatto i conti con i limiti della rappresentazione utilizzata nel software per la sua pensata; il sistema usato, infatti, non avrebbe retto all’arrivo del “numero” 2201010001, il primo minuto del primo gennaio duemilaventidue.

In altre parole, il pensiero sottostante era che Exchange non sarebbe arrivato al 2022, oppure che qualcuno altro prima o poi avrebbe pensato a sistemare le cose in tempo. O più facilmente, non c’era proprio alcun pensiero sottostante.

Per aggiornare il problema intanto che Exchange veniva rabberciato, Microsoft ha riattivato il servizio ma non il motore di scansione; un servizio usato (anche) per arginare spam e malware ha potuto riprendere a funzionare aprendo i cancelli all’uno e agli altri.

Come può essere tutto questo una premessa al blogging? Ci arrivo domani. Un indizio: chi si è adoperato per impostare un proprio servizio di posta su un proprio server autogestito, certamente non ha avuto problemi.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*
