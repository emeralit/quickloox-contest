---
title: "Passato presente"
date: 2022-08-16T00:44:02+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Stratechery, Ben Thompson, Shortcuts, Comandi Rapidi, Data Date]
---
Le cose che succedono a Ferragosto: trovare un eccezionale post di Ben Thompson che riassume [gli interventi più interessanti su Stratechery relativi al 2021](https://stratechery.com/2021/the-2021-stratechery-year-in-review/).

Invece di essere fuori tempo massimo come pare, l’articolo è una base formidabile per confrontare lo ieri con l’oggi di tanti temi relativi all’evoluzione di Internet, del web, dei mercati tecnologici, delle piattaforme digitali e altro ancora.

Non ci sono più previsioni azzeccate o letture sbagliate, piuttosto un campionamento della situazione a qualche mese fa confrontabile con l’evoluzione odierna. C’è davvero da capire molto e difficilmente si arriverà in fondo alla lista senza avere trovato appigli denti di attenzione.

Ora però chiedo scusa perché mi devo assentare sul mio microcomando rapido [Data Date](https://macintelligence.org/posts/2022-08-11-datemi-una-data/); se lo attivo tra mezzanotte e l’una, restituisce come ora le dodici e qualcosa. Ho da capire dì se sia *bug* o *feature*.

**Aggiornamento:** nella formattazione dell’ora, bisogna fare *HH:mm:ss* per avere il formato a ventiquattro ore. Le acca minuscole restituiscono quello a dodici, senza però aggiungere AM e PM.

Dicevo: compulsare l’articolo, c’è tanto da ritrovare e da rinfrescare.