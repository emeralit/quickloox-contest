---
title: "Famolo facile"
date: 2014-01-13
comments: true
tags: [Windows, iPad]
---
Perfetta [analisi di Ben Thompson su Stratēchery](http://stratechery.com/2014/windows-8-cost-complexity/), per spiegare i motivi del trimestre più negativo di sempre per la vendita di computer e i motivi per cui Windows stenta.<!--more-->

Sintetizzo, consigliando a tutti di leggere l’originale: i computer sono difficili da usare ed è arrivato il momento di apparecchi più facili.

Microsoft ha risposto con Windows 8, che dando importanza al *touch* rende più difficili i computer esistenti; ovvero, aumentando le ragioni per *non* acquistare un nuovo computer. E dove aumenta la domanda di touch, si trovano a disagio tutti quelli abituati a usare il vecchio Windows; in altre parole, aumentando ancora una volta la difficoltà.

Ci pensi, chi passa la giornata a pensare a funzioni da aggiungere: bisogna pensare ad apparecchi sempre più facili, e le funzioni vanno per quanto possibile tolte. O si rischia di finire nell’irrilevanza, come sta rischiando a medio termine Windows. E Office, sempre più difficile da usare rispetto ad *app* sempre più semplici in qualsiasi altro ambiente diverso da un Pc convenzionale.