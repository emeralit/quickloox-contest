---
title: "La scoperta del fuoco"
date: 2015-05-12
comments: true
tags: [Cisco, Mac, Windows, InfoWorld, iPhone, iPad, cloud, ActiveX, IE6, Gartner]
---
Circa sei anni fa, [racconta InfoWorld](http://www.infoworld.com/article/2612386/mac-os-x/cisco-shows-how-to-manage-35-000-macs.html), il reparto informatico di [Cisco Systems](http://www.cisco.com/web/IT/index.html) cercava modi di bloccare i Mac sulla rete aziendale, “standardizzata” (appiattita) su alcuni modelli di PC Windows.<!--more-->

Oggi Cisco amministra in azienda trentacinquemila Mac.

L’articolo è proprio istruttivo. Racconta di quando cercavano di bloccare i Mac e, per farlo meglio, di capire perché tutte questa insistenza per volerli usare:

>È saltato fuori che quelle persone erano, da un punto di vista informatico, quelle che costavano meno all’azienda perché sapevano cavarsela da soli invece che pesare con le richieste di supporto.

L’arrivo di iPhone e iPad ha fatto rimuovere dalla rete aziendale quel veleno che è ActiveX di Microsoft, capace ancora oggi di ancorare intere aziende all’antidiluviano Internet Explorer 6. Oggi Cisco punta sulla web app, moderne, snelle, veloci, che funzionano ovunque, anche su un computer da tasca o su una tavoletta.

Naturalmente c’è anche il cloud, che si usa da un mac come da qualsiasi altro computer.

Ne viene fuori il quadro di un mondo dove chi sa veramente lavorare raggiunge il risultato, qualunque sia l’hardware o il software. A imporre Windows restano gli ignoranti e gli arretrati.

Secondo Gartner, il 2015 è l’anno in cui l’azienda media accetterà l’idea di usare Mac come oggi usa Windows, [racconta sempre InfoWorld](http://www.infoworld.com/article/2612848/mac-os-x/gartner--apple-s-macs-will-be-as-accepted-as-windows-pcs-by-2015.html).

Nel caso in azienda circolasse ancora qualche cavernicolo, va informato della scoperta del fuoco.