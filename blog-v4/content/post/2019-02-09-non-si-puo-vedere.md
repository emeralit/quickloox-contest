---
title: "Non si può vedere"
date: 2019-02-09
comments: true
tags: [OnePlus, iPhone, Canon]
---
OnePlus è uno di quei computer da tasca che fanno tutto quello che fa iPhone, ma costano meno. Nessuna meraviglia che, [come Apple](https://www.apple.com/it/newsroom/2018/10/shot-on-iphone-xs-users-share-their-best/), anche loro organizzino concorsi fotografici per premiare immagini scattate con il loro apparecchio.

Nessuna meraviglia che, nel caso di OnePlus, il vincitore [abbia rubato l’immagine a un professionista](https://petapixel.com/2019/02/01/man-cant-explain-why-prize-winning-photo-is-identical-to-another-photogs/). Il quale l’ha scattata con una Canon.

Trovo la situazione perfetta per illuminarci su che cosa significhi quel *costa meno*. Per quante [polemiche](https://9to5mac.com/2019/01/24/apple-to-pay-shot-on-iphone-challenge-winners/) possa suscitare un concorso di Apple, penso che mai si vedrà una foto non scattata via iPhone.
