---
title: "Fluttuazioni e tappeti"
date: 2023-10-16T16:51:10+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Mastodon, Twitter, X, TechCrunch]
---
Continuo a sostenere che Mastodon sia un ottimo sistema per tenersi collegati tra persone intelligenti e continua a non piacermi la piega che ha preso X, fu Twitter, dall’ultimo cambio di proprietà a oggi.

Nonostante questo, parlare di Mastodon come sostituto di Twitter rimane una boiata. L’ultimo conteggio di utenti ha rivelato un errore: [Mastodon ha quattrocentosettemila utenti in più di quanto si credesse](https://techcrunch.com/2023/10/09/mastodon-actually-has-407k-more-monthly-users-than-it-thought/?guccounter=1) e questo cambia i dati di crescita. Chissà come sono finiti sotto un tappeto, ma non importa; fanno differenza.

Ecco, con tutto quello che è successo a X, la piattaforma è tuttora popolata da duecentoquarantacinque milioni di persone attive che pubblicano mezzo miliardo di post, risposte, condivisioni ogni giorno.

Quattrocentosettemila utenti in più o in meno, su X, sono una fluttuazione casuale. 