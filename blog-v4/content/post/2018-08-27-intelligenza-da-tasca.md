---
title: "Intelligenza da tasca"
date: 2018-08-27
comments: true
tags: [Siri, Ai, ZDNet, iPhone, Tsmc]
---
Detesto la mania di speculare sugli iPhone che saranno e che normalmente non sono come la speculazione voleva.

Faccio un’eccezione per ZDNet che immagina per gli iPhone del 2019 una banco di Ram non volatile in cui potrebbe trovare posto codice di *machine learning* sempre e istantaneamente disponibile, senza gravare sulla dotazione standard della macchina.

È la cosa più vicina a Siri offline che si possa oggi immaginare e sarebbe una aggiunta notevole alle possibilità di iPhone. Sarebbe anche solo l’inizio di ulteriori impieghi dell’intelligenza artificiale in un computer da tasca.

iPhone del 2019, quindi non la prossima generazione annunciata il mese prossimo, ma quella dopo. Ecco perché detesto la mania di speculare sugli iPhone di domani: già un singolo esempio da acquolina in bocca fa perdere subito la pazienza.
