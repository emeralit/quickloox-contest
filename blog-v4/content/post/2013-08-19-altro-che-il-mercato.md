---
title: "Altro che il mercato"
date: 2013-08-19
comments: true
tags: [iOS]
---
Ha scritto **Marco** a segnalare (grazie!) una pagina da leggere, scritta da un altro Marco di nome Arment, gran programmatore: [La differenza tra lo sviluppo iOS e Android e perché non è solo una questione di numeri](http://www.marco.org/2013/08/14/the-difference-between-ios-and-android-developers).<!--more-->

Arment cita un [articolo di Rene Ritchie su iMore](http://www.imore.com/difference-between-ios-and-android-development) e lo faccio anch’io:

>Le persone – i programmatori – non sono solo numeri. Hanno gusti. Hanno orientamenti. Se non fosse così, tutte le grandi app scritte per iPhone nel 2008 sarebbero già state scritte per Symbian, PalmOS, BlackBerr e Windows Mobile anni prima. Se non fosse così, tutte le grandi applicazioni per Mac sarebbero migrate a Windows da più di dieci anni.

Commenta Arment:

>Questo è anche il motivo per il quale le aziende che provano a dare bastonate agli sviluppatori o pagano piccoli incentivi per scrivere per le proprie piattaforme non capiscono gli sviluppatori e non attireranno i migliori.

Tutto il discorso ruota intorno al dibattito sull’onnipresente quota di mercato, perché Android ha tanta quota di mercato e invece iPhone ne ha, o avrebbe, poca.

Ma nessun si pone il problema di quale piattaforma ispiri e favorisca lo sviluppo delle *app* più innovative e versatili. Android vende tanto, ma la qualità del suo negozio di app, rispetto ad App Store, fa una figura mediocre.