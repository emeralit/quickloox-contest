---
title: "Un’eccezione utopica"
date: 2020-09-01
comments: true
tags: [Raskin, Canon, Cat, Macintosh, WordPress, Windows, Unix]
---
Così come la moneta cattiva scaccia la buona nei proverbi, gli strumenti tuttofare soverchiano quelli monofunzione.

Gli strumenti tuttofare sono la piaga della Prima Grande Era dell’informatica, che sarà sì e no a metà svolgimento.

La gente si illude che il principio del coltellino svizzero non sia una brillante idea per affrontare situazioni impreviste di provvisorietà, ma una soluzione sbrigativa a tutti i problemi.

Così si siedono a tavola per mangiare con il coltellino svizzero; riparano un motore con il coltellino svizzero; temperano le matite con il coltellino svizzero; tagliano la carta con il coltellino svizzero. Segano un ramo, ingrandiscono un particolare, aprono una latta con il coltellino svizzero.

Come? È gente che non ha capito? Provo a dirlo da molto ma non trovo ascolto. Ammiro forchette, chiavi inglesi, temperini, forbici, seghetti, lenti, apriscatole. Comprendo la grandezza di Unix, tanti pezzi monofunzione che si collegano per raggiungere obiettivi complessi. Venero personaggi come [Jef Raskin](https://www.fastcompany.com/1663212/the-untold-story-of-how-my-dad-helped-invent-the-first-mac), capace di creare un linguaggio con sette istruzioni che permette di usare le iniziali dei comandi e al resto pensa il sistema. Macchine dove ogni routine contiene la propria diagnostica ed è quindi a prova di errore. Ha una visione.

Altri usano Windows, usano WordPress, Excel. Si vantano di avere una soluzione a tutto, invece che a quanto serve, cento modi per risolvere un problema per il quale serve una soluzione e novantanove sono stati sviluppati invano, mettono infinite pezze con immensi coltellini svizzeri.

Non venerano nessuno, credo. Probabilmente hanno ragione. Per tutto quello che è sporco maledetto e subito, hanno sicuramente ragione. Esiste però l’utopia della ricerca della bellezza. È esistito Arthur Clarke, autore di [Profiles of the Future: An Inquiry into the Limits of the Possible](https://www.goodreads.com/work/quotes/163451-profiles-of-the-future):

>La creazione di ricchezza è certamente da non disprezzare, ma nel lungo periodo le uniche attività umane realmente preziose sono la ricerca della conoscenza e la creazione di bellezza. Non c’è discussione, se non su quale delle due venga prima.

Detesto l’utopia, con una eccezione.