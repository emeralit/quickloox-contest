---
title: "Il case è risolto"
date: 2015-05-31
comments: true
tags: [BBEdit, GitHub, regex, iPad, Mac]
---
Un file di testo immenso contenente innumerevoli periodi dispersi casualmente la cui prima parola inizia con una lettera minuscola.<!--more-->

Una sola espressione regolare in [BBEdit](http://www.barebones.com/products/bbedit/) e il caso, anzi, all’inglese, il *case* è risolto. Tutte le iniziali in maiuscolo.

Non sapevo neanche che BBEdit fosse in grado. (Bugia: BBEdit può fare *qualsiasi* cosa. Diciamo che non lo avevo toccato con mano).

Devo dire onestamente che, se avessi cercato nell’aiuto di BBEdit, sarei ancora là a cercare. Invece mi ha dato una grossa mano un [foglio riassuntivo](https://gist.github.com/ccstone/5385334) impagabile per sintesi e completezza, che ho scoperto – sicuramente per ultimo – su [GitHub](https://github.com).

Sto apprezzando in modo crescente il sistema di condivisione e contribuzione collettiva di GitHub e questo è solo l’ultimo progetto che ha finito per risiedere anche sul mio Mac. Anche qui onestamente, la mancanza di una gestione efficiente di GitHub su iPad (per quanto ne so a oggi) è una cosa di cui sento la mancanza e toglie qualcosa alla macchina.