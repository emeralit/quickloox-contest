---
title: "Nel tempo di uno zip"
date: 2016-03-09
comments: true
tags: [Goodreader, iOS, Mail, zip]
---
Accennavo [tempo fa](https://macintelligence.org/posts/2013-07-17-libertinaggio-librario/) alla capacità di [Goodreader](https://itunes.apple.com/it/app/goodreader-4/id777310222?l=en&mt=8) di trattare i file compressi in formato .zip, capacità essenziale su iOS dove il sistema non offre mezzi propri.<!--more-->

Beh, con una eccezione. Mi sono accorto per caso che un tocco su uno zip ricevuto come allegato in Mail, *adesso*, apre una anteprima dei file contenuti nello zip.

Non ne avevo notizia: fino a ora ero abituato a inoltrare questi file verso Goodreader e lì aprirli.

È anche una notizia buona a metà, perché da quello che vedo non è più possibile inoltrare lo zip verso Goodreader. E i singoli file si possono solo copiare per essere incollati altrove, non inoltrati. Uno per uno, quindi in modo scomodo.

Il miglioramento complessivo è in definitiva marginale. Però testimonia lavoro in corso e può darsi che a breve si vedranno miglioramenti ulteriori. Dopotutto le giornate si allungano da quasi un trimestre e questo significa che ci avviciniamo al raduno degli sviluppatori, dove si parlerà ovviamente di iOS 10.