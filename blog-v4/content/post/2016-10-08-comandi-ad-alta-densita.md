---
title: "Comandi ad alta densità"
date: 2016-10-08
comments: true
tags: [Terminale]
---
Di pagine che parlano del Terminale ce n’è millanta, ma [questa di Michael Tsai](http://mjtsai.com/blog/2016/09/26/mac-terminal-tips/) batte tutti i record di densità: si legge in cento secondi netti e contiene numerosi consigli tosti (quanti siano anche inediti dipende dalle conoscenze di ciascuno).<!--more-->

I commenti aggiungono altre nozioni molto utili. E poi ci sono i link a un paio di pagine spettacolari, veramente, su come usare il Terminale. Però non li metto qui, così si vanno a cercare sulla pagina di Tsai. Che merita.