---
title: "Cattivi maestri"
date: 2021-07-15T00:57:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Invalsi] 
---
I dati li tiro fuori una prossima volta, perché in realtà non servono; bastano i concetti, davanti al meme che il pessimo risultato degli ultimi test Invalsi sia *colpa della DaD* (abbreviazione che non ripeterò più perché chi parla per sigle si dimentica della sostanza delle cose, prima ancora che delle parole).

Fai che la tua azienda abbia avuto un calo di fatturato durante la pandemia. Ne ho viste e sentite di ogni, l’ultima – ieri – gli alberghi.

Fai che la tua azienda abbia cercato di limitare i danni per esempio, nel caso di un ristorante, con la vendita via asporto. Oppure, con il lavoro remoto.

Il fatturato ovviamente è diminuito. Diresti che è *colpa dell’asporto*? Io direi che è colpa della pandemia. Se il ristorante fosse rimasto chiuso, avrebbe perso tutto il fatturato, non una parte.

Se non ci fossero state le lezioni via rete e le scuole fossero semplicemente rimaste chiuse, i risultati Invalsi sarebbero stati migliori? Ne dubito proprio.

Le lezioni telematiche hanno consentito di limitare i danni. Non è colpa loro; è colpa della pandemia.

E ora, si dice, bisogna assolutamente tornare alla scuola in presenza. Ma bene, anzi, benissimo. Domanda stupida, indegna dei test di valutazione: è stato fatto tutto quello che andava fatto per assicurare la scuola in presenza sempre e comunque, a prescindere dalle condizioni a contorno?

La pandemia passerà. Restano purtroppo tante altre cause che possono portare alla chiusura di una o più scuole. Sono state eliminate? O toccherebbe ricorrere a un piano alternativo? Se questo non prevede le lezioni remote, che fanno così male ai ragazzi, che cosa prevede?

Proviamo a metterla su un altro piano. La scuola (la Scuola) dovrebbe essere attrezzata per dare a tutti adeguata qualità dell’apprendimento. In qualsiasi situazione. Se questo non accade è responsabilità della scuola, non degli strumenti di insegnamento. Che se sono usati male vanno saputi usare bene, più che essere additati come colpevoli da cattivi maestri. Non quelli delle cattedre, ma quelli della propaganda.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*