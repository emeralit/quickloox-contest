---
title: "La candela che brucia da due parti"
date: 2015-02-18
comments: true
tags: [batterie, WirelessKeyboardII]
---
Settimo cambio di batterie nella Wireless Keyboard II, per una durata di 118 giorni, quattro mesi.<!--more-->

Inizio a pensare che lasciare la tastiera in standby invece che spegnerla manualmente abbia il suo costo: dei sette *data point*, gli ultimi quattro sono i più bassi di tutta la serie. Tre dei quattro sono molto sotto i duecento giorni; il peggiore dei primi tre – quando spegnevo manualmente – è ben sopra i trecento, per una media complessiva di 268 giorni.

Però vado avanti così. La minima comodità di avere la tastiera sveglia all’istante quando si attiva Bluetooth vale quei pochi centesimi di euro.
