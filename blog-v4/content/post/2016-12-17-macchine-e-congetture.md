---
title: "Macchine e congetture"
date: 2016-12-17
comments: true
tags: [Mac, mini, Lorescuba]
---
Ricevo da **Lorescuba**:

>Oggi, libreria Mondadori. Macchinario che credo serva a stampare un libro e rilegarlo, ma è solo una congettura. Un po’ nascosto ma ben visibile, un Mac mini…

 ![Macchina da stampa in libreria Mondadori](/images/mondadori.jpg  "Macchina da stampa in libreria Mondadori") 

Mi si dirà che non è una macchina professionale, le sue specifiche sono miserelle specialmente pensando che manca di un aggiornamento da lungo tempo. Tuttavia guardo la macchina, nel senso più allargato, e mi sembra molto professionale.

Credo anche che cinque Mac mini uno sopra l’altro e collegati con il giusto software vadano più veloci di qualsiasi MacBook Pro mai prodotto. Solo una congettura.
