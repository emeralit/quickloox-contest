---
title: "Valori fuori norma"
date: 2021-04-18T01:28:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Jeff Bezos, Amazon] 
---
Jeff Bezos ha scritto la sua [ultima lettera agli azionisti](https://www.aboutamazon.com/news/company-news/2020-letter-to-shareholders) come amministratore delegato di Amazon. E, come ha sempre fatto, in fondo ha allegato anche la prima, scritta nel secolo scorso, era il 1997. L’una e l’altra insieme sono due letture assolutamente raccomandate.

Bezos era l’uomo più ricco del mondo. Poi ha divorziato. Ha riconosciuto metà del patrimonio alla moglie. Ora è ancora l’uomo più ricco del mondo. Chiaro che disponga di mezzi sconosciuti ai comuni mortali (ha disposto finanziamenti personali ad aziende di avanguardia nelle iniziative di contrasto al cambiamento climatico per dieci miliardi di dollari). Altrettanto chiaro che qualcosa lo abbia capito e lo capisca.

Per questo va letto con rispetto e attenzione, checché si pensi di Amazon o della sua gestione. La maggior parte di noi tremerebbe ad affrontare la gestione di un condominio (non tutti, eh). Lui lascia al prossimo amministratore delegato un milione e trecentomila dipendenti. Intanto, lascia un consiglio come questo:

>Chi voglia avere successo negli affari (e nella vita) deve creare più di quanto consumi. L’obiettivo dovrebbe essere creare valore per chiunque interagisca con noi. Un’attività che non crea valore per coloro che raggiunge, può sembrare di successo in superficie, ma è destinata a non durare.

Dopo di che passa a dettagliare le stime del valore creato nel 2020 per azionisti, clienti, dipendenti, venditori indipendenti: totale, trecentouno miliardi. Non sono tutti soldi; sono tempo risparmiato, denaro risparmiato, costi risparmiati. Il calcolo è semplice e per niente stupido, da leggere.

La parte che ho più apprezzato è la seguente:

>Sappiamo tutti che l’originalità ha un valore. Ci viene insegnato a “essere noi stessi” Ciò che chiedo realmente è l’essere realistici sull’energia che ci serve per mantenere l’originalità. Il mondo ci vuole tipici e ci tira per la giacchetta in mille modi. Non dobbiamo lasciarlo accadere.

>C’è un prezzo da pagare per essere originali, e ne vale la pena. La versione favoletta di “sii te stesso” è che, una volta permesso alla nostra originalità di brillare, i problemi finiscano. È una versione fuorviante. Essere sé stessi merita. Ma non bisogna aspettarsi che sia semplice o gratis. Bisogna metterci costantemente energia.

Per l’uomo che fu due volte il più ricco del mondo, l’originalità e la creazione del valore sono collegate. Per uno che ha sempre usato computer considerati fuori norma, è rinfrescante. Anche se non credo che mi usciranno mai dieci miliardi dalla tasca.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*