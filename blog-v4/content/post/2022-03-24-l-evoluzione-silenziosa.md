---
title: "L’evoluzione silenziosa"
date: 2022-03-24T02:01:59+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Scripting OS X, aa, du, grep, hdiutil, Monterey, shortcuts, Comandi rapidi, Terminale, plist]
---
Pur con qualche (notevole) eccezione come gran parte delle cellule cerebrali, non siamo gli stessi di un anno fa e neanche quelli di ieri. Nel nostro corpo cellule nuove sostituiscono quelle vecchie e gli organi rimangono sempre quelli, solo che delle cellule individuali perdiamo ogni traccia, e giustamente, essendo decine di migliaia di miliardi.

Lo stesso, su scala infinitamente e misericordiosamente più piccola, avviene nei nostri Mac. Un aggiornamento modifica file annidati nelle viscere del sistema, un elemento dell’interfaccia cambia impercettibilmente una sfumatura di colore, una *plist* guadagna o perde elementi secondo convenienza e così via.

Ecco perché ha poco senso la domanda fatidica *che cosa cambia nel nuovo sistema operativo?* Qualsiasi risposta diamo, considera al massimo quello che si vede e magari ancora qualcos’altro, che non si vede ma si conosce; ciò che è nascosto alla vista, non parliamo di quanto non viene neanche annunciato esplicitamente, non lo conosciamo e ci sono buone possibilità che non lo conosceremo mai.

A meno che non siamo curiosi come su *Scripting OS X*, dove hanno trovato [alcune novità nei comandi da Terminale di Monterey](https://scriptingosx.com/2022/03/some-cli-updates-in-macos-monterey/).

Può darsi che ce ne siano altre; i comandi di Terminale nel mio Mac sono millenovecentoquaranta (apri il Terminale, premi due volte il tasto Tab) e scandagliare tutto è impresa notevole.

Per esempio, è arrivato un comando `shortcuts` per interagire con i Comandi rapidi; `rm` contiene una nuova opzione, per avvisare – volendo – che verranno cancellati più di tre file oppure che sta per avvenire una cancellazione ricorsiva di directory, ovvero svaniranno anche le sottocartelle.

Ci sono cambiamenti in `du` (stima l’occupazione di spazio elencando i file), `grep` (espressioni regolari), `hdiutil` (Utility Disco via Terminale), `plutil` (per maneggiare i file .plist) eccetera.

Ho scoperto che esiste un comando `aa`, Apple Archive.

E altro ancora.

Un Mac cambia, si evolve, si rinnova. Anche in funzione di questo, bisogna educare la testa al cambiamento più di quanto ci venga naturale e istintivo. Il cambiamento stressa, ma sovente ci fa guadagnare. Tanto, anche pensando di riuscire a bloccare le cose per nostra convenienza, apri il Terminale e scopri che non è più come prima.