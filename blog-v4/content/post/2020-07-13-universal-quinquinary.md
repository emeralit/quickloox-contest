---
title: "Universal Quinquinary"
date: 2020-07-13
comments: true
tags: [Universal, Binary, Mac, Apple]
---
La gggente è cresciuta convinta sotto sotto che la seconda parte di _Universal Binary_ sia, certo, il riferimento a _binario_ come sinonimo di _eseguibile_, ma anche il supporto per due piattaforme diverse di hardware e software.

Una nota di lettura davvero veloce mostra invece che Apple rende possibile, in linea di principio, [eseguibili funzionanti in modo nativo su cinque piattaforme](https://developer.apple.com/documentation/foundation/1495005-mach-o_architecture): PowerPC 32 bit, PowerPC 64 bit, Intel 32 bit, Intel 64 bit, Arm 64 bit.

E ci sono anche i sottotipi di piattaforma: soprattutto grazie a varianti di Arm si potrebbe produrre una mostruosa applicazione _one size fits all_ [capace di supportare diciassette piattaforme](https://tenfourfox.blogspot.com/2020/06/the-super-duper-universal-binary.html).

Le difficoltà pratiche contano, sicuramente, più di quelle teoriche. Potrebbero essere richiesti più stadi di assemblaggio dell’applicazione, da prodursi con più versioni di Xcode. Produrre un Intel-Arm è un conto, già allargare fino a PowerPC è un altro.

Nondimeno, una applicazione Mac ben scritta potrebbe oggi essere prodotta con compatibilità PowerPC e distribuita con sforzo da minimo a moderato. Se è mal scritta, è tutta un’altra faccenda. Non impossibile, comunque.

Quando uno sviluppatore indipendente tarda ad aggiornarsi, non è un buon segno. Quando manca di supportare le macchine più vecchie, è una cosa grave.