---
title: "Per un milione di conchiglie"
date: 2021-06-20T00:07:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Museo Malacologico Piceno, Cupra Marittima, Power Macintosh 7200/90, Cossignani] 
---
Il [Museo Malacologico Piceno](http://www.malacologia.org) nasce dalla passione di due fratelli marchigiani e oggi occupa all’estremità nord di Cupra Marittima un capannone che, non fosse per una grossa scritta *malacologia* sul corpo dell’edificio e pochi cartelli stradali poco visibili, potrebbe passare completamente inosservato.

Sarebbe un peccato perché ci si può aggirare dentro anche per un paio d’ore con meraviglia e curiosità, quando non genuino interesse. Il capannone si sviluppa su due piani; a terra si visita una collezione fantasmagorica di un milione di conchiglie, mentre sopra sono in esposizione soprattutto i manufatti creati con le conchiglie e in generale con la madreperla. Si fa presto a sottovalutare il campo che è senza fine, dalle acquasantiere agli strumenti musicali di pregio, passando da oggetti da toeletta, bottoni, statue di presepio, giocattoli, ornamenti, utensili, crocefissi, ventagli (molti), decorazioni e chi più ne ha più ne metta.

Come bonus vanno segnalati una discreta rappresentanza di fossili (conchiglie a go-go, ma anche un rettile lungo molti metri e due crani capaci di portare a spasso un adulto rannicchiato), crostacei di grandi dimensioni, centocinquanta squali imbalsamati e una notevole esposizione di maschere africane, immancabilmente decorate da conchiglie. Mi dimentico sicuramente altri *paraphernalia*.

La competenza malacologica aggiunge molto alla visita eppure la gioia degli occhi e l’incredibile varietà dell’esposizione sono più che sufficienti a giustificare i dieci euro del biglietto. Il museo si mantiene da solo – in un altro Paese ne farebbero un vanto cittadino – con il commercio di conchiglie; a fronte del milione esposto, i fondatori ne dichiarano altri nove nelle collezioni di studio. Si vedono testimonianze di mostre ed eventi speciali che si sono susseguiti negli anni; andando in stagione balneare è facilissimo ritrovarsi in una decina di persone sparse tra due piani di un capannone industriale, di sabato pomeriggio, mentre due strade più in là ci si ammassa sulla spiaggia.

La collezione nasce da un interesse palesemente amatoriale ma è tutt’altro che dilettantistica; le teche sono completamente documentate, esemplare per esemplare (esistono conchiglie grandi come granelli di sabbia) e qua e là si trovano pannelli da leggere per farsi un’idea. Non si va oltre, ma la visita vale il tempo. Se non altro, chi ama il genere può portarsi via conchiglie bellissime da usare come soprammobile, regalo, fermacarte eccetera a prezzi da realizzo.

Avrei parlato di altro; solo che i fratelli Cossignani, nell’esporre veramente tutto, hanno all’ingresso anche un [Power Macintosh 7200/90](https://everymac.com/systems/apple/powermac/specs/powermac_7200_90.html), in compagnia di uno scanner professionale altrettanto datato e un paio di altri reperti appena meno fossili del resto; sono serviti in passato per organizzare e amministrare la collezione.

Nelle Marche. Negli anni novanta. Per due collezionisti di conchiglie. Un Mac. Come faceva quello spot? Per i folli, i piantagrane, i pioli tondi nei fori quadrati, quelli che vedono le cose in modo diverso…?

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*