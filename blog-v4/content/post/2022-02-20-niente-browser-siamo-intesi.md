---
title: "Niente browser, siamo intesi"
date: 2022-02-20T00:49:45+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Edoardo Volpi Kellermann, watch]
---
Avevo scritto diversi anni fa dell’inutilità di usare un browser su watch. Ieri ho ricevuto un messaggio con un link a un articolo per me molto interessante (grazie, [Edoardo](https://tolkieniana.net/wp/rivendell/musica/edoardo-volpi-kellermann/)), tanto da volerlo scorrere all’istante.

Ho toccato il link su watch e ho potuto leggere integralmente l’articolo. Esperienza funzionante, tranquilla, piacevole, ben progettata.

La resa grafica di una pagina Html è cosa differente dal browsing, dalla navigazione. Soprattutto, [watch Serie 7](https://www.apple.com/it/watch/) è un apparecchio con uno schermo ben diverso da quello della prima generazione. La luminosità per leggere, le dimensioni e quindi la parte di testo visibile sullo schermo sono tutt’altra cosa.

Anche con questi distinguo, oggi la mia posizione di qualche anno fa ha perso decisamente ragionevolezza e quindi la cambierò.
