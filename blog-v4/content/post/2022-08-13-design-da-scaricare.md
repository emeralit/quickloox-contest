---
title: "Design da scaricare"
date: 2022-08-13T01:05:51+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [iOS 16, iOS, batteria]
---
Il mondo è bello perché le persone riescono ad accapigliarsi anche sul [comportamento dell’indicatore numerico della carica della batteria in iOS 16](https://www.macrumors.com/2022/08/10/ios-16-battery-status-controversy/). Che poi è beta, ovvero: ci si accapiglia sul nulla o quasi.

La controversia tuttavia è interessante come campo di applicazione del design dell’interfaccia, dal momento che in un ambito così ristretto non c’è rischio di farsi confondere da considerazioni di contorno: come dovrebbe aggiornarsi l’icona della batteria quando è attivo l’opzionale (va ricordato) indicatore numerico? Questo tweet mostra bene [varie alternative possibili](https://twitter.com/michaelnevernot/status/1557165131888476160):

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Nothing wrong with what Apple released but I think I might’ve preferred something like Alternative A for the battery indicator <a href="https://t.co/a44879RIFk">pic.twitter.com/a44879RIFk</a></p>&mdash; Mikael Johansson (@michaelnevernot) <a href="https://twitter.com/michaelnevernot/status/1557165131888476160?ref_src=twsrc%5Etfw">August 10, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Ultimamente Apple compie numerose scelte di design quantomeno discutibili e quindi mi sono sorpreso nel vedere le ipotesi a confronto: al momento, quella di Apple (che potrà tranquillamente cambiare in peggio da qui alla versione definitiva) è *la migliore*.

Sapresti dire perché? Lo spiegherò in un prossimo post.

Sapresti proporre un design di interfaccia ancora migliore? Farò una prossima volta anche questo.