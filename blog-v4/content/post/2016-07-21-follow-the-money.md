---
title: "Follow the money"
date: 2016-07-21
comments: true
tags: [Android, iOS]
---
*Segui il denaro*. Spesso è la chiave per dipanare un intrigo, dirimere un dubbio, risolvere un problema, perfezionare un’indagine.

Può essere anche un metro di giudizio. Se per esempio mi dicono che negli ultimi tre mesi gli scaricamenti di *app* Android sono stati il doppio di quelli di iOS, ma [gli sviluppatori su iOS hanno incassato il doppio di quelli Android](https://www.appannie.com/insights/market-data/app-annie-index-market-q2-2016/), un’idea me la faccio. Di quanto valgano certe *app*, di che valore effettivo abbiano gli ecosistemi.

*[Ci vuole del denaro anche per provare a [trasformare l’editoria informatica](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/). Tempo per chiunque ci creda di [farlo sapere in concreto](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac), o farsi chiarire qualsivoglia dubbio.]*