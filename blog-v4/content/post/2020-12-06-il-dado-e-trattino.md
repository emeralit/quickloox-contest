---
title: "Il dado è trattino"
date: 2020-12-06
comments: true
tags: [WordPress]
---
Io lo so che WordPress fa funzionare un terzo dei siti di tutto il mondo, che lo usano milioni di persone, che è una piattaforma di successo planetario eccetera. Rispetto tutti e vedo siti enormi e bellissimi fatti con WordPress.

Però ecco, ieri ho perso un’ora per capire come mai non riuscivo ad aggiungere una discussione a un forum innestato in WordPress grazie all’ennesimo plugin, presumo messo a punto dal [Professor Nefario](https://despicableme.fandom.com/wiki/Dr._Nefario).

Il problema stava in un’immagine da aggiungere, ma non riuscivo a capire.

Poi sono stato messo sulla strada giusta e, con l’aiutino, ho capito: il nome file dell’immagine non era in regola con il tipo di caratteri riconosciuto dal plugin.

Perché conteneva… un trattino.

Persino il sito del mio home banking, un covo di cospiratori che ti mandano tutti i documenti di uno stesso tipo con uno stesso nome file, accetta un nome con il trattino (e sarebbe da vedere, quanti altri caratteri non accetta).

È possibile esercitare la propria sociopatia in infiniti modi attraverso WordPress, ma il divieto di trattino in un nome file non lo avevo mai visto, e *qualche* piattaforma l’ho usata.

E WordPress è almeno parzialmente colpevole di questo scempio, perché permette l’esistenza di plugin veramente al limite della decenza, quando non oltre. Non so come, ma va punito.