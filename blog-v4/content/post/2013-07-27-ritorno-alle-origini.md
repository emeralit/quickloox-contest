---
title: "Ritorno alle origini"
date: 2013-07-27
comments: true
tags: [Durate, Trackpad, Duracell]
---
L’ultimo giro di Duracell su Magic Trackpad è durato molto meno che nelle passate edizioni: solo 74 giorni, appena sopra il minimo di 70 giorni registrato dalle pile fornite di serie.<!--more-->

Ci sono due ragioni possibili: una è banalmente una partita di pile riuscita meno bene. L’altra è che, sperimentalmente, ho iniziato a non spegnere più il *trackpad* a mano, la sera, contando sul risparmio energia dell’apparecchio (quando arriva il tramonto, spengo il Bluetooth e uso Mac con tastiera e trackpad interno). Mi sembra che però il differenziale sia notevole, forse esagerato: con il prossimo set di pile proverò a mantenere la nuova abitudine e verificare se il dato rimane coerente o varia.

È il sesto cambio di pile di cui ho preso nota; i valori sono stati di 70, 133, 177, 175, 313, 74 giorni, per una media di 157 giorni di durata. Praticamente cinque mesi per ogni coppia di pile. Dal prossimo giro inizierò anche a tenere conto del prezzo di acquisto, cosa che finora non ho fatto ma inizia a divenire interessante.

Ricordo come sempre che l’aiuto di Numbers fornisce una indicazione scorretta sulla sintassi della funzione di differenza tra date; la sintassi corretta è

=DATEDIF("13/05/2013";"26/7/2013";"D")

Con il punto virgola in mezzo. *D* sta per *days*, giorni, e può assumere diversi altri valori, come *M* o *Y*. Non mi dilungo, dato che il resto dell’aiuto è a posto e spiega tutto il necessario.

**Aggiornamento:** cambiate le pile, mi sono accorto che quelle appena esaurite non erano Duracell comuni, bensì ricaricabili. Questa spiegazione è più valida di quelle già ipotizzate. L’uso di ricaricabili è stato evidentemente frutto di una qualche contingenza e per ora resterà una eccezione alla regola; sto già usando il *trackpad* con nuove Duracell usa e getta.