---
title: "Ultima chiamata per il buonsenso"
date: 2018-02-13
comments: true
tags: [Skype, Microsoft]
---
Pensavo che almeno il buonsenso imponesse di considerare Skype sepolto, all’indomani di [questo post di tre anni e mezzo fa](https://macintelligence.org/posts/2014-05-14-non-ci-parlo-piu/).<!--more-->

La piattaforma tuttavia riserva ancora delle sorprese.

Per esempio un *bug* che consente a un aggressore di prendere il controllo totale del computer. *Bug* la cui risoluzione in un aggiornamento di sicurezza richiederebbe per Microsoft troppo lavoro. Così, pazienza; eventualmente uscirà una nuova versione contenente anche la soluzione al problema. Nel frattempo si possono incrociare le dita o recitare incantesimi di protezione.

L’ultima frase è mia ed è sarcastica. Quanto la precede è un sunto fedele dell’[articolo di ZdNet](http://www.zdnet.com/article/skype-cannot-fix-security-bug-without-a-massive-code-rewrite/) sulla faccenda.

Se la rileggo, ancora non ci credo. *Troppo lavoro, arrangiatevi fino a una nuova versione*.

Microsoft è al corrente del *bug* da settembre.

Se il Chief Technical Officer in azienda insiste ancora con Skype, forse è il caso di fargli vedere l’articolo di ZdNet. O questo, se sa solo l’italiano (non c’è da ridere).

**Aggiornamento:** è andata così, ma nel passato. Il ricercatore che ha [documentato il bug](http://seclists.org/fulldisclosure/2018/Feb/33) ha frainteso le affermazioni di Microsoft, che sono quelle riportate, ma riguardano un caso già chiuso, perché la versione 8 di Skype per Windows (il problema era esclusivo di Windows) [già rimedia](https://answers.microsoft.com/en-us/skype/forum/skype_newsms/update-on-installer-for-skype-for-windows-desktop/242f1415-1399-42e1-a6a2-cd535c8b7ff8?tm=1518635969608&auth=1) e il ricercatore si riferiva alla versione 7.40.