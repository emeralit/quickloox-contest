---
title: "Un amore banale"
date: 2015-06-26
comments: true
tags: [Viticci, iPad, Editorial]
---
Le ragioni per cui ci si innamora, è raro che siano sofisticate. Certe volte uno neanche sa il perché e basta.<!--more-->

È il motivo per cui è facile spiegare quanto potente possa essere un iPad portando a esempio un Federico Viticci e, per dirne una, il suo libro [Writing on the iPad: Text Automation with Editorial](https://itunes.apple.com/it/book/writing-on-ipad-text-automation/id697865620?mt=11).

Il problema è che si tratta di una argomentazione razionale, non emotiva. Essere ascoltati su base razionale è diventato rarissimo, se non parli alla pancia – o colpisci al plesso solare – qualunque ragionamento obiettivo cade nel vuoto.

Meglio allora una narrazione del tipo [Come mi sono reinnamorato di iPad](http://www.cio.com/article/2932358/ipad/how-i-fell-in-love-with-the-ipad-again.html). Cose semplici e oneste: la passione per i videogiochi, la dimensione e quindi la comodità dello schermo, la lettura, la scrittura e la produzione, l'enorme miglioramento prestazionale negli anni, la beta di iOS 9 che funziona a meraviglia e via dicendo.

Storia lunga e da gustare proprio nella sue linearità e semplicità. Il mercato medio va verso il telefono grosso più che verso la tavoletta, spesso perché è più che sufficiente. iPad in fondo condivide qualcosa della sofferenza dei computer tradizionali: sempre più in mano a specialisti e a professionisti, sempre meno in mano a gente comune.

Eppure è il compromesso migliore che sia mai stato inventato in tema di *computing*. Dirlo è banale e per questo contestabile, ma autentico almeno nel senso della genuinità.