---
title: "Sottofondo virtuale"
date: 2014-03-11
comments: true
tags: [FreeBsd, VirtualBox, Münchausen, emacs, pioneers, Catan, Unix]
---
Facendo mille cose nella giornata mi sono trovato distrattamente tra le mani una immagine disco di [FreeBSD 10](http://www.freebsd.org) scaricata nell’entusiasmo dell’annuncio.<!--more-->

Tra la cosa 32 e la cosa 33 ho fatto anche partire [VirtualBox](https://www.virtualbox.org) e ho chiesto la creazione di una macchina virtuale, pensando che alla prima difficoltà avrei buttato via tutto per mancanza di tempo.

Nessuna difficoltà, pochi minuti e tutto è finito nel migliore dei modi. L’immagine disco era quella *minimo indispensabile*, priva di interfaccia grafica e a dire il vero praticamente di tutto fuori dall’installazione.

Ho provato il nuovo installatore di pacchetti con [emacs](https://www.gnu.org/software/emacs/) e [pioneers](http://pio.sourceforge.net) ([Catan](http://www.catan.com) in ambiente *open source*), nient’altro; nel frattempo incombeva la cosa 34.

Partire così da zero è straniante ed emozionante. Sembra di vedere il [barone di Münchausen](http://www.raiscuola.rai.it/articoli/rudolf-erich-raspe-il-barone-di-munchausen-un-libro-in-tre-minuti/3128/default.aspx) che si tira per i capelli e così riesce a uscire da una palude. Caricare la più piccola *utility* dipende da decine, centinaia di altri mattoni software, che messi insieme fanno la forza di Unix.

Il tutto nella finestra piccola di VirtualBox: un sistema che nasce mentre si lavora nello schermo grande. Poco più di una dimostrazione tecnologica, ma proverò a sfruttare l’indubbio vantaggio di poter consultare la documentazione su Mac e applicare i comandi allo schermo virtualizzato per imparare qualcosa.

Se ci sono richieste, posso provare. I miei *skill* sono limitati e chiedo pazienza.