---
title: "Chi ricerca prova"
date: 2015-04-17
comments: true
tags: [ResearchKit, Swift, Pages, iOS8, watch]
---
Non sono un programmatore di formazione. Nonostante questo, occupandomi di tecnologia, l’argomento mi ha sempre affascinato e mi piace saperne sempre un po’ di più. Oltretutto, ho già ribadito più volte che [la programmazione è il nuovo inglese](https://macintelligence.org/posts/2015-04-03-la-palestra-dellardimento).<!--more-->

La configurazione celeste si è allineata in modo per me imprevedibile anche solo pochi mesi fa. Prima di tutto ho avuto occasione di preparare un [librino](http://www.apogeonline.com/libri/9788850333387/scheda) di base su [Swift](http://www.apple.com/it/swift/), il nuovo linguaggio di programmazione per OS X e iOS; adesso sto perdendo il sonno con piacere per [tradurre (in Pages!)](2015-04-08-come-volevasi-impaginare) un rilevante tomo sulla programmazione per iOS 8; e Apple ha reso pubblico il codice di [ResearchKit](https://github.com/ResearchKit/ResearchKit), l’architettura che permette di creare con (relativa) facilità scenari di test per *app* di salute e benessere da far funzionare su iOS o [watch](http://www.apple.com/it/watch/).

Non mi vedo a breve scadenza installare ResearchKit dentro chissà quale mio progetto. Certamente potrei entrare nel 2016 sapendone ben più di quando sono entrato nel 2015. Potrei pure provare il brivido di ResearchKit.

Un programmatore vero mi sfotterà, fino al 2016. Per me son comunque soddisfazioni.