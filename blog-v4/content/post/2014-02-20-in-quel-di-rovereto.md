---
title: "In quel di Rovereto"
date: 2014-02-20
comments: true
tags: [Rovereto, Marconi, iPad]
---
Domani, venerdì 21 febbraio, trasmetto da Rovereto e più precisamente dall’[Istituto Tecnico e Tecnologico Guglielmo Marconi](http://www.marconirovereto.it). L’occasione è il [Safer Internet Day 2014](http://www.saferinternetday.org/), attorno al quale l’Istituto ha organizzato una [giornata di incontri](http://www.marconirovereto.it/component/content/article/3-eventi/669-internet-safer-day).<!--more-->

Avrò un interlocutore formidabile, Mauro Berti della Polizia Postale: ha una conoscenza eccezionale della rete e di quanto alla rete è connesso per rischi e opportunità. Parleremo di Internet sicura e anche di Internet etica, un problemaccio: farò del mio meglio per garantire la comprensione ed evitare di cadere nell’ovvietà.

La particolarità dell’incontro è che, per quanto mi concerne, saranno due: uno la mattina alle 9:20 con studenti e docenti e l’altro alle 20:30 con famiglie e chiunque fosse interessato.

Il tutto dovrebbe essere trasmesso sul web in *streaming* dagli indirizzi [http://www.marconirovereto.it/live](http://www.marconirovereto.it/live) e [http://www.marconirovereto.it/diretta](http://www.marconirovereto.it/diretta). L’organizzazione prevede inoltre di raccogliere domande via Twitter, tramite l’*hashtag* #SID_MARCONI.

L’incontro sarà in gran parte spontaneo, governato dall’interazione tra i relatori e con il pubblico davanti al palco e dentro Twitter, e questo mi entusiasma perché non so dove esattamente andremo a parere e quindi finirà per essere una cosa buona.

Metterò piede in una scuola che invece di perdersi in paure e chiacchiere, si impegna e ottiene risultati. Altro motivo di soddisfazione preventiva, neanche fossimo nella [sequenza de *L’attimo fuggente*](https://www.youtube.com/watch?v=aS1esgRV4Rc) che Apple ha ripreso per il suo ultimo [*spot* pro iPad Air](https://www.youtube.com/watch?v=jiyIcz7wUH0).

In inglese, si abbia pazienza, perché in italiano è tanto più immediato per il nostro orecchio, ma non si può ascoltare.

<iframe width="420" height="315" src="//www.youtube.com/embed/aS1esgRV4Rc" frameborder="0" allowfullscreen></iframe>

<iframe width="560" height="315" src="//www.youtube.com/embed/jiyIcz7wUH0" frameborder="0" allowfullscreen></iframe>