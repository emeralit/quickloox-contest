---
title: "Così piccolo e trascurato"
date: 2015-09-25
comments: true
tags: [iPadmini]
---
Sono tutti molto veloci a dismettere iPad mini: bello, pratico in borsetta, ideale per chi non ha da scriverci sopra, poi però è un iPad 2 o poco più, insomma, una macchina messa lì per coprire un segmento di mercato, però senza troppe pretese.<!--more-->

DisplayMate rileva intanto che iPad mini 4 ha [uno dei migliori schermi usciti finora](http://www.displaymate.com/news.html#iPad_mini_4) in tutta la gamma iPad.

In particolare, la gamma dei colori visualizzabili è praticamente perfetta e il riflesso è ai valori minimi. Il contrasto in luce ambiente è superiore di quasi tre volte alle prestazioni di tutte le altre tavolette e batte del 36 percento iPad Air 2.

Così piccolo e trascurato da Apple, iPad mini 4 ha qualche asso nella manica, che Apple neanche reclamizza. Da pensarci, la prossima volta che sentiamo dell’Android che fa le stesse cose e costa meno.