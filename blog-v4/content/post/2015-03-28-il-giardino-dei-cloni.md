---
title: "Il giardino dei cloni"
date: 2015-03-28
comments: true
tags: [opensource, Mac]
---
Una delle cose splendide del mondo *open source*: un *repository* contenente un lungo [elenco di grandi giochi del passato](http://osgameclones.com) e, dove esiste, una replica in software libero del gioco stesso.

Si può leggerla da utilizzatori (bisogna fare un piccolo sforzo per verificare l’esistenza di una versione Mac) oppure da sviluppatori (magari proprio per fare girare su Mac un gioco che non lo fa unicamente per mancanza di risorse).

Oppure da benefattori del software libero. Anche senza sapere programmare, non si ha idea di quanti progetti *open source* sarebbero più che felici di trovarsi due mani in più a realizzare una versione italiana, oppure tradurre documentazione, o sistemare la grafica, o trovare *bug* da sistemare. O solo lavorare all’elenco e aggiungere cose che mancano, trovare refusi, arricchire la base dati. Tutto serve.

Non sto a commentare l’elenco perché c’è veramente di tutto e invito alla scoperta.

Se fossi ricco avvierei una iniziativa di allargamento a Mac dei giochi dove nessuno lo ha ancora previsto.