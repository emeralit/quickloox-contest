---
title: "Impicci e impacci"
date: 2020-05-19
comments: true
tags: [Tecnicadellascuola, Dad]
---
*Tu non sei un insegnante*. Quindi dovresti fare a meno di impicciarti in faccende come l’insegnamento a distanza, mi è stato detto.

Verissimo, non sono affatto un insegnante.

Sono andato in un posto pieno di insegnanti: la rivista *Tecnica della scuola*, una testata prestigiosa come minimo per il fatto che vive dal 1949. Il sito è per certi versi ruspante ma neanche impossibile, considerato che i contenuti sono davvero molti. Attorno alla testata deve esserci attenzione considerevole e primariamente da parte, appunto, degli insegnanti.

L’archivio degli articoli taggati [didattica a distanza](https://www.tecnicadellascuola.it/tag/didattica-a-distanza) contiene ben settecentosessantadue articoli. Lascio al lettore la verifica di quanti pro e quanti contro, questione ora secondaria.

Il più recente di questi settecentosessantadue articoli è datato 18 maggio, ieri.

Il meno recente… *6 marzo 2020*.

Mi stupisce poco che in settantaquattro giorni siano passati settecentosessantadue articoli. Più di dieci al giorno. Lo dicevo, è un sito molto attivo.

Mi stupisce che, prima del coronavirus, il tema su *Tecnica della scuola* fosse assente. Non per modo di dire. *Zero articoli*. Da zero a dieci al giorno è un bel passo.

Ecco perché mi impiccio senza essere un insegnante. È pieno di insegnanti che si impicciano nella didattica a distanza da due mesi, dopo averla ignorata per una vita.