---
title: "Gli estremi non si toccano"
date: 2021-04-21T01:03:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, M1, P3, Xdr, Pro Display Xdr, iMac, iPad Pro, Jonathan Ive, Tim Cook, AirTag, Mission Impossible, Apple Park, Steve Jobs, watch, U1, AirPods, Bandley Drive] 
---
Prima di tutto: più si affina la ricetta, più amo il format online degli eventi Apple. Riescono a evitare manierismi, dare freschezza e ritmo, contestualizzare, giocare anche attorno al già eventualmente visto, alternare serietà a trovate surreali e però mai eccessive. Il filmato di promozone di AirTag è carino; la parodia di [Mission Impossible](https://en.wikipedia.org/wiki/Mission:_Impossible_(film_series)) che fa infilare il chip M1 dentro iPad Pro all’agente segreto Tim Cook, dopo il furto con destrezza da MacBook Pro, riesce a essere persino leggera nonostante il profluvio di effetti speciali. Applauso. Senza parlare della copertura dei prodotti, essenziale e studiatissima. C’è gente molto brava dietro queste produzioni e onestamente credo che preferirei un altro evento online a una seduta nello [Steve Jobs Theater](https://mashable.com/2017/09/14/secrets-of-steve-jobs-theater/?europe=true).

Dei prodotti non saprei parlare. Mi viene da parlare del complesso e questa è già una recensione. Questa organizzazione fattura duecentosettantaquattro miliardi in un anno e riesce ad andare contro il consenso comune, i pareri degli esperti, l’ovvio.

Quanti articoli ho letto sulla inevitabilità di fusione tra macOS e iPadOS (ieri iOS)? Numerosi. Quanti articoli ho letto sull’arrivo di M1 su iPad Pro? Zero. Che cosa ha fatto Apple? Ecco. Invece di unificare il sistema operativo, unifica la piattaforma hardware. Significa economie di scala folli e intanto prestazioni di eccellenza. Il massimo con il minimo.

Quando è uscito [Pro Display Xdr](https://www.apple.com/it/pro-display-xdr/), sembrava che la parte importante fosse lo stand da novecentonovantanove dollari, buono per le battute e gli sfottó. Chi ha annunciato l’arrivo di quel calibro di schermo su iPad Pro? Nessuno. (*Breaking:* lo schermo di iPad Pro, come specifiche, [è migliore](https://512pixels.net/2021/04/thoughts-on-apples-new-non-imac-hardware/)).

Quanti hanno sbeffeggiato watch alla sua uscita? Oggi è una componente notevole del bilancio aziendale. Ora già girano i meme parodistici su AirTag. Vedremo. Bluetooth, accelerometro, chip U1 (quello degli AirPods) e non ricordo che cosa ancora. Il rapporto tra tecnologia e volume dell’oggetto è mostruoso. E la batteria si cambia in un attimo…

A quanta gente ho sentito dire che a Mac manca lo schermo touch. Non capiscono che ce l’ha iPad. Il percorso diventa chiaro ed evidente oggi così come era stato chiaramente tracciato in anticipo. Messi vicino, con una tastiera addosso a iPad, che differenza c’è tra lui e MacBook Pro? Il touch. Lo schermo. Il sistema operativo. Apple Pencil. Due linee di prodotto che si completano e supportano a vicenda, ampiamente diversificate per persone con esigenze ampiamente diverse. E dietro la catena di produzione è per molti versi unica. Un capolavoro di design industriale.

Capolavoro. Design. Può esserci un dubbio qualunque sulla paternità dei nuovi iMac colorati, quasi metafisici in quello spessore che vorrebbe essere zero, scomparire, come è stato anche detto durante la presentazione? Se ne è andato, ma questo iMac è firmato Jonathan Ive da capo a piedi. Steve Jobs avrebbe amato follemente questo iMac.

Da Steve Jobs a Tim Cook. A parte guardarlo sessantenne e in gran forma sulle stradine di Apple Park in t-shirt nera, da dieci anni timona – dicono – senza genio, senza colpi d’ala, tutta organizzazione e politica interna per tenere l’ambiente tranquillo e escludere le teste calde.

Fosse anche vero, fosse Cook il vigile urbano di Cupertino, dirige il traffico più imponente del mondo. E continuano a uscire prodotti nuovi nonostante Apple Park ospiti al momento quattro gatti e quasi tutti lavorino da casa. Giusto perché il lavoro remoto non è produttivo come quello dell’ufficio.

Al termine di un evento come questo mi dispiace solo di essere già a posto lato hardware. Mi piace, molto, seguire questa traiettoria di progresso tecnologico al servizio dell’individuo. Arrivo alla fine e mi ritrovo stupito da tanti dettagli, piccole e grandi sorprese, particolari inaspettati. Il colosso della tecnologia che vince grazie all’essere bastian contraria rispetto al senso comune, esattamente come quando inalberava la bandiera dei pirati su un anonimo edificio di Bandley Drive. La chiacchiera va da una parte, Apple dalla parte opposta. E non si incontrano mai.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*