---
title: "La legge dello scripting"
date: 2022-12-02T17:28:27+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [scripting]
---
Il tempo speso a supplire alla mancanza di automazione di una attività è un ordine di grandezza superiore al tempo necessario per realizzare L’automazione.

La sofferenza per la mancanza di automazione aumenta con progressione geometrica a partire dalla prima percezione della mancanza.