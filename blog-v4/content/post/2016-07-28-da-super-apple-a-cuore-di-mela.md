---
title: "Da Super Apple a Cuore di Mela"
date: 2016-07-28
comments: true
tags: [Venerandi, Quinta, Accomazzi, Kickstarter]
---
Mi permetto di dedicare un *post* direttamente a [Cuore di Mela](http://cuoredimela.accomazzi.it) perché mancano pochissimi giorni alla fine della raccolta fondi e ha dedicato un pezzo alla faccenda [nientemeno che Fabrizio Venerandi](http://www.quintadicopertina.com/fabriziovenerandi/?p=573), dal suo blog su Quinta di Copertina (che è la sua casa editrice).<!--more-->

La premessa è che Venerandi è la persona oggi più avanti in Italia (e probabilmente anche oltre) nel campo dell’editoria elettronica. Contemporaneamente è in grado di citare con dovizia di particolari la storia di *Super Apple*, rivista eccezionale come ma ve ne furono e, come tutte le meraviglie, di breve durata.

No, il link alle tracce retrocomputeristiche di *Super Apple* non lo metto. È giusto andare su Quinta di Copertina, leggere con attenzione e fare acquisti, visto che hanno titoli notevoli (uno fra tutti, il fantastico *Locusta Temporis* di cui [già si è parlato](https://macintelligence.org/posts/2014-10-09-ce-bisogno-di-scrivere/)).

Se poi avanzano tempo ed euro da lasciare per [Cuore di Mela](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac), meglio ancora. Ma l’importante è non lasciare cadere nel nulla una citazione  di *Super Apple* fatta con cognizione di causa.

*[[Cuore di Mela](http://cuoredimela.accomazzi.it) partirà, o meno, entro pochissimi giorni. A te [fare la differenza](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac).]*