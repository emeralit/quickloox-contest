---
title: "Idee stantie"
date: 2017-11-19
comments: true
tags: [Nadella, iPad, Refresh, MacSparky, Snell]
---
L’amministratore delegato di Microsoft Satya Nadella è in tour per promuovere il suo ultimo libro. Incontrando giornalisti muniti di iPad Pro, gli ha detto [amici, avete bisogno di un vero computer](http://www.techradar.com/news/microsoft-ceo-satya-nadella-on-how-its-different-from-apple-and-google).<!--more-->

Jason Snell [gli ha risposto](https://sixcolors.com/post/2017/11/whats-a-real-computer/) indirettamente dalle pagine di *Six Colors* illustrando i modi in cui utilizza il proprio iPad Pro. *Illustrando* è la parola perfetta; anche senza capire una parola di inglese, è sufficiente guardare le foto. Parlano da sole.

David Sparks su MacSparky [ha ripreso il ragionamento](https://www.macsparky.com/blog/2017/11/the-increasingly-rare-ipad-deal-killers) di Snell e raccontato dove stiano i suoi problemi nell’usare iPad come computer: la configurazione degli stili di Word e il tracciamento delle modifiche su Google Documenti. Problemi che non sono di soluzione immediata, anche se si risolvono, ma che danno chiaro il quadro della situazione: quando sei a occuparti degli stili di Word, vuol dire che un bel po’ di cose a monte funzionano già.

Entrambi si rifanno a [un recente spot di Apple](https://www.youtube.com/watch?time_continue=68&v=sQB2NjhJHvY), emblematico: una ragazzina sveglia e piena di interessi può benissimo passare una giornata munita di iPad e chiedere in buona fede *Che cos’è un computer?*

Il libro di Nadella si intitola [Hit Refresh](https://news.microsoft.com/hitrefresh/) ma, alla luce di questo episodio, mi aspetto che contenga idee stantie.

<iframe width="560" height="315" src="https://www.youtube.com/embed/sQB2NjhJHvY" frameborder="0" allowfullscreen></iframe>