---
title: "La tassa sull’impostazione"
date: 2022-02-09T01:25:45+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Ho letto un articolo che mi ha lasciato molto perplesso, [Le impostazioni non sono una falla di progettazione](https://linear.app/blog/settings-are-not-a-design-failure), di Adrien Griveau di Linear.

Il tema è uno di quei grandi *dipende* e per fortuna Griveau lo sa; per questo scrive

> C’è una differenza tra le impostazioni che un prodotto *deve* azzeccare di default e quelle su cui progettisti *non dovrebbero*, deliberatamente, avere una forte opinione.

Il dettaglio dove si nasconde il diavolo è *quante* e *quali* impostazioni ricadano nel primo insieme oppure nel secondo.

Griveau scrive di ricordarsi che *gli utenti amano le impostazioni*. Veramente? E poi ricorre a un esempio che mi sembra ingannatore:

> Che cosa fai quando configuri il tuo nuovo computer? Cambi lo sfondo scrivania. Regoli la velocità del mouse. Preimposti il browser.

E grazie. Questi sono aggiustamenti fini, facilmente diversi per ciascun singolo utente. A eccezione della scelta del browser, sono impostazioni che è *impossibile* centrare subito, per tutti. È praticamente obbligatorio dare l’opzione di regolare la velocità del mouse.

Ma già il browser, per esempio… supponiamo di avere una situazione davvero neutra, dove non c’è dietro una Apple che vuole farti usare Safari, una Google che vuole farti usare Chrome, una Microsoft che vuole farti usare il suo clone di Chrome. Come preimpostare la questione del browser?

Provocazione: sapendo che, qui invento, Chrome è usato da tre quarti dell’umanità e Safari dall’altro quarto, progetto il sistema in modo che tre volte su quattro si preimposti su Chrome. Un certo numero di persone si ritroverà soddisfatto della scelta compiuta dal sistema in sua vece e non dovrà modificare alcuna impostazione.

Altri non saranno soddisfatti e vorranno cambiare il browser. Ovviamente bisogna che esista una impostazione per farlo. Non saranno tutti, comunque, ma un sottoinsieme; al contrario, qualsiasi situazione in cui viene chiesto di scegliere il browser al momento dell’installazione del sistema costringerebbe il cento percento degli utenti dare una risposta per poter usare il proprio programma preferito.

Sarò di vecchia scuola; il mio ideale di progettazione software dà come risultato un programma che nell’ottanta percento dei casi soddisfa l’ottanta percento degli utenti, senza dover toccare alcuna impostazione.

La gente non ama affatto le impostazioni; le peggiori situazioni dell’informatica derivano da preimpostazioni messe volontariamente dallo sviluppatore sapendo che l’utente non vorrà o, peggio, non saprà cambiarle.

Un programma dovrebbe offrire un percorso di uso che va bene a otto persone su dieci senza cambiare niente e poi, per chi vuole, offrire tutte le opzioni che vuole, purché non troppo in vista. Le opzioni necessarie – la velocità del mouse – siano bene esposte, chiarissime; le altre, facilmente accessibili, per chi vuole, senza mettere i bastoni tra le ruote alle persone che non sono interessate.

In questo senso, la struttura Unix di Mac offre una opportunità spettacolare: una interfaccia grafica univoca, con un modo per fare le cose e non senssantacinque; sotto il cofano, via Terminale, un universo di modifiche possibili.

Chi conosce BBEdit apprezza la possibilità di cambiare rapidamente la scorciatoia di tastiera associata a un menu, per dire, e poi avere a disposizione nel Terminale una configurabilità a livelli siderali di dettagli infinitamente minimi e specifici.

Che possono interessare quasi nessuno e tuttavia sono a disposizione dei *quasi* che ne vogliono approfittare. Quando non voglio essere un *quasi*, le impostazioni sono per definizione un fastidio, una tassa da pagare per avere una usabilità che il progettista non è riuscito a inserire da subito.
