---
title: "C’è posta per Apple"
date: 2015-02-20
comments: true
tags: [EngageCustomer, Apple, Android]
---
Mi dispiace che Engage Customer non pubblichi più dettaglio sulla sua ricerca, che è sconvolgente: a loro dire, nel Regno Unito [il 54 percento della posta viene aperta su un apparecchio Apple](http://www.engagecustomer.com/article.detail.php?a=11529#.VOS15Rw-NVM). Android? Otto percento.<!--more-->

In Italia, metà della posta (metà!) sarebbe aperta su un apparecchio Apple.

Sono dati scioccanti che andrebbero verificati prima di farci affidamento. Come del resto quelli sulle quote di mercato, dove c’è gente che straparla da anni e viene ascoltata come oracolo.

La prima volta che metto piede in un’azienda provo a citare la statistica e vediamo che mi dicono.