---
title: "Corse di recupero"
date: 2022-05-03T01:19:43+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Nuvia, Qualcomm, Apple Silicon, M1, M2, Tom’s Hardware]
---
Dopo che Apple ha annunciato Apple Silicon, gli altri hanno cercato di serrare i ranghi, prima facendo finta che non fosse un salto avanti di indiscutibile superiorità per nascondere l’impreparazione, poi cominciando a prepararsi.

Riferisce *Tom’s Hardware* che Qualcomm, dopo avere acquistato la startup Nuvia apposta per lo scopo, [conta di fare uscire chip paragonabili a M1](https://www.tomshardware.com/news/qualcomm-confirms-nuvia-arm-chips-late-2023).

Disponibilità di massa, per la fine del 2023. Qualcomm riferisce che già da fine di quest’anno manderà campioni del chip ai produttori di computer, per tappare buchi, aggiustare dettagli, sistemare problemi. Così ci sarà modo abbondante di fare trapelare test di velocità non verificabili e basati su hardware che nessuno può usare, tuttavia ottimi per la propaganda.

Ognuno può calcolare il ritardo tecnologico come crede meglio. Tra un anno e mezzo abbondante, posto che sia vero e che le caratteristiche del prodotto finale siano all’altezza, Qualcomm potrà orgogliosamente annunciare di avere recuperato il divario con M1.

Intanto che i Mac sono passati a M2.