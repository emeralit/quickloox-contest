---
title: "Eventualmente"
date: 2015-06-07
comments: true
tags: [Apple, AppleTv, iPhone]
---
*Da solo, lungo l’autostrada*<br />
*alle prime prime luci del mattino*<br />
*a volte spengo anche la radio*<br />
*e lascio il mio cuore incollato al finestrino…*<br />
*([Giorgio Gaber](http://www.lameditazionecomevia.it/illogicaallegria.htm))*<!--more-->

Le vendite di iPad sono in diminuzione, un iPhone 6 Plus è grosso quasi come un iPad mini, piccolo è bello, tutto quello che si vuole: ma il ruolo di un buon iPad in auto durante un paio di migliaia di chilometri è incomparabile. Qualsiasi cosa, dalla posta ai messaggi fino all’ovvio – le mappe – e al meno ovvio, come la documentazione sui luoghi, i *social* per le condizioni del traffico, fino al vizio: la pausa in autogrill guardando gli ultimi dieci minuti della finalissima intanto che si prende il caffè.

Arrivato a destinazione, si può anche bloggare. E dormire eventualmente.

*E sto bene*<br />
*sto bene come uno quando sogna…*