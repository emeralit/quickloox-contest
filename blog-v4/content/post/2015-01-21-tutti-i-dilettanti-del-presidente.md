---
title: "Tutti i dilettanti del Presidente"
date: 2015-01-22
comments: true
tags: [Mac, Obama, Sotu, Missouri, giornalismo]
---
Quella sopra la testa di Barack Obama è la tribuna stampa. Giusto un’idea di che strumenti usino i professionisti sul lavoro, all’indirizzo di certi amministratori di sistema e di rete che si trovano nelle aziende e ancora devono scrollarsi dal cervello la polvere degli standard di fatto e delle macchine per lavorare *seriamente*.<!--more-->

<blockquote class="twitter-tweet" lang="en"><p>Wow. The press gallery over the House floor for <a href="https://twitter.com/hashtag/SOTU?src=hash">#SOTU</a> looks like a damn Apple ad. <a href="http://t.co/uubqdon5qc">pic.twitter.com/uubqdon5qc</a></p>&mdash; Michael McAuliff (@mmcauliff) <a href="https://twitter.com/mmcauliff/status/557715212858839040">January 21, 2015</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Qualcuno arriverà probabilmente dalla scuola di giornalismo del Missouri, classe 2007.

<blockquote class="twitter-tweet" lang="en"><p>.<a href="https://twitter.com/llsethj">@llsethj</a> <a href="https://twitter.com/stuherbert">@stuherbert</a> photo was taken August 27th, 2007 at the Missouri School of Journalism. <a href="https://t.co/DZy3X25CSj">https://t.co/DZy3X25CSj</a></p>&mdash; Jarvis Badgley (@ChiperSoft) <a href="https://twitter.com/ChiperSoft/status/468807026521288704">May 20, 2014</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Il commento migliore è sicuramente questo. Qualsiasi battuta riesce meglio quando sotto c’è un fondo di verità.

<blockquote class="twitter-tweet" lang="en"><p><a href="https://twitter.com/mmcauliff">@mmcauliff</a> All the PC batteries already died or rebooted.</p>&mdash; Mark Miller (@MarkDMill) <a href="https://twitter.com/MarkDMill/status/557782086174912512">January 21, 2015</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

**Aggiornamento**: abbiamo anche una foto dalla [presentazione di Windows 10](http://www.imore.com/press-windows-10-event-also-looks-apple-ad).

<blockquote class="twitter-tweet" lang="en"><p>A couple MacBooks at the Windows 10 Unveiling... <a href="http://t.co/1oA1ILW6VN">pic.twitter.com/1oA1ILW6VN</a></p>&mdash; Austen Allred (@AustenAllred) <a href="https://twitter.com/AustenAllred/status/558029756277743616">January 21, 2015</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>