---
title: "Sedici notizie nascoste"
date: 2020-06-30
comments: true
tags: [Apple, Safari, Mac, Arm]
---
Tutti dietro ai [benchmark clandestini del Developer Transition Kit](https://arstechnica.com/?p=1688140), ottenuti su un Mac che non è in vendita, su un’architettura non definitiva, su un processore che non sarà identico sui modelli in vendita, con software beta, relativi allo strato di conversione da Intel Rosetta 2. Insomma nelle condizioni ideali per non valere niente.

Lo so: nelle peggiori condizioni immaginabili, uno strato di transcodifica software batte [Surface Pro X](https://mjtsai.com/blog/2020/06/29/developer-transition-kit-benchmarks/) con codice nativo. C’è gusto.

La notizia più succosa però, è questa: Apple [rifiuta di implementare in Safari sedici interfacce,di programmazione applicative per il web](https://www.zdnet.com/article/apple-declined-to-implement-16-web-apis-in-safari-due-to-privacy-concerns/) in quanto nocive per per la privacy online e abusate dai pubblicitari.

Non avrei dubbi tra scegliere un browser che lavora per me e uno che lo fa per chi vuole profilarmi senza dirmelo. Tra parentesi, il nuovo browser di quelli di Surface Pro X parte dal codice di Chromium, che le sedici interfacce le adotta senza fare un plissé.