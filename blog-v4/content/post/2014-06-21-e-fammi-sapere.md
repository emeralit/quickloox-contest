---
title: "E fammi sapere"
date: 2014-06-22
comments: true
tags: [Pushover, Homebrew, Xquartz, shell, script]
---
Altro esempio di automazione dopo la [pubblicazione aggiornata della pagina delle letture](https://macintelligence.org/posts/2014-06-20-il-buon-pastore/), perché l’estate si avvicina e con essa qualche maggiore probabilità di tempo libero.<!--more-->

L’autore è lo stesso e stavolta racconta di [come tenere automaticamente aggiornati](http://www.tuaw.com/2014/06/11/how-to-keep-homebrew-and-xquartz-updated-automatically/) [Homebrew](http://brew.sh/) e [XQuartz](http://xquartz.macosforge.org/).

L’impresa richiede la compilazione di un file *plist* per creare un evento ricorrente e due *script* di *shell*.

Ciliegina sulla torta, un altro [*script* di *shell*](https://github.com/tjluoma/po.sh) per appoggiarsi a [Pushover](https://pushover.net) e inviare messaggi di notifica dal computer da scrivania al computer da tasca, così da sapere che tutto è andato bene oppure no senza neanche avvicinarsi al computer.

Software e tecniche piene di possibilità. Consiglio l’occhiata e anche se Homebrew non appartiene al novero degli interessi, per esempio creare eventi ricorrenti che scatenano qualche risposta da parte del software è argomento di interesse ben più comune.