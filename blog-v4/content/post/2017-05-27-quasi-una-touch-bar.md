---
title: "Quasi una Touch Bar"
date: 2017-05-27
comments: true
tags: [Touch, Bar, iPad, Editorial, Keyboard]
---
Mi succede regolarmente di lavorare con iPad e tastiera esterna e non mi ero mai reso conto che le aggiunte di [Editorial](https://itunes.apple.com/it/app/editorial/id673907758?mt=8) alla tastiera risultano molto simili come effetto a [Touch Bar](https://support.apple.com/it-it/HT207055).<!--more-->

Inutile dire che uso ben più spesso i tasti software aggiuntivi di Editorial che i tasti funzione fisici.

 ![Tastiera fisica e aggiunte logiche di Editorial](/images/editorial.jpg  "Tastiera fisica e aggiunte logiche di Editorial") 