---
title: "iPad Pro (e contro)"
date: 2019-11-30
comments: true
tags: [iPad, Gruber, Thompson, Stratechery, Giannotta, Viticci, MacStories, LC, Performa, Photoshop, PageMaker]
---
Il bilancio dei primi dieci anni di vita di iPad è stato il più polarizzante che abbia visto.

John Gruber è un _contro_. Riconosce l’eccellenza dell’hardware ma è deluso dal software e [ritiene che iPad sia portato da Apple verso un vicolo cieco](https://daringfireball.net/2020/01/the_ipad_awkwardly_turns_10).

>iPad dieci anni dopo, per me, è fonte di grave disappunto. Non perché sia “cattivo”, in quanto non è cattivo — è persino grande — ma perché nonostante sia grande in molti modi, complessivamente ha mancato di esprimere il notevole potenziale che ha mostrato il primo giorno. Per raggiungere questo potenziale, Apple deve riconoscere di avere commesso profondi errori concettuali nell’interfaccia utente di iPad, errori che hanno bisogno di essere eliminati e sostituiti, non levigati e rifiniti.

Per Ben Thompson a *Stratechery*, iPad non ha fatto nascere un ecosistema di applicazioni che facessero decollare la piattaforma, come Photoshop o PageMaker hanno fatto per Mac. Le [sue conclusioni](https://stratechery.com/2020/the-ipad-at-10-the-ipad-disappointment-ipads-missing-ecosystem/) sono simili a quelle di Gruber:

>A essere onesti, vorremmo tutti “fallire” come iPad; un affare da ventuno miliardi di dollari l’anno scorso, poco meno dei ventisei miliardi di Mac. […] La tragedia di iPad non è essere stato un flop; è che non ha mai raggiunto, né probabilmente raggiungerà mai quel potenziale visto così chiaramente dieci anni fa.

Pù modestamente sono allineato con [Roberto Giannotta](https://www.facebook.com/robegian/posts/10218816638320105): *Quando Steve Jobs ha presentato l'iPad, 10 anni fa, mi sono entusiasmato immaginando tutto ciò che avrei potuto farci… e che poi in effetti ho fatto.*

<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Frobegian%2Fposts%2F10218816638320105&width=500" width="500" height="637" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>

La figura di Federico Viticci è centrale per giudicare correttamente iPad. *MacStories* ha descritto l’evoluzione di iPad negli anni e soprattutto i modi per [fare ciò che secondo l’opinione comune non poteva essere fatto](https://www.macstories.net/ipad/).

È indubitabile che certe operazioni siano meno immediate o più macchinose in iPad che su Mac. Altrettanto indubitabile è che altre operazioni sono molto più immediate e qui forse sta un errore di valutazione di Gruber: iPad non è Mac 2.0. È un altro modo di intendere il computing, a volte più sfidante, come mostra benissimo [Dr. Drang](https://leancrew.com/all-this/). Mi piace inquadrare questa situazione come altre della mia vita: vero che forse costa più impegno, ma le soddisfazioni sono maggiori.

Chiudo con un’ultima considerazione: il decimo compleanno di Mac cadeva nel 1994, non esattamente un momento felice per la linea di prodotto. I progressi che ha compiuto Mac nei venticinque anni successivi sono stati immensi rispetto alle macchine di quel tempo. Confrontiamo iPad oggi con un Mac che ha avuto *un quarto di secolo* di vantaggio per crescere ed evolversi. Se il confronto avvenisse con un [Macintosh LC o un Performa](https://everymac.com/systems/by_year/macs-released-in-1994.html), forse il giudizio dovrebbe essere diverso.