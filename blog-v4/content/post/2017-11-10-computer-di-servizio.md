---
title: "Computer di servizio"
date: 2017-11-10
comments: true
tags: [Mac, iPad, Pro, rekordbox]
---
Il cliente inaugura una prestigiosa nuova sede e indice una grande festa per la giornata di inaugurazione.

Ovviamente nello spazio eventi si trova un banco regia.

 ![Banco regia nello spazio eventi](/images/banco-regia.jpg  "Banco regia nello spazio eventi") 

Sono riuscito a sbirciare nel Mac del deejay. Usa [rekordbox](https://rekordbox.com/en/), che se non erro è venduto in abbonamento a dieci euro abbondanti per mese.

 ![rekordbox sul Mac del deejay](/images/rekordbox.jpg  "rekordbox sul Mac del deejay") 

Durante la giornata un’azienda specializzata in materia ha tenuto dimostrazioni di *coding* con kit [SAM Labs](https://www.samlabs.com).

 ![Coding e Mac](/images/coding.jpg  "Coding e Mac") 

A fine giornata ci sono state alcune premiazioni. La scaletta dell’evento finale è stata vergata a mano su un iPad Pro, a disposizione del cerimoniere. Da notare che riconoscerla come scrittura digitale non è automatico… a togliere la cornice del computer, si potrebbe venderlo come foglio di carta a pennarello.

 ![Scaletta su un iPad Pro](/images/ipad-pro.jpg  "Scaletta su un iPad Pro") 