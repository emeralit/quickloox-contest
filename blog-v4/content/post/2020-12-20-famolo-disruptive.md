---
title: "Famolo disruptive"
date: 2020-12-20
comments: true
tags: [Dediu, Apple, DX•Mba]
---
Una [lunga conversazione](http://www.dx.mba/blog/:is-apple-disruptive-long-lens-apple-and-disruption) di Horace Dediu sul tema di se e come Apple sia *disruptive*.

Per niente noiosa come direbbe il titolo e invece ben descrittiva della diversità di Apple:

>Apple ha svolto un lavoro incredibile nel fare progredire il computing personale e mobile. Hanno portato alcuni dei migliori prodotti al mondo a miliardi di consumatori soddisfatti. Sono entrati metodicamente in nuovi mercati e affrontato concorrenza ricca, consolidata e orgogliosa. Lo hanno fatto intanto che guadagnavano quota di mercato, miglioravano i prodotti e aumentavano i margini.

Il tema su cui soffermarsi è che Apple, che pure si presenta come azienda assai *disruptive* (dirompente), può essere vista sia conforme alla teoria della disruption sia in contraddizione con essa. Proprio perché apre strade nuove senza tuttavia sacrificare guadagni.

Un iPhone o un watch non vengono dal nulla o come un lampo nella testa dell’amministratore delegato. Anche dove c’è un’idea fulminante, questa poggia su anni di ricerche, lavoro, fallimenti, esperimenti. E sono pochi i posti nei quali idee dirompenti hanno possibilità di vincere la diffidenza e affermarsi.