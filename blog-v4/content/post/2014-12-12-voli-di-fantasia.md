---
title: "Voli di fantasia"
date: 2014-12-12
comments: true
tags: [United, iPhone6Plus]
---
United Airlines [distribuirà 23 mila iPhone 6 Plus ad altrettanti assistenti di volo](http://macdailynews.com/2014/12/10/united-airlines-to-deploy-23000-apple-iphone-6-plus-units-to-flight-attendants/) sui propri velivoli da qui alla metà del 2015.

Ma non si piegavano? Come fusoliere, forse.