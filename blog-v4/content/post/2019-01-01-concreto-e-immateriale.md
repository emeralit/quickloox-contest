---
title: "Concreto e immateriale"
date: 2019-01-01
comments: true
tags: [Mac, Macincloud]
---
Come viatico per il nuovo anno scelgo [Macincloud](https://www.macincloud.com), un Mac a disposizione nella Nuvola e anche con prezzi accessibili; esiste persino il piano a consumo, un dollaro l’ora con prepagate da trenta dollari.

Come idea mi ricorda la scelta di molti milanesi che hanno rinunciato all’auto di proprietà perché costa loro molto meno chiamare un taxi quando serve, o usare i servizi di condivisione. Come idea la capisco perfettamente, appena consigliata a una cara amica che purtroppo per lei usa Windows e vorrebbe pubblicare autonomamente un eBook su iTunes. Se deve fare solo quello, con pochi dollari può portare a termine il compito – che richiede un Mac – e ha risolto. Scommetto anche che, sbrigata la pratica e toccato con mano, le verrà più voglia di avere un Mac in casa invece che online.

Un Mac nel cloud sono due estremi che si toccano, è un’idea che attraversa l’intero piano dell’usabilità degli strumenti informatici da quelli più tradizionali a quelli più immateriali. È un paradosso che allarga la mente e incoraggia a pensare in modo creativo.

Personalmente preferisco ancora avere un Mac in casa, saldamente piazzato sulla scrivania. Ma quando sarò dall’altra parte del mondo con il mio iPad, magari mi verrà voglia di fruire per pochi istanti di un Mac. E sarà meglio puntare sul Mac fisico a casa oppure averne uno online, magari con il server a due passi da dove mi trovo?

Pochi modi di iniziare l’anno sono migliori del porsi domande intriganti. Felice 2019 all’insegna di Mac, comunque lo si desideri.
