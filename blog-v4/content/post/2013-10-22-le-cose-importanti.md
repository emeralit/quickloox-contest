---
title: "Le cose importanti"
date: 2013-10-22
comments: true
tags: [iPad, OSX]
---
Non riuscirò a essere *online* durante l’evento di presentazione dei nuovi iPad e forse di molte altre cose. In serata comparirò sul solito canale `#freesmug` del server `irc.freenode.net`, dando il comando `join #freesmug` dentro un programma atto allo scopo, su uno dei numerosi computer a disposizione. Con tanti ringraziamenti a [Gand](http://freesmug.org) e ancora una volta con l’invito a sostenere il software libero.<!--more-->

Tanti auguri a tutti di godersi l’evento senza essere sommersi dalle chiacchiere degli spazzaturai.

Visto che bisogna liberarsi la testa per pensare leggero e che il codice è un ottimo modo di raggiungere stati di coscienza superiori, ecco un bel comando di Terminale per elencare tutte le applicazioni scaricate da Mac App Store, [cortesia di OS X Daily](http://osxdaily.com/2013/09/28/list-mac-app-store-apps-terminal/), utile in situazioni particolari – per esempio se il Mac è remoto – ma ottimo comunque per sapere una cosa in più. Non fa mai male, metti che da domani la mettiamo in pratica su un iPad tutto nuovo.