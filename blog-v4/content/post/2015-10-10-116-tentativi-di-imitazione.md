---
title: "116 tentativi di imitazione"
date: 2015-10-10
comments: true
tags: [Dilger, Microsoft, Apple, Store]
---
Un [articolo di Daniel Eral Dilger su Apple Insider](http://appleinsider.com/articles/15/10/06/microsofts-retail-store-chain-flounders-in-stark-contrast-to-busy-apple-stores) fa il punto sul confronto tra gli Apple Store e i Microsoft Store.<!--more-->

I primi sono 460, i secondi 116. I secondi sono partiti circa otto anni dopo i primi (e dopo che i primi erano stati prefigurati [come un fallimento](https://macintelligence.org/posts/2014-05-20-sessanta-giorni-fine-mese/)).

I primi sono pieni di folla, i secondi se li fila nessuno o quasi. L’articolo di Dilger è ovviamente esemplificativo e non contiene le foto che confrontano seicento punti vendita. È possibile scattare una foto in un momento fortunato o poco propizio eccetera. Le foto, tuttavia, fanno pensare.

E c’è comunque qualcosa di strano. I primi negozi che si aprono sono quelli più ovvi e profittevoli: si va prima dove è chiaro che si avrà successo. Poi, con il crescere della catena, si riempiono nicchie sempre più piccole. Il gioco ha rendimento decrescente con il crescere dei numeri e il quattrocentosessantunesimo Apple Store non ha speranze di uguagliare il primo. Ci sono eccezioni, c’è la Cina, ma il commercio al dettaglio funziona così.

I negozi Microsoft dovrebbero funzionare mediamente meglio di quelli Apple, proprio perché questi ultimi sono quattro volte tanti. O almeno fare all’incirca lo stesso, dato che sovente sono costruiti negli stessi posti, per risparmiare sulla ricerca della *location* che è una disciplina complicata.

Invece sono ridicolmente dietro. Gli Apple Store sono considerati un costo di marketing e comunicazione; il loro primo interesse – non l’unico, ci mancherebbe – è presentare una immagine di prodotto e fare vivere una esperienza, non vendere. Non fosse così, fossero pensati unicamente come negozi, con un bilancio proprio, probabilmente sarebbero in perdita.

E sono i migliori. Quei centosedici negozi devono costare a Microsoft un’iradiddio di soldi, per rendere niente.

Affidarsi a una azienda di imitatori, che non lo sanno neanche fare bene? Che buttano i soldi dalla finestra invece che investirli sui prodotti? Boh. Il mondo è veramente strano.