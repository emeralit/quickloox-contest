---
title: "Il difetto sta nel maniaco"
date: 2016-03-21
comments: true
tags: [iTunes, malware, Windows]
---
Si segnala un [malware](http://researchcenter.paloaltonetworks.com/2016/03/acedeceiver-first-ios-trojan-exploiting-apple-drm-design-flaws-to-infect-any-ios-device/) in grado di infettare apparecchi iOS genuini, non sottoposti a *jailbreak* e privi di certificati di autenticazione aziendali (che a volte sono vulnerabili ad abusi).<!--more-->

Ohibò. È tutta la sicurezza di App Store è andata a farsi benedire? Ma no. Semplicemente, il *malware* funziona se il computer del possessore dell’aggeggio iOS  monta un software che si intromette tra iTunes e App Store con il pretesto di lavorare meglio di iTunes.

Manco a dirlo, il computer compromesso può essere solo Windows. (Può essere solo cinese al momento, ma questa è altra questione).

Insomma, un maniaco dell’imprudenza, ansioso di cacciarsi nei guai, può riuscirci. E grazie tante.
