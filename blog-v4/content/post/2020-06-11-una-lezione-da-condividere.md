---
title: "Una lezione da condividere"
date: 2020-06-11
comments: true
tags: [iTunesU, iBooks, Author, Classroom]
---
Nel 2012 Apple diede scandalo: mostrò con iBooks Author che un docente poteva ambire a creare contenuti di qualità per le proprie classi. Per chi era abituato a parcellizzare il libro di testo ufficiale, _oggi ragazzi si va da pagina mille a pagina millequindici_, deve essere stato uno shock culturale: essere considerato un soggetto attivo e consapevole, invece che un ripetitore. Si creava anche un problema motivazionale, dato che per essere attivi bisogna _volerlo_.

Un altro scandalo fu iTunes U: l'idea che una istituzione scolastica potesse organizzare contenuti didattici per l'esterno. Non solo per gli studenti fuori dall'aula. Per _chiunque_. L'idea del sapere come una risorsa e della scuola come un centro di apprendimento utile all'intera comunità.

Ci sono molti modi di dare scandalo. Tipicamente Apple anticipa tendenze che la maggioranza trova oltraggiose, salvo nuotarci dentro cinque o dieci anni dopo. Oggi le piattaforme di apprendimento online distribuiscono sapere a una platea planetaria di dimensioni enormi.

Siamo alla vigilia di un ripensamento radicale del ruolo della scuola e di come lo debba esercitare, come, per chi. Che succede se un liceo prestigioso decide domani di accettare iscrizioni remote di gente dall'altra parte del mondo? Che accade se una università di eccellenza porta online un corso di laurea aperto a centomila studenti?

Sembra fantascienza? È solo questione di raffinamento di strumenti e risorse che già esistono. Le scuole che non si sono ancora poste la questione rischiano che sia la questione a occuparsi di loro anche senza permesso. Che succederebbe se – pura fantasia, ovvio – una del tutto ipotetica emergenza costringesse le scuole a fare lezione solo online? Improvvisamente la scala dei valori cambierebbe e a farne le spese sarebbero soprattutto le rendite di posizione. Deve ancora succedere, nessuno oggi si sogna di mettere in discussione le aule; ma se succedesse, diventerebbe molto chiaro che oggi l'insegnamento deve per forza contenere una componente online.

Niente di strano che, anni dopo, Apple termini l'esperienza di iTunes U e iBooks Author per passare ad altro. [I libri di iBooks Author potranno essere importati in Pages](https://support.apple.com/en-us/HT211136); oggi, che un docente prepari lavoro per la classe, deve essere ordinaria amministrazione. [iTunes U migra nei Podcast](https://www.macstories.net/news/itunes-u-collections-are-moving-to-apple-podcasts/): i modi per fare lezione in differita e in remoto ormai sono millanta, ascoltarsi il docente in cuffia è una cosa più che normale e merita strumenti normali.

Apple ha messo a punto altri strumenti, mirati maggiormente al complesso del lavoro di classe è meno alla singola fatica dell'insegnante. Perché, dato per scontato il preparare la lezione come si deve, oggi la parte non scontata è erogarla efficacemente. Catturare l'attenzione. Raggiungere gli studenti sul canale più adatto per loro, più che obbligarli alla lezione frontale di una volta.

Cinque o dieci anni da oggi e sembrerà la cosa più ovvia del mondo.

Certo, oggi può dare scandalo. E l'idea che le lezioni possano avvenire fuori dall'aula, che assurdità! Perché mai dovrebbe accadere?