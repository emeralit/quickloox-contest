---
title: "Polli di batteria"
date: 2014-02-26
comments: true
tags: [iPad, Galaxy, batteria, Mossberg, Re/code]
---
Si [scriveva di batterie](https://macintelligence.org/posts/2014-02-12-acquisti-che-durano/) poco tempo fa. Walt Mossberg è passato dallo stesso argomento – dobbiamo migliorare la coordinazione – su *Re/code*, durante la [recensione](http://recode.net/2014/02/25/new-samsung-tablet-takes-on-the-ipad-mini/) del nuovo Galaxy Tab Pro 8.4.<!--more-->

>Per il test [di autonomia] imposto la luminosità dello schermo al 75 percento, spengo le funzioni di risparmio energetico, lascio acceso il Wi-Fi per ritirare la posta automaticamente e riproduco video fino a che la tavoletta muore. Nel test, Galaxy Tab Pro 8.4 è durato cinque patetiche ore e undici minuti, meno della metà rispetto alle undici ore e diciassette minuti di iPad mini.

Naturalmente il nuovo Galaxy Tab 8.4 costerà qualcosa meno e si troveranno allora legioni di polli a garantire che sì, fa proprio tutto quello che fa iPad.