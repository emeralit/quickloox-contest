---
title: "I Mac mancanti"
date: 2021-06-14T15:12:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Macworld, Jason Snell, Mac, Swift Playgrounds, watch, Object Capture, Ford, Walkman, Sony, Model T, Big Mac] 
---
Dopo che è trascorso qualche anno dal 1984, ho compreso che Mac, come tutti i grandi marchi che finiscono per diventare una sineddoche della propria categoria di prodotti, rappresenta molto più del computer iconico di Apple.

Almeno per me.

Quando dico _Mac_, penso a _un insieme di hardware e software che rivoluziona un’attività umana rendendola incredibilmente semplice e accessibile rispetto a prima_.

È successo così con il computing, naturalmente. E si può proseguire a definire Mac anche per il suo opposto: le attività dove manca un Mac e così restano esoteriche, di pochi, difficili.

Per esempio: le stampanti 3D. Se ne faceva un gran parlare, certamente ne trovi una in ogni Mediaworld. Una. Sono apparecchi straordinari, solo che se ne compro una, invece che risolvermi un problema, inizio a pormene molti. Dove trovo le cose da stampare. Come faccio a creare cose originali che servono a me. Come faccio a modificare qualcosa. Eccetera.

Con il primo Mac, c’era MacPaint. Disegnare sul computer, prima, era un esercizio masochista o costava un capitale in accessori. Con MacPaint, iniziavi a disegnare. Nessuno toglieva di mezzo il percorso per arrivare a disegnare capolavori, ma disegnava anche mia nonna, con MacPaint. Al settore della stampa 3D serve un Mac. O rimarrà una faccenda per pochi intimi e appassionati.

La realtà virtuale? I caschi datano a venticinque anni fa. Si fanno continui progressi, si va sempre avanti, i prezzi calano… dov’è però il Mac della realtà virtuale, quello che me la rende pronta, accessibile, godibile subito, senza fare fatica? Dove comincio in modo semplice, subito? Ecco perché sono passati venticinque anni e la realtà virtuale è una cosa di nicchia.

Con watch, Apple ha creato un Mac. Mica per leggere l’ora; per monitorare parametri vitali, per esempio. Puoi avere uno storico della frequenza cardiaca lungo a piacere. Prima era complesso e costoso.

È importante che Apple crei Mac non solo per le persone, ma anche per gli sviluppatori. Più diventa facile e accessibile produrre buon software, più viene fatto. Jason Snell ha accennato su _Macworld_ a [tecnologie emerse a WWDC per facilitare lo sviluppo](https://www.macworld.com/article/348541/wwdc-developer-tools-object-capture-matter-smart-home-swift-playgrounds-xcode-cloud.html), come Object Capture per chi lavora alla realtà aumentata e vuole creare rapidamente oggetti tridimensionali, o naturalmente Swift Playgrounds, che non sostituisce sicuramente Xcode su Mac ma è un primo passo nel consentire lo sviluppo su iPad di app per iPad.

Di Mac nella storia ne sono stati creati molti, naturalmente, senza sapere che fossero Mac. Il Modello T di Ford era un Mac; il Walkman di Sony era un Mac; il televisore era un Mac. Era un Mac persino il Big Mac.

Interessante come, nella nostra epoca, sembra che l’unica rimasta a creare Mac di un qualche livello sia proprio Apple (Con l’eccezione dei vaccini e magari di Tesla, per le maxibatterie). Eppure ne servirebbero numerosi.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*