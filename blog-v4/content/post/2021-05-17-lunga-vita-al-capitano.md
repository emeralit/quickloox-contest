---
title: "Lunga vita al capitano"
date: 2021-05-17T00:53:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Cap’n Magneto, Al Evans, Kagi, Carbon, Macintosh, Mac OS X] 
---
Al Evans, diciassettenne di Austin, [era sopravvissuto a un pauroso incidente di auto](https://eu.statesman.com/in-depth/news/2021/05/13/al-evans-capn-magneto-videogame-creator-persevered-after-injury/7185492002/) che lo aveva lasciato con ustioni gravi sul settanta percento del corpo.

I medici gli davano scarse probabilità di arrivare vivo ai trent’anni ed Evans decise di fare qualcosa per lasciare un segno: programmare un videogioco.

Fu così che, nel 1985, uscì [Cap’n Magneto per Macintosh](http://www.capnmagneto.com/index.html). Un gioco innovativo su molti versanti, per la trama, le modalità di gioco, la grafica, i controlli. Senza un Macintosh non avrebbe potuto essere quello che è stato.

*Avanti veloce* al 2020, quando un messaggio su Facebook lamenta l’impossibilità di… vincere *Cap’n Magneto*.

Il gioco era uscito come *shareware* e conteneva un meccanismo che permetteva di completarne l’esperienza solo a chi lo aveva pagato. Il pagamento era curato da Kagi, al tempo una istituzione in materia. Kagi [ha chiuso i battenti nel 2016](https://www.macrumors.com/2016/08/01/kagi-shuts-down/) e da allora è stato impossibile pagare la somma necessaria a sbloccare il gioco. Che però godeva ancora di almeno un estimatore.

Sulla pagina di *Cap’n Magneto* è stato allora aggiunto un account da usare per sbloccare il gioco e portarlo a termine, gratuitamente. Da Al Evans in persona, ancora vivo a dispetto delle previsioni dei dottori e nella terza età di una vita piena di interessi e passioni, una soddisfacente carriera di piccolo imprenditore, un matrimonio duraturo.

*Cap’n Magneto* ha visto una versione Carbon che lo ha fatto arrivare funzionante su Mac OS X, ma per funzionare sui sistemi odierni ha bisogno di una emulazione.

Non possiamo sapere se *Cap’n Magneto* continuerà a vivere anche oltre il suo autore; di certo averli entrambi ancora vivi, dopo tanti anni, scalda il cuore e racconta una storia commovente di programmazione che aiuta a superare una situazione difficile. Lunga vita al capitano.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               