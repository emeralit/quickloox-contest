---
title: "È semplice"
date: 2023-01-14T01:34:34+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Tesler, Larry Tesler, Rosa, Marek Rosa]
---
Una app che costringa a faticare per ottenere il risultato è una app i cui sviluppatori non hanno compreso come si deve maneggiare la complessità.

Esiste in ogni app [una quantità irriducibile di complessità](https://twitter.com/marek_rosa/status/1614001861635330061) e, più se ne fa carico lo sviluppatore, più verrà apprezzata e sarà produttiva per l’utente.

Se invece lo sviluppatore scarica la complessità sull’utente, sarà pianto e stridore di denti.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Law of Conservation of Complexity:<br><br>“Every application has an inherent amount of irreducible complexity. The only question is: Who will have to deal with it - the user, or the designer?” - Larry Tesler<a href="https://t.co/e9CJDfWD4q">https://t.co/e9CJDfWD4q</a><br><br>How do you deal with it? <a href="https://t.co/OTQWGz2nFI">pic.twitter.com/OTQWGz2nFI</a></p>&mdash; Marek Rosa (@marek_rosa) <a href="https://twitter.com/marek_rosa/status/1614001861635330061?ref_src=twsrc%5Etfw">January 13, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

(Larry Tesler è stato una figura di spicco nella Apple del primo periodo di Steve Jobs).