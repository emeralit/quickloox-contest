---
title: "False equivalenze"
date: 2015-05-19
comments: true
tags: [AppleWriter, Aperture, ClarisWorks, AppleWorks, Pages, Adobe, Apple, Windows, Word, Martin, Clarke]
---
Siamo nel 2015 ma ancora gira l’idea dei professionisti e di Apple che li abbandona, con dimostrazioni come [quella fornita ieri](https://macintelligence.org/posts/2015-05-18-prendere-nota/). Ma il tema solleva ancora discussione.<!--more-->

<div id="fb-root"></div><script>(function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0];  if (d.getElementById(id)) return;  js = d.createElement(s); js.id = id;  js.src = "//connect.facebook.net/it_IT/sdk.js#xfbml=1&version=v2.3";  fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script><div class="fb-post" data-href="https://www.facebook.com/lucio.bragagnolo/posts/10153884205509097" data-width="500"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/lucio.bragagnolo/posts/10153884205509097">Posted by <a href="https://www.facebook.com/lucio.bragagnolo">Lucio Bragagnolo</a> on <a href="https://www.facebook.com/lucio.bragagnolo/posts/10153884205509097">Lunedì 18 maggio 2015</a></blockquote></div></div>

Si capisce che viene facile associare il supporto dei professionisti con la presenza di una applicazione fatta apposta per loro e che allora la dismissione di Aperture, per esempio, significhi un abbandono dei fotografi (o forse dei collezionisti di fotografie). È che la logica, come dire, rema contro.

Tanto per cominciare, si vede nei commenti l’ironia di chi ricorda i tempi in cui il Mac era *buono solo per la grafica*. Vero: non c’era grafico impaginatore degno del suo nome che non usasse Mac, magari un’isola in mezzo a paludi di Windows. Eppure *Apple non aveva una applicazione professionale per gli impaginatori*. Non l’ha mai avuta. Predico che non ce l’avrà mai.

Non è che Apple non considerasse i professionisti del video prima che esistesse Final Cut. E Final Cut è nato per offrire un’alternativa, dal momento che *Adobe stava abbandonando i professionisti* con la decisione di privilegiare lo sviluppo su PC.

Non so se io sono un professionista di qualcosa. Certo è che scrivo molto e mi servono i migliori strumenti per la scrittura disponibili. Apple ha svolto questo ruolo quando su Apple //e funzionava [Apple Writer](http://apple2history.org/history/ah18/#08). Poi con Macintosh la faccenda si è subito deteriorata. [MacWrite](http://www.mac512.com/macwebpages/macwrite.htm) era bellissimo, ma tutto era tranne che professionale ([WordStar](http://www.wordstar.org) era professionale; mi si perdoni l’autocitazione, ma [lo usava Arthur Clarke](http://www.apogeonline.com/webzine/2014/05/26/non-basta-la-parola) e lo usa *ancora oggi* George Martin). Poi sono arrivati [ClarisWorks](http://groups.csail.mit.edu/mac/users/bob/clarisworks.php) e successivamente [AppleWorks](http://it.wikipedia.org/wiki/AppleWorks). Usati anche per lavoro, ma garantisco che i software professionali per scrivere sono stati sempre ben altri. Per qualcuno, figuriamoci, il software professionale era Word.

Oggi abbiamo Pages che non sto neanche a linkare. Non è software professionale per scrivere, anche se lo sto usando ora per lavoro.

L’equivalenza *professionale = applicazione professionale* viene facile, ma è finta. Professionale è quando hai la piattaforma che ti consente di realizzare il tuo lavoro con la massima resa. Potrebbe essere anche un iPod touch con Note, nella giusta situazione.

E allora, perché Aperture viene dismesso? Perché a un certo punto il software termina la propria esistenza, prima o dopo. Apple non è mai stata una azienda che esita a lasciare il passato dietro di sé. Per rimanere all’infinito a usare i programmi che abbiamo sempre usato, bisogna rivolgersi ad altri. Ma per qualcuno dovremmo usare ancora Apple Writer. Non è il caso.