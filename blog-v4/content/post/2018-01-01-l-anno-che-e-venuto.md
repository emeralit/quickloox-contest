---
title: "L’anno che è venuto"
date: 2018-01-01
comments: true
tags: [Alfred, Hazel, Maestro, MacDrifter]
---
È dell’anno scorso, questo [elenco di applicazioni Mac preferite per il 2017](http://www.macdrifter.com/2017/12/some-of-my-favorite-mac-applications-in-2017.html) apparso su *MacDrifter*.

Eppure la direzione che deve prendere il 2018 è chiara e ben tracciata: un terzo delle *nomination* riguardano programmi che automatizzano funzioni, personalizzano comandi o creano flussi di lavoro.<!--more-->

Ogni giorno di quest’anno devo chiedermi se io stia svolgendo un lavoro ripetitivo. E ogni sera, chiedermi come lasciare che a farlo sia il computer.

Non è bello distribuire impegni invece che fare auguri, ma ci provo lo stesso. Affido lo stesso impegno a ciascuno. Sarebbe bello condividere i risultati e arrivare tra dodici mesi ad avere la vita, il lavoro, il tempo libero assai meno ripetitivi di ora.