---
title: "Un programma bacato"
date: 2019-11-24
comments: true
tags: [Excel, PowerPoint, Word, LibreOffice, Ecdl]
---
A settembre la primogenita entrerà nella scuola primaria e in questi giorni siamo nel vortice delle presentazioni degli istituti, che parlano pubblicamente del proprio piano formativo, aprono le porte ai visitatori per un giorno e in generale cercano di convincere i genitori a farsi scegliere. Diffidente e scettico come sono verso la scuola attuale, ammetto che l’esperienza vissuta finora ha superato le mie (scarse) aspettative. Va meglio, o meno peggio, di quello che credevo.

Anche se, come inevitabile, si è anche aperto il capitolo coding e tecnologie. Uno degli istituti che preferiamo si picca di insegnare, nel terzo, quarto e quinto anno… Word, PowerPoint e Excel, secondo i dettami della famigerata Ecdl, la patente europea del computer nata per fare guadagnare Microsoft e creare analfabeti informatici funzionali.

Me lo aspettavo e non è che le alternative a nostra disposizione brillino. Tuttavia si sta parlando di mia figlia, per la quale il terzo anno di primaria è il 2022/2023. Che nel 2023 si creda di insegnare informatica insegnando Word è la prova provata di un sistema pieno di persone di buona volontà, docenti volonterosi e preparati, voglia di ottenere comunque buoni risultati nonostante burocrazie e arretratezze, e ciononostante bacato per la sua incapacità di guardare oltre il passato.

Ho davanti anni di assemblee di istituto, consigli di classe, colloqui periodici. Dovrò imparare a contenermi.
