---
title: "Ipertestare la scuola"
date: 2021-08-11T01:14:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Scuola, curricolo, Zork, Fortnite, Office, Adventure, Html, LaTeX, wiki, Markdown] 
---
Il primo punto all’ordine del giorno per realizzare un [curricolo informatico per la scuola primaria](https://macintelligence.org/posts/Un-curricolo-per-lestate.html) è quello ipertestuale.

Pensiamoci bene: quanto di quello che scrive un bambino di nove anni per la scuola ha forma ipertestuale? Probabilmente niente, o poco. Quanto sarà ipertestuale ciò che scriverà nella sua vita al termine della scuola dell’obbligo? Moltissimo. Userà computer da scrivania, da tasca, a tavoletta, a muro, nei vestiti, forse anche nel corpo. Userà un sacco di ipertesto. Anche senza saperlo; condividerà infiniti testi, foto, video e che cos’è una condivisione, se non l’invio o la ricezione di un ipertesto?

Fin qui sarebbe niente; ma il punto è che la nostra civiltà ha cominciato anche a *pensare* in modo ipertestuale. I siti non sono scritti come capita per poi aggiungere i link in un secondo giro; l’informazione viene strutturata da subito pensando alla forma ipertestuale. Qualsiasi gioco dove ci si sposti per locazioni, da [Adventure](https://www.historyofinformation.com/detail.php?id=2020) a [Fortnite](https://www.epicgames.com/fortnite/en-US/home), sotto sotto nasconde una struttura ipertestuale. I [librogame](http://www.librogame.net) sono ipertesti con una struttura ad albero molto semplice e a portata di lavoro di gruppo (*spoiler*).

È *necessario* che i bambini di oggi vivano a scuola un approccio all’ipertesto, ovviamente nella misura giusta per la loro età.

Naturalmente, mettersi semplicemente a linkare qua e là non è utile. A scuola si impara come funzionano gli strumenti, ma deve arrivare dopo. Prima deve arrivare la conoscenza di come funziona. Un *word processor* non è una materia da imparare, ma da usare per imparare a scrivere e ad acquisire padronanza dell’italiano. Non ci servono specialisti di Word, ma cittadini che usano splendidamente l’italiano. E sia capace di applicare ipertesto a questa conoscenza.

Per questo, vecchia proposta che porto avanti da anni, a scuola non bisogna scrivere come .doc, ma come .html. Quando i ragazzi producono qualcosa, lo fanno in Html. Diciamo dalla quinta elementare in poi? Prima imparano con gradualità che cosa significa *linguaggio di marcatura* (metto simboli attorno a quello che scrivo e ne influenzo l’aspetto e la struttura) e iniziano ad applicarsi nella scoperta dell’Html.

Questo ha vari vantaggi. Gli studenti capiscono che cosa c’è dietro le pagine HTML e imparano a padroneggiare la prima fase di quello che è costruire un sito, una base di conoscenze, un ipertesto appunto. Word è il peggiore strumento possibile a questo stadio, perché nasconde la conoscenza del fenomeno. Si clicca un’icona, la selezione diventa grassetto o corsivo, ma non si capisce il *perché*. È dal perché invece che si parte, dall’alfabeto dell’ipertesto, appunto la parte base di Html. Paragrafi, attributi del testo, liste, tabelle, gerarchia dei paragrafi stessi, naturalmente link.

Viene da sé che lo strumento ideale per questo processo di apprendimento sia l’editor di testo, non certo il word processor. Lo studente impara a scrivere in testo puro, che è il formato più semplice al mondo, quello più resistente negli anni, più facilmente aggiornabile, compatibile con l’universo. E impara a vestire il testo con Html, per dargli l’aspetto e la forma che desidera, imparando il ruolo del browser. Se le scuole avessero iniziato a produrre materiale in Html vent’anni fa, tutto il materiale prodotto negli ultimi vent’anni sarebbe perfettamente leggibile, duplicabile, aggiornabile con facilità, consultabile, riorganizzabile, riutilizzabile, tutto.

Nella fase iniziale dell’apprendimento, praticamente neanche serve un editor di testo, vero e proprio, basta qualcosa dove si possa scrivere. I primi tag verranno digitati carattere per carattere, si imparerà *minore-tag-maggiore* e poi *minore-slash-tag-maggiore*. Solo più avanti si mostrerà che esiste la possibilità di affidare la stesura dei tag all’editor. (E probabilmente vedremo modi alternativi per semplificare la generazione dell’Html, come [Markdown](https://daringfireball.net/projects/markdown/) e i sistemi [wiki](https://cft.vanderbilt.edu/guides-sub-pages/wikis/)).

Domani si prosegue con l’individuazione di qualche editor online e offline buono per la bisogna e con la prosecuzione del cammino, che più avanti porterà i più grandi a misurarsi con un linguaggio di marcatura ancora più ambizioso, [LaTeX](https://www.latex-project.org), e con la scoperta della tipografia. C’è anche da pensare all’inserimento di esperienze ed esercizi in modo trasversale dentro le materie tradizionali. C’è un sacco di roba da fare e si capisce meglio perché a scuola usare Office è sbagliato. Con Office si produce, a scuola si deve imparare come sono fatte le cose. Tecnologia compresa.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               