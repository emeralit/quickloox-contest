---
title: "Automazione, televisione, programmazione"
date: 2017-07-06
comments: true
tags: [Windows, automazione, televisione]
---
A pranzo per intervistare fondatore e Chief Technical Officer di un gruppo milanese immerso nella tecnologia fino alla punta dei capelli, con clienti come Unione Europea ed emittenti televisive internazionali. Ho il registratore acceso e lui lo sa. A un certo punto dice:

>L’automazione su Windows è come la programmazione del televisore. Il televisore lo stai a guardare.

Detto da uno che lavora quotidianamente all’automazione profonda della sua azienda e vende il servizio a quelle altrui.