---
title: "Sotto la doccia"
date: 2015-08-10
comments: true
tags: [Clash, freemium, Nyt, WoW]
---
È di questi periodi che può venire in mente a un amico di decidere di fondare un clan in [Clash of Clans](https://itunes.apple.com/it/app/clash-of-clans/id529479190?mt=8).<!--more-->

Non amo eccessivamente i giochi strategici con gestione di risorse e specialmente se sono *freemium* da scaricare gratis e dove tutto è gratis, ma si può pagare per crescere più in fretta. A mio personalissimo giudizio, vale la pena di investire denaro in un *freemium* solo se scarseggia la pazienza. Altrimenti, visto che il bello dovrebbe essere giocare, si fa carriera più lentamente e con più pazienza, di fatto giocando a spese di chi decide, di fatto, di finanziare anche il gioco altrui. Assolutamente niente di male nel pagare in un *freemium* e lo dice uno che per anni ha detenuto un abbonamento mensile a [World of Warcraft](http://eu.battle.net/wow/it/?-) e sogna di poter riavere tempo sufficiente per riaccenderlo. Probabilmente ho speso nel tempo più di molti appassionati compratori di sacchi di gemme (la moneta corrente di Clash of Clans).

Ho quindi iniziato ad applicarmi più del solito. Di solito testo un *freemium* interessante e, dopo avere colto gli elementi essenziali, lo lascio lì per altro. Adesso sto invece superando i primissimi livelli per arrivare a poter appartenere a clan. Nel fare questo, esperto di nulla e principiante massimo, mi sento di affermare che:

* Nella pletora infinita di *freemium* strategici che si trova in giro, Clash of Clan è complessivamente il meglio che abbia provato finora.
* Scendendo nello specifico, il meccanismo di acquisto di gemme è presente in modo discreto e non invasivo, a differenza di molti altri giochi del genere dove gli inviti a comprare vengono mitragliati comunque e dovunque.
* C’è anche un elemento strategico-tattico militare che ha qualche parte nel successo con il gioco; senza attenzione a come viene costruito il proprio esercito e a come vengono condotti gli scontri, la semplice consistenza numerica è insufficiente rispetto a un avversario più esperto.
* Ho letto recensioni dove la grafica un po’ elementare di Clash of Clans riceve un voto negativo e ne dissento. L’ambientazione cartoonistica è piacevole e leggera ed evita certo iperrealismo (eccessivo) di moda.

Sono prime impressioni. Per considerazioni più interessanti rimando a un bell’[articolo del New York Times](http://www.nytimes.com/2013/12/22/technology/master-of-his-virtual-domain.html) dedicato a tale George Yao che, eccezione assoluta, è rimasto in cima alla classifica del gioco per mesi (ed entrava in doccia con cinque iPad impermeabilizzati per lavarsi senza perdere il controllo dei propri account). E intanto inizio a diversificare il mio piccolo esercito. Aspetto a pié fermo chiunque volesse entrare nel gioco: l’integrazione con Game Center funziona bene ed esiste una chat interna per parlarsi.