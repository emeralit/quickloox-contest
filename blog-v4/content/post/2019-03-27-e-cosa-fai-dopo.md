---
title: "È cosa fai dopo"
date: 2019-03-27
comments: true
tags: [UE, Apple, Swift, Card, News, TV, Arcade]
---
Se una Apple [dà vita a una Card](http://www.macintelligence.org/blog/2019/03/26/chi-si-distingue-e-bravo/) e il giorno dopo annuncia l’uscita di [Swift 5](https://swift.org/blog/swift-5-released/), vuol dire che la carne al fuoco è tanta e che le iniziative sul fronte finanziario, marketing, servizi non sminuiscono quelle più puramente tecnologiche (Swift è il nuovo linguaggio di programmazione ufficiale di Apple).

Se una Unione Europea [approva una direttiva copyright](https://gizmodo.com/a-dark-day-copyright-law-that-threatens-the-internet-a-1833570802) e il giorno dopo avere pasticciato con la distribuzione dei contenuti su Internet si mette a pasticciare persino [con l’ora legale](https://www.dw.com/en/eu-parliament-votes-to-end-daylight-savings/a-48064185), significa che difficilmente – spentesi le lobby a obiettivo raggiunto – da quella assemblea potranno uscire cose non pasticciate.

È quello che fai dopo, a mostrare la tua vera statura.