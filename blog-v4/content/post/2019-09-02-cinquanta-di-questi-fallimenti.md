---
title: "Cinquanta di questi fallimenti"
date: 2019-09-02
comments: true
tags: [Unix, Multics, Unics, Stallman, Linux, Torvalds, emacs, Thompson, Canaday, Kernighan, Bell, Jobs, NeXT]
---
Il progetto venne cancellato e tolsero i computer ai programmatori, che però lavoravano in un ambiente rilassato con poca o nessuna pressione produttiva.

La cancellazione era dovuta ai difetti del progetto, troppo ambizioso, mal documentato, uscito dai vincoli di budget prima che si potesse vedere alcunché di apprezzabile. I programmatori sapevano tuttavia di potere fare meglio di quello che c’era.

Trovarono un computer inutilizzato in un altro reparto, ben più modesto nelle specifiche. Misero a frutto le loro caparità e riuscirono a produrre autonomamente gli strumenti essenziali di un sistema operativo, strumenti che presto iniziarono a essere usati dagli altri reparti.

Erano i Bell Labs, il fallimento quello di Multics, il computer quello dal laboratorio di acustica, la loro rivincita Unics (pronunciato come *eunuchs*: ironicamente voleva essere *Multics senza palle*), i personaggi gente del calibro di Ken Thompson e Brian Kernighan

[Sono passati cinquant’anni](https://arstechnica.com/gadgets/2019/08/unix-at-50-it-starts-with-a-mainframe-a-gator-and-three-dedicated-researchers/); il frutto del loro lavoro sta nelle tasche di tre quarti dell’umanità, a crescere. Unix non è certamente in procinto di essere rimpiazzato da qualcos’altro.

La vera nostalgia è per quei tempi nei quali un programmatore poteva trovarsi spalle al muro e allora scatenare veramente il proprio genio, c’erano persone pagate per dispiegare liberamente il proprio talento in assenza di condizionamenti, arrivava in regalo un cucciolo di alligatore che restava in azienda come animale di compagnia (fino a che non spaventò una segretaria).

Una azienda privata sviluppava software libero e gratuito, per smettere solo quando fu smembrata dal governo. Allora un gruppo capitanato da Richard Stallman si mise a produrre una versione libera (GNU, *Gnu’s Not Unix*) di Unix che libero non era più. Steve Jobs e il suo team in NeXT adattarono Bsd, *Berleley Software Distribution*, alle loro esigenze, a creare strumenti che ancora perdurano in macOS. Lo studente finlandese Linus Torvalds si mise a creare un proprio nucleo di sistema operativo, un *kernel*, a partire da quei mattoni di costruzione: Linux.

Queste sono altre storie, certo. Storie di successo. Tutte fondate su un grande fallimento e un gruppo di programmatori rimasti senza computer.
