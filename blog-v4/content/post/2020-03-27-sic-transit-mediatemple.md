---
title: "Sic transit MediaTemple"
date: 2020-03-27
comments: true
tags: [MediaTemple, Linode, Linux, Debian, DNS]
---
Ignoro quando verranno lette queste note, che accompagnano il trasferimento della mia famigliola web da [MediaTemple](https://mediatemple.net/) a [Linode](https://www.linode.com/).

La mossa somiglia a uscire da una palazzina condominiale per entrare in una villetta unifamiliare, di quelle che tanti si costruiscono almeno in parte da soli. MediaTemple è un disco virtuale fatto per lo *hosting*, dove basta nulla per avere un sito funzionante; Linode mette a disposizione una macchina virtuale, un computer completo che può anche, ovviamente, fare hosting. Potrebbe però fare qualunque altra cosa e per questo ha bisogno di essere configurato da zero o quasi.

Quest’ultimo è un problema e un vantaggio allo stesso tempo; aumenta il mio controllo e, per contrappasso, le cose che devo saper fare per esercitarlo.

Un altro vantaggio è di natura economica: dispongo di risorse superiori e più versatili a un quarto del costo. Avevo scelto una soluzione a prezzo di mercato ai tempi ed è così anche oggi, nessun genio, nessuna astuzia; semplicemente, l’offerta si è evoluta nel tempo in direzione di più prestazioni, più convenienti. MediaTemple era un provider piuttosto innovativo ai tempi, mentre oggi è una costola di [GoDaddy](https://it.godaddy.com/), una sorta di [Aruba](https://www.aruba.it/home.aspx) a stelle e strisce. Linode è una bella realtà basata fortemente sul software libero e sul cloud, che nel tempo si è resa accessibile anche a persone disposte a sporcarsi un po’ le mani senza essere per forza amministratori di sistema.

Chiudere il contratto con MediaTemple è stato semplicissimo e veloce; il servizio si spegnerà da sé nel giorno in cui avrebbe altrimenti addebitato il canone annuale. Accendere un account con Linode è stato simmetricamente indolore, a parte alcune ore di attesa (dichiarate in anticipo) per avere la conferma dell’attivazione.

La prima mossa è stata andare su [Network Solutions](https://www.networksolutions.com/), il mio gestore dei dominî, e cambiare i *nameserver*, l’anello di congiunzione tra indirizzo IP (l’identificativo numerico di un sito) e nome di dominio (il suo nome umano). Tra qualche ora o qualche giorno i browser che cercano macintelligence.org riceveranno istruzioni per trovarlo su Linode anziché MediaTemple. (Anche Network Solutions è una scelta obsoleta che andrà cambiata, ma meno urgente). Per inciso è la causa di eventuali disservizi nei prossimi giorni.

La parte interessante dell’esercizio è che, su Linode, i browser troveranno niente di niente. Al momento, infatti, sulla mia macchina virtuale funziona solo un Linux [Debian](https://www.debian.org/) fornito da Linode (è scelto tra numerose alternative, tutte *distro* diverse di Linux). Devo configurare quanto mi serve e comincio adesso. Racconterò come è andata.
