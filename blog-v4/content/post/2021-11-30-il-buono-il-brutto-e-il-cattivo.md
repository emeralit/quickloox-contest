---
title: "Il buono, il brutto e il cattivo"
date: 2021-11-30T01:08:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Adobe, Tiscali, Sky, OBS Studio, Calca, Fëarandil, InDesign, Creative Cloud] 
---
Dicembre è fatto per scrivere a babbo Natale; novembre è dunque un buon mese per redigere bilanci.

## Il buono

Il buono del mio 2021 è sicuramente [OBS Studio](https://obsproject.com), suggerimento di [Fëarandil](https://www.twitch.tv/fearandil) a cui devo una stagione di pizze come minimo. Lentamente, solo per colpa mia, mi si apre un mondo di possibilità nelle comunicazioni video da computer. C’è un mondo di supporto intorno e sembra che niente sia veramente impossibile, a patto di volerci mettere impegno. È computing nella sua forma più pura, software libero che offre possibilità immense veramente a chiunque.

Menzione d’onore a [Calca](http://calca.io), editor di testo Markdown che esegue calcoli come se fosse un foglio elettronico o un programma con variabili numeriche. Avevo preso l’impegno di usarlo in una serie di occasioni, dal tenere la cassa ai budget per casa e lavoro; è stupendo, semplice e completo, si sincronizza tra Mac e iOS, è gratis, zero risorse necessarie, zero carico sul processore. In un mondo dove si apre Excel per fare il calendario ferie, è un’isola di salute mentale e libertà. Peccato solo che il programma non sia più seguito dal suo sviluppatore; d’altronde quello che fa lo fa bene e dubito che possa smettere di funzionare in tempi brevi. Speriamo che a qualcuno venga l’idea di riprendere il concetto.

Altra menzione d’onore a [Pixelmator Pro e al suo omologo Pixelmator Photo](https://www.pixelmator.com/pro/) per iPad. Sono migliorati molto nel tempo e hanno sostituito qualsiasi altro software grafico nel mio flusso di lavoro, che non è intensivo né sofisticato, nondimeno esiste. Rapporto qualità/prezzo perfetto.

## Il brutto

Non riguarda me direttamente, ma ho seguito tutta la vicenda. Tiscali ti propone l’upgrade della ADSL a fibra, gratis. Ti manda il nuovo modem a casa. Il nuovo modem non si collega. Il tecnico dice che il cavo telefonico è vecchio e ne va tirato uno nuovo dall’appartamento alla centralina del condominio. Annuncia che arriverà un altro tecnico a effettuare l’installazione. Non arriva nessuno. Da settimane. Numerose chiamate all’assistenza tecnica e a quella commerciale di Tiscali sortiscono come unico effetto la raccolta di una segnalazione o di un sollecito, dopo di che cambia niente. Una situazione indecorosa per un Paese che sarebbe in Europa.

## Il cattivo

Mi serve InDesign per due settimane l’anno, diciamo tre a esagerare. Ho provato a fare l’esperienza di Creative Cloud da accendere e spegnere alla bisogna e la sconsiglio a chiunque. Adobe vuole a tutti i costi abbonati paganti ogni mese e ha messo in opera un meccanismo di bastone e carota che potrei definire adeguatamente solo se fossi in cerca di querele. Diciamo che mi impegnerò a mettere la maggiore distanza possibile tra me e l’azienda e valuterò seriamente la rinuncia a un lavoro pur di evitare il ricorso ai suoi prodotti.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per Qui
ckLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._