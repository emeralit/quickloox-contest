---
title: "Un cambiamento di spessore"
date: 2019-02-22
comments: true
tags: [Huawei, MateX]
---
Ora di farla finita con questa ossessione di Apple e dei suoi fanatici per apparecchi sempre più sottili. L’annuncio di [Mate X Huawei](https://consumer.huawei.com/en/phones/mate-x/) permette finalmente di sognare una mattonella spessa 1,1 centimetri nel punto più ingombrante.

E il continuo aumento dei prezzi, questi aggeggi che costano oltre i mille euro, cifra assurda per quello che alla fine è un telefono e deve telefonare? Basta, il problema va eliminato alla radice. Samsung non ci è riuscita, con il suo [annuncio da quattro soldi](http://www.macintelligence.org/blog/2019/02/20/piegati-ma-non-spezzati/), neanche duemila dollari. Huawei invece ha fatto le cose per bene e arriva a duemilaseicento dollari, per “un telefono”. Duemilatrecento euro. Sono ansioso di vedere su Facebook quei post di scherno aggiornati alla nuova realtà, *fatevi due viaggi*.

Detesto il tirare per la giacchetta il fantasma di Steve Jobs per fargli dire che è tutto sbagliato, tutto da rifare, ai miei tempi, signora mia. Però, una volta, i cellulari con una cerniera erano *clamshell*, a conchiglia. Adesso sono diventati *foldable*, pieghevoli, come fosse la stessa cosa. E Steve [avrebbe compiuto sessantaquattro anni](https://twitter.com/tim_cook/status/1099685362308247552). Forse un commento gli sarebbe scappato.

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Steve’s vision is reflected all around us at Apple Park. He would have loved it here, in this place he dreamed up — the home and inspiration for Apple’s future innovations. We miss him today on his 64th birthday, and every day. <a href="https://t.co/yRrTiA1iyz">pic.twitter.com/yRrTiA1iyz</a></p>&mdash; Tim Cook (@tim_cook) <a href="https://twitter.com/tim_cook/status/1099685362308247552?ref_src=twsrc%5Etfw">February 24, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 