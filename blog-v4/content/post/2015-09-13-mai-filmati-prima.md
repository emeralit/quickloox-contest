---
title: "Mai filmati prima"
date: 2015-09-13
comments: true
tags: [iPhone, Jobs, iPadPro, Pencil, Apple, Pixelmator, Autocad, 2007]
---
[Avevo avvisato](https://macintelligence.org/posts/2015-09-11-ausilio-alla-lettura/). Internet si è riempita di dementi che citano il più a sproposito possibile Steve Jobs nella presentazione di iPhone: [Chi lo vuole uno stilo?](https://www.youtube.com/watch?t=4&v=4YY3MSaUqMg).<!--more-->

Jobs parlava dell’idea di rendere *standard* uno stilo su un apparecchio da tasca con schermo da 0,15 milioni di pixel.

Siccome otto anni dopo Apple offre l’*opzione* di uno stilo su un apparecchio con trentatrè centimetri di diagonale, da 5,6 milioni di pixel sufficienti a riempire trentasei iPhone del 2007, capace di utilizzare [Autocad 360](https://itunes.apple.com/it/app/autocad-360/id393149734?mt=8) o [Pixelmator](https://itunes.apple.com/it/app/pixelmator/id924695435?mt=8), si tratterebbe di una contraddizione.

Tutta gente che darebbe la vita per poter salire su un palco a sputare una sentenza e nessuno filmerà mai, oggi come otto anni fa.

Non finisce qui, dalle fogne sale ogni sorta di miasma. A domani con altre scioccanti rivelazioni.