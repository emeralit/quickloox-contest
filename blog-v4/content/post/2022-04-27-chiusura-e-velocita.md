---
title: "Chiusura e velocità"
date: 2022-04-27T02:52:38+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Asahi Linux, M1, Apple Silicon, Eckert, Jason Eckert, Mac mini]
---
Sempre sulla falsariga della chiusura voluta da quei cattivoni di Apple, da qualche tempo Jason Eckert lavora a Asahi Linux per il suo Mac mini M1 e a inizio mese ha [relazionato sugli sviluppi](https://jasoneckert.github.io/myblog/asahi-linux/). Molto in breve, il suo commento è:

> Incredibilmente veloce.

Eckert sostiene che Linux si sia evoluto più rapidamente di macOS negli ultimi anni; nella sua esperienza, le app girano a velocità eccezionale e l’ingombro di Ram nel sistema è minore di quello di macOS.

Non è detto che sarà così per forza, perché il suo lavoro non è ancora finito. Alcune app non funzionano ancora (quelle con *framework* Electron per esempio, anche se potrebbe essere una buona idea lasciarle fuori) e lo stesso vale per il supporto di Bluetooth. Arrivare a un supporto completo potrebbe voler dire avere prestazioni in qualche misura minori delle attuali.

Comunque ha installato Firefox, vim, LibreOffice, Gnome, Inkscape, Gimp… e tutto funziona magnificamente. La pagina del suo blog è molto articolata e spiega un mare di cose. Mi limito all’installazione:

> In breve, è molto semplice. Si aziona da macOS uno script che libera spazio per Asahi e lo installa. Una volta installato, Mac si riavvia in recovery mode e completa l’installazione. L’intera procedura porta via circa trenta minuti e ogni passo viene spiegato durante l’installazione.

Per le persone giuste, Asahi Linux potrebbe essere persino un modo *migliore* di godersi i vantaggi di un Mac.

Gli sviluppatori di Asahi hanno dovuto retroingegnerizzare tutto il funzionamento dello hardware, per arrivare a questo risultato. Però non sono una multinazionale, e non ci hanno messo decenni. Se davvero si volesse impedire il funzionamento di Linux sui Mac, ci sarebbero sistemi migliori. Incredibilmente migliori.