---
title: "Echi dalla frontiera"
date: 2017-10-30
comments: true
tags: [iPhone, Butterfly, Iq]
---
[Questo medico ha diagnosticato il proprio tumore con un ecografo per iPhone](https://www.technologyreview.com/s/609195/this-doctor-diagnosed-his-own-cancer-with-an-iphone-ultrasound/).

Non è ancora una cosa per tutti: il prezzo di Butterfly Iq, una ecografia a portata di iPhone, è di duemila dollari. Ma è già approvato ufficialmente negli Stati Uniti per [tredici diverse procedure mediche ordinarie](https://www.butterflynetwork.com/faq.html).

È che qualunque altra alternativa attuale è molto più costosa ed è più grande di un joystick. Cambierà. Cambierà a seguito di iniziative pionieristiche come questa.

Dieci anni dopo il 2007, iPhone sta ancora cambiando le regole. È ancora un mondo interessante che porta innovazione e progresso. Agli altri arriva, al più, l’eco di quello che succede alla frontiera.