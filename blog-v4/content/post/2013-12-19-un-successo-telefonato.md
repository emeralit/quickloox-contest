---
title: "Un successo telefonato"
date: 2013-12-19
comments: true
tags: [iPhone, Asymco, Dediu]
---
Una analisi sommamente interessante di Horace Dediu di Asymco: il mercato americano degli *smartphone* [segue il modello di crescita logistica](http://www.asymco.com/2013/12/13/how-many-americans-will-be-using-an-iphone/), ovvero si comporta perfettamente come un mercato tipico. La giusta scala logaritmica ed ecco che la sua crescita può essere rappresentata con una semplice linea retta.<!--more-->

La retta di crescita degli iPhone è esattamente parallela a quella generale. iPhone è da solo una rappresentazione estremamente somigliante del mercato.

Nessun altro produttore o piattaforma segue lo stesso percorso. La ragione che adduce Dediu è istruttiva:

>I consumatori assorbono il prodotto in modo simile a come assorbono la tecnologia […] e suggerisce che ci sia comunicazione diretta tra il prodotto e i consumatori. Le altre piattaforme non seguono questa direzione perché non comunicano con il consumatore, ma con i produttori oppure i distributori.

Siccome iPhone è rappresentazione del mercato e il mercato si comporta in modo prevedibile, è legittimo estendere le rette e concludere che a inizio 2017 il mercato statunitense sarà saturo! ovvero il 90 percento dei possibili compratori di *smartphone* ne avrà già uno in mano.

In quel momento iPhone avrà negli Stati Uniti una quota di mercato del 68 percento.

Fermo restando che fare esercizi di previsione sui mercati tecnologici fa tremare i polsi e i *cigni neri*, gli imprevisti che sconvolgono tutto, sono in fila dietro l’angolo, viene da dire: così accade nei Paesi avanzati. In quelli arretrati c’è Android.