---
title: "Se non sono ottusi"
date: 2021-06-24T01:06:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, App Store, Vestager] 
---
L’Unione Europea procede, con annunci a orologeria, nell’[investigare Apple](https://ec.europa.eu/commission/presscorner/detail/en/ip_20_1073) per come ha strutturato App Store su iOS e se ci sia danno per la concorrenza.

Margrethe Vestager, Executive Vice-President della Commissione Europea, è riuscita a dichiarare che

>Sembra che Apple abbia ottenuto un ruolo di “guardiana” rispetto alla distribuzione di app e contenuto agli utenti dei suoi diffusi apparecchi.

*Ottenuto*. Si capisce tutto qui, perché si vede il linguaggio della politica. Meglio, è il linguaggio di chi descrive una operazione di appropriazione, non si sa quanto indebita. Come avrà fatto Apple a diventare *guardiana*? Avrà promesso favori in cambio di una nomina? Corrotto una qualche autorità? Mercanteggiato in una commissione parlamentare? Negoziato una poltrona in cambio di un’altra poltrona? *Ottenuto* vuol dire che qualcuno lo ha dato e Apple lo ha ricevuto.

L’idea che Apple possa avere creato dal nulla una piattaforma prima inesistente, e che abbia scelto di *curarla* come potrebbe succedere per una biblioteca o collezione artistica, non può pervenire in alcun modo alle sinapsi di una politicante ottusa.

Questo nulla toglie alle problematiche che riguardano App Store, il trattamento di sviluppatori, il migliogramento della sicurezza e della affidabilità eccetera. Naturalmente App Store non è perfetto.

Ma Vestager e i suoi burocrati non investigano, in realtà; cercano di figurarsi quello che per loro è un ignoto, una forma culturale aliena, qualcosa che non sono in grado di capire, anticipare, raccontare; quindi controllare.

Quello che non possono controllare, cercano di neutralizzarlo. Se almeno facessero così solo con Apple, invece che su tutto il loro difettoso scibile, a spese di quattrocento milioni di cittadini costretti a farsi governare dall’ottusità.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*