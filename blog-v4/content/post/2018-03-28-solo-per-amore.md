---
title: "Solo per amore"
date: 2018-03-28
comments: true
tags: [iPad, Chromebook, Chicago]
---
Qualche sfumatura che vorrei sottolineare all’indomani dell’[evento Apple dedicato all’education](https://www.apple.com/lae/apple-events/march-2018/) tenutosi l’altroieri a Chicago.<!--more-->

I commentatori ne hanno per lo più sottovalutato l’importanza per ridurlo al consueto scontro commerciale, questa volta tra gli apparecchi Apple e i Chromebook di Google. Che sono più somiglianti a un computer, costano meno, contengono le applicazioni di Google e sono tanto semplici da usare.

Qui la prima novità. Apple, che poteva benissimo presentare un MacBook fatto apposta per competere con i Chromebook, ha presentato un *iPad* fatto apposta. Soprattutto ha smontato l’idea classica del computer a scuola con il *word processor*, il foglio di calcolo e il modulo per le presentazioni. Servono, eh; però ragazzi a scuola nel 2018 vogliono realtà aumentata, appunti a matita elettronica, foto, video, leggerezza.

La seconda novità è che Apple poteva benissimo parlare con le scuole, un interlocutore magari più intransigente, eppure più semplice per arrivare a forniture e contratti. Invece sì è rivolta agli insegnanti, l’ultimo anello della catena, persone che spesso neanche possono decidere da sole che cosa usare in classe,

Chiaramente parliamo di un’idea che vuole portare a fare vendere iPad. Però il richiamo emozionale, l’ho trovato persino sincero per quanto comunicato da un’azienda.

La Apple migliore è quella dei prodotti che si vendono come conseguenza prima che come necessità. È quando scrivono *We love teachers* e suona plausibile.
