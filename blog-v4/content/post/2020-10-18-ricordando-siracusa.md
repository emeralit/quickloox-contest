---
title: "Ricordando Siracusa"
date: 2020-10-18
comments: true
tags: [Siracusa, Viticci, MacStories, ArsTechnica, iPadOS, iOS]
---
Della Sicilia ho ricordi stupendi, comunque pensavo a John e alle sue inimitabili [recensioni di macOS, allora Mac OS X e seguenti, su Ars Technica](https://arstechnica.com/author/john-siracusa/).

Rimangono inimitabili. Federico Viticci tuttavia ci va molto vicino con la sua [recensione di iPadOS e iOS.](https://www.macstories.net/stories/ios-and-ipados-14-the-macstories-review/)

Completamente da leggere anche se ci vuole molto. Lo stile di Federico è molto diverso da quello di John e può solo andare così, anche perché nel parlare di iOS e iPadOS è del tutto controproducente andare sotto il cofano della macchina, dove nulla sostanzialmente ci interessa.

Le aperture grafiche delle ventitré (!) pagine sono spettacolari come mai, il lavoro fatto è egregio. C’è una marea di cose interessanti ed estrarre poche cose significative, stavolta, non funziona. Però Federico dice una cosa importante verso la conclusione:

>Mi si permetta di riassumere questa recensione annuale di iOS e iPadOS con un eufemismo: il 2020 è stato un anno difficile per tutti. Riguardo al nostro angolo di Internet, questo comprende anche ingegneri e progettisti: qualunque opinione abbiamo sull’azienda, è rimarchevole che siano riusciti a preparare il (primo) rilascio di iOS e iPadOS 14 e far progredire il loro ecosistema di app in modo sostanziale senza alcuna catastrofe pubblica. Non chiedo compassione per una azienda da mille miliardi di dollari: dico che alla fine dei conti, i sistemi operativi sono fatti da persone. E le persone che lavorano in Apple hanno fatto accadere tutto questo malgrado tutti i piani del 2020 per mettere i bastoni tra le ruote di chiunque.

Sulla frase *i sistemi operativi sono fatti da persone* c’è il link a una toccante [riflessione di John Siracusa nel trentesimo anniversario di Macintosh](http://hypercritical.co/2014/01/24/macintosh). Non è retorica.

A me piace particolarmente il penultimo paragrafo:

>La creatività che iOS 14 ha aperto, che Apple se lo aspettasse o meno, non è una moda sciocca: è la prova bella e ispiratrice del nostro genio che trascende la tecnologia e trasforma il computer che usiamo di più, lo smartphone, in milioni di differenti permutazioni di sé; ognuna diversa dall’altra, ciascuna con una storia differente da raccontare.

Ed è perché soffro sempre quando mi dicono che va usato lo stesso programma per tutti, lo stesso sistema per tutti; va bene, forse, nei più rigorosi collegi svizzeri. Qua siamo umani e usiamo il computer per esprimerci.