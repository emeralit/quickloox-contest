---
title: "La pubblicità è l’anima"
date: 2017-03-13
comments: true
tags: [Chrome, iCloud, FreeBsd]
---
Google esiste grazie alla pubblicità. Eppure l’interfaccia di Chrome è libera da spot.

iTunes serve a vendere musica, film, app. Mentre si ascolta un brano o si guarda un video, la pubblicità è assente dall’interfaccia di iTunes.

Apple incoraggia in mille modi l’aggiornamento alla forma a pagamento di iCloud. Tuttavia, aprire una finestra del Finder non porta mai a leggere un consiglio per gli acquisti riguardante le tariffe di iCloud.

macOS mostra quali app consumano più batteria. Però non consiglia di passare a Safari, che consuma meno, se uno sta usando Chrome.

Se e quando Chrome mostrerà pubblicità nell’interfaccia, sparirà dal mio disco. Se e quando macOS mostrerà pubblicità nell’interfaccia, passerò a FreeBsd nel tempo più breve. (Non uso il condizionale, uso il futuro).

Probabilmente ci sarebbe una preferenza che consente di disattivare le pubblicità e questo verrebbe usato come argomento dai tifosi: sì, c’è la pubblicità, ma quando vuoi la togli.

Disgraziatamente il problema sarebbe tutt’altro, specie in quest’epoca di *design* e *user experience*. La possibilità di disattivare la pubblicità, nell’economia delle utenze a miliardi, vuol dire che la pubblicità sulla maggioranza delle utenze resterà attiva. Per distrazione, noncuranza, ignoranza, fretta, inconsapevolezza, difficoltà (un disabile, per dire, ha meno tempo di un normodotato per andare a caccia di preferenze e intanto svolgere comunque del lavoro).

Tradotto: il sistema operativo, preferenza o meno, diventa *comunque* un veicolo per fare soldi.

Sempre tradotto: la prima funzione del sistema operativo non è più servire l’utente, ma fare soldi. Surrettiziamente: mentre prima il sistema si vendeva agli utenti, adesso è venduto agli inserzionisti e gli utenti sono la merce di scambio.

L’anima del servizio è diventata la pubblicità. Il tempo perché la pubblicità arrivi a condizionare l’interfaccia secondo le proprie esigenze sarà breve.

Mi astengo da pubblicare link perché verrei accusato di parlare di cose che non conosco. Ho posto un caso puramente ipotetico e lo scrivo di nuovo per chiarezza: se e quando macOS si metterà su una strada del genere, inizierò istantaneamente la transizione verso un sistema operativo libero.

Naturalmente, chi si trovasse a usare oggi un sistema operativo molto dffuso che veicola pubblicità può esprimere la propria opinione.