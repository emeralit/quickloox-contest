---
title: "Il multisuccesso"
date: 2023-11-12T01:22:27+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Humane, AI Pin, Snell, Jason Snell, Six Colors]
---
La [recensione di Jason Snell](https://sixcolors.com/post/2023/11/ill-pin-my-hopes-on-ai-assistants/) su [AI Pin di Humane](https://www.theverge.com/2023/11/9/23953901/humane-ai-pin-launch-date-price-openai) contiene molti punti che avrei toccato volentieri nello stesso modo se la avessi scritta io.

Un apparecchio che si attacca ai vestiti, con una telecamera a osservare il mondo davanti, ha tante potenzialità. L’idea di Snell della *costellazione di apparecchi* attorno al nostro corpo a lavorare con i nostri dati è sicuramente suggestiva.

Condivido anche la parziale delusione di fronte all’idea di volere in qualche modo sostituire lo smartphone in base al principio che la gente voglia liberarsi dei telefonini. È abbastanza chiaro che la gente ami i propri telefoni e li usi molto spesso lungo tutto l’arco della giornata. Pensare che la gente vorrà fare a mano di uno schermo in tasca, almeno a oggi, è eccesso di fantasia.

Per questo, dice Snell, AI Pin non avrà successo. Non credo di essere d’accordo; meglio, condivido l’assunto che lo smartphone non sarà sostituito. Non qui, non ora, non da un coso che vuole settecento dollari in partenza e poi un abbonamento oneroso.

Il successo però è un attributo relativo e mutevole. AI Pin non venderà certamente un miliardo di esemplari (successo per iPhone) e io dico che neanche cento milioni (successo per watch). Però dieci milioni potrebbe farli tutti, da oggetto per nicchie professionali e di intrattenimento, almeno fino a che la proiezione dell’ora sulla mano fa tendenza e i proprietari evitano di metterla in pratica su un autobus affollato. Per me a Humane dieci milioni di modelli venduti darebbero soddisfazione e sarebbe, per loro, successo.

A me preoccupa la scelta di affidarsi alla presunta intelligenza artificiale così come viene propinata in questi giorni. Quando l’assistente generativo comincia a sbarellare, che fai, resetti? Non è affatto *cool*. Né intelligente, se sei per strada in una città sconosciuta e lui ti dice di girare dentro una via che si è apena inventato. Per dire. All’intelligenza artificiale vera non si è minimamente accennato e Humane fa una scommessa micidiale sul fatto che ci sarà un qualche progresso sostanziale entro il medio termine. Gli auguro il meglio, ma azioni loro non credo che ne comprerei, se fossero in vendita.