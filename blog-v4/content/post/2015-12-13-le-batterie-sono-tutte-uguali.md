---
title: "Le batterie sono tutte uguali"
date: 2015-12-13
comments: true
tags: [Smart, Battery, Case, Gruber, Daring, Fireball, iPhone]
---
Come i computer. In compenso generano recensioni che tutte uguali non sono, se si guarda alla lunghezza del [commento di John Gruber di Daring Fireball](http://daringfireball.net/2015/12/the_curious_case_of_the_curious_case) relativo allo [Smart Battery Case](http://www.apple.com/it/shop/product/MGQL2ZM/A/smart-battery-case-per-iphone-6s-charcoal-gray).<!--more-->

La recensione della batteria supplementare per il telefono è quasi lunga come quella del telefono.

Il motivo è semplice: il resto di quello che passa sul web, con eccezioni rarissime, è banalità non documentata e superficiale. Che cosa si vorrà raccontare di una batteria? Le batterie sono tutte uguali.

Qui invece verrà ricordato che iPhone 6 e 6S sono controintuitivamente più sottili delle loro controparti Plus. E appare un elenco delle sei priorità seguite secondo Gruber nella progettazione del Case.

1. Autonomia equivalente a quella di iPhone 6S Plus.
2. Buon *feeling* al tatto.
3. Inserimento e rimozione del telefono facili ed eleganti.
4. Custodia protettiva durevole.
5. Ricezione inalterata del segnale cellulare.
6. Bell’aspetto.

Appena si comincerà a scrutare dentro questa recensione, ci si accorgerà di quanto poco valgano le altre, appunto con rare eccezioni, e quante nozioni apparentemente valide, ma false, siano state distribuite ai lettori che si accontentano.

Per fortuna i lettori, almeno quelli, non sono tutti uguali.