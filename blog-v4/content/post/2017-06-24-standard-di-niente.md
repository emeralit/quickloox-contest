---
title: "Standard di niente"
date: 2017-06-24
comments: true
tags: [Gartner, Apple, Microsoft, Google, Samsung, Ibm]
---
Comunicazione di servizio per le persone di azienda rodate, quelle che parlano sempre in gergo, per acronimi, con frasi fatte del mestiere.<!--more-->

Per Gartner (Gartner, il vangelo secondo il *board*) nel settore IT (quando parlate di computer in ufficio dite IT, come se fosse tecnologia interessante) il primo *vendor* (dire produttore è brutto, invece così fa giovane e dinamico) per fatturato 2016 [è Apple](https://www.appleworld.today/blog/2017/6/20/apple-is-the-top-global-vendor-in-it).

Facciamo che Apple abbia venduto 100. (Duecentodiciotto miliardi)

Il secondo *vendor* è Samsung: 64.

Il terzo è Google: 41.

Il quarto (il quarto) è Microsoft: 39.

Il quinto è Ibm: 36.

Ecco, quando parlate di standard di fatto, IT professionale, *marketplace*, canale, *workplace* eccetera, ricordatelo. Certo software che state usando in ufficio, magari vecchio, mai aggiornato, è standard di niente.