---
title: "Anno nuovo, motore nuovo"
date: 2021-12-31T02:24:50+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Hugo, Jekyll, Coleslaw, Mac mini, M1]
---
Ora non ho tempo di spiegare compiutamente; in sintesi, non c’è modo (per me almeno) di fare funzionare [Coleslaw](https://github.com/coleslaw-org/coleslaw) su un Mac mini M1.

Ho scelto di non avventurarmi (per ora) in una esplorazione approfondita e di privilegiare la disponibilità di un motore qualsivoglia, che permetta di pubblicare. Niente diventa più definitivo del provvisorio, l’ottimo è nemico del bene, l’occhio del padrone ingrassa il cavallo (questo non c’entra).

Tra i generatori di siti statici ho provato a installare [Jekyll](https://jekyllrb.com); al momento di pubblicare ricevo un errore che non riesco a decifrare.

Se leggi questo post, significa che l’installazione di [Hugo](https://gohugo.io) è riuscita e sarà lui a fornire la spinta propulsiva per il 2022.

I post vecchi e quelli più vecchi e quelli più vecchi ancora torneranno nei prossimi giorni.

Se questa storia ha una morale, ne ha una perfetta per la vigilia di un nuovo anno: mille auguri di avere sempre supporto, aggiornamento e istruzioni comprensibili, nella vita come nel *computing*.

E grazie naturalmente per condividere con grande pazienza questo viaggio accidentato ma sempre emozionante.