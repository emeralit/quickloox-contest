---
title: "Frutti di sottobosco"
date: 2022-11-14T02:30:00+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [like]
---
Un conoscente, ho scoperto, lavora part-time per una azienda che fornisce like e visualizzazioni per conto terzi.

Passa del tempo pagato a guardare video, visitare post e mettere like, in modo seriale.

L’ho scoperto perché il conoscente ha iniziato a fare pubblicità alla cosa e a cercare nuove candidature.

Curioso, ho cercato documentazione. In effetti dentro la società c’è una forma di schema piramidale e più gente porti, più guadagni.

La società chiede a tutti un pagamento per iniziare a lavorare. Per scoraggiare pratiche come mettere like e poi cancellarli, dice, o smettere subito dopo avere cominciato, oppure fare finta di guardare i video, o subappaltare il lavoro.

In Italia la società non esiste; verrà stabilita, pare, a fine anno. L’attività in Italia è partita dopo l’estate, dalla Spagna dove sarebbe fiorente. Esisterebbero varie sedi internazionali tra cui New York, Angola, Tokyo, Melbourne.

L’azienda spagnola di partenza è emanazione di un altro gruppo, di cui non ho trovato informazioni particolari, che a sua volta è stato creato da una società canadese, di cui ho trovato solo un sito assai laconico e lacunoso.

Una piena conferma della legge secondo la quale qualsiasi meccanismo esistente su Internet verrà abusato. Mi tornano in mente le fabbriche cinesi di estrazione di moneta virtuale dai giochi di ruolo di massa.

Soprattutto, mi vengono in mente le ambizioni di certe società che vogliono vedere grandi numeri di like sulla loro pagina social, grandi visualizzazioni dei loro filmati promozionali, grandi seguiti su Twitter o LinkedIn, e pagano per farlo.

Pagano per ampliare il pubblico o per alimentare un’industria di automi umani che guardano senza vedere, cliccano senza leggere, mettono like senza piacere?

Anche Internet ha un sottobosco e vabbè. Trovarselo vicino fa differenza, un po’ come se nello scantinato del condominio si fosse stabilita una bisca.