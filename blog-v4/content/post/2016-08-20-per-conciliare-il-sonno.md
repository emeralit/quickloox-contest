---
title: "Per conciliare il sonno"
date: 2016-08-20
comments: true
tags: [iPad, Ssh]
---
A letto, ricordi all’improvviso un refuso lasciato in un articolo.<!--more-->

Afferri iPad sul comodino, entri con [Prompt](https://panic.com/prompt/) via protocollo [Ssh](https://it.wikipedia.org/wiki/Secure_shell) nel Mac lasciato aperto sulla scrivania, lanci [nano](https://www.nano-editor.org) e correggi il refuso nel file. (No, non stava su Dropbox, né in iCloud, né in Google Drive, né in altro spazio condiviso).

Chiudi tutto e ti addormenti più soddisfatto.