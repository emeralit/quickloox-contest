---
title: "Il certo per l'incerto"
date: 2020-06-13
comments: true
tags: [Hotspot, iPhone, iPad]
---
Ho usato molte volte iPhone come hotspot per iPad e, in incarnazioni precedenti di modelli e sistema operativo, tutto funzionava a dovere. Ogni tanto però lo hotspot veniva visto dopo due o tre tentativi, oppure veniva beccato all'istante anche se si trovava in un'altra stanza.

In questa incarnazione di modelli e sistemi operativi che uso adesso, lo hotspot è del tutto invisibile a distanza; come mi approssimo, compare in modo infallibile. Ovviamente poi funziona anche a distanza, la prossimità è richiesta solo per il collegamento.

La differenza è veramente netta e porta vantaggi e svantaggi. Se sono accanto a iPhone, lo hotspot appare matematicamente e questo è molto gradito. Se sei sul divano e iPhone in camera da letto, e per un motivo qualsiasi la connessione si interrompe, devi alzarti per forza perché.

Complessivamente, preferisco questo comportamento perché è perfettamente prevedibile e finora non ha sbagliato un colpo.