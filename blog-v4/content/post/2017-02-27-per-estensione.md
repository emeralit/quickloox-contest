---
title: "Per estensione"
date: 2017-02-27
comments: true
tags: [Raw, Power, Aperture, Foto]
---
*MacStories* dedica una [recensione ricca e interessante](https://www.macstories.net/reviews/raw-power-review/) a Raw Power, una estensione di Foto che mette a disposizione di chi li volesse strumenti di regolazione e modifica essenzialmente simili a quelli disponibili su Aperture, il programma *professionale* per l’amministrazione delle librerie fotografiche che a un certo punto Apple ha lasciato al suo destino.

A volte è un peccato terminare un software, altre volte è un tradimento, o ancora può trattarsi di una decisione forzata, tuttavia c'è anche spazio per le prese d'atto. Leggiamo che cosa scrive il recensore:

>Le estensioni RAW Power per Foto ai adeguano perfettamente al mio mix di scatti e al mio approccio All’editing, con l’offerta di strumenti professionali disponibili a comando in Foto sotto forma di estensione appena ne ho bisogno, che stanno fuori vista quando non servono.

Sembra una situazione buona per tutti, chi senza pretese, chi con tante. Realizzata con un programma solo e la possibilità di estenderlo, invece che con due.

L’evoluzione di Foto può essere criticabile e ci mancherebbe. Nel contempo, se escono buoni prodotti indipendenti, le sue possibilità non sono così distanti dal passato. E il professionista ne approfitta assieme all’amatore, nel modo più esteso possibile.
