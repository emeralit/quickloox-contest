---
title: "A Roma da freelance"
date: 2018-08-28
comments: true
tags: [Freelancecamp, Roma]
---
Viaggi, rientri, ripartenze, guasti alla chiavetta ed esaurimento dei giga del piano B di connessione: nei ritardi di questi giorni c’è il sapore tipico della fine delle vacanze.

Parlando di lavoro che riprende, il 10 ottobre passerò da Roma per [partecipare a Freelancecamp](https://www.freelancecamp.net/apogeo-sponsor-roma18-lucio-bragagnolo/), come da relativa breve intervista.

Il tema generale della mia prolusione sarà il lavoro *nobile*, ma scivolerò in fretta verso tematiche di scripting e automazione per quanto la composizione del pubblico lo renderà possibile. Qual è la prima cosa che dovrei presentare in tema di scripting, a una platea di liberi professionisti con specializzazioni spesso assai diverse da quelle tecnologiche? Ho in mente uno schema a tre livelli, calendarizzazione-automazione-programmazione, però devo ancora arrotondare gli angoli.

Se qualcuno si trovasse sul posto, il caffè lo offro io.
