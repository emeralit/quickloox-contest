---
title: "Terrorismo burocratico"
date: 2014-08-07
comments: true
tags: [tecnophil, StackSocial, burocrazia]
---
Ricevo e pubblico (da grassetto a grassetto) questo messaggio di [@tecnophil](https://twitter.com/tecnophil):<!--more-->

**Ho voluto** partecipare ad un [concorso ad estrazione tenuto da
StackSocial](https://stacksocial.com/giveaways/the-1000-google-play-store-giveaway/rules).

Di solito non leggo quasi mai i termini dei regolamenti, ma non riesco a capire o a spiegarmi perché noi italiani non possiamo parteciparvi.

Le nazioni escluse sono: Austria, China, Corea del sud, Francia, Giappone, Grecia, Hong Kong, Italia, Portogallo, Repubblica Ceca, Russia, Spagna, Taiwan.

Perché?

Gli Italiani sono mafiosi, rubano, sono terroristi **o cosa?**

Gli italiani, come del resto anche qualcun altro, hanno i burocrati. Chi è abituato a lavorare in efficienza, si spaventa e fugge terrorizzato.