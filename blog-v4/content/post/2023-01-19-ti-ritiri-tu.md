---
title: "Ti ritiri tu"
date: 2023-01-19T03:22:16+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Times New Roman, Calibri, State Department, Microsoft]
---
Scrive *TechCrunch* che il Dipartimento di Stato statunitense ha deciso, bontà sua, di [ritirare il font Times New Roman](https://techcrunch.com/2023/01/18/font-furore-as-state-dept-retires-times-new-roman-for-retiring-calibri/) finora impiegato nelle sue comunicazioni. Font che era quello ufficiale di Microsoft fino al 2007, quando è stato ritirato da Microsoft stessa per usare qualcosa di più moderno, come Calibri.

Sempre il Dipartimento di Stato userà d’ora in poi il font Calibri, appunto per avere qualcosa di più moderno e adeguato agli schermi nella media più piccoli di una volta, dove i font con le grazie non sempre risultano leggibili al punto giusto.

Peccato che Calibri sia stato ritirato da Microsoft nel 2021, a favore di qualcosa di più moderno.

Si può capire che una amministrazione statale abbia un atteggiamento più rilassato verso la propria immagine tipografica di quanto accada a una multinazionale. Capire fino a un certo punto; apparire vecchi e antiquati non è strettamente necessario né auspicabile.

Sfugge il perché restare attaccati a Microsoft e soprattutto a quello che Microsoft scarta una volta che sia ritenuto obsoleto. Specialmente nel 2023, quando – per fare un esempio – Google offre una varietà enorme di font ottimamente leggibili, gratuiti, con licenza di uso anche per il web, insomma, zero problema. Andare oltre Calibri, a favore di un aspetto più curato e attuale, non guasterebbe.

Purtroppo anche la consapevolezza del valore di una buona tipografia, ben oltre l’estetica, si è ritirata da tempo quasi ovunque.