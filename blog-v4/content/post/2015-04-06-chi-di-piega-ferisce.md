---
title: "Chi di piega ferisce"
date: 2015-04-07
comments: true
tags: [Bendgate, iPhone, S6, M9, Htc, Apple, Samsung]
---
Bei tempi quando si parlava ovunque di [bendgate](https://macintelligence.org/posts/2014-09-27-nelle-pieghe-dellisteria/).<!--more-->

Leggo da [9to5Google](http://9to5google.com/2015/04/03/galaxy-s6-edge-bend-test/) di un [test condotto da SquareTrade](https://www.youtube.com/watch?v=3Y7tPczbOec):

>iPhone 6 si piega sotto circa 50 chilogrammi di pressione e raggiunge il “collasso catastrofico” appena sopra gli 81 chilogrammi. Galaxy S6 Edge raggiunge il punto di piegatura all’incirca allo stesso livello di Apple, ma […] lo schermo si è subito incrinato e l’apparecchio si è distrutto a 67,6 chilogrammi, valore inferiore a quello di iPhone.

>M9 di Htc è stato il peggiore dei tre. Si è piegato alla pressione appena superiore di 54,3 chilogrammi, ma a causa della posizione del pulsante home è divenuto immediatamente disfunzionale.

Curioso come l’argomento abbia cessato di essere di attualità. Ora.

<iframe width="560" height="315" src="https://www.youtube.com/embed/3Y7tPczbOec" frameborder="0" allowfullscreen></iframe>

**Aggiornamento**: Samsung ha emesso un [comunicato](http://global.samsungtomorrow.com/the-official-statement-on-the-smartphone-durability-test-result-conducted-by-squaretrade/) nel quale spiega che i valori di piegatura e distruzione sono superiori a quelli applicati nell’uso normale degli apparecchi. Veramente? Che sorpresa.