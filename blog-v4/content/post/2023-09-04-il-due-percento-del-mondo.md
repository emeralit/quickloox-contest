---
title: "Il due percento del mondo"
date: 2023-09-04T17:05:57+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Asymco, Horace Dediu, Dediu]
---
Era tanto che non si faceva attenzione ad Asymco. Ci è mancato.

In quest’ultimo post, Horace Dediu analizza [l’ecosistema globale di Apple dal punto di vista economico](http://www.asymco.com/2023/09/04/the-2-trillion-economy/).

Il dato non va confuso con quello della capitalizzazione, valore delle azioni moltiplicato numero di azioni. Questo riguarda le vendite dei prodotti Apple, di quelli dell’indotto che ruota attorno ad Apple, quelli i cui produttori incassano il cento percento, ma non potrebbero vendere se non ci fosse l’ecosistema Apple.

L’ecosistema economico ha superato il trilione (mille miliardi) di dollari ed è in crescita regolare da tempo.

Dediu stima che potrebbe toccare i due trilioni nel 2025. L’ecosistema Apple coinvolge probabilmente decine di milioni di persone che ne ricavano da vivere, in tutto o in parte.

Più o meno equivale a quello che per le nazioni è il prodotto interno lordo.

Il prodotto interno lordo del mondo intero nel 2023, dice Dediu, è nella direzione dei centocinque trilioni e cresce a ritmo meno sostenuto di quello Apple.

Quindi è possibile ipotizzare un momento nei prossimi due-cinque anni in cui Apple costituisce, con il proprio ecosistema, il due percento del prodotto lordo mondiale.

Qualunque sia il nostro pensiero sulle dimensioni e l’importanza di Apple, è il caso di riformularlo.