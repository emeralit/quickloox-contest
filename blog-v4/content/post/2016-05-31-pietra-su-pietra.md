---
title: "Pietra su pietra"
date: 2016-05-31
comments: true
tags: [Go, Dragon]
---
Comincia il periodo che considero estivo nel senso che più facilmente mi trovo con una connessione debole e, per quanto ci sia sempre da fare fino ad agosto, le giornate sono più lunghe e viene più voglia di combinare qualcosa di extra.

Da tempo volevo provare a impratichirmi di [Go](http://www.figg.org/index.php?option=com_content&view=article&id=11&Itemid=245&lang=it) e ho trovato [Dragon Go Server](http://www.dragongoserver.net/), sito spartano ma funzionale, che permette di trovare avversari e misurarsi su plance di qualsiasi dimensione. Sto giocando la mia prima partita contro un avversario sconosciuto, su una plancia 5x5: la più semplice che abbia un senso (si può giocare a Go su una 3x3, ma la partita perde rapidamente interesse dopo la prima mossa).

Se qualcun altro condividesse, può sfidare un certo qual *loox* a pietre bianche contro pietre nere ed essere ragionevolmente certo di una vittoria agevole.

Accetto anche volentieri suggerimenti su server migliori, più frequentati, più facili da usare, che si possano usare anche fuori dal *browser* eccetera (ho appena trovato [Ogs](https://online-go.com/) per esempio, ancora da sondare per bene, e sto investigando sull’[uso di emacs come interfaccia](http://eschulte.github.io/el-go/) per collegarsi a server di Go). L’unico requisito è che non sia obbligatorio il gioco in diretta, a tempo. Riesco tranquillamente a compiere una mossa al giorno, mentre trovare mezz’ora per giocare una partita intera è difficile.