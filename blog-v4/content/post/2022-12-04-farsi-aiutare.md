---
title: "Farsi aiutare"
date: 2022-12-04T23:20:50+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [GPT, AI, intelligenza artificiale]
---
Si parla molto di software capace di generare un testo a richiesta dopo averne letti centinaia di milioni, oppure di generare una immagine a richiesta dopo averne viste altrettante.

Il problema è chiamarla *intelligenza artificiale*. Sono incappato in [un tweet che spiega la situazione](https://twitter.com/ancerj/status/1599056204353134592) molto bene e invecchierà allo stesso modo, molto bene.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">GPT excitement is appropriate, but has similar shortcoming as self-driving car excitement.<br><br>Current performance is fascinating, but commercially viable in very narrow contexts + people underestimate how exponentially hard will be to get to true disruptive performance. Not linear.</p>&mdash; José Ancer 🇺🇸 (@ancerj) <a href="https://twitter.com/ancerj/status/1599056204353134592?ref_src=twsrc%5Etfw">December 3, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Azzardo una traduzione rapida.

§§§

L’eccitazione a riguardo di [GPT](https://openai.com/api/) è appropriata, ma ha limiti che rispecchiano quelli dell’eccitazione per le auto che si guidano da sole.

Le prestazioni attuali sono affascinanti, ma valide a livello commerciale in contesti estremamente ristretti; in più, le persone sottostimano quando sarà esponenzialmente difficile arrivare a prestazioni veramente dirompenti. Non linearmente, esponenzialmente.

In altre parole, abbiamo intelligenza artificiale capace di approssimare un quoziente intellettivo di ottanta. È affascinante. Ci sono persone allo stesso livello intellettivo che sono capaci di lavorare.

Solo che arrivare a novanta, o cento, richiede progressi sostanziali che probabilmente in questo momento non sono nemmeno compresi concettualmente e non parliamo della commerciabilità di tutto questo, che non è affatto imminente.

Vale la pena anche di notare il parallelo tra la guida autonoma e GPT, che entrambi eliminerebbero lavori umani. *Non sta per succedere*.

A un livello di prestazioni dell’ottanta percento rispetto alla media umana, l’intelligenza artificiale è un amplificatore di produttività. Perlopiù aiuta le industrie più avanzate, può darsi che complichi la vita di quelle più arretrate. Niente di *dirompente*, però.

§§§

Ecco. Farsi aiutare da una intelligenza artificiale, e va bene anche chiamarla così se serve a capirsi e a fare prima, questo sì. Ho un amico che si fa aiutare da [Stable Diffusion](https://stability.ai) a creare la grafica per un suo progetto personale. Lui riesce a spingere in là il potenziale del software, il software lo aiuta, tutti sono contenti.

Da qui a ipotizzare una intelligenza artificiale corrispondente a questa definizione, capace di scalzare umani dai loro posti di lavoro, se uno lo pensa davvero oggi, meglio farsi aiutare.