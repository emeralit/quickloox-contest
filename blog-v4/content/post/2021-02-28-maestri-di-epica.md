---
title: "Maestri di epica"
date: 2021-02-28T00:52:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Jeff Vogel, Spiderweb Software, Geneforge 1 - Mutagen] 
---
Se non avessi due figlie, ora che si vocifera del ritorno della zona arancione, comprerei di corsa [Geneforge 1 - Mutagen](https://www.spiderwebsoftware.com/geneforge/index.html) e mi dimenticherei la nozione di *noia* per un paio di mesi almeno.

Ma forse quasi quasi lo compro lo stesso e ci gioco con la figlia maggiore. Esercitarsi con l’inglese è necessario, servono storie ampie e piene di accadimenti per espandere gli spazi di attenzione e insegnare pazienza e metodo, all’inizio dei nostri [novanta giorni di Arcade](https://www.macintelligence.org/posts/La-cura-del-giardino.html) abbiamo già trovato giochi straordinari, ma niente di epico o su vasta scala. Sono cresciuto con *Il signore degli anelli*, la fantascienza di Clarke, Atom Heart Mother dei Pink Floyd… qualcosa di epico ci vuole per forza.

Aggiungo che per me Jeff Vogel, *patron* di Spiderweb Software, è un *unsung hero*. Nel 1994 ha cambiato vita e mestiere, ha coinvolto la moglie e ha deciso di guadagnarsi da vivere scrivendo avventure in stile *role-playing* per Mac. Ci è riuscito perfettamente e la sua produzione è persino migliorata nel tempo.

Come tutti i suoi giochi degli ultimi anni, anche *Geneforge 1 - Mutagen* ha un demo gratis di tutto rispetto, più che sufficiente per togliersi qualche soddisfazione oltre a qualsiasi dubbio sulla qualità del gioco. In primavera esce anche su iPad.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*