---
title: "Tenga il testo"
date: 2023-11-20T16:45:19+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Infocom, IFComp]
---
Se ieri rievocavamo [le antiche avventure testuali Infocom](https://macintelligence.org/posts/2023-11-19-lanello-mancante/), oggi è finito IFComp e ci ritroviamo con [settantaquattro avventure testuali nuove di zecca](https://ifcomp.org/comp/2023) da provare e giocare, senza troppi rimorsi perché praticamente tutte richiedono da quindici a centoventi minuti di gioco.

Nessuno di noi rimarrà sveglio la notte alla ricerca di una chiave che non si è trovata o con un mondo che non basta un foglio A4 per mappare.

Il genere tuttavia regge, resiste e intriga comunque, appena se ne comprendano le potenzialità.

Arrivare a scrivere un (piccolo) gioco di avventura basato su testo è una attività che consiglierei a qualsiasi consesso di scuola primaria. Imparare come è fatta una storia, il concetto dell’albero delle scelte e dei nodi, il senso della triade posizione/oggetto/azione, il cimentarsi con i microtesti per renderli coerenti e funzionali, la libertà di ambientare nella storia o nella geografia… se appena appena si scrive in inglese, qualsiasi materia può ricavare beneficio dal mettere i bambini al lavoro su un progetto del genere.

Per le classi più piccole, non serve neanche il computer, per scrivere una storia a nodi e concepire una trama.

Le medie potrebbero programmarla davvero l’avventura, da un semplice minisito che fa interagire con i clic sui link, al prendere il coraggio a due mani e accendere l’interprete Python, o quello che è.

Siamo sul costone dell’abisso del metaverso, accerchiati dalle realtà ibride, avvelenati dai miasmi di mediocrità delle intelligenze artificiali sedicenti, ma rimane vero quello che era vero negli anni settanta: chi governa il testo, governa il sistema. Toglietemi tutto, basta che mi lasciate il testo.