---
title: "Strade nascoste"
date: 2020-10-14
comments: true
tags: [DuckDuckGo, MapKit, Apple, iPhone, HomePod]
---
Certo, iPhone 12, HomePod e compagnia sono annunci importanti e ci si tornerà sopra presto. Peraltro, erano attesi e si aggiungono a una continuità.

Intanto Apple fa cose interessanti con ricadute meno eclatanti, ma di rilievo. Per esempio, il suo _framework_ per le mappe ora [consente a DuckDuckGo di fornire indicazioni per chi guida o cammina](https://spreadprivacy.com/duckduckgo-search-map-directions/).

DuckDuckGo è un motore di ricerca _outsider_ costruito, a differenza di Google e degli altri, sul rispetto della _privacy_ dei suoi frequentatori; un’impostazione fortemente perseguita da Apple che, guarda caso, trova sbocchi fuori da Cupertino e rappresenta una sorta di certificazione reciproca: se DuckDuckGo si fida del motore di mappe Apple, possiamo fidarci anche noi. Se Apple concede MapKit a DuckDuckGo, significa che quest’ultimo è una realtà da considerare.

Ogni tanto ci provo e torno a Google per una serie di servizi accessori che è innegabile manchino a DuckDuckGo. Nondimeno, il divario continua a ridursi e, per molti utilizzi non sofisticati, i risultati sono di qualità pari a quelli di Google.

Se sceglieremo DuckDuckGo per farci dare indicazioni stradali, resterà una cosa tra noi e lui. Questo, già detto e vale la pena ripeterlo, si avvia a essere un lusso.