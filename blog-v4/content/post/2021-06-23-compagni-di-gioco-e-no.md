---
title: "Compagni di gioco (e no)"
date: 2021-06-23T01:44:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Polytopia, DeepMind, Go, Google, AlphaGo] 
---
Il gruppo di amici appassionati di Polytopia in cui milito non è l’unico a mettere cinque minuti giornalieri nello scannarsi in allegria e sportività sulle mappe strategiche del gioco.

A breve [arriva anche DeepMind](https://twitter.com/demishassabis/status/1401915428205731848), l’agente intelligente sostenuto da Google che si autoaddestra a imparare giochi di strategia da [Go](https://online-go.com/learn-to-play-go) in giù, con [risultati da prima pagina](https://en.wikipedia.org/wiki/AlphaGo_versus_Lee_Sedol).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">So excited for this, can&#39;t wait to see how our AI does on the strategic gem Polytopia, one of my favourite mobile games!! <a href="https://t.co/ooE4oOvzy0">https://t.co/ooE4oOvzy0</a></p>&mdash; Demis Hassabis (@demishassabis) <a href="https://twitter.com/demishassabis/status/1401915428205731848?ref_src=twsrc%5Etfw">June 7, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Ecco, devo ancora sentire gli altri, però io non credo che gli proporrò di giocare assieme a noi.

(Qualunque umano voglia cimentarsi, invece, è benvenuto).

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*