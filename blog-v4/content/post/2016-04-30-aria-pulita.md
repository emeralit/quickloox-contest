---
title: "Aria pulita"
date: 2016-04-30
comments: true
tags: [Icahn, Apple, Zagabria, Cook, Bbc]
---
Il notiziario Bbc a Zagabria mostra in sottopancia la notizia che Carl Icahn ha venduto le proprie azioni Apple. Ovviamente Bbc (ma anche il [Los Angeles Times](http://www.latimes.com/business/technology/la-fi-tn-apple-carl-icahn-20160428-snap-story.html)) usa il verbo *dumped*, buttate, perché così la notizia sembra più drammatica.<!--more-->

In realtà Icahn è uno speculatore puro che investe senza curarsi minimamente del contesto e si muove considerando solo il profitto in quanto tale.

Benissimo e che ognuno viva come preferisce. Di fatto la sua presenza tra gli azionisti Apple è sempre apparsa fuori posto, specie da quando Tim Cook ha fatto presente che talvolta Apple si muove per fare la cosa giusta e non per guadagnare a tutti i costi, come nel caso delle iniziative ambientali, che certamente procurano un grosso ritorno di immagine e forse pure di vendite, ma hanno a mio parere costi completamente esagerati rispetto al beneficio materiale.

Icahn senza azioni Apple suona molto come una ennesima iniziativa ambientale: aria più pulita all’assemblea degli azionisti.

P.S.: Per il *Los Angeles Times*, Icahn ha realizzato due miliardi di dollari. Lascio al lettore il compito di stimare il suo guadagno netto, sapendo che ha comprato le azioni tre anni fa.