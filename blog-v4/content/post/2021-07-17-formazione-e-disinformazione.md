---
title: "Formazione e disinformazione"
date: 2021-07-17T01:22:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Invalsi] 
---
Mi scuso per insistere, solo che passato il primo clamore – quello che fa comodo alla propaganda, perché la maggioranza si ferma lì – inizia ad alzarsi il sipario sopra la faccenda dei risultati Invalsi e la loro attribuzione di comodo alla didattica remota.

Si prenda l’eccellente articolo di Elisabetta Tola su *La valigia blu*, [Non è “il tonfo della DaD”. I dati Invalsi dicono (molto) altro](https://www.valigiablu.it/dati-invalsi-scuola-dad/):

>Da giorni impazzano titoli e interpretazioni sui dati Invalsi, usciti giovedì mattina. Peccato che titolisti, giornalisti e anche parecchi politici dimostrino, ancora una volta, di non saper leggere con la giusta attenzione i report e le tabelle pubblicate dall’Invalsi, l'Istituto nazionale per la valutazione del sistema educativo di istruzione e di formazione,  e, soprattutto, di non conoscere il concetto di correlazione e di rapporto causa-effetto.

Tola si sofferma il giusto, spiega bene, va letta, sui punti che [ho toccato superficialmente](https://macintelligence.org/posts/Imparare-a-insegnare.html) e poi sulle tematiche di fondo: qualunque peggioramento ci sia stato, si innesta su una situazione di una gravità enorme, che trascende qualsiasi diatriba sulla modalità di insegnamento ad aule chiuse, che si trascina da anni.

I risultati cambiano da ciclo a ciclo scolastico.

I risultati cambiano da tipo a tipo di scuola.

I risultati cambiano da regione a regione.

Incredibile come, nelle menti semplici, che abbondano nelle redazioni, un fenomeno così diversificato e composito abbia avuto una causa unica, semplice e ovvia. Che cosa sia accaduto negli anni scorsi nessuno se lo chiedeva, però. Se non altro, potrebbe essere l’occasione per attirare maggiore consapevolezza sul problema.

Ah, Tola ha studiato. Ha guardato le presentazioni dei dati, i dati stessi, tutto. Sa che *i dati fino a qui commentati sono provvisori* e che quelli definitivi ci saranno solo a settembre. Sa che non c’è alcuna differenziazione tra scuole che hanno insegnato da remoto, scuole che non lo hanno fatto, che lo hanno fatto un po’ sì e un po’ no eccetera. Ovvero, non c’è alcuna base scientifica nell’attribuire i risultati alla didattica remota, perché non si possono (ancora) esaminare campioni specifici. Si potrà; per assurdo potrebbe essere che la lezione via rete sia peggio che tenere le scuole chiuse, oppure che sia migliore del lavoro in aula!

In pratica, tutti sono stati concordi nello spiegare la situazione proprio come non si poteva e non si doveva cercare di spiegare, prima del tempo, senza i dati completi a disposizione e senza avere ascoltato chi li ha prodotti e presentati.

Tutta questa gente, nelle redazioni, nei partiti, su Facebook, non ha fatto un minuto di didattica a distanza in tutta la vita. Eppure è uscita dalla scuola con limiti profondi e si vede. Tanto.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*