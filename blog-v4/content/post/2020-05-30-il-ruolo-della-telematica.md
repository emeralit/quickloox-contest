---
title: "Il ruolo della telematica"
date: 2020-05-30
comments: true
tags: [Roll20]
---
Da circa sempre, ogni due venerdì la compagnia degli amici si ritrova a giocare a [Dungeons & Dragons](https://dnd.wizards.com). Tradizionalmente il master, che dà vita e consistenza al mondo in cui agiscono i personaggi dei giocatori, prepara la mappa di gioco sulla quale si svolgono i combattimenti, su fogli A3 oppure su [battle mat](https://www.amazon.com/dp/B0015IQO2O/) dove disegnare a pennarello.

Si può andare [molto più in là di questo](https://geekdad.com/2019/06/dungeon-master-resources-getting-started-with-3d-dd-terrain/), però non è la direzione che ho scelto svariati anni fa. Toccava a me fare il master e ho stabilito che avremmo giocato con mappe digitali e avatar digitali su [Roll20](https://roll20.net), un sistema nato apposta.

Ci incontravamo di persona ma sul tavolo la mappa non c'era; bisognava collegarsi a Roll20 e interagire con la piattaforma di gioco.

All'inizio non è stato semplice. La tecnologia di rete e anche da portare al tavolo di una serata tra amici non era quella di adesso. Lo stesso Roll20 aveva molti spigoli da tollerare.

Lentamente, con qualche sbuffo e qualche occasionale inceppamento, il gioco su Roll20 è diventato familiare. Posso oggi fare il master da un iPad quando all'inizio a malapena ci si poteva gestire il personaggio; anche gli amici più riottosi verso la telematica hanno finito, per lavoro o per diletto, di dotarsi di un portatile o di un iPad. Tutto l'insieme è maturato.

Così è arrivato il _lockdown_ e noi non abbiamo battuto ciglio, lato gioco di ruolo: semplicemente ci sediamo ognuno al proprio tavolo invece che tutti al medesimo. Ci vediamo in faccia, fioccano le stesse battutacce di una serata in presenza, nessuno deve viaggiare per tornare a casa e quindi le sessioni durano più a lungo, Roll20 era studiato esattamente per situazioni come questa e fa egregiamente il suo lavoro.

Ovviamente manca l'incontro di persona. Ovviamente chi ama la fisicità delle miniature di gioco o dei dadi multipoliedrici, e in presenza usava Roll20 ma lanciava i dadi veri, lavora con succedanei digitali. Naturalmente non vediamo l'ora di darci una pacca sulla spalla nel ritrovarsi e mettersi a giocare.

Ciò detto, a nessuno è neanche lontanamente passato per la testa di sospendere le nostre sessioni in attesa di potersi nuovamente riunire. Anzi, un sottoinsieme della compagnia, vista la semplicità di collegarsi con gli amici dallo studio o dal salotto, ora gioca tutti i venerdì invece che alternare.

Il telegioco di ruolo non può e non vuole rimpiazzare i nostri incontri. Torneremo a trovarci. Nell'impossibilità di farlo, tuttavia, supplisce in modo molto efficace.

Perché?

Perché eravamo preparati, anche senza saperlo. Avevamo tutti esperienza pluriennale di un sistema che permetteva di giocare assieme a persone lontane; quando usarlo è diventato necessario, abbiamo solo acceso le videocamere e tutto il resto era ampiamente collaudato.

Perché siamo motivati. Ci piace stare insieme anche solo per videoconferenza; ci divertiamo a giocare a D&D. Siamo disposti a superare qualche piccolo ostacolo pur di farlo anche a distanza.

Invece di lamentarci dei difetti del gioco via Internet, ne sfruttiamo e amplifichiamo i pregi.

Finirà che quest'estate, periodo in cui spesso per ferie e viaggi mancava il numero giusto per riunirsi, ci incontreremo comunque in rete, dal mare o dai monti.

Questo è il ruolo della telematica. Collega le persone come sarebbe altrimenti impossibile.

Ora, se penso a tanti insegnanti e tanti uomini di azienda che sono andati nel panico di fronte al proseguire la loro attività in rete, sono perplesso. Persone impreparate. Persone incapaci di lavorare sul lato positivo della situazione. Persone poco motivate e posso capirlo ma, fuori dai denti, pagate per quello che fanno. Possibile essere incapaci di usare con profitto strumenti di lavoro che esistono da lustri?

Nel nostro gruppo abbiamo amici che accendono un computer solo se costretti. Bisognava spiegargli tutto. L'abbiamo fatto. Funziona.

Un consiglio, anzi tre, per chiunque. Sii preparato alla telematica. Lavora sul lato positivo delle cose. Se non trovi una motivazione, almeno fallo per denaro. Non farlo è autolesionista e queste settimane dovrebbero averlo mostrato in modo chiaro.