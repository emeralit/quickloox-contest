---
title: "Passato e presenti"
date: 2017-07-04
comments: true
tags: [iPhone, Gruber, jailbreak]
---
Ho preferito stare lontano dal decimo anniversario di iPhone per evitare l’eccesso di melassa e l’effetto nostalgia, comprensibile ma un tantino sovraccaricato. L’unico che vale la pena di citare è [John Gruber su Daring Fireball](https://daringfireball.net/2017/06/perfect_ten):

>Nessun prodotto è paragonabile a iPhone in termini di impatto sociale o finanziario nell’era del computing. Pochi prodotti sono paragonabili nella storia del mondo. Potremmo non vedere mai più qualcosa d’altro confrontabile, da Apple o da chiunque altro.<!--more-->

Per quanti *I keynote di una volta e adesso invece che noia*.

La cosa veramente importante, lo scrive anche Gruber, è che iPhone è ben lontano dall’avere toccato il fondo del barile; il suo potenziale non è ancora stato esaurito. Di giorno in giorno arrivano novità su iPhone che sono il giusto modo di rendergli omaggio: è un oggetto con piedi ben piantati nell’attualità. Per esempio.

Apple sta dando visibilità a un cortometraggio [girato interamente con iPhone](https://9to5mac.com/2017/06/29/apple-shares-short-film-detour-shot-entirely-on-an-iphone-videos/).

Il *jailbreak* [è in via di estinzione](https://motherboard.vice.com/en_us/article/8xa4ka/iphone-jailbreak-life-death-legacy): sempre più rischi in cambio di sempre meno valore aggiunto.

Resta ancora molto da fare: c’è per esempio un problema di [*app* dalle dimensioni eccessive](https://birchtree.me/blog/the-apps-are-too-damn-big/).

Il passato di iPhone è probabilmente irripetibile da quanto straordinario. Niente, tuttavia, in confronto agli infiniti presenti nei quali giorno per giorno emerge come protagonista.
