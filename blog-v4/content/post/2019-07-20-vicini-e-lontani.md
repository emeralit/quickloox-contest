---
title: "Vicini e lontani"
date: 2019-07-20
comments: true
tags: [Aaa, AllAboutApple, Luna, Aaalunati, Savona]
---
Ho sempre amato la realtà e le persone di All About Apple, solo che la paternità mi ha levato molte occasioni di essere a Savona. Così mi mancano, la realtà e le persone.

Sono quindi contentissimo di apprestarmi a raggiungere la sede del museo per la [serata che oggi dedica allo sbarco sulla Luna](https://www.allaboutapple.com/2019/07/aaallunati-il-20-luglio-la-notte-bianca-di-all-about-apple/). Quando ci fu la diretta, ero un moccioso riottoso che rifiutò di andare a dormire per non perdere l’evento.

Stasera è possibile che rivedrò quelle immagini in compagnia di una mocciosa riottosa che mi toccherà mettere a letto di peso, schiantata dal viaggio e dalle emozioni di una giornata in gita. In mezzo ad amici lontani da troppo tempo, che [continuano a far vivere un sogno straordinario nonostante la fatica](https://video.repubblica.it/tecno-e-scienze/viaggio-nel-tempio-apple-che-e-a-savona-e-non-a-cupertino-10mila-pezzi-originali-ma-niente-sponsor/339956/340543?ref=RHPPRT-BS-I0-C4-P1-S1.4-T1) e saranno finalmente vicini come è giusto, per qualche ora almeno.

La felicità è un concetto asintotico, ci si può andare molto vicini anche se rimane elusiva. Stasera mi sentirò felice per un po’, tra gente straordinaria, una collezione unica di passato Apple, un anniversario unico e due bambine in più.
