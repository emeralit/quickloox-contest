---
title: "Buona la ennesima"
date: 2023-02-10T02:20:02+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Myst, Myst Mobile, Tubular Bells, Tubular Bells II, The Mix, Kraftwerk, The Catalogue, Blade Runner]
---
Amo [Tubular Bells](https://open.spotify.com/album/0a3YQpBnRzJzNktOjb6Dum). Dopo anni di ascolto, la versione che mi piace di più, delle dodicimila che sono state prodotte, è [Tubular Bells II](https://www.youtube.com/watch?v=zH-pzIf8xyY).

Adoro la musica dei Kraftwerk e ho tutta la loro (non abbondante) produzione. Dopo anni di ascolti, il piacere maggiore lo ricavo dai pezzi di [The Mix](https://www.last.fm/music/Kraftwerk/The+Mix) oppure [The Catalogue 3-D](https://kraftwerk.com/catalogue/index-BLUray.html).

Insomma, non è mica detto che la prima uscita sia quella giusta (succede, eh; [Blade Runner](https://www.warnerbros.com/movies/blade-runner), per esempio).

Ecco perché sostengo a spada tratta, dopo neanche un quarto d’ora, che [Myst Mobile](https://apps.apple.com/it/app/myst-mobile/id6444813759) è meglio dell’originale, anche se è lo stesso gioco. E finisce che metterò lì anche i dodici euro.

(Attenzione a non confondersi con altre versioni vecchie come [RealMyst](https://cyan.com/games/realmyst/); stai cercando esattamente *Myst Mobile*).

<iframe width="560" height="315" src="https://www.youtube.com/embed/zH-pzIf8xyY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>