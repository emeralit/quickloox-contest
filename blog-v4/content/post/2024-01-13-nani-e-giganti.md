---
title: "Nani e giganti"
date: 2024-01-13T13:05:40+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Wirth, Niklaus Wirth, Pascal, Tetris, Gruber, John Gruber, Daring Fireball, A Plea for Lean Software]
---
Sappiamo di essere [come nani seduti sulle spalle dei giganti](https://e-l.unifi.it/pluginfile.php/777742/mod_resource/content/1/XII%20SECOLO-UMBERTO%20ECO.pdf) mentre scorriamo distratti il blog di John Gruber e troviamo due commenti in sequenza.

Il [primo](https://daringfireball.net/linked/2024/01/11/wirth-plea-for-lean-software) rimanda a un appello di Niklaus Wirth sulla necessità di [scrivere software snello](https://github.com/sysprv/demo-corporate-documentation-public/blob/master/Niklaus%20Wirth%20-%20A%20Plea%20for%20Lean%20Software.pdf). Appena scomparso, Wirth è stato un padre nobile della scienza dei computer e, in particolare, l’inventore del linguaggio di programmazione Pascal.

A me Pascal non piace, ma resta ugualmente una pietra miliare e un modello di strutturazione e pulizia. Come ricorda Gruber, tutte le applicazioni degne di un’occhiata all’inizio di Macintosh erano scritte in Pascal e il relativo ambiente di programmazione era dieci anni avanti.

Il [secondo commento](https://daringfireball.net/linked/2024/01/11/tetris-for-mac) segue la notizia del [nuovo record di punteggio in *Tetris*](https://macintelligence.org/posts/2024-01-04-nessuno-siamo-perfetti/) e recupera un suo articolo del 2018 per descrivere la mancanza di una buona app per giocare a *Tetris* su Mac. Cinque anni dopo, la situazione è identica e l’unico modo per farlo è ricorrere a siti web, compreso quello di Tetris Company, dopo che il gioco ha avuto tempo di diventare un classico.

Siamo nani seduti sulle spalle dei giganti, solo che qualcuno è seduto in modo da guardare indietro.