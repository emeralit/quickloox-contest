---
title: "Barra a dritta"
date: 2016-03-31
comments: true
tags: [Windows, OSX, bash, Linux]
---
Su Windows 10 [sarà presto possibile usare bash](http://arstechnica.com/information-technology/2016/03/ubuntus-bash-and-linux-command-line-coming-to-windows-10/), la *shell* (il programma di interfaccia a riga di comando) che è da sempre prerogativa di Linux e OS X contiene dalla nascita, anno 2000, per usarla come preimpostata credo da una decina d’anni.<!--more-->

Il commento serio lo [lascio a Federico](https://www.facebook.com/federico.fogli/posts/10209050583091211).

<div id="fb-root"></div><script>(function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0];  if (d.getElementById(id)) return;  js = d.createElement(s); js.id = id;  js.src = "//connect.facebook.net/it_IT/sdk.js#xfbml=1&version=v2.3";  fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script><div class="fb-post" data-href="https://www.facebook.com/federico.fogli/posts/10209050583091211" data-width="500"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/federico.fogli/posts/10209050583091211"><p>La fine di Windows come OS. Microsoft dei servizi in arrivo.</p>Pubblicato da <a href="#" role="button">Federico Fogli</a> su&nbsp;<a href="https://www.facebook.com/federico.fogli/posts/10209050583091211">Mercoledì 30 marzo 2016</a></blockquote></div></div>

Per una e una sola volta, mi permetto una battuta senza spiegarla: avranno un bel daffare a raddrizzare le barre.
