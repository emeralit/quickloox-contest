---
title: "La regione e l’intelletto"
date: 2016-11-23
comments: true
tags: [bollo, online]
---
Mi hanno prenotato un volo e inviato i dettagli per email, dopo di che ho provveduto a effettuare il *check-in* tramite la *app* della compagnia aerea.

Ho comprato online i biglietti dei mezzi necessari a raggiungere l’aeroporto.

Sulla mia scrivania, intanto, campeggia la lettera della Regione Lombardia: ti facciamo lo sconto con il bollo auto se compili questo modulo di carta e lo spedisci via posta ottocentesca.

Come chiamarlo? Sconto arretratezza? È perché se uno sta ancora a compilare moduli con la biro si merita qualcosa in cambio per la sopportazione?

Come al solito, il denaro pubblico non si fa problemi a buttarsi via. E con esso il tempo dei privati.