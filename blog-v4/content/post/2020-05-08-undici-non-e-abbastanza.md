---
title: "Undici non è abbastanza"
date: 2020-05-08
comments: true
tags: [ iPad, Pro, MacStories, Christoffel]
---
Prima di decidermi tra iPad Pro 12"9 e 11" [è stata dura](https://macintelligence.org/posts/2018-11-27-non-recensioni-ipad-pro-12-9).

Ryan Christoffel di _MacStories_ mi ha fatto un grosso favore con il suo [confronto tra iPad Pro 12”9 e iPad Pro 11”](https://www.macstories.net/stories/my-11-inch-ipad-pro-experiment/).

È una prova accurata e puntigliosa, che ha dato tempo all'ecosistema di maturare prima di essere valutato.

Il suo scopo era verificare se e quanto l'uso di iPad Pro 11" sia penalizzante rispetto a quello con lo schermo più grande, in linea con i miei dubbi.

I risultati mi hanno confortato. Christoffel riferisce di finestre affiancate più confortevoli del previsto e visione a pagina piena meno soddisfacente delle aspettative, controintuitivo ma importante per me che, come lui, lavoro a schermo pieno almeno la metà del tempo.

Un punto dove proprio non avrei potuto farcela è la tastiera fisica; quella per l'11" è leggermente più piccola, con i tasti leggermente più ristretti. Da tempo uso la stessa tastiera per Mac e iPad e avere due formati diversi darebbe gran fastidio alla memoria dei miei polpastrelli.

Alla fine rimango non solo convinto della scelta ma anche privo di ripensamenti. Che è una bella cosa. Se iPad Pro è una macchina dove veramente si svolge lavoro fitto per un tempo significativo, undici pollici non sono abbastanza.
