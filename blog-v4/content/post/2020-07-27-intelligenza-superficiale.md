---
title: "Intelligenza superficiale"
date: 2020-07-27
comments: true
tags: [Hofstadter, Gödel, Escher, Bach, Gpt-3, Parmigiano, Reggiano]
---
[Gpt-3](https://github.com/openai/gpt-3/tree/master/dataset_statistics) è un lavoro di prima classe nell’ambito dell’apprendimento meccanizzato. Cambierà la prospettiva su molte cose, per esempio [sullo sviluppo web](https://kitze.io/posts/gpt3-is-the-beginning-of-the-end).

La sua padronanza del linguaggio naturale, che riempie in questi giorni siti e social con esempi impressionanti di articoli brevi, è uno specchietto per le allodole. Di intelligente non c’è nulla di nuovo rispetto a vent’anni fa, se non un lavoro certosino di affinamento di certi algoritmi e filtri, poderoso nello sforzo, sofisticato nella sua precisione, ben lontano dall’intelligenza artificiale.

Fossi un ricercatore, mi toccherebbe scrivere un saggio che, a partire dal sempre verde [Gödel, Escher, Bach: un’Eterna Ghirlanda Brillante](https://www.adelphi.it/libro/9788845907555), mostra come, se l’intelligenza artificiale è il Parmigiano Reggiano, Gpt-3 sia la crosta. Ripulita, grattugiata, è buonissima. Il cuore della forma è altra cosa, però.

Anche la lettura di [Concetti fluidi e analogie creative](https://www.adelphi.it/libro/9788845912528), meglio se in [originale](https://s3.amazonaws.com/arena-attachments/669097/a6e33859f5f6677f20615f14fdbf52fa.pdf) (gratis!), aiuterebbe a capire come questa, fosse intelligenza, sarebbe superficiale.