---
title: "Grandi decisioni"
date: 2021-01-28T00:24:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, Tweetbot, Aqueux] 
---
Alla luce dei [centoquattordici miliardi di fatturato Apple nel trimestre natalizio](https://www.apple.com/newsroom/2021/01/apple-reports-first-quarter-results/), adeguo la mia scala di investimento e pongo un interrogativo di lungo respiro.

Spenderesti prima [6,49 euro annuali per Tweetbot 6 iOS](https://apps.apple.com/it/app/tweetbot-6-for-twitter/id1527500834) oppure [tre euro definitivi per una raccolta di eleganti, raffinati ed evoluti sfondi scrivania dinamici](https://hector.me/aqueux), per Mac, iPhone e iPad, ispirati ai colori delle varie versioni di Mac OS X?

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*