---
title: "Ricette semplici"
date: 2020-02-14
comments: true
tags: [Homebrew, basictex, pdflatex, Pandoc, Markdown]
---
Poco tempo a disposizione, gli appunti che – quasi per riflesso – avevo scritto in [Markdown](https://daringfireball.net/projects/markdown/).

Finita la relazione, ho (finalmente!) installato [Pandoc](https://pandoc.org) via [Homebrew](https://brew.sh). Un breve istante di perplessità, in quanto Pandoc segnalava l’assenza di [pdflatex](https://www.tug.org/applications/pdftex/). Ma su [Superuser](https://superuser.com) ho trovato in pochi secondi le risposte che cercavo.

Ho installato anche pdflatex da Homebrew e, importante, ho lanciato una nuova sessione di Terminale, da una nuova finestra.

Quasi come per magia avevo, con pochi comandi infine semplici da capire, un file formato testo scritto e soprattutto formattato nel modo più veloce possibile e insieme la chiave per la sua trasformazione istantanea in documento Html, Pdf, Word, qualsiasi cosa.

La persona non pratica vedrà i paragrafi sopra come una teoria di nomi astrusi e inconoscibili, mentre si tratta di un percorso, veramente, di dieci minuti al massimo.

Qui leggono esperti veri di Markdown e Pandoc e non mi rivolgo ovviamente a loro, ma a tutti gli altri, che temono la complessità. È un piccolo apologo che vorrebbe esortarli a non avere paura, ché non è mai stato così facile lavorare con il proprio computer in modo più profondo e, in definitiva, più semplice.

Provare, provare sempre. Mai accontentarsi di quello che abbiamo sotto i polpastrelli. Si può fare meglio, si può fare in modo più elegante, si può fare e farci diventare più bravi di quello che abbiamo sempre creduto di doverci limitare a essere.