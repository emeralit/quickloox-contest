---
title: "La rosa delle ventole"
date: 2016-03-15
comments: true
tags: [Mac, iPad, web, consumo, JavaScript]
---
Mac resta acceso sulla scrivania durante la notte, perché iPad rimane sul comodino e a volte torna utile il dialogo tra le due macchine prima di dormire.<!--more-->

Ieri sera, prima di dormire, le ventole di Mac muggivano pur se lo schermo si era già spento. Incuriosito e in mutande, ho aperto rapidamente Monitoraggio Attività per capire la causa.

I primi cinque processi per consumo di Cpu erano servizi di sistema oppure pagine web. Ho chiuso queste ultime e riprovato. Ancora, servizi di sistema e *altre* pagine web (in una giornata di lavoro tipica finisco sempre per avere molte pagine aperte, che lo restano il più delle volte).

La sequenza si è ripetuta abbastanza a lungo da chiudere quasi tutte le pagine aperte. Mac si è acquietato e io sono andato a dormire.

Si noti che erano – sono – aperti tipo venticinque programmi, alcuni anche relativamente avidi.

La realtà è che decenni di sviluppo hanno portato a tecniche molto efficaci di contenimento della spesa energetica dei programmi. Mentre l’evoluzione delle pagine web, molto più giovane, si è rivolta alle prestazioni prima di tutto, ignorando l’efficienza (come d’altronde viene naturale a qualsiasi giovane).

Le pagine web sono da considerarsi da questo punto di vista programmi, né più né meno di un TextEdit o un Mail. Non deve ingannare una loro apparenza semplice: può esserci codice JavaScript dovunque. E anche pagine prive di interattività, ma piene di pubblicità, sono letali, principalmente per via del fatto che le inserzioni sono scaricate da server diversi da quello della pagina, nessuno particolarmente curato dal punto di vista della leggerezza del traffico.

Tutto questo con un uso massiccio di Safari, uno dei *browser* migliori per tutela delle batterie dei portatili. Con altri potrebbe andare peggio.

Per ogni pagina che chiudiamo, le ventole del computer rallentano molto più che se chiudiamo un programma.
