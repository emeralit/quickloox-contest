---
title: "Basta anticaglie"
date: 2024-01-17T16:07:26+01:00
draft: false
toc: false
comments: true
categories: [Internet, Education]
tags: [Venerandi, Fabrizio Venerandi, Quinta di copertina]
---
Oggi lo chef propone un [Venerandi d’annata](http://www.quintadicopertina.com/fabriziovenerandi/?p=1366) con una lunga e profonda invettiva sulla crisi valoriale della scuola e il degrado delle capacità degli studenti e sul loro rispetto per l’istituzione.

>Universale è il lamento per l’indisciplina delle scolaresche; e questo lamento non è mosso per ragione di scapataggini, proprie dell’indole vivace dei ragazzi, o per quelle che si chiamano monellerie; ma piuttosto per atti frequenti di aperta ribellione e di cinico disprezzo contro l’autorità del maestro, per l’insofferenza di qualsiasi ammonizione o consiglio, sia pure amorevole, de’maggiori, per la pretesa sfacciata di premi e di promozioni, senza alcun merito reale che le giustifichi; per la facile abitudine di attaccar brighe con chicchessia, e di riescire il tormento di chi ha la poco lieta fortuna di abitare o di passare vicino ad una scuola. Il sigaro, il turpiloquio, la bestemmia appaiono frutti già maturi sulle labbra degli adolescenti. I quali e colla sguaiataggine delle maniere e colla sconcezza de’discorsi, e col piglio arrogante del mascalzone si permettono d’insolentire contro le persone più rispettabili.

Certo, non è sua; è presa da una testata del 1884. Eppure, con un’aggiustata al linguaggio per togliere qualche espressione desueta, siamo in piena attualità.

Il pezzo è molto più lungo e, fatto forse salvo un paragrafo dedicato al servizio militare, senza gli indizi linguistici, potrebbe sembrare tutto scritto l’altroieri.

Questo è il perché, quando sento parlare dei ragazzi a scuola con il cellulare, con i tempi di attenzione brevi e tutto il resto del solito armamentario polemico da psicologo invecchiato male, mi viene da pensare che siano una generazione come le altre, con aspetti belli e meno belli, che farà grandi cose e disastri e tra qualche decine di anni si lamenterà dei nuovi esattamente come si fa ora nei loro confronti. Tutto esattamente come oggi e come ieri, quando i nostri genitori si lamentavano di noi. Tutto che torna puntuale e uguale a sé stesso. Ora di accorgersene e bollare queste storie come anticaglie, loro e chi le racconta.

E quei cellulari a scuola, facciamoglieli usare per qualcosa di serio. Metti mai che possano insegnarci qualcosa, questi sdraiati.