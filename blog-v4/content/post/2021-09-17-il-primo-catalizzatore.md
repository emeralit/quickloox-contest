---
title: "Il primo catalizzatore"
date: 2021-09-17T00:10:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Clive Sinclair, Macintosh, ZX Spectrum, Basic, Ql, Psion Chess, Apple II, Commodore 64, The Hobbit, Lords of Midnight, Lisp, Steve Jobs, Steve Wozniak, Psion] 
---
Tengo particolarmente al [ricordo di Sir Clive Sinclair](https://www.theguardian.com/technology/2021/sep/16/home-computing-pioneer-sir-clive-sinclair-dies-aged-81) perché ho usato un Macintosh nel 1984.

Prima di Macintosh avevo già usato un Apple //, che mi piaceva moltissimo. Era un computer della redazione, che durante le vacanze di Natale mi portai a casa per giocarci a [Lode Runner](https://loderunnerwebgame.com/game/). Ero affascinato.

Poi però usai un Macintosh, il primo arrivato presso la casa editrice dove lavoravo. Fu uno shock culturale: improvvisamente diventata chiaro quanto fosse possibile superare i propri limiti con l’ausilio del computer e quanto potente fosse l’interfaccia grafica per stabilire il legame necessario.

Apple // era straordinario ma non mi aveva dato questa sensazione, intensa ai limiti dello sconvolgimento.

Solo un altro computer mi ha dato la stessa sensazione: [ZX Spectrum](https://en.wikipedia.org/wiki/ZX_Spectrum). Lo comprai appena terminato il servizio militare; le vetrine dei negozi di elettronica di consumo erano pieni dei primi microcomputer, una grande novità.

Scoprii la programmazione, con il Basic dello Spectrum. Una sera andai a dormire pieno di curiosità: avevo lasciato lo Spectrum ad accendere sullo schermo pixel determinati a caso dal randomizzatore del Basic (l’ho già raccontato, ma oggi è morto Sir Clive ed è necessaria la replica). La mattina dopo ero metà incuriosito e metà deluso: lo schermo era attraversato da righe diagonali a distanza regolare. Avevo appena scoperto gli algoritmi pseudorandom.

La maggioranza dei nuovi adepti del microcomputer decantava i giochi, i colori, il sonoro di [Commodore 64](https://www.wemedia.it/commodore-64-modelli-116.html): intanto tutti i progressi significativi avvenivano su Spectrum, esattamente come in seguito la maggioranza vociante si sarebbe buttata su PC ignara del fatto che i programmi capaci di fare la storia arrivavano da Macintosh.

È su Spectrum che Psion ha pubblicato per la prima volta alcuni dei programmi più brillanti che abbia mai visto, a partire da [Psion Chess](https://second.wiki/wiki/psion_chess) (in redazione organizzai un torno di scacchi tra computer e Psion Chess si comportò mirabilmente, nonostante la potenza di elaborazione hardware a sua disposizione fosse la peggiore).

È su Spectrum che ho scoperto il linguaggio [Lisp](https://lisp-lang.org). A quei tempi si usavano le Lisp Machine, calcolatori dedicati di costo inavvicinabile a un privato. Spectrum aveva un [interprete Lisp](https://spectrumcomputing.co.uk/entry/8718/ZX-Spectrum/LISP_Interpreter) ineccepibile, persino con uno spazio di memoria per routine in linguaggio macchina ove servisse velocità.

È su Spectrum che è uscita una avventura testuale come [The Hobbit](https://worldofspectrum.org/archive/software/text-adventures/the-hobbit-melbourne-house), che faceva progressi mai visti prima nell’interpretazione del linguaggio parlato, oppure un gioco di strategia come [Lords of Midnight](https://www.gog.com/game/the_lords_of_midnight), un capolavoro di programmazione che riusciva a inserire nei poco più di quaranta chilobyte a disposizione del programma mi pare dodicimila locazioni diverse, con relativa rappresentazione grafica, per quanto stilizzata.

Non ricordo il nome del word processor migliore per Spectrum (quasi inusabile sulla tastiera di gomma), ma riusciva ingegnosamente a presentare sessantaquattro caratteri per riga in luogo dei trentadue normalmente previsti.

Il mio primo lavoro da libero professionista, la traduzione di un libro, richiedeva non solo un word processor, ma anche una tastiera almeno decente. Per poterci lavorare acquistai un [Ql](http://www.dilwyn.me.uk), che aveva una tastiera decente al minor prezzo possibile, e un word processor notevole (programmato, tu guarda, da Psion).

Clive Sinclair non aveva solo progettato microcomputer, cosa certo non banale ma praticatissima nella prima metà degli anni ottanta; aveva progettato piattaforme catalizzatrici di talento, dove programmatori prodigiosi spostavano in avanti la frontiera di quello che poteva fare il software. Esattamente quello che accadde con Macintosh. Steve Jobs fu un grande catalizzatore di genio programmatorio, ma arrivò dopo Sinclair.

Il creatore di Spectrum e Ql (per tacere di tutto il resto) era anche uno sfidante dello status quo: le sue scelte hardware erano coraggiose e a volte genuinamente folli. Anche dove erano, oltre che folli, poco indovinate (una su tutte, i *microdrive* di Ql), si distinguevano per ingegno e creatività industriale. Come le meraviglie di Steve Wozniak e degli altri guru del team Macintosh.

Se vogliamo spingere oltre il lecito il parallelo, eccone uno oltraggiosamente paradossale: Sinclair produsse effettivamente l’auto elettrica, cui si dice che Apple stia lavorando da anni.

La quantità di cose che amo dell’informatica e che derivano dal lavoro di Sir Clive è davvero alta e a lui debbo molto della mia formazione.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*