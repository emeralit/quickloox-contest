---
title: "Acquisti che durano"
date: 2014-02-13
comments: true
tags: [iPad, Which, Android]
---
Uno si chiede perché dovrebbe comprare un costoso iPad Air, quando là fuori è pieno di attraenti Samsung che *fanno tutto quello che fa iPad* e *alla fine sono tutti uguali*.<!--more-->

In Inghilterra, *Which* si è preso il disturbo di collaudare la batteria delle tavolette in vendita, con un [doppio test](http://blogs.which.co.uk/technology/tablets-2/ipad-air-reigns-supreme-for-tablet-battery-life/) di visione filmati e navigazione Internet.

Sull’uso di Internet, il podio è occupato da iPad Air, iPad mini Retina e iPad 2, con 658 minuti, 614 minuti, 590 minuti. Galaxy Note 10.1 edizione 2014: 483 minuti.

Con i video, iPad Air vince a 777 minuti. Secondo Kindle Fire Hdx 8.9 di Amazon (714), terzo Nexus 7 di Google (669). iPad 2 segue al quarto posto con 660 minuti e iPad mini Retina si ferma a 604. Galaxy Note 10.1 edizione 2014: 465 minuti.

Oggi sono in Piemonte a parlare di creatività e innovazione allo [stabilimento Alstom di Savigliano](http://www.alstom.com/italy/locations/transport/savigliano/), provincia di Cuneo. Voglio partire la mattina, tornare la sera e dimenticarmi l’alimentatore dentro lo zaino. Per questo scelgo un iPad.