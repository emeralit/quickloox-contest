---
title: "Libertà di limite"
date: 2021-01-27T01:39:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Matt Birchler, Hey, Apple] 
---
Matt Birchler [commenta un anno di abbonamento](https://birchtree.me/blog/how-im-using-hey-email-almost-one-year-later/) al servizio di posta [Hey](https://hey.com) e osserva qualcosa di interessante rispetto all’obbligo di usare la app propria del servizio:

>[Niente app indipendenti, sei matto?] Può essere, ma in questo caso ritengo sia una questione di limite che diventa una forma di libertà. Quando usavo Gmail e Outlook come fondamento della mia infrastruttura di posta, avevo la mia scelta della app. Apple Mail, Spark, Outlook, Gmail, Airmail, Edison, Blue, Newton, Spike, Polymail… […] Spark veniva aggiornata e la usavo. Poi Outlook faceva qualcosa di nuovo e tornavo lì. Poi però aveva un bug e allora mi mettevo su Apple Mail, fino a quando mi annoiavo e tornavo a Spark e il cerchio si chiudeva per cominciare daccapo.

>Era scelta? Assolutamente sì, ma era una cosa buona per la mia posta? Neanche un po’.

Se penso a quanti dibattiti attorno al tema *che app di posta dovrei usare*, ora mi rendo (più) conto di quanto fossero vacui.

Se penso alle molte volte in cui Apple ha limitato la scelta di opzioni in un computer o in un sistema operativo, capisco (meglio) la logica dietro la decisione.

Certo, a volte non funziona bene. Lo riconosce anche Birchler:

>Se la app di Hey fosse tremenda avrei un problema serio, ma non solo sa il fatto suo; è la app di posta più affidabile che abbia mai usato e viene tenuta aggiornata e migliorata più velocemente della maggior parte delle altre.

Se la scelta è unica, ma ben pensata e ben realizzata, vale più libertà di chi vuole scegliere tra mille alternative di bassa qualità.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*