---
title: "Le frontiere dello sviluppo"
date: 2018-10-01
comments: true
tags: [Magic, Trackpad, Linux, Google]
---
Curiosa la notizia che a quasi tre anni di distanza dalla presentazione sia imminente il [supporto di Magic Trackpad 2 da parte di Linux](https://www.phoronix.com/scan.php?page=news_item&px=Linux-Apple-Magic-Trackpad-2).

Ancora più curioso che lo si debba a un ingegnere di Google. Il mondo open va così.

Da notare soprattutto che, guardando questa [pagina GitHub](https://github.com/torvalds/linux/pull/332), si noti un buon numero di persone effettivamente interessate alla cosa. Non si tratta del virtuosismo fine a se stesso di un programmatore, ma aiuterà un po’ di gente.

Gente che, se non tutti almeno a maggioranza, disporranno di un Mac. Sul quale, sempre la metà più uno di sicuro sta usandolo per lavoro, probabilmente per sviluppo software.

Potrebbe essere l’indicatore più indiretto della diffusione professionale di Mac che abbiamo trovato finora.