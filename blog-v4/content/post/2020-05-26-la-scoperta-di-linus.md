---
title: "La scoperta di Linus"
date: 2020-05-26
comments: true
tags: [Linux, Linus, Torvalds, Intel, Amd, Arm, iOS, Apple, Wintel]
---
Certo che Apple passerà a processori Arm appena potrà, per la ragione che Linus Torvalds (creatore di Linux, uno dei programmatori più importanti della storia) [ha aggiornato il proprio computer principale](http://lkml.iu.edu/hypermail/linux/kernel/2005.3/00406.html).

>In effetti, la cosa più entusiasmante della mia settimana è stata aggiornare il mio computer principale che, per la prima volta in quindici anni, non è basato su processore Intel. No, non sono ancora passato ad Arm, ma adesso sto maneggiando un Threadripper 3970x AMD. I miei build di test 'allmodconfig' ora sono veloci il triplo di prima, cosa abbastanza irrilevante nei periodi di calma, ma che si farà certamente sentire durante la prossima fase periodica di merge.

A parte il gergo, che riguarda l’aggiornamento del *kernel* di Linux, la situazione è chiara.

Apple era passata a Intel quando (semplifico) Ibm non riusciva più a progettare processori portatili competitivi. È ora delle nemesi. Intel fatica a stare dietro la concorrenza in vari campi e non ha la situazione di monopolio privilegiato dei tempi felici del cartello Wintel, quando poteva vendere agli sprovveduti i megahertz del processore come se fossero stati una misura di velocità, anche perché con Microsoft si adoperava per abbattere qualunque alternativa.

Per chi abbia avuto la fortuna di essere nato più tardi: è come se oggi si vendessero orologi al quarzo *più veloci* degli orologi meccanici, visto che questi ultimi spaccano il secondo [in circa quattro vibrazioni](https://www.gq.com/story/watch-glossary-frequency) e invece i primi ne usano [trentaduemilasettecentosessantotto](https://www.explainthatstuff.com/quartzclockwatch.html).

Intel ovviamente è un colosso. Ci vorranno anni perché si ridimensioni veramente. Succederà. Di Linus, quando parla di processori, ci si può fidare.