---
title: "La palestra dell’ardimento"
date: 2014-03-07
comments: true
tags: [Mac]
---
Il computer è una macchina generica, che ha cambiato la storia del mondo esattamente perché, al contrario di qualsiasi altro strumento inventato dall’umanità, ha capacità potenziali illimitate. Una forbice taglia, un martello picchia, un [30th Anniversary Super Tool 300 Leatherman](http://www.leatherman.com/831912.html) contiene diciannove strumenti, né diciotto né venti; un computer aspetta solo di essere programmato e se domani si inventa una nuova possibilità, lui può aggiungerla.

Se vale questa premessa, il computer è diverso dal luogo che ci consente di applicare per tutta la vita undici azioni che abbiamo imparato a memoria, solo quelle, per sempre.

Proprio perché il computer è infinitamente proteiforme, domani i flussi di lavoro possono cambiare, in meglio o anche in peggio; l’evoluzione passa anche dai vicoli ciechi e dagli errori.

Quello che ci si aspetta da noi al momento del cambiamento è ciò che si attende dalla razza umana a partire da centomila anni fa: trovare il modo migliore di sfruttare il cambiamento in atto.

Se tutto dovesse restare come lo abbiamo imparato la prima volta, ci alzeremmo la mattina nelle nostre caverne pronti per una giornata a raccogliere bacche e sfuggire ai predatori.

Semmai la perdita di un flusso di lavoro cui teniamo molto dovrebbe spingerci a sviluppare capacità superiori che ci consentano di ripristinarlo. Oppure accendere la creatività per trovare un flusso di lavoro ancora migliore di prima. Il computer è un luogo per osare; per adagiarsi c’è il divano.