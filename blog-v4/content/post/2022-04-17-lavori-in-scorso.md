---
title: "Lavori in scorso"
date: 2022-04-17T00:29:04+01:00
draft: false
toc: false
comments: true
categories: [Software, Web]
tags: [blog, QuickLoox, Wayback Machine, Script, Playmedia, iCreate, Mimmo, Internet Archive]
---
Solo una nota pasquale per dire che stiamo lavorando a portare online gli antichi post dell’epoca Ping… e anche quelli di Script, la parentesi di blogging effettuata per l’editore Playmedia ai tempi del magazine iCreate.

Pensavo che fossero persi, ma Mimmo sta compiendo un lavoro spettacoloso su [Internet Archive](https://archive.org/) e recuperando tutto il recuperabile.

Siccome ci sto prendendo gusto, una volta visto quanto si è recuperato proverò a chiedere se qualcuno tenga in tasca altri vecchi post. Ma ogni cosa a suo tempo. E anche il passato diventerà presente su queste pagine.