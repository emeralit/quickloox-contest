---
title: "Un sabato italiano"
date: 2022-07-10T01:56:31+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Fano, Balì, Museo del Balì]
---
In visita al [Museo del Balì](https://www.museodelbali.it), veramente raccomandato nonostante mi risulti praticamente sconosciuto fuori dai dintorni, e poi cena in direzione del porto, con musica dal vivo.

![A sinistra tastierista/vocalist femmina, a destra percussionista/vocalist maschio, in mezzo un Mac](/images/fano.jpg "Un trio bene assortito")