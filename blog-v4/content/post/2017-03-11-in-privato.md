---
title: "In privato"
date: 2017-03-11
comments: true
tags: [Slack]
---
Una delle decisioni più positive che ho preso ultimamente attorno a questo blog è stata aprire il canale Slack personale cui [ho invitato](https://macintelligence.org/posts/2016-09-30-nuovi-canali/) tutti gli interessati tempo fa.<!--more-->

Si è rivelato un porto sicuro, dove l’ambiente rilassato e il traffico modesto generano stress zero e in compenso saltano fuori risorse e occasioni di apprendimento come mai prima.

Colgo l’occasione per ringraziare di cuore tutti gli iscritti al canale per l’opportunità che mi concedono. Non faccio classifiche né elenchi e neppure attribuzioni, perché non è questione di primi della classe o di competizione fine a se stessa.

Un solo esempio: non avrei mai altrimenti letto questo [eccezionale documento](http://plain-text.co/) di consigli diretti agli studenti universitari di scienze sociali su come attrezzarsi per scrivere e elaborare le loro ricerche. Sì, tre quarti dell’umanità è appiattita su Office, ma per produrre documentazione di caratura scientifica si usano strumenti professionali, non programmi di videoscrittura. Tiè.

Le iscrizioni al canale Slack sono sempre possibili, basta chiedermi un invito e indicarmi un indirizzo di posta cui spedirlo. Avvertenza: è un luogo di positività e propositività. Si discute anche, ma molto serenamente e appunto per fare un passo avanti invece che due indietro. Ho avvisato.
