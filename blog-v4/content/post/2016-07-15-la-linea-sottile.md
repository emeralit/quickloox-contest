---
title: "La linea sottile"
date: 2016-07-15
comments: true
tags: [Android, iOS]
---
Il commento di oggi è affidato a **Stefano**.

**Prendo spunto dalle ultime notizie** sulla beta di iOS 10 e dall’ultimo Keynote al WWDC dove hanno presentato Siri in macOS Sierra: a mio avviso la direzione ora è chiarissima.

Compri Apple: [capex](https://it.wikipedia.org/wiki/Capex) iniziale molto alto ma entri in un ecosistema che ti procura tutti i servizi (alcuni non ancora all'altezza di Google, ma ci arriveranno) senza profilazione e con piena sicurezza in tema di *privacy*. Le ricerche si stanno spostando sempre più sull’OS senza utilizzare quella di Google. Il business non sono i tuoi dati.

Compri Android: *capex* iniziale molto basso con tutti i servizi Google gratuiti. Sei costantemente profilato e i tuoi dati sono venduti come merce di scambio. Il business sono i tuoi dati.

**Con chi vogliamo stare?**

Un caso in cui la linea sarà pure sottile, ma anche più netta del solito.

*[Il [più-che-libro](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/) Cuore di Mela conterrà spiegazioni e informazioni dettagliate a beneficio di chi ha a cuore la riservatezza dei propri dati. C’è solo da [concretizzarlo](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac) con il passaparola e il sostegno fattivo.]*