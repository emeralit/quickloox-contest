---
title: "L’ultimo PostScript"
date: 2023-10-01T01:35:23+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [PostScript, Sonoma]
---
Apple ha completato con Sonoma il processo di [smantellamento della compatibilità di macOS con PostScript iniziato con Catalina nel 2019]((https://macdailynews.com/2023/09/28/postscript-hits-the-end-of-the-road-in-macos-sonoma/)). Il sistema operativo infatti non offre più la conversione in PDF dei file PostScript.

Mac spesso abbandona vecchie tecnologie in spregio al resto del mondo che continua a supportarle, però con PostScript non è questo il caso. Le mosse di Apple sono state praticamente parallele a quelle di Adobe, che per esempio nel 2021 ha levato da Photoshop la compatibilità con i font Type 1, quelli usati inizialmente in PostScript.

Nello stesso anno [è mancato Charles Geschke](https://appleinsider.com/articles/21/04/19/adobe-co-founder-charles-geschke-dies), inventore di PostScript e cofondatore di Adobe, e sembra quasi che l’azienda abbia aspettato che se ne andasse l’autore, per rispetto, prima di svecchiare.

Il linguaggio era elegante e potente e naturalmente è stato un pilastro del desktop publishing: dentro le stampanti laser ha portato la tipografia nell’informatica, ha messo chiunque in condizione di potersi inventare autore o editore, ha dato una spinta vitale e decisiva ad Apple e a Macintosh. Poi era magico: nessuno si rendeva conto di scrivere un documento e, nello stamparlo, inviare alla laser un *programma*.

Display PostScript, la versione per disegnare oggetti e testo sullo schermo, sarebbe stata un traguardo formidabile: l’identità tra video e carta, il sogno realizzato della promessa *What you see is what you get*. Purtroppo Adobe in quella occasione ha raggiunto il suo limite: sul video la tecnologia era troppo lenta (tecnologico) e Apple non voleva cedere il controllo del rendering su Mac a una società terza (politico). Microsoft? In quegli anni scheggiavano le selci per fare punte di freccia.

E poi problemi di sicurezza e poi il fatto che Type 1 aveva anche lui bei limiti.

PDF deriva da PostScript, il sistema di rendering dello schermo Quartz di Apple deriva dal Pdf; la sua eredità è immensa e PostScript naturalmente continua a vivere, nelle stampanti e in tutti i sistemi che lo sostituiscono su Mac e altrove, come [GhostScript](https://www.ghostscript.com). Quindi come commiato è anche contenuto.

P.S.: io comunque un software di impaginazione grafica e controllo del testo scritto in PostScript lo avrei accolto con grande favore.