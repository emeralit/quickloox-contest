---
title: "Iperpropulsione e testo"
date: 2019-01-21
comments: true
tags: [Drang, Gruber, BBEdit, regex, Friedl]
---
Dr. Drang [esorta ad abbracciare le espressioni regolari](https://leancrew.com/all-this/2019/01/dont-fear-the-regex/) (da qui in poi *regex*) con un post eccellente che meriterebbe la traduzione integrale e che, nella tradizione dell’autore, mette a posto ogni cosa con pochi paragrafi ineccepibili.

Le *regex* sono come il [gioco del Go](https://www.britgo.org/intro/intro2.html): straordinariamente difficili ad alto livello, semplicissime in partenza, materia perfetta per imparare dato che si possono esplorare poco per volta ed è possibile ottenere ottimi risultati anche con poche nozioni elementari (il che equivale grosso modo alla possibilità di giocare a Go su scacchiere molto piccole).

Drang consiglia anche le cose giuste. Il libro *Mastering Regular Expressions* è obbligatorio sullo scaffale di uno specialista, ma duro da leggere e molto più lungo di quanto serva alla maggior parte di noi, che invece ha imparato – come Drang stesso – sul [manuale utente di BBEdit](https://s3.amazonaws.com/BBSW-download/BBEdit_12.5.2_User_Manual.pdf#page182), dove c’è un capitolo dedicato. Probabilmente è la migliore esposizione del tema esistente fuori da *Mastering Regular Expressions*

La mia conoscenza del tema regex è superficiale, ma mi ha cambiato la vita lavorativa in più occasioni. Una di quelle più eclatanti è stata la realizzazione delle *pagine gialle di Internet* per l’editore Tecniche Nuove. Di fatto un database di URL interessanti completi di descrizione.

Per tutta una serie di ragioni, non era il caso di usare un database vero e proprio, anche se la struttura logica del tomo era effettivamente quella di una serie di *record* composti dagli stessi campi. Inizialmente utilizzai un foglio elettronico con un record su ciascuna riga e un campo in ciascuna colonna, solo che le possibilità di editing del testo erano limitate e scomode.

Alla fine maneggiavo tutto con un file BBEdit, una riga per record e campi separati da tabulatori. Avevo messo a punto regex assai rozze, ma funzionali, per molti scopi, per esempio intervenire su un campo dentro tutti i record lasciando inalterato il resto del testo. L’equivalente di selezionare una colonna dentro il foglio di calcolo, fatto invece in BBEdit.

Un detto simpatico, citato anche da Drang, sancisce che, se hai un problema e pensi di risolverlo a suon di regex, hai due problemi. Più seriamente, le regex non sono la marcia in più di chi lavora con il testo. Sono l’iperpropulsione a razzo.