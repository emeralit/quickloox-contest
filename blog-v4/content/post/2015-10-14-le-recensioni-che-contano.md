---
title: "Le recensioni (che) contano"
date: 2015-10-14
comments: true
tags: [Viticci, Mann, foto, fotografia, iPhone, 6, 6s, Svizzera, editoria]
---
Tanto di cappello a [Federico Viticci](https://twitter.com/viticci) per avermi [fatto scoprire](https://www.macstories.net/linked/austin-manns-iphone-6s-camera-review-in-switzerland/) una [eccezionale recensione](http://austinmann.com/trek/iphone-6s-camera-review-switzerland), ma soprattutto Austin Mann, straordinario recensore della fotovideocamera di iPhone.<!--more-->

Durante un viaggio Mann ha prodotto migliaia tra foto, video, panorami e dispone di tutte le nozioni tecniche necessarie per giudicare la bontà del lavoro dell’obiettivo.

Oltre a questo sa spiegare in modo chiaro ed esauriente le cose che contano anche a una persona non tecnica e condisce la spiegazione con esempi piacevoli da guardare e confrontare con quelli prodotti da iPhone 6.

In altre parole, quando uscirà iPhone 7 il prossimo anno, la parola da attendere sulla qualità fotografica sarà quella di Mann.

Da ricordare, visto che [se ne parlava tempo fa](https://macintelligence.org/posts/2015-09-18-ok-la-crisi-pero), quando si accenna alla crisi dell’editoria. Quale rivista pubblica, *offline* oppure *online*, una recensione così?
