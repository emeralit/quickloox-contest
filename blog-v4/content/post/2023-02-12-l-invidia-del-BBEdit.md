---
title: "L’invidia del BBEdit"
date: 2023-02-12T00:14:33+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Drang, BBEdit, AppleScript, scripting]
---
Lo hardware è un formidabile equalizzatore. Un Mac può costare molto, o molto meno, avere tanto spazio o poco, uno schermo grande o uno schermo piccolo. Alla fine però è un Mac, con i pregi e i difetti del Mac, per il ricco e per il povero.

Il software è il più positivo generatore di disuguaglianze che esista. Dr. Drang e io abbiamo comprato [BBEdit](https://www.barebones.com/products/bbedit/) allo stesso prezzo e abbiamo identiche funzioni a disposizione.

Però lui può trasformare BBEdit nella sua macchina perfetta per pubblicare sul proprio blog. Ha praticamente un menu supplementare dedicato, con tanto di [note a pié di pagina in Markdown](https://leancrew.com/all-this/2023/02/markdown-footnoting-in-bbedit/).

Io invece devo fare ancora un bel po’ di strada. E se c’è disuguaglianza, c’è merito. Tutto suo.