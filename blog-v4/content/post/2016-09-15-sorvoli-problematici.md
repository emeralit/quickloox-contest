---
title: "Sorvoli problematici"
date: 2016-09-15
comments: true
tags: [Samsung, Stefano, Galaxy, S7]
---
Sono giorni in cui ringrazio spesso **Stefano** e stavolta più degli altri, grazie a questo *scoop* sull’avviso inviato da una [linea aerea indiana](http://spicejet.com) *low cost*.<!--more-->

 ![Note 7 spento please](/images/no-note-7.jpg  "Note 7 spento please") 

L’avviso si aggiunge alle [iniziative](http://uk.businessinsider.com/airlines-faa-samsung-galaxy-note-7-restrictions-7-2016-9) che stanno prendendo più compagnie aeree in America e in Europa. Essenzialmente, obbligo di tenere spenti i Note 7 e divieto di caricarli.

<blockquote class=twitter-tweet data-lang=en><p lang=en dir=ltr>Flight attendant issues stern instructions not to use or charge Samsung Galaxy Notes. Oof. <a href=https://t.co/FOISqC9EiE>pic.twitter.com/FOISqC9EiE</a></p>&mdash; Henry Blodget (@hblodget) <a href=https://twitter.com/hblodget/status/775288978660552704>September 12, 2016</a></blockquote> <script async src=//platform.twitter.com/widgets.js charset=utf-8></script>

Eravamo finalmente vicini all’indifferenza delle compagnie aeree nei confronti della tecnologia a bordo.

Ora, grazie a Samsung, si può dire davvero che scoppia nuovamente la prudenza. Qui [un bambino](http://nypost.com/2016/09/11/recalled-samsung-phone-explodes-in-little-boys-hands/), là [una signora](https://www.thesun.co.uk/news/1772176/samsung-s7-edge-overheated-and-then-exploded-in-teachers-hands-in-the-middle-of-busy-cafe/), vicino [una mamma](http://www.dailymail.co.uk/news/article-3496685/Samsung-phone-EXPLODES-mother-three-year-old-child.html), dentro [un hotel](http://www.express.co.uk/life-style/science-technology/708018/Samsung-Galaxy-Note-7-Explode-Pictures-Damage-Refund-UK), ti distrai un momento e [la tua jeep va a fuoco](http://bgr.com/2016/09/08/note-7-explode-fire-recall-florida-samsung/); ovunque una deflagrazione di attenzione verso i computer da tasca. Con toni surreali: le unità esplose, dice Samsung, sono *solo* ventiquattro su un milione (è un numero enorme rispetto a qualsiasi altro incidente di questo tipo). I nuovi modelli [sono marchiati](http://www.samsung.com/au/galaxynote7-notice/) in modo da essere distinti da quelli vecchi e riconosciuti come non esplosivi. Al che viene spontanea la battuta: ecco perché esplodono, si erano dimenticati il marchio… (in realtà un errore di produzione raro produce un cortocircuito nella batteria).

<blockquote class=twitter-tweet data-lang=en><p lang=en dir=ltr>.<a href=https://twitter.com/united>@united</a> issues warning to passengers about Samsung Galaxy Note 7 devices in the gate area. <a href=https://t.co/az3u76aJnj>pic.twitter.com/az3u76aJnj</a></p>&mdash; AirlineGeeks.com (@AirlineGeeks) <a href=https://twitter.com/AirlineGeeks/status/774623694958239744>September 10, 2016</a></blockquote> <script async src=//platform.twitter.com/widgets.js charset=utf-8></script>

Se non fosse che ci rimettono persone infortunate anche seriamente, verrebbe da farsi una risata amara e sorvolare su un brutto inciampo.

Invece non si può più neanche volare tranquilli se il vicino ha un coso Samsung acceso e la protagonista di questo successone vola bassissimo: alle vittime di un telefono esplosivo, in un impeto di generosità, regala *un altro telefono*. Come se quello prima non si fosse già distinto a sufficienza.

<blockquote class=twitter-tweet data-lang=en><p lang=en dir=ltr>.<a href=https://twitter.com/samsung>@Samsung</a> shares are sinking by 6% due to <a href=https://twitter.com/hashtag/GalaxyNote7?src=hash>#GalaxyNote7</a> recall and airline bans (eg. this notice from my flight yest) <a href=https://t.co/pj1HSFozjE>pic.twitter.com/pj1HSFozjE</a></p>&mdash; Leisha Chi (@BBCLeishaChi) <a href=https://twitter.com/BBCLeishaChi/status/775167472752218113>September 12, 2016</a></blockquote> <script async src=//platform.twitter.com/widgets.js charset=utf-8></script>