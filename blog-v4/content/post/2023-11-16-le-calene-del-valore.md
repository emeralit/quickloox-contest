---
title: "Le catene del valore"
date: 2023-11-16T13:13:05+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Ars Technica, Android, Samsung, Apple]
---
Samsung riceve due miliardi di dollari l’anno da Google perché quest’ultimo sia il motore di ricerca preimpostato sui *device*.

Le società che producono telefoni, in generale, tipo Oppo o Motorola o Sony, ricevono da Google il dodici percento del fatturato derivante dalla ricerca.

Apple riceve da Google diciotto miliardi l’anno più il trentasei percento del fatturato della ricerca.

Fa quasi impressione vedere questi dati, divenuti pubblici solo grazie a una serie di cause in tribunale di cui [riferisce Ars Technica](https://arstechnica.com/gadgets/2023/11/googles-36-search-revenue-share-with-apple-is-3x-what-android-oems-get/). Al bar si confrontano due *device* da come stanno in mano, il colore, questo passa i file vie Bluetooth, ha la app di serie per ricordarsi dove hai parcheggiato. Il rettangolo dove digitare la ricerca ha un valore vertiginosamente diverso da una piattaforma a un’altra e questo dice qualcosa anche sul valore integrale della piattaforma. *Fa le foto belle al buio* è un giudizio che sta su altri gradini della scala.

Nell’articolo si racconta diffusamente della situazione e anche dei fattori che influenzano queste quotazioni: uno degli elementi di valutazione di Google è il danno che riceverebbe da Apple nel momento in cui questa decidesse di cambiare motore di ricerca o, peggio ancora, farsi il proprio; il loro precedente sono le mappe.

Quando Apple lanciò le proprie mappe, il traffico su quelle di Google diminuì del sessanta percento, con una riduzione di fatturato per forza consistente. E Google è disposta a pagare pur di mantenere fonti di fatturato importanti.

Un iPhone costa a volte qualcosa in più di un Android equivalente, tuttavia il suo valore effettivo – agli occhi di Google – è di vari multipli. E Google, piuttosto che perdere il privilegio della ricerca su iPhone, si incatena.

Se domani Oppo passa a Bing, sì, va beh, dispiace, un segnale preoccupante, ma anche un po’ vaffa e la vita continua.