---
title: "Suck e no suck"
date: 2022-11-26T19:20:15+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Dock, suck, Ventura, Terminale, Scala, Genio]
---
Mi è venuta voglia di ripristinare l’effetto nascosto *risucchio* delle finestre in movimento da e per il Dock; mi piace più degli effetti Genio e Scala presenti nell’interfaccia.

Per farlo, si scrive nel Terminale

`defaults write com.apple.dock mineffect suck; killall Dock`

Solo che, in Ventura, sembra non esserci più la possibilità di vedere il movimento al rallentatore, se si preme il tasto maiuscole. Avere il rallentatore è l’intera ragione di tutto il traffico necessario a configurare l’effetto.

A meno, ho scoperto, di riabilitare la funzione con

`defaults write com.apple.dock slow-motion-allowed -bool YES && killall Dock`

e così avere un effetto *suck* che… *doesn’t suck*.