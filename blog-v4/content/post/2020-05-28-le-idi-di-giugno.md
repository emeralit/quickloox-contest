---
title: "Le idi di giugno"
date: 2020-05-28
comments: true
tags: [Covid-19, coronavirus, Mongolia, Francia, Immuni]
---
La Francia [è sulla strada per varare la propria applicazione di tracciamento contatti](https://www.euronews.com/2020/04/29/coronavirus-french-mps-approve-covid-19-tracing-app-despite-privacy-concerns) e il dibattito ferve.

Lo trovo irrilevante. Da un punto di vista dell'utilità, quando devo rispondere a qualcuno nel più breve tempo, dico _andava fatta a marzo_. Si discute sulle percentuali di adozione necessarie in modo veramente inutile. Se proprio, quanto vale una vita? Ove [Immuni](https://github.com/immuni-app) avesse salvato una singola vita, per me avrebbe ripagato ogni sforzo.

Il punto non è neanche questo. La Francia fa la app e la condanna da subito all'insuccesso, perché non adotta [ExposureNotification](https://developer.apple.com/documentation/exposurenotification) e di conseguenza su iOS non funziona in background. [Ciao pepp](https://macintelligence.org/blog/2020/04/20/ciao-pepp-pt/).

A quest'ultimo link sta la spiegazione, perché non l'ho aggiornato (anche se l'aggiornamento si trova nei commenti). Alla Francia non interessa che la app funzioni per i cittadini; deve funzionare per lo stato. Raccogliere dati per centralizzarli e da lì chissà. All'Italia magari sarebbe interessato, ma a governare è gente pavida e ignorante ancora più dei francesi e così hanno – fortunatamente per noi – scelto la strada giusta per paura di perdere consenso.

A questo proposito, tutti si spera che i contagi continuino a calare e che il livello di normalità aumenti. Dopo di che veramente tutti, al governo, nelle regioni, nei comuni, dovrebbero prendere atto di avere sulla coscienza abbastanza morti da riempire uno stadio e lasciare le loro poltrone, per sempre. Chi ne ha di più, chi ne ha di meno, ma anche qui, non riuscirei a dormire la notte se ne avessi uno, solo uno, non cento o mille. Quanto vale una vita?

La Mongolia non è più quella delle barzellette. Ha una capitale da un milione e mezzo di abitanti con densità abitativa comparabile a quella di Bergamo e collegamenti continui in aereo, treno, auto con Cina e Russia, due nazioni che di coronavirus possono dire qualcosa.

Attualmente la Mongolia ha centoquaranta casi di Covid-19, tutti importati, tutti sotto controllo. Il tasso di trasmissione della malattia è zero. I morti sono zero. I nostri, ufficiali, più di trentaduemila.

Come hanno fatto? Con il _lockdown_. Solo che [lo hanno iniziato il 23 gennaio](https://medium.com/@indica/covid-underdogs-mongolia-3b0c162427c2). _Con zero casi interni_. E poi hanno unito al lockdown un'azione aggressiva e preventiva per dare zero chance al virus.

I nostri campioni di destra, di sinistra, maggioranze e opposizioni, di tutti i colori e tutti gli odori, un mese dopo il 23 gennaio dovevano ancora capire.

Che almeno ci diano la app, non importa quanto serva. È un argomento di conversazione, mostra un governo che ha calato le brache davanti ai sostenitori delle libertà individuali e questo deve essere motivo di soddisfazione. Che arrivi alle idi di giugno, o luglio, ma che arrivi.

Così poi torniamo a cose più interessanti, come [i font nascosti di Catalina](https://blog.chrishannah.me/the-awesome-mac-os-catalina-fonts-you-didnt-know-you-had-access-to/) e poi ovviamente [WWDC online](https://macintelligence.org/posts/2020-04-24-questa-non-me-la-perdo).