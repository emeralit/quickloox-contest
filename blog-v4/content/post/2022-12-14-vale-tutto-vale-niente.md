---
title: "Vale tutto, vale niente"
date: 2022-12-14T02:14:03+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [ChatGPT, Facebook, Venerandi, Fabrizio Venerandi]
---
Si svolgono interessanti dibattiti sulla natura di [ChatGPT](https://chat.openai.com/) e se possa veramente essere considerato intelligenza artificiale.

Io dico di no, altri – [Fabrizio Venerandi](https://www.facebook.com/fabrizio.venerandi/posts/pfbid029NW9MLVzxBemtZqFbz9AmNybMfgkcXe9Tf4Y46eTxuUatbk7QgZerDyCAv5v9E1Ul?comment_id=487246170178397&notif_id=1670910753085552&notif_t=feedback_reaction_generic&ref=notif) per esempio – è più per il sì, ma non è questo il punto.

A me preoccupa che, per rispondere a certe mie obiezioni, Venerandi faccia dire a ChatGPT:

>No, non sono un sistema esperto degli anni novanta.

E io, per ribattere, impieghi una manciata di minuti per fargli dire:

>Sì, in pratica posso essere considerato un sistema esperto con una maggiore comprensione del linguaggio naturale.

Che non è proprio l’esatto contrario, però insomma.

Ovvero: ChatGPT, un sistema che genererà tonnellate di contenuti presi nella loro stragrande maggioranza per buoni senza alcuna verifica, è *manipolabile*.

Se uso un computer, a volte, è proprio per avere la certezza che un certo calcolo sia preciso o che una data informazione sia sufficientemente confermata.

Con la generazione del testo affidata alla sedicente intelligenza artificiale, qualunque appiglio scompare. Potremmo avere davanti qualsiasi cosa.

Vale tutto. Quindi, non vale niente.

(Per chi non avesse voglia di seguire il link verso Facebook, qui c’è una veloce rappresentazione grafica dei dialoghi con il programma).

![Chiacchiere con ChatGPT](/images/esperto.png "Non sono un sistema esperto. Anche se in effetti lo sono…")
