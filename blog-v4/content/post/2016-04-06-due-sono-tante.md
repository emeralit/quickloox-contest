---
title: "Due sono tante"
date: 2016-04-06
comments: true
tags: [Tesla, Apple]
---
Si sa che certe code fuori dagli Apple Store hanno generato sarcasmo e cattiverie nei confronti di chi le popolava e in quelli di chi possiede lo store.<!--more-->

Vorrei sentire qualcuno di questi specialisti commentare il lancio dell’auto elettrica Tesla Model 3, che uscirà probabilmente tra due anni; tuttavia in 276 mila hanno anticipato mille dollari per essere i primi della lista. Speso mille dollari, non fatto una coda.

Se qualcosa succede per Apple, si può sempre argomentare che è una religione di fanatici; se succede per Apple *e per altri*, bisogna trovare motivazioni più convincenti.

Confrontare il lancio di iPhone con il lancio della Tesla 3 viene di un istintivo pazzesco. C’è poi tutta una serie di considerazioni più razionali, che invito a leggere su [Stratechery](https://stratechery.com/2016/its-a-tesla/).