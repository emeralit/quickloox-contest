---
title: "Le cose in prospettiva"
date: 2016-07-13
comments: true
tags: [ macOS, Sierra]
---
*Ars Technica* ha dedicato buon spazio e attenzione alla *beta* di macOS Sierra e il suo [lungo articolo](http://arstechnica.com/apple/2016/06/the-macos-sierra-developer-preview-different-name-same-ol-mac/1/) è la cosa da leggere in questo momento per chiarirsi le idee.

Giusto due paragrafi verso la fine:

>Quando Mac OS X (come veniva chiamato) è passato a un ciclo di pubblicazione annuale, è come se Apple avesse faticato un poco a definire il giusto campo di azione per ciascuna nuova edizione. Lion, il primo della serie e il primo a mutuare un numero significativo di funzioni da iOS, in retrospettiva sembrava una versione finita a metà di Mountain Lion; Mavericks ha eliminato parte degli scheumorfismi delle edizioni precedenti ma ha raggiunto il passo di iOS solo un anno dopo, con Yosemite.

>Da Yosemite, le cose appaiono più controllate e pianificate. El Capitan e Sierra presentano ambedue una o due funzioni “pilastro” su cui basare il marketing (gestione delle finestre in El Capitan, Siri in Sierra), un insieme apprezzabile di novità di media portata, almeno un cambiamento importante sotto il cofano (System Integrity Protection in 10.11, Gatekeeper in 10.12 e [il nuovo filesystem] APFS l’anno prossimo se tutto va bene), più migliorie minori alle app base.

È giusto avere aspettative, ma anche a metterle in prospettiva rispetto a quello che gli ingegneri Apple sono davvero in grado di fare – bene – in un anno di lavoro, che non sono dieci.

*[Va da sé che Sierra sarà cittadino di prima importanza nel [progetto editoriale di Akko](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/), che continua a chiedere [passaparola e sostegno](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac) per divenire realtà.]*
