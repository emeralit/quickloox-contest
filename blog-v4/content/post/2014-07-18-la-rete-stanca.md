---
title: "La rete stanca"
date: 2014-07-18
comments: true
tags: [Ikea]
---
Quando non funziona, quando è fatta male, quando chi dovrebbe fornire un servizio non capisce, quando ti senti un salmone a valle della cascata e sopra c’è l’orso che aspetta.<!--more-->

Sto cambiando *provider* Internet e, dopo quarti d’ora passati a concordare il cambio in modo da minimizzare l’interruzione di servizio, sono senza rete per un numero di giorni imprecisato.

Poco male, c’è la chiavetta di riserva che funziona benissimo. Solo che il mio Mac per motivi ignoti si rifiuta di attivare una condivisione Internet funzionante.

Poco male, si mette la chiavetta sul Mac di fianco che invece non batte ciglio. Ma intanto se ne è andata una mezz’ora di prove e controprove.

Fino a che la giornata termina e si va all’Ikea per acquisti prioritari e una meritata pausa al ristorante svedese, per godere anche della novità: il Wi-Fi per i detentori della tessera fedeltà.

Il risultato si vede nella parte inferiore della figura. Inutile ogni tentativo di arrivare a una connessione.

Poco male, c’è il 3G. Però, a che pro reclamizzare un servizio che non funziona, non si comporta come previsto, è inutilizzabile?

 ![Ikea](/images/ikea.jpg  "Ikea")