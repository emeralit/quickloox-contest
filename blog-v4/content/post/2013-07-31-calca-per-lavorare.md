---
title: "Calca per lavorare"
date: 2013-07-31
comments: true
tags: [iOS, Mac]
---
Ogni giorno la mamma dei programmi per scrivere resta incinta e sarebbe ingeneroso scrivere che sono fessi; certo quella ricerca della semplicità a tutti i costi semplifica anche il lavoro del programmatore, che così se la cava alla svelta.<!--more-->

Una bella eccezione a questa tendenza sembra essere [Calca](http://calca.io), 2,69 euro [per iOS](https://itunes.apple.com/it/app/calca/id635757879?l=en&mt=8) e 4,49 euro [per Mac](https://itunes.apple.com/it/app/calca/id635758264?l=en&mt=12).

Oltre alla compatibilità [Markdown](http://daringfireball.net/projects/markdown/), che oramai vantano anche le radiosveglie, Calca fa una cosa diversa: accetta espressioni numeriche. Mica però come i *word processor* dei bei tempi, le quattro operazioni e via andare: accetta variabili, funzioni, equazioni, matrici, logica e quant’altro.

Il sito è ricco di esempi che chiariscono perfettamente la situazione. Calca si situa nella nicchia – che suppongo ampia – dove il supporto di Markdown permette di elaborare risultati in modo proibito finora a un programma per scrivere e presentarli come un foglio di calcolo si blocca per rigidità intrinseca.

Non ho ancora proceduto all’acquisto in assenza di due informazioni fondamentali: se esista la sincronizzazione via iCloud (o almeno Dropbox) e se Calca tratti i numeri casuali, perché lancio montagne di dadi. In caso di risposta positiva – soprattutto al secondo punto – farò spazio a Calca sul mio *desktop*.