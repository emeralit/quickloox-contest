---
title: "Meno spazi fuori"
date: 2023-06-08T01:31:35+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Quickloox, Macintelligence, Mimmo]
---
Un tocco di fino di [Mimmo](https://muloblog.netlify.app) e ora, almeno negli abstract del blog, non viene inserito arbitrariamente uno spazio dopo un link.

Prossima sfida: intervenire sul meccanismo che provoca lo stesso effetto e lo stesso spazio superfluo anche all’interno degli articoli. Gli spazi in eccesso fuori sono stati eliminati, ora tocca a quelli dentro.

Un passo alla volta, si migliora (non per merito mio, eh, però ogni miglioria, anche piccola, restituisce un mese di vita).