---
title: "Pubblico e privato"
date: 2020-02-03
comments: true
tags: [Nfl, Super, Bowl, LIV, Dazn, Sky, Fox, Ivacy, iPad, Pro, iPhone, Tv, AppleWorld.Today, AirPlay, Chiefs, 49ers, Mahomes, Garoppolo]
---
Il rituale della visione in diretta del [Super Bowl LIV](https://www.nfl.com/super-bowl) è stato molto più semplice da realizzare e più godibile nella fruizione rispetto agli [anni passati](http://macintelligence.org/blog/categories/bowl/).

Per questo 2020 i canali italiani erano più che adeguati, tra Dazn e Mediaset, ma volevo avere il commento originale e anche le pubblicità. Il linguaggio commerciale del Super Bowl è caratteristico e merita almeno la possibilità di un’occhiata; la sua mancanza costituisce la lacuna principale di qualunque diretta italiana.

Mesi fa ho stipulato un abbonamento *lifetime* a [Ivacy](https://www.ivacy.com/), fornitore di VPN, per poche decine di dollari grazie a un’offerta di [Apple World Today](https://www.appleworld.today/). La rete privata virtuale mi serve in poche occasioni specifiche, tra cui questa, dove l’obiettivo è ottenere contenuto da cui sarei altrimenti escluso per motivi geografici.

Ho puntato il browser di iPhone X su [Fox Sports](https://www.foxsports.com/nfl/2020-superbowl-live-stream) e con AirPlay ho replicato lo *streaming* sul grande schermo di casa tramite Tv. iPad Pro ha fornito il *second screen* per avere dati e commenti da Internet.

Sicuramente una delle edizioni più confortevoli da seguire dato l’equipaggiamento a disposizione e pure una bella partita. Patrick Mahomes dei Chiefs è stato impressionante all’inizio, poi i 49ers lo hanno sterilizzato fino quasi alla fine e se non fosse stato *quasi* avrebbero avuto ragione. Jimmy Garoppolo crescerà; a San Francisco nel suo ruolo hanno visto ben altre gestioni di finali concitati.

Buone azioni, belle emozioni. Quando tutto funziona in modo perfetto e veloce, l’unica cosa che resta da fare è concentrarsi sul gioco e lasciarsi coinvolgere. Ne è valsa la pena.