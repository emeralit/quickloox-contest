---
title: "Proprio come una volta"
date: 2020-08-20
comments: true
tags: [Hey, Mac]
---
Matt Birchler è molto soddisfatto dei suoi primi mesi con il servizio di posta elettronica [Hey](https://hey.com). Ecco, infatti, [come ne parla](https://birchtree.me/blog/a-few-months-with-hey/):

>Sto cercando di dire quella cosa che gli amanti di Apple conoscono da decenni: non è il numero di funzioni che hai, ma quanto bene sono state messe a punto.

Che bello rivedere un argomento tipico di tanto tempo fa, a proposito di un servizio che può piacere oppure no, ma rappresenta comunque una delle pochissime vere novità di questi tempi.

Non so se apprezzo Hey perché mi sembra distante dalle mie necessità, ma sono ugualmente intenzionato a provarlo (gratis per due settimane senza impegno). Nel caso, chi volesse inviare un messaggio qualsiasi a loox@hey.com entro le prossime due settimane avrà la mia gratitudine. Niente di importante o da cui dipenda un rapporto di stima. È un test gratuito, potrei anche non vederla mai.

Ogni tanto si dice che Apple non faccia più innovazione. Anche fosse vero, si vede germinare qualche seme caduto fuori dal campo e i frutti sono interessanti.