---
title: "Ufficio complicazione affari semplici"
date: 2014-05-06
comments: true
tags: [Swype, Android, Nuance, CyanogenMod]
---
[Leggo](http://forum.swype.com/showthread.php?13052-Swype-makes-almost-4000-location-requests-every-day) dal *forum* degli utilizzatori di Swype su Android:<!--more-->

>Ho appena installato CyanogenMod 10.2 sul mio telefono e sono rimasto deliziato dalla funzione “Privacy Guard”, che permette di vedere (e bloccare) i permessi che hanno le app. In quattro giorni, Swype ha consultato i miei dati di posizionamento geografico 14.989 volte. 3.750 volte al giorno, due volte e mezzo al minuto. Leggo in alcune altre discussioni che Nuance [i produttori] è reticente sul perché abbia bisogno del dato geografico, ma la portata di questa raccolta è inquietante!

Compri un apparecchio dove possono funzionare applicazioni particolarmente ansiose di scoprire dove ti trovi, per scopi non chiarissimi, dato che per [la loro funzione](http://www.swype.com) non ne hanno bisogno.

Per fortuna puoi scaricare e installare una versione del *firmware* alternativa, da sostituire a quella di serie, per ovviare al problema di una piattaforma dove la tua *privacy* vale un soldo bucato.

Così puoi bloccare la *app* intrusiva che – bloccata – perde qualunque utilità e allora tanto valeva che non la si scaricasse neppure.

Mi sento un po’ stanco. Sarà che mentre di là installavano il nuovo *firmware*, scoprivano le *app* impiccione, le bloccavano e passavano il tempo a indagare su che applicazioni fanno che cosa, di qua la stessa quantità di tempo l’ho passata a lavorare, imparare, divertirmi, comunicare e quant’altro.