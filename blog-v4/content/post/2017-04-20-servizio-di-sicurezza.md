---
title: "Servizio di sicurezza"
date: 2017-04-20
comments: true
tags: [Paoloo, 7zip, Aes-256]
---
Ho ricevuto da **Paoloo** tutto quanto segue, che pubblico con gratitudine e ammirazione per quanto ha saputo fare. Il funzionamento della faccenda è responsabilità sua. Così come il merito, quandunque la faccenda suddetta fosse di utilità effettiva.<!--more-->

**Caro Lucio,**

data la necessità di affidare a storage remoti i miei documenti in forma criptata ho implementato un servizio di sistema che archivia una cartella in formato 7zip con password utilizzando la cifratura AES-265 ( massimo standard di sicurezza oggigiorno ) e ne effettua l’upload simultaneo su Dropbox e Google Drive.

Dopo un paio d’anni d’utilizzo ho percepito, anche per caso se vogliamo, i caratteri di non banalità ed esclusività della mia soluzione e questo mi ha spinto alla condivisione della stessa.

![Menu servizi](https://www.dropbox.com/s/tqgvmnt4wrttq9o/Immagine-1.gif?raw=1)

![Dialog box](https://www.dropbox.com/s/xxfrs56as6oqnvm/Immagine-2.gif?raw=1)

**UPLOAD SIMULTANEO**

_( Qualora si usasse solo Dropbox creare la cartella "Multicloud" nella cartella dropbox, `/Users/Paolo/Dropbox/Multicloud`, e saltare questa sezione )_

Per ottenere l’upload simultaneo su Dropbox e Google Drive bisogna creare una cartella in Google Drive e relativo symlink in Dropbox ( non vale Il contrario, Google Drive non digerisce i symlink ).

Nel mio caso ho creato la cartella _Multicloud_ in Google Drive ( `/Users/Paolo/Google Drive/Multicloud` ) e suo symlink in Dropbox ( `/Users/Paolo/Dropbox/Multicloud` ), qualsiasi elemento aggiunto in _Multicloud_ verrà uploadato simultaneamente verso i due servizi di storage.

Per creare il symlink da terminale 

    ln -s /path/to/original /path/to/symlink

nello specifico 

    ln -s /users/paolo/google\ drive/multicloud /users/paolo/dropbox/Multicloud

 

_N.B. Gli spazi interni a un path devono essere preceduti da backslash ( \ ). I path sono case unsensitive tranne il nome del nuovo symlink ( “Multicloud” )._

**KEKA**

Lo script necessita la presenza dell’utility di de/compressione Keka.app http://www.kekaosx.com/it/ nella cartella Applicazioni ( utilizza infatti da Terminale il binario /Applications/Keka.app/Contents/Resources/keka7z ).

L'utility inoltre è indicata a scompattare su Mac i file 7zip creati.

**INFO SUL SERVIZIO**

Il servizio è attivo selezionando una cartella o un file, in quest’ultimo caso verrà archiviata la cartella che lo contiene.

Il servizio dev’essere configurato modificando le due variabili al punto #A dello script.

Il servizio controlla se nella cartella _Multicloud_ esiste già un file 7z omonimo dell’archivio che si sta creando, in tal caso lo sposta nella cartella _home_ aggiungendo il suffisso “_oldarchive” al nome del file.
Nel caso remoto il servizio non andasse a buon fine e quindi il nuovo file 7zip non fosse creato in _Multicloud_, il vecchio archivio rimane disponibile nella _home_, altrimenti viene eliminato.

Il servizio si premura di non lasciar traccia delle password degli archivi disabilitando temporaneamente la registrazione dei comandi nella command hystory del Terminale ( punti #F e #Fb dello script ).

Il servizio crea l’archivio criptato tramite comando

    keka7z a -t7z -mx -mhe -ms -mmt -w —p<password> <fileName>.7z <sourcePath>

con parametri



- **t7z** - use 7z file type

- **mx** - use max compression (level 9)

- **mhe**  - encrypt headers

- **ms** - create solid archive

- **mmt** - multithread the operation (faster)

- **w** - use a temp directory as the working directory

- **p&lt;password&gt;** - specifies password

- **&lt;fileName&gt;.7z** - destination archive file

- **&lt;sourcePath&gt;** - path to the source directory

**IMPLEMENTAZIONE**

![Automator](https://www.dropbox.com/s/flugv9rncpiyt1e/Immagine-3.gif?raw=1)

Copiare lo script sottostante in Automator come da figura, modificare le variabili al punto #A . Salvare.

	on run {input, parameters}
		
		# A - Modificare il valore delle seguenti due variabili secondo le proprie esigenze (valori in minuscolo) 
		set userName to "paolo"
		set dropFolderName to "multicloud"
		
		set pathToDropFolder to "/users/" & userName & "/dropbox/" & dropFolderName & "/"
		
		# B - Recupera pathToSourceDirectory, path della cartella da archiviare
		tell application "Finder"
			
			set selectedItem to item 1 of (get selection)
			
			if class of selectedItem is folder then
				set theFolder to selectedItem
			else
				set theFolder to (container of selectedItem)
			end if
			
			set pathToSourceDirectory to POSIX path of (theFolder as string)
			
		end tell
		
		# C - Finestra di dialogo, richiede all’utente nome e password dell'archivio
		display dialog "Verrà creato un 7z della cartella in dropbox/" & dropFolderName & return & return & "Inserisci nome e 	password separati da una virgola" default answer "NomeDellArchivio,password"
		
		
		# D - Imposta archiveName e archivePassword splittando il testo ritornato dal box di dialogo
		set nameAndPasswordByUser to text returned of the result
		set ASTID to AppleScript's text item delimiters
		set AppleScript's text item delimiters to ","
		set archiveName to first text item of nameAndPasswordByUser
		set archivePassword to second text item of nameAndPasswordByUser
		set AppleScript's text item delimiters to ASTID
		
		
		# E - Imposta oldAchiveExists, indica se in dropFolder esiste già <archiveName>.7z, file omonimo dell’archivio da creare
		set pathToArchive to pathToDropFolder & archiveName & ".7z"
		set oldAchiveExists to false
		tell application "Finder" to if exists pathToArchive as POSIX file then set oldAchiveExists to true
		
		
		tell application "Terminal"
			# F - Disabilita la registrazione della command history nel Terminale ( per non lasciar traccia della password )
			set currentTab to do script ("set +o history")
			
			# G - Se esiste già <archiveName>.7z in dropFolder, lo sposta nella cartella home rinominandolo <	archiveName>_oldarchive.7z
			if oldAchiveExists then
				set pathToOldArchive to "/users/" & userName & "/" & archiveName & "_oldarchive.7z"
				do script ("mv '" & pathToArchive & "' '" & pathToOldArchive & "'") in currentTab
			end if
			
			# H - Crea archivio compresso e crittografato AES256 nella dropFolder 
			do script ("/Applications/Keka.app/Contents/Resources/keka7z a -t7z -mx -mhe -ms -mmt -w -p" & archivePassword & " '" 	& pathToArchive & "' '" & pathToSourceDirectory & "'") in currentTab
			
			# I - Eventualmente elimina il vecchio archivio dalla home
			if oldAchiveExists then
				set newArchiveWasCreated to false
				tell application "Finder" to if exists pathToArchive as POSIX file then set newArchiveWasCreated to true
				
				if newArchiveWasCreated then
					do script ("rm '" & pathToOldArchive & "'") in currentTab
				end if
			end if
			
			# Fb - Ripristina la registrazione della command history nel Terminale
			set currentTab to do script ("set -o history")
		end tell
		
		return input
	end run

