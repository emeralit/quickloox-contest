---
title: "Come mai prima"
date: 2018-07-06
comments: true
tags: [MacStories, Christoffel]
---
Il titolo [Apple sta costruendo una piattaforma mediale come mai prima](https://www.macstories.net/stories/apple-is-building-a-media-platform-like-never-before/) (Ryan Christoffel su *MacStories*) mi sembrava eccessivo e fuori dal tono moderato tipico del sito. Poi ho letto l’*incipit*.

>Hai mai guardato la costruzione di un nuovo edificio senza avere idea di come sarà una volta finito? I progressi arrivano un pezzo per volta e ti lasciano all’oscuro dell’obiettivo fino a che arriva un punto nel quale, in un singolo attimo, improvvisamente tutto acquista un senso.

Ed è vero; l’analisi di Christoffel è completa e suggestiva. Si pone anche domande sensate circa il futuro sviluppo della piattaforma, senza andare sulla fantascienza ma restando nel concreto.

Alla fine me lo sono letto tutto e sono contento di averlo fatto. Mi ha aperto un mondo che non avevo considerato, a partire dal fatto che normalmente associamo l’attività mediale di Apple a iTunes, e invece c’è molto di più, molto più variegato, da TV in avanti.

Lettura raccomandata.