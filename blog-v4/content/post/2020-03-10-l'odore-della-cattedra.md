---
title: "L’odore della cattedra"
date: 2020-03-10
comments: true
tags: [Venerandi]
---
Fabrizio Venerandi è come sempre anni avanti a chiunque. Il suo [post su Facebook](https://www.facebook.com/fabrizio.venerandi/posts/10223091822029922) in versione integrale:

>Mi piacerebbe sapere quelli che dicono che l’insegnamento *vero* si fa solo in classe, a quanti corsi di didattica a distanza abbiano partecipato. Dico, professionali, non improvvisati da qualche università italiana con slide e uno che parla monotono leggendole in camera fissa.

>Perché io negli ultimi vent’anni ho seguito diversi corsi in inglese di università americane, sui metadati, sulla programmazione JavaScript, sulla scrittura di videogiochi in Python, sugli automata e sono state tra le esperienze di insegnamento più motivanti che mi siano capitate.

Potrebbe bastare. Ma seguono i commenti: spaccato dal mondo della scuola.

Più o meno, commentano i migliori sulla piazza. Quelli che almeno ci provano, che hanno avuto qualche esperienza, che non oppongono un rifiuto netto. La media tuttavia è chiara: quando va bene, la didattica a distanza è stata una buona eccezione alla regola e niente di più.

Quanta scuola italiana è ferma a *slide e uno che parla monotono leggendole in camera fissa* o anche più indietro di così?

Soprattutto, può essere che un docente nell’anno duemilaventi non sappia di didattica a distanza e debba improvvisare costretto, sottolineo costretto, dal virus?

Poi capita di leggere che comunque le lezioni in aula sono meglio, che si insegna anche con il corpo, che è più difficile mantenere l’attenzione online e altro assortito. Dall’università c’è chi [ironizza sulla propria improvvisazione](https://www.facebook.com/photo.php?fbid=2589801587975192&set=a.1410250725930290&type=3):

>Nella prima [lezione a distanza] pensavo di aver eliminato il video, e invece compaio in un angolino in basso a destra con il mollettone nei capelli, un po' in avanti sulla fronte, come non vorresti farti vedere neanche dalla tua migliore amica.

>Nel secondo credevo di aver messo in pausa invece no, dunque la lezione include come bonus una telefonata con mia sorella (rivedendola noto che non cambio espressione da una all'altra. e non mi sembra un buon segno).

Notare due cose: la totale indifferenza rispetto a quanto arrivi – o non arrivi – agli studenti e il coro entusiasta dei commenti, favorevole con maggioranza schiacciante.

Penso a che lezione sarebbe se il docente entrasse in aula e, seduto in cattedra, iniziasse a leggere con voce monotona il libro di testo, per un’ora. Una cosa orrenda. Fatto da casa invece diventa del tutto accettabile e ci si ride pure sopra. Penso a un anestesista che ironizza sulla sua improvvisazione, un controllore di volo, un pompiere. Certo a insegnare male non si rischiano danni seri. Non subito. Non evidenti. Se ti disinteressi di come insegni a cinquanta ragazzi l’anno, per vent’anni, hai insegnato male a mille ragazzi.

Dici che non si tratta del succo della lezione ma di come viene tenuta ed è un’altra cosa? Se viene tenuta così, posso immaginare come sia stata preparata.

Ho idea che gli idolatri della cattedra, dell’aula, della lezione frontale siano un insieme parallelo a quello dei libri di una volta, l’odore della carta, il fascino dell’inchiostro. Tutte cose condivisibili, solo che confrontare le gemme dell’analogico con l’immondizia del digitale è disonesto.