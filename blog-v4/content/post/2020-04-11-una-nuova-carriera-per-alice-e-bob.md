---
title: "Una nuova carriera per Alice e Bob"
date: 2020-04-11
comments: true
tags: [Alice, Bob, Pgp, Coronavirus, Apple, Google, Bluetooth]
---
Chi ha frequentato l’informatica quando era disciplina esoterica, che si sviluppava di pari passo con gli entusiasmi di chi si cimentava, conosce bene Alice e Bob.

Erano i figuranti, le cavie, le vittime di esperimenti, esempi, prove su strada. Più di tutto, erano famosi per le loro [interpretazioni della cifratura a chiave doppia](https://cs.stanford.edu/people/eroberts/courses/soco/projects/2004-05/cryptography/pgp.html): Alice cifra il messaggio con la chiave privata, lo manda a Bob, che prende la chiave pubblica di Alice eccetera eccetera.

Quando sembrava che Alice e Bob sarebbero andati in pensione stanchi di gloria e infinite chiavi di cifratura, ecco che per loro si apre una finestra professionale inaspettata: potranno interpretare il [framework di tracciamento contatti prossimamente messo a punto da Apple e Google](https://techcrunch.com/2020/04/10/apple-and-google-are-launching-a-joint-covid-19-tracing-tool/).

Alice e Bob si incontrano e ora sono i loro Bluetooth a parlarsi; se poi Bob sviluppasse qualche giorno dopo sintomi da coronavirus, Alice verrebbe avvisata automaticamente. Senza sapere che si tratta proprio di Bob, però in modo da poter pensare alla sicurezza propria e altrui.

 ![Alice e Bob ai tempi del coronavirus](/images/alice-bob-i.jpg  "Alice e Bob ai tempi del coronavirus") 

Per gli Stati nazionali si tratta di uno smacco e, più grave, di una ammissione di incapacità intrinseca. Il prossimo futuro ha bisogno di qualche forma di tracciamento dei contatti per essere vivibile e, se si incontrassero in aereo un italiano, un francese e un tedesco, la barzelletta classica verterebbe sulle loro app nazionali, che certamente sarebbero incapaci di parlarsi e condividere le informazioni come serve. Cosa che invece, a livello di Android e iOS, non accadrà.

(Personalmente sono convinto che il governo americano abbia esplicitamente chiesto a Google e Apple di accordarsi, ma sono senza prove).

Chi teme per la privacy e la libertà di movimento, ha solo da gioire. È una iniziativa che sarà messa alla prova da chiunque, ricercatori, hacker, politici, giornalisti, gruppi di opinione, associazioni di consumatori, autorità legislative esecutive e giudiziarie, cretini su Facebook, centri sociali, militari; chiunque. A qualsiasi livello, qualunque critica sensata verrà recepita, per scongiurare un danno di immagine di proporzioni bibliche. Né Apple né Google possono permettersi di tradire le aspettative. Al contrario, i governi potrebbero largamente ignorare critiche alle loro app e quelli autoritari prima di tutti. Non parliamo dell’efficacia. Siamo tristemente abituati a governi tecnologicamente analfabeti. Da Apple e Google pretendiamo che funzioni e basta.

La vera sfida riguarda la funzionalità effettiva. Ogni persona che spegne il tracciamento rende il sistema meno efficace. Quanto Bluetooth e compagnia possano gestire efficacemente assembramenti o contatti sporadici, è tutto da vedere. I tempi utili sono per forza veloci e quindi non ci sarà testing massiccio prima della messa in campo. Lavorare bene e in fretta non è da tutti e non ha sempre buon esito.

Tutto ciò non riguarda Alice e Bob. Per loro si annuncia un nuovo, inaspettato capitolo della loro carriera. Hanno già ripreso a esibirsi. Diventeranno conosciuti a una platea sei ordini di grandezza superiore a quella che avevano. Non sai mai che cosa ha in serbo la vita.

 ![Alice e Bob ai tempi del coronavirus](/images/alice-bob-ii.jpg  "Alice e Bob ai tempi del coronavirus") 
