---
title: "Relazione stabile"
date: 2021-03-15T02:05:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Big Sur] 
---
Big Sur tutto sommato mi piace. Dopo qualche settimana di utilizzo sono sicuro di considerarlo *casa*.

Certamente non è perfetto. Ammetto che parte delle obiezioni più comuni – per esempio la trasparenza della barra dei menu – per me sono plus, da vero bastian contrario, più che problemi. Immagino che sia perché ci vedo bene, la barra la uso poco (tastiera, tastiera, tastiera), non mi dispiace un ambiente che muta durante la giornata, almeno in modo ragionevole. Ho anche adottato lo sfondo scrivani dinamico, che mostra la *location* diversamente illuminata secondo l’ora del giorno. Eccetera.

Ciò detto, qualche cosina qua e là da aggiustare c’è di sicuro. Lato musicale, per esempio, la app Musica ha perso i comandi da tastiera per riprodurre i brani. A volte l’indicazione del brano in riproduzione è diversa tra app e menulet grafico, il che è ben strano.

Per il resto vivo tranquillo e lavoro bene. Ho un solo software critico che lavora a trentadue bit, impossibile da aggiornare, che ora vive dentro una macchina virtuale. Tutto il resto funziona.

Soprattutto funziona che è arrivato l’aggiornamento 11.2.3, [sesto della serie](https://support.apple.com/en-us/HT211896#macos1123). Non ho riscontri puntuali, solo una sensazione; trovo che questa versione del sistema sia finalmente vicina a quello che intendeva essere dall’inizio.

Il nostro rapporto è cominciato abbastanza bene e, superata qualche piccola asperità che capita anche nelle migliori famiglie, ora è più solido e appagante di prima.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*