---
title: "La parola agli esperti"
date: 2021-03-20T02:00:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Tim Cook, People, Steve Jobs] 
---
È normalissimo trovare vantaggi nel condurre attività produttive fianco a fianco con i colleghi e altrettanto lo è trovare vantaggi equivalenti nel condurre attività analoghe da una postazione remota. Dopo un anno di pandemia bisognerebbe averlo capito; invece ci troviamo con un sacco di gente bravissima e competente, che però sa funzionare solo in un modo.

La nuova normalità ha già mostrato da un pezzo che verrà richiesto di funzionare in più modi e il virus non c’entra: ha solo sbattuto l’evidenza in faccia a chi resisteva a un cambiamento che ha basi ben più profonde dell’evitare il contagio.

Verremo vaccinati o il virus si attenuerà o tutte e due le cose, ma non si tornerà indietro. Settimana scorsa mi hanno chiesto di presenziare a una riunione importante in sede. Ho risposto che quel giorno sarei stato felice di intervenire per un’ora ma l’agenda non consentiva altro spazio.

*Non si può allungare un po’?*, mi hanno chiesto.

*Certo*, ho risposto. *Possiamo tenere la riunione online e sono a disposizione per tre ore, visto che risparmio un’ora di macchina all’andata e altrettanto al ritorno*.

La riunione si è tenuta online ed è riuscita benissimo. Abbiamo anche concluso in due ore soltanto.

Ma sono solo un qualunque professionista. Ci sono imprenditori, uomini d’affari, dirigenti di alto livello convinti di sapere perfettamente perché è necessario che tutto torni come prima, negli stessi modi di prima, nell’errata convinzione che il lavoro remoto sia frutto dell’emergenza invece che dell’ingresso del digitale nella vita quotidiana, anche lavorativa.

Ci sono anche persone come Tim Cook, a capo di un’azienda capitalizzata quanto il prodotto interno lordo italiano, con una sede centrale da dodicimila posti, che a queste problematiche ha dovuto pensare su un poco. Lo ha [intervistato di recente People](https://people.com/human-interest/apple-ceo-tim-cook-expects-return-to-office-post-pandemic/) e ha detto cose poco interessanti, ma terribilmente concrete. Se Steve Jobs, quando esagerava, fuggiva in avanti dove nessuno riusciva a seguirlo, Cook quando esagera diventa l’impiegato della porta accanto, che snocciola ovvietà disarmanti tanto quanto ineludibili.

>La pancia mi dice che, per noi, è ancora molto importante essere fisicamente in contatto, perché la collaborazione non è sempre un’attività pianificata.

Gente che vorrebbe lavorare tutta la vita senza mai mettere più piede in ufficio: è improbabile. Dipende dall’azienda, ma è improbabile. E chi ha già pianificato tutto per il momento del ritorno, invece, quando tutto tornerà come prima?

>A essere onesti, stiamo ancora pensando a come organizzarci.

La collaborazione non è sempre un’attività pianificata. Nemmeno la postazione di lavoro può più esserlo. Ma parlarsi e vedersi dal vivo è sempre e comunque preferibile, giusto?

>Abbiamo imparato che ci sono alcune cose perfette da fare in virtuale via Zoom, WebEx, FaceTime o quello che è a disposizione. Per questo penso che [quando i dipendenti rientreranno negli uffici] allestiremo un ambiente ibrido per un po’.

Ma la produttività? Come ci si può fidare di gente che lavora altrove senza controllo, capace di distrarsi o di fare finta di lavorare? L’azienda non funziona meglio *in presenza*?

>Dopo la chiusura degli uffici a metà marzo, abbiamo avuto questo periodo enormemente prolifico con [la presentazione del] primo iPhone 5G. Abbiamo introdotto il chip M1 nei Mac. Sono traguardi importanti.

Ecco. Ascoltiamo i so-tutto sul digitale e l’analogico; poi però seguiamo quelli che affrontano sul serio la situazione e organizzano decine di migliaia di persone per occuparsi di un business da duecento miliardi l’anno, con successo. Magari qualcosa hanno capito.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*