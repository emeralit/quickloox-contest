---
title: "Ci siamo quasi"
date: 2018-04-04
comments: true
tags: [Acer, Chromebook, OS, Chrome]
---
Se fosse solo una questione di vendere qualche macchina in più, Apple avrebbe apprestato una versione economica di MacBook per le scuole. In futuro forse lo farà, ma [come prima mossa](https://macintelligence.org/posts/2018-03-28-solo-per-amore/) ha presentato un iPad che riconosce Apple Pencil, monta una versione di Pages orientata alla creazione di eBook ed è potente a sufficienza per la realtà aumentata. Il tutto al prezzo più basso che si possa avere per un iPad con contorno di software per l’amministrazione della classe e duecento gigabyte di iCloud.<!--more-->

I critici dicono che costa più di un Chromebook (che sarebbe oggi la scelta preferita delle scuole), non c’è innovazione, [non fa quello che serve](https://9to5mac.com/2018/03/28/making-the-grade-why-apples-education-strategy-is-not-based-on-reality/) e signora mia, quando c’era Steve sì che ci divertivamo.

Di fronte a questo fallimento, che cosa viene in mente a Google, dominatrice del mercato appunto con i Chromebook? Per tramite di Acer [presenta una tavoletta Chrome OS](https://9to5mac.com/2018/03/26/google-debuts-chrome-os-tablets-ipad-education/).

Che riconosce uno stilo quasi buono come Apple Pencil; pesa quasi identico, solo un pochino di più; è quasi sottile uguale, solo un po’ più spesso; come software ha zero dotazione extra, quindi fa quasi quello che può fare l’iPad *education*.

Ma il prezzo, signora mia. Lì sì che ci siamo. Esattamente uguale.
