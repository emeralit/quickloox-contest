---
title: "La guerra del trasportabile"
date: 2022-08-29T13:14:15+01:00
draft: false
toc: false
comments: true
categories: [Internet, Hardware]
tags: [Sabino Maggi, Maggi, briand06, Osborne 1, Apple II, Apple III, Macintosh Portable, Osborne]
---
Bellissima la rievocazione scritta da **briand06** dell’[uso di Osborne 1 per il giornalismo in zona di guerra](https://melabit.wordpress.com/2022/08/26/in-prima-linea-con-losborne-1/).

Osborne 1 è una delle mie cicatrici: un computer straordinario che non sono mai riuscito a utilizzare, al contrario di altri trasportabili prodotti dalle nostre parti che, a parte funzionare e ci mancherebbe, mancavano totalmente del *quid* che rende speciali.

In un certo senso Osborne, l’azienda, è stata antesignana di Apple sotto più aspetti; Osborne 1 non è sicuramente un antesignano di Mac, ma emergeva su tutta la produzione dell’epoca e in questo si ritrova a condividere dei tratti con [Apple II](https://americanhistory.si.edu/collections/search/object/nmah_334638). Anche se il primo [Macintosh Portable](https://lowendmac.com/1989/mac-portable/) non era esattamente leggero come una piuma, Apple evitò di farsi prendere dalla smania dei trasportabili da dieci chili e, per quanto capace di fallire su altri fronti come con [Apple ///](https://apple.fandom.com/wiki/Apple_III), si risparmiò un filone di prodotti per forza destinato, bastava guardarli, a una rapida obsolescenza.

Proprio come Apple, Osborne si è scontrata con l’industria dei *rumor* finendo, al contrario di Apple, per soccombere. *Quello che riveli sui tuoi progetti futuri*, legge fisica nel mondo della tecnologia, *verrà abusato e frainteso in malafede e a tuo danno*.

Osborne 1, comunque, era un gran computer e invito a leggere almeno qualche link di quelli presenti nell’articolo: ne vale la pena per avere più chiara la storia della tecnologia e del *computing* personale.