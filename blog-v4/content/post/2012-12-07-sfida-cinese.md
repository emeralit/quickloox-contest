---
title: "Sfida cinese"
date: 2012-12-07
comments: true
tags: [Apple, iMac, Cook, Cina, NbcNews]
---
Prima sono uscite le notizie di Mac che riportano la dicitura <i>Assembled in USA</i>, assemblato negli Stati Uniti.<!--more-->

Ora Tim Cook parla di <a href="http://rockcenter.nbcnews.com/_news/2012/12/06/15708290-apple-ceo-tim-cook-announces-plans-to-manufacture-mac-computers-in-usa?lite">piani per la *fabbricazione* dei Mac</a>, sempre negli Stati Uniti.

Tutti? Improbabile, nel breve e anche nel medio. Perché? Un sacco di ragioni potenziali, compreso il controllo che è nel Dna di Apple. Per ipotesi, potrebbero concepire una produzione pilota di Mac i cui parametri vengono poi trasmesse ad aziende esterne una volta che la qualità globale viene ritenuta soddisfacente (e Apple ha potuto compiere tutti gli esperimenti del mondo in un ambiente più confidenziale delle aziende cinesi). Questo giustificherebbe anche una produzione limitata.

Se Apple ha davvero preso la direzione della sfida al mito della Cina manifatturiera… si possono discutere, disapprovare, ignorare, ma quello che fa questa azienda non lo fa nessun altro.

(Tra parentesi, il <a href="http://video.msnbc.msn.com/rock-center/50112247">filmato dell’intervista</a> concessa da Tim Cook a NbcNews merita di essere guardato, anche se su un Mac pretende Flash. Ci si trucca da iPad et voilà, si capisce subito quale sia l’informatica perdente.)