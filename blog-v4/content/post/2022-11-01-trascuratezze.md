---
title: "Trascuratezze"
date: 2022-11-01T01:35:34+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Apple, MacStories, John Voorhees, Voorhees]
---
Scelgo [l’articolo di MacStories scritto da John Voorhees](https://www.macstories.net/news/apple-reports-expectations-beating-q4-2022-results-of-90-1-billion/) perché contiene un chiarissimo grafico a torta che mostra la ripartizione degli incassi di Apple nel trimestre estivo.

Mi sono venuti in mente i lamenti di quanto Apple trascurava i Mac perché pensava solamente a iPhone, in considerazione del fatto che iPhone costituiva la punta di diamante del fatturato.

Oggi iPhone è sempre in prima linea: raccoglie il quarantasette percento del business.

Mac, dopo iPhone, è il segmento hardware più importante (tredici percento). La trascuratezza di Apple ha dato risultati inaspettati.

La fetta di torta più esigua è quella di iPad, ultimo in classifica, unico a cifra percentuale singola (otto percento).

Adesso vorrei sentire qualcuno lamentarsi perché Apple si concentra unicamente su iPhone e trascura iPad.