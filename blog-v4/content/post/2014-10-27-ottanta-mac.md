---
title: "Ottanta Mac, ottanta euro"
date: 2014-10-27
comments: true
tags: [iMac5k, Macintosh128k]
---
Sono passati trent’anni. A contare i pixel, dentro un nuovo iMac 5k stanno ottanta schermi del Macintosh originale.<!--more-->

Detta così non rende. Su [Things of Interest](http://thingsofinterest.com/2014/10/22/difference-30-years-makes-imac-retina-5k-display-vs-original-macintosh/) c’è una schermata molto più eloquente.

Se questo Stato, invece che balbettare a ottanta euro per volta, fosse diventato otto volte più efficiente. O capace.