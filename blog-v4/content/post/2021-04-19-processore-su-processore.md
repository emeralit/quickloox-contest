---
title: "Processore su processore"
date: 2021-04-19T01:54:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [M1, Arm, Parallels] 
---
Giusto perché domani Apple tiene un evento di lancio di qualcosa, tengo a ricordare oggi che esiste Parallels Desktop 16.5 che supporta i Mac con processore M1.

Con Parallels Desktop si può accendere una macchina virtuale su cui eseguire per esempio un Linux per Arm e [farla andare il 30 percento più veloce](https://www.macrumors.com/2021/04/14/parallels-desktop-native-m1-support/) di quanto succede con un consueto Mac Intel equipaggiato con Cpu i9 e abbondanza di Ram.

Una cosa da usare su Parallels e che funziona *più veloce* ignorando il sovraccarico di elaborazione dato dalla infrastruttura della macchina virtuale, è da uscire di testa.

L’impressione è che Apple sia andata cinque anni davanti a chiunque, nel giro di neanche dodici mesi dal primo annuncio della transizione ad Arm.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*