---
title: "Ricorsione a New York"
date: 2017-09-03
comments: true
tags: [Tisch, Windows, Nokia, iPhone, Nypd]
---
Avevamo lasciato la storia della polizia di New York che [si disfa di 36 mila computer da tasca Windows Phone non più aggiornabili](https://macintelligence.org/posts/2017-08-29-servire-proteggere-riciclare/) allo stadio in cui Jessica Tisch, Deputy Commissioner della polizia newyorchese al reparto informatico e indicata da tutti come la più forte sponsor del progetto originale, era in vacanza mentre scoppiava il bubbone.<!--more-->

Tisch è tornata e [ha difeso con veemenza il programma](http://www.businessinsider.com/jessica-tisch-nypd-windows-smartphone-program-2017-8), visti i risultati che ha dato.

Ovvero, la scelta di dare computer da tasca ai poliziotti li ha resi più efficienti ed efficaci e in sé è stata ottima.

Nel momento in cui uno si chiede, comunque, quale sia stato l’impulso che ha orientato la scelta del sistema operativo, Tisch scrive un libro sulla storia recente dell’informatizzazione in due righe:

>Tre anni fa abbiamo preso la decisione di portare il computing mobile nella polizia di New York. A quel tempo, né iOS né Android ci permettevano di utilizzare in modo conveniente investimenti precedenti in applicazioni Windows.

Hanno scelto Windows *perché avevano già Windows* e c’era già un programma. Prima ancora, probabilmente hanno deciso di sviluppare per Windows perché da qualche altra parte c’era Windows. La ragione di usare Windows è l’avere già usato Windows, e via ricorsivamente fino alla singolarità pre-Big Bang.

Non è tutto.

>Il contratto prevedeva la fornitura degli smartphone a costo zero. E ci permetteva dopo due anni di sostituirli con gli apparecchi che volevamo, sempre a costo zero.

In italiano: hanno firmato un contratto da 160 milioni di dollari per qualcos’altro e in omaggio hanno ricevuto trentaseimila telefoni. In più hanno ottenuto gratis di poterli sostituire con… niente, perché dopo due anni quella piattaforma è priva di valore. Roba da *remake* di [Totòtruffa 62](http://www.imdb.com/title/tt0055535/plotsummary).

In effetti, quante volte l’ho già sentita? Costava tutto così poco.