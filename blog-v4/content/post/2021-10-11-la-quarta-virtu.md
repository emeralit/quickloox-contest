---
title: "La quarta virtù"
date: 2021-10-11T00:28:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Dr. Drang, Python, AppleScript, BBEdit, MultiMarkdown, Numbers, Larry Wall, Perl] 
---
Che leggerezza, che letizia vedere Dr. Drang mettere insieme un pezzo di [Python](https://www.python.it) e un pezzo di [AppleScript](https://developer.apple.com/library/archive/documentation/AppleScript/Conceptual/AppleScriptLangGuide/introduction/ASLR_intro.html) per [risolvere una faccenda di *web scraping* inerente il recente voto in California](https://leancrew.com/all-this/2021/10/data-cleaning-without-boredom/). Dove, racconta,

>il problema maggiore era raccogliere i risultati del voto pubblicati dall’ufficio della Segreteria di stato californiana. […] Che non ha pubblicato alcuna tabella singola dei voti organizzati per contea, ma cinquantotto pagine web indipendenti, appunto una per contea.

La sfida diventa allora *automatizzare il processo per evitare errori indotti dalla noia*, cosa che potrebbe accadere a chi si ritrovasse a riunire a mano i risultati provenienti da cinquantotto pagine differenti.

Passando da [Numbers](https://www.apple.com/it/numbers/), [BBEdit](https://www.barebones.com/products/bbedit/) e [MultiMarkdown](https://fletcherpenney.net/multimarkdown/), arriva alla tabella riassuntiva. Il suo metodo ha un pregio particolare per la situazione: in una elezione, è facile che uno o più dati parziali cambino nel tempo. Avere un automatismo permette di rifare i calcoli ogni volta che sia necessario.

Il creatore del linguaggio [Perl](https://www.perl.org), Larry Wall, definì le [Tre virtù del programmatore](http://threevirtues.com) come *pigrizia, impazienza e hubris* (la *qualità che ti fa scrivere – e manutenere – programmi di cui nessuno vorrà parlare male*).

Grazie a Dr. Drang, conosciamo una quarta virtù: l’insofferenza per la noia.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*