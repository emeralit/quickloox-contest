---
title: "Io giro intorno al mondo"
date: 2013-10-15
comments: true
tags: [Unix, OSX]
---
Lavorando al prossimo libro su OS X – che sarà pronto in tempi piuttosto rapidi – mi sono imbattuto in cose già viste e mai abbastanza segnalate.<!--more-->

Il primo Multiuser Dungeon (Mud) della storia, [British Legends](http://british-legends.com/CMS/). E il primo Mud italiano, [Neonecronomicon](http://www.neonecronomicon.it).

[Pioneers](http://pio.sourceforge.net), clone di [The Settlers of Catan](http://www.catan.com) che fa venire voglia di (re)installare [Fink](http://fink.thetis.ig42.org) apposta.

E poi l’attualità a contorno delle giornate. La guida rapida per [creare sfondi scrivania](http://howto.cnet.com/8301-11310_39-57605292-285/create-the-perfect-parallax-wallpaper-in-ios-7/) adatti a sfruttare l’effetto parallasse di iOS 7.

Il comune di Cupertino, California, che [discute l’approvazione definitiva](http://9to5mac.com/2013/10/12/apple-shows-off-its-campus-2-project-video-to-cupertino/) del nuovo *campus* a ciambella di Apple. Nel frattempo la multinazionale americana, centocinquanta miliardi di dollari l’anno, centinaia di milioni di apparecchi venduti, oltre settantamila dipendenti, verso il quattrocento negozi in tutto il mondo, [scrive lettere ai futuri vicini di casa](http://techcrunch.com/2013/10/11/here-is-apples-mailer-asking-cupertino-residents-to-support-its-new-campus-ahead-of-city-council-vote/) per stimolare la loro partecipazione in positivo al processo di approvazione.

Ed [esce un aggeggio Android](http://www.theverge.com/2013/10/14/4836286/htc-one-max-review) che riconosce le impronte digitali, proprio come iPhone 5S. Ecco, non *proprio come*:

>Per prima cosa, [il lettore] è posizionato esattamente nel posto sbagliato. Immediatamente sotto l’obiettivo, bisognoso di una spazzata per essere attivato, praticamente invita a sporcare la fotocamera ogni volta che ci si voglia identificare. La necessità di una spazzata verticale è ugualmente problematica, perché la posizione naturale della mano chiede un movimento scomodo. Inevitabilmente questo porta al mancato riconoscimento dell’impronta.

Solo l’inizio. Android non finirà mai, di stupire.

E adesso sotto con le bozze.