---
title: "L’inevitabile proposito"
date: 2015-01-07
comments: true
tags: [DesertGolfing, AppStore, scacchi, Roll20]
---
Dopo avere lungamente riflettuto sul senso da dare a un nuovo anno, direi che il lavoro [lo ha già fatto Apple](https://www.apple.com/start-something-new/).<!--more-->

Fare partire qualcosa di nuovo. Una nota creativa come il materiale che appare sulla pagina. Un progetto come [Desert Golfing](https://itunes.apple.com/it/app/desert-golfing/id902062673?l=en&mt=8), apparentemente puerile, invece cattura un’essenza.

Anche se è una cosa più piccola che [risolvere il gioco degli scacchi](https://www.kickstarter.com/projects/1239228060/solving-chess-yes-solving-chess-step-1-of-3).

Per cominciare va bene anche una macro non ridotta all’osso su [Roll20](https://app.roll20.net/home), una cartella *smart* un po’ più *smart*.

Fare partire qualcosa di nuovo.

<iframe width="560" height="315" src="//www.youtube.com/embed/v7M4uAHpc-8" frameborder="0" allowfullscreen></iframe>