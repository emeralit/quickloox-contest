---
title: "Programmi d’argento"
date: 2017-09-12
comments: true
tags: [BBEdit, Snell, Colors]
---
Bel pezzo di Jason Snell sui [numerosi anniversari di grande software per Mac](https://sixcolors.com/post/2017/09/built-to-last/) che ricorrono quest’anno.

Il tema sembra il solito *amarcord* e invece c’è sotto una grande verità. Scrive sacrosantamente Snell:

>I più anziani amano accusare il mondo moderno di essere usa e getta, al contrario dei loro tempi in cui le cose erano costruite per durare. Di fatto, la gran parte del software degli anni novanta è scomparsa. Sopravvivere tanto a lungo è estremamente raro. Sono numerosi i fattori necessari alla longevità di un prodotto. Deve essere buono, avere successo finanziario e poi… l’ingrediente segreto. Una qualche combinazione di persistenza, perseveranza, stabilità, agilità, testardaggine e adattabilità che permette a poche anime coriacee di durare.

A parte le mostruosità multinazionali, se un software indipendente prospera per un quarto di secolo, ha molte più doti di quelle che si vedono sullo schermo.