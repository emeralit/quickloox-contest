---
title: "Software francescano per iPad"
date: 2014-01-03
comments: true
tags: [iPad]
---
[Usare prima gli strumenti deboli e solo dopo quelli forti](https://macintelligence.org/posts/2013-12-15-software-francescano/). Si era partiti da qui ed era rimasta l’[obiezione di **avariatedeventuali**](https://moot.it/quickloox#!/blog/blog/2013/12/15#software-francescano), che si parlava solo di software Mac.<!--more-->

Il mio primo pensiero è stato superficiale: *iPad è già uno strumento debole a confronto di un Mac*. Superficiale perché sono due animali diversi. Mentre altri superficiali, tipo il [presidente di Nintendo](http://www.technovia.co.uk/2013/06/my-favourite-meme-the-ipad-is-just-a-bigger-ipod-touch.html), si divertivano a canzonare iPad come *iPod touch più grosso*, iniziava una evoluzione. iPhone, [racconta Ben Thompson su stratēchery](http://stratechery.com/2013/the-ipad-and-the-disaggregation-of-computing/), diventava un vero computer tascabile, pensato per cavarsela in ogni situazione. iPad si configurava invece come uno strumento ideale per certi compiti, dove è meglio di un Mac, e meno per altri, quando conviene Mac. Sono tutti compiti dove diventa *altro*: uno studio di registrazione, una macchina per scrivere, una lavagna, un libro, una telecamera, una calcolatrice. Diversamente da iPhone, perché iPhone si usa quando non c’è alternativa; iPad si sceglie in funzione del momento e del compito.

Se voglio uno studio di registrazione, in effetti, voglio [GarageBand](https://itunes.apple.com/it/app/garageband/id408709785?l=en&mt=8), non comandi Unix verso una libreria di manipolazione dell’audio.

Anche se il discorso è sfumato: invece che compilare il blog con [Blogsy](https://itunes.apple.com/it/app/blogsy-for-wordpress-blogger/id428485324?l=en&mt=8) diventa interessante avere uno strumento come [Editorial](https://itunes.apple.com/it/app/editorial/id673907758?l=en&mt=8) e applicarsi per creare una interfaccia su misura per il proprio blog. O altro: consiglio il filmato nel quale Federico Viticci di MacStories [mostra](http://www.macstories.net/tutorials/simulating-multiple-cursors-in-editorial/) come ha emulato in Editorial una funzione di cursori multipli, grazie all’ambiente Python presente nel programma. Nel giorno degli strumenti deboli, comporre su iPad un testo strutturato in Editorial invece che in Pages è sicuramente eccellente esercizio. Su iPad, per quello, funziona persino [vim](https://itunes.apple.com/it/app/vim/id492668168?l=en&mt=8).

D’altro canto, per come è la logica di iPad, per disegnare vorrò [Inspire Pro](https://itunes.apple.com/it/app/inspire-pro-paint-draw-sketch/id355460798?l=en&mt=8) (preso in regalo con [12 Days of Gifts](https://itunes.apple.com/it/app/12-days-of-gifts/id777716569?l=en&mt=8), vero?). iPad diventa una tela, non è più un computer. Il vero strumento debole in questo caso è il dito, chiamato a rimpiazzare i pennelli.

Rimane sempre una possibilità affascinante: usare iPad come terminale mobile verso un ambiente di computing remoto, magari cloud, grazie a [issh - ssh / Vnc Console](https://itunes.apple.com/it/app/issh-ssh-vnc-console/id287765826?l=en&mt=8) e succedanei. La vicenda di Mark O’Connor, programmatore che [passa la propria giornata lavorativa su un iPad connesso a Linode](http://yieldthought.com/post/31857050698/ipad-linode-1-year-later), è nota e prosegue da un bel po’.

Ottenere il massimo dall’apparecchio più sobrio possibile: più complicato come concetto, di *usare gli strumenti deboli*, ma di potenziale immenso.

Forse, iPad è strumento debole in quanto tale. La stessa idea dell’inizio, applicata all’hardware.