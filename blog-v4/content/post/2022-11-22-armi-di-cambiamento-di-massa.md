---
title: "ARMi di cambiamento di massa"
date: 2022-11-22T02:30:00+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Apple, Acorn, ARM, Newton, Larry Tesler, Lisp, Tesler, Advanced Technology Group, ATG, Möbius, Gavarini, Paul Gavarini, Pittard, Tom Pittard, Macintosh, Apple II, Newton, Hobbit, AT&T, Olivetti, VLSI Technology, Advanced RISC Machines]
---
Due componenti dell’Advanced Technology Group di Apple, Paul Gavarini e Tom Pittard, avevano costruito un prototipo di computer chiamata Möbius. Usava un chip ARM2 ed eseguiva software per Apple ][ e Macintosh, emulando i processori 6502 e 68000 a velocità maggiori di quelle native. Il livello superiore del management di Apple rimase confuso dalla macchina e la cancellò in fretta, ma Gavarini e Pittard continuarono a propagandare ARM nelle loro presentazioni interne, dove mostravano test di velocità impressionanti nell’esecuzione di linguaggio Lisp.

Lisp veniva usato internamente da Apple per testare nuove interfacce grafiche. Ma era considerato troppo ingombrante per applicazioni all’interno di apparecchi. Quando il veterano di Apple Larry Tesler vide i test, gli si accese una lampadina in testa.

Tesler era appena diventato responsabile del progetto Apple Newton e doveva sostituire il mediocre chip Hobbit di AT&T, la scelta originale del progetto. ARM era una scelta ideale, per la sua grande velocità e i suoi bassi consumi. Ma c’era un problema: Acorn, produttrice di ARM, era una diretta concorrente di Apple.

Solo che il management di ARM voleva affrancarsi da Acorn, che era in declino. Il principale azionista di Acorn, Olivetti, era più interessata ai cloni di PC. VLSI Technology, che produceva fisicamente i chip ARM, voleva più clienti. E Apple voleva il chip in licenza. Rendere ARM indipendente era interesse di tutti.

Nel novembre 1990 venne raggiunto un accordo. Apple investì tre milioni di dollari per una quota del trenta percento. VLSI ci mise mezzo milione di dollari, più know-how e macchinari per la produzione. Acorn trasferì nella nuova società tutta la proprietà intellettuale di ARM e dodici dipendenti. Su richiesta di Apple, la nuova società si chiamò Advanced RISC Machines.

Quanto sopra è citazione più ampia del consueto del [secondo articolo](https://arstechnica.com/gadgets/2022/11/a-history-of-arm-part-2-everything-starts-to-come-together/) di una serie di Ars Technica dedicata, vediamo chi indovina, ad ARM. Raccomando vivamente l’intera serie e mi fermo a riflettere sulle costanti della storia di Apple nei suoi lati positivi: la testardaggine di dipendenti con buone idee anche se non capite dalla dirigenza, l’inventiva e l’iniziativa libera dei tecnici, la presenza di visionari in grado di capire la portata di una grande idea.

Senza quei tre milioni di dollari, ma soprattutto senza le presentazioni di Gavarini-Pittard e senza la visione di Larry Tesler, oggi Apple non sarebbe forte come è e non avrebbe cambiato la vita di miliardi di persone, una per volta.