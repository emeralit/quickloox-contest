---
title: "Entità inconoscibili"
date: 2023-09-25T13:34:03+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Cook, John D. Cook, entità, Html, BBEdit]
---
John D. Cook ha ragione: [è difficile avere un elenco esaustivo delle entità Html](https://www.johndcook.com/blog/2023/09/23/html-entity-data/). La parola chieve è *esaustivo* qui; mentre alle accentate o a qualche carattere semigrafico di uso più comune ci si arriva in fretta, le entità definite nello standard sono di una varietà notevole. Svelto: come si scrive &radic; in Html? È solo un esempio

Lui fa del suo meglio, producendo un [elenco di entità XML proveniente da W3C](https://github.com/w3c/xml-entities/blob/gh-pages/html5ents.xml) e poi due colonne di testo che, dice a ragione, risultano più *human-readable* che *machine-readable*.

Per una volta posso aggiungere una cosa anch’io: [BBEdit](https://www.barebones.com/products/bbedit/) ha la migliore palette di entità che io abbia trovato finora. Riporta i valori decimali ed esadecimali oltre ai codici HTML, la tabella si ordina alfabeticamente per ciascuno dei quattro parametri, scrivendo il codice a mano si arriva alla voce relativa.

Anche qui però non abbiamo la conoscenza totale. L’elenco XML di W3C contiene più di duemiladuecento voci e BBEdit ne mostra molte meno, probabilmente perché non entra nel reame di Unicode.

Però non scambierei la comodità della tabella di BBEdit, per ora, con alcun altro metodo, se non altro in quanto le entità di cui ho bisogno nella vita rientrano agevolmente in quella tabella. Non lo pagassi, me ne terrei una copia in modalità gratuita. Quella volta che serve, risolve.