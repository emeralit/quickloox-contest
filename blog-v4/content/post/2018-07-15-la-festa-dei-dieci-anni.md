---
title: "La festa dei dieci anni"
date: 2018-07-15
comments: true
tags: [Ruspadana, App, Store]
---
Si capisce che App Store ha lasciato il segno sulla società quando arriva il suo decimo anniversario e ne parlano i *deejay* alla radio, in *prime time*. Alla radio si parla di qualcosa solo se interessa una vasta maggioranza ed è innegabile che le app abbiano cambiato il modo di intendere il software e di distribuirlo, oltre a trasformare il telefono cellulare in un computer da tasca.

Che tutti usino App Store (tra quello originale e le sue copiette) è pacifico; che tutti capiscano come funziona e come è arrivato fino a noi, zero. Né potrebbe essere diverso; quelli che vanno allo stadio a protestare contro la campagna acquisti del club nulla sanno dei meccanismi che veramente tengono in piedi il calciomercato.

Siccome in dieci anni App Store e il meccanismo che lo fa funzionare hanno digerito app nell’ordine dei *milioni*, è chiaro che possano esserci applicazioni per un motivo o per un altro controverse. Dall’esperienza e da articoli assolutamente da leggere come [quello di Ryan Christoffel su MacStories](https://www.macstories.net/stories/10-years-of-app-store-controversies/), sappiamo che la pubblicazione su App Store di app controverse è accaduta spesso e volentieri. Certe volte la app è stata eliminata, certe altre si è guadagnata il diritto a restare; certe volte Apple ha deciso saggiamente, certe altre ha preso cantonate; certe volte le regole di approvazione hanno finito per migliorare e certe altre no.

Chiaro che il rapporto tra i positivi e i negativi non è equo: App Store del 2018 è un luogo spettacolarmente migliore di quello del 2008. Si è andati avanti, complessivamente, facendo di sicuro anche qualche passo indietro tra i molti nella direzione giusta. La maggior parte delle controversie ha portato a un progresso.

Poi ci sono quelli che, come i tifosi allo stadio, sentono il bisogno di dire la loro senza avere la minima idea di come funziona. Un caso esemplare è accaduto di recente con la app *Ruspadana*, accusata di razzismo.

La app, levata dallo Store nel giro di tre ore, era demoralizzante da tutti i punti di vista, compreso quello tecnico. Doveva sparire in tre secondi, o neanche apparire.

Ma quanto siano stati demoralizzanti i moralizzatori, con le iperboli sceme, le frasi fatte, i ditini puntati, il turpiloquio gratuito, la demagogia imparata a memoria, il vuoto pneumatico cerebrale, i solleciti al boicottaggio, le reazioni scomposte, il correttore ortografico drammaticamente spento, la puzza di gregge becero seppure ammaestrato, è veramente indicibile.

Chi ha bisogno di una scusa per assumere un antiacido può leggere questo [thread di Twitter](https://twitter.com/frankiehinrgmc/status/1012714881034813442) dove si salvano in pochi e, guarda caso, quelli che hanno l’idea di come funzioni.

Gli altri probabilmente pensavano che con i dieci anni si festeggiasse la loro età mentale.

<blockquote class="twitter-tweet" data-lang="en"><p lang="it" dir="ltr">Caro <a href="https://twitter.com/AppStore?ref_src=twsrc%5Etfw">@AppStore</a> sei d&#39;accordo con me che un giochino prescolare in cui ammazzi immigrati con una ruspa sia <a href="https://twitter.com/hashtag/lammerda?src=hash&amp;ref_src=twsrc%5Etfw">#lammerda</a>? Che ne dici di ritirarlo e bannare gli autori da future produzioni? E magari atare più attenti a cosa pubblicate? <a href="https://twitter.com/disinformatico?ref_src=twsrc%5Etfw">@disinformatico</a> <a href="https://twitter.com/molleindustria?ref_src=twsrc%5Etfw">@molleindustria</a> <a href="https://twitter.com/makkox?ref_src=twsrc%5Etfw">@makkox</a> <a href="https://t.co/LB8v1nJbyA">pic.twitter.com/LB8v1nJbyA</a></p>&mdash; Francesco Di Gesù (@frankiehinrgmc) <a href="https://twitter.com/frankiehinrgmc/status/1012714881034813442?ref_src=twsrc%5Etfw">June 29, 2018</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
