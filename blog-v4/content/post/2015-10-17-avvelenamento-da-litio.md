---
title: "Avvelenamento da litio"
date: 2015-10-17
comments: true
tags: [Magic, Trackpad]
---
Altro cambio di batteria su Magic Trackpad, per una durata di 211 giorni, undicesimo *data point* che porta a una media di 159 giorni e spiccioli.<!--more-->

Il senso della rilevazione va tuttavia a perdersi perché la [nuova edizione di Magic Trackpad](http://www.apple.com/it/magic-accessories/) ha la batteria ricaricabile. Non conto al momento di acquistarla perché il modello che ho mi soddisfa pienamente e dunque aspetto che si rompa, che mi venga a noia o mi facciano un regalo di Natale, quindi continuerò a tenere nota dei consumi. Tuttavia l’utilità della raccolta dati passa da minima a infima.

Adesso sarà interessante vedere cozzare al bar le teste dei salvatori del mondo, per i quali una batteria fissa all’interno dell’apparecchio è un progresso, e quelle che *sul mio computer faccio che voglio* e, orrore, non posso neanche scegliere che pile metterci, come se fosse una scelta documentata o consapevole.
