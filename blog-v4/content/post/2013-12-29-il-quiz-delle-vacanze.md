---
title: "Il quiz delle vacanze"
date: 2013-12-29
comments: true
tags: [Mac]
---
Grazie a [Francesco](http://quasi.me) per avermi permesso di vedere e ritrarre dal vivo questo *layout* di tastiera non italiano. Un bel complimento a chi lo riconosce!

 ![Layout di tastiera esotico](/images/tastiera.jpg  "Layout di tastiera esotico") 