---
title: "Uno store da restaurare"
date: 2023-03-07T02:20:02+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Mac App Store, Panic, Reagan, Ronald Reagan, Sasser, Cabel Sasser]
---
Servirebbe una sterzata di novità presso gli App Store.

Su iOS e iPadOS non accadrà. A riecheggiare la battuta di Ronald Reagan sul debito pubblico americano, App Store è abbastanza grande per badare a sé stesso. Il successo dell’iniziativa è stato siderale e la crescita ha passato ogni limite ragionevole. Il meccanismo ha una inerzia tale che nessun cambiamento radicale ha la possibilità di incidere. Quindi ci terremo il buono e il cattivo, le app più belle del mondo e anche le pubblicità indesiderate, i clonacci, i *freemium* tossici. Il bilancio complessivo, d’altronde, è positivo.

Su Mac, invece, aspettiamo solo qualcuno con il coraggio di dire che lo Store deve funzionare con criteri diversi, o serve a nulla. [Leggo da Cabel Sasser di Panic](https://social.panic.com/@cabel/109977366338856750):

>Una volta abbiamo sottoposto ad approvazione [Untitled Goose Game](https://goose.game). È stato respinto perché si pensava che fosse impossibile saltare i credits. Abbiamo spiegato che bastava tenere premuta la barra spaziatrice. Allora è stato respinto per qualche altro motivo e a quel punto ci siamo semplicemente stufati. Non ci siamo più curati di riprovare.

Il problema è duplice. Spiega per prima cosa John Gruber che [il gioco è uno dei più originali e divertenti dell’ultimo decennio](https://daringfireball.net/linked/2023/03/06/sasser-mac-app-store-story). Il sistema allontana e scoraggia quelli bravi; dovrebbe allontanare ciofeche e faccendieri, invece.

Secondo. Su iOS, con tutti i suoi difetti, una Panic Software sarebbe comunque obbligata a ottenere un’approvazione. Sembra una cosa brutta e invece è interessante. Siccome non ci sono alternative, Apple è comunque costretta a tenere un certo livello di qualità. Può scoppiare uno scandalo, possono essere ribaltate decisioni stupide, si può avere un lieto fine anche dopo una brutta partenza.

Su Mac le alternative esistono. Così Apple può lavorare male perché tanto è ininfluente e gli sviluppatori fregarsene, per lo stesso motivo.

È concepibile blindare Mac come iOS? Naturalmente no. Quindi bisogna cambiare radicalmente le regole, fare a gara per avere quelli bravi, chiedere l’uno percento del prezzo di vendita e pazienza se la profittabilità sarà bassa. Credo che gestire il Mac App Store attuale non porti profitti particolari e sia pure un danno di immagine.