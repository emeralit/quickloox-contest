---
title: "L'improvvisazione al potere"
date: 2013-08-14
comments: true
tags: [iOS]
---
Buon Ferragosto! **Mario** ha descritto nel modo migliore il senso di [iOS applicato alle vacanze](http://multifinder.wordpress.com/2013/08/12/non-ci-sono-piu-le-vacanze-di-una-volta/).<!--more-->

Degna di nota anche una circostanza: da giovani si è fatta tutti una vacanza improvvisata, senza punti di riferimento, pianificata giorno per giorno.

Ma si dormiva e si mangiava seguendo un consiglio, la folla che sciamava verso il centro città, le indicazioni di una guida. Avere un panorama informativo completo su luoghi di ristoro e pernotto, e averlo aggiornato, e poter prenotare un giorno dopo l’altro sarebbe stato per molti versi impossibile oppure decisamente poco pratico.

Si commentano sempre gli utilizzi della tecnologia nel sostituirsi ai vecchi modi di procedere nella quotidianità e questo invece è un esempio concreto di tecnologia che consente qualcosa prima non consentito. Che non c’era e ha una vera utilità.