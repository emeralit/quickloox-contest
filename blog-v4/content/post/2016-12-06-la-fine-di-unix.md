---
title: "La fine di Unix"
date: 2016-12-06
comments: true
tags: [Humble, Bundle, Unix, O’Reilly]
---
Sta finendo una [offerta libraria da vertigine](https://www.humblebundle.com/books/unix-book-bundle) riguardante il lato tecnico di Unix.

Chi la lascia finire senza acquistare alcunché deve presentarsi in presidenza con giustificazione scritta dai genitori.