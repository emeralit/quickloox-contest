---
title: "La discarica dei cervelli"
date: 2023-10-15T16:07:10+01:00
draft: false
toc: false
comments: true
categories: [Internet, Enviroment]
tags: [e-waste]
---
[E ora qualcosa di completamente diverso](https://www.youtube.com/watch?v=tnC-75lR3S8), come [dicevano i Monty Python](https://www.imdb.com/title/tt0066765/): nel mondo le persone butterebbero in discarica nove miliardi di chilogrammi l’anno di spazzatura tecnologica, *e-waste*, *invisibile*.

Lo racconta [un articolo di Popular Scienze](https://www.popsci.com/technology/invisible-e-waste-pollution/) in occasione dell’International E-waste Day che si è appena svolto, sotto l’egida delle Nazioni Unite e del Waste Electrical and Electronic Equipment Forum.

Sarebbe spazzatura invisibile perché la gente non si renderebbe conto della sua natura di e-waste. Tra gli esempi, accessori con Led, cavi Usb, aggeggi contenenti una batteria ricaricabile al litio e gli apparecchi per fumare le sigarette elettroniche. Sarebbe anche spazzatura esplicitamente attribuibile ai consumatori e non alle aziende o ad altre organizzazioni.

Più di un chilo a testa ogni anno. Tutti ma proprio tutti, dal neonato all’ultimo dei grandi anziani, dall’americano ben nutrito al giovane che soffre il sottosviluppo africano, dalle megalopoli alla Patagonia. Soprattutto, mentre una ulteriore quantità di e-waste viene invece riconosciuta come tale e smaltita secondo le regole.

Senza avventurarsi in giornalismo investigativo, a me pare una affermazione molto forte. Vado sul personale non per cercare di fare statistica da solo, sempre attività ad alta rilevanza comica, ma per provare a esprimere un concetto.

Nella mia abitazione gli oggetti a rischio di e-waste non identificato sono decine se non centinaia. Mi guardo intorno e naturalmente vedo cavi e cavetti, giocattoli a pila, luci da lettura. vecchi alimentatori, due iPod, una antica fotocamera e via discorrendo.

Di e-waste in casa ne avrei ancora di più volendo, ma siamo per quanto possibile responsabili e abbiamo diligentemente portato materiali elettrici ed elettronici in discarica. Viviamo in Italia, fanalino di coda classico ma pur sempre civilizzato, nella parte settentrionale, dove forse le infrastrutture sono più sviluppate e il comportamento collettivo tende verso il rispetto delle regole.

Non faccio fatica a individuare un miliardo buono di persone che vive grosso modo in queste condizioni e ha un atteggiamento vagamente responsabile verso il tema. Il campione coincide con il maggiore utilizzo di futura spazzatura elettronica, peraltro.

La maggior parte di questi materiali, in mano a queste persone, finisce smaltita correttamente, fin dove lo si può vedere senza inseguire i camion che svuotano i container delle piattaforme ecologiche.

Non posso certo escludere che, sul tutto, di tanto in tanto un cavetto possa finire nell’immondizia comune per fretta, nevrosi, distrazione. Probabilmente di tanto intanto qualcuno svuota il box e raccoglie un sacco di e-waste fuori quota.

Ma un chilo, da tutti, tutti gli anni. Nella mia famiglia dovrebbero essere quattro chili. Da quando è nata la secondogenita a oggi, dovrebbero essere venti chili di spazzatura che non abbiamo riconosciuto come elettronica. Venti chili sono troppi anche per l’elettronica che usiamo invece di buttarla via.

Anche se fossimo irrispettosi delle regole, mica tutti lo sarebbero. Per fare danni per la nostra parte e per un’altra famiglia identica ma ligia al dovere, dovremmo produrre due chili a testa di e-waste. Otto chili di cavetti, accessori, alimentatori, caricatori, aggeggi vari. Neanche li compriamo, in un anno.

Non so. L’ambiente è importante, il recupero è sacrosanto, lo spreco va evitato, tuttavia certe affermazioni a effetto suonano un po’ spazzatura, certo non elettronica. Non so se e quanto aiutino la causa.

<iframe width="560" height="315" src="https://www.youtube.com/embed/tnC-75lR3S8?si=ZUk4if80IA-KHcu5" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>