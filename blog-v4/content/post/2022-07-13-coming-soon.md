---
title: "Coming Soon"
date: 2022-07-13T01:13:07+01:00
draft: false
toc: false
comments: true
categories: [Web, Hardware, Software]
tags: [MacStories, watchOS, watchOS 9, John Voorhes, Alex Guyot, iPadOS, iPadOS 16, iOS, iOS 16, Federico Viticci, Viticci, Guyot, Voorhes, macOS, Ventura, macOS Ventura]
---
Per le recensioni c’è tempo, però quest’anno ci sono da leggere anteprime interessanti. Sarò presuntuoso o approssimativo, però ritengo che ci sia semplicemente da stare collegati su MacStories, che è diventato da qualche tempo *il* sito per sapere di Apple le cose importanti.

Sto ancora leggendo e non ho molto da dire ancora; invito a farmi compagnia sulle [pagine di Alex Guyot dedicate a watchOS 9](https://www.macstories.net/stories/watchos-9-the-macstories-preview/) e su quelle [riservate a macOS Ventura a firma John Voorhes](https://www.macstories.net/stories/macos-ventura-the-macstories-preview/).

So che è molto da leggere. Apple fa… molte cose. L’età del monoprodotto, con il Mac e la LaserWriter a essere buoni, è finita da un pezzo. Se anche non ci fosse più innovazione, con l’evoluzione c’è da tenersi impegnati per un bel po’.

Bonus: il pezzo di Federico Viticci su [un mese trascorso in compagnia di iPadOS 16 e iOS 16](https://www.macstories.net/stories/ios-and-ipados-16-impressions/) è notevole e va letto subito per portarsi avanti, anche se – come lui stesso anticipa – è un’anteprima dell’anteprima. Eppure iPadOS 16, soprattutto, è una versione di sistema che introduce cambiamenti davvero importanti rispetto alla serie storica dei sistemi operativi per iPad.

Chi finisce prima racconta. 