---
title: "Il mio primo comando rapido"
date: 2019-07-26
comments: true
tags: [Shortcuts, Octopress]
---
Avevo già usato una singola istruzione qua e là, ma un vero comando rapido deve concatenarne almeno tre, facciamo anche due se non è una conseguenza indotta dal programma. Ne ho messe insieme sette; una sciocchezza per chi usa a fondo i Comandi rapidi di iOS, però intanto ho cominciato.

Il mio Comando rapido copia nella Clipboard uno header completo necessario a Octopress per tenere in ordine questo blogghino. Uno header è fatto così:

`---`
`layout: post`
`title: "Il mio primo comando rapido"`
`date: 2019-07-26 12:36`
`comments: true`
`categories: Shortcuts Octopress`
`---`

Il comando rapido mi chiede in input titolo e categorie e inserisce automaticamente la data. Le categorie vengono inserite in una variabile mentre il titolo viene digitato direttamente nel testo prima che venga passato alla Clipboard.

Le difficoltà sono state formattare correttamente la data (è occorso qualche tentativo per capire il formato) e poi in fase di debugging, quando data e categoria venivano inseriti una al posto delle altre. Ho risolto cambiando la sequenza delle istruzioni.

Alla fine incollo il risultato in un nuovo file creato a mano in Editorial. Dovrei fare una cosa più elegante e creare tutto in automatico; tuttavia Editorial è prezioso però non più aggiornato. Potrei creare un workflow dentro Editorial e poi richiamarlo dai Comandi rapidi, ma la realtà è che mi sto guardando intorno e potrei cambiare programma di editing, per cui non voglio restare eccessivamente legato.

I Comandi rapidi sono evidentemente giovani e promettono, con qualche difetto di gioventù, come quello dei valori scambiati di posto cui ho accennato prima. In compenso contengono già astuzie che facilitano molto. Per esempio, credevo di dovere creare una variabile per titolo-data-categorie e invece il software, intelligentemente, mi ha richiesto l’operazione solo nel caso della data. Gli altri parametri ho potuto inserirlo direttamente nella composizione dello header.

Mi piace. Voglio fare di più. Prossima lettura: [Shortcuts User Guide](https://support.apple.com/en-in/guide/shortcuts/welcome/ios).
