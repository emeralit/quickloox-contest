---
title: "Fantasie da cloud"
date: 2018-07-21
comments: true
tags: [iCloud]
---
È abbastanza facile condividere [i tre problemi di Apple con iCloud](https://9to5mac.com/2018/07/20/icloud-future-opinion/) come sono elencati da 9to5Mac:

- i cinque gigabyte gratuiti sono inadeguati rispetto alle necessità normali e appaiono sproporzionati rispetto a quanto spende l’acquirente tipico, che magari è anche di lungo corso ed è al quinto iPhone o al terzo Mac;
- la cifratura dei dati non è completa, end-to-end;
- non è chiarissimo all’utente medio che cosa viene salvato in iCloud e che cosa no.

Il secondo e il terzo punto sono poco interessanti; si chiede ad Apple di applicare cifratura integrale e dare più controllo e informazione sul comportamento in iCloud.

Si può parlare più costruttivamente del primo punto, perché si intravede una difficoltà di base, al contrario degli altri due dove si tratta quasi unicamente di prendere una decisione: per qualche motivo Apple è restia ad ampliare la fascia gratuita di iCloud e quasi certamente la questione è di costi.

*9to5Mac* propone tre soluzioni:

- alzare la quota gratuita di gigabyte e assorbirne il costo;
- prevedere una quota cumulativa per prodotto acquistato. Opero quattro apparecchi Apple sotto un singolo ID Apple e quindi dispongo di cinque gigabyte, esattamente come chi possiede un apparecchio solo e proprio come chi ne possiede ventisei. Non sembra equo; inoltre, chi usa Mac ininterrottamente dal 2000 potrebbe avere più spazio di chi lo usa da settimana scorsa, una sorta di premio fedeltà come scritto nell’articolo;
- offrire gratis uno spazio consistente ma solo per il primo anno.

Le soluzioni mirano ad aumentare la fruibilità di iCloud per l’utente e contenere il maggiore esborso per Apple.

La seconda mi pare quella più sensata e anche quella che rischia di ingenerare più confusione di gestione. Faccio solo un esempio: è ampiamente possibile che un singolo acquirente operi i suoi apparecchi sotto più ID Apple diversi.

Invece una soluzione possibile potrebbe venire dalla stessa Apple: quella attualmente applicata per le fotografie.

Attualmente iCloud accoglie Streaming Foto, cioè le mille foto più recenti oppure gli ultimi trenta giorni di scatti, e scarta quanto esce da questi limiti. Per un istante ignoriamo la questione del backup automatico delle foto su iCloud e consideriamo solo la loro memorizzazione primaria.

Come si fa a conservare foto su iCloud senza che spariscano una volta che escono dai confini di Streaming Foto? Si crea un album. L’album si conserva indefinitamente ed è indipendente dal limite dei cinque gigabyte.

Si tratta, soprattutto, di una soluzione che incoraggia un uso intelligente e responsabile di iCloud. L’ho impiegata per numerosi album e implica sapere che cosa si vuole memorizzare, assemblare l’album, dargli un nome, invitare eventuali spettatori… insomma serve un minimo di classificazione e di organizzazione. Qualcosa di molto sano e ben diverso dal buttare in rete tonnellate di scatti a casaccio che nessun guarderà mai più, pretendendone la conservazione eterna.

Facciamolo anche con i documenti. Diciamo che iCloud memorizza automaticamente gli ultimi cinquanta documenti o gli ultimi trenta giorni, dopo di che elimina quanto va oltre, a meno che l’utente si prenda cura di un documento a rischio di cancellazione. Come? Per esempio, associandogli metadati.

In questo modo si promuoverebbe un uso consapevole dei documenti e dei metadati e finirebbe su iCloud in modo stabile solo quanto si è ragionevolmente convinti di voler conservare, con un impatto sulle economie di iCloud altrettanto ragionevole.

In questo scenario non sarebbe male se iCloud si ispirasse alla sincronizzazione selettiva di Dropbox e permettesse di eliminare documenti da iCloud su un apparecchio e lasciarli invece sugli altri. Chi ha un terabyte di dati su iMac e un iPhone da trentadue gigabyte sa che cosa intendo: a volte la sincronizzazione totale non è desiderabile.

Altre opzioni?