---
title: "L’invenzione della filosofia"
date: 2022-07-27T01:26:41+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software, Internet, Web]
tags: [Meta, Mark Zuckerberg, Zuckerberg, metaverso, Apple, netbook, eWorld]
---
In mancanza di correnti d’aria che rinfreschino, *The Verge* prova a muovere qualcosa [creando un dualismo artificiale](https://www.theverge.com/2022/7/26/23279478/meta-apple-mark-zuckerberg-metaverse-competition) sulle parole di Mark Zuckerberg rivolte al personale di Meta.

Nel primo paragrafo si riporta come, secondo Zuckerberg, Meta e Apple siano impegnate in una *gara filosofica e molto profonda* a chi costruirà il *metaverso*, a suggerire – questo lo dice la testata – che le due aziende competeranno nella vendita di hardware per realtà aumentata e virtuale.

Nel secondo paragrafo, si scopre che Zuckerberg ha parlato di una competizione tra Apple e Meta per decidere *in che direzione dovrebbe andare Internet* e che Meta si posiziona come *alternativa più aperta e più economica ad Apple*.

Premessa: quello che dice internamente Zuckerberg, è detto per motivare le persone e per arrivare sui media. Corrisponde magari a quello che succede, con un ampio intervallo di approssimazione.

Ciò detto, complimenti per questa strategia di posizionamento verso Apple. L’alternativa più aperta ed economica. Davvero, in questi ultimi vent’anni non ci ha provato nessuno, premio originalità del secolo.

Secondo e più cogente. Le cose possono cambiare anche da domattina; finora, però, Apple non ha *mai* seguito una parola d’ordine del mercato. Quando la parola d’ordine era *netbook*, per dire, Apple [si è ben guardata dal produrne uno](https://macintelligence.org/posts/2009-10-24-in-attesa-del-notbook/) in quanto tale.

L’idea che Apple persegua una filosofia di costruzione del metaverso è integralmente ridicola. Apple segue strategie per valorizzare le proprie piattaforme e i propri prodotti. Non c’è dubbio che, se per farlo deve costruire un’esperienza di realtà aumentata, lo farà; ma lo farà in funzione della propria offerta, non di un concetto astratto.

Apple vende decine di milioni di prodotti hardware per la realtà aumentata: iPhone, iPad. Meta opera nel business dei social media e certamente vende visori per la realtà aumentata o virtuale, pochi, costosi, esoterici. Per vedere gli iPhone in mano a chiunque basta camminare per la strada. Che tipo di concorrenza sia questa, sfugge. Sono piani di attività trasversali, che certamente a un certo punto si intersecano e condividono una linea. A parte questo, i piani restano abbondantemente separati.

Credo che qualcuno in Apple si ricordi [eWorld](https://macintelligence.org/posts/2022-01-18-un-futuro-metadiverso/), dell’idea che rappresentava, della sua evoluzione. Ho dubbi che qualcuno vorrà ripetere quell’esperienza, come appunto scrivevo sei mesi fa. Mi sembra anche bizzarro che qualcuno voglia scommetterci l’azienda nel 2022, ma è chiaro che posso sbagliare.