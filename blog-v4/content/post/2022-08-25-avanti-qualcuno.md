---
title: "Avanti qualcuno"
date: 2022-08-25T01:17:16+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Torvalds, Linus Torvalds, Linux]
---
Dov’è il prossimo [ventunenne che chiede feedback per un piccolo progetto cui sta lavorando](https://www.wired.com/2009/08/0825-torvalds-starts-linux/) e finisce per diventare un simbolo (e non solo) di libertà e condivisione in rete?

Servirebbe, perché l’ossatura dell’open source ha perso molta consistenza grazie all’affetto che le multinazionali continuano a dimostrargli e il software libero rischia seriamente di diventare irrilevante.

In questi casi, l’unica regola che funziona nel gioco è rovesciare il tavolo e imporre un gioco nuovo, che colga impreparate anche le organizzazioni danarose e tentacolari.

Non necessariamente finlandese, non necessariamente ventunenne, va bene davvero tutto, ma per favore una bella grande idea da fare crescere per smuovere equilibri oramai troppo acquisiti.