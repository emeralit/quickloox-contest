---
title: "Tempus edax sicut draco"
date: 2024-01-19T00:46:14+01:00
draft: false
toc: false
comments: true
categories: [Internet, People, Software]
tags: [Mills, David Mills, NTP, Network Time Protocol, New Yorker]
---
Si deve dire un po’ cinicamente che di questo periodo scompare un padre di Internet ogni settimana. La [scomparsa di David Mills](https://twitter.com/om/status/1748439256299880881) tuttavia spicca su tante altre, perché lui ha creato Network Time Protocol, NTP, del quale racconta questo [spettacoloso articolo del New Yorker](https://www.newyorker.com/tech/annals-of-technology/the-thorny-problem-of-keeping-the-internets-time).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">RIP, Internet&#39;s Time Lord!<br><br>I want to take a moment and say a silent prayer for David Mills, the creator of Network Time Protocol (NTP), which is fundamental to the functioning of networks. Mills, nicknamed the Internet’s Time Lord by his peers, passed away on January 17 at the…</p>&mdash; OM (@om) <a href="https://twitter.com/om/status/1748439256299880881?ref_src=twsrc%5Etfw">January 19, 2024</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Grazie a NTP, miliardi di apparecchi riescono a sincronizzare il proprio orologio su un orario comune, anche in condizioni di linea critiche. Questo traguardo permette una quantità siderale di operazioni su cui si regge il mondo odierno. Senza Network Time Protocol il mondo sarebbe significativamente più arretrato di quanto sia. Ed è stato un lavoro quasi diabolico per difficoltà: nell’articolo un altro padre della Rete come Vint Cerf, vivente, lo paragona alla magia nera.

C’è un’ironia della sorte nella fine del tempo concesso a chi il tempo lo ha imbrigliato e ordinato per il progresso dell’umanità, una delle imprese più difficili tra quante hanno permesso la nascita e la crescita di Internet.

Come se il tempo si fosse vendicato su chi, novello Prometeo, lo ha messo a disposizione degli umani in forma universale.

Ma il genio non tornerà nella lampada. David Mills rimarrà un grande anche nel futuro e il tempo, *vorace quanto un drago*, può avere avuto alla fine ragione della sua manifestazione fisica. Ma nulla può contro il suo protocollo.