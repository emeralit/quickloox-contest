---
title: "Leggere leggero"
date: 2017-12-08
comments: true
tags: [NextDraft, Anteprima, Dell’Arti]
---
Rimango completamente convinto di quello che ho scritto oltre un anno fa [a proposito di umani, algoritmi e informazione](https://macintelligence.org/posts/2016-11-09-umani-e-algoritmi/) e rincaro la dose oltre a (ri)consigliare [NextDraft](https://nextdraft.com) per le notizie dagli Usa.<!--more-->

Giorgio Dell’Arti sta lanciando [Anteprima](http://anteprima.news), una newsletter mattutina che – se riuscirà a tenere il ritmo – permette di lasciare nel dimenticatoio tutte le schifezze che i quotidiani italiani osano chiamare siti.

Sarà a pagamento e so già che obiezioni riceverò; dico solo che merita.

Mancherebbe qualcosa del genere per la tecnologia digitale e per il mondo Apple in particolare.