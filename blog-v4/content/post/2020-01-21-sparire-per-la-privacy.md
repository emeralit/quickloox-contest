---
title: "Sparire per la privacy"
date: 2020-01-21
comments: true
tags: [iOS, Android, privacy, password, Bgr, Reddit]
---
Sincera curiosità: se riprendi uno screenshot del tuo apparecchio Android mentre mostra una password, [nel file della schermata puoi vedere la password](https://www.reddit.com/r/iphone/comments/eqox55/if_you_go_into_your_passwords_and_try_to/)?

Perché, se lo faccio su iPhone, il campo password appare vuoto.

 ![screenshot di coordinate bancarie dal quale iOS leva la password](/images/bpm.png  "screenshot di coordinate bancarie dal quale iOS leva la password") 

Basti pensare ai servizi di sincronizzazione che mandano sul cloud tutte le immagini prodotte su un apparecchio, per capire quanto sia una piccolezza, ma di una certa utilità.

(Inutile farsi domande sul numero utente; questo conto non esiste più da anni. Ma la password è tuttora presente nel mio iPhone).