---
title: "Professionisti e ciechi"
date: 2016-07-09
comments: true
tags: [Twitter, iPad]
---
Ho scoperto che [djay Pro di Algoriddim](http://www.algoriddim.com/) ha vinto quest’anno un Apple Design Award. E che djay per iPad [ha vinto un Apple Design Award](https://www.algoriddim.com/press_releases/188-algoriddim-s-djay-for-ipad-wins-2011-apple-design-award) nel 2011.

Tecnicamente non è la stessa applicazione, ma di fatto è la stessa base di codice o poco ci manca. Penso, attendendo smentita, che nessun’altra base di codice abbia vinto due volte un premio Apple Design.

Il quale premio è arrivato quest’anno in virtù delle funzioni di accessibilità inserite nel programma, che permettono anche a un non vedente di lavorare come un deejay professionista.

Detta così, la trovo spettacolare. Ops, ho detto professionista. Almeno chi lavora in discoteca e ai matrimoni può ancora usare un Mac per avere qualità professionale. C’è ancora speranza. Per chi riesce a vedere le cose come sono, ovviamente.

*[[Cuore di Mela](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/) è la nuova fatica editoriale di Akko. Lui però vuole farne molto più di un libro. [Chi ci sta?](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac)]*