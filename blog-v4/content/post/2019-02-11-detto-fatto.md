---
title: "Detto, fatto"
date: 2019-02-11
comments: true
tags: [Shortcuts, Comandi, Siri]
---
Spero mi si perdonerà l’ingenuità da fanciullo. Mi serviva rielaborare una serie di date e avevo a disposizione solo iPad. Nei Comandi rapidi ho trovato tutte le funzioni che mi facevano comodo e nel giro di pochi minuti avevo un automatismo prefetto.

Non solo: posso richiamarlo con Siri. Mi è diventato più facile dirlo che farlo, alla lettera.

A chi osserva che sto scoprendo l’acqua calda, rispondo che ha perfettamente ragione. Tuttavia voglio esortare quelli come me a scoprirla proprio come ho fatto io. Sorvolare sui Comandi rapidi è fin troppo facile per persone frettolose o impegnate e si finisce per sprecare una occasione preziosa di risparmiare tempo.
