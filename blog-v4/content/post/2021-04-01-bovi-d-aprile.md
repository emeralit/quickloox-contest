---
title: "Bovi d’aprile"
date: 2021-04-01T00:57:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [iOS, Apple, Android] 
---
Suppongo che voglia essere [uno scherzo](https://twitter.com/sramakk/status/1377384481632309251).

<blockquote class="twitter-tweet"><p lang="it" dir="ltr">Anche se il telefono è configurato in modo restrittivo e non è attivo, in media ogni 4,5 minuti, sia iOS che Google Android condividono i dati di telemetria con Apple e/o Google: <a href="https://t.co/OtCc127XGD">https://t.co/OtCc127XGD</a></p>&mdash; sramakk (@sramakk) <a href="https://twitter.com/sramakk/status/1377384481632309251?ref_src=twsrc%5Etfw">March 31, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

L’idea è che uno pensi che sia vero, scopra che non lo è e ci si facciano grasse risate sopra: gli scherzi funzionano all’incirca così.

A parte l’idea insinuata sotto sotto che *telemetria* significhi per forza *minaccia alla privacy*, un pochino forzata, un altro messaggio che passa con lo scherzone è che iOS e Android siano in fin dei conti equivalenti.

Poi lo scherzo finisce, quando si apre *Ars Technica* e si legge [Uno studio sostiene che Android invii a Google venti volte più dati di quanti ne invii iOS ad Apple](https://arstechnica.com/gadgets/2021/03/android-sends-20x-more-data-to-google-than-ios-sends-to-apple-study-says/).

Ma più che ai pesci, per la profondità dell’analisi, viene da pensare ai ruminanti, semplicemente alle prese con balle di news più che trifogli nel prato.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*