---
title: "Agosto, Apple Watch: ti riconosco"
date: 2022-08-12T03:23:53+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Apple Watch]
---
So che non dovrei temere alcunché, eppure per un qualche retaggio dimenticato ho sempre tenuto [watch](https://www.apple.com/it/apple-watch-series-7/) a secco quest’estate, togliendolo prima di entrare in acqua.

Ho fatto una eccezione e lui se ne è accorto, proponendomi di registrare una nuotata.

La parte interessante è che ero in una piscina per ragazzi, ad accompagnare le figlie. Si giocava, ci si muoveva, certamente non si nuotava in senso stretto.

L’algoritmo ha tuttavia centrato perfettamente la situazione, per lui inedita a mio riguardo. Quando a Wwdc si illustravano miglioramenti degli algoritmi di riconoscimento delle attività, non avevo dato all’annuncio più di tanta importanza.

Forse continua a non averne, però faccio [per la seconda volta](https://macintelligence.org/posts/2022-07-24-lora-di-comprarsi-un-computer-da-polso/) i miei complimenti al team.