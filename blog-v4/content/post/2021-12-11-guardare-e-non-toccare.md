---
title: "Guardare e non toccare"
date: 2021-12-11T00:37:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple Design Team, Wallpaper] 
---
Apple si racconta in questi ultimi anni molto più che nei precedenti. Nondimeno, trovare articoli come questa [panoramica di Apple Design Team](https://www.wallpaper.com/design/apple-park-behind-the-scenes-design-team-interview) è ancora evento raro.

Il testo è interessante, contiene qualche piccola rivelazione e descrive bene l’essenza del lavoro del team addetto al design dei prodotti.

Ma le foto. Il testo si può anche non toccare; le foto sono tutte da guardare, parlano in modo che più eloquente non si può. Di una filosofia, di un metodo, di un attitudine.

Basta guardare le foto e ci si fa un’idea piuttosto precisa di che cosa voglia dire design in Apple. Che cosa ci sia dietro un prodotto, i materiali, le forme, le scelte.

Alla fine viene anche da pensare che dire mi piace/non mi piace in base ad avere tenuto in mano un oggetto per qualche giorno, o per il colore, o per questo o quel comando, sia davvero un feedback inadeguato. L’uomo della strada che smanetta su una tastiera all’Apple Store e batte un team di specialisti al lavoro da anni su tutti gli aspetti di un prodotto… idea molto romantica e ottimistica. La realtà è che, nel novantanove percento dei casi, è sbagliata.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per Qui
ckLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._