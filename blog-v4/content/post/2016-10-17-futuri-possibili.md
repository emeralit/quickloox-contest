---
title: "Futuri possibili"
date: 2016-10-17
comments: true
tags: [bookabook, Savi, Daniele, crowdfunding]
---
Mentre [correggo gli impaginati della versione cartacea](https://macintelligence.org/posts/2016-10-15-un-kickstarter-per-cambiare-il-libro/) di *Cuore di Mela*, arriva una email da **Daniele**:<!--more-->

>Sto partecipando con il mio ultimo romanzo alla selezione di Bookabook, una startup editoriale milanese che investe sugli autori più meritevoli offrendo editing professionale, pubblicazione e promozione sul territorio nazionale. Per convincerli, però, devo superare una campagna di crowdfunding, raccogliendo 150 preordini.

>Il libro si intitola “La Teoria del Tutto”, ed è un'avventura fantastica ambientata a Milano. Per non dilungarmi qui, ho preparato [un minisito che spiega tutto il funzionamento](http://scrittorideltutto.it).

150 preordini è poco meno di quanto ha fatto nascere *Cuore di Mela*. Che è stato una bella fatica e a cui tengo.

Credo quindi di avere avvertito qualcuna delle emozioni e degli interrogativi che Daniele avverte in questo momento.

Per questo mi sento tenuto a dare notizia del suo lavoro e invitare tutti a valutarlo e premiarlo se lo ritengono valido e interessante. Non voglio parlare di prezzi e condizioni perché sono elementi secondari. Se vale, merita e su queste premesse mi sono mosso io per valutare il mio contributo all’iniziativa di Daniele.