---
title: "La cura dell’immagine"
date: 2023-03-22T02:30:50+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [ImageOptim. Drang]
---
Grazie ai [buoni uffici di Dr. Drang](https://leancrew.com/all-this/2023/03/imageoptim/) mi sono accorto di [ImageOptim](https://imageoptim.com/mac), programma open source per Mac che prende in carico una immagine e attua una serie di interventi in modo da ridurne il peso mantenendone la qualità.

Il tema è più sottile di *aumento il livello di compressione Jpeg*; ci sono per esempio metadati che possono trovarsi lì solo per portare via spazio. E gli algoritmi di compressione non ottimizzati fanno certamente un buon lavoro, ma non il migliore possibile.

Talvolta ho bisogno di contenere il peso di certe immagini e da domani ImageOptim verrà messo sotto collaudo.