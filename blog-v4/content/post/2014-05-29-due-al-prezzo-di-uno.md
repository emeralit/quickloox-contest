---
title: "Due al prezzo di uno"
date: 2014-05-29
comments: true
tags: [Chander, iOS8, iPad]
---
Tra i pettegolezzi precedenti il [convegno mondiale degli sviluppatori Apple](https://developer.apple.com/wwdc/) figura la possibilità che iOS 8 comprenda una modalità di funzionamento di due *app* affiancate sullo schermo di iPad.<!--more-->

Qualsiasi nuova funzione che si voglia aggiungere a iOS o OS X da una parte può risolvere un problema o consentire una nuova libertà. Dall’altra parte aggiungere una funzione significa aggiungere complicazione. Rendere il sistema più complicato porta più vantaggi che svantaggi oppure il contrario?

Ogni decisione in questo senso va pesata, se l’interfaccia deve restare la migliore del mondo. E tutto deve essere stato pensato il più possibile in anticipo.

Roopesh Chander ha provato a lavorare sull’idea e ha [riassunto le sue considerazioni](http://roopc.net/posts/2014/imagining-split-screen-ipad/). Occorrono nuove interfacce per i programmatori, risolvere ambiguità con il comportamento di elementi esistenti del sistema, trovare un metodo semplice da scoprire e da apprendere, immediato per i programmatori delle *app*, potente ed efficace. Questa la sua conclusione:

>Almeno inizialmente non vedremo in iOS il lavoro a finestre affiancate per le applicazioni indipendenti. Posto che veramente arriverà questa funzione, probabilmente sarà ristretta ad alcune delle app di Apple.

Cambiare un sistema operativo di successo in modo tanto significativo da mostrare due programmi dove prima ne stava uno solo, è complicato.