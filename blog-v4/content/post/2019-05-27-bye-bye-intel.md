---
title: "Bye bye Intel"
date: 2019-05-27
comments: true
tags: [Apple, Intel, Arm, MacBook, Pro, Logic, Photoshop, ]
---
Il [comunicato](https://www.apple.com/newsroom/2019/05/apple-introduces-first-8-core-macbook-pro-the-fastest-mac-notebook-ever/) di annuncio della nuova versione di MacBook Pro con processore a otto nuclei di elaborazione trasuda soddisfazione. Questi sono gli esempi di aumento delle prestazioni:

* I produttori di musica possono riprodurre in Logic Pro X progetti multitraccia massicci con un numero di plugin Alchemy fino al doppio di quello precedente [MacBook Pro 15” con processore a quattro nuclei].
* I progettisti 3D possono renderizzare scenari in Maya Arnold fino a due volte più velocemente.
* I fotografi possono applicare in Photoshop filtri e modifiche complessa fino al settantacinque percento più velocemente.
* Gli sviluppatori possono compilare codice in Xcode fino al sessantacinque percento più velocemente. 
* Scienziati e ricercatori possono computare in TetrUSS simulazioni complesse di dinamica dei fluidi fino al cinquanta percento più velocemente.
* I montaggisti possono lavorare in Final Cut Pro X con undici flussi di video 4K multicam simultanei.

Mica male, vero? Viene l’acquolina. Leggiamo il non scritto.

La prima considerazione, che c’entra poco ma è importante, è che Apple si è premurata di darci definizioni dettagliate della sua idea di *professionista*. È chiaro che non sono tutte, così come è altrettanto chiaro che si tratti di settori su cui è posato l’occhio, nel bene e nel male. Semira anche che tutti gli abbandoni di massa di cui si è letto senza prove negli anni abbiano lasciato circa il tempo che trovavano.

La seconda, che è dove si voleva arrivare, è il metamessaggio: *finalmente possiamo offrirti questo livello di prestazioni e abbiamo dovuto aspettare che Intel riuscisse a darci i processori che volevamo*.

Apple detesta le dipendenze da terzi per gli aspetti strategici della sua attività. Dunque niente più che un trionfale annuncio di processori di avanguardia su MacBook Pro evidenzia l’intenzione di arrivare a fare da sola prima che sia possibile.

Che accadrà davvero, e quando, difficile dire. Certamente però Tim Cook e compagnia non vedono l’ora di poter mettere nei Mac un processore pronto quando vogliono loro, prodotto su misura per i Mac, che faccia esattamente quello che gli si chiede. Come per esempio avviene nell’iPad Pro su cui sto scrivendo, che è una bomba.
