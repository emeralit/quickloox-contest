---
title: "Mannaggia al fotografo"
date: 2018-09-26
comments: true
tags: [Mann, iPhone, XS, Zanzibar]
---
Anche quest’anno Austin Mann si è sobbarcato la fatica di collaudare a livello professionale la fotocamera del nuovo iPhone e, dopo [Guatemala](https://macintelligence.org/posts/2017-11-08-obiettivo-guatemala/), [Ruanda](https://macintelligence.org/posts/2016-09-23-a-tutto-schermo/) e [Svizzera](http://www.macintelligence.org/blog/2015/10/14/le-recensioni-che-contano/), gli è toccata la faticaccia di [un viaggio a Zanzibar](http://austinmann.com/trek/iphone-xs-camera-review-zanzibar).

Dobbiamo capirlo e apprezzarne lo spirito di sacrificio.

Battute a parte, Mann scrive che da un aggiornamento *esse* di iPhone non si aspetta di solito grandi cose, ma stavolta i progressi della fotocamera sono *huge* (ingenti, enormi). A proposito della funzione di Smart Hdr:

>Non ho mai lavorato con una fotocamera capace di equilibrare la luce in questo modo; né che ci sia arrivata vicino.

Il progresso da iPhone X a iPhone XS, in termini fotografici, è davvero rilevante e non ci vuole l’occhio del professionista per finire con la mascella pendula dalla sorpresa.

Che l’occhio del professionista parli (è una figura retorica) in questi termini di un’apparecchiatura andrebbe tenuto in ugual conto. Mannaggia al fotografo che non si sente abbandonato e scatta immagini prodigiose con un telefono.