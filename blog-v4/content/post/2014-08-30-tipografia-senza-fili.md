---
title: "Tipografia senza fili"
date: 2014-08-30
comments: true
tags: [Wi-Fi, Helmholtz]
---
Tra le richieste di supporto che ricevo più spesso, figura ai primi posti un Wi-Fi che non funziona, funziona male, non arriva dove si vorrebbe e viceversa.<!--more-->

Nel tempo e con l’esperienza ho coniato una delle mie leggine *ad personam*:

>I problemi Wi-Fi sono dovuti a interferenze.

I guasti alle apparecchiature si isolano in fretta. Esclusi quelli, mai trovato una situazione dove una paziente – noiosa – scrematura di canali radio, posizionamenti degli apparecchi e considerazione degli ostacoli fisici ed elettrici non porta a una soluzione.

Il primo luogo dove questo è successo è casa mia. Il Wi-Fi funziona indisturbato da anni, ma è stato necessario sacrificare la parte migliore di un fine settimana.

Andare per tentativi e approssimazioni, tuttavia, svanisce di fronte a qualcuno che per governare il *wireless* di casa [ha risolto l’equazione di Helmholtz](http://jasmcole.com/2014/08/25/helmhurts/), che governa la propagazione delle onde elettromagnetiche.

Invidia. Ancora più invidia per la disinvoltura con cui l’autore del pezzo ha pubblicato sulla pagina web equazioni, matrici, grafici, Gif animate e video senza sforzo alcuno, dimostrando una comprensione del web davvero rara a vedersi, tecnica e di design.

Nei commenti all’articolo si accenna anche all’ipotesi di farne un servizio da erogare via web o una soluzione da vendere. L’autore nicchia, adducendo problemi di potenza di calcolo richiesta.

Non resta che confidare in [Briand06](http://melabit.wordpress.com) per la riproduzione dell’*exploit*.