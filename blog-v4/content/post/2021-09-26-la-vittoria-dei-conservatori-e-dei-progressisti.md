---
title: "La vittoria dei conservatori e dei progressisti"
date: 2021-09-26T00:28:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [OpenRA, Red Alert, Dune 2000, Command and Conquer] 
---
Oggi lezione di politica.

Il vero progressista non vuole levare di mezzo il passato. Vuole il progresso.

Il vero conservatore non rifiuta il futuro per aggrapparsi al passato. Vuole partire dal passato che vale e merita di essere conservato e usarlo per progredire.

Progressisti e conservatori, se sono veri, vogliono la stessa cosa.

Ecco [OpenRA](https://www.openra.net). Un vecchio gioco, bellissimo ma vecchio, viene ritenuto meritevole di proseguire la sua vita. Perché è vecchio, ma bellissimo.

Si forma un gruppo di lavoro, aggiornano la modalità di gioco, le mappe, le funzioni di gioco online, arricchiscono i contenuti, creano versioni native per tutti i sistemi più diffusi, rendono pubblico il kit di sviluppo software per chi volesse creare altri giochi basandosi sullo stesso meccanismo, creano una comunità dedita a tornei ed eventi.

Il progressista ideologico inorridirebbe. Lui voleva un gioco nuovo, perché questo era vecchio.

Il conservatore ideologico inorridirebbe. Questo non è più il gioco che c’era prima, perché i cambiamenti lo hanno reso nuovo.

Un vero progressista e un vero conservatore esultano, scaricano e iniziano a sfidarsi con tutte le gioie e i piaceri di un concetto di gioco solido e collaudato, reso nella forma più attuale e fruibile per una persona che vive nel presente.

Viva la politica fatta come si deve. Viva OpenRA.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*