---
title: "Serpenti di aprile"
date: 2019-04-01
comments: true
tags: [Google, Snake, Maps, Verge, Byte]
---
Fatico a vedere stupido, offensivo o fuori luogo l’offrire per una settimana [un videogioco dentro le mappe](https://www.theverge.com/platform/amp/2019/3/31/18289518/google-maps-app-snakes-game-april-fools-day) come pesce d’aprile.

Rimango pertanto della [mia opinione](https://macintelligence.org/posts/2019-03-28-insegnagli-a-pescare/).	

Sono sprovvisto di pesci; ho avuto bellissime idee che richiedevano troppo tempo, troppo sviluppo o ambedue. Le tengo per un futuro con più agio e magari anche un clima culturale più salubre. Saluto con una retrospettiva di uno dei migliori pesci di aprile su media informatici: *Byte* che [recensisce un computer da polso](https://www.inverse.com/article/54440-ps5-new-xbox-sony-microsoft-consoles-video-games). Di valore doppio pensando che, nell’ironia dello scherzo, la redazione ha prefigurato un oggetto che oggi al polso lo mettiamo davvero, certo senza la pretesa di inserirvi picofloppy disk.

Mi sarebbe piaciuto recuperare il pesce sempre di *Byte* che mi iniziò al primo di aprile tecnologico: la recensione di una matita descritta come un word processor innovativo dalle straordinarie capacità. Se qualcuno riesce a pescarla, gliene sarò grato.
