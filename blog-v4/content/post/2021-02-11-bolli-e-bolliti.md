---
title: "Bolli e bolliti"
date: 2021-02-11T01:48:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Ordine dei Giornalisti] 
---
Mi sono appena ricordato di avere regolarmente pagato la quota annuale di iscrizione all’Ordine dei Giornalisti e di dover ancora attaccare il bollino sulla tessera.

Una volta la procedura era assai analogica: l’Ordine spediva a casa il bollino, stampato su carta adesiva. Appiccicavo il bollino sulla tessera.

Oggi è giusto fare queste cose in digitale, per risparmiare risorse e tempo.

Così mi devo collegare al sito dell’Ordine, scaricare il bollino, stamparlo, ritagliarlo e appiccicarlo sulla tessera.

Motivo di orgoglio, probabilmente, deve essere per noi iscritti all’Ordine (come pubblicista, eh, il sottoscritto) l’avere una procedura di aggiornamento della tessera che descrive meglio di qualunque altra la situazione della pubblica amministrazione nazionale. Oltre ogni ragionevole stato di cottura.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*