---
title: "Uomini e topi"
date: 2019-06-05
comments: true
tags: [Apple, iPadOS, iOS, WatchOS, AirPods, macOS, Catalina]
---
All’unanimità, il premio per la funzione meno importante presentata nel *keynote* della [WWDC](https://www.apple.com/apple-events/june-2019/) è il supporto del mouse su iPad per aumentare l’accessibilità.

Ferma restando l’importanza dell’accessibilità per i disabili e ci mancherebbe, quanti scrivono *finalmente* come se ci fosse stata davvero una necessità fuori da situazioni speciali, suscitano perplessità.