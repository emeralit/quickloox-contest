---
title: "Una ricetta di valore"
date: 2023-01-30T01:43:46+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Common Lisp Cookbook]
---
Si trovano risorse per imparare Common Lisp ovunque, del tutto gratis e nulla di quello che sta per seguire è dovuto, necessario, per forza conveniente. Eppure.

[The Common Lisp Cookbook](https://lispcookbook.github.io/cl-cookbook/) è comodamente accessibile via web in edizione integrale, gratis. È un testo di base che va abbastanza avanti per chiunque sia seriamente interessato e risolve alcuni problemi iniziali di configurazione di poco peso, ma che possono disorientare un principiante, come scegliere un ambiente di sviluppo adeguato e che cosa installare per potersi muovere liberamente. Buona lettura, in definitiva.

Esistono l’edizione Pdf e quella ePub. Per averle ambedue, è necessario pagare una cifra a piacere, purché siano almeno sei euro.

I proventi vanno ovviamente a sostegno del progetto. I cui autori sono… chiunque; l’opera sta su Github e per aggiungere una sezione, correggere un errore, portare aggiornamenti, basta volerlo fare.

Mi sono immaginato (come autore; programmare Lisp al punto di insegnarlo, è un’altra cosa) al posto di qualcuno con la voglia, il tempo e la capacità di contribuire e sei euro li ho lasciati lì, anche se non era indispensabile. Non so se torneranno; in ogni caso, hanno di che divertirsi.