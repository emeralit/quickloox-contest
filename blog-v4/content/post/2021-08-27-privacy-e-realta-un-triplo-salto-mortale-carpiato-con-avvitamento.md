---
title: "Privacy e realtà: un triplo salto mortale carpiato con avvitamento"
date: 2021-08-27T00:49:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Edward Snowden, Apple, Csam, iPhone, Mit Technology Review, WhatsApp, Microsoft, Google, iMore, Ncmec, Imgr, SnapChat, iCloud, Instagram] 
---
Prima di spiegare perché Edward Snowden [ha attaccato in modo ingiusto e disonesto Apple](https://edwardsnowden.substack.com/p/all-seeing-i) rispetto al suo [programma di individuazione di contenuti pedopornografici su iPhone](https://macintelligence.org/posts/Grandi-e-piccine.html), è necessario contestualizzare.

C’è [un articolo di Mit Technology Review](https://www.technologyreview.com/2021/08/06/1030852/apple-child-abuse-scanning-surveillance/) che chiarisce la situazione.

Le autorità di tutto il mondo fanno grande pressione sulle società di Big Tech perché lascino porte aperte dove c’è intenzione di cifrare comunicazioni e file e perché siano parte attiva per segnalare crimini perpetrati (anche) utilizzando sistemi di comunicazione digitale. In questo, la diffusione e la detenzione di materiale pedopornografico ha una fortissima priorità. Ecco [che cosa può scrivere nel 2021](https://twitter.com/SenBlumenthal/status/1423397903637299206) un senatore degli Stati Uniti:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Thank you <a href="https://twitter.com/tim_cook?ref_src=twsrc%5Etfw">@tim_cook</a> for taking action to help catch sexual predators &amp; prevent the horrific abuse of children—now it’s time for others in Big Tech to step up &amp; for Congress to pass the EARN IT Act.</p>&mdash; Richard Blumenthal (@SenBlumenthal) <a href="https://twitter.com/SenBlumenthal/status/1423397903637299206?ref_src=twsrc%5Etfw">August 5, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

L’esortazione al Congresso riguarda una [proposta di legge molto controversa](https://cdt.org/insights/the-new-earn-it-act-still-threatens-encryption-and-child-exploitation-prosecutions/), che aumenta i poteri delle forze dell’ordine nel monitoraggio delle conversazioni online alla ricerca di tracce di abuso di minori.

Apple non si muove *in a vacuum*, come scriverebbero negli States, nel vuoto; c’è pressione da parte di autorità e governi. Ho già scritto che vorrei Apple disinteressata a quello che la gente memorizza sui propri apparecchi; questo, oggi, *non è possibile*. A meno che Apple chiuda o ridimensioni catastroficamente le proprie attività, per esempio smettendo di vendere computer, o eliminando iCloud.

Soprattutto, Apple si trova in una posizione doppiamente difficile. Nell’articolo di *Mit Technology Review* si accenna ai problemi di WhatsApp con i contenuti illegali (WhatsApp applica alla propria messaggistica cifratura end-to-end e in condizioni ordinarie non è in grado di spiare nei messaggi):

>WhatsApp contiene funzioni di reportistica che permettono a qualsiasi utente di segnalare contenuti abusivi. Per quanto le capacità del sistema non siano affatto perfette, l’anno scorso WhatsApp ha segnalato a NCMEC oltre quattrocentomila casi.

([Ncmec](https://www.missingkids.org) è l’associazione americana di riferimento per le iniziative contro l’abuso e lo sfruttamento dei minori).

**Quante segnalazioni di materiale abusivo di minori sono arrivate a Ncmec nel 2020?** [Ce lo ricorda iMore](https://www.imore.com/apple-child-safety-csam-detection-truth-lies-and-technical-details):

>oltre 21 milioni da provider Internet. Venti milioni da Facebook, compresi Instagram e WhatsApp. Più di 546 mila da Google, oltre 144 mila da Snapchat, 96 mila da Microsoft, 65 mila da Twitter, 31 mila da Imagr, 22 mila da TikTok, 20 mila da Dropbox.

>Da Apple? 265. Non 265 mila. Duecentosessantacinque.

**Come si crede che Google possa mettere insieme oltre mezzo milione di segnalazioni?** o Microsoft quasi centomila? O Facebook venti milioni? Hanno meccanismi di scansione dei rispettivi cloud e server, checché ne pensino i paladini della privacy.

**E Apple?** Come fa ad avere così poche segnalazioni? Ma prima di tutto, come fa *ad averne*, di segnalazioni?

Dal 2019 applica sistemi di identificazione delle foto abusive al traffico su iCloud Mail. Non su iCloud nel suo insieme; su iCloud Mail. La differenza è fondamentale perché, salvo eccezioni costituite da servizi relativamente piccoli oppure costosi, la posta elettronica viaggia universalmente in chiaro. Quella di tutti, non quella di iCloud. Se metto una foto in posta elettronica e spedisco il messaggio, chiunque sia in ascolto delle mie comunicazioni può vedere la foto. Chiunque.

Si capisce che l’email non sia un sistema ideale per fare viaggiare foto proibite. Quindi ne viaggiano poche. In chiaro; se cifro le foto prima di spedirle, rimangono inaccessibili a chiunque. Se criminali appena esperiti vogliono trasmettere foto proibite per email, le trasmettono cifrate. Oppure usano altri sistemi.

Mettiamoci nei panni del politico che vuole le Big Tech attive sulla lotta contro i materiali abusivi dei minori. Apple lavora diecimila volte meno di Facebook. Duemila volte meno di Google. quattrocento volte meno di Microsoft.

Il politico pensa che *Apple non sta facendo la sua parte*.

Non per niente, a un dirigente di Apple è scappato detto – in una conversazione privata divenuta atto giudiziario – che iCloud è oggi, involontariamente, [la migliore piattaforma per detenere materiale abusivo di minori](https://www.theverge.com/22611236/epic-v-apple-emails-project-liberty-app-store-schiller-sweeney-cook-jobs). Oppure, se vogliamo capovolgere la rappresentazione, quella che meglio protegge i dati dell’utente, persino quando questo sia un criminale.

Ora si dovrebbe avere capito perché si trova in una situazione di doppia difficoltà. Viene pressata dai politici, come tutte le altre, solo che non produce risultati.

Apple lo sa bene ed è per questo che la difficoltà è tripla. Perché ancora non dispone di una cifratura inviolabile su iCloud. Gli iPhone fisici non sono inviolabili (niente è inviolabile in assoluto), ma sono molto ben protetti. Servizi come iMessage sono analogamente molto ben protetti. Invece ci sono strati di iCloud che sono accessibili nel caso le autorità chiedano un mandato. La grandissima parte dei dati forniti alle autorità da Apple, sotto mandato, viene ottenuta bypassando gli apparecchi e accedendo a uno strato di iCloud non cifrato o di cui Apple possiede le chiavi.

Questo è dunque il triplo salto mortale: pressioni politiche, risultati pessimi, cifratura incompleta.

**Che cosa cerca di fare Apple?** È abbastanza chiaro ora: rispondere efficacemente alle pressioni politiche e mettere in opera un sistema che le consenta di *arrivare alla cifratura completa di iCloud ma avere comunque risultati da presentare*. Il tutto – carpiato con avvitamento – rispettando in modo ragionevole la privacy di chi usa i suoi apparecchi. Dettaglio (dettaglio?) trascurato con disinvoltura da tutti gli altri soggetti.

Può non piacere; a me non piace. Però il piano di partenza è questo, non l’utopia da cui parte Snowden come se tutto quanto sopra non esistesse. Domani spiego perché la critica di Snowden è davvero molto sotto quello che ci si aspetterebbe da uno con la sua reputazione. A partire dal fatto che a volere mettere le mani sulle foto abusive sono *i governi*, non Apple.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*