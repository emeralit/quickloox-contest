---
title: "Quattrocento milioni di domande"
date: 2015-12-30
comments: true
tags: [Game, Center, Apple, 9To5Mac, iTunes, Ping, watch, tv, iPad, iPhone, Mac, OSX, iOS]
---
Tematica interessante quella sollevata su 9To5Mac: [il numero totale di utilizzatori di Game Center](http://9to5mac.com/2015/12/29/game-center-players/). Il quale ha superato i quattrocento milioni, cifra assai o per nulla significativa, dipende da come la si guarda. La domanda veramente interessante è proprio quest’ultima.<!--more-->

<blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr">So Game Center App has a live real time total of users: currently at 405 million. <a href="https://twitter.com/asymco">@asymco</a> <a href="https://twitter.com/neilcybart">@neilcybart</a> <a href="https://twitter.com/benthompson">@benthompson</a> <a href="https://t.co/K0clNRKd47">pic.twitter.com/K0clNRKd47</a></p>&mdash; Kirk Burgess (@kirkburgess) <a href="https://twitter.com/kirkburgess/status/681725360149409792">December 29, 2015</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Intanto si deve notare che Apple è poco incline a mostrare numeri che non le vada a genio mostrare; non abbiamo avuto un dettaglio analogamente preciso degli utenti del defunto *social network* Ping legato a iTunes, uno dei tanti esempi possibili. Quindi quattrocento milioni è un numero buono.

Altrettanto chiaro è che molti fanno numero in modo poco consapevole e possiedono a malapena un profilo: semplicemente hanno giocato a qualcosa di collegato a Game Center e hanno un account iTunes.

Tuttavia c’è un livello di attività, per quanto minimo. Mi considero uno scarso frequentatore di Game Center ma tutti gli anni arriva qualche amico vero, non solo account fantasma, e a volte (rare) mi diverto a rispondere a una sfida.

Riassumerei nella mia esperienza personale che Game Center è una rete a bassa intensità, con interazioni vere per quanto rade e poco interesse diretto. Credo che nessuno sia interessato a attivare Game Center in quanto tale; l’attività indiretta provocata su Game Center dai giochi collegati, invece, deve essere un riverbero molto interessante del mondo dei giochi su iOS e OS X.

Buone informazioni per Apple, insomma, oltre che un servizio utile quella volta l’anno che veramente vuoi sfidare un amico. E quattrocento milioni significa, comunque si voglia depurare la cifra, fermento di centinaia di milioni di persone vere attorno ai giochi presenti su iPad, iPhone, Mac, domani Apple TV e magari pure watch.

Ecco perché è una buona cifra e perché la possiamo vedere.

P.S.: davanti a me in classifica su Game Center, solo sessanta milioni di avversari. Posso farcela.