---
title: "Questione di metodo / 2"
date: 2017-05-09
comments: true
tags: [ Jobs, iPhone, Cook]
---
Si è visto come l’attenzione serrata di Steve Jobs alla segretezza sia un tratto comune anche per la Apple dove lui non lavora più. Ci sono [altri tratti che sono rimasti inalterati](https://macintelligence.org/posts/2017-05-08-questione-di-metodo-i/), pur trasformandosi e maturando, diversi dall’imitazione acefala?<!--more-->

Per esempio, l’iterazione di prodotto. Il primo iPhone non aveva connessione 3G, aveva una sola videocamera e neanche fantastica, mancava di App Store e così via, tutta una lista di mancanze e scelte al ribasso che nei confronti dell’epoca lo ponevano in coda ad apparecchi sistematicamente più intelligenti, più evoluti, più potenti, più completi, più sportivi, più seri, più professionali e più variegati che sono spariti.

Passo dopo passo, edizione dopo edizione, iPhone ha sempre apportato miglioramenti e, quando non c’erano, diventava più sottile e leggero. È l’apparecchio più venduto nella storia dell’uomo, traguardo che mai sarebbe stato raggiunto se ci si fosse limitati anno dopo anno a gonfiare la Ram e aggiungere un *gadget*.

Anche iPad ha fatto in tempo ad attraversare una iterazione di prodotto durante la vita di Jobs e, di nuovo, il primo iPad che è uscito neanche aveva una videocamera. Le sue prestazioni e possibilità, rispetto a un iPad Pro di oggi, sono di un passato lontano.

Apple ha serbato e saputo coltivare questo tratto? Proviamo a guardare watch. Ne porto uno al polso: è superficialmente impermeabile (ok la doccia, ok la nuotata) ma non oltre. È privo di Gps e dipende strettamente da iPhone anche per molte altre applicazioni. È partito con cautela, tra le critiche e gli snobismi.

I dati di venduta assoluti non vengono comunicati, perché si tratta di un mercato piccolo piccolo, circa cinquanta volte quello dei telefonini. Tuttavia Apple ha fatto sapere che nell’ultimo trimestre le vendite sono quasi raddoppiate anno su anno.

Quatto quatto, watch è arrivato a vendere [più unità di qualunque altro *wearable*](https://www.engadget.com/2017/05/05/apple-watch-biggest-wearables-provider-q1-2017/), i computer che si indossano. Lista comprendente anche infiniti aggeggi capaci di costare anche un decimo del computer da polso Apple, [secondo per fatturato solo a Rolex](https://macintelligence.org/posts/2017-02-08-ora-secondi-a-nessuno/) se lo consideriamo come un orologio.

Un mercato piccolo piccolo. Un mercato [più grande di quello di Surface](https://www.cultofmac.com/479533/apple-watch-lot-bigger-think/), un successo [assai più relativo](https://macintelligence.org/posts/2017-05-02-pre-verita/) di quello che si racconta.

Ma divago. Il punto: l’iterazione sta pagando nel caso di watch esattamente come ha fatto con iPhone, iPad, tv. Il metodo di Steve Jobs è ancora in opera a Cupertino e porta risultati, anche se è difficile progettare serialmente apparecchi in grado di vendersi a miliardi come iPhone.
