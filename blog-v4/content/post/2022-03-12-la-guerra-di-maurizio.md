---
title: "La guerra di Maurizio"
date: 2022-03-12T02:56:12+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Maurizio]
---
I miei genitori hanno una seconda casa e Maurizio è un dirimpettaio. O meglio lo era: è venuto a mancare.

Maurizio aveva una certa opinione nei riguardi di una certa misura di prevenzione verso una certa malattia e disgraziatamente è stato colpito in forma grave proprio da quella malattia.

So che mi sentirò dire che un evento singolo non fa statistica e che le cose che ci toccano da vicino si vedono diversamente da quelle che non ci toccano eccetera.

Ecco, no. Perché sono un orso, piuttosto timido, che presso la seconda casa dei genitori tende a osservare orari anomali e comunque badare poco al mondo esterno. Maurizio era un dirimpettaio, tuttavia io nemmeno so che faccia avesse. Non l’ho mai incrociato e quindi mai neanche salutato. Voglio solo dire che il grado di empatia nei suoi confronti è identico a quello che provo per ogni altro estraneo, non che ci fosse antipatia o freddezza. Non l’ho mai visto e quello che so lo so tramite il resoconto dei miei genitori.

Essendo stato un dirimpettaio, il resoconto dei miei genitori su Maurizio è relativamente dettagliato. Lascia quattro figli dai dodici anni in giù. Due sono bambine; la loro madre, la vedova, viene da un Paese dove è diffusissima una religione che porta a una condizione della donna per un italiano molto discutibile. E pare che, a seguito del lutto, voglia portare i figli, soprattutto le figlie, proprio là.

Nessuno può dire con certezza che, se Maurizio avesse avuto una opinione diversa verso il rimedio alla malattia che lo ha colpito, e avesse adottato il rimedio, ora sarebbe vivo. Con certezza, tuttavia, le sue probabilità di restare vivo sarebbero state superiori.

Dell’opinione di Maurizio mi interessa poco. Tutti hanno diritto a un’opinione e al rispetto, se non dell’opinione, della persona che la esprime. Di lui, poi, so solo quanto ho appena scritto.

Da dirimpettaio, però, sono informato sulle conseguenze della sua opinione. Una famiglia sconvolta, figli la cui vita cambierà e non per forza in meglio, figlie per cui potrebbe cambiare molto, ma molto in peggio.

Una disgrazia, un incidente, una coincidenza sfortunata, un destino avverso, un malore improvviso accadono ovunque. Nessuno di noi è matematicamente certo di arrivare a domattina e questa è una condizione umana. Però poi esiste la responsabilità; se non ti allacci le cinture in auto, diminuisci le tue probabilità di sopravvivere a un viaggio. Se vai a nuotare in acque che la tua perizia di nuotatore non è in grado di dominare, rischi maggiormente di non tornare più a riva. Sono generalmente fatti tuoi. Se però hai la macchina piena di persone senza cintura di sicurezza, se porti a nuotare da inesperto qualcuno ancora più inesperto di te, l’opinione passa in secondo piano. La tua responsabilità è pesante e le opinioni anche bislacche non eliminano il rispetto dovuto; l’irresponsabilità invece sì.

Quello che dici non conta. Gli effetti sulle altre persone di quello che dici contano enormemente.

Perché scrivo tutto questo in un blog di tecnologia, Internet, Apple? Perché la mia, di opinione, è che la misura sia colma.

Maurizio è stato irresponsabile verso la sua famiglia, come minimo, e colpevole di questo. È stato anche vittima, però, di una macchina di disinformazione capillare, condotta con un cinismo e una indifferenza che sgomentano.

Deve essere chiaro che le vittime di questa macchina sono comunque colpevoli di quello che causano agli altri, delle conseguenze verso terzi della loro irresponsabilità. Chi fa girare la macchina ha solo colpe e il danno causato diventa sempre più ingente, neanche più limitato all’ambito sanitario.

In questi mesi, anni, ho seguito sui social media numerose persone vittime della disinformazione e colpevoli di diffonderla a loro volta. Ho provato a capire la loro situazione, mi sono immedesimato, ho studiato il loro modo di esprimersi e di giudicare. Qualche volta mi sono confrontato, per comprendere il loro atteggiamento, sempre con pacatezza e sempre sul merito delle informazioni, mai sulle persone.

Le conseguenze della loro irresponsabilità hanno ampiamente superato il credito di comprensione ed empatia che potevano eventualmente vantare nei miei confronti. Da ora farò qualsiasi cosa in mio potere per segnalarli, bloccarli, farli censurare, diminuire il loro _ranking_ nei motori di ricerca. Qualsiasi cosa.

Non per l’opinione che hanno, ma per le conseguenze della loro opinione. Ci sono persone che muoiono, o che si ritrovano in condizioni inumane, anche a causa di questi sostegni alla macchina della disinformazione. Fermarli è salvare delle vite e, se sono caduti in questa condizione a causa di un disagio personale o sociale, si farà di tutto per aiutarli.

Dopo. Quando si saranno zittiti. Le idee malvagie hanno pieno diritto di circolare e venire battute da idee benigne e migliori. Le idee false non meritano più alcuna condiscendenza, specie se contribuiscono a uccidere.