---
title: "Dove si va per i servizi"
date: 2019-04-28
comments: true
tags: [Apple, Evans]
---
Come per il [lato finanziario](http://www.macintelligence.org/blog/2019/04/25/azioni-coraggiose/), ci sono più modi di guardare alla [virata di Apple](https://macintelligence.org/posts/2019-03-26-chi-si-distingue-e-bravo/) verso lo sviluppo dei servizi. Li sintetizza bene Benedict Evans, [nel numero di tre](https://www.ben-evans.com/benedictevans/2019/4/4/apple-plus-brand-versus-subscription):

* Iterazioni cadenzate e predicibili del prodotto News/Games/Apple Pay, che innovano zero ma risolvono problemi di semplicità/accesso/scoperta. Manager di prodotto che fanno management di prodotto.
* Tutti questi prodotti tendono a rendere più probabile che il prossimo telefono acquistato sia un altro iPhone e naturalmente incrementano il fatturato.
* *Brand*.

La parte più interessante è questa. Certo, dopo avere messo in mano un telefono a tutti gli adulti del pianeta, spunti di crescita hardware ce ne sono pochi. Però leggerla sempre con gli occhi del contabile è limitante. La storia può avere anche un altro taglio; per esempio quello del ruolo che si ricopre sul mercato. Ieri la promessa, dice Evans, era *non devi preoccuparti che non funzioni*.

Quei tempi son lontani e oggi siamo tutti più distanti di una volta dal fatto tecnico. La promessa va allora rinnovata e oggi diventa *non devi preoccuparti che ti freghino*. I servizi Apple puntano su sicurezza e privacy come fattore che diversifica l’offerta rispetto alle norma.

Diventa di fatto una riedizione di *for the rest of us* per i tempi in cui dirlo nell’hardware non ha più senso e invece conta farlo con una carta di credito.
