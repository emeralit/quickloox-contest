---
title: "Talento non calcolato"
date: 2020-09-03
comments: true
tags: [GraphingCalculator, Calcolatrice, Grafica, Avitzur, TheLoop, Darlymple, Mac, Apple]
---
Ma in quante altre società potrebbe nascere un software come la Calcolatrice Grafica nel modo in cui è nata per Mac?

Una storia incredibile di clandestinità, ingegno e collaborazione solidale cui [si è già accennato](https://macintelligence.org/blog/2014/10/05/concorso-esterno/).

Ora *The Loop* ha rilanciato la stessa storia, stavolta [raccontata in video](https://www.loopinsight.com/2020/09/03/the-graphing-calculator-story/).

Ascoltata, invece che letta, è ancora più straordinaria, in senso etimologico.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Dl643JFJWig" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Se l’inglese è ostico e i sottotitoli di YouTube non bastano, qui sotto c’è un estratto da *Macintosh Story*: la storia espresso della Calcolatrice Grafica.

§§§

##La Calcolatrice del clandestino

Ron Avitzur è il padre della Calcolatrice Grafica, nome in codice NuCalc. Uno dei pezzi di software più eleganti mai presentati da Apple, che è stato per lungo tempo dotazione standard di Mac OS e, nei negozi più lungimiranti, costituiva una demo imbattibile.

Ma non avrebbe mai dovuto essere inserita nella dotazione dei Mac e, se è accaduto, lo si deve a una serie incredibile di circostanze.

Dopo avere iniziato a lavorare alla Calcolatrice Grafica a scuola, Avitzur viene assunto con un contratto a termine da Apple, per lavorare a un progetto segreto. Nel 1993 il progetto viene annullato. Ma Avitzur non vuole saperne di lasciare il lavoro a metà e il suo badge magnetico gli permette ancora di entrare in Apple, per cui continua a recarsi in ditta, anche se il suo lavoro ufficialmente non esiste più.

Avitzur entra in modalità *skunkwork* (*skunk* in inglese è la puzzola). Con questa parola si definisce in gergo una persona che lavora praticamente di nascosto, nella fiducia che la bontà del suo progetto convincerà qualcuno. Gli ingegneri lo conoscono e apprezzano molto le demo della Calcolatrice che lui organizza semiclandestinamente. Ma nessuno ha il potere o la volontà di farlo entrare ufficialmente in un team di sviluppo.

Avitzur non ha idea di come modificare la Calcolatrice per farla funzionare sul nuovo processore PowerPC su cui si sta portando Apple. Una sera di agosto, dopo cena, nel suo ufficio entrano due tizi che gli annunciano le loro intenzioni: stare lì fino a quando non abbiano adattato la Calcolatrice a PowerPC.

Dopo sei ore e innumerevoli modifiche a cinquantamila righe di codice, il software funziona cinquanta volte più veloce sul nuovo processore che su quello vecchio. Gran risultato, per le demo. Ma il prodotto è ben lontano dall’essere completo.

Nel frattempo un amico di Avitzur, Greg Robbins, termina il suo contratto con Apple e Avitzur gli chiede aiuto. Robbins accetta, si reca dal suo ex manager e annuncia che da quel momento lavora sotto Avitzur. Il quale dal canto suo inizia a spiegare in giro che avrebbe lavorato sotto Robbins. Stabilita una parvenza di ufficialità nessuno disturba più di tanto i due, che lavorano dodici ore al giorno sette giorni su sette.

Se qualcuno chiede spiegazioni, Avitzur dà una demo e illustra la sua situazione. Non avendo mutui né famiglia, può vivere dei suoi risparmi. Molti ingegneri in Apple hanno visto annullare progetti in cui sono coinvolti e Avitzur riscuote comprensione da tutti.

In settembre Apple riassegna a qualcuno l’ufficio dove lavorava Avitzur, ufficialmente vuoto. Emerge la questione. I badge di Avitzur e Robbins vengono disattivati e i due sono invitati a non farsi più vedere.

Ma intanto in Apple è stagione di licenziamenti e tremila persone stanno restando a casa. Avitzur e Robbins non sono a libro paga e il loro progetto ufficialmente non esiste. Continuano a vestire i vecchi badge, inutili per entrare ma funzionali per darsi un contegno. Gironzolano davanti all’entrata fino a quando non arriva qualcuno con un badge funzionante e, facendo finta di niente, gli si accodano. Molte persone li conoscono e, abituate a vederli in giro, nessuno fa domande.

La Calcolatrice Grafica a quel punto era un buon programma, ma non un prodotto finito. Gli ingegneri sono bravi a programmare, non a giudicare la qualità di quello che programmano. Nel frattempo la storia di Avitzur e Robbins è diventata piuttosto conosciuta ai piani bassi di Apple e la coppia viene contattata da due addetti al controllo qualità. Il loro lavoro normale è noioso; uno dei due ha un dottorato in matematica e ammira l’idea della Calcolatrice. Si offrono di dare una mano negli orari extralavorativi, senza dirlo ai loro superiori.

Nel frattempo un amico di Avitzur, esperto nella programmazione di grafica 3D, prende due giorni di ferie e dà una mano a realizzare tutta la parte di rendering grafico delle equazioni della Calcolatrice. Avitzur sostiene che, fosse toccato a lui, ci avrebbe messo un mese.

Rimane il problema che il progetto della Calcolatrice Grafica non esiste ufficialmente e stanti così le cose sarà difficile che entri a fare parte del software di sistema.

Una notte, alle due, nell’ufficio di Avitzur entra un perfetto sconosciuto, l’ingegnere responsabile della creazione fisica del master del disco di sistema di Mac OS. Se gli verrà consegnata una copia del software il giorno prima della produzione di massa dei dischi, spiega, la Calcolatrice potrebbe anche fare parte della dotazione.

La possibilità che la Calcolatrice possa veramente farcela a diventare realtà dà ad Avitzur possibilità che neanche con un contratto ufficiale avrebbe potuto avere. Per esempio, le macchine PowerPC disponibili sono pochissime. Ma gli ingegneri dell’hardware le portano a mezzanotte passata nell’ufficio di Avitzur, di nascosto, per riportarle in laboratorio altrettanto di nascosto, a collaudi eseguiti.

A ottobre quanti hanno aiutato il progetto di nascosto chiedoro alla coppia di dare una demo del prodotto finito ai loro superiori. Avitzur accetta, pensando che sia giusto dare in qualche modo ufficialità al progetto. La demo ha un successo oltre ogni previsione e i dirigenti chiedono ad Avitzur come mai non abbiano visto niente fino a quel momento. Lui spiega la situazione. Dopo avere capito che non è uno scherzo, i dirigenti gli raccomandano di non ripetere quella storia in giro.

Si verifica un paradosso. Essendo parte del software di sistema, la Calcolatrice viene sottoposta al controllo qualità (da parte degli stessi che ci avevano lavorato di nascosto). Il gruppo di localizzazione riceve l’ordine di tradurla in venti lingue. Il gruppo addetto alle interfacce deve verificarne la facilità di utilizzo. Ma Avitzur e Robbins, riferimenti del progetto, sono clandestini che non potrebbero neanche entrare nell’edificio da soli. Per avere un badge devono essere sotto contratto da parte di qualcuno, ma i contratti devono essere approvati dall’ufficio legale e loro, legalmente, sono fuorilegge. Un giorno, alla reception, l’ennesimo ingegnere tallonato per guadagnare l’ingresso si insospettisce e chiede l’intervento della sicurezza.

No, non sono assunti. No, neanche sotto contratto. Sì, devono avere accesso all’edificio, per via di software incluso in Mac OS. Alla fine ottengono un badge da *vendor*, come i camerieri della *cafeteria* e gli addetti alla riparazione delle fotocopiatrici. Però funziona.

Di nuovo paradossalmente, a quel punto Apple mette il suo peso a difesa del lavoro fatto. Wolfram Research, che ha creato il software [Mathematica](https://www.wolfram.com/mathematica/online/), chiede che la Calcolatrice venga ritirata per non intralciare sul suo mercato. Un programmatore indipendente accusa, falsamente, Avitzur di violazione di brevetto. Ma le battaglie legali e burocratiche vengono superate e nel gennaio 1994 la Calcolatrice Grafica viene infine completata. È stata inclusa in oltre venti milioni di macchine da allora. Ufficialmente, non è mai esistita.

La Calcolatrice Grafica non fa più parte di Mac OS, sostituita all’incirca da Grapher a partire da Mac OS X 10.4 alias Tiger, ma è viva e vegeta, con tanto di [versione iOS](https://itunes.apple.com/us/app/pacific-tech-graphing-calculator/id1135478998?ls=1&mt=8) (il link va all’App Store americano; su quello italiano nono sono riuscito a identificarla con certezza). Ron Avitzur ha dato vita a [Pacific Tech](http://pacifict.com). Sul sito si possono trovare i Viewer gratuiti per vedere i documenti creati dal programma senza bisogno di averlo, la storia originale, varie ed eventuali.

Ron Avitzur non ha mai occupato la ribalta ma per quello che ha fatto, e come, con la Calcolatrice Grafica merita un posto nella storia del Mac. Lui e le sue massime preferite. Una: *Il segreto della programmazione è avere amici tosti*. Due: *Il primo 90 percento del lavoro è facile, il secondo 90 percento del lavoro ti esaurisce, l’ultimo 90 percento del lavoro fa la bontà del prodotto*.

§§§