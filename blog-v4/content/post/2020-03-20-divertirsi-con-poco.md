---
title: "Divertirsi con poco"
date: 2020-03-20
comments: true
tags: [512Pixels, MacMadness, Rogue, Ars, Technica, roguelike]
---
Un attimo di distensione, anzi due: il primo per votare nella [Mac Madness](https://512pixels.net/2020/03/introducing-mac-madness-2020/) (scopo: fare avanzare sul tabellone il Mac o i Mac che si sono amati di più negli anni).

Il secondo per leggere la bella [storia dei giochi roguelike](https://arstechnica.com/gaming/2020/03/ascii-art-permadeath-the-history-of-roguelike-games/) appena apparsa su *Ars Technica*.

Ci vorrà più di un attimo, è lunga e articolata, eccellente per chi sia appassionato del tema (grafica Ascii, morte permanente, chi sbaglia ricomincia da capo anche se ha fatto un milione di turni per arrivare a un passo dall’obiettivo). Ma appunto, è divertirsi con poco, quando l’eroe che lotta per sopravvivere dentro un sotterraneo letale ha l’aspetto di una chiocciola.

Non sono ancora riuscito a finire [Brogue](https://sites.google.com/site/broguegame/) intanto, ed è una frustrazione.