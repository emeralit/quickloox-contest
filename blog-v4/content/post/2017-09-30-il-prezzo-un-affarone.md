---
title: "Il prezzo? Un affarone!"
date: 2017-09-30
comments: true
tags: [iPhone, X]
---
Da una autorevolissima ricerca che ho condotto negli spogliatoi del centro sportivo tra i compagni di cimento agonistico, risulta un vivo interesse per la novità [iPhone X](https://www.apple.com/it/iphone-x/).

Nonostante questo, nessuno è al corrente della data di uscita né delle sue prerogative più immediate, tipo il riconoscimento facciale ma anche l’ingombro ridotto a risoluzione elevata, o la mancanza del tasto Home.

Tutti sanno quanto costa, ma nessuno ne è scandalizzato: semplicemente dicono *costa troppo* oppure *ci sto facendo un pensierino*. Nessuna tirata, nessun moralismo, nessuna vanteria, nessun ditino alzato. Nessuno ha rievocato il [conte Oliver del Gruppo Tnt](https://it.wikipedia.org/wiki/Personaggi_di_Alan_Ford#Il_Conte_Oliver) e il suo intercalare preferito nelle trattative con il ricettatore Bing.

A volte la mente collettiva prende cantonate epiche. A volte è più saggia dei criticoni.
