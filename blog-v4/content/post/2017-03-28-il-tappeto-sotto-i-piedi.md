---
title: "Il tappeto sotto i piedi"
date: 2017-03-28
comments: true
tags: [8.1, 10.3, Apfs, Hfs+, iOS, iWork, Sierra, tvOS, watchOS]
---
Questa Apple che riposa sugli allori, non innova e considera solo iPhone, mi dicono, ha iniziato a distribuire iOS 10.3.<!--more-->

iOS 10.3 *cambia il filesystem*. Se sembra una delle tante funzioni “nuove” di cui si parla in occasione di un aggiornamento, si tenga presente che il precedente più recente è [Mac OS 8.1](http://www.info.apple.com/usen/macos8.1/), vent’anni fa.

Allora si passava da un filesystem piatto, che fingeva di essere gerarchico, a un sistema gerarchico vero, dove una sottocartella stava veramente sotto e improvvisamente diventava pratico e sostenibile stoccare molti più file in più di prima, con efficacia superiore.

Oggi si passa da quel sistema, che avendo vent’anni nel mondo tecnologico ne avrebbe duecento se fosse una persona, a [un’organizzazione nuova](https://arstechnica.com/apple/2017/03/a-tour-of-ios-10-3-checking-out-apfs-the-settings-app-and-other-tweaks/) e ancora più evoluta chiamata Apfs, dove un *crash* non fa più rischiare di ritrovarsi dati danneggiati, dove gli *snapshot* faciliteranno di molto le operazioni di *backup* e ripristino dei sistemi, dove il software occuperà perfino un po’ meno spazio grazie a una maggiore efficienza nell’archiviazione e mille altre cose.

Uno dirà ecco, Apple pensa solo a iPhone e ovviamente il nuovo sistema arriva lì come prima cosa. Arriva prima, solo che è *un rischio colossale*. L’installatore lavorerà più a lungo del solito: toglierà letteralmente il tappeto sotto i piedi del software per inserirne magicamente uno più robusto, gradevole e duraturo.

Se qualcosa andasse male nel passaggio di iPhone a Apfs, il danno sarebbe enorme, in soldi, reputazione, mancate vendite. Sarebbe un disastro. Ci pensasse solo a iPhone userebbe piuttosto come cavia una piattaforma meno frequentata e meno centrale, prima di mettere alla prova quella più importante e redditizia.

Questo aggiornamento di sistema è uno di quelli storici. Incidentalmente, nello stesso momento vengono presentati aggiornamenti a [Swift](https://swift.org/blog/swift-3-1-released/), [iWork](https://www.macrumors.com/2017/03/27/apple-updates-iwork-software-suite/), [Sierra, watchOS, tvOS](http://appleinsider.com/articles/17/03/27/apple-releases-macos-10124-watchos-32-tvos-102-updates) e chissà che altro sto dimenticando.

In un’altra epoca tutto si sarebbe fermato ad aspettare con trepidazione l’esito del passaggio ad Apfs. Che peraltro sarà installato in maniera semiautomatica su centinaia di milioni di apparecchi invece che venduto in centinaia di migliaia di esemplari. La portata e la complessità dell’operazione sono cresciute di tre ordini di grandezza. *Respect*.