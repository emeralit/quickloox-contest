---
title: "Chi si contenta"
date: 2017-04-24
comments: true
tags: [Numbers, Sheets]
---
Bene a protestare per i cambiamenti alla tastiera virtuale di [Numbers per iOS](https://itunes.apple.com/it/app/numbers/id361304891?l=en&mt=8). È arrivato presto [un aggiornamento](https://9to5mac.com/2017/04/25/iwork-ios-macos-numbers-ipad-keyboard-fix/), se non a sistemare, almeno a mitigare i problemi.<!--more-->

Il posto più vicino all’inferno comunque è un altro. In questi giorni uso sovente [Google Sheets](https://itunes.apple.com/it/app/google-sheets/id842849113?l=en&mt=8) su iPad con una tastiera esterna.

Ecco, i programmatori che hanno pensato all’interazione con Sheets attraverso una tastiera esterna sono persone molto cattive. O dell’altro. E in ogni caso, l’accontentarsi è quello. Un solo esempio: i tasti cursore orizzontali funzionano solo in editing della casella, quelli verticali solo nello spostamento da una casella all’altra. Si potrebbe andare avanti a lungo.