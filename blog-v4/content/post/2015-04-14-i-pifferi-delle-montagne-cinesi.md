---
title: "I pifferi delle montagne cinesi"
date: 2015-04-15
comments: true
tags: [Lei, Xiaomi, Apple]
---
Lei Jun, amministratore delegato di Xiaomi, si è lamentato che [i falsi e le imitazioni derubano l’azienda](http://www.bloomberg.com/news/articles/2015-04-09/xiaomi-confronts-counterfeits-as-fake-products-eat-into-sales) di una percentuale enorme del fatturato, che potrebbe essere due o tre volte superiore.<!--more-->

L’amministratore delegato suddetto, quando sale sul palco, indossa un maglione nero a collo alto, jeans e scarpe sportive, tanto per dire quanto Xiaomi si *ispiri* ad Apple. Non parliamo del *design*.

Andarono per copiare e furono copiati.