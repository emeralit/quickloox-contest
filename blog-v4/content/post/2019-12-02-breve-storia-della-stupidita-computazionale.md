---
title: "Breve storia della stupidità computazionale"
date: 2019-12-02
comments: true
tags: [Amazon, Aws, DeepRacer, DeepComposer, DeepLens, Colossal, Cave, Telegram, Pdp-10]
---
Se escludiamo la preistoria delle schede perforate, la storia della programmazione è iniziata con i *terminali*, [stupidi](https://www.webopedia.com/TERM/D/dumb_terminal.html) o [intelligenti](https://www.webopedia.com/TERM/I/intelligent_terminal.html): cioè dotati di un processore apposta per mostrare sullo schermo caratteri in grassetto o lampeggianti (il secondo caso) oppure neanche quello. I terminali erano collegati da qualche parte nel pianeta a un mainframe, che amministrava tutte le risorse di elaborazione. I terminali avevano il minimo indispensabile per ricevere messaggi dal *mainframe* e inviargliene.

legioni di programmatori veterani hanno iniziato la loro carriera digitando su un terminale privo di risorse di elaborazione, che staccato dalla rete era buono al più per illuminare la stanza. Dalla digitazione sui terminali, nel caso un [Pdp-10](http://www.columbia.edu/cu/computinghistory/pdp10.html), è nato per esempio [Colossal Cave](https://developers.redhat.com/articles/build-run-colossal-cave-adventure-game/), capostipite di una famiglia immensa e meravigliosa di giochi di avventura, oggi giocabile persino su [Telegram](https://telegram.org/) da chiunque inizi una conversazione con l’utente (robotico) @CaveBot. 

Amazon, non paga di avere presentato [DeepLens](https://aws.amazon.com/blogs/aws/deeplens/) nel 2017 e [DeepRacer](https://www.amazon.com/AWS-DeepRacer-Fully-autonomous-developers/dp/B07JMHRKQG/ref=sr_1_2) nel 2018, ha annunciato quest’anno [DeepComposer](https://www.amazon.com/dp/B07YGZ4V5B/ref=as_li_ss_tl), nell’ordine una fotocamera, un automodello e un sintetizzatore musicale.

In comune hanno l’integrazione con i servizi di *machine learning* di Amazon. Sono oggetti fatti per sviluppatori che vogliono imparare, sperimentare, progredire nella conoscenza e nell’uso delle tecnologie di intelligenza artificiale. In un attimo hanno accesso al cloud di Amazon e (a pagamento) a tutti i servizi di apprendimento meccanizzato che lì si trovano, tra i più avanzati al mondo.

Per esempio, DeepComposer è in grado di girare in chiave rock una melodia classica se gli si chiede di inserire nella partitura una batteria e un basso elettrico; non insulterò l’intelligenza di chi legge e non perderò tempo a dire quanto e come si possa fare con il machine learning a proposito di fotografia e autoveicoli, per quanto in scala.

Prodotti concepiti per gente che programma di mestiere, privi delle risorse per programmare autonomamente, con accesso integrato alle risorse di computazione più evolute al mondo o quasi e quindi con modalità di lavoro equivalenti, fatti quarant’anni di tara, ai terminali sui quali altri programmatori hanno dato vita a *Colossal Cave*.

*Colossal Cave* non è programmazione? Lavorare sul machine learning attraverso un sintetizzatore non è programmare? Io dico di sì. E quei programmatori di quarant’anni fa si sedevano davanti a un aggeggio privo di qualunque intelligenza, ma computavano e programmavano; semplicemente, lo facevano a distanza. Non erano computer, quelli che avevano davanti, anche se erano in sé scatole vuote con una tastiera e uno schermo? No.

Ma sono convinto che, nel chiacchierare con la moglie o con i colleghi, dicevano *adesso mi metto al computer che ho da fare*.

Il computer è quello che fai, non lo strumento che usi, sia stupido o intelligente.
