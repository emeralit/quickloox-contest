---
title: "Fallo di mano"
date: 2016-11-30
comments: true
tags: [AppleScript, Snell, AirPort, Express, Federighi, automazione]
---
Posto che sia vera, la notizia di Apple che [smette di costruire basi wireless](https://www.bloomberg.com/news/articles/2016-11-21/apple-said-to-abandon-development-of-wireless-routers-ivs0ssec) mi lascia circa indifferente. La AirPort Express che distribuisce il Wi-Fi in casa mia è in servizio da talmente tanti anni che ho perso il conto e mi dispiace dover progettare per un futuro l’acquisto di un aggeggio meno costoso e meno durevole. È anche vero che il Wi-Fi oramai è una *commodity*, un servizio acquisito. Mi pare che l’offerta media oramai sia decentemente affidabile e di qualità.<!--more-->

Sappiamo che è vera la notizia di Apple che [elimina la posizione di Sal Soghoian](http://www.macintelligence.org/blog/2016/11/20/aut-out/) e la cosa mi punge sul vivo, perché l’automazione è un pilastro essenziale del computing e su Mac in particolare, perché Mac ha sempre offerto possibilità superiori in qualità e quantità rispetto a quanto accade in purgatorio.

Craig Federighi, il responsabile software di Apple, ha dichiarato che l’azienda [ha ogni intenzione di supportare l’automazione su macOS](http://www.macworld.com/article/3143093/macs/apple-s-craig-federighi-every-intent-to-support-automation-in-macos.html), ma non è abbastanza: vogliamo che cresca, migliori, si sviluppi, arrivi a tante persone che usano Mac come se fosse un computer qualunque.

Un bell’esempio per comprendere è [l’articolo di Jason Snell su iMore](http://www.imore.com/all-ways-i-automate), che va sul pratico:

>Ogni volta che pubblico una immagine su Six Colors, lo faccio tramite un servizio: faccio control-clic sull’immagine, seleziono un servizio e l’immagine viene aperta in background dentro Photoshop, ridimensionata secondo le specifiche, salvata in Jpeg con impostazioni particolari e inoltrata al server tramite la app Transmit. Intanto l’Html necessario viene copiato nei miei Appunti pronto da incollare nell’articolo (questo incantesimo avviene grazie a un AppleScript che ho composto un paio di anni fa).

Come scrive Snell, l’automazione fa la differenza tra il tempo risparmiato e il tempo guadagnato; magari occorre sudare per un’ora per automatizzare un’operazione da dieci minuti ma, dalla settima volta che si richiede quell’operazione in poi, è tempo guadagnato.

Bisogna riconoscere che mai nella storia di macOS l’automazione ha ricevuto un trattamento prioritario. Al tempo stesso è più volte risorta dalle sue (quasi) ceneri e ognuno di noi è chiamato a usarla, mandare feedback, chiedere miglioramenti, diffondere il verbo, creare grande automazione per risolvere problemi non importa se piccoli.

E se la cosa non interessa, perché *non c’è tempo* o peggio perché c’è da imparare qualcosa, si sta perdendo qualcosa. Davanti allo schermo, tutte le volte che si usano le mani per agire bisogna chiedersi se siano necessarie. E, nel caso, fare in modo che ci pensi il computer. Rendendoci più liberi.