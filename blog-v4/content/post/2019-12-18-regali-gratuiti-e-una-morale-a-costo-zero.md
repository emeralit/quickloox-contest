---
title: "Regali gratuiti e una morale a costo zero"
date: 2019-12-18
comments: true
tags: [Vectornator]
---
Quando sei su Mac da trentacinque anni è difficile avere sorprese e vorresti sempre scoprire qualcosa di nuovo e interessante, solo che spesso non succede (più).

Quando arriva [Vectornator](https://vectornator.io): un vettoriale innovativo nell’interfaccia, di buonissime possibilità anche se al primo impatto richiede pazienza, gratuito e disponibile sia su iOS che su Mac.

Per me, che ho bisogno del vettoriale due volte scarse l’anno e certamente sono più che soddisfatto da una scelta decente gratuita, è un salto di qualità notevole. Fino a ora sono andato avanti con Inkscape oppure, per le cose veramente troppo semplici, con… Pages. Vectornator è una bella sorpresa per il 2020 e la regalo a tutti quelli che non hanno la necessità di pagarsi Illustrator o programmi commerciali impegnativi.

Una cosa, su Mac: Vectornator richiede come minimo Catalina. Non sono ancora passato a macOS 10.15 per motivi che spiegherò dettagliatamente a tempo debito e mi sono entusiasmato per Vectornator su iPad Pro.

La morale però è chiara: aggiornare ti porta anche novità che altrimenti non avresti.