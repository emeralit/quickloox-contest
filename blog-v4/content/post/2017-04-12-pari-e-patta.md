---
title: "Pari e patta"
date: 2017-04-12
comments: true
tags: [Schiller, Maggi, Sabino, Melabit, Mac, iPad, Salone, Risparmio, scacchi]
---
Ho passato la giornata al [Salone del Risparmio 2017](https://www.salonedelrisparmio.com/page/home) tra finanzieri, gestori di fondi, banchieri privati e personali, sviluppatori di software per istituzioni finanziarie, società di intermediazione e affini.

Ho visto la solita ventata di Mac ma, differentemente dall’[anno scorso](https://macintelligence.org/posts/2016-04-13-mac-e-banche-private/), c’era una valanga di iPad. Impiegati dai compiti più umili, come la raccolta dei dati anagrafici, a quelli più ambiziosi, come l’amministrazione di motori intelligenti di risparmio autogestito oppure l’editing video compiuto al momento, appena girata l’intervista.

Li ho fotografati tutti.

Però Apple [ha assunto una posizione ufficiale e precisa](https://macintelligence.org/posts/2017-04-07-mac-pro-e-iphone-4/), oltre che impegnativa, sul futuro dei Mac cosiddetti professionali. Sabino ha scritto un [bellissimo articolo](https://melabit.wordpress.com) e soprattutto Phil Schiller ha detto senza equivoci quello che andava detto. Cito da Sabino che ha già tradotto:

>“Quando parliamo di utenti ‘pro’, dobbiamo dire chiaramente che non c’è un utente professionale tipico. Pro è un termine molto ampio, che copre moltissime categorie [diverse] di utenti […]

>Ci sono i creativi, che producono musica o video, o gli artisti grafici, un segmento fantastico [di utenti] del Mac. Ma ci sono anche gli scienziati, gli ingegneri, gli architetti, gli sviluppatori di software, un segmento in grande crescita, soprattutto per quanto riguarda lo sviluppo di applicazioni sull’App Store.

>Ci sono moltissime cose e moltissime persone che possiamo chiamare ‘professionali’, flussi di lavoro professionali, e dobbiamo stare particolarmente attenti a non semplificare troppo, a dire ‘i professionisti vogliono questo’ oppure ‘i professionisti non vogliono quest’altro’, le cose sono ben più complicate di così.”

Insomma, il clima si sta pacificando. A ognuno il suo e speriamo tanti Mac Pro e iMac professionali il prima possibile. Allora mostro del Salone qualcosa di diverso, scelto tra simulatori di Formula Uno, golf giocato in realtà virtuale, distribuzioni di prosecco alle undici del mattino, *selfie* tra finti lingotti d’oro, *scooter* avveniristici e altri intrattenimenti vari.

 ![Partite a scacchi al Salone del Risparmio](/images/salone.jpg  "Partite a scacchi al Salone del Risparmio") 