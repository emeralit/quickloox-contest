---
title: "Non succede spesso"
date: 2021-04-24T21:28:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [iMac, watch, John Gruber, Daring Fireball, M1] 
---
John Gruber [fa notare su Daring Fireball](https://daringfireball.net/2021/04/spring_loaded_thoughts_and_observations) come lo spessore dei [nuovi iMac](https://www.apple.com/it/imac-24/) sia di 11,5 millimetri… e quello di un [watch Series 6](https://www.apple.com/it/apple-watch-series-6/) sia di 10,7 millimetri.

Se ci chiedessero di percepire la differenza con il tatto, in uno di quei test comparativi da eseguire a occhi bendati, potremmo solo tirare a indovinare.

M1 permette soluzioni che nessuno avrebbe mai nemmeno scritto come fantascienza, tipo un computer desktop con undici milioni di pixel sottile – in sostanza – come un orologio da polso.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*