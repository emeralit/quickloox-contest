---
title: "Ho fatto tredici"
date: 2019-10-10
comments: true
tags: [BBEdit, Grep, Pattern, Playgrounds]
---
Ho aggiornato [BBEdit](https://www.barebones.com/products/bbedit/bbedit13.html).

Ci sono decine e decine di [aggiunte e modifiche](https://www.barebones.com/support/bbedit/notes-13.0.html) ma mi bastano Pattern Playgrounds, Grep Cheat Sheet e il comando Command. E avanzano. Più supporto per l’uso delle espressioni regolari, miglior uso del programma con le mani che restano sulla tastiera; il resto, che è tanto, è già contorno. E parlo per esempio della Live Search: nel documento si evidenzia la striga cercata man mano che si digita.

Per qualcuno 34,53 euro sembreranno troppi. Per tutto quello che faccio con BBEdit, il supporto, gli aggiornamenti costanti, a me paiono una benedizione. Come rapporto prezzo/prestazioni, si batte a mani basse persino il gratta e vinci.