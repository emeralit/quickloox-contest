---
title: "La libertà per ventidue euro"
date: 2022-11-04T01:24:46+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [StopTheMadness, Jeff Johnson, John Gruber, Daring Fireball, Gruber, Johnson, Arial, Courier New, Helvetica, Courier, San Francisco]
---
Mi si ricorda che John Gruber [non sopporta Arial e Courier New](https://daringfireball.net/linked/2020/08/17/apple-developer-program-license-agreement), i font infestanti con cui Microsoft combatte la tipografia fatta come si deve e la libertà di espressione conseguente. In fin dei conti è una delle ragioni positive per cui seguo Gruber.

Vengo a sapere che Gruber ha chiesto a Jeff Johnson, sviluppatore di [StopTheMadness](https://underpassapp.com/StopTheMadness/), di aggiungere una funzione per cambiare automaticamente il font delle pagine web visitate, nel caso ci siano tracce di infezione da Arial e Courier New.

Johnson risponde che [lo si può già fare](https://underpassapp.com/news/2022-11-3.html) a partire dalle opzioni di configurazione di StopTheMadness. E Gruber, a contorno, spiega [come visualizzare le pagine in San Francisco](https://daringfireball.net/linked/2022/11/03/substitute-web-fonts-with-stopthemadness), il font istituzionale di Apple, se desiderato.

Tutto questo è bellissimo, prima di arrivare al punto: non sapevo che esistesse StopTheMadness. È un’estensione per Safari su Mac e [iPhone](https://apps.apple.com/it/app/stopthemadness-mobile/id1583082931?platform=iphone)/[iPad](https://apps.apple.com/it/app/stopthemadness-mobile/id1583082931?platform=ipad) che elimina una lista di comportamenti ostili all’utente messi in campo da un numero crescente di siti web.

La lista è lunga e inquietante. Ci siamo dentro tutti i giorni ma non ci si pensa. Ci dovremmo pensare. Il prezzo getterà nella disperazione le persone abituate a discettare gravemente di inezie semi-inutili che costano un decimo o un ventesimo.

Consiglio, per una volta, di leggere *bene* la descrizione su App Store. Leggerla *tutta*. Comprenderla. Pensarci sopra. E poi decidere se vale più tenersi il denaro in tasca oppure ritornare all’epoca in cui un sito era al servizio di chi navigava, invece del contrario.