---
title: "Solo una richiesta"
date: 2014-05-13
comments: true
tags: [Wwdc, iCloud, Drive, Dropbox]
---
Sollecitato sul tema del prossimo convegno mondiale degli sviluppatori Apple e sui probabili annunci conseguenti, ho partorito solo due idee, confuse a sufficienza.<!--more-->

La prima è una certezza: sentiremo molto OS X, a dispetto di quelli che *pensano solo a iPhone*.

La seconda è un desiderio. Uso molto Dropbox, molto Google Drive, molto iCloud e saltuariamente altro. Vorrei poter usare molto iCloud e poco tutto il resto.