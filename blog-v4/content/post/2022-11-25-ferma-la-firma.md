---
title: "Ferma la firma"
date: 2022-11-25T17:35:00+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Fsfe, Free Software Foundation Europe, Playdate SDK]
---
Sarà che Free Software Foundation Europe ha passato un brutto periodo in corrispondenza delle [tribolazioni di Richard Stallman](https://macintelligence.org/posts/2019-09-29-la-caduta-di-re-riccardo/), però alla giusta necessità di riprendersi dovrebbero corrispondere azioni di buonsenso. Apparentemente la [presa di distanza dal ritorno di Stallman](https://fsfe.org/news/2021/news-20210324-01.en.html) nel consiglio di amministrazione di Free Software Foundation (manca *Europe*) sa di buonsenso.

Invece la [lettera aperta per installare qualsiasi software su qualsiasi dispositivo](https://fsfe.org/news/2022/news-20221121-01.it.html) dimostra, di buonsenso, una mancanza totale.

Lo slogan semplice, senza se e senza ma, accattivante, suona molto bene, certo. Appena il pensiero scende un pizzico nei particolari, dove si nascondono come noto le creature maligne, lo slogan diventa un mostro capace di affossare un’industria.

Per farla breve, la domanda è chi pagherebbe per questa cornucopia di software installabile dovunque e comunque. Vale anche per i televisori e sul mio potrei chiedere di installare watchOS. Oppure [Playdate SDK](https://play.date/dev/). Mi chiedo se quei piccolissimi dettagli da risolvere spetterebbero a Panic Software, al produttore del televisore, a chissà chi. E quanto costerebbe. E come consentire, su queste premesse, di innovare o di fare meglio degli altri.

Le intenzioni di Fsfe non sono queste, ovviamente. Stallmaniamente, puntano ad avere la possibilità di installare software open source ovunque. E chi non è d’accordo? In larga parte, è pure possibile già ora.

Commovente l’intenzione di chiedere che gli utenti possano *continuare a utilizzare il proprio computer per quanto tempo vogliono*. Il web farà certamente il downgrade di milioni di pagine perché a casa ho un Sinclair QL che non conosce JavaScript, ma parte e funziona, quindi ho diritto di usarlo.

La causa del software libero è cruciale. C’è da trovare un modo per non lasciarla in mano a fanatici privi di senso comune.

Anche perché questo post, in realtà, poteva comporsi di una singola frasetta. *Le petizioni su Internet valgono niente e portano niente*.