---
title: "Se tanto mi dà tanto"
date: 2023-02-27T02:06:00+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Pages, LibreOffice, Word, giovanni68, LyX, TexMaker, LaTeX, Overleaf, VerbTeX]
---
Ringrazio **giovanni68** per avere portato la mia attenzione sul tema della [creazione di equazioni nelle app di produttività](https://muut.com/quickloox/comments#!/comments:posts-2023-02-26-la-cortina).

Da moltissimo tempo non ci davo un’occhiata ed effettivamente le cose sono cambiate tantissimo da come le me ricordavo.

Pages ha un [inseritore](https://support.apple.com/en-gb/guide/pages/tanca5a4fbd9/mac) che, per cose semplici, trovo splendido: in pratica basta scrivere l’equazione e ci pensa lui. Per fare cose complicate serve conoscere la quantità necessaria di MathML o LaTeX e l’editor non aiuta particolarmente la stesura di sintassi complicata.

LibreOffice offre un [sistema potentissimo](https://books.libreoffice.org/en/GS73/GS7309-GettingStartedWithMath.html) che però, se il lavoro da fare è semplice, mi sembra macchinoso. A quel punto forse ci si diverte di più con Grapher, che mi risulta [presente in Ventura](https://support.apple.com/en-gb/guide/grapher/gcalb3dec608/mac), per poi incollare il lavoro fatto in un documento dove serve.

L’unico momento dove si potrebbe preferire Word, a costo di sostenere le forze del male, è quando si lavora su una equazione di uso comune, che potrebbe trovarsi in un [elenco di formule già pronte](https://support.microsoft.com/en-us/office/write-an-equation-or-formula-4f799df7-4ca4-4670-afd3-6135768b01d0). Per il resto il sistema è fermo alla vecchia palette di simboli da cliccare. Sembrerebbe un vantaggio poter disegnare direttamente un’equazione con lo stilo o con il dito, o con il mouse (il cielo ce ne scampi). Lo è veramente? Troppi passaggi tra interfacce di input diverse. Non per niente, Pages su iPad [funziona per le equazioni come su Mac](https://support.apple.com/en-ie/guide/pages-ipad/tanca5a4fbd9/ipados).

Se toccasse a me dover lavorare con intensità su contenuti matematicamente importanti, credo che cercherei in ogni modo di usare [BBEdit](https://www.barebones.com/products/bbedit/) come editor e poi produrre codice LaTeX o MathML o quello che serve.

Al limite, un editor dedicato a LaTeX potrebbe fare al caso. Ne avevo indicati un paio parlando di [word processor su Mac](https://macintelligence.org/posts/2012-01-13-de-bello-scrivere/) molti anni fa e alla fine, differentemente che sulle app di produttività, è cambiato poco. In tempi moderni [LyX](https://www.lyx.org) va accudito con pazienza prima di poter lavorare (non è ancora Apple Silicon, vuole Python che non è installato di default, vuole [MacTeX](https://www.tug.org/mactex/)…). [TeXMaker](https://www.xm1math.net/texmaker/) lavora anch’esso in emulazione, ma almeno parte subito. Le interfacce degli editor LaTeX non sono stellari e forse vale la pena di stare online su [Overleaf](https://www.overleaf.com), il che chiude i conti anche con la sincronizzazione tra Mac, iPad e iPhone, salvo affidarsi a soluzioni come [VerbTeX](https://apps.apple.com/it/app/verbtex-latex-editor/id560869163) che hanno una app per ogni piattaforma.

Quanto è intenso il lavoro matematico richiesto? Questa domanda è la mamma di tutte le risposte sensate, quando si chiede che cosa usare per scrivere equazioni al computer. Fermo restando che l’interfaccia ideale per farlo in modo sensato usando solo la tastiera deve ancora nascere e nessuno se ne occuperà, in tempi dove l’equazione basta chiederla al sistema generativo di turno, che fornirà gentilmente un codice LaTeX *probabilmente* corretto e comunque correggibile in breve tempo. (Non usabile in Word, per paradosso).