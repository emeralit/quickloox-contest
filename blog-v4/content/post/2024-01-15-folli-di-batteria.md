---
title: "Folli di batteria"
date: 2024-01-15T00:06:32+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Tom’s Hardware, BetaVolt]
---
Posto che, come anche [commenta Tom’s Hardware](https://www.tomshardware.com/pc-components/power-supplies/chinese-developed-nuclear-battery-has-a-50-year-lifespan), la strada è lunga e le sfide notevoli, mi piacerebbe un sacco vedere [la tecnologia promessa da BetaVolt](https://www.betavolt.tech/359485-359485_645066.html) arrivare al livello richiesto dai device di oggi e di domani.

BetaVolt ha annunciato batterie tascabili a energia nucleare piccolissime (quindici per quindici per cinque millimetri), con autonomia di cinquanta anni e sicure poiché basate su un isotopo del nickel che decade in un isotopo stabile del rame.

Al momento l’energia erogata è minima, cento microwatt a tre volt, con una versione da un watt promessa per l’anno prossimo. Le batterie si possono collegare in serie o in parallelo, ma è evidente che c’è tutta una fetta di mercato che si aspetta molto, molto di più.

Se ci riuscissero, però, sai che spettacolo? Il Codacons scende in piazza a protestare, iFixit dichiara il blocco navale dei container cinesi, la California discute l’estensione del *right-to-repair* agli eredi degli eredi…