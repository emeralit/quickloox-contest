---
title: "Cose a caso"
date: 2022-11-15T00:18:00+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [AppleScript, Comandi rapidi, Shortcuts]
---
Mi serviva estrarre un elemento a caso da una lista.

[AppleScript](https://macosxautomation.com/applescript/firsttutorial/index.html):

`set miaLista to {"Uno", "Due", "Tre", "Quattro", "Cinque", "Sei"} as list`<br>
`set sceltaCasuale to some item of miaLista`<br>
`display dialog sceltaCasuale`

[Swift](https://github.com/apple/swift):

`let lista = [1, 2, 3, 4, 5, 6]`<br>
`let sceltaCasuale = lista.randomElement()!`<br>
`print(sceltaCasuale)`

[Comandi rapidi](https://support.apple.com/en-gb/guide/shortcuts/welcome/ios):

![Scelta casuale di un elemento da una lista in Comandi rapidi](/images/lista-shortcuts.png "I Comandi rapidi a volte sono di una semplicità disarmante.")

Mai fidarsi di quelli che si lamentano dell’impossibilità moderna di smanettare in macOS. Se sembrano cose difficili, non lo sono (ci arriva veramente chiunque) e poi i Comandi rapidi sanno essere a volte astrusi, ma altre volte talmente facili che è più difficile non usarli che il contrario.