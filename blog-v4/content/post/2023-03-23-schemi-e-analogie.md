---
title: "Schemi e analogie"
date: 2023-03-23T02:42:15+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Hofstadter, Douglas Hofstadter, Fluid Concepts and Creative Analogies, Concetti fluidi e analogie creative, A2, Strozzi, Marin, Filippo Strozzi, Roberto Marin]
---
Lunedì prossimo mi ritrovo con [Filippo e Roberto](https://macintelligence.org/posts/2023-03-08-detto-tutto/) a chiacchierare di intelligenza artificiale a scopo podcast e intanto continuo la [rilettura e riscoperta del libro Concetti fluidi e analogie creative](https://macintelligence.org/posts/2023-03-13-pattern-chiari-amicizia-lunga/).

Significa che dà una parte seguo lo sviluppo dei sistemi generativi basati su grandi modelli linguistici e dall’altra parte seguo un interrogarmi profondo e concreto sulle radici dell’intelligenza.

Da sostenitore dell’intelligenza artificiale forte come sono sempre stato, la risultante è che sono entusiasta delle possibilità e degli orizzonti che si aprono grazie ai sistemi che iniziano a spuntare come funghi. Contemporaneamente, perdo la pazienza in un millisecondo quando sento parlare con convinzione di intelligenza artificiale applicata a *loro*, che di intelligente hanno il nulla assoluto.

(Per chi si fosse perso gli anni novanta: intelligenza artificiale forte è quella che arriva all’autocoscienza e alla capacità di ragionare come ragiona un essere umano, o almeno emularne molto bene i meccanismi; intelligenza artificiale debole è quella che riesce ad affrontare problemi applicando algoritmi che trovano soluzioni non originalmente presenti nei dati, ma rimane un livello sotto l’intelligenza umana. Ci fu e c’è ancora un dibattito articolato tra sostenitori della possibilità di raggiungere l’autocoscienza e quanti lo ritengono impossibile a priori).

Da una parte siamo al poter installare su un Mac un modello linguistico in locale e giocare, in senso allargato, con i dati. In teoria nulla mi vieta di dare in pasto al sistema tutti i post di questo blog e tirare fuori dei post generati artificialmente. Mi stancherei presto, ma l’esperienza sarebbe interessante e certamente nuova.

Dall’altra siamo, eravamo – *Concetti fluidi* è del 1993 – a ragionare sui mattoni fondamentali dell’intelligenza come il riconoscimento di schemi, la formazione di analogie, la microattività del sistema sotto il livello dell’introspezione che porta a macroaggregsti di pensiero sempre più complessi. La realtà attuale dei sistemi di cui tutti oggi parlano, per quanto affascinante e coinvolgente, con ricadute potenzialmente enormi in qualunque campo, sta all’intelligenza artificiale quanto una pozzanghera a un lago.

*Ma che ti frega di come si chiama, l’importante è lo strumento e che cosa ci si fa…*

Col cavolo. Il fatto di chiamarlo intelligenza artificiale provoca un sacco di cortocircuiti in gente anche di buona volontà e con capacità sopra la media, che però cede al fascino dell’oracolo, vede un prodotto facile da vendere a clienti ignari, crede e fa credere che il *prompt engineering* sia davvero una nuova professione. Cioè, ci sono danni concreti. *Ma ci sono già gli annunci di ricerca di personale…* Certo e li ha pubblicati qualche fesso che si è fatto intortare, pronto ad assumere gente non so se più intortata ancora o solo pronta a cogliere l’attimo e rubare uno stipendio finché dura. Sono già in uscita i libri *for dummies*, con un pubblico che presumo scemo abbastanza da avere bisogno di (credere in) intelligenze artificiali.

Chiamarli assistenti generativi, per dire, sarebbe onesto, chiaro e preciso. Troppe persone passano sopra alla questione per soldi, narcisismo o ingenuità e non va bene.

Sarà stimolante. Cercherò di non essere troppo bastian contrario.