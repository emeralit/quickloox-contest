---
title: "un buon indirizzo di iCloud"
date: 2021-11-19T02:36:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [iCloud, Mail, Linode] 
---
Ho cercato recentemente di mettere in piedi un server di posta elettronica su un mio dominio e di farlo da solo, recuperando via rete le competenze necessarie passo dopo passo. Non ci sono ancora riuscito; è un procedimento che chiede grande familiarità con installazioni di più software a basso livello, la compatibilità reciproca dei quali spesso dipende dall’intuizione di chi installa più che dall’intesa collaudata su protocolli precisi. Dipendenza dopo dipendenza, peculiarità dopo peculiarità, sono arrivato a un punto che non riesco a passare e per cui non emerge alcun aiuto dalla rete. La mancanza di tempo e concentrazione fanno il resto. Il lavoro è da finire.

Un indirizzo email da gestire però ce l’ho e mi ha sempre dato fastidio negli ultimi mesi che restasse inutilizzato a causa dello stallo di cui sopra.

Per questo mi sono lanciato nella attivazione del servizio di [dominio personalizzato di email](https://support.apple.com/en-us/HT212514) su iCloud: in pratica, inviare e ricevere posta da e per un mio dominio affidandosi all’architettura di iCloud stesso.

Dopo qualche ora di tentativi, la procedura è riuscita.

Semplice, non banale. Apple fornisce istruzioni molto buone, ma si tratta pur sempre di attivare record di [DNS](https://aws.amazon.com/it/route53/what-is-dns/) su un pannello di controllo di un provider.

Nel mio caso, provider [Linode](https://www.linode.com), ci ho messo del tempo per identificare la grafia corretta di alcuni record. Apple vuole che la stringa di certi parametri abbia un punto finale e Linode lo accetta al momento di digitarlo, solo che poi non lo mostra. Analogamente, un altro parametro andrebbe inserito circondato da doppie virgolette secondo Apple, mentre in verità Linode non ne vuole (un altro provider potrebbe funzionare diversamente).

Dovevo infine creare un record [SPF](https://www.dmarcanalyzer.com/spf/), dice Apple, solo che il pannello di controllo di Linode non prevede questa possibilità.

Nello scoprire la soluzione ai vari enigmi è risultato strumentale il supporto tecnico di Linode, stellare per usare un eufemismo. Un addetto ha preso a cuore la situazione e mi ha spedito non solo chiarimenti ma anche una spiegazione di come tenere d’occhio i parametri inseriti con il comando da Terminale [dig](https://linuxize.com/post/how-to-use-dig-command-to-query-dns-in-linux/) e verificare grazie a [DNS Checker](https://dnschecker.org) la propagazione dei DNS modificati.

Senza il suo aiuto avrei davvero faticato, per esempio, a scoprire che è possibile creare un secondo record TXT se l’interfaccia non prevede un’opzione SPF.

L’esperienza mi fa raccomandare Linode. Un supporto così puntuale e dettagliato, per non parlare dei tempi, vale e stravale cinque dollari al mese.

iCloud Mail mi consente di (riprendere a) utilizzare un indirizzo veramente personale, agganciato a un dominio di mia proprietà. Apprezzo l’estensione delle possibilità del servizio di Apple (accessibile a chiunque paghi per iCloud; il sottoscritto versa 99 centesimi al mese).

Apro il weekend con soddisfazione.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._