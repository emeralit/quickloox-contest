---
title: "Due app, un gusto"
date: 2015-09-08
comments: true
tags: [watch, Mappe, Attività, iTunes, Mac, iPhone, TV]
---
Questi giorni sono più propenso a parlare di watch che di altro. Dipende dal fatto che è una piattaforma nuova, con quello che ne consegue in esplorazione ed esperienza.<!--more-->

Oggi esperienza doppiamente positiva: in centro storico di città diversa dal solito, orientamento con le mappe su watch. A sera noleggio di film su TV, usando il computer da polso come telecomando.

Nel secondo caso, essendo la prima volta, ho dovuto apparentare l’apparecchio alla libreria di iTunes e, nel farlo, passare dall’autenticazione a due fattori che invia un codice su iPhone. Quindi ho dovuto operare brevemente sia su quest’ultimo che su Mac.

A parte questo, iPhone è poi rimasto al suo posto. Avere il telecomando sul polso è liberatorio e persino divertente, forse per la novità. Diventa incentivo a indossare watch anche a casa, la sera almeno.

Le mappe sono uno spettacolo. Data la dimensione del quadrante, uno non ci scommetterebbe un centesimo. La magia consiste nell’affidare lo zoom alla corona digitale. Si scorre con un dito, si ingrandisce o riduce con un dito e funziona. iPhone rimane in tasca.

Confesso di avere seguito già un paio di volte i consigli della *app* di attività, che esorta ad alzarsi se si è seduti da un’ora. L’esperienza è gentilmente e subdolamente persuasiva, nonché vagamente gratificante.

Per dire che ero scettico, ma devo ammettere che c’è gusto a usare watch.