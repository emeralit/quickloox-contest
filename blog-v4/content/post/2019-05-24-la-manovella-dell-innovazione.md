---
title: "La manovella dell’innovazione"
date: 2019-05-24
comments: true
tags: [Playdate, Panic]
---
Nel dibattito sull’innovazione, e anche oltre, oggi si può parlare solo di [Playdate](https://play.date), la microconsole annunciata da Panic per l’inizio 2020.

(Quasi) un quadratino con il lato che eccede di poco i sette centimetri, quasi un centimetro di spessore, schermo Lcd bianco e nero da 400 x 240 punti, con una manovella sul lato a integrare i pulsanti, sarà venduta con dodici giochi a sorpresa, realizzati da programmatori indipendenti, che arriveranno uno per volta ogni lunedì sull’apparecchio del proprietario. Prezzo, 149 dollari.

L’oggetto è incantevole e io dico, nel dibattito, che questo è un esempio micidiale di innovazione in questo secolo. Si è già visto tutto in termini di console e videogiochi, per esempio, ma l’offerta di Playdate è fuori da ogni logica tradizionale in tema. Come privi a incastrarla in una categoria, in un filone, ti rendi conto che non vi sono oggetti analoghi o che le differenze sono tali da rendere razionalmente impossibile una associazione.

Per esempio, microconsole con schermo in bianco e nero se ne sono viste, ma a questa risoluzione e con questa capacità di riflessione, in queste dimensioni, zero. La dotazione di giochi e la modalità di reperimento degli stessi è sostanzialmente inedita. Chi altri offrirà giochi a sorpresa? O ti farà aspettare una settimana prima di vedere il gioco successivo?

Ancora, in Panic hanno scritto un sistema operativo apposta. Niente Linux, Android, altro di esistente. In pieno 2019 annunciano che, nonostante la dotazione di Bluetooth e Wi-Fi permetta tranquillamente di offrire giochi multiplayer, si concentreranno su quelli in solitario. Sappiamo già che uno dei primi dodici giochi usa volutamente come strumento di input la manovella. Quale altro apparecchio al mondo permette una esperienza del genere? Nessuna.

Panic, peraltro, non ha alcuna ambizione di dominare il mondo o di sventare chissà che concorrenza. Playdate si pone con eleganza e sfacciataggine in un ambito completamente proprio.

Prima provocazione: sono convinto che sia innovazione e di brutto. Qualcuno penserà il contrario e sono disposto ad ascoltare, ma desidero una bella spiegazione e non una frase fatta.

Seconda provocazione: come e dove potrebbero essersi ispirati in Panic, per le loro scelte di hardware, software, catalogo messe in opera per Playdate?

Sulla seconda non ho risposte e sarebbe bello svilupparle assieme.