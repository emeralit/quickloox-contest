---
title: "E ora tutti insieme"
date: 2019-06-22
comments: true
tags: [Above, Avalon, Gruber, Apple]
---
Ha [scritto John Gruber](https://daringfireball.net/linked/2019/06/08/thompson-wwdc) a proposito dell’ultima WWDC:

>La mia analisi di alto livello è che questo è il primo anno nel quale sembra che Apple sia riuscita a spingere un passo avanti tutte le proprie piattaforme.

E poi [Above Avalon](https://www.aboveavalon.com/notes/2019/6/19/apples-product-strategy-is-changing):

>Apple ha saputo fare avanzare tutte le proprie categorie di prodotto nello stesso momento.

Come dire che se arriva qualcuno a commentare di Apple che pensa solo a iPhone e trascura Mac o amenità del genere, già è normalmente bizzarro, ma questo è proprio l’anno sbagliato.
