---
title: "I Like PotatoP"
date: 2023-03-10T11:15:53+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [Hackaday, PotatoP, uLisp, Lisp, McCarthy, John McCarthy]
---
Dice l’anziano-dentro, *non sono più i tempi di una volta, quando con i computer si poteva smanettare*.

Ecco che su *Hackaday* trovi uno smanettatore impegnato nel progetto che ha chiamato [PotatoP](https://hackaday.io/project/184340-potatop). In inglese, *potato* può essere un dispregiativo per un apparecchio poco prestante o sottoequipaggiato.

Si tratta di un portatile completamente autocostruito, in via di completamento, con schermo in bianco e nero da cinquantatré caratteri per riga, per il quale l’autore ha anche scritto un editor di testo elementare, che fa pochissime cose ma tutto l’essenziale e, sorprendentemente, quanto gli basta per programmare in modo soddisfacente.

L’autonomia arriva fino a *due anni* e il progetto definitivo prevede l’autoalimentazione a energia solare.

Come è possibile che un programmatore possa lavorare nel 2023 con un ambiente a cinquantatré caratteri per riga in bianco e nero e un editor di testo messo insieme alla buona? Semplice: l’ambiente software è basato su [uLisp](http://www.ulisp.com).

Perché all’anziano-dentro pareva la fine del mondo il Basic e smanettava senza avere letto niente. Come abbiamo fatto tutti. Ma poi nel tempo almeno qualcuno, come dimostra PotatoP, ha letto dell’esistenza di [John McCarthy](http://jmc.stanford.edu) e ha capito delle cose in più.

<iframe width="560" height="315" src="https://www.youtube.com/embed/G2wkO0DhpEY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>