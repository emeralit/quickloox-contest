---
title: "Du is meglio che uan"
date: 2014-11-15
comments: true
tags: [AnandTech, iPadAir2, GPU, A8X, iPadAir]
---
I computer sono tutti uguali nell’occhio di chi vuole giustificare un acquisto di bassa qualità e figuriamoci le tavolette.

Per tutti gli altri c’è l’[analisi di AnandTech sulla Graphics Processing Unit (GPU) di iPad Air 2](http://www.anandtech.com/show/8716/apple-a8xs-gpu-gxa6850-even-better-than-i-thought).<!--more-->

La lettura è impegnativa ma non proibitiva tecnicamente e si riesce a cogliere il succo: nel realizzare il processore A8X di iPad Air 2, Apple ha applicato notevole personalizzazione al chip di partenza, arrivando di fatto all’equivalente di avere due schede grafiche di uguale potenza sovrapposte.

Qualcosa che non esiste così com’è presso i fornitori, nessun’altra tavoletta può sfoggiare e guarda caso eroga prestazioni grafiche vastamente superiori rispetto al già notevole iPad Air. Non solo:

>Apple è andata molto vicina a inserire in una tavoletta un sottosistema video equivalente a quello di un computer desktop a basso costo e forse può altrettanto sorprendentemente sostenere questo livello di prestazioni per ore.

La seconda parte del capoverso è significativa. A tavoletta fredda sono tutti bravi, ma quando arriva il surriscaldamento le prestazioni calano e non poco. Su altri prodotti.

La morale: iPad Air 2 mostra che Apple interviene in prima persona nel progetto dei *chip*, crea processori configurati come nient’altro presente sul mercato e – aggiunge AnandTech – lascia margini di crescita importanti nel progetto, segno di grandi ambizioni oggi (per esempio per i giochi, sempre più affamati di prestazioni video) e di progettazione a lungo termine che darà ulteriori frutti più avanti.

Certo, quando ti chiedono aggressivamente perché spendere *tutti quei soldi* per un iPad Air 2, una risposta di questo tipo vale se davanti c’è qualcuno in grado di capire la differenza. Normalmente c’è solo uno che vuole spendere poco non perché debba, ma per abito mentale.