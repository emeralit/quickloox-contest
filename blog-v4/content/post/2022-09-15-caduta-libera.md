---
title: "Caduta libera"
date: 2022-09-15T13:05:28+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Apple Watch, Sasha]
---
Ci sono gli orologi da polso che riconoscono le cadute (a brevissimo anche le collisioni in auto) e forniscono un servizio di chiamata di emergenza.

Poi ci sono gli altri, che a leggere le istruzioni parrebbero avere lo schermo dedicato agli accidenti e uno può cadere come gli pare senza il disturbo di dover anche smentire la notizia.

(Grazie **Sasha**!).

![Ouch screen al posto di Touch screen](/images/ouch-screen.jpg "Un amico commenta: Ahi lo smartwatch nuovo…")