---
title: "Lezioni italiane parte prima"
date: 2021-11-11T00:50:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [No Rocket Science, Nrs, AirTable, Duolingo, Ecamm Live, OBS Studio] 
---
Ho [scritto una volta](https://nrs.substack.com/p/il-fine-giustifica-i-mezzi) per *No Rocket Science*, ma avrei adorato essere l’autore della puntata [Imparo, imparo, sì che imparo](https://nrs.substack.com/p/imparo-imparo-si-che-imparo).

Chiunque l’abbia scritta, ha iniziato o va avanti a destreggiarsi con [AirTable](https://airtable.com), [Ecamm Live](https://www.ecamm.com) (*L’[OBS Studio](https://obsproject.com) dei ricchi*) e [Duolingo](https://www.duolingo.com).

Che mestiere fa? Non lo so (non so chi sia, l’autore, per quanto possa avanzare alcune ipotesi). E *non conta*. Chi mai potrebbe avere studiato AirTable a scuola, o avere frequentato un master di Ecamm Live? In che curriculum potrebbero mai essere stati visti?

Eppure hanno un grande senso. Su Nrs l’autore scrive *Un database non è un foglio di calcolo, lo sto (ri)scoprendo a mie spese, ma mi permette di tenere d'occhio più aspetti della mia vita professionale in una maniera nuova e diversa e - devo dire - mi sto anche divertendo.*

Un database può servire in qualsiasi situazione. Ancora di più può servire *saper pensare* secondo una logica da database. Lo stesso vale per la registrazione video e non parliamo nemmeno di una seconda lingua, che oggi non si può neanche chiamare seconda: dovrebbe essere gemella, pena perdere occasioni di carriera se sei giovane del tutto e mille cose interessanti se l’anagrafe ha smesso di corrispondere.

>Imparare a usare due software non è esattamente la stessa cosa che buttarsi su una lingua nuova, o mettersi sotto con la fisica aerospaziale. Eh, lo so. Ma allarga sempre, un pochino, almeno un millimetro, gli orizzonti, e fa sempre bene.

Ecco, appunto. Un’occhiata a Nrs, *by the way*, è un’altra cosa che per molti sarà nuova e per qualcuno diventerà un’abitudine. Da provare.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*