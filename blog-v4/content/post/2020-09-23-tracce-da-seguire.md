---
title: "Tracce da seguire"
date: 2020-09-23
comments: true
tags: [iPhone, Nyt, Cook]
---
Serve un progetto di ricerca per il quadrimestre? Suggerisco ampiamente traduzione, discussione, esplorazione pratica delle maestose rivelazioni del New York Times sul [tracciamento dei cittadini americani](https://www.nytimes.com/interactive/2019/12/19/opinion/location-tracking-cell-phone.html) (e chiaramente del mondo) che avviene attraverso i loro computer da tasca.

Maestose come certe onde da surf, che a un certo punto si rovesciano a terra. La sezione *Privacy Project* del quotidiano ha messo le mani su un file di cinquanta miliardi di geoposizionamenti di cellulari… uno degli innumerevoli file simili prodotti dall’attività di tracciamento.

Attraverso l’analisi dei dati, i giornalisti del *Times* – e non un team di esperti del Massachusetts Institute of Technology, o di Stanford – hanno potuto ricostruire movimenti e profili di persone a caso oppure selezionate, con poco sforzo. Hanno seguito i movimenti del Presidente tramite il telefono di un addetto alla sicurezza (a sua volta identificato e tracciato). Hanno individuato un dipendente del Pentagono che riceve cure psichiatriche, cittadini con una passione regolare e intensa per gli alberghi a ore, industriali, frequentatori di ville di divi a Beverly Hills, realmente qualsiasi cosa abbiano voluto.

Per ragazzi della giusta età per capire sarebbe illuminante imparare che cosa succede, come, perché, con che conseguenze, che cosa si può fare, quali obiettivi vengono oggi perseguiti. Scommetto che vedremmo meno TikTok e più consapevolezza.

Quanto agli adulti, già scritto, viene solo da ringraziare che esista una Apple con il coraggio di sfidare questa industria con funzioni di tutela della privacy sempre più stringenti.

Già scritto, ma adesso ho anche letto e consiglio molto di farlo. Che la divinità preferita di ciascuno ci conservi Tim Cook al posto di comando e che escano iPhone sempre più capaci di lasciare decidere a noi se, quanto, quando e come ci faccia comodo essere tracciati oppure no.