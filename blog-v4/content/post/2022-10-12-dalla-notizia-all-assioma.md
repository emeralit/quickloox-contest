---
title: "Dalla notizia all’assioma"
date: 2022-10-12T01:55:08+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Gartner, Idc, Canalys, Mac]
---
Trovo meraviglioso che, secondo IDC, Apple abbia venduto il quaranta percento in più di Mac in estate rispetto alla primavera, mentre Gartner dice che le stesse vendite sono diminuite del sedici percento e Canalys, per non fare torto a nessuno, propende per una crescita dell’uno virgola sette percento.

Detta così si vede poco; in numeri assoluti, invece, la stima dei Mac venduti questo trimestre è rispettivamente di dieci milioni, cinque milioni e ottocentomila, otto milioni.

Differenze di questa entità suggeriscono che almeno due di queste stime siano non sbagliate, ma clamorosamente sbagliate, a livello di incompetenza sostanziale.

*MacRumors* ha trovato il titolo perfetto per descrivere la situazione: [Gli analisti non hanno idea di quanti Mac siano stati venduti nell’ultimo trimestre](https://www.macrumors.com/2022/10/11/apple-mac-shipments-analyst-estimates-q3/).

Meglio, *quasi* perfetto; il riferimento al trimestre è superfluo.

*Gli analisti non hanno idea di quanti Mac venda Apple*. Così è da epigrafe su monumento equestre. L’unico problema è che, da notizia, diventa assioma. Non abbiamo neanche bisogno di dircelo, sappiamo che è ovvio e che è così sempre.

In tempi di *fake news*, ecco una verità solida e stabile.