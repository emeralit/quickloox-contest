---
title: "Cercasi breakthrough"
date: 2021-12-28T03:57:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, Magic Trackpad] 
---
Ho un Mac che non vuole vedere una (vecchia) tastiera Bluetooth Apple. O forse viceversa.

Esiste qualche modo per forzare un reset della tastiera più drastico che levargli le pile per qualche minuto?

Lo stack Bluetooth in teoria è a posto e dovrebbe esserci compatibilità. Tutto è stato spento, riacceso, rispento, prima uno, poi l’altro, poi l’opposto e così via. Pile cambiate e nuove.

Non ho (ancora) un’altra tastiera per fare un test, mentre la tastiera funziona bene presso un altro Mac. Il Mac vede regolarmente Magic Trackpad.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per Qui
ckLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._