---
title: "Bugie, grandi bugie e sbadigli"
date: 2013-05-23
comments: true
tags: [iPad, Script]
---
Microsoft vuole tanto diventare il secondo venditore di computer a tavoletta dopo Apple. Così ha pubblicato una <a href="http://windows.microsoft.com/en-us/windows-8/compare#t1=asus-vivotab-smart">tabella comparativa</a> di iPad con vari modelli di tavoletta Windows 8. Il modello preimpostato per comparire è VivoTab Smart di Asus, che già con un nome così parte per forza svantaggiato.<!--more-->

Le due foto iniziali sembrano un po’ fuori scala, ma pazienza.

Spessore: differenza minima. Sbadiglio. Batteria: differenza minima. Sbadiglio. Peso: differenza minima. Sbadiglio.

Dimensione schermo: ehi, ma quanto è più grande Windows 8!

<a href="http://www.curi.us/1571-lying-microsoft-advertising">Bugia</a>. La diagonale Asus è maggiore, però le dimensioni sono diverse: 7,76 x 5,82 pollici iPad, 8,8 x 4,95 pollici Asus. Che ha un’area *minore del 3,55 percento*. Sbadiglio. Bugia, però.

Non sto a tediare sul resto (cloud superiore? No, perché un iPad può avere iCloud e SkyDrive insieme). Alla fine, tra gli sbadigli, si arriva al prezzo e sempre lì finisce: l’unico motivo per scegliere qualcosa con l’odore di Windows è che costa meno. Vale meno.

Stranamente manca un accenno alla risoluzione dello schermo, Retina su iPad e invece no sugli altri. Oppure al numero di applicazioni disponibili. O di accessori. O alle prestazioni.

Se serviva una testimonianza di quanto siano diventate inutili queste comparazioni nel XXI secolo, eccola servita pronta da Microsoft. Sbadigli, bugie e tanti alibi per dire senza dirlo *se per caso non vuoi comprare un iPad, per favore non prendere Android*.