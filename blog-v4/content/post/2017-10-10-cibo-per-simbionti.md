---
title: "Cibo per simbionti"
date: 2017-10-10
comments: true
tags: [Microsoft, Windows, mobile]
---
Purtroppo è necessario occuparsi di Microsoft in questi giorni; succedono [l’abbandono del settore musicale](https://macintelligence.org/posts/2017-10-08-musica-maestri/) e [quello dei computer da tasca](https://arstechnica.com/gadgets/2017/10/windows-phone-is-now-officially-dead-a-sad-tale-of-what-might-have-been/), solo che nessuno spiega il perché.

Che è questo (grazie **Mastro**): Microsoft prospera perché [i difetti intrinseci del suo ecosistema generano un indotto sconfinato](https://hackernoon.com/the-microsoft-hustle-2-0-9de19a277e52) e permettono a un insieme immenso di aziende simbionti di vivere sistemando i problemi causati dall’ecosistema Microsoft.

(Il che ha meno relazione di quanto sembra con la qualità del software. Prima di commentare, prima di commentare *qualsiasi cosa*, è obbligatorio leggere e comprendere nella sua totalità l’articolo linkato e pure l’articolo precedente a quello).

Tutto qui. Non puoi vendere musica da un ecosistema difettoso e vincere la concorrenza che invece lavora meglio.

Non puoi neanche giocare al difetto sui computer da tasca. Sono una cosa troppo personale, troppo intima. E la concorrenza, per quanto perfettibile, si rinnova continuamente, migliora costantemente. Se vuoi affermarti nei computer da tasca, devi fare le cose molto bene.

Difatti le recensioni su Windows Phone sono sempre state generalmente positive; Microsoft era cosciente di non poter ricorrere alle solite armi, in quel mercato. E però, senza le solite armi, perde.

Se Microsoft lavora bene, non ce la fa. Riesce solo dove lavora intenzionalmente male per lasciare spazio ai simbionti. Pensarci bene. I miei dischi sono *Microsoft free*.