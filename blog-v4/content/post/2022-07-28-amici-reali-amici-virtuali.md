---
title: "Amici reali, amici virtuali"
date: 2022-07-28T01:38:22+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [Roberto Marin, Marin, Roberto, Filippo, Strozzi, Filippo Strozzi, A2, podcast, macOS, Utm, VirtualBuddy, Virtualization]
---
Registrare un podcast assieme a **Roberto** e **Filippo**, come abbiamo [rifatto](https://macintelligence.org/posts/2022-05-31-eravamo-tre-amici-alla-touch-bar/) [pochi giorni fa](https://macintelligence.org/posts/2022-07-26-un-bel-cast-e-un-bel-podcast/), è un divertimento unico. Sono amici veri per la mia definizione personale.

È anche un bello stress, perché loro sono davvero esperti e fare finta di sapere almeno al loro livello è veramente impegnativo.

A questo riguardo, *Ars Technica* mi ha fatto un bello sgarbo, nel presentare con imperdonabili giorni di ritardo sulla registrazione [un bell’articolo sulle app che semplificano oltre ogni dire la creazione di macchine virtuali macOS sopra macOS](https://arstechnica.com/gadgets/2022/07/how-to-use-free-virtualization-apps-to-safely-test-the-macos-ventura-betas/), dove lo hardware sottostante sia Apple Silicon.

È materiale che per ora non tocco perché lavoro dall’ambito balneare con risorse ridotte di banda, ma che testerò immediatamente non appena ritorno almeno su una Adsl. Troppo promettente. Nondimeno avrei potuto fare bella figura con loro, che sul *framework* Virtualization di macOS erano preparati alla grande.

E invece. Devo pensare che cosa studiare ad agosto. Che cosa studio?