---
title: "La guerra strisciante"
date: 2018-08-02
comments: true
tags: [Apple, Bloomberg]
---
La migliore celebrazione del trilione di valutazione di mercato di Apple è [quella di Bloomberg](https://www.bloomberg.com/features/2018-apple-trillion-dollar-world/).

Il traguardo, per quanto [non sia veramente la prima volta](http://www.macintelligence.org/blog/2018/08/01/tempi-e-spazi/), è di enorme portata simbolica e psicologica. Apple vale come il prodotto interno lordo dell’Indonesia e domani lo starnuto di un analista potrebbe certo cambiare la classifica, ma non l’impatto della notizia.

La parte importante è capire che cosa succederà da qui in avanti. Riguarda il pianeta, non Apple.

Il mio parere è che questo è solo l’inizio. Le quotazioni possono variare, ma la sostanza è che altre aziende arriveranno al miliardo. Alphabet, Amazon, Microsoft.

Si tratta di nazioni trasversali, popolate un po’ meno della Cina, che muovono denaro e stabiliscono regole in maniera incontrollabile da parte degli Stati nazionali.

Ci sarà la guerra. Non militare, ovvio. Ma gli Stati nazionali tenteranno di tutto pur di rimettere sotto il loro controllo entità che di fatto ne esulano.

Sono già cominciate le prime scaramucce. Storie di tasse, multe, indagini antitrust… non sono più episodi fini a se stessi, ma capitoli di un confronto oggi strisciante che diventerà sempre più evidente e serrato.

A un certo punto potremmo anche ritrovarci a dover decidere da che parte stare. Ed è una riflessione intrigante.