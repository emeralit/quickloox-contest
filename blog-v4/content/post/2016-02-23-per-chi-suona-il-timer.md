---
title: "Per chi suona il timer"
date: 2016-02-23
comments: true
tags: [watch]
---
watch certamente non vende milioni e milioni di esemplari ogni mese come accade per iPhone. Peraltro, dati del trimestre di Natale, [tre su cinque](https://www.strategyanalytics.com/strategy-analytics/news/strategy-analytics-press-releases/strategy-analytics-press-release/2016/02/18/global-smartwatch-shipments-overtake-swiss-watch-shipments-in-q4-2015#.VsurAcfTyBI) computer da polso venduti (esattamente il 63 percento) sarebbero watch, se Strategy Analytics ha lavorato bene e pubblica dati attendibili.<!--more-->

Ancora più interessante è che a Natale 2014 si sono venduti 1,9 milioni di computer da polso e 8,3 milioni di orologi svizzeri. A Natale 2015, i primi sono passati a 8,1 milioni e i secondi sono scesi a 7,9 milioni.

È evidente che si è aperto un fronte di acquirenti nuovo (o rinnovato, di tanti che sono tornati a pensare di avere un apparecchio al polso). E l’aritmetica vuole che si vendano più computer da polso che orologi svizzeri.

D’obbligo ricordare i [problemi inediti di esportazione degli orologi svizzeri](https://macintelligence.org/posts/2015-11-20-a-che-serve-un-apple-watch/) e le [reazioni snob e infastidite dei produttori](https://macintelligence.org/posts/2015-09-26-condannati-a-ripetere/) all’ingresso di Apple in nuovi mercati tra i quali gli orologi.

Un po’ [alla Hemingway](https://it.wikipedia.org/wiki/Per_chi_suona_la_campana) viene da dire che forse, per qualcuno poco attento alle novità nel proprio settore, sta ticchettando il conto alla rovescia.