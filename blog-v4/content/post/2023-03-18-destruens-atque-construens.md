---
title: "Destruens atque construens"
date: 2023-03-18T13:02:58+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [ChatGPT, Accomazzi, Luca Accomazzi, Misterakko, Petey, Siri, Shortcuts, Comandi rapidi, Copilot, Romero, Medioman, ChatGPT Siri, George Romero]
---
Leggo che [Microsoft 365 ha ora a disposizione un Copilot](https://blogs.microsoft.com/blog/2023/03/16/introducing-microsoft-365-copilot-your-copilot-for-work/) che aiuterà a redigere documenti.

Una volta Microsoft si proponeva l’obiettivo di mettere [un computer su ogni scrivania](https://www.businessinsider.com/microsoft-ceo-satya-nadella-bothered-by-bill-gates-mission-2017-2).

Ora si sono aggiornati: l’obiettivo nuovo è mettere su ogni scrivania un utente. Uno solo, sempre quello.

Le dimensioni della catastrofe sono inimmaginabili. Una catastrofe silente e strisciante. Nel giro di pochi anni la comunicazione aziendale e in generale professionale ricorderà un [film di George Romero](https://www.imdb.com/name/nm0001681/).

È tutto da capire se a un certo punto l’inevitabile presa di coscienza (se tutti i comunicati sono uguali, nessuno più si differenzia e nessuno più legge) si tradurrà in azioni concrete o se invece subentrerà la rassegnazione e gli uffici stampa, abitati ormai da schiacciabottoni pagati pro forma, semplicemente accetteranno di operare come gesto dovuto e ignorare qualsiasi metrica di attenzione, dato che nessuno più ne presterà. Da qui a cinque anni la comunicazione aziendale potrebbe perdere ogni traccia di vita intelligente.

Naturalmente, il luddismo o la chiusura nel guscio non sono la soluzione. ChatGPT non se ne andrà e anzi arriveranno a decine come lui. È già cominciata l’alluvione. Arriverà sempre tra cinque, forse dieci anni il prossimo inverno dell’intelligenza artificiale, come quello degli anni novanta. Ma nessuno cancellerà il sistema, che ha già iniziato a usare se stesso per autoaggiornarsi e creare un modello di contenuti stile [Medioman](https://www.ceraunavolta.org/medioman/), la media della media della media. Se continui a mescolare colori sulla tavolozza, a un certo punto ottieni un marrone indistinto. Miliardi di inconsapevoli, eccitatissimi dal nuovo grande progresso percepito come tale a causa della mancanza di radici e di cultura, contribuiscono già ora al nuovo mondo che sarà marrone.

L’unico modo per stare a galla è saper nuotare. Conoscere come funziona l’acqua, capire tutti i modi in cui può avvantaggiarci nel lavoro. Capire come rispondere in modo efficace quando il collega propone di usare il sistema in modo inappropriato o dannoso. Alla fine, sapere usare il sistema meglio degli altri. Purtroppo qui non si può *fare a meno di Word* essendoci di mezzo, anzi dietro, modelli di linguaggio, database, oggetti in cloud. Bisogna avere in mano il Word della situazione, meglio degli altri, o comanderanno gli ignoranti, gli aridi e gli avidi.

Ecco perché passare alla *pars construens*. Ecco perché consigliare l’acquisto di [Petey],(https://apps.apple.com/it/app/petey-ai-assistant/id6446047813?l=it) per esempio, che mette un’interfaccia di dialogo a disposizione di watch. Ecco perché segnalare il lavoro di [Misterakko](https://www.accomazzi.net/soluzioni-internet/chatgpt-commerce-entra-futuro-oggi-0002825.html), che [ha reso in italiano il Comando rapido per accedere al sistema a partire da Siri](https://apple.quora.com/Ehi-Siri-fammi-parlare-con-ChatGPT).

Alla fine, la soluzione sta sempre nella conoscenza. Alla fine, ci conviene approfittare degli strumenti superiori che abbiamo in mano. E anche questa volta maturare un vantaggio competitivo.