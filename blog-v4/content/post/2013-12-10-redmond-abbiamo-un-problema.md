---
title: "Redmond, abbiamo un problema"
date: 2013-12-10
comments: true
tags: [Microsoft]
---
Microsoft è l’azienda che fa gli strumenti per lavorare.<!--more-->

>se i giovani intervistati avessero a disposizione 400 euro da spendere a loro piacimento, quasi il 40% di loro sceglierebbe la tecnologia – acquistando un tablet o uno smartphone – preferendola rispetto ad un weekend fuori porta con gli amici (15%) e allo shopping (11%).

Lo standard di fatto per i professionisti e per l’ufficio.

>Il 23%, infatti, preferirebbe vivere fino a 60 anni con la possibilità di accedere a Internet piuttosto che fino a 80 senza poterlo fare mai, rinunciando quindi a 20 anni della propria vita pur di essere online.

Un’azienda seria che ispira fiducia ai reparti informatici delle imprese.

>Un quarto degli intervistati preferirebbe correre per strada nudo piuttosto che rendere pubblica la propria cronologia di navigazione web passata e futura.

Mica come quei *parvenu* di Apple che fanno cose per figli di papà sfaccendati e pieni di soldi.

>Le applicazioni rappresentano un’altra fonte di “estremismo”, con il 37% dei ragazzi che preferirebbe ricevere app gratis per tutta la vita piuttosto che cibo gratuito per tutta la vita.

Le citazioni provengono da un comunicato stampa Microsoft frutto di una *ricerca su Msn di 2.800 studenti universitari in sette Paesi europei*. Lascio giudicare alle aziende e ai professionisti il livello, delle domande fatte, di quelli che rispondono, dell’atmosfera promossa su Msn, di come spende il denaro il reparto marketing. Utilizzatore di software per la produttività, correresti per strada nudo oppure renderesti pubblica la tua cronologia web passata e futura?