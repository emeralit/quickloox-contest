---
title: "Non si gioca più"
date: 2017-12-10
comments: true
tags: [Swift, Objective-C, Ibm]
---
Ho accennato [numerose volte](http://www.macintelligence.org/blog/categories/swift/) al linguaggio di programmazione Swift ideato da Apple per sostituire il vecchio Objective-C e ora *open source*, liberamente adottabile anche fuori da un Mac.<!--more-->

Spesso ho anche affermato compiaciuto che Swift si poteva anche collaudare nel browser, grazie alla Sandbox di Ibm.

Ora non potrò più farlo, perché Ibm ha annunciato la [*deprecation* di Swift Sandbox](https://developer.ibm.com/swift/2017/12/07/package-catalog-sandbox-deprecation/) per gennaio 2018.

La ragione è che gli sforzi di sviluppo Swift di Ibm stanno andando a gonfie vele:

>Oggi, Swift su server prospera. Gli eventi di comunità Swift in tutto il mondo crescono con partecipazione da record, mentre abbondano newsletter e corsi online dedicati a Swift. […] Swift su Linux è pronto! Sono disponibili sistemi di produzione che sfruttano la tecnologia cloud, tra i quali Swift su Docker, Ibm Cloud e Vapor Cloud.

Con la forza trainante che ha raggiunto Swift, continua il pezzo, avere l’anteprima di Swift nel browser non è più un bisogno o una opportunità. Si può fare meglio con gli strumenti che (ora) sono ampiamente disponibili.

Esiste peraltro [un’altra sandbox per Swift nel browser](https://www.weheartswift.com/swift-sandbox/). Ma non è prestigiosa come quella di Ibm.

Dove con Swift fanno talmente sul serio che non pensano neanche più a giocarci.