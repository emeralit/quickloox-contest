---
title: "Internet contro l’Anagrafe"
date: 2021-05-25T17:31:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Internet, Comune, Anagrafe] 
---
Il 24 maggio sera mi sono informato sul sito del mio Comune per fare le carte di identità alle figliole; qualcosa che ci farebbe piacere avere, tipo, entro il 15 di luglio, per questioni di vicinanza a una frontiera.

C’è da prendere l’appuntamento. Un appuntamento per ciascuna carta, senza opzioni per le famiglie con un numero di figlie pari.

Il sito ha il link per prenotare l’appuntamento. Lo uso. 13 agosto. Un po’ tardi. Ci deve essere un traffico di carte di identità mostruoso.

Il sito ha *anche* un link al sistema nazionale delle prenotazioni. Lo uso. 25 maggio ore 10, 25 maggio ore 11.

Il bello è che il sistema nazionale delle prenotazioni ti manda *al Comune di residenza*. Lo stesso Comune che di suo voleva farmi aspettare undici settimane.

Oggi, compilando la domanda un appuntamento per volta e una figlia per volta, abbiamo capito il perché di tutto. Una storia come tante altre di gente sfaccendata e stipendiata per fare poco più di niente.

Non occorre aggiungere una storia in più. È bello sapere che, nonostante l’impegno di taluni per offrire zero servizi in cambio del privilegio della nullafacenza, a volte Internet si prende la rivincita e trova modo di farti respirare per qualche attimo l’aria di un posto normale.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               