---
title: "Argento vivo"
date: 2015-07-20
comments: true
tags: [Wesnoth, CCG, Argentum, Hearthstone]
---
È noto il mio amore per [Battle for Wesnoth](http://www.wesnoth.org), gioco di strategia a turni ricco interessante e affinato come pochi giochi *open source* arrivano a essere, banale da cominciare e pazzamente difficile da padroneggiare.<!--more-->

La notizia è che alcuni programmatori tra i numerosi che hanno dato una mano a questo progetto si sono lanciati nella creazione di un gioco di carte da collezionare (CCG, Collectible Card Game) con elementi tattici: [Argentum Age](http://argentum-age.com).

La provenienza dal *team* di Battle for Wesnoth si vede già dall’aspetto del sito e il gioco promette. Per ora siamo a una versione *alpha* e quindi peggio che preliminare, ma l’idea di fondo merita assolutamente l’attenzione al progetto e la realizzazione procede bene, come testimonia l’aggiornamento di centonovanta megabyte che si è scaricato non appena lanciati i duecentotrentasette dell’applicazione Mac.

Argentum Age gira su tutti i sistemi da tavolo attuali ed è gratuito, anzi, in cerca di volontari disposti a fornire *feedback* e suggerimenti, se non sforzi di programmazione che contribuiscano alla maturazione del gioco. Lo sviluppo intanto procede e parrebbe in modo serrato.

Se diventasse tra i giochi di carte quello che Wesnoth rappresenta per gli strategici, sarebbe una bella vittoria per il software libero. E lo dice un fedelissimo di [Hearthstone](http://eu.battle.net/hearthstone/it/?-)