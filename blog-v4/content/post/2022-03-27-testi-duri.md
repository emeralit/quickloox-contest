---
title: "Testi duri"
date: 2022-03-27T01:05:03+01:00
draft: false
toc: false
comments: true
categories: [Software, Web]
tags: [Plain Text Sports, Protocol]
---
Ho anticipato – penso – *Protocol* nel [parlare del fenomeno Plain Text Sports](https://macintelligence.org/posts/2022-03-21-contesto-e-passione/), il sito solo testo dedicato agli sport americani.

*Protocol* naturalmente [allarga il discorso](https://www.protocol.com/newsletters/sourcecode/plain-text-sports-internet) e aggiunge un paio di cose che meritano il rilancio.

La prima e più importante è che di siti solo testo ne esistono a bizzeffe e neanche mancano i browser solo testo, nessuna novità su questo. [Plain Text Sports](https://plaintextsports.com/) si distingue però per avere un output semplice, semplicissimo, abbinato a una notevole complessità dietro le quinte. Il lavoro che permette al sito di esistere e continuare ad aggiungere nuovi sport non è affatto banale. Basta poi dare uno sguardo al codice per vedere che nelle pagine web c’è abbondanza di Css e di JavaScript: *testo puro* è diverso da dire *Html base* e può benissimo implicare la sofisticazione. Purché sia interna, nascosta, e che il visitatore possa beneficiare di un lavoro svolto per nascondere la complessità dell’organizzazione delle informazioni.

La seconda è che *Plain Text Sports* non fa guadagnare un centesimo all’autore (il sito è fatto *da una sola persona*, per diletto; riprovare a visitarlo, *please*). Tuttavia, essendo così semplice nella sua concezione, costa come un passatempo: la sua spesa presso Amazon è di circa cinquanta dollari al mese. Gente dedita ai giochi da console o alle serie TV potrebbe totalizzare cifre maggiori.

La lezione è che il futuro di Internet, quello vero, quello che porta con sé anche il progresso oltre alla banale novità, potrebbe essere basato sul testo. Mi fa piacere perché lo dico da sempre a un altro livello, la produzione di documenti, e perché il sogno originale della rete era dare a tutti la possibilità di esprimersi, magari alla pari con una grande azienda, dove avere risorse o avere soldi conta un po’ meno che avere buone idee.

Quando il gioco si fa duro, i duri usano il testo; una persona che sa il fatto suo tecnicamente può creare cose incredibili a costi minimi. Non tutti sanno il fatto proprio naturalmente e questo ci porta al problema dell’analfabetismo tecnologico, che però è tutt’altro tema.

Qui voglio solo aggiungere che, il giorno che tornasse di moda [Internet Relay Chat](https://www.irchelp.org), mi troverebbe pronto e disponibile. Magari per quel momento sarò anche migrato a [emacs](https://www.gnu.org/software/emacs/).