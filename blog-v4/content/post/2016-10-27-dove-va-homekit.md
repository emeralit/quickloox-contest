---
title: "Dove va HomeKit"
date: 2016-10-27
comments: true
tags: [Jobs, Gretzky, IoT, DDoS, HomeKit, Forbes, botnet]
---
Negli ultimi giorni una parte notevole di Internet è stata [messa in ginocchio da attacchi Distributed Denial of Service (DDoS)](http://arstechnica.com/security/2016/10/double-dip-internet-of-things-botnet-attack-felt-across-the-internet/): faccio in modo che il tuo sito riceva talmente tante richieste di accesso che non ce la fa e schianta. Poi tengo il livello di richieste più alto che posso, in modo che il sito rimanga irraggiungibile.<!--more-->

Sono stati battuti i record di potenza e questo grazie a *botnet*, squadre di apparecchi che non sanno di appartenervi e sono pilotati da burattinai del software, costituite da aggeggi della cosiddetta *Internet of Things* (IoT), cioè l’insieme degli oggetti di uso comune che, da questo secolo, sono capaci di entrare in Rete.

Orologi, fotocamere, sensori, elettrodomestici, televisori, lampadine, caffettiere hanno una cosa in comune: siccome è da pochissimo che vanno su Internet e le aziende che li producono non hanno idea della situazione, hanno livelli di sicurezza infimi. I loro acquirenti non se ne rendono conto e, come accadeva con i videoregistratori di un tempo dove nessuno sistemava l’orologio e lo lasciava a lampeggiare, così oggi non si cura di impostare password decenti. E il primo che passa può prendere il controllo dell’apparecchio.

Il risultato è che una squadra di orologi, fotocamere, sensori, elettrodomestici, televisori, lampadine, caffettiere ha messo fuori combattimento una parte importante della rete. Senza saperlo.

Cambio di scena, ora: articolo di Forbes sul [perché solo cinque aziende finora hanno lanciato apparecchi compatibili con HomeKit](http://www.forbes.com/sites/aarontilley/2015/07/21/whats-the-hold-up-for-apples-homekit/#3416ac98322b), l’architettura di Apple per la domotica, quest’ultima una propaggine domestica della Internet of Things.

Spiegazione: Apple pone requisiti fortissimi e futuribili di sicurezza degli apparecchi, con chiavi di cifratura di lunghezza inusitata e [funzioni di cifratura all’avanguardia nel settore](https://cr.yp.to/ecdh.html). I costruttori hanno difficoltà a rispettare i requisiti e nel contempo mantenere le velocità di risposta che ci si aspetta da una serratura o da un rubinetto. La cifratura robusta, infatti, impone calcoli impegnativi che molti apparecchi odierni non riescono a eseguire in tempi ristretti.

Dettaglio: alla fine della presentazione di iPhone nel 2007, Steve Jobs [citò il campione di hockey su ghiaccio Wayne Gretzky](http://sports.yahoo.com/top/blog/canada/post/steve-jobs-secret-inspiration-wayne-gretzky):

>Pattino dove sta andando il dischetto, non dove è stato.

Dovrebbe essere semplice tirare le conclusioni su dove sta andando Apple con HomeKit e perché.