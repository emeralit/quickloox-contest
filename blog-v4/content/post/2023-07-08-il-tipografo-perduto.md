---
title: "Il tipografo perduto"
date: 2023-07-08T12:45:58+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [WordPress, LaTeX, TeX, MetaFont, Knuth, Donald Knuth, Macintosh, LaserWriter]
---
Nel rendermi conto che la modalità visuale di WordPress mostra un allineamento in alto tra una immagine e un testo affiancato che non corrisponde affatto alla resa effettiva dell’Html,chiudo un pomeriggio di frustrazioni e ne inizio un altro, di studio di CSS alla ricerca della proprietà che mi permetterà di ottenere l’effetto che voglio, posto che WordPress mi permetta di usarla. Per esempio, il WordPress su cui mi trovo non accetta l’uso di `flexbox`, che mi tornerebbe utile. Sì, esiste certamente un plugin, ma non lo posso installare. Già, basta andare nel back-end, ma non ho accesso.

Intanto rifletto più utilmente, forse, sulla tipografia. Il digitale ha fatto a pezzi tutta la scienza tipografica cumulata in cinquecento anni, per imporre il minimo comune denominatore: ASCII, font quelli permessi dalla stampante se li permetteva e poi il nulla.

Nel 1984 è iniziata veramente l’inversione di tendenza grazie prima a Macintosh e poi a LaserWriter. All’inizio aveva mi pare otto font e suonava come una ricchezza.

Poi, più niente o quasi… salvo per il genio di Donald Knuth, che ha inventato [TeX](https://ctan.org/tex) e ci ha ridato la potenzialità del controllo tipografico totale. Il progetto dei [CSS](https://www.w3.org/Style/CSS/Overview.en.html) prosegue e cresce, con fatica per le questioni di compatibilità e di litigi nei vari comitati, però cresce.

Intanto però è passata la metà di un secolo. Un decimo di quanto Manuzio e compagnia hanno impiegato per sviluppare l’arte tipografica, ma un’eternità in termini informatici. LaTeX è lì, ma chi può bloggare e pubblicare con facilità in LaTeX? Di fatto un tesoro tecnologico è alla portata solo di una élite, la quale poi comunque non ha mezzi seri per pubblicare regolarmente materiale di alta qualità tipografica.

Con i CSS si può fare tanto. WordPress comanda due terzi dei siti sul pianeta e se ne fotte allegramente dei CSS. Li vuoi usare? Arrangiati. Già è molto se li faccio funzionare.

C’è il plugin, mi dite. No, non c’è il plugin. A nessuno interessa andare oltre testo dall’aspetto sciatto e buttato in riquadri come il pastone nel trogolo. Non c’è da fare soldi, con i CSS, mica la gente ha tempo da perdere.

Abbiamo perso una dimensione fondamentale dell’espressione. Abbiamo tutti i mezzi per invertire la rotta. Abbiamo la passività di chi potrebbe incrinare la dittatura del *good enough*.

A settembre mi batterò per introdurre l’insegnamento della base dei CSS nella scuola media dove prima o poi arriverà mia figlia. Una goccia nell’oceano. Ma in qualche scuola, qualche bambino, grazie a qualche docente intelligente, magari, forse. E un domani potrebbe nascere una startup, un movimento, un meme che si propaga, chi lo sa.

Di tutte le battaglie ideali e astratte, questa è la più inutile di tutte. Ma è una battaglia di civiltà e di istruzione e di cultura. Mi spenderò più che potrò.