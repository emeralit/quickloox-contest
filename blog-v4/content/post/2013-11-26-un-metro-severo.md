---
title: "Un metro severo"
date: 2013-11-26
comments: true
tags: [iOS, iWork, Pages, Numbers, Keynote]
---
Iljitsch van Beijnum ha pubblicato su *Ars Technica* una severa, lunga e centrata recensione della [suite di produttività precedentemente nota come iWork](http://arstechnica.com/apple/2013/11/review-the-productivity-suite-formerly-known-as-iwork/).<!--more-->

A parte il merito del pezzo, ha individuato con precisione lo scopo di Apple nel realizzare la riscrittura di Keynote/Numbers/Pages a prezzo di perdere – in qualche caso momentaneamente – funzioni per strada:

>Ciò che Apple sembrava volere era l’unificazione del formato dei file e dei motori di rappresentazione del contenuto, in modo che versioni diverse delle app potessero lavorare sui file in modo non distruttivo [pienamente leggibile dalla stessa app su un apparecchio diverso o nel browser]. Le versioni iOS e web mostrano la formattazione anche ove manchino dei controlli per modificarla. Apple è stata attenta a lavorare in modo che una versione meno capace della stessa app non rovini la formattazione.

Intanto è arrivato il primo aggiornamento, che non conta perché tempi alla mano era palesemente in lavorazione da tempo e non può tenere conto di alcun *feedback*. Che ho lasciato presso Apple per la seconda volta rispetto al conteggio caratteri di Pages, ancora orfano degli spazi e di conseguenza contronatura.