---
title: "Bufale e mozzarelle"
date: 2017-06-30
comments: true
tags: [Corriere, Quintarelli, iSpazio, AppleInsider, iPhone]
---
Mi rifiuto di entrare nel merito del [disegno di legge 2484](http://www.senato.it/japp/bgt/showdoc/17/DDLPRES/983230/index.html?part=ddlpres_ddlpres1-articolato_articolato1-articolo_articolo4), quello presentato dai più noti media italiani come *anti-Apple* o quello [che potrebbe mettere al bando iPhone in Italia](http://www.corriere.it/tecnologia/17_giugno_23/disegno-legge-che-potrebbe-mettere-bando-l-iphone-9c3c0f24-57ef-11e7-abb9-de301c7bc284.shtml). Basta leggerlo. Anche se informarsi è fuori moda.

Noto invece lo stato veramente dimesso della nostra informazione. Poche ore dopo l’intemerata del *Corriere* (solo come esempio, una rassegna stampa sarebbe impietosa), *Apple Insider* [spiega come stanno le cose](http://appleinsider.com/articles/17/06/23/proposed-italian-consumer-protection-law-unlikely-to-have-any-effect-on-apples-iphone-sales). *Apple Insider*! Dall’America. Con l’aiuto di Google Traduttore e un avvocato madrelingua italiano.

Stefano Quintarelli, l’unico firmatario dei venticinque ad avere la statura tecnica e culturale per presentarlo, ha spiegato che non c’è alcuna norma anti-Apple. Bontà sua, ha anche evitato di sottolineare che è stato presentato un emendamento che vieterebbe la vendita di apparecchi con *crapware* preinstallato (come dire, al bando tutto tranne iPhone).

Lo ha spiegato a [iSpazio](http://www.ispazio.net/1808504/facciamo-chiarezza-liphone-non-verra-bandito-dallitalia-stefano-quintarelli-definisce-meglio-la-questione-su-ispazio).

Infine ha dovuto scrivere un [post dedicato](http://blog.quintarelli.it/2017/06/ecco-perche-e-una-fake-news-la-legge-contro-liphone-italia.html). Lo si legga per favore, lo si diffonda e, prima di farlo, si legga la rassegna stampa creata con una ricerca in Google: *Apple vietato Italia*.

I veri giornalisti si trovano oramai ovunque, tranne che nei giornali. Dove sono rimaste le mozzarelle: il loro motto è *sono più buone quelle di bufala*.