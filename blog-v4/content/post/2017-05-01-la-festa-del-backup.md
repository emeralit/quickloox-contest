---
title: "La festa del backup"
date: 2017-05-01
comments: true
tags: [Degoo]
---
Apparentemente Degoo offre uno [sconto del 95 percento](https://deals.appleworld.today/sales/degoo-ultimate-2tb-backup-plan-lifetime-subscription?utm_source=appleworld.today&utm_medium=referral&utm_campaign=degoo-ultimate-2tb-backup-plan-lifetime-subscription_050117&utm_term=scsf-230913) su due terabyte di spazio backup per la vita.<!--more-->

Bisogna sempre ricordarsi che questa offerte *lifetime* hanno la vita di chi le offre. Ma, anche se Degoo durasse due anni e poi sparisse, mi pare un’ottima offerta.