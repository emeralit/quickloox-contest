---
title: "2019 e non sentirli"
date: 2018-04-11
comments: true
tags: [Panzarino, Mac, Pro, iMac, TechCrunch]
---
La parte interessante dell’[articolo di Matthew Panzarino su TechCrunch](https://techcrunch.com/2018/04/05/apples-2019-imac-pro-will-be-shaped-by-workflows/) è meno l’arrivo del prossimo Mac Pro nel 2019, che il *come* arriverà.<!--more-->

È noto l’atteggiamento del professionista: *Apple non ascolta i professionisti* o se ne è dimenticata, o li ignora, o li considera di poco valore rispetto alla massa bovina (secondo il professionista) che compra iPhone.

Qualcosa sta cambiando, racconta Panzarino:

>[Il gruppo di lavoro] vuole che i propri architetti hardware e software siedano fianco a fianco con clienti veri, per comprendere il loro reale flusso di lavoro e vedere in tempo reale come lavorano. Il problema è che, se tipicamente i clienti sono molto collaborativi quando Apple li chiama, non è sempre facile sapere come lavorano perché spesso sono impegnati in progetti riservati. John Powell, per esempio, è utente Logic da molto tempo e sta lavorando sul nuovo film di Star Wars dedicato a Han Solo. Facile immaginare che portare in Apple una colonna sonora ultrasegreta per lavorarci in presenza di ingegneri Apple possa diventare un punto di attrito.

>Così Apple ha deciso di fare un passo oltre e ha cominciato ad assumere direttamente i creativi, a volte a contratto, a volte a tempo indeterminato. Si tratta di artisti e tecnici pluripremiati, pagati per portare la loro esperienza con progetti veri […] e mettere alla prova hardware e software, per individuare le frizioni e gli ostacoli che possono frustrare o demotivare un utente pro.

Vedremo che Mac Pro esce da questo processo (Phil Schiller parla di *schermo con sistema modulare*). Intanto sappiamo per certo che l’atteggiamento critico del professionista cambierà. Non sentiremo più *Apple non ascolta i professionisti*, quanto invece *Apple non ascolta me*.

Che è già qualcosa.