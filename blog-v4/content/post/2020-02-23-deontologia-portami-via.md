---
title: "Deontologia portami via"
date: 2020-02-23
comments: true
tags: [Jobs, Parc, Xerox, Tesler, Post]
---
Dopo [il mio pezzo](https://macintelligence.org/posts/2020-02-21-se-questa-e-una-rapina/) sul trattamento della scomparsa di Larry Tesler da parte de *Il Post*, la redazione del suddetto [ha aggiunto al suo articolo una frase](https://www.ilpost.it/2020/02/20/larry-tesler-inventore-copia-incolla-morto/):

>Le visite di Jobs erano state comunque concordate anche in seguito a un investimento da parte di Xerox in Apple.

Me ne sono accorto grazie a una dritta di **manuelmagic**, che ringrazio. La modifica è riuscita a sfuggire alla cache di Google, ma avevo tenuta aperta la finestra dell’articolo in versione originale, che ora ho salvato in Pdf e come schermata del paragrafo in questione.

Ovviamente non ho alcun merito; chissà quanti hanno segnalato al Post che quel paragrafo richiedeva un riequilibrio tra cerchio e botte. Speriamo tanti.

Se si modifica un articolo in modo significativo, è buona coscienza farlo sapere, magari aggiungendo in fondo all’articolo una piccola nota sulle modifiche effettuate. È una cosa di routine sulle più grandi testate americane, di cui si accorge chiunque legga in giro, non serve avere frequentato una scuola di giornalismo. Al Post tuttavia hanno glissato.

Il nuovo paragrafo rende evidentemente fango puro le affermazioni di chi parlava della visita di Steve Jobs al Palo Alto Research Center Xerox come della *più grande rapina informatica della storia*, ma nessuno ha ritenuto di cancellare quella parte o modificarla.

Il nuovo paragrafo contiene un link a una pagina che corrobora la nozione dell’accordo tra Xerox e Apple, mentre manca un link a supporto della storia della rapina. Secondo me – pura speculazione senza prove – Al Post tengono aperta la pessima biografia di Jobs scritta da Walter Isaacson, [dove si può leggere](https://books.google.it/books?id=26ev_abfrU8C&pg=PT94&lpg=PT94&dq=the+greatest+computer+robbery+ever+jobs+parc&source=bl&ots=NJE5N7sSOd&sig=ACfU3U0CdIT-BcFchbVPDSH9uSf6nXEp7g&hl=en&sa=X&ved=2ahUKEwjY5cmHt-bnAhUS3aQKHW-GCCcQ6AEwAHoECAQQAQ#v=onepage&q=the%20greatest%20computer%20robbery%20ever%20jobs%20parc&f=false):

>Il raid Apple presso il Parc Xerox viene talvolta descritto come una delle più grandi rapine nella storia dell’informatica.

Chi lo avrebbe scritto, o detto? Isaacson non lo esplicita. Il Post manco cita Isaacson (o chi per esso). Forse è ora di parlare di leggenda urbana?

Torniamo al nuovo paragrafo e al nuovo link: al Post non lo hanno letto. Altrimenti avrebbero corretto il testo preesistente dove si parla delle due visite di Steve Jobs, quando invece furono due visite di Apple, con Jobs presente solamente la seconda volta.

E avrebbero dovuto tralasciare _anche_: le visite di Apple a Xerox furono direttamente connesse all’investimento di Xerox in Apple e non c’erano altri motivi.

Insomma, sembra così difficile usare correttezza giornalistica nel 2020. Almeno fosse una spietata disamina del business di Apple. Invece è un necrologio e si approfitta pure di quello per fare, eufemismo, scarsa informazione.