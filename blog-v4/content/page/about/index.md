---
title: "Bio"
date: "2022-02-01"
url: "/about/"
---

{{< imgcap src="nerviano.jpg" title="Lucio Bragagnolo" >}} 

Lucio Bragagnolo è giornalista, divulgatore, produttore di contenuti, consulente in comunicazione e media. Si occupa di Apple e del benessere digitale per persone e aziende, con entusiasmo crescente. Nel tempo libero gioca di ruolo, legge, balbetta Lisp e pratica sport di squadra. È sposato felicemente con Stefania e padre apprendista di Lidia e Nive.

Contatti:

- Apple Messaggi: lux@mac.com
- Telegram: Lucio Bragagnolo
- Signal: lux.19
- Element di Matrix: Lucio Bragagnolo
- Mastodon: @loox@mastodon.uno
- Twitter/X: @loox
