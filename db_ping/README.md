### Premessa

Mancano dei passi, pochi, per avere l'opera quasi omnia dell'attività di blogger di Lucio.
Gli ultimi anni ci sono tutti, i post di Script ci sono, ma manca il periodo Ping!.

La buona notizia: Lucio ha l'archivio completo. La cattiva notizia: bisogna rendere i file compatibili con hugo. Per sapere come sono al momento archiviati i file leggete (qui)[https://macintelligence.org/post/2022-04-17-lavori-in-scorso/].

Il lavoro da fare è delineato (qui)[https://muloblog2.netlify.app/22/03/30/ping-reloaded/].

Una delle difficoltà risiede nel recuperare le date originali di tutti i post, ma il problema è risolto con l'uso di `stat` sull'archivio di Lucio. Il risultato è nel file Ping.csv.

E adesso la sfida che vi propongo.

Se confrontate un file nella cartella fino-2009 con uno della cartella da-2010, troverete che nel primo vengono anche esplicitati il numero di commenti al post, mentre manca nel secondo, causa i tag cambiati nel tempo. Però l'importante è sapere che esistono dei commenti e che possono essere recuperati. Lo script commenti1.R è un embrione di possibile scraping dei commenti da WayBackMachine. 

- Come recuperare il massimo numero di commenti?
- Come associarli ai post, tenendo conto che i titoli dei post di Ping! andranno in qualche modo rivisti, corretti e integrati?


