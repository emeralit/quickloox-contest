library(rvest)
library(openssl)

mesi <- c(
  "gen",
  "feb",
  "mar",
  "apr",
  "mag",
  "giu",
  "lug",
  "ago",
  "set",
  "ott",
  "nov",
  "dic"
)

mesi2 <- c(
  "gennaio",
  "febbraio",
  "marzo",
  "aprile",
  "maggio",
  "giugno",
  "luglio",
  "agosto",
  "settembre",
  "ottobre",
  "novembre",
  "dicembre"
)

# pagine <- seq(124, 130)
# lapply(seq_along(pagine), function(pp) {
url <- paste0("https://web.archive.org/web/20091224213049/http://www.macworld.it/blogs/ping/?m=200912")
print(url)
s <- read_html(url)

titolo <- s |>
  html_elements("h2") |>
  html_text2()
titolo <- titolo
titolo <- gsub("/-", "", titolo)
titolo <- gsub("’", "-", titolo)
titolo <- gsub("/", "-", titolo)
titolo <- gsub(",", "", titolo)
titolo <- sub("\\d{1,4} ", "", titolo)
titolo <- gsub(" ", "-", titolo) |> tolower()
titolo

datapost <- s |>
  html_elements(".articolo small") |>
  html_text2()
datapost <- gsub(",", "", datapost)
datapost
datapost <- gsub("(^.*\\d{1,2} \\d{4})(\\|.*$)", "\\1", datapost)
datapost <- as.POSIXct(datapost, format = "%B %d %Y")

# giornopost <- gsub("(^.*) (\\d{1,2}) (.*)$", "\\2", datapost)
# giornopost <- gsub("^(\\d)$", "0\\1", giornopost)
# giornopost
#
# mesepost <- gsub("(^.*)( \\d{1,2} .*)$", "\\1", datapost) |> tolower()
# mesepost <- match(mesepost, mesi2)
# mesepost <- gsub("^(\\d)$", "0\\1", mesepost)
# mesepost
#
# annopost <- gsub("(^.*) (\\d{1,2}) (.*)$", "\\3", datapost)
# annopost <- gsub("^(\\d)$", "0\\1", annopost)
# annopost

print(paste(datapost, titolo, sep = "-"))
# })
