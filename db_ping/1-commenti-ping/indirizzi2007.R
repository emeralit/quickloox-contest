library(rvest)
library(openssl)

mesi <- c(
  "gen",
  "feb",
  "mar",
  "apr",
  "mag",
  "giu",
  "lug",
  "ago",
  "set",
  "ott",
  "nov",
  "dic"
)

mesi2 <- c(
  "gennaio",
  "febbraio",
  "marzo",
  "aprile",
  "maggio",
  "giugno",
  "luglio",
  "agosto",
  "settembre",
  "ottobre",
  "novembre",
  "dicembre"
)

# pagine <- seq(1468, 1468)

# lapply(seq_along(pagine), function(pp) {
url <- paste0("https://web.archive.org/web/20091224213049/http://www.macworld.it/blogs/ping/?p=1468")
print(url)
s <- read_html(url)

titolo <- s |>
  html_elements("h2 a") |>
  html_text2()
titolo <- titolo[1]
titolo <- gsub(" ", "-", titolo) |> tolower()
titolo <- gsub("/-", "", titolo)
titolo <- gsub("’", "-", titolo)
titolo <- gsub("/", "-", titolo)
titolo <- gsub(",", "", titolo)
titolo <- sub("\\d{1,4}-", "", titolo)
print(titolo)

datapost <- s |>
  html_element("small") |>
  html_text2()
datapost <- gsub(",", "", datapost)
datapoat <- gsub("\\#", "", datapost)
datapost <- gsub("(^.*\\d{1,2} \\d{4})(\\|.*$)", "\\1", datapost)
datapost

giornopost <- gsub("(^.*) (\\d{1,2}) (.*)$", "\\2", datapost)
giornopost <- gsub("^(\\d)$", "0\\1", giornopost)
giornopost

mesepost <- gsub("(^.—) (.+) (\\d{1,2}) (.*)$", "\\2", datapost) |> tolower()
mesepost
mesepost <- match(mesepost, mesi2)
mesepost <- gsub("^(\\d)$", "0\\1", mesepost)
mesepost

annopost <- gsub("(^.*) (\\d{1,2}) (.*)$", "\\3", datapost)
annopost <- gsub("^(\\d)$", "0\\1", annopost)
annopost


autore <- s |>
  html_elements("li cite") |>
  html_text2()
autore

datacommento <- s |>
  html_elements(".commentmetadata a") |>
  html_text2()
datacommento <- gsub(",", "", datacommento)
datacommento <- gsub("at ", "", datacommento)
datacommento <- gsub("(\\d{1,2})(th)", "\\1", datacommento)
datacommento <- gsub("(\\d{1,2})(st)", "\\1", datacommento)
datacommento <- gsub("(\\d{1,2})(nd)", "\\1", datacommento)
datacommento <- gsub("(\\d{1,2})(rd)", "\\1", datacommento)
datacommento <- as.POSIXct(datacommento, format = "%B %d %Y %H:%M")
datacommento

annocommento <- gsub("(^\\d{4})(.*$)", "\\1", datacommento)
annocommento

mesecommento <- gsub("(^.*)-(\\d{1,2})-(.*$)", "\\2", datacommento)
mesecommento

giornocommento <- gsub("(^.*)-(\\d{1,2})-(.*$)", "\\2", datacommento)
giornocommento <- gsub("^(\\d)$", "0\\1", giornocommento)
giornocommento

orariocommento <- gsub("(^.*)-(\\d{1,2}-\\d{1,2} )(.*$)", "T\\3", datacommento)
orariocommento

nuovadata <-
  paste0(paste(annocommento, mesecommento, giornocommento, sep = "-"), orariocommento)
nuovadata

datapost <- paste(annopost, mesepost, giornopost, sep = "-")
datapost

commenti <- s |>
  html_elements("ol li") |>
  html_text2()
commenti <- sub("^.*m\\n\\n", "", commenti)
print(paste("Numero commenti:", length(commenti)))

data_epoch <-
  as.integer(as.POSIXct(nuovadata, format = "%Y-%m-%dT%H:%M:%S"))
data_epoch

acazzo <- runif(seq_along(commenti), 1000000000, 9999999999)
acazzo



lapply(seq_along(commenti), function(u) {
  t1 <- paste(datapost, titolo, sep = "-")
  t2 <- paste(data_epoch, round(acazzo, 0), "cmt", sep = ".")
  t3 <- paste(t1, t2, sep = "-")

  percorso <- paste0("/Users/mimmo/commentibis/", t3[u])
  write(commenti[u], file = percorso)
  etichette <- paste0(
    "<item><parent>", t1, "</parent>", "<w3cdate>", nuovadata[u], "</w3cdate>", "<description>", commenti[u], "</description>", "<ipaddress></ipaddress><author>", autore[u], "</author><email></email><link></link></item>"
  )
  writeLines(etichette, percorso)
})
# Sys.sleep(2)
# })
