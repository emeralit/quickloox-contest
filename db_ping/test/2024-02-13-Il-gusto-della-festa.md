---
title: Il gusto della festa
date: 2024-02-13
tags: ["ping"]
---

Time Machine, in s&#233;, &#232; tecnologia piuttosto semplice. Tanto che un bravo utilizzatore di Linux ha pensato di farsene una <a href="http://code.google.com/p/flyback/" target="_blank">versione personale</a>.

Il novantanove percento di noi, tuttavia, solo a leggere la pagina si sente preso dallo sconforto. Il computer dovrebbe servire a raggiungere obiettivi, non essere un obiettivo.

Ecco perch&#233; Time Machine merita. &#200; un pacco regalo, che basta scartare per essere usato senza preoccupazioni, come in un piccolo Natale informatico.