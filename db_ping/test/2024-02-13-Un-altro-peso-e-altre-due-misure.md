---
title: Un altro peso e altre due misure
date: 2024-02-13
tags: ["ping"]
---

Vlc, programma <i>open source</i> per la visione di filmati, &#232; <a href="http://www.tuaw.com/2011/01/08/vlc-app-removed-from-app-store/" target="_blank">stato escluso da App Store</a> perch&#233; uno dei programmatori del progetto ne ha chiesto ad Apple la rimozione, nonostante il progetto in quanto tale non abbia preso posizione e anzi, secondo i programmatori che hanno realizzato l&#8217;edizione di Vlc per App Store, abbia fattivamente collaborato al progetto.

Un sacco di polemiche.

Silenzio assordante sul fatto che Windows Marketplace, la scopiazzatura di App Store per Windows Phone 7, <a href="http://create.msdn.com/downloads/?id=638" target="_blank">proibisce esplicitamente la diffusione di un&#8217;intera categoria di software libero</a>.

Apple non &#232; si &#232; neanche sognata di inserire qualcosa del genere nei regolamenti di App Store e anzi, a partire dalla mia sempre dolce ossessione <a href="http://www.wesnoth.org/" target="_blank">Battle for Wesnoth</a>, i software liberi presenti su App Store ci sono eccome.