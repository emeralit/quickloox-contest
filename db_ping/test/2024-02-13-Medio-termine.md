---
title: Medio termine
date: 2024-02-13
tags: ["ping"]
---

<a href="http://www.macworld.it/ping/life/2011/02/18/sdegno-damante-poco-dura/comment-page-1/#comment-14093" target="_blank">Commenta</a> <b>iSimone</b>:

<cite>Me la segno e fra sei mesi, se non &#232; vero, vengo qui a rinfacciarglielo ogni giorno&#8230;
</cite>

Sarebbe bellissimo se accadesse davvero. Senza scherzi; il problema vero dell&#8217;informazione attuale &#8211; mica solo Mac &#8211; &#232; proprio che si pu&#242; scrivere qualsiasi cosa, tanto non si paga mai pegno.

Per quanto mi riguarda, avere alla calcagna persone che mi chiedono conto delle cose che ho scritto &#232; ricevere un grande favore e incoraggio con amicizia chiunque a farmi le pulci sempre e comunque su quello che affermo.

Non so chi ricorda l&#8217;<i>antennagate</i>, il grave problema all&#8217;antenna che affliggeva iPhone 4 e che forse avrebbe costretto Apple a richiamare tutti gli apparecchi venduti&#8230; non sto a linkare, ma tra agosto e settembre si parlava a malapena di qualsiasi altra cosa.

Non se ne parla pi&#249; e gli iPhone 4 si vendono a carrettate. Se &#232; un problema &#232; cos&#236; grave, perch&#233; nessuno pi&#249; ne parla, quando si vendono molti pi&#249; iPhone di prima e dovrebbe anzi emergere maggiormente? Se non &#232; un problema o almeno non &#232; cos&#236; grave, perch&#233; se ne parlava in quei termini apocalittici?

Adesso, ribadisco, si fanno le polemiche sugli abbonamenti via App Store. E ribadisco che tra sei mesi non ne parler&#224; pi&#249; nessuno.

S&#236;, spero che tutti se la segnino sull&#8217;agenda e si ripromettano di controllare.

Nel frattempo Advertising Age riporta che Elle, Nylon e Popular Science <a href="http://adage.com/mediaworks/article?article_id=148920" target="_blank">hanno aderito al meccanismo di App Store</a>. La seconda &#232; ignota a noi italiani, ma la terza - seppure inglese &#8211; &#232; vendutissima e Elle &#232; un periodico multinazionale a tutti noto.